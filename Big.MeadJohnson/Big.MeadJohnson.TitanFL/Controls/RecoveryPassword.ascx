﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="RecoveryPassword.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.Controls.RecoveryPassword" %>
<%@ Register Src="ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc1" %>
<div id="divFirst" runat="server">
    <div class="row">
        <div class="span6 offset3">
            <h3>¿Olvidó su contraseña?</h3>
            <p>Ingrese su Nombre de Usuario para que reciba una contraseña a su correo.</p>
            <div class="form-horizontal">
                <div class="control-group">
                    <label class="control-label">Nombre de Usuario</label>
                    <div class="controls">
                        <asp:TextBox ID="txtUser" runat="server"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="rfvUser" runat="server" ControlToValidate="txtUser"
                            ErrorMessage="Ingrese el Nombre de Usuario" Display="None">*</asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <asp:Button ID="btnSend" runat="server" Text="Enviar" OnClick="btnSend_Click" CssClass="btn btn-primary" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="divSecond" runat="server" style="display: none">
    <div class="row">
        <div class="span6">
            <div class="alert alert-info">
                <uc1:ResultMessage ID="resultMessage" runat="server" />
            </div>
            <div id="divThird" runat="server" style="display: none">
                <asp:LinkButton ID="lnkRetornar" runat="server" PostBackUrl="~/APP/Login.aspx" CssClass="btn btn-primary">Ingresar</asp:LinkButton>
            </div>
        </div>
    </div>
</div>
<asp:ValidationSummary ID="vSummary" runat="server" DisplayMode="List" ShowMessageBox="true"
    ShowSummary="false" />
