﻿//===============================================================================
// ControlFecha.js
// Author : Jorge Vélez (jvelez@dmgcolombia.com)
//          Jairo Yate (jyate@dmgcolombia.com)
//          Wveimar López M. (wlopez@dmgcolombia.com)
// Last update: 01 diciembre 2006
//===============================================================================
// Copyright © Data Marketing Group.  All rights reserved.
//===============================================================================

function FillValue(yearCtrl, monthCtrl, dayCtrl, hiddenDate)
{
   var mylistYear = document.getElementById(yearCtrl);
   var mylistMonth = document.getElementById(monthCtrl);
   var mylistDay = document.getElementById(dayCtrl);
   
   var year = mylistYear.options[mylistYear.selectedIndex].value;
   var month = mylistMonth.options[mylistMonth.selectedIndex].value;
   var day = mylistDay.options[mylistDay.selectedIndex].value;
   
//   if (year != -1)
//      document.getElementById(hiddenDate).value = year + "/" + month + "/" + day;
//   else
//      document.getElementById(hiddenDate).value = "";
   
   if ( document.getElementById(hiddenDate) != undefined )
	{
      document.getElementById(hiddenDate).value = year + "/" + month + "/" + day;
   }
}

function DateFillDays(yearCtrl, monthCtrl, dayCtrl, hiddenDate)
{
  var mylistYear = document.getElementById(yearCtrl);
  var mylistMonth = document.getElementById(monthCtrl);

  var year = mylistYear.options[mylistYear.selectedIndex].value;  
  var month = mylistMonth.options[mylistMonth.selectedIndex].value;

  if (year == -1)
  {
     document.getElementById(monthCtrl).value = '-1';
     month = '-1';
  }
	
  DateCleanSelected(dayCtrl);
  
  var sizeDays = 0;
  
  switch ( month )
  {
     case '01':
     case '03':
     case '05':
     case '07':
     case '08':
     case '10':
     case '12':
        sizeDays = 31;
        break;
     case '04':
     case '06':
     case '09':
     case '11':
        sizeDays = 30;
        break;
     case '02':
        if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
           sizeDays = 29;
        else
           sizeDays = 28;
        break;
     case '-1':
		  sizeDays = 0;
		  break;
  }
  
  var elements = "-1|- Día -,";
  
  for ( var i = 1; i <= sizeDays; i++ )
  {
     if (i < 10)
         elements += "0" + i + "|" + "0" + i + ",";
     else
         elements += i + "|" + i + ",";
  }
  
  if ( elements.length != 0 )
     elements = elements.substring(0, elements.length-1);
  
  DateFillSelect(elements, dayCtrl);

   if ( document.getElementById(hiddenDate) != undefined )
   {
      document.getElementById(hiddenDate).value = year + "/" + month + "/" + "-1";
   }
}

function DateCleanSelected(control)
{
   for ( var i = (document.getElementById(control).length - 1); i != -1; i-- )
   {
      document.getElementById(control).remove(0);
   }
}

function DateFillSelect(stringValues, idControl)
{
  var optionsList = stringValues.split(",");
  var elSel = null;
  var item = null;
  
  if ( document.getElementById(idControl) != null )
     elSel = document.getElementById(idControl);
  
  if ( elSel != null )
  {
     for (var i=0; i < optionsList.length; i++) 
     {
        item = optionsList[i].split("|");
        var elOptNew = document.createElement('option');
        elOptNew.value = item[0];
        elOptNew.text = item[1];
        
        try {
           elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
        }
        catch(ex) {
           elSel.add(elOptNew); // IE only
        }
     }
  }
}