﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ControlFecha.ascx.cs" Inherits="OnData.TitanFL.Web.UI.Controls.ControlFecha" %>
<div>
   <asp:DropDownList ID="YearList" runat="server" AutoPostBack="false">
   </asp:DropDownList>
   <asp:CustomValidator ID="cvYearList" runat="server" ErrorMessage="Se debe seleccionar el año"
      ControlToValidate="YearList" ClientValidationFunction="ValidatorDate" ValidateEmptyText="true"
      EnableClientScript="false">*</asp:CustomValidator>
   <asp:DropDownList ID="MonthList" runat="server" AutoPostBack="false">
   </asp:DropDownList>
   <asp:CustomValidator ID="cvMonthList" runat="server" ErrorMessage="Se debe seleccionar el mes"
      ControlToValidate="MonthList" ClientValidationFunction="ValidatorDate" ValidateEmptyText="true"
      EnableClientScript="false">*</asp:CustomValidator>
   <asp:DropDownList ID="DayList" runat="server" AutoPostBack="false">
   </asp:DropDownList>
   <asp:CustomValidator ID="cvDayList" runat="server" ErrorMessage="Se debe seleccionar el día"
      ControlToValidate="DayList" ClientValidationFunction="ValidatorDate" ValidateEmptyText="true"
      EnableClientScript="false">*</asp:CustomValidator>
   <asp:HiddenField ID="hdDate" runat="server" />
</div>

<script type="text/javascript">
    function ValidatorDate(source, arguments) {
        if (arguments.Value == "-1")
            arguments.IsValid = false;
        else
            arguments.IsValid = true;
    }
</script>