﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CompanyControl.ascx.cs" Inherits="Multisponsor.Alliances.Backend.Web.UI.Util.CompanyControl" %>
<telerik:RadComboBox ID="rcbCompany" ClientIDMode="Static" NoWrap="true" Width="200" MaxHeight="100"
    runat="server" EmptyMessage="Seleccione una compañía" EnableLoadOnDemand="true"
    LoadingMessage="Cargando.." OnClientLoad="OnCompanyClientLoad"
    OnClientItemsRequested="OnCompanyClientItemsRequested">
    <WebServiceSettings Method="GetCompanies" Path="~/Services/UIService.asmx" />
</telerik:RadComboBox>

<asp:RequiredFieldValidator ID="rfvRequiredCompany" runat="server" ErrorMessage="El campo compañia es requerido"
    Display="Dynamic" ControlToValidate="rcbCompany" InitialValue="">*</asp:RequiredFieldValidator>
<asp:CustomValidator ID="rcvRequiredCompany" runat="server" ErrorMessage="Seleccione un valor correcto"
    ControlToValidate="rcbCompany" EnableClientScript="true" Display="None" ValidateEmptyText="true"
    ClientValidationFunction="CheckCompanyValue" />
<asp:HiddenField ID="hfCompanySelected" runat="server" />
<telerik:RadScriptBlock ID="scriptBlock" runat="server">
    <script type="text/javascript">

        function CheckCompanyValue(source, arguments) {
            var companyId = $find("<%=rcbCompany.ClientID %>").get_value();
            if (companyId == "" || companyId == null) {

                arguments.IsValid = false;
            }
            else {
                arguments.IsValid = true;
            }
        }

        function OnCompanyClientLoad(sender, eventArgs) {

            var selectedCompanyId = $get("<%=hfCompanySelected.ClientID %>").value;
            if (selectedCompanyId != "") {
                $find("<%=rcbCompany.ClientID %>").requestItems("12," + selectedCompanyId, true);
            }

        }

        function OnCompanyClientItemsRequested(sender, eventArgs) {
            var selectedCompanyId = $get("<%=hfCompanySelected.ClientID %>").value;
            if (selectedCompanyId != "") {
                var item = $find("<%=rcbCompany.ClientID %>").findItemByValue(selectedCompanyId);
                if (item != null)
                    item.select();

                $get("<%=hfCompanySelected.ClientID %>").value = "";
            }
        }
    </script>
</telerik:RadScriptBlock>
