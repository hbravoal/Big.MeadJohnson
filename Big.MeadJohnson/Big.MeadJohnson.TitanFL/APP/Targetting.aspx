﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/Popup.Master" AutoEventWireup="true"
    CodeBehind="Targetting.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Targetting" %>

<%@ Register Src="Controls/TargetDimension.ascx" TagName="TargetDimension" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script src="Scripts/YUI/yahoo/yahoo-min.js" type="text/javascript"></script>
    <script src="Scripts/YUI/connection/connection-min.js" type="text/javascript"></script>
    <script src="Scripts/YUI/event/event.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="Scripts/YUI/container/container-min.js"></script>
    <script src="Scripts/YUI/treeview/treeview.js" type="text/javascript"></script>
    <script src="Scripts/TitanFL.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="Scripts/YUI/container/assets/container.css" />
    <link rel="stylesheet" type="text/css" href="Css/tree.css" />
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" Width="100%" MultiPageID="radMultiPage"
            SelectedIndex="0">
            <Tabs>
                <telerik:RadTab Text="Configuración de la Consulta" PageViewID="QueryConfig" Value="QueryConfig"
                    Selected="True">
                </telerik:RadTab>
                <telerik:RadTab Text="Consultas Guardadas" PageViewID="SavedQueries" Value="SavedQueries">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" runat="server" Width="100%" SelectedIndex="0"
            CssClass="pageView">
            <telerik:RadPageView ID="QueryConfig" runat="server">
                <div id="adminedit2">
                    Entidad de Negocio:&nbsp;<telerik:RadComboBox ID="rcbBusinessEntities" runat="server"
                        Width="200px" NoWrap="true" AutoPostBack="true" AppendDataBoundItems="true" OnSelectedIndexChanged="rcbBusinessEntities_SelectedIndexChanged">
                        <Items>
                            <telerik:RadComboBoxItem Text="[Seleccione]" Value="" />
                        </Items>
                    </telerik:RadComboBox>
                    <br />
                    <br />
                    Roles:&nbsp;<telerik:RadListBox ID="rlbRoles" runat="server" Width="300px" Height="70px"
                        CheckBoxes="true" AutoPostBack="True" OnItemCheck="rlbRoles_ItemCheck">
                    </telerik:RadListBox>
                    <br />
                    <br />
                    <uc1:TargetDimension ID="businessEntityDimension" runat="server" Visible="false" />
                    <br />
                    <telerik:RadPanelBar runat="server" ID="radPanelBar" Visible="true" Width="100%"
                        ExpandMode="SingleExpandedItem" Enabled="false">
                    </telerik:RadPanelBar>
                    <br />
                    <div id="saveZone" runat="server">
                        Nombre de la Consulta:&nbsp;<asp:TextBox ID="txtQueryName" runat="server" MaxLength="100"
                            Width="250px"></asp:TextBox>&nbsp;<asp:RequiredFieldValidator ID="rfvQueryName" runat="server"
                                ControlToValidate="txtQueryName" ErrorMessage="El nombre de la consulta es requerido"
                                ValidationGroup="SaveQuery" Display="Dynamic"></asp:RequiredFieldValidator>
                        <br />
                        <br />
                    </div>
                    <asp:Button ID="btnRun" runat="server" Text="Ejecutar Consulta" OnClick="btnRun_Click" />&nbsp;<asp:Button
                        ID="btnGenerateTarget" runat="server" Text="Generar Target" OnClick="btnGenerateTarget_Click"
                        Visible="false" />&nbsp;<asp:Button ID="btnExport" runat="server" Text="Exportar Consulta"
                            OnClick="btnExport_Click" />&nbsp;<asp:Button ID="btnSave" runat="server" Text="Guardar Consulta"
                                OnClick="btnSave_Click" ValidationGroup="SaveQuery" />
                    <br />
                    <br />
                    <div style="height: 100%">
                        <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                            <script type="text/javascript">

                                function GridCreated(sender, eventArgs) {
                                    var gridContainer = document.getElementById("<%=rgList.ClientID %>_GridData");
                                    if (gridContainer != null)
                                        gridContainer.style.height = "100%";
                                }
                            </script>
                        </telerik:RadScriptBlock>
                        <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="True" AllowPaging="true"
                            PageSize="10" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                            OnColumnCreated="rgList_ColumnCreated">
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" FileName="Target" IgnorePaging="true">
                            </ExportSettings>
                            <MasterTableView TableLayout="Auto" Width="100%" CommandItemDisplay="None">
                                <CommandItemSettings RefreshText="Actualizar" ExportToExcelText="Exportar a Excel"
                                    ShowAddNewRecordButton="false" ShowRefreshButton="false" ShowExportToExcelButton="true" />
                                <Columns>
                                </Columns>
                                <NoRecordsTemplate>
                                    No se encontraron registros
                                </NoRecordsTemplate>
                            </MasterTableView>
                            <ClientSettings AllowDragToGroup="false">
                                <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                <Selecting AllowRowSelect="false" />
                                <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                            </ClientSettings>
                        </telerik:RadGrid>
                        <hr />
                        <strong>Número total de registros:&nbsp;<asp:Label ID="lblCantidadRegistros" runat="server"
                            Text="0"></asp:Label></strong>
                    </div>
                </div>
                <asp:Button ID="btnAddFilter" runat="server" OnClick="btnAddFilter_Click" Style="visibility: hidden" />
                <asp:Button ID="btnRemoveFilter" runat="server" OnClick="btnRemoveFilter_Click" Style="visibility: hidden" />
                <asp:Button ID="btnUpdateFilterConnector" runat="server" OnClick="btnUpdateFilterConnector_Click"
                    Style="visibility: hidden" />
                <asp:HiddenField ID="hfViewName" runat="server" />
                <asp:HiddenField ID="hfPropertyName" runat="server" />
                <asp:HiddenField ID="hfPropertyDisplayName" runat="server" />
                <asp:HiddenField ID="hfFilter" runat="server" />
                <asp:HiddenField ID="hfFilterOperator" runat="server" />
                <asp:HiddenField ID="hfFilterValue" runat="server" />
                <asp:HiddenField ID="hfFilterSecondValue" runat="server" />
                <asp:HiddenField ID="hfFilterGuid" runat="server" />
                <asp:HiddenField ID="hfFilterConnector" runat="server" />
                <asp:HiddenField ID="hfAllowExport" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="SavedQueries" runat="server">
                <div id="adminedit2">
                    <telerik:RadGrid ID="rgQueryList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgQueryList_NeedDataSource"
                        OnItemCommand="rgQueryList_ItemCommand">
                        <MasterTableView TableLayout="Auto" Width="100%">
                            <Columns>
                                <telerik:GridTemplateColumn>
                                    <ItemTemplate>
                                        <a href='<%#string.Format("Targetting.aspx?targetGuid={0}&targetType={1}",  Eval("GUID"), Request.QueryString["targetType"]) %>'
                                            title="Ver Detalle">
                                            <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                        </a>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="btnDeletetarget" runat="server" ToolTip="Borrar" CommandName="DeleteTarget"
                                            CommandArgument='<%#Eval("GUID") %>'>
                                            <asp:Image ID="imgDelete" runat="server" ImageUrl="~/APP/Images/icon-delete.gif" BorderWidth="0px" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn DataField="TargetDate" HeaderText="Fecha Consulta">
                                    <ItemStyle Width="160px" />
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Name" HeaderText="Consulta">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                No se encontraron consultas almacenadas
                            </NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </telerik:RadAjaxPanel>
    <telerik:RadWindowManager ID="rdPopUp" runat="server" ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radWindow" runat="server" Width="760px" Height="80px" Behaviors="Close"
                InitialBehaviors="Close" Modal="true" ReloadOnShow="true" ShowContentDuringLoad="false"
                VisibleStatusbar="false">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript" language="javascript">

            function BuildFilter(viewName, propertyName, propertyDisplayName, filter, referentialEntity, referentialEntityFilterProperty) {
                radopen("TargetFilter.aspx?filter=" + filter
            + "&viewName=" + viewName
            + "&propertyName=" + propertyName
            + "&propertyDisplayName=" + encodeURIComponent(propertyDisplayName)
            + "&referentialEntity=" + referentialEntity
            + "&referentialEntityFilterProperty=" + referentialEntityFilterProperty
            , "radWindow");
            }

            function AddFilter(viewName, propertyName, propertyDisplayName, filter, filterOperator, filterValue, filterSecondValue) {
                document.getElementById('<%=hfViewName.ClientID %>').value = viewName;
                document.getElementById('<%=hfPropertyName.ClientID %>').value = propertyName;
                document.getElementById('<%=hfPropertyDisplayName.ClientID %>').value = propertyDisplayName;
                document.getElementById('<%=hfFilter.ClientID %>').value = filter;
                document.getElementById('<%=hfFilterOperator.ClientID %>').value = filterOperator;
                document.getElementById('<%=hfFilterValue.ClientID %>').value = filterValue;
                document.getElementById('<%=hfFilterSecondValue.ClientID %>').value = filterSecondValue;

                var oManager = GetRadWindowManager();
                oManager.CloseAll();

                document.getElementById('<%=btnAddFilter.ClientID %>').click();
            }

            function RemoveFilter(viewName, guid) {
                document.getElementById('<%=hfViewName.ClientID %>').value = viewName;
                document.getElementById('<%=hfFilterGuid.ClientID %>').value = guid;
                document.getElementById('<%=btnRemoveFilter.ClientID %>').click();
            }

            function UpdateFilter(viewName, guid, filterList) {
                document.getElementById('<%=hfViewName.ClientID %>').value = viewName;
                document.getElementById('<%=hfFilterGuid.ClientID %>').value = guid;
                document.getElementById('<%=hfFilterConnector.ClientID %>').value = filterList.options[filterList.selectedIndex].value;
                document.getElementById('<%=btnUpdateFilterConnector.ClientID %>').click();
            }


            function pnlRequestStarted(ajaxPanel, eventArgs) {

                //if (eventArgs.EventTarget == "ctl00$maincontent$rgList$ctl00$ctl03$ctl01$ExportToExcelButton") {
                if (eventArgs.EventTarget == "ctl00$maincontent$btnExport") {
                    document.getElementById('<%=hfAllowExport.ClientID %>').value = "true";
                    eventArgs.EnableAjax = false;
                }
            }
           

    
        </script>
    </telerik:RadScriptBlock>
</asp:Content>
