﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityContentCategories.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.ConsumersSystem.ContentCategories.BusinessEntityContentCategories" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24">
                    <asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px">
                    <div style="height: 100%">
                        Punto de Contacto:&nbsp;
                        <asp:DropDownList ID="ddlContactPoints" runat="server" AutoPostBack="True" DataTextField="DESCRIPTION"
                            DataValueField="ID" OnSelectedIndexChanged="ddlContactPoints_SelectedIndexChanged">
                        </asp:DropDownList>
                        <br />
                        <br />
                        <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                            <script type="text/javascript">

                                function GridCreated(sender, eventArgs) {
                                    var gridContainer = document.getElementById("ctl00_maincontent_rgList_GridData");
                                    if (gridContainer != null)
                                        gridContainer.style.height = "100%";
                                }
                            </script>
                        </telerik:RadScriptBlock>
                        <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                            PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                            OnItemDataBound="rgList_ItemDataBound">
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <MasterTableView TableLayout="Fixed" Width="100%">
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Categoría de Contenido" DataField="NAME">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Tema de Interés">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTopicOfInterest" runat="server" Enabled="false" />
                                            <asp:HiddenField ID="hfTopicOfInterest" runat="server" Value='<%#Eval("ISFROMUSER") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Target">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTarget" runat="server" Enabled="false" />
                                            <asp:HiddenField ID="hfTarget" runat="server" Value='<%#Eval("ISFROMTARGET") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridTemplateColumn HeaderText="Tracking">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkTracking" runat="server" Enabled="false" />
                                            <asp:HiddenField ID="hfTracking" runat="server" Value='<%#Eval("ISFROMTRACKING") %>' />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Visitas" DataField="VISITS">
                                    </telerik:GridBoundColumn>
                                </Columns>
                                <NoRecordsTemplate>
                                    Sin categorías de contenido relacionadas.
                                </NoRecordsTemplate>
                            </MasterTableView>
                            <ClientSettings AllowDragToGroup="false">
                                <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                <Selecting AllowRowSelect="false" />
                                <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
