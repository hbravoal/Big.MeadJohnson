﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="List.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.ConsumersSystem.ContentCategories.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <div id="adminedit">
        <div>
            Punto de Contacto:&nbsp;<asp:DropDownList ID="ddlContactPoints" runat="server" AutoPostBack="True"
                DataTextField="DESCRIPTION" DataValueField="ID" OnSelectedIndexChanged="ddlContactPoints_SelectedIndexChanged">
            </asp:DropDownList>
            <br />
            <br />
            <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                PageSize="20" GridLines="None" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                OnItemDataBound="rgList_ItemDataBound">
                <MasterTableView TableLayout="Fixed">
                    <Columns>
                        <telerik:GridTemplateColumn>
                            <ItemTemplate>
                                <a href='Detail.aspx?contentCategoryGuid=<%# Eval("GUID") %>' title="Editar">
                                    <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                </a>
                            </ItemTemplate>
                            <ItemStyle Width="35px" />
                            <HeaderStyle Width="35px" />
                        </telerik:GridTemplateColumn>
                        <telerik:GridBoundColumn HeaderText="Id Categoría" DataField="ID">
                            <ItemStyle Width="90px" />
                            <HeaderStyle Width="90px" />
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Punto de Contacto" DataField="CONTACTPOINT">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Nombre" DataField="NAME">
                        </telerik:GridBoundColumn>
                        <telerik:GridBoundColumn HeaderText="Descripción" DataField="DESCRIPTION">
                        </telerik:GridBoundColumn>
                        <telerik:GridTemplateColumn HeaderText="Activo">
                            <ItemTemplate>
                                <asp:CheckBox ID="chkIsActive" runat="server" Enabled="false" />
                                <asp:HiddenField ID="hfIsActive" runat="server" Value='<%#Eval("ISACTIVE") %>' />
                            </ItemTemplate>
                            <ItemStyle Width="60px" />
                            <HeaderStyle Width="60px" />
                        </telerik:GridTemplateColumn>
                    </Columns>
                    <NoRecordsTemplate>
                        Sin categorías de contenido registradas
                    </NoRecordsTemplate>
                </MasterTableView>
            </telerik:RadGrid>
        </div>
        <div style="width: 100%; text-align: right">
            <br />
            <asp:Button ID="btnNew" runat="server" OnClientClick="window.location.href='Detail.aspx';return false;"
                Text="Ingresar" />
        </div>
    </div>
</asp:Content>
