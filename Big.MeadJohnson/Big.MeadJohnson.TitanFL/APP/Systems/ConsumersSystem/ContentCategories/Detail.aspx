﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Detail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.ConsumersSystem.ContentCategories.Detail" %>

<%@ Register Src="../../../../Controls/ResultMessage.ascx" TagName="ResultMessage"
    TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" Width="100%" MultiPageID="radMultiPage"
            SelectedIndex="0">
            <Tabs>
                <telerik:RadTab Text="Información Categoría" PageViewID="categoryInfo" Value="categoryInfo">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" runat="server" Width="100%" SelectedIndex="0"
            CssClass="pageView">
            <telerik:RadPageView ID="categoryInfo" runat="server">
                <div style="text-align: left; padding: 10px">
                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td align="left">
                                <table cellpadding="4" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="text-align: left;">
                                            Id Categoría
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:Literal ID="ltCategoryId" runat="server" Text="?"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Nombre
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtName" runat="server" Width="250px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidatorName" runat="server" ControlToValidate="txtName" ErrorMessage="El nombre es requerido."
                                                Display="None" ValidationGroup="Profile">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            Descripción
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDescription" runat="server" Width="250px" MaxLength="300" TextMode="MultiLine"
                                                Height="60px"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Punto de Contacto
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlContactPoints" runat="server" DataTextField="DESCRIPTION"
                                                DataValueField="ID" AppendDataBoundItems="true">
                                                <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorProgramType" runat="server"
                                                ControlToValidate="ddlContactPoints" ErrorMessage="El punto de contacto es requerido."
                                                Display="None" InitialValue="" ValidationGroup="Profile">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;">
                                            <span style="color: Red">*</span>&nbsp;Activo
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <uc1:ResultMessage ID="resultMessage" runat="server" />
                                            <br />
                                            <asp:HyperLink ID="hlBuildTarget" runat="server" Text="Configurar Target" Visible="false"></asp:HyperLink>&nbsp;&nbsp;<asp:LinkButton
                                                ID="btnCleanTarget" runat="server" Text="Borrar Target" Visible="false" OnClick="btnCleanTarget_Click"></asp:LinkButton>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: left; padding: 10px">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" ValidationGroup="Profile" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CausesValidation="False"
                                    OnClientClick="window.location.href='List.aspx';return false;" />
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="Profile" />
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
    <asp:Button ID="btnSetTarget" runat="server" OnClick="btnSetTarget_Click" Style="visibility: hidden" />
    <asp:HiddenField ID="hfTargetGuid" runat="server" />
    <telerik:RadWindowManager ID="rdPopUp" runat="server" ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radWindow" runat="server" Width="900px" Height="600px" Behaviors="Close"
                InitialBehaviors="Close" Modal="true" ReloadOnShow="true" ShowContentDuringLoad="false"
                VisibleStatusbar="false" NavigateUrl="~/APP/Targetting.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadScriptBlock ID="RadScriptBlock2" runat="server">
        <script type="text/javascript" language="javascript">

            function LoadTarget(targetGuid) {
                radopen("../../../Targetting.aspx?targetType=ContentCategoryTarget"
                    + "&targetGuid=" + targetGuid
                    , "radWindow");
            }

            function SetTarget(targetGuid) {
                var oManager = GetRadWindowManager();
                oManager.CloseAll();

                document.getElementById('<%=hfTargetGuid.ClientID %>').value = targetGuid;
                document.getElementById('<%=btnSetTarget.ClientID %>').click();

            }
    
        </script>
    </telerik:RadScriptBlock>
</asp:Content>
