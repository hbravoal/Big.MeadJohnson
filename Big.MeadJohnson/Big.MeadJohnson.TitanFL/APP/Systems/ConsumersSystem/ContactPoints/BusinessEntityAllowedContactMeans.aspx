﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityAllowedContactMeans.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.ConsumersSystem.ContactPoints.BusinessEntityAllowedContactMeans" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24">
                    <asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px">
                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="text-align: left;" width="300px">
                                Punto de Contacto
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlContactPoints" runat="server" AutoPostBack="True" DataTextField="DESCRIPTION"
                                    DataValueField="ID" OnSelectedIndexChanged="ddlContactPoints_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Acepta Recibir Información por correo electrónico
                            </td>
                            <td>
                                <asp:CheckBox ID="chkAllowEmailContact" runat="server" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Acepta Recibir Información en telefono celular
                            </td>
                            <td>
                                <asp:CheckBox ID="chkAllowMobilePhoneContact" runat="server" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Acepta Recibir Información por telefono fijo
                            </td>
                            <td>
                                <asp:CheckBox ID="chkAllowPhoneContact" runat="server" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Acepta Recibir Información por correo directo
                            </td>
                            <td>
                                <asp:CheckBox ID="chkDirectmailContact" runat="server" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Acepta Recibir Información por redes sociales
                            </td>
                            <td>
                                <asp:CheckBox ID="chkSocialNetworkContact" runat="server" Checked="false" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnSaveValue" runat="server" Text="Guardar" OnClick="btnSaveValue_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
