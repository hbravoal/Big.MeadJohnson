﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/Popup.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityProfileDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.ConsumersSystem.Profiles.BusinessEntityProfileDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation-min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit2">
        <table cellpadding="4" cellspacing="4" width="100%" border="0">
            <tr>
                <td bgcolor="LightSteelBlue">
                    <strong>Perfiles</strong>
                </td>
                <td bgcolor="LightSteelBlue">
                    <strong>Valores de Perfil</strong>
                </td>
            </tr>
            <tr>
                <td style="width: 300px; vertical-align: top">
                    <telerik:RadTreeView ID="treeView" runat="server" Width="100%" OnNodeClick="treeView_NodeClick">
                        <WebServiceSettings Path="BusinessEntityProfiles.aspx" Method="GetProfiles" />
                    </telerik:RadTreeView>
                </td>
                <td style="vertical-align: top">
                    <asp:RadioButtonList ID="rbtnlProfileValues" runat="server" RepeatLayout="Table"
                        CellPadding="3" CellSpacing="3" TextAlign="Right" RepeatColumns="1" RepeatDirection="Vertical"
                        Visible="false">
                    </asp:RadioButtonList>
                    <br />
                    <asp:Literal ID="ltProfileNotSelectedMessage" runat="server" Text="Seleccione un perfil en el arbol de la izquierda"></asp:Literal>
                    <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" Visible="false" />
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript" language="javascript">

        function CloseProfile() {
            alert("Perfil guardado.");
            parent.ReloadPage();
        }
    </script>
</asp:Content>
