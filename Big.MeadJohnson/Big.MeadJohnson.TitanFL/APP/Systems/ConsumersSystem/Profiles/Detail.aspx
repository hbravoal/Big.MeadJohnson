﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Detail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.ConsumersSystem.Profiles.Detail" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Charting" TagPrefix="telerik" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" Width="100%" MultiPageID="radMultiPage"
            SelectedIndex="0">
            <Tabs>
                <telerik:RadTab Text="Información del Perfil" PageViewID="profileInfo" Value="profileInfo">
                </telerik:RadTab>
                <telerik:RadTab Text="Valores de Perfil" PageViewID="profileValues" Value="profileValues"
                    Visible="false">
                </telerik:RadTab>
                <telerik:RadTab Text="Tablero de Control" PageViewID="profileDashboard" Value="profileDashboard"
                    Visible="false">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" runat="server" Width="100%" SelectedIndex="0"
            CssClass="pageView">
            <telerik:RadPageView ID="profileInfo" runat="server">
                <div style="text-align: left; padding: 10px">
                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td align="left">
                                <table cellpadding="4" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="text-align: left;">
                                            Id Perfil
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:Literal ID="ltProfileId" runat="server" Text="?"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Descripción Perfil
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDescription" runat="server" Width="250px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidatorName" runat="server" ControlToValidate="txtDescription"
                                                ErrorMessage="La descripción es requerida." Display="None" ValidationGroup="Profile">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Rol
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlRoles" runat="server" AutoPostBack="True" AppendDataBoundItems="true"
                                                DataSourceID="odsRoles" DataTextField="DESCRIPTION" DataValueField="GUID" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged">
                                                <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:ObjectDataSource ID="odsRoles" runat="server" 
                                                SelectMethod="GetAllRolesByBusinessId" 
                                                TypeName="OnData.TitanFL.Systems.Consumers.BF.Domain.Role">
                                                <SelectParameters>
                                                    <asp:Parameter Name="businessId" Type="Int32" />
                                                </SelectParameters>
                                            </asp:ObjectDataSource>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Tipo de Perfil
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlProfileType" runat="server">
                                                <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                            </asp:DropDownList>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorProgramType" runat="server"
                                                ControlToValidate="ddlProfileType" ErrorMessage="El tipo de perfil es requerido."
                                                Display="None" InitialValue="" ValidationGroup="Profile">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;">
                                            <span style="color: Red">*</span>&nbsp;Activo
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkIsActive" runat="server" Checked="true" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: left; padding: 10px">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" ValidationGroup="Profile" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CausesValidation="False"
                                    OnClientClick="window.location.href='List.aspx';return false;" />
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="Profile" />
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="profileValues" runat="server">
                <div style="padding: 10px">
                    <table>
                        <tr>
                            <td style="width: 50%; vertical-align: top">
                                <table cellpadding="3" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="text-align: left;">
                                            Id Valor
                                        </td>
                                        <td style="text-align: left;">
                                            <asp:Literal ID="ltProfileValueId" runat="server" Text="?"></asp:Literal>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Descripción Valor
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtValueDescription" runat="server" Width="250px" MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator
                                                ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtValueDescription"
                                                ErrorMessage="La descripción del valor es requerida." Display="None" ValidationGroup="ProfileValues">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;">
                                            <span style="color: Red">*</span>&nbsp;Activo
                                        </td>
                                        <td>
                                            <asp:CheckBox ID="chkValueIsActive" runat="server" Checked="true" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:HiddenField ID="hfProfileValueGuid" runat="server" />
                                            <asp:Button ID="btnSaveValue" runat="server" Text="Guardar" OnClick="btnSaveValue_Click"
                                                ValidationGroup="ProfileValues" />&nbsp;<asp:Button ID="btnCancelValue" runat="server"
                                                    Text="Cancelar" OnClick="btnCancelValue_Click" Visible="false" />
                                            <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                                                ShowSummary="False" ValidationGroup="ProfileValues" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 50%; vertical-align: top">
                                <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="false"
                                    GridLines="None" Width="100%" OnNeedDataSource="rgList_NeedDataSource" OnItemDataBound="rgList_ItemDataBound"
                                    OnItemCommand="rgList_ItemCommand">
                                    <MasterTableView TableLayout="Fixed">
                                        <Columns>
                                            <telerik:GridTemplateColumn>
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="btnEdit" runat="server" CommandArgument='<%# Eval("GUID") %>'
                                                        ToolTip="Editar" CommandName="EditValue">
                                                        <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                                    </asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="35px" />
                                                <HeaderStyle Width="35px" />
                                            </telerik:GridTemplateColumn>
                                            <telerik:GridBoundColumn HeaderText="Id Valor Perfil" DataField="ID">
                                                <ItemStyle Width="100px" />
                                                <HeaderStyle Width="100px" />
                                            </telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn HeaderText="Descripción Valor" DataField="DESCRIPTION">
                                            </telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn HeaderText="Activo">
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="chkIsActive" runat="server" Enabled="false" />
                                                    <asp:HiddenField ID="hfIsActive" runat="server" Value='<%#Eval("ISACTIVE") %>' />
                                                </ItemTemplate>
                                                <ItemStyle Width="60px" />
                                                <HeaderStyle Width="60px" />
                                            </telerik:GridTemplateColumn>
                                        </Columns>
                                        <NoRecordsTemplate>
                                            Sin valores de perfil registrados
                                        </NoRecordsTemplate>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </td>
                        </tr>
                    </table>
                </div>
            </telerik:RadPageView>
            <telerik:RadPageView ID="profileDashboard" runat="server">
                <div style="padding: 10px">
                    <telerik:RadChart ID="radChart" runat="server" DefaultType="Pie" Width="500px" AutoTextWrap="True"
                        DataSourceID="odsProfileDistribution" OnItemDataBound="radChart_ItemDataBound"
                        Skin="LightBlue" PlotArea-EmptySeriesMessage-TextBlock-Text="Sin información">
                        <Series>
                            <telerik:ChartSeries Name="Series 1" DataYColumn="TIMES" Type="Pie">
                                <Appearance LegendDisplayMode="ItemLabels">
                                </Appearance>
                            </telerik:ChartSeries>
                        </Series>
                        <ChartTitle>
                            <TextBlock Text="Distribución Perfil">
                            </TextBlock>
                        </ChartTitle>
                    </telerik:RadChart>
                    <asp:ObjectDataSource ID="odsProfileDistribution" runat="server" SelectMethod="GetProfileDistribution"
                        TypeName="OnData.TitanFL.Systems.Consumers.BF.Domain.Profile">
                        <SelectParameters>
                            <asp:QueryStringParameter Name="profileGuid" QueryStringField="profileId" Type="String" />
                            <asp:ControlParameter ControlID="ddlRoles" Name="roleGuid" PropertyName="SelectedValue"
                                Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </div>
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
