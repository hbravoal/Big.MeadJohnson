﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="ViewManager.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.EntitySystem.ViewManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" Runat="Server">
   <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>   
   <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
   <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
   <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
   
   <div id="adminedit">
      <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
         <tr>
            <td valign="top">
               <table cellpadding="0" cellspacing="0" border="0" width="100%">
                  <tr>
                     <td height="20" align="right" valign="Top">
                        <span id="wait">
                           <img src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando...</span></td>
                  </tr>
               </table>
               <div id="divConfigView">
               </div>
            </td>
         </tr>
      </table>
      <table border="0">
         <tr>
            <td>
               <div id="divErrorAJAX">
               </div>
            </td>
         </tr>
         <tr>
            <td>
               <div id="divContentAJAX">
               </div>
            </td>
         </tr>
      </table>
   </div>
   
   <div id="dlgGroupNew" style="visibility: hidden;">
      <div id="bd">
         <table width="100%" border="0" style="padding-left:10px;padding-top:10px">
            <tr>
               <td style="width: 40%;text-align:right">
                  <span style="color:Red">*</span>&nbsp;Nombre del Grupo
               </td>
               <td style="width: 60%">
                  <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtGroupNameNew" value="" maxlength="128" onkeypress="return inCharacters(event)" />
               </td>
            </tr>
            <tr>
               <td style="width: 40%;text-align:right">
                  <span style="color:Red">*</span>&nbsp;Nombre a mostrar del Grupo
               </td>
               <td style="width: 60%">
                  <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtGroupDisplayNameNew" maxlength="128" value="" />
               </td>
            </tr>
         </table>
      </div>
   </div>
   
   <script type="text/javascript" src="ViewManager.js"></script>

</asp:Content>
