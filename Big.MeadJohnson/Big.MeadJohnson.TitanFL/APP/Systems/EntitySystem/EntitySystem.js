﻿
/****************************************************************************************
* Si PropertyType es igual a Entity entonces debe habilitar los siguientes campos:
* - PropertySourceEntityName
* - PropertySourceEntityDisplayProperty
* - PropertySourceEntitySelectionType
* Si es igual a String o LongString entonces debe habilitar los siguientes campos:
* - Length
* Si es igual a Number, Decimal o Currency entonces debe habilitar los siguientes campos:
* - Precision
* - Scale
* NOTA: Es igual la lógica para editar y crear propiedad.
****************************************************************************************/
function habilityControlsEntitys(x) {
    var mylist = document.getElementById(x);
    var valueList = mylist.options[mylist.selectedIndex].value;

    if (valueList == '12') {
        if (document.getElementById("ddlPropertySourceEntityName") != null)
            document.getElementById("ddlPropertySourceEntityName").disabled = false;

        if (document.getElementById("ddlPropertySourceEntityDisplayProperty") != null)
            document.getElementById('ddlPropertySourceEntityDisplayProperty').disabled = false;

        if (document.getElementById("ddlPropertySourceEntitySelectionType") != null)
            document.getElementById('ddlPropertySourceEntitySelectionType').disabled = false;

        if (document.getElementById("txtLength") != null) {
            document.getElementById("txtLength").disabled = true;
            document.getElementById("txtLength").value = "0";
        }

        if (document.getElementById("txtPrecision") != null) {
            document.getElementById("txtPrecision").disabled = true;
            document.getElementById("txtPrecision").value = "0";
        }

        if (document.getElementById("txtScale") != null) {
            document.getElementById("txtScale").disabled = true;
            document.getElementById("txtScale").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidator") != null) {
            document.getElementById("txtPropertyRexValidator").disabled = true;
            document.getElementById("txtPropertyRexValidator").value = "";
        }

        if (document.getElementById("chkRequired") != null)
            document.getElementById("chkRequired").disabled = false;
    }
    else if (valueList == '0' || valueList == '1' || valueList == '11' || valueList == '13') {
        if (document.getElementById("ddlPropertySourceEntityName") != null)
            document.getElementById("ddlPropertySourceEntityName").disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayProperty") != null)
            document.getElementById('ddlPropertySourceEntityDisplayProperty').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionType") != null)
            document.getElementById('ddlPropertySourceEntitySelectionType').disabled = true;

        if (document.getElementById("txtLength") != null) {
            document.getElementById("txtLength").disabled = false;
            document.getElementById("txtLength").value = "0";
        }

        if (document.getElementById("txtPrecision") != null) {
            document.getElementById("txtPrecision").disabled = true;
            document.getElementById("txtPrecision").value = "0";
        }

        if (document.getElementById("txtScale") != null) {
            document.getElementById("txtScale").disabled = true;
            document.getElementById("txtScale").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidator") != null) {
            document.getElementById("txtPropertyRexValidator").disabled = false;
            document.getElementById("txtPropertyRexValidator").value = "";
        }

        if (document.getElementById("chkRequired") != null)
            document.getElementById("chkRequired").disabled = false;
    }
    else if (valueList == '2' || valueList == '3' || valueList == '10') {
        if (document.getElementById("ddlPropertySourceEntityName") != null)
            document.getElementById("ddlPropertySourceEntityName").disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayProperty") != null)
            document.getElementById('ddlPropertySourceEntityDisplayProperty').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionType") != null)
            document.getElementById('ddlPropertySourceEntitySelectionType').disabled = true;

        if (document.getElementById("txtLength") != null) {
            document.getElementById("txtLength").disabled = false;
            document.getElementById("txtLength").value = "0";
        }

        if (document.getElementById("txtPrecision") != null) {
            document.getElementById("txtPrecision").disabled = false;
            document.getElementById("txtPrecision").value = "0";
        }

        if (document.getElementById("txtScale") != null) {
            document.getElementById("txtScale").disabled = false;
            document.getElementById("txtScale").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidator") != null) {
            document.getElementById("txtPropertyRexValidator").disabled = true;
            document.getElementById("txtPropertyRexValidator").value = "";
        }

        if (document.getElementById("chkRequired") != null)
            document.getElementById("chkRequired").disabled = false;
    }
    else if (valueList == '14') {
        if (document.getElementById("ddlPropertySourceEntityName") != null)
            document.getElementById('ddlPropertySourceEntityName').disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayProperty") != null)
            document.getElementById('ddlPropertySourceEntityDisplayProperty').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionType") != null)
            document.getElementById('ddlPropertySourceEntitySelectionType').disabled = true;

        if (document.getElementById("txtLength") != null) {
            document.getElementById("txtLength").disabled = true;
            document.getElementById("txtLength").value = "0";
        }

        if (document.getElementById("txtPrecision") != null) {
            document.getElementById("txtPrecision").disabled = true;
            document.getElementById("txtPrecision").value = "0";
        }

        if (document.getElementById("txtScale") != null) {
            document.getElementById("txtScale").disabled = true;
            document.getElementById("txtScale").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidator") != null) {
            document.getElementById("txtPropertyRexValidator").disabled = true;
            document.getElementById("txtPropertyRexValidator").value = "";
        }

        if (document.getElementById("chkRequired") != null) {
            document.getElementById("chkRequired").disabled = true;
            document.getElementById("chkRequired").checked = false;
        }
    }
    else {
        if (document.getElementById("ddlPropertySourceEntityName") != null)
            document.getElementById('ddlPropertySourceEntityName').disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayProperty") != null)
            document.getElementById('ddlPropertySourceEntityDisplayProperty').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionType") != null)
            document.getElementById('ddlPropertySourceEntitySelectionType').disabled = true;

        if (document.getElementById("txtLength") != null) {
            document.getElementById("txtLength").disabled = true;
            document.getElementById("txtLength").value = "0";
        }

        if (document.getElementById("txtPrecision") != null) {
            document.getElementById("txtPrecision").disabled = true;
            document.getElementById("txtPrecision").value = "0";
        }

        if (document.getElementById("txtScale") != null) {
            document.getElementById("txtScale").disabled = true;
            document.getElementById("txtScale").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidator") != null) {
            document.getElementById("txtPropertyRexValidator").disabled = true;
            document.getElementById("txtPropertyRexValidator").value = "";
        }

        if (document.getElementById("chkRequired") != null)
            document.getElementById("chkRequired").disabled = false;
    }
}

function habilityControlsEntitysNew(x) {
    var mylist = document.getElementById(x);
    var valueList = mylist.options[mylist.selectedIndex].value;

    if (valueList == '12') {
        if (document.getElementById("ddlPropertySourceEntityNameNew") != null)
            document.getElementById("ddlPropertySourceEntityNameNew").disabled = false;

        if (document.getElementById("ddlPropertySourceEntityDisplayPropertyNew") != null)
            document.getElementById('ddlPropertySourceEntityDisplayPropertyNew').disabled = false;

        if (document.getElementById("ddlPropertySourceEntitySelectionTypeNew") != null)
            document.getElementById('ddlPropertySourceEntitySelectionTypeNew').disabled = false;

        if (document.getElementById("txtLengthNew") != null) {
            document.getElementById("txtLengthNew").disabled = true;
            document.getElementById("txtLengthNew").value = "0";
        }

        if (document.getElementById("txtPrecisionNew") != null) {
            document.getElementById("txtPrecisionNew").disabled = true;
            document.getElementById("txtPrecisionNew").value = "0";
        }

        if (document.getElementById("txtScaleNew") != null) {
            document.getElementById("txtScaleNew").disabled = true;
            document.getElementById("txtScaleNew").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidatorNew") != null) {
            document.getElementById("txtPropertyRexValidatorNew").disabled = true;
            document.getElementById("txtPropertyRexValidatorNew").value = "";
        }

        if (document.getElementById("chkRequiredNew") != null)
            document.getElementById("chkRequiredNew").disabled = false;
    }
    else if (valueList == '0' || valueList == '1' || valueList == '11' || valueList == '13') {
        if (document.getElementById("ddlPropertySourceEntityNameNew") != null)
            document.getElementById('ddlPropertySourceEntityNameNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayPropertyNew") != null)
            document.getElementById('ddlPropertySourceEntityDisplayPropertyNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionTypeNew") != null)
            document.getElementById('ddlPropertySourceEntitySelectionTypeNew').disabled = true;

        if (document.getElementById("txtLengthNew") != null) {
            document.getElementById("txtLengthNew").disabled = false;
            document.getElementById("txtLengthNew").value = "0";
        }

        if (document.getElementById("txtPrecisionNew") != null) {
            document.getElementById("txtPrecisionNew").disabled = true;
            document.getElementById("txtPrecisionNew").value = "0";
        }

        if (document.getElementById("txtScaleNew") != null) {
            document.getElementById("txtScaleNew").disabled = true;
            document.getElementById("txtScaleNew").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidatorNew") != null) {
            document.getElementById("txtPropertyRexValidatorNew").disabled = false;
            document.getElementById("txtPropertyRexValidatorNew").value = "";
        }

        if (document.getElementById("chkRequiredNew") != null)
            document.getElementById("chkRequiredNew").disabled = false;
    }
    else if (valueList == '2' || valueList == '3' || valueList == '10') {
        if (document.getElementById("ddlPropertySourceEntityNameNew") != null)
            document.getElementById('ddlPropertySourceEntityNameNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayPropertyNew") != null)
            document.getElementById('ddlPropertySourceEntityDisplayPropertyNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionTypeNew") != null)
            document.getElementById('ddlPropertySourceEntitySelectionTypeNew').disabled = true;

        if (document.getElementById("txtLengthNew") != null) {
            document.getElementById("txtLengthNew").disabled = false;
            document.getElementById("txtLengthNew").value = "0";
        }

        if (document.getElementById("txtPrecisionNew") != null) {
            document.getElementById("txtPrecisionNew").disabled = false;
            document.getElementById("txtPrecisionNew").value = "0";
        }

        if (document.getElementById("txtScaleNew") != null) {
            document.getElementById("txtScaleNew").disabled = false;
            document.getElementById("txtScaleNew").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidatorNew") != null) {
            document.getElementById("txtPropertyRexValidatorNew").disabled = true;
            document.getElementById("txtPropertyRexValidatorNew").value = "";
        }

        if (document.getElementById("chkRequiredNew") != null)
            document.getElementById("chkRequiredNew").disabled = false;
    }
    else if (valueList == '14') {
        if (document.getElementById("ddlPropertySourceEntityNameNew") != null)
            document.getElementById('ddlPropertySourceEntityNameNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayPropertyNew") != null)
            document.getElementById('ddlPropertySourceEntityDisplayPropertyNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionTypeNew") != null)
            document.getElementById('ddlPropertySourceEntitySelectionTypeNew').disabled = true;

        if (document.getElementById("txtLengthNew") != null) {
            document.getElementById("txtLengthNew").disabled = true;
            document.getElementById("txtLengthNew").value = "0";
        }

        if (document.getElementById("txtPrecisionNew") != null) {
            document.getElementById("txtPrecisionNew").disabled = true;
            document.getElementById("txtPrecisionNew").value = "0";
        }

        if (document.getElementById("txtScaleNew") != null) {
            document.getElementById("txtScaleNew").disabled = true;
            document.getElementById("txtScaleNew").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidatorNew") != null) {
            document.getElementById("txtPropertyRexValidatorNew").disabled = true;
            document.getElementById("txtPropertyRexValidatorNew").value = "";
        }

        if (document.getElementById("chkRequiredNew") != null) {
            document.getElementById("chkRequiredNew").disabled = true;
            document.getElementById("chkRequiredNew").checked = false;
        }
    }
    else {
        if (document.getElementById("ddlPropertySourceEntityNameNew") != null)
            document.getElementById('ddlPropertySourceEntityNameNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntityDisplayPropertyNew") != null)
            document.getElementById('ddlPropertySourceEntityDisplayPropertyNew').disabled = true;

        if (document.getElementById("ddlPropertySourceEntitySelectionTypeNew") != null)
            document.getElementById('ddlPropertySourceEntitySelectionTypeNew').disabled = true;

        if (document.getElementById("txtLengthNew") != null) {
            document.getElementById("txtLengthNew").disabled = true;
            document.getElementById("txtLengthNew").value = "0";
        }

        if (document.getElementById("txtPrecisionNew") != null) {
            document.getElementById("txtPrecisionNew").disabled = true;
            document.getElementById("txtPrecisionNew").value = "0";
        }

        if (document.getElementById("txtScaleNew") != null) {
            document.getElementById("txtScaleNew").disabled = true;
            document.getElementById("txtScaleNew").value = "0";
        }

        if (document.getElementById("txtPropertyRexValidatorNew") != null) {
            document.getElementById("txtPropertyRexValidatorNew").disabled = true;
            document.getElementById("txtPropertyRexValidatorNew").value = "";
        }

        if (document.getElementById("chkRequiredNew") != null)
            document.getElementById("chkRequiredNew").disabled = false;
    }
}


/****************************************************************************************
* Namespace para los container.
****************************************************************************************/
YAHOO.namespace('OnData.TitanFL.container');


/****************************************************************************************
* Validar la entrada de caracteres.
****************************************************************************************/
function inCharacters(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    if (tecla == 8)
        return true;

    if (tecla == 0)
        return true;

    patron = /[A-Za-z0-9_]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/****************************************************************************************
* Sólo numeros
****************************************************************************************/
function onlyNumbers(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    if (tecla == 8)
        return true;

    if (tecla == 0)
        return true;

    patron = /[0-9]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/****************************************************************************************
* AJAX
****************************************************************************************/
var divListEntities = document.getElementById('divListEntities'); // Es el div en donde se pone el List de las entidades.
var divConfigEntity = document.getElementById('divConfigEntity'); // Es el div en donde se pone la configuración de una entidad.

// Si el callback es exitoso al momento de cargar las entidades en el List.
var handleSuccessEntities = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        divListEntities.innerHTML = response;

        // Si el control existe y si tiene por lo menos un elemento, entonces se trae la configuración de la
        // primera entidad.
        if (document.getElementById('lstEntities') != null && document.getElementById('lstEntities').size > 0) {
            document.getElementById('lstEntities').options[0].selected = true;
            getConfigEntity(document.getElementById('lstEntities').options[0].value);
        }

        // Aca estaba antes la de cargar los tipos de entidades para una nueva. El problema era que siempre
        // que se invocaba duplicaba la lista.

        // Presenta error. Quita de una el waiting.
        // YAHOO.OnData.TitanFL.container.wait.hide();
    }
};

// Si el callback es exitoso al momento de crear una entidad.
var handleSuccessEntitiesNew = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        divListEntities.innerHTML = response;

        for (var i = 0; i < document.getElementById('lstEntities').length; i++) {
            if (document.getElementById('lstEntities').options[i].value == o.argument) {
                document.getElementById('lstEntities').options[i].selected = true;
                getConfigEntity(document.getElementById('lstEntities').options[i].value);
            }
        }
    }
};

// Si el callback es exitoso al traer la configuración de una entidad.
var handleSuccessEntity = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        divConfigEntity.innerHTML = response;
        //YAHOO.OnData.TitanFL.container.wait.hide();


        // Llenar el DropDownList con Tipos de entidades.
        fillEntityTypeSetting();
        YAHOO.OnData.TitanFL.container.wait.style.display = "none";
    }
};

// Si el callback es exitoso al traer los atributos de una propiedad.
var handleSuccessProperty = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        YAHOO.OnData.TitanFL.container.dlgProperty.setHeader("Editar Propiedad");
        YAHOO.OnData.TitanFL.container.dlgProperty.setBody(response);
        YAHOO.OnData.TitanFL.container.dlgProperty.show();

        // Antes lo tenia de esta forma, pero la pantalla hacia un refresh muy notable. Lo quite y parece funcionar bien.
        // De todas formas va a estar en seguimiento.
        //YAHOO.OnData.TitanFL.container.dlgProperty.render(document.body);

        // Llenar el Dropdownlist con los SourceEntity. Desde el xslt ya se tiene creado el control "ddl...".
        fillPropertySourceEntityName();

        var myList = null;

        if (document.getElementById("lstPropertyType") != null) {
            myList = document.getElementById("lstPropertyType");

            if (myList.options[myList.selectedIndex].value == '12') {
                fillPropertySourceEntityDisplayPropertyEdit();
            }
        }

        //YAHOO.OnData.TitanFL.container.wait.hide();
        YAHOO.OnData.TitanFL.container.wait.style.display = "none";
    }
};

// Si el callback es exitoso al guardar los datos generales de una entidad.
var handleSuccessEntitySave = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        args1[0] = o.argument;

        makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntities", callbackEntitiesNew);
    }
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
}

var handleSuccessEntityGenerate = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        ShowError(response);
    }
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
}

// Si el callback es exitoso al guardar los datos generales de una entidad nueva.
var handleSuccessEntitySaveNew = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (response == "True") {
            args1[0] = o.argument;

            makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntities", callbackEntitiesNew);
        }
        else
            ShowError("La Entidad ya esta creada");
    }
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
}

// Si el callback es exitoso al eliminar una entidad.
var handleSuccessEntityDelete = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (response != "") {
            ShowError(response);
        }

        makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntities", callbackEntities);
    }
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
}

// Si el callback es exitoso al modificar los atributos de una propiedad.
var handleSuccessPropertyEdit = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        //ShowError("Propiedad modificada");
        getConfigEntity(document.getElementById('txtEntityName').value);
    }
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
};

// Si el callback es exitoso al guardar una propiedad nueva.
var handleSuccessPropertyNew = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        if (response == "True") {
            getConfigEntity(document.getElementById('txtEntityName').value);
        }
        else {
            ShowError("La Propiedad ya existe");
        }
    }
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
};

// Si el callback es exitoso al eliminar una propiedad.
var handleSuccessDeleteProperty = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        //ShowError("Propiedad eliminada");
        getConfigEntity(document.getElementById('txtEntityName').value);
    }
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
};

// Si el callback es exitoso al traer los SourceEntity.
// Si el xslt creo una opcion con un valor, entonces debe tomar ese valor, sacarlo de la lista,
// agregar los SourceEntity y seleccionar el valor que tenia desde el xslt. Sino solamente agrega los
// SourceEntity.
var handleSuccessPropertySourceEntityName = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        var optionSelected = "";

        // Si viene con una opcion.
        if (document.getElementById("ddlPropertySourceEntityName") != null && document.getElementById("ddlPropertySourceEntityName").length > 1) {
            optionSelected = document.getElementById("ddlPropertySourceEntityName").options[1].value;
        }

        // Si venia una valor entonces debo dejarlo como seleccionado.
        if (optionSelected != "") {
            if (document.getElementById('ddlPropertySourceEntityName') != null)
                document.getElementById('ddlPropertySourceEntityName').remove(1);

            // Lleno el DropDownList con SourceEntity.
            fillSelect(response, 'ddlPropertySourceEntityName');

            // Selecciono el que venia desde el xslt.
            if (document.getElementById('ddlPropertySourceEntityName') != null) {
                for (var i = 0; i < document.getElementById('ddlPropertySourceEntityName').length; i++) {
                    if (document.getElementById('ddlPropertySourceEntityName').options[i].value == optionSelected)
                        document.getElementById('ddlPropertySourceEntityName').options[i].selected = true;
                }
            }
        }
        // Solo agrego los elementos.
        else {
            fillSelect(response, 'ddlPropertySourceEntityName');
        }
    }
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
};

// Si el callback es exitoso al traer los SourceEntity para cuando se trata de una propiedad nueva.
var handleSuccessPropertySourceEntityNameNew = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (document.getElementById('ddlPropertySourceEntityNameNew') != null) {
            for (var i = (document.getElementById('ddlPropertySourceEntityNameNew').length - 1); i != 0; i--) {
                document.getElementById('ddlPropertySourceEntityNameNew').remove(1);
            }
        }
        fillSelect(response, 'ddlPropertySourceEntityNameNew');
    }
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
};

var handleSuccessKey = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (response == 'true')
            document.getElementById('chkUniqueNew').disabled = true;
        else
            document.getElementById('chkUniqueNew').disabled = false;
    }
};

// Si el callback es exitosos al traer los tipos de entidades.
var handleSuccessEntityType = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        var optionSelected = "";

        fillSelect(response, 'ddlEntityTypeNew');
    }
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
};

// Si el callback es exitosos al traer los tipos de entidades para la configuracion.
var handleSuccessEntityTypeSetting = function (o) {
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];
        var optionSelected = "";

        // Si viene con una opcion.
        if (document.getElementById("ddlEntityType") != null && document.getElementById("ddlEntityType").length > 1) {
            optionSelected = document.getElementById("ddlEntityType").options[1].value;
        }

        // Si venia una valor entonces debo dejarlo como seleccionado.
        if (optionSelected != "") {
            if (document.getElementById('ddlEntityType') != null)
                document.getElementById('ddlEntityType').remove(1);

            // Lleno el DropDownList con SourceEntity.
            fillSelect(response, 'ddlEntityType');

            // Selecciono el que venia desde el xslt.
            if (document.getElementById('ddlEntityType') != null) {
                for (var i = 0; i < document.getElementById('ddlEntityType').length; i++) {
                    if (document.getElementById('ddlEntityType').options[i].value == optionSelected)
                        document.getElementById('ddlEntityType').options[i].selected = true;
                }
            }
        }
        // Solo agrego los elementos.
        else {
            fillSelect(response, 'ddlEntityType');
        }
    }
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
};

var handleSuccessPropertySourceEntityDisplayProperty = function (o) {
    var control = "";

    if (o.argument == "ddlPropertySourceEntityNameNew")
        control = "ddlPropertySourceEntityDisplayPropertyNew";
    else {
        if (o.argument == "ddlPropertySourceEntityName")
            control = "ddlPropertySourceEntityDisplayProperty";
    }

    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (document.getElementById(control) != null) {
            cleanSelected(control);

            if (response != "")
                fillSelect(response, control);
        }
    }

    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
}

var handleSuccessPropertySourceEntityDisplayPropertyEdit = function (o) {
    var control = "ddlPropertySourceEntityDisplayProperty";

    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (document.getElementById(control) != null) {
            var optionSelected = "";
            optionSelected = document.getElementById(control).options[1].value;
            document.getElementById(control).remove(1);

            // lleno el dropdownlist con sourceentity.
            if (response != "") {
                //ShowError(response);
                selectionProperty(response, 'ddlPropertySourceEntityDisplayProperty');
            }
            //

            // selecciono el que venia desde el xslt.
            for (var i = 0; i < document.getElementById(control).length; i++) {
                if (document.getElementById(control).options[i].value == optionSelected)
                    document.getElementById(control).options[i].selected = true;

            }
        }
    }

    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
}

function selectionProperty(stringValues, idControl) {
    var optionsList = stringValues.split(",");
    var elSel = null;
    var item = null;

    if (document.getElementById(idControl) != null)
        elSel = document.getElementById(idControl);

    if (elSel != null) {
        for (var i = 0; i < optionsList.length; i++) {
            item = optionsList[i].split("|");
            var elOptNew = document.createElement('option');
            elOptNew.value = item[0];
            elOptNew.text = item[1];

            try {
                elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
            }
            catch (ex) {
                elSel.add(elOptNew); // IE only
            }
        }
    }
}

/* Version 01
function selectionProperty(stringValues, idControl)
{
var optionsList = stringValues.split(",");
var elSel = null;
   
   
if ( document.getElementById(idControl) != null )
elSel = document.getElementById(idControl);
   
if ( elSel != null )
{
for (var i=0; i < optionsList.length; i++) 
{
var elOptNew = document.createElement('option');
elOptNew.text = optionsList[i];
elOptNew.value = optionsList[i];
         
try {
elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
}
catch(ex) {
elSel.add(elOptNew); // IE only
}
}
}
}*/

// Llenar un DropDownList con SourceEntity.
// stringValues: Son los valores que llegan del lado del servidor. Deben estar separados por coma.
function fillSelect(stringValues, idControl) {
    var optionsList = stringValues.split(",");
    var elSel = null;
    var item = null;

    if (document.getElementById(idControl) != null)
        elSel = document.getElementById(idControl);

    if (elSel != null) {
        for (var i = 0; i < optionsList.length; i++) {
            item = optionsList[i].split("|");
            var elOptNew = document.createElement('option');
            elOptNew.value = item[0];
            elOptNew.text = item[1];

            try {
                elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
            }
            catch (ex) {
                elSel.add(elOptNew); // IE only
            }
        }
    }
}

/* Version 01
function fillSelect(stringValues, idControl)
{
var optionsList = stringValues.split(",");
var elSel = null;
   
if ( document.getElementById(idControl) != null )
elSel = document.getElementById(idControl);
   
if ( elSel != null )
{
for (var i=0; i < optionsList.length; i++) 
{
var elOptNew = document.createElement('option');
elOptNew.text = optionsList[i];
elOptNew.value = optionsList[i];
         
try {
elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
}
catch(ex) {
elSel.add(elOptNew); // IE only
}
}
}
}*/

function cleanSelected(idControl) {
    for (var i = document.getElementById(idControl).length - 1; i > 0; i--)
        document.getElementById(idControl).remove(i);
}

// Si el callback falla. Puede fallar en los siguiente casos:
// - Al cargar las entidades en el List.
// - Al cargar la configuración de una entidad.
// - Al cargar los atributos de una propiedad.
// - Al modificar los atributos de una propiedad.
// - Al guardar los datos generales de una entidad.
// - Al guardar los datos generales de una entidad nueva.
// - Al eliminar una entidad.
// - Al cargar SourceEntity en un DropDownList.
var handleFailure = function (o) {
    if (o.responseText !== undefined) {
        var divErrorAJAX = document.getElementById('divErrorAJAX');
        var divContentAJAX = document.getElementById('divContentAJAX');
        divErrorAJAX.innerHTML = "<strong>Error!</strong>";
        divContentAJAX.innerHTML += "<li>Id de la transacción: " + o.tId + "</li>";
        divContentAJAX.innerHTML += "<li>Estado HTTP: " + o.status + "</li>";
        divContentAJAX.innerHTML += "<li>Código del mensaje de estado: " + o.statusText + "</li>";
        //YAHOO.OnData.TitanFL.container.wait.hide();

        YAHOO.OnData.TitanFL.container.wait.style.display = "none";
    }
};

// Callback para traer las entidades.
var callbackEntities =
{
    success: handleSuccessEntities,
    failure: handleFailure
};

var args1 = [''];

// Callback para traer las entidades cuando es nueva.
var callbackEntitiesNew =
{
    success: handleSuccessEntitiesNew,
    failure: handleFailure,
    argument: args1
};

// Callback para traer la configuración de una entidad.
var callbackEntity =
{
    success: handleSuccessEntity,
    failure: handleFailure
};

// Callback para editar los atributos de una propiedad.
var callbackEditProperty =
{
    success: handleSuccessProperty,
    failure: handleFailure
};

// Callback para eliminar una propiedad.
var callbackDeleteProperty =
{
    success: handleSuccessDeleteProperty,
    failure: handleFailure
};

// Callback para modificar los atributos de una propiedad.
var callbackSaveProperty =
{
    success: handleSuccessPropertyEdit,
    failure: handleFailure
};

// Callback para guardar una propiedad.
var callbackSavePropertyNew =
{
    success: handleSuccessPropertyNew,
    failure: handleFailure
};

// Callback para guardar los datos generales de una entidad.
var args3 = [''];
var callbackSaveEntity =
{
    success: handleSuccessEntitySave,
    failure: handleFailure,
    argument: args3
}

var callbackGenerateEntity =
{
    success: handleSuccessEntityGenerate,
    failure: handleFailure
}

// Callback para guardar los datos generales de una entidad nueva.
var args2 = [''];
var callbackSaveEntityNew =
{
    success: handleSuccessEntitySaveNew,
    failure: handleFailure,
    argument: args2
}

// Callback para eliminar una entidad.
var callbackDeleteEntity =
{
    success: handleSuccessEntityDelete,
    failure: handleFailure
}

// Callback para cargar SourceEntity.
var callbackPropertySourceEntityName =
{
    success: handleSuccessPropertySourceEntityName,
    failure: handleFailure
}

// Callback para cargar SourceEntity cuando es una nueva propiedad.
var callbackPropertySourceEntityNameNew =
{
    success: handleSuccessPropertySourceEntityNameNew,
    failure: handleFailure
}

// Callback para cargar los tipos de entidades.
var callbackEntityType =
{
    success: handleSuccessEntityType,
    failure: handleFailure
}

// Callback para cargar los tipos de entidades para la configuración.
var callbackEntityTypeSetting =
{
    success: handleSuccessEntityTypeSetting,
    failure: handleFailure
}

var args = [''];

// Callback para cargar las propiedades de una entidad.
var callbackPropertySourceEntityDisplayProperty =
{
    success: handleSuccessPropertySourceEntityDisplayProperty,
    failure: handleFailure,
    argument: args
}

var callbackPropertySourceEntityDisplayPropertyEdit =
{
    success: handleSuccessPropertySourceEntityDisplayPropertyEdit,
    failure: handleFailure
}

var callbackKey =
{
    success: handleSuccessKey,
    failure: handleFailure
}

// Para cualquier solicitud a través de AJAX
// sUrl: Es la pagina aspx.
// postData: Los parametros que se envian.
// callbakc: El callback que tiene cuando es exitoso o fallido.
function makeRequest(sUrl, postData, callback) {
    //waitPanel();
    var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);
}

// Trae la configuración de una entidad.
function getConfigEntity(entity) {
    document.getElementById('divContentAJAX').innerHTML = "";
    document.getElementById('divErrorAJAX').innerHTML = "";
    YAHOO.OnData.TitanFL.container.wait.style.display = "block";
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntityConfig&Entity=" + entity, callbackEntity);
}

// Edita los atributos de una propiedad.
function editProperty(property, entity) {
    //YAHOO.OnData.TitanFL.container.wait.show();
    YAHOO.OnData.TitanFL.container.wait.style.display = "block";
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEditProperty&Entity=" + entity + "&Property=" + property, callbackEditProperty);
}

// Guardar los atributos de una propiedad. Al editar.
function saveProperty() {
    var vlrEntityName = "";
    var vlrPropertyName = "";
    var vlrPropertyDisplayName = "";
    var vlrPropertyRexValidator = "";
    var vlrPropertyDisplayFormat = "";
    var vlrPropertyState = "";
    var vlrPropertyIndexed = "";
    var vlrPropertyTimeStamEnable = "";
    var vlrPropertySearchable = "";
    var vlrPropertyEncrypt = "";
    var vlrPropertyType = "";
    var vlrPropertySourceEntityName = "";
    var vlrPropertySourceEntityDisplayProperty = "";
    var vlrPropertySourceEntitySelectionType = "";
    var vlrLength = "";
    var vlrPrecision = "";
    var vlrScale = "";
    var vlrToolTipText = "";
    var vlrRequired = "";
    var vlrUnique = "";

    var parameters = "";
    var mylist = null;

    if (document.getElementById("lstPropertyType") != null) {
        mylist = document.getElementById("lstPropertyType");

        if (mylist.options[mylist.selectedIndex].value != '' && mylist.options[mylist.selectedIndex].value != "Seleccione...") {
            vlrPropertyType = "&PropertyType=" + mylist.options[mylist.selectedIndex].value;

            if (mylist.options[mylist.selectedIndex].value == "12") {
                var myListSEN = null;
                var myListSED = null;
                var myListSES = null;

                if (document.getElementById("ddlPropertySourceEntityName") != null) {
                    myListSEN = document.getElementById("ddlPropertySourceEntityName");

                    if (myListSEN.options[myListSEN.selectedIndex].value != '' && myListSEN.options[myListSEN.selectedIndex].value != "Seleccione...")
                        vlrPropertySourceEntityName = "&PropertySourceEntityName=" + myListSEN.options[myListSEN.selectedIndex].value;
                }

                if (document.getElementById("ddlPropertySourceEntityDisplayProperty") != null) {
                    myListSED = document.getElementById("ddlPropertySourceEntityDisplayProperty");

                    if (myListSED.options[myListSED.selectedIndex].value != '' && myListSED.options[myListSED.selectedIndex].value != "Seleccione...")
                        vlrPropertySourceEntityDisplayProperty = "&PropertySourceEntityDisplayProperty=" + myListSED.options[myListSED.selectedIndex].value;
                }

                if (document.getElementById("ddlPropertySourceEntitySelectionType") != null) {
                    myListSES = document.getElementById("ddlPropertySourceEntitySelectionType");

                    if (myListSES.options[myListSES.selectedIndex].value != '' && myListSES.options[myListSES.selectedIndex].value != "Seleccione...")
                        vlrPropertySourceEntitySelectionType = "&PropertySourceEntitySelectionType=" + myListSES.options[myListSES.selectedIndex].value;
                }
            }
        }
    }

    if (document.getElementById("txtEntityName") != null && document.getElementById("txtEntityName").value != '')
        vlrEntityName = "&EntityName=" + document.getElementById("txtEntityName").value;

    if (document.getElementById("txtPropertyName") != null && document.getElementById("txtPropertyName").value != '')
        vlrPropertyName = "&PropertyName=" + document.getElementById("txtPropertyName").value;

    if (document.getElementById("txtPropertyDisplayName") != null && document.getElementById("txtPropertyDisplayName").value != '')
        vlrPropertyDisplayName = "&PropertyDisplayName=" + document.getElementById("txtPropertyDisplayName").value;

    if (document.getElementById("txtPropertyRexValidator") != null && document.getElementById("txtPropertyRexValidator").value != '')
        vlrPropertyRexValidator = "&PropertyRexValidator=" + document.getElementById("txtPropertyRexValidator").value;

    if (document.getElementById("txtPropertyDisplayFormat") != null && document.getElementById("txtPropertyDisplayFormat").value != '')
        vlrPropertyDisplayFormat = "&PropertyDisplayFormat=" + document.getElementById("txtPropertyDisplayFormat").value;

    if (document.getElementById("chkRequired") != null) {
        if (document.getElementById("chkRequired").checked)
            vlrRequired = "&Required=true";
        else
            vlrRequired = "&Required=false";
    }

    if (document.getElementById("chkUnique") != null) {
        if (document.getElementById("chkUnique").checked)
            vlrUnique = "&Unique=true";
        else
            vlrUnique = "&Unique=false";
    }

    if (document.getElementById("chkPropertyState") != null) {
        if (document.getElementById("chkPropertyState").checked)
            vlrPropertyState = "&PropertyState=true";
        else
            vlrPropertyState = "&PropertyState=false";
    }

    if (document.getElementById("chkPropertyIndexed") != null) {
        if (document.getElementById("chkPropertyIndexed").checked)
            vlrPropertyIndexed = "&PropertyIndexed=true";
        else
            vlrPropertyIndexed = "&PropertyIndexed=false";
    }

    if (document.getElementById("chkPropertyTimeStamEnable") != null) {
        if (document.getElementById("chkPropertyTimeStamEnable").checked)
            vlrPropertyTimeStamEnable = "&PropertyTimeStamEnable=true";
        else
            vlrPropertyTimeStamEnable = "&PropertyTimeStamEnable=false";
    }

    if (document.getElementById("chkPropertySearchable") != null) {
        if (document.getElementById("chkPropertySearchable").checked)
            vlrPropertySearchable = "&PropertySearchable=true";
        else
            vlrPropertySearchable = "&PropertySearchable=false";
    }

    if (document.getElementById("chkPropertyEncrypt") != null) {
        if (document.getElementById("chkPropertyEncrypt").checked)
            vlrPropertyEncrypt = "&PropertyEncrypt=true";
        else
            vlrPropertyEncrypt = "&PropertyEncrypt=false";
    }

    //if ( document.getElementById("txtLength") != null && document.getElementById("txtLength").value != '' )
    if (document.getElementById("txtLength") != null) {
        if (isNaN(document.getElementById("txtLength").value)) {
            ShowError("La longitud debe ser un número");
            return;
        }
        else if (mylist.options[mylist.selectedIndex].value == "0") {
            if (document.getElementById("txtLength").value <= 0 || document.getElementById("txtLength").value > 50) {
                ShowError("Cuando es de tipo cadena de caracteres, la longitud debe estar entre 1 y 50");
                return;
            }
        }
        else if (mylist.options[mylist.selectedIndex].value == "1") {
            if (document.getElementById("txtLength").value <= 50) {
                ShowError("Cuando es de tipo cadena larga de caracteres, la longitud debe ser mayor de 50");
                return;
            }
            else if (document.getElementById("txtLength").value > 4000) {
                ShowError("Cuando es de tipo cadena larga de caracteres, la longitud debe ser menor o igual a 4000");
                return;
            }
        }
        else if (mylist.options[mylist.selectedIndex].value == "11") {
            if (document.getElementById("txtLength").value == 0 || document.getElementById("txtLength").value > 4000) {
                ShowError("Cuando la propiedad es de tipo contraseña, la longitud debe estar entre 1 y 4000");
                return;
            }
        }
        else if (mylist.options[mylist.selectedIndex].value == "2" || mylist.options[mylist.selectedIndex].value == "3" || mylist.options[mylist.selectedIndex].value == "10" || mylist.options[mylist.selectedIndex].value == "13") {
            if (document.getElementById("txtLength").value <= 0) {
                ShowError("La longitud debe ser mayor a 1");
                return;
            }
        }
        else if (document.getElementById("txtLength").value == '') {
            ShowError("Ingrese una longitud");
            return;
        }
        vlrLength = "&Length=" + document.getElementById("txtLength").value;
    }

    if (document.getElementById("txtPrecision") != null && document.getElementById("txtPrecision").value != '') {
        if (isNaN(document.getElementById("txtPrecision").value)) {
            ShowError("La precisión debe ser un número");
            return;
        }
        else if (document.getElementById("txtPrecision").value > 38) {
            ShowError("La precisión debe ser menor o igual a 38");
            return;
        }
        else
            vlrPrecision = "&Precision=" + document.getElementById("txtPrecision").value;
    }

    if (document.getElementById("txtScale") != null && document.getElementById("txtScale").value != '') {
        if (isNaN(document.getElementById("txtScale").value)) {
            ShowError("La escala debe ser un número");
            return;
        }
        else if (document.getElementById("txtScale").value > 18) {
            ShowError("La escala debe ser menor o igual a 18");
            return;
        }
        else
            vlrScale = "&Scala=" + document.getElementById("txtScale").value;
    }

    if (document.getElementById("txtToolTipText") != null && document.getElementById("txtToolTipText").value != '')
        vlrToolTipText = "&ToolTipText=" + document.getElementById("txtToolTipText").value;

    if (vlrPropertyType == "&PropertyType=12") {
        if (vlrPropertySourceEntityName == '') {
            ShowError("Seleccione el nombre de la Entidad fuente");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            return 0;
        }

        if (vlrPropertySourceEntityDisplayProperty == '') {
            ShowError("Seleccione la propiedad a mostrar de la Entidad");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            return 0;
        }

        if (vlrPropertySourceEntitySelectionType == '') {
            ShowError("Seleccione el tipo de selección");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            return 0;
        }
    }

    parameters = vlrEntityName + vlrPropertyName + vlrPropertyDisplayName + vlrPropertyRexValidator + vlrPropertyDisplayFormat + vlrPropertyState + vlrPropertyIndexed + vlrPropertyTimeStamEnable + vlrPropertySearchable + vlrPropertyType + vlrPropertySourceEntityName + vlrPropertySourceEntityDisplayProperty + vlrPropertySourceEntitySelectionType + vlrLength + vlrPrecision + vlrScale + vlrToolTipText + vlrRequired + vlrUnique + vlrPropertyEncrypt;

    //YAHOO.OnData.TitanFL.container.wait.show();
    YAHOO.OnData.TitanFL.container.wait.style.display = "block";
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLSaveProperty" + parameters, callbackSaveProperty);
    return 1;
}

// Guardar los atributos de una propiedad nueva.
// Esta llamando la misma funcion del lado del servidor a la que tiene saveProperty. Lo que puedo agregar para que distinga en que una
// es para editar y la otra para agregar es enviando otro parametro.
function savePropertyNew() {
    var vlrEntityName = "";
    var vlrPropertyName = "";
    var vlrPropertyDisplayName = "";
    var vlrPropertyRexValidator = "";
    var vlrPropertyDisplayFormat = "";
    var vlrPropertyState = "";
    var vlrPropertyIndexed = "";
    var vlrPropertyTimeStamEnable = "";
    var vlrPropertySearchable = "";
    var vlrPropertyEncrypt = "";
    var vlrPropertyType = "";
    var vlrPropertySourceEntityName = "";
    var vlrPropertySourceEntityDisplayProperty = "";
    var vlrPropertySourceEntitySelectionType = "";
    var vlrLength = "";
    var vlrPrecision = "";
    var vlrScale = "";
    var vlrToolTipText = "";
    var vlrRequired = "";
    var vlrUnique = "";

    var parameters = "";
    var mylist = null;

    if (document.getElementById("lstPropertyTypeNew") != null) {
        mylist = document.getElementById("lstPropertyTypeNew");

        if (mylist.options[mylist.selectedIndex].value != '' && mylist.options[mylist.selectedIndex].value != "Seleccione...") {
            vlrPropertyType = "&PropertyType=" + mylist.options[mylist.selectedIndex].value;

            if (mylist.options[mylist.selectedIndex].value == "12") {
                var myListSEN = null;
                var myListSED = null;
                var myListSES = null;

                if (document.getElementById("ddlPropertySourceEntityNameNew") != null) {
                    myListSEN = document.getElementById("ddlPropertySourceEntityNameNew");

                    if (myListSEN.options[myListSEN.selectedIndex].value != '' && myListSEN.options[myListSEN.selectedIndex].value != "Seleccione...")
                        vlrPropertySourceEntityName = "&PropertySourceEntityName=" + myListSEN.options[myListSEN.selectedIndex].value;
                }

                if (document.getElementById("ddlPropertySourceEntityDisplayPropertyNew") != null) {
                    myListSED = document.getElementById("ddlPropertySourceEntityDisplayPropertyNew");

                    if (myListSED.options[myListSED.selectedIndex].value != '' && myListSED.options[myListSED.selectedIndex].value != "Seleccione...")
                        vlrPropertySourceEntityDisplayProperty = "&PropertySourceEntityDisplayProperty=" + myListSED.options[myListSED.selectedIndex].value;
                }

                if (document.getElementById("ddlPropertySourceEntitySelectionTypeNew") != null) {
                    myListSES = document.getElementById("ddlPropertySourceEntitySelectionTypeNew");

                    if (myListSES.options[myListSES.selectedIndex].value != '' && myListSES.options[myListSES.selectedIndex].value != "Seleccione...")
                        vlrPropertySourceEntitySelectionType = "&PropertySourceEntitySelectionType=" + myListSES.options[myListSES.selectedIndex].value;
                }
            }
        }
    }

    if (document.getElementById("txtEntityName") != null && document.getElementById("txtEntityName").value != '')
        vlrEntityName = "&EntityName=" + document.getElementById("txtEntityName").value;

    if (document.getElementById("txtPropertyNameNew") != null && document.getElementById("txtPropertyNameNew").value != '')
        vlrPropertyName = "&PropertyName=" + document.getElementById("txtPropertyNameNew").value;

    if (document.getElementById("txtPropertyDisplayNameNew") != null && document.getElementById("txtPropertyDisplayNameNew").value != '')
        vlrPropertyDisplayName = "&PropertyDisplayName=" + document.getElementById("txtPropertyDisplayNameNew").value;

    if (document.getElementById("txtPropertyRexValidatorNew") != null && document.getElementById("txtPropertyRexValidatorNew").value != '')
        vlrPropertyRexValidator = "&PropertyRexValidator=" + document.getElementById("txtPropertyRexValidatorNew").value;

    if (document.getElementById("txtPropertyDisplayFormatNew") != null && document.getElementById("txtPropertyDisplayFormatNew").value != '')
        vlrPropertyDisplayFormat = "&PropertyDisplayFormat=" + document.getElementById("txtPropertyDisplayFormatNew").value;

    if (document.getElementById("chkRequiredNew") != null) {
        if (document.getElementById("chkRequiredNew").checked)
            vlrRequired = "&Required=true";
        else
            vlrRequired = "&Required=false";
    }

    if (document.getElementById("chkUniqueNew") != null) {
        if (document.getElementById("chkUniqueNew").checked)
            vlrUnique = "&Unique=true";
        else
            vlrUnique = "&Unique=false";
    }

    if (document.getElementById("chkPropertyStateNew") != null) {
        if (document.getElementById("chkPropertyStateNew").checked)
            vlrPropertyState = "&PropertyState=true";
        else
            vlrPropertyState = "&PropertyState=false";
    }

    if (document.getElementById("chkPropertyIndexedNew") != null) {
        if (document.getElementById("chkPropertyIndexedNew").checked)
            vlrPropertyIndexed = "&PropertyIndexed=true";
        else
            vlrPropertyIndexed = "&PropertyIndexed=false";
    }

    if (document.getElementById("chkPropertyTimeStamEnableNew") != null) {
        if (document.getElementById("chkPropertyTimeStamEnableNew").checked)
            vlrPropertyTimeStamEnable = "&PropertyTimeStamEnable=true";
        else
            vlrPropertyTimeStamEnable = "&PropertyTimeStamEnable=false";
    }

    if (document.getElementById("chkPropertySearchableNew") != null) {
        if (document.getElementById("chkPropertySearchableNew").checked)
            vlrPropertySearchable = "&PropertySearchable=true";
        else
            vlrPropertySearchable = "&PropertySearchable=false";
    }

    if (document.getElementById("chkPropertyEncryptNew") != null) {
        if (document.getElementById("chkPropertyEncryptNew").checked)
            vlrPropertyEncrypt = "&PropertyEncrypt=true";
        else
            vlrPropertyEncrypt = "&PropertyEncrypt=false";
    }

    //if ( document.getElementById("txtLengthNew") != null && document.getElementById("txtLengthNew").value != '' )
    if (document.getElementById("txtLengthNew") != null) {
        if (isNaN(document.getElementById("txtLengthNew").value)) {
            ShowError("La longitud debe ser un número");
            return;
        }
        else if (mylist.options[mylist.selectedIndex].value == "0") {
            if (document.getElementById("txtLengthNew").value <= 0 || document.getElementById("txtLengthNew").value > 50) {
                ShowError("Cuando es de tipo cadena de caracteres, la longitud debe estar entre 1 y 50");
                return;
            }
        }
        else if (mylist.options[mylist.selectedIndex].value == "1") {
            if (document.getElementById("txtLengthNew").value <= 50) {
                ShowError("Cuando la propiedad es de tipo cadena larga de caracteres, la longitud debe ser mayor de 50");
                return;
            }
            else if (document.getElementById("txtLengthNew").value > 4000) {
                ShowError("Cuando es de tipo cadena larga de caracteres, la longitud debe ser menor o igual a 4000");
                return;
            }
        }
        else if (mylist.options[mylist.selectedIndex].value == "11") {
            if (document.getElementById("txtLengthNew").value == 0 || document.getElementById("txtLengthNew").value > 4000) {
                ShowError("Cuando la propiedad es de tipo contraseña, la longitud debe estar entre 1 y 4000");
                return;
            }
        }
        else if (mylist.options[mylist.selectedIndex].value == "2" || mylist.options[mylist.selectedIndex].value == "3" || mylist.options[mylist.selectedIndex].value == "10" || mylist.options[mylist.selectedIndex].value == "13") {
            if (document.getElementById("txtLengthNew").value <= 0) {
                ShowError("La longitud debe ser mayor a 1");
                return;
            }
        }
        vlrLength = "&Length=" + document.getElementById("txtLengthNew").value;
    }

    if (document.getElementById("txtPrecisionNew") != null && document.getElementById("txtPrecisionNew").value != '') {
        if (isNaN(document.getElementById("txtPrecisionNew").value)) {
            ShowError("La precisión debe ser un número");
            return;
        }
        else if (document.getElementById("txtPrecisionNew").value > 38) {
            ShowError("La precisión debe ser menor o igual a 38");
            return;
        }
        else
            vlrPrecision = "&Precision=" + document.getElementById("txtPrecisionNew").value;
    }

    if (document.getElementById("txtScaleNew") != null && document.getElementById("txtScaleNew").value != '') {
        if (isNaN(document.getElementById("txtScaleNew").value)) {
            ShowError("La escala debe ser un número");
            return;
        }
        else if (document.getElementById("txtScaleNew").value > 18) {
            ShowError("La escala debe ser menor o igual a 18");
            return;
        }
        else
            vlrScale = "&Scala=" + document.getElementById("txtScaleNew").value;
    }

    if (document.getElementById("txtToolTipTextNew") != null && document.getElementById("txtToolTipTextNew").value != '')
        vlrToolTipText = "&ToolTipText=" + document.getElementById("txtToolTipTextNew").value;

    if (vlrPropertyName == '') {
        ShowError("Ingrese el nombre de la propiedad");
        return 0;
    }

    if (vlrPropertyDisplayName == '') {
        ShowError("Ingrese el nombre a mostrar de la propiedad");
        return 0;
    }

    if (vlrPropertyType == '') {
        ShowError("Seleccione el tipo de la propiedad");
        return 0;
    }

    if (vlrPropertyType == "&PropertyType=12") {
        if (vlrPropertySourceEntityName == '') {
            ShowError("Seleccione el nombre de la Entidad fuente");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            return 0;
        }

        if (vlrPropertySourceEntityDisplayProperty == '') {
            ShowError("Seleccione la propiedad a mostrar de la Entidad");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            return 0;
        }

        if (vlrPropertySourceEntitySelectionType == '') {
            ShowError("Seleccione el tipo de selección");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            return 0;
        }
    }

    parameters = vlrEntityName + vlrPropertyName + vlrPropertyDisplayName + vlrPropertyRexValidator + vlrPropertyDisplayFormat + vlrPropertyState + vlrPropertyIndexed + vlrPropertyTimeStamEnable + vlrPropertySearchable + vlrPropertyType + vlrPropertySourceEntityName + vlrPropertySourceEntityDisplayProperty + vlrPropertySourceEntitySelectionType + vlrLength + vlrPrecision + vlrScale + vlrToolTipText + vlrRequired + vlrUnique + vlrPropertyEncrypt;

    YAHOO.OnData.TitanFL.container.wait.style.display = "block";
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLSavePropertyNew" + parameters, callbackSavePropertyNew);
    return 1;
}

// Limpia los campos para crear una nueva propiedad.
function cleanFieldsPropertyNew() {
    if (document.getElementById("txtPropertyNameNew") != null)
        document.getElementById("txtPropertyNameNew").value = "";

    if (document.getElementById("txtPropertyDisplayNameNew") != null)
        document.getElementById("txtPropertyDisplayNameNew").value = "";

    if (document.getElementById("txtPropertyRexValidatorNew") != null) {
        document.getElementById("txtPropertyRexValidatorNew").value = "";
        document.getElementById("txtPropertyRexValidatorNew").disabled = true;
    }

    if (document.getElementById("txtPropertyDisplayFormatNew") != null)
        document.getElementById("txtPropertyDisplayFormatNew").value = "";

    if (document.getElementById("chkRequiredNew") != null) {
        document.getElementById("chkRequiredNew").checked = false;
        document.getElementById("chkRequiredNew").disabled = false;
    }

    if (document.getElementById("chkUniqueNew") != null)
        document.getElementById("chkUniqueNew").checked = false;

    if (document.getElementById("chkPropertyStateNew") != null)
        document.getElementById("chkPropertyStateNew").checked = false;

    if (document.getElementById("chkPropertyIndexedNew") != null)
        document.getElementById("chkPropertyIndexedNew").checked = false;

    if (document.getElementById("chkPropertyTimeStamEnableNew") != null)
        document.getElementById("chkPropertyTimeStamEnableNew").checked = false;

    if (document.getElementById("chkPropertySearchableNew") != null)
        document.getElementById("chkPropertySearchableNew").checked = false;

    if (document.getElementById("chkPropertyEncryptNew") != null)
        document.getElementById("chkPropertyEncryptNew").checked = false;

    if (document.getElementById("lstPropertyTypeNew") != null)
        document.getElementById("lstPropertyTypeNew").options[0].selected = true;

    if (document.getElementById("ddlPropertySourceEntityNameNew") != null) {
        document.getElementById("ddlPropertySourceEntityNameNew").options[0].selected = true;
        document.getElementById("ddlPropertySourceEntityNameNew").disabled = true;
    }

    if (document.getElementById("ddlPropertySourceEntityDisplayPropertyNew") != null) {
        document.getElementById("ddlPropertySourceEntityDisplayPropertyNew").options[0].selected = true;
        document.getElementById("ddlPropertySourceEntityDisplayPropertyNew").disabled = true;
    }

    if (document.getElementById("ddlPropertySourceEntitySelectionTypeNew") != null) {
        document.getElementById("ddlPropertySourceEntitySelectionTypeNew").options[0].selected = true;
        document.getElementById("ddlPropertySourceEntitySelectionTypeNew").disabled = true;
    }

    if (document.getElementById("txtLengthNew") != null) {
        document.getElementById("txtLengthNew").value = "";
        document.getElementById("txtLengthNew").disabled = true;
    }

    if (document.getElementById("txtPrecisionNew") != null) {
        document.getElementById("txtPrecisionNew").value = "";
        document.getElementById("txtPrecisionNew").disabled = true;
    }

    if (document.getElementById("txtScaleNew") != null) {
        document.getElementById("txtScaleNew").value = "";
        document.getElementById("txtScaleNew").disabled = true;
    }

    if (document.getElementById("txtToolTipTextNew") != null)
        document.getElementById("txtToolTipTextNew").value = "";
}

// Limpia los campos para crear una nueva entidad.
function cleanFieldsEntityNew() {
    if (document.getElementById("txtEntityNameNew") != null)
        document.getElementById("txtEntityNameNew").value = "";

    if (document.getElementById("txtEntityDisplayNameNew") != null)
        document.getElementById("txtEntityDisplayNameNew").value = "";

    if (document.getElementById("ddlEntityTypeNew") != null)
        document.getElementById("ddlEntityTypeNew").options[0].selected = true;
}

// Elimina una propiedad.
function deleteProperty(property, entity) {
    if (confirm("¿Realmente desea eliminar la propiedad?", "Si", "No")) {
        //YAHOO.OnData.TitanFL.container.wait.show();
        YAHOO.OnData.TitanFL.container.wait.style.display = "block";
        makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLDeleteProperty&Entity=" + entity + "&Property=" + property, callbackDeleteProperty);
    }
}

// Crear una propiedad.
function newProperty() {
    cleanFieldsPropertyNew();
    YAHOO.OnData.TitanFL.container.dlgPropertyNew.setHeader("Crear Propiedad");
    YAHOO.OnData.TitanFL.container.dlgPropertyNew.show();
    //YAHOO.OnData.TitanFL.container.dlgPropertyNew.render();
    //YAHOO.OnData.TitanFL.container.wait.hide();
    fillPropertySourceEntityNameNew();
    validateKey();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";
}

function validateKey() {
    var entity = "";

    if (document.getElementById("txtEntityName") != null && document.getElementById("txtEntityName").value != '')
        entity = document.getElementById("txtEntityName").value;

    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLKey&Entity=" + entity, callbackKey);
}

// Guardar los datos generales de una entidad.
function saveEntity() {
    if (document.getElementById("lstEntities").value != "" && document.getElementById("lstEntities").value != null) {
        //YAHOO.OnData.TitanFL.container.wait.show();
        YAHOO.OnData.TitanFL.container.wait.style.display = "block";
        var mylist = null;

        if (document.getElementById("ddlEntityType") != null)
            mylist = document.getElementById("ddlEntityType");

        var vlrEntityName = "";
        var vlrEntityDisplayName = "";
        var vlrEntityType = "";
        var vlrEntityDescription = "";
        var vlrEntityReadOnly = "";
        var vlrEntityHidden = "";

        if (document.getElementById("txtEntityName") != null && document.getElementById("txtEntityName").value != '')
            vlrEntityName = "&EntityName=" + document.getElementById("txtEntityName").value;

        if (document.getElementById("txtEntityDisplayName") != null && document.getElementById("txtEntityDisplayName").value != '')
            vlrEntityDisplayName = "&EntityDisplayName=" + document.getElementById("txtEntityDisplayName").value;

        if (mylist != null && mylist.options[mylist.selectedIndex].value != '' && mylist.options[mylist.selectedIndex].value != 'Seleccione...')
            vlrEntityType = "&EntityType=" + mylist.options[mylist.selectedIndex].value;

        if (document.getElementById("txtEntityDescription") != null && document.getElementById("txtEntityDescription").value != '')
            vlrEntityDescription = "&EntityDescription=" + document.getElementById("txtEntityDescription").value;

        if (document.getElementById("chkEntityReadOnly") != null) {
            if (document.getElementById("chkEntityReadOnly").checked)
                vlrEntityReadOnly = "&EntityReadOnly=true";
            else
                vlrEntityReadOnly = "&EntityReadOnly=false";
        }

        if (document.getElementById("chkEntityHidden") != null) {
            if (document.getElementById("chkEntityHidden").checked)
                vlrEntityHidden = "&EntityHidden=true";
            else
                vlrEntityHidden = "&EntityHidden=false";
        }

        if (vlrEntityDisplayName == '') {
            ShowError("Ingrese el nombre a mostrar de la Entidad");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            YAHOO.OnData.TitanFL.container.wait.style.display = "none";
            return;
        }

        if (vlrEntityType == '') {
            ShowError("Debe seleccionar un tipo de Entidad");
            //YAHOO.OnData.TitanFL.container.wait.hide();
            YAHOO.OnData.TitanFL.container.wait.style.display = "none";
            return;
        }

        args3[0] = document.getElementById("txtEntityName").value;

        makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntitySave" + vlrEntityName + vlrEntityDisplayName + vlrEntityType + vlrEntityDescription + vlrEntityReadOnly + vlrEntityHidden, callbackSaveEntity);
    }
    else {
        ShowError("Seleccione una Entidad");
    }
}

function generateEntity() {
    if (document.getElementById("lstEntities").value != "" && document.getElementById("lstEntities").value != null) {
        //YAHOO.OnData.TitanFL.container.wait.show();
        YAHOO.OnData.TitanFL.container.wait.style.display = "block";

        var vlrEntityName = "";

        if (document.getElementById("txtEntityName") != null && document.getElementById("txtEntityName").value != '')
            vlrEntityName = "&EntityName=" + document.getElementById("txtEntityName").value;

        makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntityGenerate" + vlrEntityName, callbackGenerateEntity);
    }
    else {
        ShowError("Seleccione una Entidad");
    }
}

// Eliminar una entidad.
function deleteEntity() {
    if (confirm("¿Realmente desea eliminar la entidad?", "Si", "No")) {
        if (document.getElementById("lstEntities").value != "" && document.getElementById("lstEntities").value != null) {
            //YAHOO.OnData.TitanFL.container.wait.show();
            YAHOO.OnData.TitanFL.container.wait.style.display = "block";
            var mylist = null;

            if (document.getElementById("lstEntities") != null)
                mylist = document.getElementById("lstEntities");

            var vlrEntity = "";

            if (mylist != null)
                vlrEntity = "&Entity=" + mylist.options[mylist.selectedIndex].value;

            makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntityDelete" + vlrEntity, callbackDeleteEntity);
        }
        else {
            ShowError("Seleccione una Entidad");
        }
    }
}

// Crear una entidad.
function newEntity() {
    cleanFieldsEntityNew();
    YAHOO.OnData.TitanFL.container.dlgEntity.setHeader("Crear Entidad");
    YAHOO.OnData.TitanFL.container.dlgEntity.show();
    //YAHOO.OnData.TitanFL.container.dlgEntity.render();
    //YAHOO.OnData.TitanFL.container.wait.hide();
    YAHOO.OnData.TitanFL.container.wait.style.display = "none";

    // Llenar el SourceEntity
    // Mirar el nombre del SourceEntity, si es lst o un ddl.
}

// Guardar los datos de una entidad nueva.
function saveNewEntity() {
    //YAHOO.OnData.TitanFL.container.wait.show();
    YAHOO.OnData.TitanFL.container.wait.style.display = "block";
    var mylist = null;

    if (document.getElementById("ddlEntityTypeNew") != null)
        mylist = document.getElementById("ddlEntityTypeNew");

    var vlrEntityName = "";
    var vlrEntityDisplayName = "";
    var vlrEntityType = "";

    if (document.getElementById("txtEntityNameNew") != null && document.getElementById("txtEntityNameNew").value != '')
        vlrEntityName = "&EntityName=" + document.getElementById("txtEntityNameNew").value;

    if (document.getElementById("txtEntityDisplayNameNew") != null && document.getElementById("txtEntityDisplayNameNew").value != '')
        vlrEntityDisplayName = "&EntityDisplayName=" + document.getElementById("txtEntityDisplayNameNew").value;

    if (mylist != null && mylist.options[mylist.selectedIndex].value != '' && mylist.options[mylist.selectedIndex].value != 'Seleccione...')
        vlrEntityType = "&EntityType=" + mylist.options[mylist.selectedIndex].value;

    if (vlrEntityName == '') {
        ShowError("Ingrese el nombre de la Entidad");
        //YAHOO.OnData.TitanFL.container.wait.hide();
        YAHOO.OnData.TitanFL.container.wait.style.display = "none";
        return 0;
    }

    if (vlrEntityDisplayName == '') {
        ShowError("Ingrese el nombre a mostrar de la Entidad");
        //YAHOO.OnData.TitanFL.container.wait.hide();
        YAHOO.OnData.TitanFL.container.wait.style.display = "none";
        return 0;
    }

    if (vlrEntityType == '') {
        ShowError("Seleccione un tipo de Entidad");
        //YAHOO.OnData.TitanFL.container.wait.hide();
        YAHOO.OnData.TitanFL.container.wait.style.display = "none";
        return 0;
    }

    args2[0] = document.getElementById("txtEntityNameNew").value;

    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntitySaveNew" + vlrEntityName + vlrEntityDisplayName + vlrEntityType, callbackSaveEntityNew);
    return 1;
}

// Cargar en un DropDownList SourceEntity.
function fillPropertySourceEntityName() {
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLPropertySourceEntityName", callbackPropertySourceEntityName);
}

// Cargar en un DropDownList SourceEntity para cuando es una nueva propiedad.
function fillPropertySourceEntityNameNew() {
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLPropertySourceEntityName", callbackPropertySourceEntityNameNew);
}

function fillPropertySourceEntityDisplayPropertyEdit() {
    var vlrPropertySourceEntityName = "";
    var myListSEN = null;

    if (document.getElementById('ddlPropertySourceEntityName') != null) {
        myListSEN = document.getElementById('ddlPropertySourceEntityName');

        if (myListSEN.options[myListSEN.selectedIndex].value != '' && myListSEN.options[myListSEN.selectedIndex].value != "Seleccione...")
            vlrPropertySourceEntityName = "&Entity=" + myListSEN.options[myListSEN.selectedIndex].value;
    }

    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLPropertySourceEntityDisplayProperty" + vlrPropertySourceEntityName, callbackPropertySourceEntityDisplayPropertyEdit);
}

// Cargar en un DropDownList los tipos de entidad.
function fillEntityType() {
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntityType", callbackEntityType);
}

// Cargar en un DropDownList los tipos de entidad para la configuración.
function fillEntityTypeSetting() {
    // Analizar si me sirve la misma funcion del lado del servidor.
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntityType", callbackEntityTypeSetting);
}

// Llenar el ddlPropertySourceEntityDisplayProperty tanto para edit y new en funcion del ddlPropertySourceEntityNameNew.
function fillPropertySourceEntityDisplayProperty(control) {
    var vlrPropertySourceEntityName = "";
    var myListSEN = null;

    if (document.getElementById(control) != null) {
        myListSEN = document.getElementById(control);

        if (myListSEN.options[myListSEN.selectedIndex].value != '' && myListSEN.options[myListSEN.selectedIndex].value != "Seleccione...")
            vlrPropertySourceEntityName = "&Entity=" + myListSEN.options[myListSEN.selectedIndex].value;
    }

    args[0] = control;

    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLPropertySourceEntityDisplayProperty" + vlrPropertySourceEntityName, callbackPropertySourceEntityDisplayProperty);
}

function init() {
    /****************************************************
    * Creación del waiting...
    ****************************************************/
    //      YAHOO.OnData.TitanFL.container.wait = new YAHOO.widget.Panel("wait", { width:"25px", context:["divPanelWait","br","br"], close:false, visible:true, modal:false} );
    //      
    //      YAHOO.OnData.TitanFL.container.wait.setHeader("");
    //		  YAHOO.OnData.TitanFL.container.wait.setBody("<img src='../../Images/Wait/busy_twirl2_1.gif'/>");
    //      YAHOO.OnData.TitanFL.container.wait.render(document.body);
    //      YAHOO.OnData.TitanFL.container.wait.hide();
    //      YAHOO.OnData.TitanFL.container.wait.style.display = "block";
    YAHOO.OnData.TitanFL.container.wait = document.getElementById("divPanelWait");

    /****************************************************
    * Creación del formulario para editar una propiedad.
    ****************************************************/
    var handleCancel = function (e) {
        this.hide();
    }

    var handleOK = function (e) {
        if (saveProperty() == 1)
            this.hide();
    }

    YAHOO.OnData.TitanFL.container.dlgProperty = new YAHOO.widget.SimpleDialog("dlgProperty", { visible: false, width: "450px", fixedcenter: true, modal: true, draggable: true });
    //YAHOO.OnData.TitanFL.container.dlgProperty.cfg.queueProperty("icon",YAHOO.widget.SimpleDialog.ICON_WARN);
    YAHOO.OnData.TitanFL.container.dlgProperty.cfg.queueProperty("buttons", [
																	{ text: "Guardar", handler: handleOK, isDefault: true },
																	{ text: "Cancelar", handler: handleCancel }
																]);

    var listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancel, scope: YAHOO.OnData.TitanFL.container.dlgProperty, correctScope: true });
    YAHOO.OnData.TitanFL.container.dlgProperty.cfg.queueProperty("keylisteners", listeners);

    YAHOO.OnData.TitanFL.container.dlgProperty.render(document.body);

    /****************************************************
    * Creación del formulario para crear una entidad.
    ****************************************************/
    var handleCancelEntity = function (e) {
        this.hide();
    }

    var handleOKEntity = function (e) {
        if (saveNewEntity() == 1)
            this.hide();
    }

    YAHOO.OnData.TitanFL.container.dlgEntity = new YAHOO.widget.SimpleDialog("dlgEntity", { visible: false, width: "400px", fixedcenter: true, modal: true, draggable: true });
    YAHOO.OnData.TitanFL.container.dlgEntity.cfg.queueProperty("buttons", [
																	{ text: "Guardar", handler: handleOKEntity, isDefault: true },
																	{ text: "Cancelar", handler: handleCancelEntity }
																]);

    var listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelEntity, scope: YAHOO.OnData.TitanFL.container.dlgEntity, correctScope: true });
    YAHOO.OnData.TitanFL.container.dlgEntity.cfg.queueProperty("keylisteners", listeners);

    YAHOO.OnData.TitanFL.container.dlgEntity.render(document.body);

    // Llenar el DropDownList con los tipos de entidad. El control ya esta creado en esta página.
    fillEntityType();

    /****************************************************
    * Creación del formulario para crear una propiedad.
    ****************************************************/
    var handleCancelPropertyNew = function (e) {
        this.hide();
    }

    var handleOKPropertyNew = function (e) {
        if (savePropertyNew() == 1)
            this.hide();
    }

    YAHOO.OnData.TitanFL.container.dlgPropertyNew = new YAHOO.widget.SimpleDialog("dlgPropertyNew", { visible: false, width: "450px", fixedcenter: true, modal: true, draggable: true });
    YAHOO.OnData.TitanFL.container.dlgPropertyNew.cfg.queueProperty("buttons", [
																	{ text: "Guardar", handler: handleOKPropertyNew, isDefault: true },
																	{ text: "Cancelar", handler: handleCancelPropertyNew }
																]);

    var listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelPropertyNew, scope: YAHOO.OnData.TitanFL.container.dlgPropertyNew, correctScope: true });
    YAHOO.OnData.TitanFL.container.dlgPropertyNew.cfg.queueProperty("keylisteners", listeners);

    YAHOO.OnData.TitanFL.container.dlgPropertyNew.render(document.body);

    // Lo llena la primera vez
    //fillPropertySourceEntityNameNew();

    /****************************************************
    * MakeRequest para cargar el List con las entidades
    ****************************************************/
    //YAHOO.OnData.TitanFL.container.wait.show();
    YAHOO.OnData.TitanFL.container.wait.style.display = "block";
    makeRequest("../../UIProviders/EntitySystem/EntitySystemUIProvider.aspx", "MethodToCall=GetTitanFLEntities", callbackEntities);
}

YAHOO.util.Event.addListener(window, "load", init, "", true);


