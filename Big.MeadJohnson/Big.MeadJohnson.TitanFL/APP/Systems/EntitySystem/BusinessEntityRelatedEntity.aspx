﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityRelatedEntity.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.EntitySystem.BusinessEntityRelatedEntity" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24">
                    <asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px">
                    <div style="height: 100%">
                        <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                            <script type="text/javascript">

                                function GridCreated(sender, eventArgs) {
                                    var gridContainer = document.getElementById("ctl00_maincontent_rgList_GridData");
                                    if (gridContainer != null)
                                        gridContainer.style.height = "100%";
                                }
                            </script>
                        </telerik:RadScriptBlock>
                        <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="true" AllowPaging="true"
                            PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                            OnColumnCreated="rgList_ColumnCreated" OnItemDataBound="rgList_ItemDataBound">
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <MasterTableView TableLayout="Fixed" Width="100%">
                                <Columns>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                            <asp:LinkButton ID="lnkEdit" runat="server" ToolTip="Editar" CommandArgument='<%# Eval("GUID") %>'
                                                CausesValidation="false">
                                                <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="35px" />
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <NoRecordsTemplate>
                                    Sin registros encontrados
                                </NoRecordsTemplate>
                            </MasterTableView>
                            <ClientSettings AllowDragToGroup="false">
                                <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                <Selecting AllowRowSelect="false" />
                                <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                            </ClientSettings>
                        </telerik:RadGrid></div>
                    <div style="width: 100%; text-align: right; padding-top: 10px">
                        <asp:Button ID="btnAdd" runat="server" Text="Ingresar" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadWindowManager ID="rdPopUp" runat="server" ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radWindow" runat="server" Width="700px" Height="550px" Behaviors="Close"
                InitialBehaviors="Close" Modal="true" ReloadOnShow="true" ShowContentDuringLoad="false"
                VisibleStatusbar="false" NavigateUrl="BusinessEntityRelatedEntityDetail.aspx"
                OnClientClose="ReloadPage">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <script type="text/javascript">
        function openDetail(url) {
            radopen(url, "radWindow");
        }

        function ReloadPage(sender, eventArgs) {
            window.location.href = window.location.href;
        }
    </script>
</asp:Content>
