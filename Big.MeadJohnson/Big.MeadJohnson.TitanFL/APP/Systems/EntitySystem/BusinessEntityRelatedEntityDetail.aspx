﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/Popup.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityRelatedEntityDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.EntitySystem.BusinessEntityRelatedEntityDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <script type="text/javascript" src="../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit2">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24px">
                    &nbsp;
                </td>
                <td height="20" align="right" valign="Top">
                    <span id="imgLoading">
                        <img src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando...</span>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px" colspan="2">
                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td>
                                <asp:Xml ID="xmlDetailContainer" runat="server"></asp:Xml>
                                <link href="../../Scripts/CalendarControl.css" rel="stylesheet" type="text/css" />
                                <script type="text/javascript" src="../../Scripts/CalendarControl.js"></script>
                                <asp:PlaceHolder ID="controlContainer" runat="server" />
                                <div id="entityDetailContainer">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="dlgPassword" style="visibility: hidden;">
        <div id="Div1">
            <table width="100%" border="0" style="padding-left: 10px; padding-top: 10px">
                <tr>
                    <td style="width: 40%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Contraseña actual
                    </td>
                    <td style="width: 60%">
                        <input type="password" id="pswCurrent" style="width: 90%" onkeypress="return inCharactersPasswordTitanFL(event)"
                            value="" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 40%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Contraseña nueva
                    </td>
                    <td style="width: 60%">
                        <input type="password" id="pswNew" style="width: 90%" onkeypress="return inCharactersPasswordTitanFL(event)"
                            value="" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 40%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Confirmar contraseña nueva
                    </td>
                    <td style="width: 60%">
                        <input type="password" id="pswNewConfirm" style="width: 90%" onkeypress="return inCharactersPasswordTitanFL(event)"
                            value="" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="dlgEntityReferenceData" style="visibility: hidden;">
        <div id="bd">
            <table cellpadding="10px">
                <tr>
                    <td style="width: 47%; text-align: center;">
                        <select style="width: 250px;" id="lstEntityInstanceDataNew" size="10">
                        </select>
                    </td>
                    <td style="width: 6%;" align="center">
                        <table>
                            <tr>
                                <td align="center">
                                    <img src="../../Images/Cluster/last.gif" style="cursor: pointer" onclick="AllEntityInstanceToSelected('New')" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="../../Images/Cluster/next.gif" style="cursor: pointer" onclick="SomeEntityInstanceToSelected('New')" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="../../Images/Cluster/previous.gif" style="cursor: pointer" onclick="SomeSelectedToEntityInstance('New')" />
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <img src="../../Images/Cluster/first.gif" style="cursor: pointer" onclick="AllSelectedToEntityInstance('New')" />
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 47%; text-align: center;">
                        <select style="width: 250px;" id="lstSelectedNew" size="10">
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="dlgManageOutOfRangeInstances" style="visibility: hidden;">
        <div id="bd">
            <table border="0" cellpadding="5px">
                <tr>
                    <td style="text-align: left; vertical-align: middle">
                        Que comience por:&nbsp;<input type="text" id="txtFindValue" style="width: 50%" value="" />&nbsp;<input
                            type="button" id="btnFindValue" value="Buscar" onclick="findValue()" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 174px;" align="center" valign="middle">
                        <div style="width: 380px" id="divSelectOutofRange">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
