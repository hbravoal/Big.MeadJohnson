﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="ClusterManager.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.EntitySystem.ClusterManager" %>
<asp:Content ID="ClusterManagerContent" ContentPlaceHolderID="maincontent" Runat="Server">
   <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>   
   <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
   <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
   <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
   
   <div id="adminedit">
      
      <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
         <tr>
            <td valign="top">
               <table cellpadding="0" cellspacing="0" border="0" width="100%">
                  <tr>
                     <td height="20" align="right" valign="Top">
                        <span id="wait">
                           <img src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando...</span></td>
                  </tr>
               </table>
               <div id="divConfigCluster">
               </div>
            </td>
         </tr>
      </table>
      
      <table border="0">
         <tr>
            <td>
               <div id="divErrorAJAX">
               </div>
            </td>
         </tr>
         <tr>
            <td>
               <div id="divContentAJAX">
               </div>
            </td>
         </tr>
      </table>
   </div>
   
   <%--Formulario para crear cluster--%>
   <div id="dlgCluster" style="visibility: hidden;">
      <div id="bd">
         <table border="0" width="100%" style="padding-left:10px;padding-top:10px">
            <tr>
               <td style="width: 30%;text-align:right">
                  <span style="color:Red">*</span>&nbsp;Nombre del Cluster</td>
               <td style="width: 70%">
                  <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtClusterNameNew" value="" onkeypress="return inCharacters(event)" maxlength="128"/>                  
               </td>
            </tr>
            <tr>
               <td style="width: 30%;text-align:right">
                  <span style="color:Red">*</span>&nbsp;Nombre a mostrar del Cluster</td>
               <td style="width: 70%">
                  <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtClusterDisplayNameNew" value="" maxlength="128"/>
               </td>
            </tr>
            <tr>
               <td colspan="2">
                  &nbsp;
               </td>
            </tr>
         </table>
         <table border="0" width="100%" cellpadding="10px">
            <tr>
               <td align="center"><strong>Entidades del Negocio</strong></td>
               <td></td>
               <td align="center"><span style="color:Red">*</span>&nbsp;&nbsp;<strong>Entidades del Cluster</strong></td>
            </tr>
            <tr>
               <td style="width: 47%; text-align:center;">
                  <select style="width: 250px;" id="lstBusinessEntitiesNew" size="10" >
                  </select>
               </td>
               <td style="width: 6%;" align="center">
                  <table>
                     <tr>
                        <td align="center">
                           <img src="../../Images/Cluster/last.gif" style="cursor: pointer" onclick="AllBusinessEntititesToCluster('New')" /></td>
                     </tr>
                     <tr>
                        <td align="center">
                           <img src="../../Images/Cluster/next.gif" style="cursor: pointer" onclick="SomeBusinessEntititesToCluster('New')"/></td>
                     </tr>
                     <tr>
                        <td align="center">
                           <img src="../../Images/Cluster/previous.gif" style="cursor: pointer" onclick="SomeClusterToBusinessEntitites('New')"/></td>
                     </tr>
                     <tr>
                        <td align="center">
                           <img src="../../Images/Cluster/first.gif" style="cursor: pointer" onclick="AllClusterToBusinessEntitites('New')"/></td>
                     </tr>
                  </table></td>
               <td style="width: 47%; text-align:center;">
                  <select style="width: 250px;" id="lstClustersNew" size="10" >
                  </select>
               </td>
            </tr>
         </table>
      </div>
   </div>
   
   <script type="text/javascript" src="ClusterManager.js"></script>

</asp:Content>
