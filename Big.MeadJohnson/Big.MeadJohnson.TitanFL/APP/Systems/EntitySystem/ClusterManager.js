﻿YAHOO.namespace('OnData.TitanFL.container');

var divConfigCluster = document.getElementById('divConfigCluster'); // Es el div en donde se pone la configuración de un cluster.

// Si el callback es exitoso al momento de cargar los clusters.
var handleSuccessClusters = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      divConfigCluster.innerHTML = response;
      
      document.getElementById('wait').style.display = "none";
   }
};

// Si el callback es exitoso al guardar la información de un cluster nuevo.
var handleSuccessSaveNewCluster = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response == "True" )
      {
         makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLClusters", callbackClusters);
         document.getElementById('wait').style.display = "none";
      }
      else
      {
         ShowError("El Cluster ya existe");
         document.getElementById('wait').style.display = "none";
      }
   }
};

// Si el callback es exitoso al eliminar un cluster.
var handleSuccessDeleteCluster = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response != "" )
      {
         ShowError(response);
      }
      
      makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLClusters", callbackClusters);
      document.getElementById('wait').style.display = "none";
   }
};

// Si el callback es exitoso al crear el formulario de edición para el cluster.
var handleSuccessEditCluster = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      YAHOO.OnData.TitanFL.container.dlgEditCluster.setHeader("Editar Cluster");
		YAHOO.OnData.TitanFL.container.dlgEditCluster.setBody(response);
		fillBusinessEntities('lstBusinessEntitiesEdit', 'true');
		YAHOO.OnData.TitanFL.container.dlgEditCluster.show();
	   
	   document.getElementById('wait').style.display = "none";
   }
};

// Si el callback es exitoso al traer los entidades de tipo BusinessEntities.
var handleSuccessGetBusinessEntities = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      fillSelect(response, o.argument[0]);
      
      if ( o.argument[1] == 'true' )
      {
         var listClusters = document.getElementById('lstClustersEdit');
         var listBusinessEntities = document.getElementById('lstBusinessEntitiesEdit');
         
         if ( listClusters == null || listBusinessEntities == null )
            ShowError('No existen los controles');
            
         for ( var i = 0; i < listClusters.length; i++ )
         {
            for ( var j = 0; j < listBusinessEntities.length; j++ )
            {
               if ( listClusters.options[i].value == listBusinessEntities.options[j].value )
               {
                  listClusters.options[i].text = listBusinessEntities.options[j].text;
                  listBusinessEntities.remove(j);
               }
            }         
         }
      }
   }
   document.getElementById('wait').style.display = "none";
};

// Si el callback falla.
var handleFailure = function(o){
   if(o.responseText !== undefined){
      var divErrorAJAX = document.getElementById('divErrorAJAX');
      var divContentAJAX = document.getElementById('divContentAJAX');    
      divErrorAJAX.innerHTML = "<strong>Error!</strong>";
      divContentAJAX.innerHTML += "<li>Id de la transacción: " + o.tId + "</li>";
      divContentAJAX.innerHTML += "<li>Estado HTTP: " + o.status + "</li>";
      divContentAJAX.innerHTML += "<li>Código del mensaje de estado: " + o.statusText + "</li>";
      document.getElementById('wait').style.display = "none";
   }
};

// Callback para traer los clusters.
var callbackClusters =
{
   success:handleSuccessClusters,
   failure:handleFailure
};

// Callback para guardar información de un nuevo cluster.
var callbackSaveNewCluster =
{
   success:handleSuccessSaveNewCluster,
   failure:handleFailure
};

// Callback para eliminar un cluster.
var callbackDeleteCluster =
{
   success:handleSuccessDeleteCluster,
   failure:handleFailure
};

var callbackEditCluster =
{
   success:handleSuccessEditCluster,
   failure:handleFailure
};

// Callback para traer las entidades de tipo BusinessEntities.
var args = ['',''];

var callbackGetBusinessEntities =
{
   success:handleSuccessGetBusinessEntities,
   failure:handleFailure,
   argument:args
}

// Para cualquier solicitud a través de AJAX
// sUrl: Es la pagina aspx.
// postData: Los parametros que se envian.
// callbakc: El callback que tiene cuando es exitoso o fallido.
function makeRequest(sUrl, postData, callback)
{
   var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);      
}

// Abre el dialogo para crear un cluster.
function newCluster()
{
   cleanFieldsClusterNew();
   fillBusinessEntities('lstBusinessEntitiesNew','false');
   YAHOO.OnData.TitanFL.container.dlgCluster.setHeader("Crear Cluster");
   YAHOO.OnData.TitanFL.container.dlgCluster.show();      
}

// Guardar un nuevo cluster.
function saveNewCluster()
{
   document.getElementById('wait').style.display = "block";
   
   var mylist = null;
   
   if ( document.getElementById("lstClustersNew") != null )
      mylist = document.getElementById("lstClustersNew");
   
   var vlrClusterName = "";
   var vlrClusterDisplayName = "";
   var vlrClusterBusinessEntities = "";
   
   if ( document.getElementById("txtClusterNameNew") != null && document.getElementById("txtClusterNameNew").value != '' )
      vlrClusterName = "&ClusterName=" + document.getElementById("txtClusterNameNew").value;
      
   if ( document.getElementById("txtClusterDisplayNameNew") != null && document.getElementById("txtClusterDisplayNameNew").value != '' )
      vlrClusterDisplayName = "&ClusterDisplayName=" + document.getElementById("txtClusterDisplayNameNew").value;
      
   if ( vlrClusterName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Ingrese el nombre del Cluster");
      return 0;
   }
   
   if ( vlrClusterDisplayName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Ingrese el nombre a mostrar del Cluster");
      return 0;
   }
   
   if ( mylist.length == 0 )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Agregue por lo menos una entidad");
      return 0;
   }
   else
   {
      var elements = "";
      
      for ( var i = 0; i < mylist.length; i++ )
      {
         elements += mylist.options[i].value + ",";
      }
      
      if ( elements.length != 0 )
      elements = elements.substring(0, elements.length-1);
      
      vlrClusterBusinessEntities = "&BusinessEntities=" + elements;
   }
   
   makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLSaveNewCluster" + vlrClusterName + vlrClusterDisplayName + vlrClusterBusinessEntities + "&Action=New", callbackSaveNewCluster);
   return 1;
}

// Guardar un cluster editado.
function saveEditCluster()
{
   document.getElementById('wait').style.display = "block";
   
   var mylist = null;
   
   if ( document.getElementById("lstClustersEdit") != null )
      mylist = document.getElementById("lstClustersEdit");
   
   var vlrClusterName = "";
   var vlrClusterDisplayName = "";
   var vlrClusterBusinessEntities = "";
   
   if ( document.getElementById("txtClusterNameEdit") != null && document.getElementById("txtClusterNameEdit").value != '' )
      vlrClusterName = "&ClusterName=" + document.getElementById("txtClusterNameEdit").value;
      
   if ( document.getElementById("txtClusterDisplayNameEdit") != null && document.getElementById("txtClusterDisplayNameEdit").value != '' )
      vlrClusterDisplayName = "&ClusterDisplayName=" + document.getElementById("txtClusterDisplayNameEdit").value;
      
   if ( vlrClusterName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Ingrese el nombre del Cluster");
      return 0;
   }
   
   if ( vlrClusterDisplayName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Ingrese el nombre a mostrar del Cluster");
      return 0;
   }
   
   if ( mylist.length == 0 )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Agregue por lo menos una entidad");
      return 0;
   }
   else
   {
      var elements = "";
      
      for ( var i = 0; i < mylist.length; i++ )
      {
         elements += mylist.options[i].value + ",";
      }
      
      if ( elements.length != 0 )
      elements = elements.substring(0, elements.length-1);
      
      vlrClusterBusinessEntities = "&BusinessEntities=" + elements;
   }
   
   makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLSaveNewCluster" + vlrClusterName + vlrClusterDisplayName + vlrClusterBusinessEntities + "&Action=Edit", callbackSaveNewCluster);
   return 1;
}

// Llenar las entidades tipo BusinessEntities en el dialogo de crear un cluster.
function fillBusinessEntities(control, edit)
{
   args[0] = control;
   args[1] = edit;
   
   //document.getElementById('wait').style.display = "block";
   makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLBusinessEntities", callbackGetBusinessEntities);
}

// Llenar un select con las opciones que vienen en un string.
// stringValues: Son los valores que llegan del lado del servidor. Deben estar separados por coma.
// idControl: Es el control al cual se le va a agregar las opciones que llegan en el string.
function fillSelect(stringValues, idControl)
{
   var optionsList = stringValues.split(",");
   var elSel = null;
   var item = null;
   
   if ( document.getElementById(idControl) != null )
      elSel = document.getElementById(idControl);
   
   if ( elSel != null )
   {
      for (var i=0; i < optionsList.length; i++) 
      {
         item = optionsList[i].split("|");
         var elOptNew = document.createElement('option');
         elOptNew.value = item[0];
         elOptNew.text = item[1];
         
         try {
            elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
         }
         catch(ex) {
            elSel.add(elOptNew); // IE only
         }
      }
   }
}

// Pasar todas las opciones del select que contiene las entidades de tipo businessEntities al select que contiene las entidades
// que hacen parte del cluster.
function AllBusinessEntititesToCluster(type)
{
   if ( document.getElementById('lstBusinessEntities' + type).length != 0 )
      fillSelect(CleanSelected('lstBusinessEntities' + type), 'lstClusters' + type);
}

// Pasar todas las opciones del select que contiene las entidades que hacen parte del cluster al select que contiene las entidades
// de tipo businessEntities.
function AllClusterToBusinessEntitites(type)
{
   if ( document.getElementById('lstClusters' + type).length != 0 )
      fillSelect(CleanSelected('lstClusters' + type), 'lstBusinessEntities' + type);
}

// Pasa la opcion seleccionada del select que contiene las entidades que hacen parte del cluster al select que contiene las entidades
// de tipo businessEntities.
function SomeClusterToBusinessEntitites(type)
{
   if ( document.getElementById('lstClusters' + type).length != 0 )
      ExchangeOptions('lstClusters' + type, 'lstBusinessEntities' + type);
}

// Pasa la opcion seleccionada del select que contiene las entidades de tipo businessEntities al select que contiene las entidades
// que hacen parte del cluster.
function SomeBusinessEntititesToCluster(type)
{
   if ( document.getElementById('lstBusinessEntities' + type).length != 0 )
      ExchangeOptions('lstBusinessEntities' + type, 'lstClusters' + type);
}

// Intercambiar opciones entre 2 selects.
function ExchangeOptions(controlOld, controlNew)
{
   var selected = document.getElementById(controlOld).selectedIndex;
   var vlrOption = "";
   var vlrText = "";
   
   if ( selected != -1 )
   {
      vlrOption = document.getElementById(controlOld).options[selected].value;
      vlrText = document.getElementById(controlOld).options[selected].text;
      
      document.getElementById(controlOld).remove(selected);
      
      var elOptNew = document.createElement('option');
      elOptNew.text = vlrText;
      elOptNew.value = vlrOption;
      
      try {
         document.getElementById(controlNew).add(elOptNew, null); // standards compliant; doesn't work in IE
      }
      catch(ex) {
         document.getElementById(controlNew).add(elOptNew); // IE only
      }
   }
}

// Quitar todas las opciones de un select y retornarlo como un string con las opciones retiradas.
function CleanSelected(control)
{
   var elements = "";
   for ( var i = (document.getElementById(control).length - 1); i != -1; i-- )
   {
      elements += document.getElementById(control).options[0].value + "|" + document.getElementById(control).options[0].text + ",";
      document.getElementById(control).remove(0);
   }
   
   if ( elements.length != 0 )
      elements = elements.substring(0, elements.length-1);
      
   return elements;
}

// Validar la entrada de caracteres.
function inCharacters(e) {
   tecla = (document.all) ? e.keyCode : e.which;
   
   if ( tecla == 8 ) 
      return true;
   
   if ( tecla == 0 ) 
      return true;
   
   patron =/[A-Za-z0-9_]/;
   te = String.fromCharCode(tecla);
   return patron.test(te);
}

// Limpia los controles cuando se va a crear un cluster.
function cleanFieldsClusterNew()
{
   if ( document.getElementById("txtClusterNameNew") != null )
      document.getElementById("txtClusterNameNew").value = "";
      
   if ( document.getElementById("txtClusterDisplayNameNew") != null )
      document.getElementById("txtClusterDisplayNameNew").value = "";
      
   CleanSelected('lstClustersNew');
   CleanSelected('lstBusinessEntitiesNew');
}

// Eliminar un cluster.
function deleteCluster(cluster)
{
   if ( confirm("¿Realmente desea eliminar el cluster?", "Si", "No" ) )
   {
      document.getElementById('wait').style.display = "block";
      makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLDeleteCluster&ClusterName=" + cluster, callbackDeleteCluster);
   }
}

function editCluster(cluster)
{
   document.getElementById('wait').style.display = "block";
   makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLEditCluster&ClusterName=" + cluster, callbackEditCluster);
}

function init() 
{
   /****************************************************
    * Creación del formulario para crear un cluster.
    ****************************************************/
   var handleCancelCluster = function(e) {
      this.hide();
	}

	var handleOKCluster = function(e) {
	   if ( saveNewCluster() == 1 )
	      this.hide();
	}
   
   YAHOO.OnData.TitanFL.container.dlgCluster = new YAHOO.widget.SimpleDialog("dlgCluster", { visible:false, width: "590px", fixedcenter:true, modal:true, draggable:true });        
	YAHOO.OnData.TitanFL.container.dlgCluster.cfg.queueProperty("buttons", [ 
																	{ text:"Guardar", handler:handleOKCluster, isDefault:true },
																	{ text:"Cancelar", handler:handleCancelCluster }
																]);
																
   var listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handleCancelCluster ,scope:YAHOO.OnData.TitanFL.container.dlgCluster, correctScope:true} );
	YAHOO.OnData.TitanFL.container.dlgCluster.cfg.queueProperty("keylisteners", listeners);

	YAHOO.OnData.TitanFL.container.dlgCluster.render(document.body);

   /****************************************************
    * Creación del formulario para editar un cluster.
    ****************************************************/
   var handleCancelEditCluster = function(e) {
      this.hide();
	}

	var handleOKEditCluster = function(e) {
	   if ( saveEditCluster() == 1 )
	      this.hide();
	}
   
   YAHOO.OnData.TitanFL.container.dlgEditCluster = new YAHOO.widget.SimpleDialog("dlgEditCluster", { visible:false, width: "590px", fixedcenter:true, modal:true, draggable:true });        
	YAHOO.OnData.TitanFL.container.dlgEditCluster.cfg.queueProperty("buttons", [ 
																	{ text:"Guardar", handler:handleOKEditCluster, isDefault:true },
																	{ text:"Cancelar", handler:handleCancelEditCluster }
																]);
																
   listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handleCancelEditCluster ,scope:YAHOO.OnData.TitanFL.container.dlgEditCluster, correctScope:true} );
	YAHOO.OnData.TitanFL.container.dlgEditCluster.cfg.queueProperty("keylisteners", listeners);

	YAHOO.OnData.TitanFL.container.dlgEditCluster.render(document.body);	
	
   /****************************************************
    * MakeRequest para cargar los clusters
    ****************************************************/      
   document.getElementById('wait').style.display = "block";
   makeRequest("../../UIProviders/EntitySystem/EntityClusterUIProvider.aspx", "MethodToCall=GetTitanFLClusters", callbackClusters);
}

YAHOO.util.Event.addListener(window, "load", init, "",true);

