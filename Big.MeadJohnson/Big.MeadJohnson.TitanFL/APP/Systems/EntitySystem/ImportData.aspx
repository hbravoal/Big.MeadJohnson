﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="ImportData.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.EntitySystem.ImportData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit" style="display: block">
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width: 180px; text-align: right">
                    Identificar registro por:
                </td>
                <td valign="top" colspan="2">
                    <asp:RadioButton ID="rdbClavePrimaria" runat="server" Text="Clave primaria" GroupName="Identicacion"
                        Checked="true" />&nbsp;
                    <asp:RadioButton ID="rdbGuid" runat="server" Text="GUID" GroupName="Identicacion" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Generar GUID:
                </td>
                <td valign="top" colspan="2">
                    <asp:CheckBox ID="chkGenerarGuid" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Sistema:
                </td>
                <td valign="top">
                    <asp:DropDownList ID="cboSystems" AutoPostBack="true" runat="server" OnSelectedIndexChanged="cboSystems_SelectedIndexChanged">
                        <asp:ListItem>-- Seleccione --</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td style="width: 200" valign="top" align="right">
                    <span id="imgLoading" style="display: none">
                        <img alt="" src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando... </span>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Entidades:
                </td>
                <td valign="top" colspan="2">
                    <asp:DropDownList ID="cboEntities" runat="server" AutoPostBack="True" OnSelectedIndexChanged="cboEntities_SelectedIndexChanged">
                        <asp:ListItem>-- Seleccione --</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Validar Fecha Registro:
                </td>
                <td valign="top" colspan="2">
                    <asp:CheckBox ID="Chkvalidar" runat="server" AutoPostBack="True" Enabled="False"
                        OnCheckedChanged="Chkvalidar_CheckedChanged" />
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Campos Entidad:
                </td>
                <td valign="top" colspan="2">
                    <asp:DropDownList ID="ddlCampos" runat="server" Enabled="False">
                        <asp:ListItem Text="-- Seleccione --" Value="-1"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Separador:
                </td>
                <td valign="top" colspan="2">
                    <asp:DropDownList ID="ddlSeparador" runat="server">
                        <asp:ListItem Text="Coma (,)" Value="," Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pipe (|)" Value="|"></asp:ListItem>
                        <asp:ListItem Text="Punto y coma (;)" Value=";"></asp:ListItem>
                        <asp:ListItem Text="Tabulador" Value="t"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Archivo:
                </td>
                <td valign="top" colspan="2">
                    <input type="file" id="txtArchivo" onkeydown="javascript:return false;" size="50"
                        runat="server" />
                </td>
            </tr>
            <%--<tr>
               <td></td>
               <td colspan="2">
                  <asp:Panel ID="Panel1" runat="server" GroupingText="Opciones de importación" Height="100px" Width="180px">
                     <asp:RadioButton ID="rdbInsertar" runat="server" Text="Insertar datos" GroupName="Importacion"/>
                     <asp:RadioButton ID="rdbActualizar" runat="server" Text="Actualizar datos" GroupName="Importacion"/></asp:Panel>
               </td>
            </tr>--%>
        </table>
        <table width="100%" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width: 125px">
                    &nbsp;
                </td>
                <td>
                    <asp:Button Text="Importar" ID="btnImportar" runat="server" OnClick="btnImportar_Click" />&nbsp;
                </td>
            </tr>
        </table>
        <hr />
        <table width="100%" cellpadding="4" cellspacing="0" border="0">
            <tr>
                <td style="width: 120px; text-align: right">
                    Estado:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCargando" runat="server">
                        <asp:ListItem Text="-- Seleccione --" Value=""></asp:ListItem>
                        <asp:ListItem Text="Cargando" Value="1" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Carga terminada" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    <asp:Button Text="Buscar" ID="btnBuscar" runat="server" OnClick="btnBuscar_Click" />
                </td>
            </tr>
        </table>
        <%--Colocar listado--%>
        <asp:GridView ID="gvCargas" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None"
            Width="100%" EmptyDataText="No hay cargas" AllowPaging="True" PageSize="10" OnPageIndexChanging="gvCargas_PageIndexChanging"
            OnRowDataBound="gvCargas_RowDataBound">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EditRowStyle BackColor="#999999" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
    </div>
</asp:Content>
