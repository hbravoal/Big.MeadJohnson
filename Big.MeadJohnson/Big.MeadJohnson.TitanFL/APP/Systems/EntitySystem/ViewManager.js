﻿YAHOO.namespace('OnData.TitanFL.container');

var divConfigView = document.getElementById('divConfigView'); // Es el div en donde se pone la configuración de una vista.

var handleSuccessViews = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      divConfigView.innerHTML = response;
      
      document.getElementById('wait').style.display = "none";
   }
};

var handleSuccessNewView = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      divConfigView.innerHTML = response;
      
      document.getElementById('wait').style.display = "none";
   }
};

var handleSuccessGetPropertiesCluster = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      document.getElementById('divWorkArea').innerHTML = response;
      
      document.getElementById('wait').style.display = "none";
   }
};

var handleSuccessGetGroupsView = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      document.getElementById('divWorkArea').innerHTML = response;
      
      document.getElementById('wait').style.display = "none";
   }
};

var handleSuccessSaveNewView = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response == "True" )
      {
         if ( document.getElementById('btnGroupsNew') != null )
            document.getElementById('btnGroupsNew').disabled = false;
         if ( document.getElementById('txtViewNameNew') != null )
            document.getElementById('txtViewNameNew').disabled = true;
         if ( document.getElementById('ddlClusterNew') != null )
            document.getElementById('ddlClusterNew').disabled = true;
         if ( document.getElementById('btnGenerateNew') != null )
            document.getElementById('btnGenerateNew').disabled = false;
         if ( document.getElementById('btnSaveNew') != null )
            document.getElementById('btnSaveNew').onclick = "saveNewView('Edit')";
            
         document.getElementById('wait').style.display = "none";
         
         GetGroupsView();
      }
      else if ( response == "False" )
      {
         ShowError("La Vista ya existe");
         Load();
         document.getElementById('wait').style.display = "none";
      }
      else
      {
         ShowError(response);
         Load();
         document.getElementById('wait').style.display = "none";
      }
   }
};

var handleSuccessSaveNewGroup = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
         
      if ( response == "True" )
      {
         if ( document.getElementById('btnPropertiesNew') != null )
            document.getElementById('btnPropertiesNew').disabled = false;
            
         document.getElementById('wait').style.display = "none";
            
         GetGroupsView();
      }
      else
      {
         ShowError("El Grupo ya existe");
         document.getElementById('wait').style.display = "none";
      }
   }
};

var handleSuccessDeleteView = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      document.getElementById('wait').style.display = "none";
      
      Load();
   }
};

var handleSuccessEditView = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      divConfigView.innerHTML = response;
      
      document.getElementById('btnGroupsNew').disabled = false;
      document.getElementById('btnPropertiesNew').disabled = false;
      
      GetGroupsView();
      document.getElementById('wait').style.display = "none";
   }
};

var handleSuccessGenerateView = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response == "" )
         ShowError("Vista generada");
      else
         ShowError(response);
      
      document.getElementById('wait').style.display = "none";
   }
};

var handleSuccessDeleteViewGroup = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      document.getElementById('wait').style.display = "none";
      
      GetGroupsView();
      ValidateProperties();
   }
};

var handleSuccessValidateProperties = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response == 'false' )
      {
         if ( document.getElementById('btnPropertiesNew') != null )
            document.getElementById('btnPropertiesNew').disabled = true;
      }
      else
      {
         if ( document.getElementById('btnPropertiesNew') != null )
            document.getElementById('btnPropertiesNew').disabled = false;
      }
      
      document.getElementById('wait').style.display = "none";
      
      
   }
};

var handleSuccessEditViewGroup = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      YAHOO.OnData.TitanFL.container.dlgGroupEdit.setHeader("Editar Grupo");
		YAHOO.OnData.TitanFL.container.dlgGroupEdit.setBody(response);
	   YAHOO.OnData.TitanFL.container.dlgGroupEdit.show();
      
      document.getElementById('wait').style.display = "none";
   }
};

var handleSuccessSaveEditGroup = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      ShowError("Grupo modificado");
      document.getElementById('wait').style.display = "none";
      
      GetGroupsView();
   }
};

var handleSuccessSavePropertiesViewNew = function(o)
{
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      ShowError("Propiedades guardadas");
      document.getElementById('wait').style.display = "none";
      
      GetPropertiesCluster();
   }
};

var handleFailure = function(o){
   if(o.responseText !== undefined){
      var divErrorAJAX = document.getElementById('divErrorAJAX');
      var divContentAJAX = document.getElementById('divContentAJAX');    
      divErrorAJAX.innerHTML = "<strong>Error!</strong>";
      divContentAJAX.innerHTML += "<li>Id de la transacción: " + o.tId + "</li>";
      divContentAJAX.innerHTML += "<li>Estado HTTP: " + o.status + "</li>";
      divContentAJAX.innerHTML += "<li>Código del mensaje de estado: " + o.statusText + "</li>";
      document.getElementById('wait').style.display = "none";
   }
};

var callbackViews =
{
   success:handleSuccessViews,
   failure:handleFailure
};

var callbackNewView = 
{
   success:handleSuccessNewView,
   failure:handleFailure
};

var callbackGetPropertiesCluster =
{
   success:handleSuccessGetPropertiesCluster,
   failure:handleFailure
};

var callbackGetGroupsView =
{
   success:handleSuccessGetGroupsView,
   failure:handleFailure
};

var callbackSaveNewView =
{
   success:handleSuccessSaveNewView,
   failure:handleFailure
};

var callbackSaveNewGroup =
{
   success:handleSuccessSaveNewGroup,
   failure:handleFailure
};

var callbackDeleteView =
{
   success:handleSuccessDeleteView,
   failure:handleFailure
};

var callbackEditView =
{
   success:handleSuccessEditView,
   failure:handleFailure
};

var callbackGenerateView =
{
   success:handleSuccessGenerateView,
   failure:handleFailure
};

var callbackDeleteViewGroup =
{
   success:handleSuccessDeleteViewGroup,
   failure:handleFailure
};

var callbackValidateProperties =
{
   success:handleSuccessValidateProperties,
   failure:handleFailure
};

var callbackEditViewGroup =
{
   success:handleSuccessEditViewGroup,
   failure:handleFailure
};

var callbackSaveEditGroup =
{
   success:handleSuccessSaveEditGroup,
   failure:handleFailure
};

var callbackSavePropertiesViewNew =
{
   success:handleSuccessSavePropertiesViewNew,
   failure:handleFailure
};

// Para cualquier solicitud a través de AJAX
// sUrl: Es la pagina aspx.
// postData: Los parametros que se envian.
// callbakc: El callback que tiene cuando es exitoso o fallido.
function makeRequest(sUrl, postData, callback)
{
   var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);      
}

function NewView()
{
   document.getElementById('wait').style.display = "block";
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLNewView", callbackNewView);
}

function saveNewView(action)
{
   document.getElementById('wait').style.display = "block";
   
   var mylist = null;
   
   var vlrViewName = "";
   var vlrViewDisplayName = "";
   var vlrClusterName = "";
   var vlrRoleGroupId = "";
   var parameters = "";
   
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
      vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
      
   if ( vlrViewName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Ingrese el nombre del rol");
      return;
   }
   
   if ( document.getElementById("txtViewDisplayNameNew") != null && document.getElementById("txtViewDisplayNameNew").value != '' )
      vlrViewDisplayName = "&ViewDisplayName=" + document.getElementById("txtViewDisplayNameNew").value;
      
   if ( vlrViewDisplayName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Ingrese el nombre a mostrar del rol");
      return;
   }
   
   if ( document.getElementById("ddlClusterNew") != null )
   {
      mylist = document.getElementById("ddlClusterNew");
      
      if ( mylist.options[mylist.selectedIndex].value != '' && mylist.options[mylist.selectedIndex].value != "Seleccione...")
      {
         vlrClusterName = "&ClusterName=" + mylist.options[mylist.selectedIndex].value;
      }
   }
   
   if ( vlrClusterName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Debe seleccionar un cluster para el rol");
      return;
  }

  if (document.getElementById("ddlRoleTypeNew") != null) {
      mylist = document.getElementById("ddlRoleTypeNew");

      if (mylist.options[mylist.selectedIndex].value != '' && mylist.options[mylist.selectedIndex].value != "Seleccione...") {
          vlrRoleGroupId = "&RoleGroupId=" + mylist.options[mylist.selectedIndex].value;
      }
  }

  if (vlrRoleGroupId == "") {
      document.getElementById('wait').style.display = "none";
      ShowError("Debe seleccionar un grupo de roles");
      return;
  }



  parameters = vlrViewName + vlrViewDisplayName + vlrClusterName + vlrRoleGroupId;
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLSaveNewView" + parameters + "&Action=" + action, callbackSaveNewView);
}

function GetPropertiesCluster()
{
   document.getElementById('wait').style.display = "block";
   
   var mylist = null;
   var valueList = "";
   var vlrViewName = "";
   
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
      vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
      
   if ( vlrViewName == "" )
   {
      document.getElementById('wait').style.display = "none";
      ShowError("Se requiere el nombre de la Vista");
      return;
   }
   
   if ( document.getElementById('ddlClusterNew') != null )
   {
      mylist = document.getElementById('ddlClusterNew');
      valueList = "&Cluster=" + mylist.options[mylist.selectedIndex].value;
   }
   
   if ( valueList == "" )
   {
      aleShowErrorrt("Se requiere un Cluster");
      return;
   }
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLPropertiesCluster" + vlrViewName + valueList, callbackGetPropertiesCluster);
}

function DeleteView(viewName)
{
   if ( confirm("¿Realmente desea eliminar la vista?", "Si", "No" ) )
   {
      document.getElementById('wait').style.display = "block";
      makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLDeleteView&ViewName=" + viewName, callbackDeleteView);
   }
}

function EditView(viewName)
{
   document.getElementById('wait').style.display = "block";
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLEditView&ViewName=" + viewName, callbackEditView);
}

function GetGroupsView()
{
   document.getElementById('wait').style.display = "block";
   
   var vlrViewName = "";
   
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
         vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLGroupsView" + vlrViewName, callbackGetGroupsView);

}

function AddGroupView()
{
   cleanFieldsGroupNew();
   YAHOO.OnData.TitanFL.container.dlgGroupNew.setHeader("Crear Grupo");
   YAHOO.OnData.TitanFL.container.dlgGroupNew.show();
}

function cleanFieldsGroupNew()
{
   if ( document.getElementById("txtGroupNameNew") != null )
      document.getElementById("txtGroupNameNew").value = "";
      
   if ( document.getElementById("txtGroupDisplayNameNew") != null )
      document.getElementById("txtGroupDisplayNameNew").value = "";
}

function saveNewGroup()
{
   document.getElementById('wait').style.display = "block";
   
   var vlrViewName = "";
   var vlrGroupName = "";
   var vlrGroupDisplayName = "";
   
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
      vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
   
   if ( vlrViewName == '' )
   {
      ShowError("El nombre de la Vista es necesario");
      document.getElementById('wait').style.display = "none";
      return 0;
   }
   
   if ( document.getElementById("txtGroupNameNew") != null && document.getElementById("txtGroupNameNew").value != '' )
      vlrGroupName = "&GroupName=" + document.getElementById("txtGroupNameNew").value;
   
   if ( vlrGroupName == '' )
   {
      ShowError("Ingrese el nombre del Grupo");
      document.getElementById('wait').style.display = "none";
      return 0;
   }
      
   if ( document.getElementById("txtGroupDisplayNameNew") != null && document.getElementById("txtGroupDisplayNameNew").value != '' )
      vlrGroupDisplayName = "&GroupDisplayName=" + document.getElementById("txtGroupDisplayNameNew").value;
   
   if ( vlrGroupDisplayName == '' )
   {
      ShowError("Ingrese el nombre a mostrar del Grupo");
      document.getElementById('wait').style.display = "none";
      return 0;
   }
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLSaveNewGroup" + vlrViewName + vlrGroupName + vlrGroupDisplayName, callbackSaveNewGroup);
   return 1;
}

function DeleteViewGroup(viewName, groupName)
{
   if ( confirm("¿Realmente desea eliminar el grupo?", "Si", "No" ) )
   {
      document.getElementById('wait').style.display = "block";
      makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLDeleteViewGroup&ViewName=" + viewName + "&GroupName=" + groupName, callbackDeleteViewGroup);
   }
}

function EditViewGroup(viewName, groupName)
{
   document.getElementById('wait').style.display = "block";
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLEditViewGroup&ViewName=" + viewName + "&GroupName=" + groupName, callbackEditViewGroup);
}

function ValidateProperties()
{
   document.getElementById('wait').style.display = "block";
   
   var vlrViewName = "";
   
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
      vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
      
   if ( vlrViewName == '' )
   {
      ShowError("El nombre de la Vista es necesario");
      document.getElementById('wait').style.display = "none";
      return;
   }
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLValidateProperties" + vlrViewName, callbackValidateProperties);
}

function inCharacters(e) {
   tecla = (document.all) ? e.keyCode : e.which;
   
   if ( tecla == 8 ) 
      return true;
   
   if ( tecla == 0 ) 
      return true;
   
   patron =/[A-Za-z0-9_]/;
   te = String.fromCharCode(tecla);
   return patron.test(te);
}

function saveEditGroup()
{
   document.getElementById('wait').style.display = "block";
   
   var vlrViewName = "";
   var vlrGroupName = "";
   var vlrGroupDisplayName = "";
   
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
      vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
   
   if ( vlrViewName == '' )
   {
      ShowError("El nombre de la Vista es necesario");
      document.getElementById('wait').style.display = "none";
      return 0;
   }
   
   if ( document.getElementById("txtGroupNameEdit") != null && document.getElementById("txtGroupNameEdit").value != '' )
      vlrGroupName = "&GroupName=" + document.getElementById("txtGroupNameEdit").value;
   
   if ( vlrGroupName == '' )
   {
      ShowError("Ingrese el nombre del Grupo");
      document.getElementById('wait').style.display = "none";
      return 0;
   }
      
   if ( document.getElementById("txtGroupDisplayNameEdit") != null && document.getElementById("txtGroupDisplayNameEdit").value != '' )
      vlrGroupDisplayName = "&GroupDisplayName=" + document.getElementById("txtGroupDisplayNameEdit").value;
   
   if ( vlrGroupDisplayName == '' )
   {
      ShowError("Ingrese el nombre a mostrar del Grupo");
      document.getElementById('wait').style.display = "none";
      return 0;
   }
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLSaveEditGroup" + vlrViewName + vlrGroupName + vlrGroupDisplayName, callbackSaveEditGroup);
   return 1;
}

function SavePropertiesViewNew(form)
{
   document.getElementById('wait').style.display = "block";
   
   if ( !ValidatePropertyRequired(form) )
   {
      ShowError("Hay propiedades que son requeridas en ciertas entidades que seleccionó");
      document.getElementById('wait').style.display = "none";
      return;
   }
   
   var vlrViewName = "";
   
   // Nombre de la vista
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
      vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
   
   if ( vlrViewName == '' )
   {
      ShowError("El nombre de la Vista es necesario");
      document.getElementById('wait').style.display = "none";
      return;
   }
   
   var varGroupName = "";
   var varEntityName = "";
   var varPropertyName = "";
   var varOrder = "";
   var varRequired = "";
   var varCounter = 1;
   var mylist = null;
   var varProperties = "";
   
   for(var i = 0; i < form.elements.length; i++)
   {
      if(form.elements[i].getAttribute("EntityProperty") == "true")
      {
         if (form.elements[i].checked == true )
         {
            // EL nombre del grupo
            if ( document.getElementById(form.elements[i].getAttribute("ddlGroupProperties")) != null )
            {
               mylist = document.getElementById(form.elements[i].getAttribute("ddlGroupProperties"));
               
               if ( mylist.options[mylist.selectedIndex].value != '' && mylist.options[mylist.selectedIndex].value != "Seleccione...")
               {
                  varGroupName = mylist.options[mylist.selectedIndex].value;
               }
               
               if ( varGroupName == "" )
               {
                  document.getElementById('wait').style.display = "none";
                  ShowError("Si ha seleccionado una propiedad debe también seleccionar un grupo al cual va a pertenecer");
                  return;
               }
            }
            
            // Nombre de la entidad
            if ( form.elements[i].getAttribute("EntityName") != '' )
               varEntityName = form.elements[i].getAttribute("EntityName");
            else
            {
               ShowError("La entidad de la propiedad es necesario");
               document.getElementById('wait').style.display = "none";
               return;
            }
            
            // Nombre de la propiedad
            if ( form.elements[i].getAttribute("PropertyName") != '' )
               varPropertyName = form.elements[i].getAttribute("PropertyName");
            else
            {
               ShowError("El nombre de la propiedad es necesario");
               document.getElementById('wait').style.display = "none";
               return;
            }
            
            // Order
            varOrder = varCounter;
            varCounter++;
            
            // Required
            if ( document.getElementById(form.elements[i].getAttribute("chkRequired")).checked == true )
               varRequired = "true";
            else
               varRequired = "false";
            
            varProperties += varGroupName + "," + varEntityName + "," + varPropertyName + "," + varOrder + "," + varRequired + ";";
         }
      }
   }
   
   if ( varProperties == "" )
   {
      ShowError("Debe seleccionar por lo menos una propiedad");
      document.getElementById('wait').style.display = "none";
      return;
   }
   
   if ( varProperties.length != 0 )
      varProperties = varProperties.substring(0, varProperties.length-1);
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLSavePropertiesViewNew" + vlrViewName + "&Properties=" + varProperties, callbackSavePropertiesViewNew);
}

function Generate() 
{
   document.getElementById('wait').style.display = "block";
   
   var vlrViewName = "";
   
   if ( document.getElementById("txtViewNameNew") != null && document.getElementById("txtViewNameNew").value != '' )
      vlrViewName = "&ViewName=" + document.getElementById("txtViewNameNew").value;
   
   if ( vlrViewName == '' )
   {
      ShowError("El nombre de la Vista es necesario");
      document.getElementById('wait').style.display = "none";
      return;
   }
   
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLGenerateView" + vlrViewName, callbackGenerateView);
}

function FillPropertyRequired(control, form)
{
   var controlSelected = document.getElementById(control);
   var entityName = "";
   
   if ( controlSelected.checked == true )
   {
      entityName = controlSelected.getAttribute('EntityName');
      
      for(var i = 0; i < form.elements.length; i++)
      {
         if ( form.elements[i].getAttribute('EntityName') == entityName )
         {
            if ( document.getElementById(form.elements[i].getAttribute('chkRequired')).checked == true )
            {
               form.elements[i].checked = true;
            }
         }
      }
   }
}

function ValidatePropertyRequired(form)
{
   var entityName = "";
   
   for(var i = 0; i < form.elements.length; i++)
   {
      if ( form.elements[i].getAttribute('EntityProperty') == "true" )
      {
         if ( form.elements[i].checked == true )
         {
            entityName = form.elements[i].getAttribute('EntityName');
            
            for(var j = 0; j < form.elements.length; j++)
            {
               if ( form.elements[j].getAttribute('EntityName') == entityName )
               {
                  if ( document.getElementById(form.elements[j].getAttribute('chkRequired')).checked == true )
                  {
                     if ( form.elements[j].checked == false )
                     {
                        return false;
                     }
                  }
               }
            }
         }
      }
   }
   
   return true;
}

function Load() 
{
   document.getElementById('wait').style.display = "block";
   makeRequest("../../UIProviders/EntitySystem/EntityViewUIProvider.aspx", "MethodToCall=GetTitanFLViews", callbackViews);
}

function init() 
{
   /****************************************************
    * Creación del formulario para crear un grupo.
    ****************************************************/
   var handleCancelGroupNew = function(e) {
      this.hide();
	}

	var handleOKGroupNew = function(e) {
	   if ( saveNewGroup() == 1 )
	      this.hide();
	}
   
   YAHOO.OnData.TitanFL.container.dlgGroupNew = new YAHOO.widget.SimpleDialog("dlgGroupNew", { visible:false, width: "400px", fixedcenter:true, modal:true, draggable:true });
	YAHOO.OnData.TitanFL.container.dlgGroupNew.cfg.queueProperty("buttons", [ 
																	{ text:"Guardar", handler:handleOKGroupNew, isDefault:true },
																	{ text:"Cancelar", handler:handleCancelGroupNew }
																]);
																
   var listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handleCancelGroupNew ,scope:YAHOO.OnData.TitanFL.container.dlgGroupNew, correctScope:true} );
	YAHOO.OnData.TitanFL.container.dlgGroupNew.cfg.queueProperty("keylisteners", listeners);

	YAHOO.OnData.TitanFL.container.dlgGroupNew.render(document.body);

   /****************************************************
    * Creación del formulario para editar un grupo.
    ****************************************************/
   var handleCancelGroupEdit = function(e) {
      this.hide();
	}

	var handleOKGroupEdit = function(e) {
	   if ( saveEditGroup() == 1 )
	      this.hide();
	}
   
   YAHOO.OnData.TitanFL.container.dlgGroupEdit = new YAHOO.widget.SimpleDialog("dlgGroupEdit", { visible:false, width: "400px", fixedcenter:true, modal:true, draggable:true });
	YAHOO.OnData.TitanFL.container.dlgGroupEdit.cfg.queueProperty("buttons", [ 
																	{ text:"Guardar", handler:handleOKGroupEdit, isDefault:true },
																	{ text:"Cancelar", handler:handleCancelGroupEdit }
																]);
																
   var listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handleCancelGroupEdit ,scope:YAHOO.OnData.TitanFL.container.dlgGroupEdit, correctScope:true} );
	YAHOO.OnData.TitanFL.container.dlgGroupEdit.cfg.queueProperty("keylisteners", listeners);

	YAHOO.OnData.TitanFL.container.dlgGroupEdit.render(document.body);	
	
   /****************************************************
    * MakeRequest para cargar las vistas
    ****************************************************/      
   Load();
}

YAHOO.util.Event.addListener(window, "load", init, "",true);

