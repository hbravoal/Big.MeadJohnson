﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master"
    AutoEventWireup="true"
    CodeBehind="BusinessEntitiesData.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.EntitySystem.BusinessEntitiesData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table id="tblSelectBusinessEntity" runat="server" width="100%" cellpadding="2"
            cellspacing="0" border="0">
            <tr>
                <td bgcolor="LightSteelBlue">
                    <strong>Entidades de Negocio</strong>
                </td>
            </tr>
            <tr>
                <%--<td style="width: 150px">
                    Entidad de Negocio:
                </td>--%>
                <td>
                    <br />
                    <asp:DropDownList ID="ddlViews" runat="server" Width="300px" AutoPostBack="True"
                        OnSelectedIndexChanged="ddlViews_SelectedIndexChanged">
                    </asp:DropDownList>
                    &nbsp;
                    <asp:Button ID="btnCambiarVista" runat="server" Text="Cambiar Entidad de Negocio"
                        Visible="false" OnClick="btnCambiarVista_Click" />
                </td>
            </tr>
        </table>
        <br />
        <div id="divBusqueda" runat="server" style="display: none">
            <table width="100%" cellpadding="2" cellspacing="0" border="0">
                <tr>
                    <td bgcolor="LightSteelBlue">
                        <strong>Búsqueda</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <br />
                        <asp:Xml ID="xmlSearch" runat="server"></asp:Xml>
                        <asp:HiddenField ID="hdfValues" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="Buscar" Visible="false"
                            OnClick="btnSearch_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="padding-top: 10px; height: 100%">
                            <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                                <script type="text/javascript">

                                    function GridCreated(sender, eventArgs) {
                                        var gridContainer = document.getElementById("ctl00_maincontent_rgList_GridData");
                                        if (gridContainer != null)
                                            gridContainer.style.height = "100%";
                                    }
                                </script>
                            </telerik:RadScriptBlock>
                            <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="true"
                                AllowPaging="true"
                                PageSize="10" GridLines="None" Height="100%" Width="100%" OnItemCommand="rgList_ItemCommand"
                                OnNeedDataSource="rgList_NeedDataSource" OnColumnCreated="rgList_ColumnCreated">
                                <HeaderContextMenu EnableAutoScroll="True">
                                </HeaderContextMenu>
                                <MasterTableView TableLayout="Auto" Width="100%">
                                    <Columns>
                                        <telerik:GridTemplateColumn>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" ToolTip="Editar" CommandName="RowEdit"
                                                    CommandArgument='<%#string.Format("ViewName={1}&EntityGuid={0}",  Eval("GUID"),  Eval("VIEWNAME")) %>'
                                                    CausesValidation="false">
                                                    <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif"
                                                        BorderWidth="0px" />
                                                </asp:LinkButton>
                                            </ItemTemplate>
                                            <ItemStyle Width="35px" />
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        Sin registros encontrados
                                    </NoRecordsTemplate>
                                </MasterTableView>
                               <%-- <ClientSettings AllowDragToGroup="false">
                                    <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                    <Selecting AllowRowSelect="false" />
                                    <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                                </ClientSettings>--%>
                            </telerik:RadGrid>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>Ingresar registro en rol:</strong>&nbsp;
                        <asp:DropDownList ID="ddlRoles" runat="server" Width="300px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvRole" runat="server" ControlToValidate="ddlRoles"
                            ErrorMessage="Seleccione un rol para ingresar el registro" Display="None"
                            ValidationGroup="Insert">*</asp:RequiredFieldValidator>
                        &nbsp;<asp:Button ID="btnAdd" runat="server" Text="Ingresar" Visible="false"
                            OnClick="btnAdd_Click"
                            ValidationGroup="Insert" />
                        <asp:ValidationSummary ID="vsInsert" runat="server" ValidationGroup="Insert"
                            ShowMessageBox="true"
                            ShowSummary="false" />
                    </td>
                </tr>
                <tr style="display: none">
                    <td>
                        <strong>La cantidad de registros es:&nbsp;<asp:Label ID="lblCantidadRegistros"
                            runat="server"
                            Text="0"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="imgLoading">
    </div>
    <div id="dlgManageOutOfRangeInstances" style="visibility: hidden;">
        <div id="bd">
            <table width="100%" border="0" cellpadding="10px">
                <tr>
                    <td style="text-align: left; vertical-align: middle">Que comience por:&nbsp;<input
                        type="text" id="txtFindValue" style="width: 50%" value="" />&nbsp;<input
                            type="button" id="btnFindValue" value="Buscar" onclick="findValue()" />
                    </td>
                </tr>
                <tr>
                    <td style="height: 174px;" align="center" valign="middle">
                        <div style="width: 380px" id="divSelectOutofRange">
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript">
        var handleSuccessGetSystemEntityReferentialValues = function (o) {
            response = o.responseText;
            if (response !== undefined) {
                response = response.split("<!")[0];

                if (response != '') {
                    document.getElementById("divSelectOutofRange").innerHTML = response;
                }
            }
        };

        var callbackGetSystemEntityReferentialValues =
{
    success: handleSuccessGetSystemEntityReferentialValues,
    failure: handleFailure
};

        YAHOO.namespace('OnData.TitanFL.container');

        function init() {
            /*************************************************************************
            * Creación del formulario para cuando hay mas una cantidad maxima de datos
            **************************************************************************/
            var handleCancelOutOfRange = function (e) {
                this.hide();
            }

            var handleOKOutOfRange = function (e) {
                SaveOutOfRange();
            }

            YAHOO.OnData.TitanFL.container.dlgOutOfRange = new YAHOO.widget.SimpleDialog("dlgManageOutOfRangeInstances", { visible: false, width: "400px", fixedcenter: true, modal: true, draggable: true });
            YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("buttons", [
																	{ text: "Seleccionar", handler: handleOKOutOfRange, isDefault: true },
																	{ text: "Cancelar", handler: handleCancelOutOfRange }
            ]);

            listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelOutOfRange, scope: YAHOO.OnData.TitanFL.container.dlgOutOfRange, correctScope: true });
            YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("keylisteners", listeners);

            YAHOO.OnData.TitanFL.container.dlgOutOfRange.render(document.body);

            /*************************************************************************
            * FIN del formulario
            *************************************************************************/
        }

        YAHOO.util.Event.addListener(window, "load", init, "", true);

        var systemOutOfRangeGral = "";
        var entityOutOfRangeGral = "";
        var propertyOutOfRangeGral = "";
        var oOutOfRangeGral = null;
        var oHiddenOutOfRangeGral = null;

        function GetManageOutOfRangeInstances(systemOutOfRange, entityOutOfRange, propertyOutOfRange, oOutOfRange, oHiddenOutOfRange) {
            systemOutOfRangeGral = systemOutOfRange;
            entityOutOfRangeGral = entityOutOfRange;
            propertyOutOfRangeGral = propertyOutOfRange;
            oOutOfRangeGral = oOutOfRange;
            oHiddenOutOfRangeGral = oHiddenOutOfRange;

            if (document.getElementById("txtFindValue") != null)
                document.getElementById("txtFindValue").value = '';

            document.getElementById("divSelectOutofRange").innerHTML = "";

            YAHOO.OnData.TitanFL.container.dlgOutOfRange.setHeader("Buscar");
            YAHOO.OnData.TitanFL.container.dlgOutOfRange.show();
        }

        function SaveOutOfRange() {
            var mylist = null;

            if (document.getElementById("lstOutOfRange") != null) {
                mylist = document.getElementById("lstOutOfRange");

                if (mylist.length != 0) {
                    if (mylist.selectedIndex != -1) {
                        oOutOfRangeGral.value = mylist.options[mylist.selectedIndex].text;
                        oHiddenOutOfRangeGral.value = mylist.options[mylist.selectedIndex].value;
                        YAHOO.OnData.TitanFL.container.dlgOutOfRange.hide();
                    }
                    else {
                        alert("Debe seleccionar un elemento");
                    }
                }
                else {
                    alert("No hay elementos para seleccionar");
                }
            }
            else {
                alert("No hay elementos para seleccionar");
            }
        }

        function findValue() {
            var vlrFindValue = "";

            if (document.getElementById("txtFindValue") != null && document.getElementById("txtFindValue").value != '') {
                vlrFindValue = "&FindValue=" + document.getElementById("txtFindValue").value;
                document.getElementById("divSelectOutofRange").innerHTML = "Buscando...";
                makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetSystemEntityReferentialValues&System=" + systemOutOfRangeGral + "&Entity=" + entityOutOfRangeGral + "&PropertyName=" + propertyOutOfRangeGral + vlrFindValue, callbackGetSystemEntityReferentialValues);
            }
            else {
                alert("Digite un patrón de búsqueda");
            }
        }
    </script>
</asp:Content>
