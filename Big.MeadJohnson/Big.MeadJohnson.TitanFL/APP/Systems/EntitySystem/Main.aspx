﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Main.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.EntitySystem.Main" %>

<asp:Content ID="MainContent" ContentPlaceHolderID="maincontent" runat="Server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table width="100%" height="100%" cellpadding="0" cellspacing="0" border="0">
            <tr>
                <td style="width: 30%; vertical-align: top;">
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr>
                            <td style="text-align: center">
                                <h3>
                                    &nbsp;&nbsp;Entidades</h3>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" valign="top">
                                <div id="divListEntities">
                                    &nbsp;</div>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: center;" valign="top">
                                <br />
                                <input id="btnAddEntity" type="button" value="Ingresar" onclick="newEntity()" />
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="width: 70%; vertical-align: top;">
                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                        <tr>
                            <td>
                                <table border="0" width="100%">
                                    <tr>
                                        <td>
                                            <h3>
                                                Configuración de la Entidad</h3>
                                            <hr />
                                        </td>
                                        <td align="right" valign="top">
                                            <span id="divPanelWait">
                                                <img src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando...</span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <div id="divConfigEntity">
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        &nbsp; &nbsp; &nbsp;
        <%--Mensajes de error--%>
        <table border="0">
            <tr>
                <td>
                    <div id="divErrorAJAX">
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="divContentAJAX">
                    </div>
                </td>
            </tr>
        </table>
    </div>
    <%--Formulario para crear entidad--%>
    <div id="dlgEntity" style="visibility: hidden;">
        <div id="bd">
            <table width="100%" style="padding-left: 10px; padding-top: 10px">
                <tr>
                    <td style="width: 45%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Nombre de la Entidad
                    </td>
                    <td style="width: 55%">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtEntityNameNew"
                            maxlength="128" value="" onkeypress="return inCharacters(event)" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 45%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Nombre a mostrar de la Entidad
                    </td>
                    <td style="width: 55%">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtEntityDisplayNameNew"
                            maxlength="128" value="" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 45%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Tipo de la Entidad
                    </td>
                    <td style="width: 55%">
                        <select style="width: 80%" id="ddlEntityTypeNew">
                            <option value="Seleccione...">Seleccione...</option>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <%--Formulario para crear una propiedad--%>
    <div id="dlgPropertyNew" style="visibility: hidden;">
        <div id="bd">
            <table width="100%" border="0" style="padding-left: 10px; padding-top: 10px">
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Nombre de la Propiedad
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtPropertyNameNew"
                            onkeypress="return inCharacters(event)" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Nombre a mostrar de la Propiedad
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtPropertyDisplayNameNew" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Expresión Regular
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtPropertyRexValidatorNew"
                            disabled="disabled" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Formato
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtPropertyDisplayFormatNew" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0" >
                            <tr>
                                <td>
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="margin-bottom: 6px;" id="chkRequiredNew" />
                                            </td>
                                            <td style="vertical-align: text-top;">
                                                Requerido
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="margin-bottom: 6px;" id="chkUniqueNew" />
                                            </td>
                                            <td style="vertical-align: text-top;">
                                                Único
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="margin-bottom: 6px;" id="chkPropertyStateNew" />
                                            </td>
                                            <td style="vertical-align: text-top;">
                                                Estado
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="margin-bottom: 6px;" id="chkPropertyEncryptNew" />
                                            </td>
                                            <td style="vertical-align: text-top;">
                                                Encriptar
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td style="padding-left: 20px">
                                    <table>
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="margin-bottom: 6px;" id="chkPropertyIndexedNew" />
                                            </td>
                                            <td style="vertical-align: text-top;">
                                                Indexar
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="margin-bottom: 6px;" id="chkPropertyTimeStamEnableNew" />
                                            </td>
                                            <td style="vertical-align: text-top;">
                                                Estampilla de cambios
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input type="checkbox" style="margin-bottom: 6px;" id="chkPropertySearchableNew" />
                                            </td>
                                            <td style="vertical-align: text-top;">
                                                Búsqueda
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        <span style="color: Red">*</span>&nbsp;Tipo de Propiedad
                    </td>
                    <td style="width: 60%;">
                        <!-- OJO Cambiar el onchange -->
                        <select style="width: 90%; margin-bottom: 2px;" id="lstPropertyTypeNew" onchange="habilityControlsEntitysNew(this.id)">
                            <option>Seleccione...</option>
                            <option value="0">Cadena de caracteres</option>
                            <option value="1">Cadena larga de caracteres</option>
                            <option value="2">Número</option>
                            <option value="3">Decimal</option>
                            <option value="4">Verdadero/Falso</option>
                            <%--<option value="5">Binary </option>
                     <option value="6">LongBinary </option>--%>
                            <option value="7">Fecha</option>
                            <%--<option value="8">Date </option>
                     <option value="9">Time </option>--%>
                            <option value="10">Moneda</option>
                            <option value="11">Contraseña</option>
                            <option value="12">Entidad</option>
                            <option value="13">Correo electrónico</option>
                            <option value="14">Identidad</option>
                            <option value="15">Ubicación Geográfica</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Nombre de la Entidad fuente
                    </td>
                    <td style="width: 60%;">
                        <select style="width: 90%; margin-bottom: 2px;" id="ddlPropertySourceEntityNameNew"
                            disabled="disabled" onchange="fillPropertySourceEntityDisplayProperty(this.id)">
                            <option>Seleccione...</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Propiedad a mostrar de la Entidad
                    </td>
                    <td style="width: 60%;">
                        <select style="width: 90%; margin-bottom: 2px;" id="ddlPropertySourceEntityDisplayPropertyNew"
                            disabled="disabled">
                            <option>Seleccione...</option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Tipo de selección
                    </td>
                    <td style="width: 60%;">
                        <select style="width: 90%; margin-bottom: 2px;" id="ddlPropertySourceEntitySelectionTypeNew"
                            disabled="disabled">
                            <option>Seleccione...</option>
                            <option value="1">Varios valores</option>
                            <option value="0">Un sólo valor </option>
                        </select>
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Longitud
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtLengthNew" disabled="disabled"
                            onkeypress="return onlyNumbers(event)" maxlength="4" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Precisión
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtPrecisionNew" disabled="disabled"
                            onkeypress="return onlyNumbers(event)" maxlength="2" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Escala
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtScaleNew" disabled="disabled"
                            onkeypress="return onlyNumbers(event)" maxlength="2" />
                    </td>
                </tr>
                <tr>
                    <td style="vertical-align: text-top; width: 40%; text-align: right">
                        Descripción
                    </td>
                    <td style="width: 60%;">
                        <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtToolTipTextNew" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <script type="text/javascript" src="EntitySystem.js"></script>
</asp:Content>
