﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="ProgramsList.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.ProgramsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="adminedit">
        <table cellpadding="4" cellspacing="0" width="100%" border="0">
            <tr>
                <td style="width: 100px">
                    Tipo de Programa:
                </td>
                <td>
                    <asp:DropDownList ID="ddlProgramType" runat="server" DataTextField="Name" DataValueField="Id">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Nombre del Programa:
                </td>
                <td>
                    <asp:TextBox ID="txtProgramName" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td width="100%" colspan="4">
                    <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                        OnItemDataBound="rgList_ItemDataBound">
                        <MasterTableView TableLayout="Auto" Width="100%">
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="EditColumn">
                                    <ItemTemplate>
                                        <a href='ProgramsDetails.aspx?Action=Edit&Guid=<%# Eval("Guid") %>' title="Ver Detalle">
                                            <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                        </a>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" HorizontalAlign="Center" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                                <%--<telerik:GridTemplateColumn UniqueName="DeleteColumn">
                            <ItemTemplate>
                                <a href='ProgramsDetails.aspx?Action=Delete&Guid=<%# Eval("Guid") %>' title="Borrar">
                                    <asp:Image ID="imgDelete" runat="server" ImageUrl="~/APP/Images/icon-delete.gif" BorderWidth="0px" />
                                </a>
                            </ItemTemplate>
                            <ItemStyle Width="35px" HorizontalAlign="Center"/>
                            <HeaderStyle Width="35px" />
                        </telerik:GridTemplateColumn>--%>
                                <telerik:GridBoundColumn HeaderText="Fecha de Creación" DataField="CreationDate"
                                    DataFormatString="{0:yyyy/MM/dd}">
                                    <ItemStyle Width="160px" />
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Activo">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkIsActive" runat="server" Enabled="false" />
                                        <asp:HiddenField ID="hfIsActive" runat="server" Value='<%#Eval("ENABLED") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" />
                                    <HeaderStyle Width="60px" />
                                </telerik:GridTemplateColumn>
                                <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name">
                                </telerik:GridBoundColumn>
                                <%--<telerik:GridBoundColumn HeaderText="Descripción" DataField="Description">
                                </telerik:GridBoundColumn>--%>
                                <telerik:GridBoundColumn HeaderText="Tipo de Programa" DataField="PROGRAMTYPE">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Campañas">
                                    <ItemTemplate>
                                        <a href='CampaignsList.aspx?Guid=<%# Eval("Guid") %>' title="Ver Campañas del Programa">
                                            <asp:Image ID="imgCampaigns" runat="server" ImageUrl="~/APP/Images/gears.gif" BorderWidth="0px" />
                                        </a>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" HorizontalAlign="Center" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                Sin Programas registrados
                            </NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <div style="width: 100%; text-align: right">
                        <br />
                        <asp:Button ID="btnNew" runat="server" OnClientClick="window.location.href='ProgramsDetails.aspx?Action=Insert';return false;"
                            Text="Ingresar" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
