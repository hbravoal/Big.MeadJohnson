﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/Popup.Master" AutoEventWireup="true"
    CodeBehind="LoadPretarget.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.LoadPretarget" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit2" style="display: block">
        <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager ID="radAjaxManager" runat="server" DefaultLoadingPanelID="loadingPanel">
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="ddlRoles">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="ddlRoles" />
                        <telerik:AjaxUpdatedControl ControlID="lstViewFields" />
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <table width="100%" cellpadding="4" cellspacing="0" border="1">
            <tr>
                <td colspan="2" bgcolor="LightSteelBlue">
                    <strong>Cargar archivo de contactos base de la campaña</strong>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Rol:
                </td>
                <td valign="top" colspan="2">
                    <telerik:RadComboBox ID="ddlRoles" runat="server" NoWrap="true" AutoPostBack="True"
                        Width="300px" OnSelectedIndexChanged="ddlRoles_SelectedIndexChanged">
                    </telerik:RadComboBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: right" valign="top">
                    Propiedades a cruzar:
                </td>
                <td valign="top">
                    <telerik:RadListBox ID="lstViewFields" runat="server" CheckBoxes="true" Width="100%">
                        <Items>
                            <telerik:RadListBoxItem Text="Seleccione un rol" Value="" Enabled="false" />
                        </Items>
                    </telerik:RadListBox>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Separador:
                </td>
                <td valign="top" colspan="2">
                    <asp:DropDownList ID="ddlSeparador" runat="server">
                        <asp:ListItem Text="Coma (,)" Value="," Selected="True"></asp:ListItem>
                        <asp:ListItem Text="Pipe (|)" Value="|"></asp:ListItem>
                        <asp:ListItem Text="Punto y coma (;)" Value=";"></asp:ListItem>
                        <asp:ListItem Text="Tabulador" Value="t"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="text-align: right">
                    Archivo:
                </td>
                <td valign="top" colspan="2">
                    <input type="file" id="txtArchivo" onkeydown="javascript:return false;" size="50"
                        runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td>
                    <asp:Button Text="Cargar Contactos" ID="btnLoad" runat="server" OnClick="btnLoad_Click" />&nbsp;
                </td>
            </tr>
        </table>
    </div>
    <script type="text/javascript" language="javascript">

        function ClosePretarget() {
            alert("Archivo procesado.");
            parent.ReloadPage();
        }
    </script>
</asp:Content>
