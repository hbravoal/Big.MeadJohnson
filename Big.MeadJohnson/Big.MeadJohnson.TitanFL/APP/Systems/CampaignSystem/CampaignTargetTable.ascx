﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignTargetTable.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignTargetTable" %>
<link rel="stylesheet" type="text/css" href='<%=Page.ResolveUrl("~/App/Util/modal/subModal.css") %>' />
<script type="text/javascript" src='<%=Page.ResolveUrl("~/App/Util/modal/common.js") %>'></script>
<script type="text/javascript" src='<%=Page.ResolveUrl("~/App/Util/modal/subModal.js") %>'></script>
<%--<ajax:AjaxPanel ID="Ajaxpanel1" runat="server">--%>
<script type="text/javascript" language="javascript">



    function SelectContacts() {
        showPopWin("../QuerySystem/QueryBuilderPopupNew.aspx?Targeting=true", 990, 480, null);
    }

    function ReloadTable(hdnTarsystem, hdnTarmainEntity, hdnTarentitiesList, hdnTarpropertiesList, hdnTarpropertiesListDisplay, hdnTarfiltersList, hdnTarordersList, hdnTarformatList, hdnTarwithDuplicates) {
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarsystem').value = hdnTarsystem;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarmainEntity').value = hdnTarmainEntity;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarentitiesList').value = hdnTarentitiesList;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarpropertiesList').value = hdnTarpropertiesList;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarpropertiesListDisplay').value = hdnTarpropertiesListDisplay;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarfiltersList').value = hdnTarfiltersList;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarordersList').value = hdnTarordersList;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarwithDuplicates').value = hdnTarwithDuplicates;
        document.getElementById('maincontent_CampaignTargetTable1_hdnTarformatList').value = hdnTarformatList;
        document.getElementById(btnReloadCtrl).click();
    }
       
    
</script>
<table cellpadding="5px" cellspacing="0" width="100%" border="0">
    <tr>
        <td>
            <input type="button" value="Seleccionar Contactos" runat="server" id="btnSeletContacts"
                onclick="SelectContacts();" />
            &nbsp;
            <asp:Button ID="btnClean" runat="server" Text="Limpiar" Width="90px" OnClick="btnClean_Click" />
            &nbsp;
            <asp:Button ID="btnReload" runat="server" Text="Recargar" Width="90px" OnClick="btnReload_Click" />
            &nbsp;
            <asp:Button ID="btnExport" runat="server" Text="Exportar" Width="90px" OnClick="btnExport_Click" />
            &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;<br />
            <asp:HiddenField ID="hdnTarsystem" runat="server" />
            <asp:HiddenField ID="hdnTarmainEntity" runat="server" />
            <asp:HiddenField ID="hdnTarentitiesList" runat="server" />
            <asp:HiddenField ID="hdnTarpropertiesList" runat="server" />
            <asp:HiddenField ID="hdnTarpropertiesListDisplay" runat="server" />
            <asp:HiddenField ID="hdnTarfiltersList" runat="server" />
            <asp:HiddenField ID="hdnTarordersList" runat="server" />
            <asp:HiddenField ID="hdnTarformatList" runat="server" />
            <asp:HiddenField ID="hdnTarwithDuplicates" runat="server" />
            &nbsp; &nbsp;&nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <asp:GridView ID="TargetGridView" PageSize="13" AllowPaging="True" runat="server"
                AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None"
                Width="100%" OnSorting="TargetGridView_Sorting" AllowSorting="True" OnPageIndexChanging="TargetGridView_PageIndexChanging"
                OnRowDeleting="TargetGridView_RowDeleting">
                <Columns>
                    <asp:BoundField DataField="Guid" HeaderText="Guid" SortExpression="Guid">
                        <ItemStyle Width="250px" />
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Vista" HeaderText="Vista" SortExpression="Vista">
                        <ItemStyle Width="200px" />
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Dato" HeaderText="Dato" SortExpression="Dato">
                        <HeaderStyle HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:CommandField ShowDeleteButton="True" DeleteText="Eliminar" />
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <EditRowStyle BackColor="#999999" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
        </td>
    </tr>
    <tr>
        <td>
            <hr />
            <strong>Número total de registros:&nbsp;<asp:Label ID="lblCantidadRegistros" runat="server"
                Text=""></asp:Label></strong>
        </td>
    </tr>
</table>
<%--</ajax:AjaxPanel>--%>
<!--#INCLUDE File=~/APP/Util/Modal/modal_divs.aspx-->
