﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="CampaignsDetails.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignsDetails" %>
<%@ Register Src="CampaignOptions.ascx" TagName="CampaignOptions" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">

	<script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>

	<script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>

	<script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>

	<script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>

	<script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>

	<script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>

	<script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>

	<script type="text/javascript" src="../../Scripts/TitanFL.js"></script>

	<link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
	<div id="adminedit">
		<uc1:CampaignOptions ID="CampaignOptions1" CurrentPanel="pnlCampaign" runat="server" />
		<table cellpadding="3" cellspacing="0" width="100%" border="1" bordercolor="lightgrey">
			<tr>
				<td bgcolor="LightSteelBlue">
					<strong>Información de la Campaña</strong>
				</td>
			</tr>
			<tr>
				<td align="left">
					<table cellpadding="4" cellspacing="0" width="100%" border="0">
						<tr>
							<td width="150px" style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Nombre de la Campaña</td>
							<td>
								<asp:TextBox ID="txtName" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ErrorMessage="Se requiere el nombre."
									ControlToValidate="txtName" Display="None">*</asp:RequiredFieldValidator></td>
						</tr>
						<tr>
							<td style="text-align: right">
								Tipo de campaña</td>
							<td>
								<asp:DropDownList ID="ddlCampaignType" runat="server" DataSourceID="CampaignTypeDataSource"
									Width="255px" DataTextField="Name" DataValueField="Id">
								</asp:DropDownList>
								<asp:ObjectDataSource ID="CampaignTypeDataSource" runat="server" SelectMethod="Get"
									TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.CampaignType"></asp:ObjectDataSource>
							</td>
						</tr>
						<tr>
							<td style="text-align: right">
							</td>
							<td>
								<asp:RadioButton ID="rdoEnabledOpen" runat="server" Text="Abierta" GroupName="Enabled"
									Checked="True" />
								<asp:RadioButton ID="rdoEnabledClose" runat="server" Text="Cerrada" GroupName="Enabled" /></td>
						</tr>
						<tr>
							<td style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Descripción</td>
							<td colspan="3">
								<asp:TextBox ID="txtDescription" runat="server" Width="100%" Height="60px" TextMode="MultiLine"
									MaxLength="255"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
									ErrorMessage="Se requiere la descripción." ControlToValidate="txtDescription"
									Display="None">*</asp:RequiredFieldValidator></td>
						</tr>
						<tr>
							<td width="150px" style="text-align: right">
								Fecha de creación</td>
							<td>
								<asp:TextBox ID="txtCreationDate" runat="server" Width="200px" Enabled="false"></asp:TextBox>
							</td>
						</tr>
						<tr>
							<td width="150px" style="text-align: right">
								Fecha de inicio</td>
							<td>
								<table>
									<tr>
										<td>
											<asp:DropDownList ID="ddlYearStartDate" runat="server" onChange="FillDaysStartDate()"
												Width="60px">
											</asp:DropDownList></td>
										<td>
											<asp:DropDownList ID="ddlMonthStartDate" runat="server" onChange="FillDaysStartDate()"
												Width="85px">
											</asp:DropDownList></td>
										<td>
											<asp:DropDownList ID="ddlDayStartDate" runat="server" Width="50px" onChange="FillValueStartDate()">
											</asp:DropDownList></td>
									</tr>
								</table>
								<asp:HiddenField ID="hfFechaInicio" runat="server" />
							</td>
							<td width="150px" style="text-align: right">
								Fecha de finalización</td>
							<td>
								<table>
									<tr>
										<td>
											<asp:DropDownList ID="ddlYearEndDate" runat="server" onChange="FillDaysEndDate()"
												Width="60px">
											</asp:DropDownList></td>
										<td>
											<asp:DropDownList ID="ddlMonthEndDate" runat="server" onChange="FillDaysEndDate()"
												Width="85px">
											</asp:DropDownList></td>
										<td>
											<asp:DropDownList ID="ddlDayEndDate" runat="server" Width="50px" onChange="FillValueEndDate()">
											</asp:DropDownList></td>
									</tr>
								</table>
								<asp:HiddenField ID="hfFechaFin" runat="server" />
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td bgcolor="LightSteelBlue">
					<strong>Objetivos</strong>
				</td>
			</tr>
			<tr>
				<td align="left">
					<table cellpadding="4" cellspacing="0" width="100%" border="0">
						<tr>
							<td width="150px" style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Objetivo general</td>
							<td>
								<asp:TextBox ID="txtGeneralGoal" runat="server" Width="100%" Height="60px" TextMode="MultiLine"
									MaxLength="1024"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorGeneralGoal" runat="server"
									ErrorMessage="Se requiere el objetivo general." ControlToValidate="txtGeneralGoal"
									Display="None">*</asp:RequiredFieldValidator></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td bgcolor="LightSteelBlue">
					<strong>Información General</strong>
				</td>
			</tr>
			<tr>
				<td align="left">
					<table cellpadding="4" cellspacing="0" width="100%" border="0">
						<tr>
							<td width="150px" style="text-align: right; height: 89px;">
								<span style="color: Red">*</span>&nbsp;Información general</td>
							<td style="height: 89px">
								<asp:TextBox ID="txtGeneralInformation" runat="server" Width="100%" Height="60px"
									TextMode="MultiLine" MaxLength="1024"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorGeneralInformation" runat="server"
									ErrorMessage="Se requiere la información general." ControlToValidate="txtGeneralInformation"
									Display="None">*</asp:RequiredFieldValidator></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td bgcolor="LightSteelBlue">
					<strong>Contacto</strong>
				</td>
			</tr>
			<tr>
				<td align="left">
					<table cellpadding="4" cellspacing="0" width="100%" border="0">
						<tr>
							<td width="150px" style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Nombre del contacto</td>
							<td>
								<asp:TextBox ID="txtContactName" runat="server" Width="250px" MaxLength="128"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorContactName" runat="server"
									ErrorMessage="Se requiere el nombre del contacto." ControlToValidate="txtContactName"
									Display="None">*</asp:RequiredFieldValidator></td>
							<td width="150px" style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Apellidos del contacto</td>
							<td>
								<asp:TextBox ID="txtContactLastName" runat="server" Width="250px" MaxLength="128"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorContactLastName" runat="server"
									ErrorMessage="Se requiere el(los) apellido(s) del contacto." ControlToValidate="txtContactLastName"
									Display="None">*</asp:RequiredFieldValidator></td>
						</tr>
						<tr>
							<td style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Email del contacto</td>
							<td>
								<asp:TextBox ID="txtContactEmail" runat="server" Width="250px" MaxLength="255"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorContactEmail" runat="server"
									ErrorMessage="Se requiere el Email del contacto." ControlToValidate="txtContactEmail"
									Display="None">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="RegularExpressionContactEmail" runat="server"
									ErrorMessage="El Email del contacto es incorrecto." ControlToValidate="txtContactEmail"
									Display="None" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">*</asp:RegularExpressionValidator></td>
							<td style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Descripción del contacto</td>
							<td>
								<asp:TextBox ID="txtContactDescription" runat="server" Width="250px" MaxLength="512"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorContactDescription" runat="server"
									ErrorMessage="Se requiere una descripción del contacto." ControlToValidate="txtContactDescription"
									Display="None">*</asp:RequiredFieldValidator></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td bgcolor="LightSteelBlue">
					<strong>Indicadores</strong>
				</td>
			</tr>
			<tr>
				<td align="left">
					<table cellpadding="4" cellspacing="0" width="100%" border="0">
						<tr>
							<td width="150px" style="text-align: right">
								<span style="color: Red">*</span>&nbsp;Presupuesto</td>
							<td>
								<asp:TextBox ID="txtBudget" runat="server" Width="250px" Style="text-align: right"
									MaxLength="15"></asp:TextBox>
								<asp:RequiredFieldValidator ID="RequiredFieldValidatorBudget" runat="server" ErrorMessage="Se requiere un presupuesto."
									ControlToValidate="txtBudget" Display="None">*</asp:RequiredFieldValidator>
								<asp:RegularExpressionValidator ID="RegularExpressionValidatorBudget" runat="server"
									ControlToValidate="txtBudget" Display="None" ErrorMessage="El presupuesto debe ser un número."
									ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator></td>
							<td style="text-align: right">
								Costo de la Campaña</td>
							<td>
								<asp:TextBox ID="txtRealCost" runat="server" Width="250px" Enabled="false" Style="text-align: right"></asp:TextBox></td>
						</tr>
						<tr>
							<td style="text-align: right" width="150px">
								Cumplimiento general</td>
							<td>
								<asp:TextBox ID="txtGeneralFulfillment" runat="server" Width="100px" Style="text-align: right"
									MaxLength="15" ReadOnly="true"></asp:TextBox>&nbsp;%
								<asp:RegularExpressionValidator ID="RegularExpressionValidatorGeneralFulfillment"
									runat="server" ControlToValidate="txtGeneralFulfillment" Display="None" ErrorMessage="El cumplimiento general debe ser un número."
									ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator></td>
							<td style="text-align: right">
								Cumplimiento presupuestado</td>
							<td>
								<asp:TextBox ID="txtBudgetFulfillment" runat="server" Width="250px" Style="text-align: right"
									MaxLength="15"></asp:TextBox>
								<asp:RegularExpressionValidator ID="regularexpressionvalidatorbudgetfulfillment"
									runat="server" ControlToValidate="txtbudgetfulfillment" Display="none" ErrorMessage="El cumplimiento presupuestado debe ser un número."
									ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator></td>
						</tr>
						<tr>
							<td style="text-align: right" width="150px">
								Costo/Contacto</td>
							<td>
								<asp:TextBox ID="txtCostContact" runat="server" Width="250px" Style="text-align: right"
									MaxLength="15"></asp:TextBox>
								<asp:RegularExpressionValidator ID="RegularExpressionValidatorCostContact" runat="server"
									ControlToValidate="txtCostContact" Display="none" ErrorMessage="El costo/contacto presupuestado debe ser un número."
									ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator></td>
							<td style="text-align: right">
								Costos óptimos</td>
							<td>
								<asp:TextBox ID="txtCostOptimal" runat="server" Width="250px" Style="text-align: right"
									MaxLength="15"></asp:TextBox>
								<asp:RegularExpressionValidator ID="RegularExpressionValidatorCostOptimal" runat="server"
									ControlToValidate="txtCostOptimal" Display="none" ErrorMessage="El costo óptimo debe ser un número."
									ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator></td>
						</tr>
						<tr>
							<td style="text-align: right" width="150px">
								Cantidad contactos esperados</td>
							<td>
								<asp:TextBox ID="txtAmountContactsHoped" runat="server" Width="250px" Style="text-align: right"
									onkeypress="return onlyNumbersTitanFL(event)" MaxLength="8"></asp:TextBox></td>
							<td style="text-align: right">
								Cantidad contactos efectivos</td>
							<td>
								<asp:TextBox ID="txtAmountContactsEffective" runat="server" Width="250px" Style="text-align: right"
									onkeypress="return onlyNumbersTitanFL(event)" MaxLength="8"></asp:TextBox></td>
						</tr>
						<tr>
							<td width="150px" style="text-align: right">
								Participación de mercados</td>
							<td>
								<asp:TextBox ID="txtMarketParticipation" runat="server" Width="250px" Style="text-align: right"
									onkeypress="return onlyNumbersTitanFL(event)" MaxLength="8"></asp:TextBox></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td align="center">
					<asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" OnClientClick="if(!ValidateControlsCampaign()) return false" />
					<asp:Button ID="btnDelete" runat="server" Text="Eliminar" OnClick="btnDelete_Click"
						OnClientClick='return confirm("¿Realmente desea eliminar el registro?")' CausesValidation="False" />
					<asp:Button ID="btnCancel" runat="server" Text="Cancelar" OnClick="btnCancel_Click"
						CausesValidation="False" />
				</td>
			</tr>
		</table>
		<asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
			ShowSummary="False" />
		<br />
	</div>
	
	<script type="text/javascript">
	    function FillValueStartDate() {
	        var mylistYear = document.getElementById('<%=ddlYearStartDate.ClientID%>');
	        var mylistMonth = document.getElementById('<%=ddlMonthStartDate.ClientID%>');
	        var mylistDay = document.getElementById('<%=ddlDayStartDate.ClientID%>');
	        var year = mylistYear.options[mylistYear.selectedIndex].value;
	        var month = mylistMonth.options[mylistMonth.selectedIndex].value;
	        var day = mylistDay.options[mylistDay.selectedIndex].value;

	        document.getElementById('<%=hfFechaInicio.ClientID%>').value = year + "/" + month + "/" + day;
	    }

	    function FillValueEndDate() {
	        var mylistYear = document.getElementById('<%=ddlYearEndDate.ClientID%>');
	        var mylistMonth = document.getElementById('<%=ddlMonthEndDate.ClientID%>');
	        var mylistDay = document.getElementById('<%=ddlDayEndDate.ClientID%>');
	        var year = mylistYear.options[mylistYear.selectedIndex].value;
	        var month = mylistMonth.options[mylistMonth.selectedIndex].value;
	        var day = mylistDay.options[mylistDay.selectedIndex].value;

	        document.getElementById('<%=hfFechaFin.ClientID%>').value = year + "/" + month + "/" + day;
	    }

	    function FillDaysStartDate() {
	        var mylistYear = document.getElementById('<%=ddlYearStartDate.ClientID%>');
	        var mylistMonth = document.getElementById('<%=ddlMonthStartDate.ClientID%>');

	        var year = mylistYear.options[mylistYear.selectedIndex].value;
	        var month = mylistMonth.options[mylistMonth.selectedIndex].value;

	        CleanSelected('<%=ddlDayStartDate.ClientID%>');

	        var sizeDays = 0;

	        switch (month) {
	            case '01':
	            case '03':
	            case '05':
	            case '07':
	            case '08':
	            case '10':
	            case '12':
	                sizeDays = 31;
	                break;
	            case '04':
	            case '06':
	            case '09':
	            case '11':
	                sizeDays = 30;
	                break;
	            case '02':
	                if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
	                    sizeDays = 29;
	                else
	                    sizeDays = 28;
	                break;
	        }

	        var elements = "";

	        for (var i = 1; i <= sizeDays; i++) {
	            if (i < 10)
	                elements += "0" + i + "|" + "0" + i + ",";
	            else
	                elements += i + "|" + i + ",";
	        }

	        if (elements.length != 0)
	            elements = elements.substring(0, elements.length - 1);

	        fillSelect(elements, '<%=ddlDayStartDate.ClientID%>');
	        document.getElementById('<%=hfFechaInicio.ClientID%>').value = year + "/" + month + "/01";
	    }

	    function FillDaysEndDate() {
	        var mylistYear = document.getElementById('<%=ddlYearEndDate.ClientID%>');
	        var mylistMonth = document.getElementById('<%=ddlMonthEndDate.ClientID%>');

	        var year = mylistYear.options[mylistYear.selectedIndex].value;
	        var month = mylistMonth.options[mylistMonth.selectedIndex].value;

	        CleanSelected('<%=ddlDayEndDate.ClientID%>');

	        var sizeDays = 0;

	        switch (month) {
	            case '01':
	            case '03':
	            case '05':
	            case '07':
	            case '08':
	            case '10':
	            case '12':
	                sizeDays = 31;
	                break;
	            case '04':
	            case '06':
	            case '09':
	            case '11':
	                sizeDays = 30;
	                break;
	            case '02':
	                if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
	                    sizeDays = 29;
	                else
	                    sizeDays = 28;
	                break;
	        }

	        var elements = "";

	        for (var i = 1; i <= sizeDays; i++) {
	            if (i < 10)
	                elements += "0" + i + "|" + "0" + i + ",";
	            else
	                elements += i + "|" + i + ",";
	        }

	        if (elements.length != 0)
	            elements = elements.substring(0, elements.length - 1);

	        fillSelect(elements, '<%=ddlDayEndDate.ClientID%>');
	        document.getElementById('<%=hfFechaFin.ClientID%>').value = year + "/" + month + "/01";
	    }

	    function CleanSelected(control) {
	        var elements = "";
	        for (var i = (document.getElementById(control).length - 1); i != -1; i--) {
	            elements += document.getElementById(control).options[0].value + "|" + document.getElementById(control).options[0].text + ",";
	            document.getElementById(control).remove(0);
	        }

	        if (elements.length != 0)
	            elements = elements.substring(0, elements.length - 1);

	        return elements;
	    }

	    function fillSelect(stringValues, idControl) {
	        var optionsList = stringValues.split(",");
	        var elSel = null;
	        var item = null;

	        if (document.getElementById(idControl) != null)
	            elSel = document.getElementById(idControl);

	        if (elSel != null) {
	            for (var i = 0; i < optionsList.length; i++) {
	                item = optionsList[i].split("|");
	                var elOptNew = document.createElement('option');
	                elOptNew.value = item[0];
	                elOptNew.text = item[1];

	                try {
	                    elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
	                }
	                catch (ex) {
	                    elSel.add(elOptNew); // IE only
	                }
	            }
	        }
	    }

	    function ValidateControlsCampaign() {
	        var isValid = true;
	        var errorMessageOther = "";
	        var dateStart = new Date();
	        var dateEnd = new Date();

	        var mylistYearStart = document.getElementById('<%=ddlYearStartDate.ClientID%>');
	        var mylistMonthStart = document.getElementById('<%=ddlMonthStartDate.ClientID%>');
	        var mylistDayStart = document.getElementById('<%=ddlDayStartDate.ClientID%>');

	        var yearStart = mylistYearStart.options[mylistYearStart.selectedIndex].value;
	        var monthStart = mylistMonthStart.options[mylistMonthStart.selectedIndex].value;
	        var dayStart = mylistDayStart.options[mylistDayStart.selectedIndex].value;

	        var mylistYearEnd = document.getElementById('<%=ddlYearEndDate.ClientID%>');
	        var mylistMonthEnd = document.getElementById('<%=ddlMonthEndDate.ClientID%>');
	        var mylistDayEnd = document.getElementById('<%=ddlDayEndDate.ClientID%>');

	        var yearEnd = mylistYearEnd.options[mylistYearEnd.selectedIndex].value;
	        var monthEnd = mylistMonthEnd.options[mylistMonthEnd.selectedIndex].value;
	        var dayEnd = mylistDayEnd.options[mylistDayEnd.selectedIndex].value;

	        dateStart.setFullYear(yearStart, monthStart - 1, dayStart);
	        dateEnd.setFullYear(yearEnd, monthEnd - 1, dayEnd);

	        if (dateStart > dateEnd) {
	            errorMessageOther += "- La fecha de finalización debe ser mayor a la fecha de inicio<br/>";
	            isValid = false;
	        }

	        if (document.getElementById('<%= txtDescription.ClientID %>').value.length > 255) {
	            errorMessageOther += "- La descripción no puede tener más de 255 caracteres<br/>";
	            isValid = false;
	        }

	        if (document.getElementById('<%= txtGeneralGoal.ClientID %>').value.length > 1024) {
	            errorMessageOther += "- El objetivo general no puede tener más de 1024 caracteres<br/>";
	            isValid = false;
	        }

	        if (document.getElementById('<%= txtGeneralInformation.ClientID %>').value.length > 1024) {
	            errorMessageOther += "- La información general no puede tener más de 1024 caracteres<br/>";
	            isValid = false;
	        }

	        if (!isValid) {
	            ShowError(errorMessageOther);
	            return false;
	        }
	        else {
	            return true;
	        }
	    }

	    function onlyDecimals(varNumber) {
	        patron = /^\d*\,?\d*$/;
	        return patron.test(varNumber);
	    }

	    function onlyNumbersTitanFL(e) {
	        tecla = (document.all) ? e.keyCode : e.which;

	        if (tecla == 8)
	            return true;

	        if (tecla == 0)
	            return true;

	        patron = /[0-9]/;
	        te = String.fromCharCode(tecla);
	        return patron.test(te);
	    }
	</script>

</asp:Content>

