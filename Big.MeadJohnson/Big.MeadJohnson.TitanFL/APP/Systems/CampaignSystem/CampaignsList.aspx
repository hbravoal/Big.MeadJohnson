﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="CampaignsList.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignsList" %>

<%@ Register Src="~/Controls/ControlFecha.ascx" TagName="ControlFecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="adminedit">
        <table cellpadding="4" cellspacing="0" width="100%" border="0">
            <tr>
                <td colspan="4" bgcolor="lightsteelblue">
                    <strong>
                        <asp:Label ID="txtCampaignProgram" runat="server" Text="Programa" Font-Bold="True"></asp:Label></strong>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Tipo de Campaña:
                </td>
                <td>
                    <asp:DropDownList ID="ddlCampaignTypes" runat="server" DataTextField="Name" DataValueField="Id">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Nombre de la Campaña:
                </td>
                <td>
                    <asp:TextBox ID="txtName" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    Fecha inicial
                </td>
                <td>
                    <uc1:ControlFecha ID="cfInitialDate" runat="server" AllowNulls="true" />
                </td>
            </tr>
            <tr>
                <td>
                    Fecha final
                </td>
                <td>
                    <uc1:ControlFecha ID="cfEndDate" runat="server" AllowNulls="true" />
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td width="100%" colspan="4">
                    <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource">
                        <MasterTableView TableLayout="Auto" Width="100%">
                            <Columns>
                                <telerik:GridTemplateColumn UniqueName="EditColumn">
                                    <ItemTemplate>
                                        <a href='CampaignsDetails.aspx?Action=Edit&Guid=<%# Eval("Guid") %>&IdCampaignProgram=<%# Eval("IdCampaignProgram") %>'
                                            title="Ver Detalle">
                                            <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                        </a>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" HorizontalAlign="Center" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                                <%--<telerik:GridTemplateColumn UniqueName="DeleteColumn">
                            <ItemTemplate>
                                <a href='CampaignsDetails.aspx?Action=Delete&Guid=<%# Eval("Guid") %>&IdCampaignProgram=<%# Eval("IdCampaignProgram") %>' title="Borrar">
                                    <asp:Image ID="imgDelete" runat="server" ImageUrl="~/APP/Images/icon-delete.gif" BorderWidth="0px" />
                                </a>
                            </ItemTemplate>
                            <ItemStyle Width="35px" HorizontalAlign="Center" />
                            <HeaderStyle Width="35px" />
                        </telerik:GridTemplateColumn>--%>
                                <telerik:GridBoundColumn HeaderText="Nombre" DataField="Name">
                                </telerik:GridBoundColumn>
                                <%--<telerik:GridBoundColumn HeaderText="Descripción" DataField="Description">
                                </telerik:GridBoundColumn>--%>
                                <telerik:GridBoundColumn HeaderText="Tipo de Campaña" DataField="CampaignType">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Fecha de Creación" DataField="CreationDate"
                                    DataFormatString="{0:yyyy/MM/dd}">
                                    <ItemStyle Width="160px" />
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Fecha de Inicio" DataField="StartDate" DataFormatString="{0:yyyy/MM/dd}">
                                    <ItemStyle Width="160px" />
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn HeaderText="Actividades">
                                    <ItemTemplate>
                                        <a href='ActivitiesList.aspx?Action=List&Guid=<%# Eval("Guid") %>&IdCampaignProgram=<%# Eval("IdCampaignProgram") %>'
                                            title="Ver actividades de la campaña">
                                            <asp:Image ID="imgCampaigns" runat="server" ImageUrl="~/APP/Images/gears.gif" BorderWidth="0px" />
                                        </a>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" HorizontalAlign="Center" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                Sin campañas registradas
                            </NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <div style="width: 100%; text-align: right">
                        <br />
                        <asp:Button ID="btnNew" runat="server" Text="Ingresar" />&nbsp;<asp:Button ID="btnReturn"
                            runat="server" Text="Volver" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
