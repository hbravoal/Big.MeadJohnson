﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="ActivitiesDetails.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.ActivitiesDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="3" cellspacing="0" width="100%" border="0">
            <tr>
                <td bgcolor="LightSteelBlue">
                    <strong>Actividades</strong>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td style="text-align: right" width="100px">
                                <span style="color: Red">*</span>&nbsp;Nombre
                            </td>
                            <td>
                                <asp:TextBox ID="txtName" runat="server" MaxLength="50" Width="250px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ErrorMessage="Se requiere el nombre."
                                    ControlToValidate="txtName" Display="None">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" width="100px">
                                <span style="color: Red">*</span>&nbsp;Descripción
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescription" runat="server" Height="60px" TextMode="MultiLine"
                                    Width="80%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
                                    ErrorMessage="Se requiere la descripción." ControlToValidate="txtDescription"
                                    Display="None">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" width="100px">
                            </td>
                            <td>
                                <asp:CheckBox ID="chkMeanUnfolding" runat="server" Text="Medio de despliegue" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" width="100px">
                                Fecha de inicio
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlYearStartDate" runat="server" onChange="FillDaysStartDate()"
                                                Width="60px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlMonthStartDate" runat="server" onChange="FillDaysStartDate()"
                                                Width="85px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDayStartDate" runat="server" Width="50px" onChange="FillValueStartDate()">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="hfFechaInicio" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" width="100px">
                                Fecha de finalización
                            </td>
                            <td>
                                <table>
                                    <tr>
                                        <td>
                                            <asp:DropDownList ID="ddlYearEndDate" runat="server" onChange="FillDaysEndDate()"
                                                Width="60px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlMonthEndDate" runat="server" onChange="FillDaysEndDate()"
                                                Width="85px">
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="ddlDayEndDate" runat="server" Width="50px" onChange="FillValueEndDate()">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <asp:HiddenField ID="hfFechaFin" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" width="100px">
                                Tipo de actividad
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlActivityType" runat="server" Width="255px" DataSourceID="ActivityTypeDataSource"
                                    DataTextField="Name" DataValueField="Id">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right" width="100px">
                                Costo real
                            </td>
                            <td>
                                <asp:TextBox ID="txtRealCost" runat="server" Width="250px" MaxLength="15"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidatorRealCost" runat="server"
                                    ControlToValidate="txtRealCost" Display="none" ErrorMessage="El costo real debe ser un número."
                                    ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left;" width="120px">
                                <span style="color: Red">*</span>&nbsp;Punto de Contacto
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlContactPoints" runat="server" DataTextField="Name" DataValueField="Id">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorProgramType" runat="server"
                                    ControlToValidate="ddlContactPoints" ErrorMessage="El punto de contacto es requerido."
                                    Display="None" InitialValue="">*</asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <asp:ObjectDataSource ID="ActivityTypeDataSource" runat="server" SelectMethod="GetActivityTypes"
            TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.ActivityType"></asp:ObjectDataSource>
        <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
        <br />
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" OnClientClick="if(!ValidateControls()) return false" />
                </td>
                <td>
                    <asp:Button ID="btnDelete" runat="server" Text="Eliminar" OnClientClick='return confirm("¿Realmente desea eliminar el registro?")'
                        CausesValidation="False" OnClick="btnDelete_Click" />
                </td>
                <td>
                    <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CausesValidation="False"
                        OnClick="btnCancel_Click" />
                </td>
            </tr>
        </table>
        <br />
        <br />
        <div style="visibility: hidden">
            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                <tr>
                    <td bgcolor="LightSteelBlue">
                        <strong>Preguntas</strong>
                    </td>
                </tr>
            </table>
            <br />
            <br />
            <asp:ObjectDataSource ID="ActivityResponseTypesDataSource" runat="server" SelectMethod="GetActivityResponseTypes"
                TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.ActivityResponseType">
                <SelectParameters>
                    <asp:ControlParameter ControlID="ddlActivityType" Name="id" PropertyName="SelectedValue"
                        Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:GridView ID="GridViewActitvityResponseTypes" runat="server" AutoGenerateColumns="False"
                DataSourceID="ActivityResponseTypesDataSource" CellPadding="4" ForeColor="#333333"
                GridLines="None" Width="80%">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Pregunta" SortExpression="Name" />
                    <asp:BoundField DataField="Description" HeaderText="Descripci&#243;n" SortExpression="Description" />
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <EditRowStyle BackColor="#999999" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
            <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td height="20" align="right" valign="Top">
                        <span id="wait">
                            <img src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando...</span>
                    </td>
                </tr>
            </table>
            <asp:ObjectDataSource ID="TempTargetDataSource" runat="server" SelectMethod="GetCampaignTargets"
                TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.CampaignTarget">
                <SelectParameters>
                    <asp:QueryStringParameter Name="guidCampaign" QueryStringField="IdCampaign" Type="String" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <asp:GridView ID="GridViewResponses" runat="server" AutoGenerateColumns="False" DataSourceID="TempTargetDataSource"
                CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" AllowPaging="true"
                PageSize="4">
                <Columns>
                    <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Name" HeaderStyle-Width="20%">
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="Preguntas" HeaderStyle-Width="72%">
                        <ItemTemplate>
                            <table cellpadding="0" cellspacing="0" width="100%">
                                <tr>
                                    <td colspan="2">
                                        <table cellpadding="0" cellspacing="0" width="100%">
                                            <tr>
                                                <td>
                                                    <input type="button" id="btnResponses_<%# Eval("Guid") %>" value="Respuestas" onclick="GetResponsesByTarget('<%# Eval("Guid") %>', '<%# Guid %>', '<%# ddlActivityType.SelectedValue  %>')" />
                                                </td>
                                                <td>
                                                    <input type="hidden" id="hidden_<%# Eval("Guid") %>" image="Image" guidtarget="<%# Eval("Guid") %>"
                                                        idcampaignactivity="<%# Guid %>" idactivitytype="<%# ddlActivityType.SelectedValue  %>"
                                                        style="display: none;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <div id="div_<%# Eval("Guid") %>" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table>
                                            <tr>
                                                <td>
                                                    <input type="button" id="btnSaveResponse_<%# Eval("Guid") %>" value="Guardar" onclick="SaveResponses(this.form, '<%# Eval("Guid") %>', '<%# Guid %>', '<%# ddlActivityType.SelectedValue  %>')"
                                                        style="display: none;" />
                                                </td>
                                                <td>
                                                    <input type="button" id="btn_<%# Eval("Guid") %>" value="Cerrar" onclick="HideResponses('<%# Eval("Guid") %>')"
                                                        style="display: none;" />
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Respondido" HeaderStyle-Width="8%">
                        <ItemTemplate>
                            <img id="image_<%# Eval("Guid") %>" src="../../Images/required.gif" style="display: none;"></img>
                            <div id="divCount_<%# Eval("Guid") %>" style="display: block;">
                            </div>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" />
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <EditRowStyle BackColor="#999999" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            </asp:GridView>
        </div>
        <script type="text/javascript">
            function FillValueStartDate() {
                var mylistYear = document.getElementById('<%=ddlYearStartDate.ClientID%>');
                var mylistMonth = document.getElementById('<%=ddlMonthStartDate.ClientID%>');
                var mylistDay = document.getElementById('<%=ddlDayStartDate.ClientID%>');
                var year = mylistYear.options[mylistYear.selectedIndex].value;
                var month = mylistMonth.options[mylistMonth.selectedIndex].value;
                var day = mylistDay.options[mylistDay.selectedIndex].value;

                document.getElementById('<%=hfFechaInicio.ClientID%>').value = year + "/" + month + "/" + day;
            }

            function FillValueEndDate() {
                var mylistYear = document.getElementById('<%=ddlYearEndDate.ClientID%>');
                var mylistMonth = document.getElementById('<%=ddlMonthEndDate.ClientID%>');
                var mylistDay = document.getElementById('<%=ddlDayEndDate.ClientID%>');
                var year = mylistYear.options[mylistYear.selectedIndex].value;
                var month = mylistMonth.options[mylistMonth.selectedIndex].value;
                var day = mylistDay.options[mylistDay.selectedIndex].value;

                document.getElementById('<%=hfFechaFin.ClientID%>').value = year + "/" + month + "/" + day;
            }

            var handleSuccessGetResponsesByTarget = function (o) {
                response = o.responseText;
                if (response !== undefined) {
                    response = response.split("<!")[0];
                    document.getElementById("div_" + o.argument[0]).innerHTML = response;
                    document.getElementById("div_" + o.argument[0]).style.display = "block";
                    document.getElementById("btn_" + o.argument[0]).style.display = "block";
                    document.getElementById("btnSaveResponse_" + o.argument[0]).style.display = "block";
                    document.getElementById("btnResponses_" + o.argument[0]).style.display = "none";
                    //document.getElementById("image_" + o.argument[0]).style.display = "none";
                    document.getElementById('wait').style.display = "none";
                }
            };

            var handleSuccessSaveResponse = function (o) {
                response = o.responseText;
                if (response !== undefined) {
                    response = response.split("<!")[0];
                    //ShowError(response);
                    AllResponse();
                    document.getElementById('wait').style.display = "none";
                }
            };

            var handleSuccessAllResponse = function (o) {
                response = o.responseText;
                if (response !== undefined) {
                    response = response.split("<!")[0];

                    if (response != "") {
                        var imagesList = response.split(";");
                        var imageInfo = "";

                        for (var i = 0; i < imagesList.length; i++) {
                            imageInfo = imagesList[i].split(",");

                            if (imageInfo[1] == "True") {
                                document.getElementById("divCount_" + imageInfo[0]).style.display = "none";
                                document.getElementById("image_" + imageInfo[0]).style.display = "block";
                            }
                            else {
                                document.getElementById("image_" + imageInfo[0]).style.display = "none";
                                document.getElementById("divCount_" + imageInfo[0]).innerHTML = imageInfo[2];
                            }
                        }
                    }
                    document.getElementById('wait').style.display = "none";
                }
            };

            var args = [''];
            var callbackGetResponsesByTarget =
      {
          success: handleSuccessGetResponsesByTarget,
          failure: handleFailureCurrent,
          argument: args
      }

            var callbackSaveResponse =
      {
          success: handleSuccessSaveResponse,
          failure: handleFailureCurrent
      }

            var callbackAllResponse =
      {
          success: handleSuccessAllResponse,
          failure: handleFailureCurrent
      }

            var handleFailureCurrent = function (o) {
                if (o.responseText !== undefined) {
                    ShowError("Error: " + o.responseText);
                }
            };

            function makeRequest(sUrl, postData, callback) {
                var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);
            }

            function GetResponsesByTarget(guidTarget, idCampaignActivity, idActivityType) {
                document.getElementById('wait').style.display = "block";
                args[0] = guidTarget;
                makeRequest("../../UIProviders/CampaignSystem/EntityCampaignUIProvider.aspx", "MethodToCall=GetResponsesByTarget&GuidTarget=" + guidTarget + "&IdCampaignActivity=" + idCampaignActivity + "&IdActivityType=" + idActivityType, callbackGetResponsesByTarget);
            }

            function HideResponses(guid) {
                document.getElementById("div_" + guid).style.display = "none";
                document.getElementById("btn_" + guid).style.display = "none";
                document.getElementById("btnSaveResponse_" + guid).style.display = "none";
                document.getElementById("btnResponses_" + guid).style.display = "block";
                document.getElementById("image_" + guid).style.display = "block";
                AllResponse();
            }

            function SaveResponses(form, guidTarget, idCampaignActivity, idActivityType) {
                document.getElementById('wait').style.display = "block";

                var i = 0; j = 0;
                var varResponses = "";
                var hayRespuesta = false;

                for (i = 0; i < form.elements.length; i++) {
                    if (form.elements[i].getAttribute("Target") == guidTarget) {
                        hayRespuesta = false;

                        for (j = 0; j < document.getElementsByName(form.elements[i].getAttribute("Response")).length; j++) {
                            if (document.getElementsByName(form.elements[i].getAttribute("Response"))[j].checked) {
                                varResponses += form.elements[i].getAttribute("ActivityResponseType") + "," + document.getElementsByName(form.elements[i].getAttribute("Response"))[j].value + ",";
                                hayRespuesta = true;
                                break;
                            }
                        }

                        if (hayRespuesta) {
                            var mylist = document.getElementById(form.elements[i].getAttribute("IdARState"));
                            var valueList = mylist.options[mylist.selectedIndex].value;

                            if (valueList != "") {
                                varResponses += valueList + ",";
                            }
                            else {
                                varResponses += ",";
                            }

                            var descriptionARS = document.getElementById(form.elements[i].getAttribute("DescriptionARState")).value;

                            if (!vacio(descriptionARS)) {
                                varResponses += descriptionARS + ";";
                            }
                            else {
                                varResponses += ";";
                            }

                            if (valueList != "" && vacio(descriptionARS)) {
                                ShowError("A la pregunta '" + form.elements[i].getAttribute("NameQuestion") + "' se le ha seleccionado un estado, debe ingresar entonces una descripción");
                                AllResponse();
                                document.getElementById('wait').style.display = "none";
                                return;
                            }

                            if (!vacio(descriptionARS) && valueList == "") {
                                ShowError("A la pregunta '" + form.elements[i].getAttribute("NameQuestion") + "' se le ha ingresado una descripción, debe seleccionar entonces un estado");
                                AllResponse();
                                document.getElementById('wait').style.display = "none";
                                return;
                            }
                        }
                    }
                }

                if (varResponses.length != 0)
                    varResponses = varResponses.substring(0, varResponses.length - 1);

                makeRequest("../../UIProviders/CampaignSystem/EntityCampaignUIProvider.aspx", "MethodToCall=SaveResponse&GuidTarget=" + guidTarget + "&IdCampaignActivity=" + idCampaignActivity + "&IdActivityType=" + idActivityType + "&ActivitiesResponses=" + varResponses, callbackSaveResponse);
            }

            function AllResponse() {
                document.getElementById('wait').style.display = "block";
                var varImages = "";
                var i = 0;
                var form = document.forms[0];

                for (i = 0; i < form.elements.length; i++) {
                    if (form.elements[i].getAttribute("Image") == 'Image') {
                        varImages += form.elements[i].getAttribute("GuidTarget") + "," + form.elements[i].getAttribute("IdCampaignActivity") + "," + form.elements[i].getAttribute("IdActivityType") + ";";
                    }
                }

                if (varImages.length != 0) {
                    varImages = varImages.substring(0, varImages.length - 1);
                    makeRequest("../../UIProviders/CampaignSystem/EntityCampaignUIProvider.aspx", "MethodToCall=AllResponse&Images=" + varImages, callbackAllResponse);
                }
                else
                    document.getElementById('wait').style.display = "none";
            }

            function init() {
                document.getElementById('wait').style.display = "none";
                AllResponse();
            }

            YAHOO.util.Event.addListener(window, "load", init, "", true);
      
        </script>
        <script type="text/javascript">
            function FillDaysStartDate() {
                var mylistYear = document.getElementById('<%=ddlYearStartDate.ClientID%>');
                var mylistMonth = document.getElementById('<%=ddlMonthStartDate.ClientID%>');

                var year = mylistYear.options[mylistYear.selectedIndex].value;
                var month = mylistMonth.options[mylistMonth.selectedIndex].value;

                CleanSelected('<%=ddlDayStartDate.ClientID%>');

                var sizeDays = 0;

                switch (month) {
                    case '01':
                    case '03':
                    case '05':
                    case '07':
                    case '08':
                    case '10':
                    case '12':
                        sizeDays = 31;
                        break;
                    case '04':
                    case '06':
                    case '09':
                    case '11':
                        sizeDays = 30;
                        break;
                    case '02':
                        if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
                            sizeDays = 29;
                        else
                            sizeDays = 28;
                        break;
                }

                var elements = "";

                for (var i = 1; i <= sizeDays; i++) {
                    if (i < 10)
                        elements += "0" + i + "|" + "0" + i + ",";
                    else
                        elements += i + "|" + i + ",";
                }

                if (elements.length != 0)
                    elements = elements.substring(0, elements.length - 1);

                fillSelect(elements, '<%=ddlDayStartDate.ClientID%>');
                document.getElementById('<%=hfFechaInicio.ClientID%>').value = year + "/" + month + "/01";
            }

            function FillDaysEndDate() {
                var mylistYear = document.getElementById('<%=ddlYearEndDate.ClientID%>');
                var mylistMonth = document.getElementById('<%=ddlMonthEndDate.ClientID%>');

                var year = mylistYear.options[mylistYear.selectedIndex].value;
                var month = mylistMonth.options[mylistMonth.selectedIndex].value;

                CleanSelected('<%=ddlDayEndDate.ClientID%>');

                var sizeDays = 0;

                switch (month) {
                    case '01':
                    case '03':
                    case '05':
                    case '07':
                    case '08':
                    case '10':
                    case '12':
                        sizeDays = 31;
                        break;
                    case '04':
                    case '06':
                    case '09':
                    case '11':
                        sizeDays = 30;
                        break;
                    case '02':
                        if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
                            sizeDays = 29;
                        else
                            sizeDays = 28;
                        break;
                }

                var elements = "";

                for (var i = 1; i <= sizeDays; i++) {
                    if (i < 10)
                        elements += "0" + i + "|" + "0" + i + ",";
                    else
                        elements += i + "|" + i + ",";
                }

                if (elements.length != 0)
                    elements = elements.substring(0, elements.length - 1);

                fillSelect(elements, '<%=ddlDayEndDate.ClientID%>');
                document.getElementById('<%=hfFechaFin.ClientID%>').value = year + "/" + month + "/01";
            }

            function CleanSelected(control) {
                var elements = "";
                for (var i = (document.getElementById(control).length - 1); i != -1; i--) {
                    elements += document.getElementById(control).options[0].value + "|" + document.getElementById(control).options[0].text + ",";
                    document.getElementById(control).remove(0);
                }

                if (elements.length != 0)
                    elements = elements.substring(0, elements.length - 1);

                return elements;
            }

            function fillSelect(stringValues, idControl) {
                var optionsList = stringValues.split(",");
                var elSel = null;
                var item = null;

                if (document.getElementById(idControl) != null)
                    elSel = document.getElementById(idControl);

                if (elSel != null) {
                    for (var i = 0; i < optionsList.length; i++) {
                        item = optionsList[i].split("|");
                        var elOptNew = document.createElement('option');
                        elOptNew.value = item[0];
                        elOptNew.text = item[1];

                        try {
                            elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
                        }
                        catch (ex) {
                            elSel.add(elOptNew); // IE only
                        }
                    }
                }
            }

            function ValidateControls() {
                var isValid = true;
                var errorMessageOther = "";
                var dateStart = new Date();
                var dateEnd = new Date();

                var mylistYearStart = document.getElementById('<%=ddlYearStartDate.ClientID%>');
                var mylistMonthStart = document.getElementById('<%=ddlMonthStartDate.ClientID%>');
                var mylistDayStart = document.getElementById('<%=ddlDayStartDate.ClientID%>');

                var yearStart = mylistYearStart.options[mylistYearStart.selectedIndex].value;
                var monthStart = mylistMonthStart.options[mylistMonthStart.selectedIndex].value;
                var dayStart = mylistDayStart.options[mylistDayStart.selectedIndex].value;

                var mylistYearEnd = document.getElementById('<%=ddlYearEndDate.ClientID%>');
                var mylistMonthEnd = document.getElementById('<%=ddlMonthEndDate.ClientID%>');
                var mylistDayEnd = document.getElementById('<%=ddlDayEndDate.ClientID%>');

                var yearEnd = mylistYearEnd.options[mylistYearEnd.selectedIndex].value;
                var monthEnd = mylistMonthEnd.options[mylistMonthEnd.selectedIndex].value;
                var dayEnd = mylistDayEnd.options[mylistDayEnd.selectedIndex].value;

                dateStart.setFullYear(yearStart, monthStart - 1, dayStart);
                dateEnd.setFullYear(yearEnd, monthEnd - 1, dayEnd);

                if (dateStart > dateEnd) {
                    errorMessageOther += "- La fecha de finalización debe ser mayor o igual a la fecha de inicio\n";
                    isValid = false;
                }

                if (document.getElementById('<%= txtDescription.ClientID %>').value.length > 255) {
                    errorMessageOther += "- La descripción no puede tener más de 255 caracteres\n";
                    isValid = false;
                }

                if (!isValid) {
                    ShowError(errorMessageOther);
                    return false;
                }
                else {
                    return true;
                }
            }

            function vacio(q) {
                for (i = 0; i < q.length; i++) {
                    if (q.charAt(i) != " ") {
                        return false;
                    }
                }
                return true;
            }
        </script>
    </div>
</asp:Content>
