﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="CampaignActivityResponses.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignActivityResponses" %>

<%@ Register Src="CampaignOptions.ascx" TagName="CampaignOptions" TagPrefix="uc1" %>
<%@ Register Src="../../../Controls/ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <uc1:CampaignOptions ID="CampaignOptions1" currentpanel="pnlActivitiesResponse" runat="server" />
        <table cellpadding="3" cellspacing="0" width="100%" border="1" bordercolor="lightgrey">
            <tr>
                <td colspan="2" bgcolor="LightSteelBlue">
                    <strong>Carga de respuestas actividades de campaña</strong>
                </td>
            </tr>
            <tr>
                <td style="width: 200px">
                    Entidad de Negocio:
                </td>
                <td>
                    <asp:DropDownList ID="ddlViews" runat="server" Width="300px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvView" runat="server" ControlToValidate="ddlViews"
                        ValidationGroup="Load" ErrorMessage="Debe seleccionar una entidad de negocio">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Rol:
                </td>
                <td>
                    <asp:DropDownList ID="ddlRoles" runat="server" Width="300px">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlRoles"
                        ValidationGroup="Load" ErrorMessage="Debe seleccionar un rol">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Archivo:
                </td>
                <td>
                    <asp:FileUpload ID="fileUpload" runat="server" ToolTip="Seleccione el archivo de carga"
                        size="30" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button Text="Cargar Archivo" ID="btnLoad" runat="server" OnClick="btnLoad_Click"
                        ValidationGroup="Load" />
                    <asp:ValidationSummary ID="vsLoad" runat="server" ShowMessageBox="true" ShowSummary="false"
                        ValidationGroup="Load" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc2:ResultMessage ID="resultMessage" runat="server" />
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
