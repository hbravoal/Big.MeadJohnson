﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="CampaignsDetailsTarget.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignsDetailsTarget" %>

<%@ Register Src="CampaignTargetTable.ascx" TagName="CampaignTargetTable" TagPrefix="uc2" %>
<%@ Register Src="CampaignOptions.ascx" TagName="CampaignOptions" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
        <div id="adminedit">
            <uc1:CampaignOptions ID="CampaignOptions1" CurrentPanel="pnlContacts" runat="server" />
            <table cellpadding="3" cellspacing="0" width="100%" border="1" bordercolor="lightgrey">
                <tr>
                    <td bgcolor="LightSteelBlue">
                        <strong>Listado de contactos de la campaña</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <input type="button" value="Seleccionar Contactos" onclick="LoadTarget();return false;" />
                        &nbsp;
                        <asp:Button ID="btnClean" runat="server" Text="Limpiar Contactos" OnClick="btnClean_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="height: 100%">
                            <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                                <script type="text/javascript">

                                    function GridCreated(sender, eventArgs) {
                                        var gridContainer = document.getElementById("ctl00_maincontent_rgList_GridData");
                                        if (gridContainer != null)
                                            gridContainer.style.height = "100%";
                                    }
                                </script>
                            </telerik:RadScriptBlock>
                            <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                PageSize="20" GridLines="None" Height="100%" Width="100%" OnItemCommand="rgList_ItemCommand"
                                OnNeedDataSource="rgList_NeedDataSource">
                                <HeaderContextMenu EnableAutoScroll="True">
                                </HeaderContextMenu>
                                <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" FileName="ContactosBase"
                                    IgnorePaging="true">
                                </ExportSettings>
                                <MasterTableView TableLayout="Fixed" Width="100%" CommandItemDisplay="Bottom">
                                    <CommandItemSettings RefreshText="Actualizar" ExportToExcelText="Exportar a Excel"
                                        ShowAddNewRecordButton="false" ShowRefreshButton="true" ShowExportToExcelButton="false" />
                                    <Columns>
                                        <telerik:GridBoundColumn DataField="GUID" HeaderText="GUID">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ENTITYGUID" HeaderText="GUID_CONSUMIDOR">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn DataField="ENTITYVIEWNAME" HeaderText="ROL">
                                        </telerik:GridBoundColumn>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        Sin contactos seleccionados.
                                    </NoRecordsTemplate>
                                </MasterTableView>
                                <ClientSettings AllowDragToGroup="false">
                                    <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                    <Selecting AllowRowSelect="false" />
                                    <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                                </ClientSettings>
                            </telerik:RadGrid>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <hr />
                        <strong>Número total de registros:&nbsp;<asp:Label ID="lblCantidadRegistros" runat="server"
                            Text="0"></asp:Label></strong>
                    </td>
                </tr>
            </table>
        </div>
        <asp:Button ID="btnSetTarget" runat="server" OnClick="btnSetTarget_Click" Style="visibility: hidden" />
        <asp:HiddenField ID="hfTargetGuid" runat="server" />
    </telerik:RadAjaxPanel>
    <telerik:RadWindowManager ID="rdPopUp" runat="server" ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radWindow" runat="server" Width="900px" Height="600px" Behaviors="Close"
                InitialBehaviors="Close" Modal="true" ReloadOnShow="true" ShowContentDuringLoad="false"
                VisibleStatusbar="false" NavigateUrl="~/APP/Targetting.aspx">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadScriptBlock runat="server">
        <script type="text/javascript" language="javascript">

            function LoadTarget() {
                radopen("../../Targetting.aspx?targetType=CampaignTarget"
                    , "radWindow");
            }

            function SetTarget(targetGuid) {
                var oManager = GetRadWindowManager();
                oManager.CloseAll();

                document.getElementById('<%=hfTargetGuid.ClientID %>').value = targetGuid;
                document.getElementById('<%=btnSetTarget.ClientID %>').click();

            }
    
        </script>
    </telerik:RadScriptBlock>
</asp:Content>
