﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="CampaignPretarget.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignPretarget" %>

<%@ Register Src="CampaignOptions.ascx" TagName="CampaignOptions" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <uc1:CampaignOptions ID="CampaignOptions1" currentpanel="pnlBaseContacts" runat="server" />
        <table cellpadding="3" cellspacing="0" width="100%" border="1" bordercolor="lightgrey">
            <tr>
                <td bgcolor="LightSteelBlue">
                    <strong>Listado de contactos base de la campaña</strong>
                </td>
            </tr>
            <tr>
                <td>
                    <input type="button" value="Cargar Contactos Base" onclick="LoadBaseContacts('<%=Request.QueryString["Guid"] %>');return false;" />
                    &nbsp;
                    <asp:Button ID="btnClean" runat="server" Text="Limpiar Contactos Base" OnClick="btnClean_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="height: 100%">
                        <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                            <script type="text/javascript">

                                function GridCreated(sender, eventArgs) {
                                    var gridContainer = document.getElementById("ctl00_maincontent_rgList_GridData");
                                    if (gridContainer != null)
                                        gridContainer.style.height = "100%";
                                }
                            </script>
                        </telerik:RadScriptBlock>
                        <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="True" AllowPaging="true"
                            PageSize="20" GridLines="None" Height="100%" Width="100%" OnItemCommand="rgList_ItemCommand"
                            OnNeedDataSource="rgList_NeedDataSource">
                            <HeaderContextMenu EnableAutoScroll="True">
                            </HeaderContextMenu>
                            <ExportSettings Excel-Format="ExcelML" ExportOnlyData="true" FileName="ContactosBase"
                                IgnorePaging="true">
                            </ExportSettings>
                            <MasterTableView TableLayout="Auto" Width="100%" CommandItemDisplay="Bottom">
                                <CommandItemSettings RefreshText="Actualizar" ExportToExcelText="Exportar a Excel"
                                    ShowAddNewRecordButton="false" ShowRefreshButton="true" ShowExportToExcelButton="true" />
                                <Columns>
                                    <%--<telerik:GridTemplateColumn UniqueName="DeleteColum">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="btnDeleteContact" runat="server" CommandArgument='<%#Eval("GUID") %>'
                                                CommandName="DeleteContact" ToolTip="Borrar Contacto">
                                                <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-delete.gif" BorderWidth="0px" />
                                            </asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle Width="35px" />
                                        <HeaderStyle Width="35px" />
                                    </telerik:GridTemplateColumn>--%>
                                </Columns>
                                <NoRecordsTemplate>
                                    Sin contactos base cargados.
                                </NoRecordsTemplate>
                            </MasterTableView>
                            <ClientSettings AllowDragToGroup="false">
                                <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                <Selecting AllowRowSelect="false" />
                                <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                            </ClientSettings>
                        </telerik:RadGrid>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                    <strong>Número total de registros:&nbsp;<asp:Label ID="lblCantidadRegistros" runat="server"
                        Text="0"></asp:Label></strong>
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadWindowManager ID="rdPopUp" runat="server" ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radWindow" runat="server" Width="700px" Height="550px" Behaviors="Close"
                InitialBehaviors="Close" Modal="true" ReloadOnShow="true" ShowContentDuringLoad="false"
                VisibleStatusbar="false" NavigateUrl="LoadPretarget.aspx" OnClientClose="ReloadPage">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <script type="text/javascript" language="javascript">

        function LoadBaseContacts(campaignGuid) {
            radopen("LoadPretarget.aspx?campaignGuid=" + campaignGuid
            , "radWindow");
        }

        function ReloadPage(sender, eventArgs) {
            var oManager = GetRadWindowManager();
            oManager.CloseAll();
            window.location.href = window.location.href;
        }
    
    </script>
</asp:Content>
