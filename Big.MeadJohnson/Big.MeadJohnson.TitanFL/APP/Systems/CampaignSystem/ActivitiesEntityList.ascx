﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ActivitiesEntityList.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.ActivitiesEntityList" %>
<script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
<script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
<link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
<span id="wait">
    <img alt="" src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando...</span>
<asp:GridView ID="GridViewActivities" runat="server" AutoGenerateColumns="False"
    DataSourceID="ActivitiesDataSource" Width="100%" CellPadding="4" ForeColor="#333333"
    GridLines="None">
    <Columns>
        <asp:TemplateField HeaderText="Respuestas">
            <ItemTemplate>
                <a href='ResponseEntityDetail.aspx' onclick="ChangeReference(this, '<%# Eval("Guid") %>', '<%# Eval("IdCampaign") %>', '<%# Eval("IdActivity") %>')">
                    <img border="0" src="../../Images/gears.gif" />
                </a>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Name" />
        <asp:BoundField DataField="Description" HeaderText="Descripci&#243;n" SortExpression="Description" />
        <asp:BoundField DataField="StartDate" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Fecha inicial"
            HtmlEncode="False" SortExpression="StartDate" />
        <asp:BoundField DataField="EndDate" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Fecha final"
            HtmlEncode="False" SortExpression="EndDate" />
        <asp:BoundField DataField="ACTIVITYTYPE" HeaderText="Tipo de actividad" ReadOnly="True"
            SortExpression="NameActivityType" />
        <asp:TemplateField HeaderText="Respondido">
            <ItemTemplate>
                <input type="hidden" id="hidden_<%# Eval("Guid") %>" image="Image" guid="<%# Eval("Guid") %>"
                    idcampaign="<%# Eval("IdCampaign") %>" idactivitytype="<%# Eval("IdActivity")  %>"
                    style="display: none;" />
                <img id="image_<%# Eval("Guid") %>" src="../../Images/required.gif" style="display: none;" />
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>
    </Columns>
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <EditRowStyle BackColor="#999999" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>
<asp:ObjectDataSource ID="ActivitiesDataSource" runat="server" SelectMethod="GetCampaignActivities"
    TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.CampaignActivity">
    <SelectParameters>
        <asp:QueryStringParameter Name="campaignGuid" QueryStringField="Guid" Type="String" />
        <asp:Parameter Name="activityTypeId" Type="Int32" />
        <asp:Parameter Name="activityName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<script type="text/javascript">
    function ChangeReference(o, guid, idCampaign, idActivity) {
        o.href = 'ResponseEntityDetail.aspx?ViewName=' + viewName + '&EntityGuid=' + entityGuid + "&Guid=" + guid + "&IdCampaign=" + idCampaign + "&IdActivity=" + idActivity;
    }

    var handleAllResponse = function (o) {
        response = o.responseText;
        if (response !== undefined) {
            response = response.split("<!")[0];

            if (response != "") {
                var imagesList = response.split(";");
                var imageInfo = "";

                for (var i = 0; i < imagesList.length; i++) {
                    imageInfo = imagesList[i].split(",");

                    if (imageInfo[1] == "True")
                        document.getElementById("image_" + imageInfo[0]).style.display = "block";
                    else
                        document.getElementById("image_" + imageInfo[0]).style.display = "none";
                }
            }
            document.getElementById('wait').style.display = "none";
        }
    };

    var handleFailureCurrent = function (o) {
        if (o.responseText !== undefined) {
            ShowError(o.responseText);
        }
    };

    var callbackAllResponse =
{
    success: handleAllResponse,
    failure: handleFailureCurrent
}

    function makeRequest(sUrl, postData, callback) {
        var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);
    }

    function AllResponse() {
        document.getElementById('wait').style.display = "block";
        var varImages = "";
        var i = 0;
        var form = document.forms[0];
        var idCampaign = "";

        for (i = 0; i < form.elements.length; i++) {
            if (form.elements[i].getAttribute("Image") == 'Image') {
                varImages += form.elements[i].getAttribute("Guid") + "," + form.elements[i].getAttribute("IdActivityType") + ";";
                idCampaign = form.elements[i].getAttribute("IdCampaign");
            }
        }

        if (varImages.length != 0) {
            varImages = varImages.substring(0, varImages.length - 1);
            makeRequest("../../UIProviders/CampaignSystem/EntityCampaignUIProvider.aspx", "MethodToCall=AllResponseForActivities&ViewName=" + viewName + "&EntityGuid=" + entityGuid + "&IdCampaign=" + idCampaign + "&Images=" + varImages, callbackAllResponse);
        }
        else
            document.getElementById('wait').style.display = "none";
    }

    function init() {
        document.getElementById('wait').style.display = "none";
        AllResponse();
    }

    YAHOO.util.Event.addListener(window, "load", init, "", true);
</script>
