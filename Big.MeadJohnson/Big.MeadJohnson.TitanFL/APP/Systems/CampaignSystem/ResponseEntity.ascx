﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ResponseEntity.ascx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.ResponseEntity" %>
<script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>

<script type="text/javascript" src="../../Scripts/TitanFL.js"></script>

<link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
<span id="wait">
	<img alt="" src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando...</span>
<asp:Xml ID="xmlResponses" runat="server"></asp:Xml>

<script type="text/javascript">
    var handleSuccessSaveResponse = function (o) {
        response = o.responseText;
        if (response !== undefined) {
            response = response.split("<!")[0];

            if (response == "") {
                ShowError("No se ha respondido nada");
            }
            else {
                ShowError(response);
            }
            document.getElementById('wait').style.display = "none";
        }
    };

    var handleFailureCurrent = function (o) {
        if (o.responseText !== undefined) {
            ShowError(o.responseText);
        }
    };

    var callbackSaveResponse =
{
    success: handleSuccessSaveResponse,
    failure: handleFailureCurrent
}

    function makeRequest(sUrl, postData, callback) {
        var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);
    }

    function SaveResponses(guidTarget, idCampaignActivity, idActivityType) {
        document.getElementById('wait').style.display = "block";

        var i = 0; j = 0;
        var varResponses = "";
        var form = document.forms[0];

        for (i = 0; i < form.elements.length; i++) {
            if (form.elements[i].getAttribute("Target") == guidTarget) {
                hayRespuesta = false;

                for (j = 0; j < document.getElementsByName(form.elements[i].getAttribute("Response")).length; j++) {
                    if (document.getElementsByName(form.elements[i].getAttribute("Response"))[j].checked) {
                        varResponses += form.elements[i].getAttribute("ActivityResponseType") + "," + document.getElementsByName(form.elements[i].getAttribute("Response"))[j].value + ",";
                        hayRespuesta = true;
                        break;
                    }
                }

                if (hayRespuesta) {
                    var mylist = document.getElementById(form.elements[i].getAttribute("IdARState"));
                    var valueList = mylist.options[mylist.selectedIndex].value;

                    if (valueList != "") {
                        varResponses += valueList + ",";
                    }
                    else {
                        varResponses += ",";
                    }

                    var descriptionARS = document.getElementById(form.elements[i].getAttribute("DescriptionARState")).value;

                    if (!vacio(descriptionARS)) {
                        varResponses += descriptionARS + ";";
                    }
                    else {
                        varResponses += ";";
                    }

                    if (valueList != "" && vacio(descriptionARS)) {
                        ShowError("A la pregunta '" + form.elements[i].getAttribute("NameQuestion") + "' se le ha seleccionado un estado, debe ingresar entonces una descripción");
                        document.getElementById('wait').style.display = "none";
                        return;
                    }

                    if (!vacio(descriptionARS) && valueList == "") {
                        ShowError("A la pregunta '" + form.elements[i].getAttribute("NameQuestion") + "' se le ha ingresado una descripción, debe seleccionar entonces un estado");
                        document.getElementById('wait').style.display = "none";
                        return;
                    }
                }
            }
        }

        if (varResponses.length != 0)
            varResponses = varResponses.substring(0, varResponses.length - 1);

        makeRequest("../../UIProviders/CampaignSystem/EntityCampaignUIProvider.aspx", "MethodToCall=SaveResponse&GuidTarget=" + guidTarget + "&IdCampaignActivity=" + idCampaignActivity + "&IdActivityType=" + idActivityType + "&ActivitiesResponses=" + varResponses, callbackSaveResponse);
    }

    function init() {
        document.getElementById('wait').style.display = "none";
    }

    function vacio(q) {
        for (i = 0; i < q.length; i++) {
            if (q.charAt(i) != " ") {
                return false;
            }
        }
        return true;
    }

    YAHOO.util.Event.addListener(window, "load", init, "", true);
</script>

