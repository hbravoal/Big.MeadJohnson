﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Draws.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.Draws" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
        <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
        <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
        <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
        <div id="adminedit">
            <table cellpadding="5" cellspacing="0" border="1" width="100%" bordercolor="lightgrey">
                <tr>
                    <td bgcolor="lightsteelblue" colspan="2">
                        <strong>Seleccionar datos de campaña para generara sorteo</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 200px">
                        <span style="color: Red">*</span>&nbsp;Entidad de Negocio:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlViews" runat="server" Width="300px">
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvView" runat="server" ControlToValidate="ddlViews"
                            ValidationGroup="Load" ErrorMessage="Debe seleccionar una entidad de negocio">*</asp:RequiredFieldValidator>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="El campo entidad de negocio es requerido"
                            ValidationGroup="Draw" ControlToValidate="ddlCampaignPrograms" InitialValue=""
                            Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        <span style="color: Red">*</span>&nbsp;Programa:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCampaignPrograms" runat="server" OnSelectedIndexChanged="ddlCampaignPrograms_SelectedIndexChanged"
                            DataSourceID="odsCampaignPrograms" DataTextField="NAME" DataValueField="GUID"
                            AppendDataBoundItems="true" AutoPostBack="true">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="odsCampaignPrograms" runat="server" SelectMethod="GetCampaignPrograms"
                            TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.CampaignProgram">
                            <SelectParameters>
                                <asp:Parameter Name="programTypeId" Type="Int32" />
                                <asp:Parameter Name="programName" Type="String" />
                                <asp:Parameter DefaultValue="True" Name="enabledPrograms" Type="Boolean" />
                            </SelectParameters>
                        </asp:ObjectDataSource>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="El campo programa es requerido"
                            ValidationGroup="Draw" ControlToValidate="ddlCampaignPrograms" InitialValue=""
                            Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: Red">*</span>&nbsp;Campaña:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlCampaigns" runat="server" DataTextField="NAME" DataValueField="GUID"
                            AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCampaigns_SelectedIndexChanged"
                            AutoPostBack="true">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="El campo campaña es requerido"
                            ValidationGroup="Draw" ControlToValidate="ddlCampaigns" InitialValue="" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td>
                        <span style="color: Red">*</span>&nbsp;Actividad:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlActivities" runat="server" DataTextField="NAME" DataValueField="GUID"
                            AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlActivities_SelectedIndexChanged">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="El campo actividad es requerido"
                            ValidationGroup="Draw" ControlToValidate="ddlActivities" InitialValue="" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="width: 150px">
                        <span style="color: Red">*</span>&nbsp;Respuesta:
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlResponseType" runat="server" AppendDataBoundItems="true">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="rfvResponseType" runat="server" ErrorMessage="El campo respuesta es requerido"
                            ValidationGroup="Draw" ControlToValidate="ddlResponseType" InitialValue="" Display="None"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;" width="150px">
                        <span style="color: Red">*</span>&nbsp;Cantidad de ganadores
                    </td>
                    <td>
                        <telerik:RadNumericTextBox ID="rntWinnersQuantity" runat="server" Type="Number" Value="0"
                            MinValue="0" NumberFormat-DecimalDigits="0" NumberFormat-AllowRounding="true">
                        </telerik:RadNumericTextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="rntWinnersQuantity"
                            ErrorMessage="El campo cantidad de ganadores es requerido." Display="None" InitialValue="0"
                            ValidationGroup="Draw">*</asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnBuscar" runat="server" Text="Generar Ganadores" 
                            ValidationGroup="Draw" onclick="btnBuscar_Click" />
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                            ShowSummary="False" ValidationGroup="Draw" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <div style="height: 100%">
                            <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="true" AllowPaging="true"
                                PageSize="50" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource">
                                <MasterTableView TableLayout="Fixed" Width="100%" CommandItemDisplay="Top">
                                    <CommandItemSettings ShowRefreshButton="false" RefreshText="Actualizar" ShowAddNewRecordButton="false" />
                                    <Columns>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        Sin ganadores generados
                                    </NoRecordsTemplate>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadAjaxPanel>
</asp:Content>
