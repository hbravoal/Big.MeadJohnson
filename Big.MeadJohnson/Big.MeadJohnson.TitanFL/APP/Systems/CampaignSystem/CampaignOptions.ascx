﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignOptions.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignOptions" %>
<link href="<%=Page.ResolveUrl("~/APP/Systems/CampaignSystem/CampaignOptions.css") %>"
    rel="stylesheet" type="text/css" />
<table cellpadding="0" cellspacing="0" border="0">
    <tr>
        <td>
            <div id="mainmenu">
                <ul>
                    <li>
                        <asp:HyperLink ID="pnlCampaign" runat="server">Campaña</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="pnlGoals" runat="server">Metas</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="pnlBaseContacts" runat="server">Contactos Base</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="pnlContacts" runat="server">Contactos</asp:HyperLink>
                    </li>
                    <li>
                        <asp:HyperLink ID="pnlActivitiesResponse" runat="server">Respuestas</asp:HyperLink>
                    </li>
                </ul>
            </div>
        </td>
    </tr>
</table>
