﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="CampaignGoals.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignGoals" %>

<%@ Register Src="CampaignOptions.ascx" TagName="CampaignOptions" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <uc1:CampaignOptions ID="CampaignOptions1" CurrentPanel="pnlGoals" runat="server" />
        <table cellpadding="3" cellspacing="0" width="100%" border="1" bordercolor="lightgrey">
            <tr>
                <td align="left">
                    <asp:GridView ID="gvGoals" runat="server" AutoGenerateColumns="False" Width="100%"
                        CellPadding="4" ForeColor="#333333" GridLines="None" OnRowCommand="gvGoals_RowCommand"
                        OnRowCreated="gvGoals_RowCreated" EmptyDataText="La campaña no tiene metas">
                        <Columns>
                            <asp:TemplateField SortExpression="Guid">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkEdit" runat="server" CommandName="RowEdit" CausesValidation="false">Editar</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="2%" />
                            </asp:TemplateField>
                            <asp:TemplateField SortExpression="Guid">
                                <ItemTemplate>
                                    <asp:LinkButton ID="lnkDelete" runat="server" CommandName="RowDelete" CausesValidation="false"
                                        OnClientClick="return confirm('Esta seguro que desea eliminar el registro');">Eliminar</asp:LinkButton>
                                </ItemTemplate>
                                <ItemStyle Width="2%" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Meta" SortExpression="Name">
                                <ItemStyle Width="20%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Description" HeaderText="Descripci&#243;n" SortExpression="Description">
                                <ItemStyle Width="28%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="HopedGoal" HeaderText="Esperada (%)" SortExpression="HopedGoal"
                                HtmlEncode="False">
                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="WidthGoal" HeaderText="Peso (%)" SortExpression="WidthGoal"
                                HtmlEncode="False">
                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RealGoal" HeaderText="Real (%)" SortExpression="RealGoal"
                                HtmlEncode="False">
                                <ItemStyle Width="10%" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FulfillmentGoal" HeaderText="Cumplimiento (%)" SortExpression="FulfillmentGoal"
                                HtmlEncode="False">
                                <ItemStyle Width="12%" HorizontalAlign="Center" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <EditRowStyle BackColor="#999999" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="LightSteelBlue" Font-Bold="True" ForeColor="Gray" />
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="btnAdd" runat="server" Text="Adicionar meta" CausesValidation="false"
                        OnClick="btnAdd_Click" />&nbsp;
                    <asp:Button ID="btnSaveGoalCampaign" runat="server" Text="Guardar metas de la campaña"
                        CausesValidation="false" OnClick="btnSaveGoalCampaign_Click" />
                    <asp:Button ID="btnCancelGoals" runat="server" Text="Cancelar" CausesValidation="false"
                        OnClick="btnCancelGoals_Click" />&nbsp;
                </td>
            </tr>
        </table>
        <br />
        <br />
        <div id="divMeta" runat="server" style="display: none">
            <table cellpadding="3" cellspacing="0" width="60%" border="1" bordercolor="lightgrey"
                align="center">
                <tr>
                    <td bgcolor="LightSteelBlue" colspan="2" align="center">
                        <strong>Meta</strong>
                    </td>
                </tr>
                <tr>
                    <td style="width: 50%">
                        <table cellpadding="3" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="right" style="width: 60%">
                                    <span style="color: Red">*</span>&nbsp;Meta
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGoal" runat="server"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGoal" runat="server" ErrorMessage="Se requiere el nombre de la meta"
                                        ControlToValidate="txtGoal" Display="None">*</asp:RequiredFieldValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <span style="color: Red">*</span>&nbsp;Descripción
                                </td>
                                <td>
                                    <asp:TextBox ID="txtDescription" runat="server" Height="72px" TextMode="MultiLine"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvDescription" runat="server" ErrorMessage="Se requiere la descripción de la meta."
                                        ControlToValidate="txtDescription" Display="None">*</asp:RequiredFieldValidator>
                                    <asp:CustomValidator ID="cvDescription" runat="server" ErrorMessage="La descripción no puede tener más de 255 caracteres"
                                        ControlToValidate="txtDescription" ClientValidationFunction="ClientValidate255"
                                        Display="None">*</asp:CustomValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td style="width: 50%">
                        <table cellpadding="3" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td align="right" style="width: 55%">
                                    <span style="color: Red">*</span>&nbsp;Meta esperada (%)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGoalHoped" runat="server" Width="70%" onchange="CalculateGoalFulfillment()"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGoalHoped" runat="server" ErrorMessage="Se requiere la meta esperada."
                                        ControlToValidate="txtGoalHoped" Display="None">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revGoalHoped" runat="server" ControlToValidate="txtGoalHoped"
                                        Display="none" ErrorMessage="La meta esperada debe ser un número decimal." ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator>
                                    <asp:RangeValidator ID="rvGoalHoped" runat="server" ErrorMessage="La meta esperada deber ser un valor entre 0 y 100"
                                        ControlToValidate="txtGoalHoped" Display="None" MaximumValue="100" MinimumValue="0"
                                        Type="Double">*</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    <span style="color: Red">*</span>&nbsp;Peso de la meta (%)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGoalWidth" runat="server" Width="70%"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvGoalWidth" runat="server" ErrorMessage="Se requiere el peso de la meta."
                                        ControlToValidate="txtGoalWidth" Display="None">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revGoalWidth" runat="server" ControlToValidate="txtGoalWidth"
                                        Display="none" ErrorMessage="El peso de la meta debe ser un número decimal."
                                        ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator>
                                    <asp:RangeValidator ID="rvGoalWidth" runat="server" ErrorMessage="El peso de la meta deber ser un valor entre 0 y 100"
                                        ControlToValidate="txtGoalWidth" Display="None" MaximumValue="100" MinimumValue="0"
                                        Type="Double">*</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Meta real (%)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGoalReal" runat="server" Width="70%" onchange="CalculateGoalFulfillment()"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="revGoalReal" runat="server" ControlToValidate="txtGoalReal"
                                        Display="none" ErrorMessage="La meta real debe ser un número decimal." ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator>
                                    <asp:RangeValidator ID="rvGoalReal" runat="server" ErrorMessage="La meta real deber ser un valor entre 0 y 100"
                                        ControlToValidate="txtGoalReal" Display="None" MaximumValue="100" MinimumValue="0"
                                        Type="Double">*</asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">
                                    Cumplimiento de la meta (%)
                                </td>
                                <td>
                                    <asp:TextBox ID="txtGoalFulfillment" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="revGoalFulfillment" runat="server" ControlToValidate="txtGoalFulfillment"
                                        Display="none" ErrorMessage="El cumplimiento de la meta debe ser un número decimal."
                                        ValidationExpression="^\d*\,?\d*$">*</asp:RegularExpressionValidator>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:HiddenField ID="hdfIndex" runat="server" Value="" />
                        <asp:Button ID="btnSave" runat="server" Text="Guardar en la lista" OnClick="btnSave_Click" />&nbsp;
                        <asp:Button ID="btnCancel" runat="server" Text="Cancelar" CausesValidation="false"
                            OnClick="btnCancel_Click" />
                    </td>
                </tr>
            </table>
            <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                ShowSummary="False" />
        </div>
    </div>
    <script type="text/javascript" language="javascript">
        function ClientValidate255(source, arguments) {
            if (arguments.Value.length <= 255)
                arguments.IsValid = true;
            else
                arguments.IsValid = false;
        }
        function CalculateGoalFulfillment() {
            var hopedGoal = '', hopedGoalF;
            var realGoal = '', realGoalF;
            var fulfillmentGoal = '', fulfillmentGoalF;

            if (document.getElementById('<%=txtGoalHoped.ClientID %>') != null && document.getElementById('<%=txtGoalHoped.ClientID %>').value != '') {
                hopedGoal = document.getElementById('<%=txtGoalHoped.ClientID %>').value;

                if (onlyDecimals(hopedGoal)) {
                    hopedGoal = hopedGoal.replace(',', '.');
                    hopedGoalF = parseFloat(hopedGoal);
                }
                else {
                    alert("La meta esperada debe ser un número decimal");
                    return;
                }
            }

            if (document.getElementById('<%=txtGoalReal.ClientID %>') != null && document.getElementById('<%=txtGoalReal.ClientID %>').value != '') {
                realGoal = document.getElementById('<%=txtGoalReal.ClientID %>').value;

                if (onlyDecimals(realGoal)) {
                    realGoal = realGoal.replace(',', '.');
                    realGoalF = parseFloat(realGoal);
                }
                else {
                    alert("La meta real debe ser un número decimal");
                    return;
                }
            }

            if ((hopedGoal != '') && (realGoal != '')) {
                if (hopedGoal != 0) {
                    fulfillmentGoalF = (realGoal / hopedGoal) * 100;
                    fulfillmentGoal = String(fulfillmentGoalF);
                    fulfillmentGoal = fulfillmentGoal.replace('.', ',');
                    var controlGoalFulfillment = document.getElementById('<%=txtGoalFulfillment.ClientID %>');
                    controlGoalFulfillment.setAttribute("value", fulfillmentGoal);
                }
            }
        }
        function onlyDecimals(varNumber) {
            patron = /^\d*\,?\d*$/;
            return patron.test(varNumber);
        }
    </script>
</asp:Content>
