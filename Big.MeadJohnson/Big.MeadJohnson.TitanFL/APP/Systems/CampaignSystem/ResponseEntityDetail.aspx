﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="ResponseEntityDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.ResponseEntityDetail" %>
<%@ Register Src="ResponseEntity.ascx" TagName="ResponseEntity" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
	<div id="adminedit">
		<table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
			<tr>
				<td height="24">
					<asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
				</td>
			</tr>
			<tr>
				<td style="border: solid thin gray; padding: 5px 5px 5px 5px" colspan="2">
					<br />
					<table cellpadding="4" cellspacing="0" width="100%" border="0">
						<tr>
							<td bgcolor="lightsteelblue">
								<strong>Respuestas</strong>
							</td>
						</tr>
						<tr>
							<td style="height: 21px">
								<uc1:ResponseEntity ID="ResponseEntity1" runat="server" />
								<br />
								<asp:HyperLink ID="hypSave" NavigateUrl="#" runat="server" OnLoad="hypSave_Load">Guardar</asp:HyperLink>
								<asp:HyperLink ID="hypBack" runat="server" OnLoad="hypBack_Load">Cancelar</asp:HyperLink>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		</table>
	</div>
</asp:Content>

