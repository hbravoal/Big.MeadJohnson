﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CampaignEntityList.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignEntityList" %>
<asp:GridView ID="GridViewCampaigns" runat="server" AutoGenerateColumns="False" DataSourceID="CampaignDataSource"
    Width="100%" CellPadding="4" ForeColor="#333333" GridLines="None">
    <Columns>
        <asp:TemplateField HeaderText="Actividades">
            <ItemTemplate>
                <a href='ActivitiesEntityDetail.aspx' onclick="ChangeReference(this, '<%# Eval("Guid") %>')">
                    <img alt="" border="0" src="../../Images/gears.gif" />
                </a>
            </ItemTemplate>
            <ItemStyle HorizontalAlign="Center" />
        </asp:TemplateField>
        <asp:BoundField DataField="Name" HeaderText="Nombre de la campa&#241;a" SortExpression="Name" />
        <asp:BoundField DataField="Description" HeaderText="Descripci&#243;n" SortExpression="Description" />
        <asp:BoundField DataField="NameCampaignType" HeaderText="Tipo de campa&#241;a" ReadOnly="True"
            SortExpression="NameCampaignType" />
        <asp:BoundField DataField="CreationDate" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Fecha de creaci&#243;n"
            HtmlEncode="False" SortExpression="CreationDate" />
        <asp:BoundField DataField="StartDate" DataFormatString="{0:yyyy/MM/dd}" HeaderText="Fecha de inicio"
            HtmlEncode="False" SortExpression="StartDate" />
    </Columns>
    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
    <EditRowStyle BackColor="#999999" />
    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
</asp:GridView>
<asp:ObjectDataSource ID="CampaignDataSource" runat="server" SelectMethod="GetCampaignsByEntityGuidAndViewName"
    TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.CampaignTarget">
    <SelectParameters>
        <asp:QueryStringParameter Name="entityGuid" QueryStringField="EntityGuid" Type="String" />
        <asp:QueryStringParameter Name="viewName" QueryStringField="ViewName" Type="String" />
    </SelectParameters>
</asp:ObjectDataSource>
<script type="text/javascript">
    function ChangeReference(o, guid) {
        o.href = 'ActivitiesEntityDetail.aspx?ViewName=' + viewName + '&EntityGuid=' + entityGuid + "&Guid=" + guid;
    }
</script>
