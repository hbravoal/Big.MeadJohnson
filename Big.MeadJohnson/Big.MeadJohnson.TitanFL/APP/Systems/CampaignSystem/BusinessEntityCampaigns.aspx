﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityCampaigns.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.BusinessEntityCampaigns" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24">
                    <asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px">
                    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
                    </telerik:RadAjaxLoadingPanel>
                    <telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
                        <br />
                        <table cellpadding="4" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td bgcolor="LightSteelBlue" colspan="2">
                                    <strong>Campañas Relacionadas</strong>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div style="height: 100%">
                                        <br />
                                        <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                                            <script type="text/javascript">

                                                function GridCreated(sender, eventArgs) {
                                                    var gridContainer = document.getElementById("ctl00_maincontent_rgList_GridData");
                                                    if (gridContainer != null)
                                                        gridContainer.style.height = "100%";
                                                }
                                            </script>
                                        </telerik:RadScriptBlock>
                                        <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                            PageSize="10" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource">
                                            <HeaderContextMenu EnableAutoScroll="True">
                                            </HeaderContextMenu>
                                            <MasterTableView TableLayout="Fixed" Width="100%">
                                                <Columns>
                                                    <telerik:GridTemplateColumn UniqueName="EditColumn">
                                                        <ItemTemplate>
                                                            <a href='javascript:openDetail("<%#Eval("CAMPAIGNGUID") %>", "<%#Eval("ACTIVITYGUID") %>");'
                                                                title="Ver Detalle">
                                                                <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                                            </a>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="35px" />
                                                        <HeaderStyle Width="35px" />
                                                    </telerik:GridTemplateColumn>
                                                    <telerik:GridBoundColumn HeaderText="Programa" DataField="PROGRAM">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Campaña" DataField="CAMPAIGN">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Actividad" DataField="ACTIVITY">
                                                    </telerik:GridBoundColumn>
                                                    <telerik:GridBoundColumn HeaderText="Tipo de Actividad" DataField="ACTIVITYTYPE">
                                                    </telerik:GridBoundColumn>
                                                </Columns>
                                                <NoRecordsTemplate>
                                                    Sin campañas relacionadas.
                                                </NoRecordsTemplate>
                                            </MasterTableView>
                                            <ClientSettings AllowDragToGroup="false">
                                                <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                                <Selecting AllowRowSelect="false" />
                                                <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                                            </ClientSettings>
                                        </telerik:RadGrid>
                                    </div>
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td bgcolor="LightSteelBlue" colspan="2">
                                    <strong>Relacionar Campaña</strong>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 150px">
                                    Programa:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCampaignPrograms" runat="server" OnSelectedIndexChanged="ddlCampaignPrograms_SelectedIndexChanged"
                                        DataSourceID="odsCampaignPrograms" DataTextField="NAME" DataValueField="GUID"
                                        AppendDataBoundItems="true" AutoPostBack="true">
                                        <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:ObjectDataSource ID="odsCampaignPrograms" runat="server" SelectMethod="GetCampaignPrograms"
                                        TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.CampaignProgram">
                                        <SelectParameters>
                                            <asp:Parameter Name="programTypeId" Type="Int32" />
                                            <asp:Parameter Name="programName" Type="String" />
                                            <asp:Parameter DefaultValue="True" Name="enabledPrograms" Type="Boolean" />
                                        </SelectParameters>
                                    </asp:ObjectDataSource>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Campaña:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlCampaigns" runat="server" DataTextField="NAME" DataValueField="GUID"
                                        AppendDataBoundItems="true" OnSelectedIndexChanged="ddlCampaigns_SelectedIndexChanged"
                                        AutoPostBack="true">
                                        <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Actividad:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlActivities" runat="server" DataTextField="NAME" DataValueField="GUID"
                                        AppendDataBoundItems="true">
                                        <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnAddCampaign" runat="server" Text="Adicionar Campaña" OnClientClick="javascript:openNew();return false;" />
                                </td>
                            </tr>
                        </table>
                    </telerik:RadAjaxPanel>
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadWindowManager ID="rdPopUp" runat="server" ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radWindow" runat="server" Width="700px" Height="400px" Behaviors="Close"
                InitialBehaviors="Close" Modal="true" ReloadOnShow="true" ShowContentDuringLoad="false"
                VisibleStatusbar="false" NavigateUrl="BusinessEntityCampaignDetail.aspx" OnClientClose="ReloadPage">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <telerik:RadScriptBlock runat="server" ID="RadScriptBlock2">
        <script type="text/javascript">
            function openNew() {

                var campaignGuid = document.getElementById('<%=ddlCampaigns.ClientID %>').options[document.getElementById('<%=ddlCampaigns.ClientID %>').selectedIndex].value;
                var activityGuid = document.getElementById('<%=ddlActivities.ClientID %>').options[document.getElementById('<%=ddlActivities.ClientID %>').selectedIndex].value;


                if (campaignGuid == "") {
                    alert("Debe seleccionar una campaña");
                    return false;
                }

                if (activityGuid == "") {
                    alert("Debe seleccionar una actividad");
                    return false;
                }

                radopen("BusinessEntityCampaignDetail.aspx?campaignGuid=" + campaignGuid
            + "&activityGuid=" + activityGuid
            + "&viewName=" + viewName
            + "&entityGuid=" + entityGuid
            , "radWindow");
            }

            function openDetail(campaignGuid, activityGuid) {
                radopen("BusinessEntityCampaignDetail.aspx?campaignGuid=" + campaignGuid
            + "&activityGuid=" + activityGuid
            + "&viewName=" + viewName
            + "&entityGuid=" + entityGuid
            , "radWindow");
            }

            function ReloadPage(sender, eventArgs) {
                var oManager = GetRadWindowManager();
                oManager.CloseAll();
                window.location.href = window.location.href;
            }
        </script>
    </telerik:RadScriptBlock>
</asp:Content>
