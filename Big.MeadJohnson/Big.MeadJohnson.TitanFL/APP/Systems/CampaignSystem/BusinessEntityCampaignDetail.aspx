﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/Popup.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityCampaignDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.BusinessEntityCampaignDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit2">
        <table cellpadding="4" cellspacing="0" width="100%" border="0">
            <tr>
                <td bgcolor="LightSteelBlue" colspan="2">
                    <strong>Respuesta Actividad</strong>
                </td>
            </tr>
            <tr>
                <td style="width: 150px">
                    Respuesta:
                </td>
                <td>
                    <asp:DropDownList ID="ddlResponseType" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="rfvResponseType" runat="server" ErrorMessage="El campo respuesta es requerido"
                        ValidationGroup="Response" ControlToValidate="ddlResponseType" InitialValue=""
                        Display="Dynamic"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    Descripción Respuesta:
                </td>
                <td>
                    <asp:TextBox ID="txtResponseDescription" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnAdd" runat="server" Text="Adicionar" ValidationGroup="Response"
                        OnClick="btnAdd_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br />
                    <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="10" GridLines="None" Width="100%" 
                        OnNeedDataSource="rgList_NeedDataSource" onitemcommand="rgList_ItemCommand">
                        <MasterTableView TableLayout="Fixed" Width="100%">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Respuesta" DataField="RESPONSETYPE">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Descripción" DataField="DESCRIPTION" EmptyDataText="N/A">
                                </telerik:GridBoundColumn>
                                <telerik:GridTemplateColumn UniqueName="EditColumn">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lnkEdit" runat="server" ToolTip="Borrar Respuesta" CommandName="RowDelete"
                                            CommandArgument='<%#Eval("GUID") %>'
                                            CausesValidation="false">
                                            <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-delete.gif" BorderWidth="0px" />
                                        </asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                Sin respuestas registradas
                            </NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                </td>
            </tr>
        </table>
        <script type="text/javascript" language="javascript">

            function CloseDetail() {
                alert("Respuesta Registrada.");
                parent.ReloadPage();
            }
        </script>
    </div>
</asp:Content>
