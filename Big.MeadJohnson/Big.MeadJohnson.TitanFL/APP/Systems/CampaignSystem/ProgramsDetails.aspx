﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="ProgramsDetails.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.ProgramsDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
   <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
   <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
   <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
   <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
   <div id="adminedit">
      <div style="text-align: left">
         <table cellpadding="3" cellspacing="0" width="100%" border="0">
            <tr>
               <td bgcolor="LightSteelBlue">
                  <strong>Programa</strong>
               </td>
            </tr>
            <tr>
               <td align="left">
                  <table cellpadding="4" cellspacing="0" width="100%" border="0">
                     <tr>
                        <td style="text-align: right;" width="100px" >
                           <span style="color: Red">*</span>&nbsp;Nombre</td>
                        <td>
                           <asp:TextBox ID="txtName" runat="server" Width="250px" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator
                              ID="RequiredFieldValidatorName" runat="server" ControlToValidate="txtName" ErrorMessage="Se requiere el nombre."
                              Display="None">*</asp:RequiredFieldValidator></td>
                     </tr>
                     <tr>
                        <td style="text-align: right;" width="100px">
                           Tipo de programa</td>
                        <td>
                           <asp:DropDownList ID="ddlProgramType" runat="server" DataSourceID="ProgramTypeDataSource"
                              DataTextField="Name" DataValueField="Id" Width="255px">
                           </asp:DropDownList><asp:RequiredFieldValidator ID="RequiredFieldValidatorProgramType"
                              runat="server" ControlToValidate="ddlProgramType" ErrorMessage="Se requiere el tipo de programa."
                              Display="None">*</asp:RequiredFieldValidator></td>
                     </tr>
                     <tr>
                        <td style="text-align: right;" width="100px">
                           <span style="color: Red">*</span>&nbsp;Descripción</td>
                        <td>
                           <asp:TextBox ID="txtDescription" runat="server" Width="80%" MaxLength="255" Height="52px"
                              TextMode="MultiLine"></asp:TextBox>
                           <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
                              ControlToValidate="txtDescription" ErrorMessage="Se requiere la descripción."
                              Display="None">*</asp:RequiredFieldValidator></td>
                     </tr>
                     <tr>
                        <td style="text-align: right;" width="100px">
                           Fecha de creación</td>
                        <td>
                           <asp:Label ID="lblFecha" runat="server" Text="" Font-Bold="true"></asp:Label>
                           <%--<table>
                              <tr>
                                 <td>
                                    <asp:DropDownList ID="ddlYearCreationDate" runat="server" onChange="FillDays()" Width="60px">
                                    </asp:DropDownList></td>
                                 <td>
                                    <asp:DropDownList ID="ddlMonthCreationDate" runat="server" onChange="FillDays()"
                                       Width="85px">
                                    </asp:DropDownList></td>
                                 <td>
                                    <asp:DropDownList ID="ddlDayCreationDate" runat="server" Width="50px" onChange="FillValue()">
                                    </asp:DropDownList></td>
                              </tr>
                           </table>
                           <asp:HiddenField ID="hfFechaCreacion" runat="server" />--%>
                        </td>
                     </tr>
                     <tr>
                        <td style="text-align: right;">
                        </td>
                        <td>
                           <asp:CheckBox ID="chkEnabled" runat="server" Text="Activo" /></td>
                     </tr>
                  </table>
               </td>
            </tr>
         </table>
         <asp:ObjectDataSource ID="ProgramTypeDataSource" runat="server" SelectMethod="GetProgramTypes"
            TypeName="OnData.TitanFL.Systems.Campaign.BF.Domain.ProgramType"></asp:ObjectDataSource>
      </div>
      <table>
         <tr>
            <td>
               <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" OnClientClick="if(!ValidateControls()) return false"/></td>
            <td>
               <asp:Button ID="btnDelete" runat="server" Text="Eliminar" OnClick="btnDelete_Click"
                  OnClientClick='return confirm("¿Realmente desea eliminar el registro?")' /></td>
            <td>
               <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CausesValidation="False" OnClick="btnCancelar_Click" /></td>               
         </tr>
      </table>
      <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
         ShowSummary="False" />
   </div>
   
   <script type="text/javascript">
       function ValidateControls() {
           var isValid = true;
           var errorMessageOther = "";

           if (document.getElementById('<%= txtDescription.ClientID %>').value.length > 255) {
               errorMessageOther += "- La descripción no puede tener más de 255 caracteres\n";
               isValid = false;
           }

           if (!isValid) {
               ShowError(errorMessageOther);
               return false;
           }
           else {
               return true;
           }
       }
   </script>
   
</asp:Content>
