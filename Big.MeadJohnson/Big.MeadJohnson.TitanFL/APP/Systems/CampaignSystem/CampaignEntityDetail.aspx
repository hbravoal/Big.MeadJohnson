﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="CampaignEntityDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.CampaignEntityDetail" %>

<%@ Register src="CampaignEntityList.ascx" tagname="CampaignEntityList" tagprefix="uc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <div id="adminedit">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24">
                    <asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px">
                    <br />
                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td bgcolor="lightsteelblue">
                                <strong>Campañas</strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                
                                <uc1:CampaignEntityList ID="CampaignEntityList1" runat="server" />
                                
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
