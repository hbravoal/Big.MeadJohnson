﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="ActivitiesEntityDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.CampaignSystem.ActivitiesEntityDetail" %>

<%@ Register Src="ActivitiesEntityList.ascx" TagName="ActivitiesEntityList" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <div id="adminedit">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24">
                    <asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px" colspan="2">
                    <br />
                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td bgcolor="lightsteelblue">
                                <strong>Actividades</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 21px">
                                <uc1:ActivitiesEntityList ID="ActivitiesEntityList1" runat="server" />
                                <asp:HyperLink ID="hypBack" runat="server" OnLoad="hypBack_Load">Cancelar</asp:HyperLink>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
