﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="List.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.RewardsSystem.Promotions.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxPanel ID="ajaxPanel" LoadingPanelID="loadingPanel" runat="server">
            <div>
                <table cellpadding="3">
                    <tr>
                        <td style="width: 130px">
                            Tipo de promoción:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPromotionTypes" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                OnSelectedIndexChanged="ddlPromotionTypes_SelectedIndexChanged" 
                                DataSourceID="odsPromotionTypes" DataTextField="Description" 
                                DataValueField="PromotionTypeId">
                                <asp:ListItem Text="[Todos]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="odsPromotionTypes" runat="server" SelectMethod="Get" 
                                TypeName="OnData.TitanFL.Systems.Rewards.BF.Domain.PromotionType">
                            </asp:ObjectDataSource>
                        </td>
                        <td style="width: 150px">
                            Estado de la promoción:
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPromotionStatus" runat="server" AutoPostBack="True" AppendDataBoundItems="True"
                                OnSelectedIndexChanged="ddlPromotionStatus_SelectedIndexChanged" 
                                DataSourceID="odsPromotionStatus" DataTextField="Description" 
                                DataValueField="PromotionStatusId">
                                <asp:ListItem Text="[Todos]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="odsPromotionStatus" runat="server" SelectMethod="Get" 
                                TypeName="OnData.TitanFL.Systems.Rewards.BF.Domain.PromotionStatus">
                            </asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
                <br />
                <br />
                <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                    PageSize="20" GridLines="None" Width="100%" OnNeedDataSource="rgList_NeedDataSource">
                    <MasterTableView TableLayout="Fixed">
                        <Columns>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <a href='Detail.aspx?promotionId=<%# Eval("GUID") %>' title="Editar">
                                        <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                    </a>
                                </ItemTemplate>
                                <ItemStyle Width="35px" />
                                <HeaderStyle Width="35px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Código" DataField="CODE">
                                <ItemStyle Width="90px" />
                                <HeaderStyle Width="90px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Nombre" DataField="NAME">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Tipo" DataField="PROMOTIONTYPE">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Estado" DataField="PROMOTIONSTATUS">
                            </telerik:GridBoundColumn>
                        </Columns>
                        <NoRecordsTemplate>
                            No existen promociones registradas
                        </NoRecordsTemplate>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </telerik:RadAjaxPanel>
        <div style="width: 100%; text-align: right">
            <br />
            <asp:Button ID="btnNew" runat="server" OnClientClick="window.location.href='Detail.aspx';return false;"
                Text="Ingresar" />
        </div>
    </div>
</asp:Content>
