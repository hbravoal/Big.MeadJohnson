﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Detail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.RewardsSystem.Promotions.Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <telerik:radtabstrip id="radTabStrip" runat="server" width="100%" multipageid="radMultiPage"
            selectedindex="0">
            <tabs>
                <telerik:RadTab Text="Información de la promoción" PageViewID="promotionInfo" Value="promotionInfo">
                </telerik:RadTab>
                <telerik:RadTab Text="Estado de la promoción" PageViewID="promotionStatus" Value="promotionStatus"
                    Visible="false">
                </telerik:RadTab>
            </tabs>
        </telerik:radtabstrip>
        <telerik:radmultipage id="radMultiPage" runat="server" width="100%" selectedindex="0"
            cssclass="pageView">
            <telerik:radpageview id="promotionInfo" runat="server">
                <div style="text-align: left; padding: 10px">
                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td align="left">
                                <table cellpadding="4" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="text-align: left;">
                                            <span style="color: Red">*</span>&nbsp;Código promoción
                                        </td>
                                        <td style="text-align: left;">
                                            <telerik:radtextbox id="txtCode" runat="server" width="50px" maxlength="5">
                                            </telerik:radtextbox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCode"
                                                ErrorMessage="El campo código promoción es requerido." Display="None" ValidationGroup="Promotion">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Nombre
                                        </td>
                                        <td>
                                            <telerik:radtextbox id="txtName" runat="server" width="350px" maxlength="100">
                                            </telerik:radtextbox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorName" runat="server" ControlToValidate="txtName"
                                                ErrorMessage="El campo nombre es requerido." Display="None" ValidationGroup="Promotion">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Descripción
                                        </td>
                                        <td>
                                            <telerik:radtextbox id="txtDescription" runat="server" width="350px" height="50px"
                                                textmode="MultiLine" maxlength="500">
                                            </telerik:radtextbox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDescription"
                                                ErrorMessage="El campo descripción es requerido." Display="None" ValidationGroup="Promotion">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Tipo promoción
                                        </td>
                                        <td>
                                            <telerik:radcombobox id="ddlPromotionTypes" runat="server" appenddatabounditems="True"
                                                datasourceid="odsPromotionTypes" datatextfield="Description" datavaluefield="PromotionTypeId">
                                                <items>
                                                    <telerik:RadComboBoxItem Text="[Seleccione]" Value="" />
                                                </items>
                                            </telerik:radcombobox>
                                            <asp:ObjectDataSource ID="odsPromotionTypes" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.Rewards.BF.Domain.PromotionType">
                                            </asp:ObjectDataSource>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlPromotionTypes"
                                                ErrorMessage="El campo tipo promoción es requerido." Display="None" InitialValue="[Seleccione]"
                                                ValidationGroup="Promotion">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Valor promoción
                                        </td>
                                        <td>
                                            <telerik:radnumerictextbox id="rntPromotionValue" runat="server" type="Number" value="0"
                                                minvalue="0" numberformat-decimaldigits="2" numberformat-allowrounding="true">
                                            </telerik:radnumerictextbox>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="rntPromotionValue"
                                                ErrorMessage="El campo valor promoción es requerido." Display="None" InitialValue="0"
                                                ValidationGroup="Promotion">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Fecha inicio
                                        </td>
                                        <td>
                                            <telerik:raddatepicker id="rdpStardDate" runat="server" dateinput-dateformat="dd/MM/yyyy">
                                            </telerik:raddatepicker>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="rdpStardDate"
                                                ErrorMessage="El campo fecha inicio es requerido." Display="None" ValidationGroup="Promotion">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left;" width="120px">
                                            <span style="color: Red">*</span>&nbsp;Fecha fin
                                        </td>
                                        <td>
                                            <telerik:raddatepicker id="rdpEndDate" runat="server" dateinput-dateformat="dd/MM/yyyy">
                                            </telerik:raddatepicker>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="rdpEndDate"
                                                ErrorMessage="El campo fecha fin es requerido." Display="None" ValidationGroup="Promotion">*</asp:RequiredFieldValidator>
                                            <asp:CompareValidator ID="compare" runat="server" ControlToValidate="rdpEndDate"
                                                ControlToCompare="rdpStardDate" Operator="GreaterThan" Display="None" ValidationGroup="Promotion"
                                                ErrorMessage="La fecha de fin debe ser mayor a la fecha de inicio"></asp:CompareValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: left; padding: 10px">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" ValidationGroup="Promotion" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CausesValidation="False"
                                    OnClientClick="window.location.href='List.aspx';return false;" />
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="Promotion" />
                </div>
            </telerik:radpageview>
            <telerik:radpageview id="promotionStatus" runat="server">
                <div style="text-align: left; padding: 10px">
                    <table cellpadding="3" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td align="left">
                                <table cellpadding="4" cellspacing="0" width="100%" border="0">
                                    <tr>
                                        <td style="text-align: left;" width="180px">
                                            <span style="color: Red">*</span>&nbsp;Estado promoción
                                        </td>
                                        <td>
                                            <telerik:radcombobox id="ddlPromotionStatus" runat="server" appenddatabounditems="True"
                                                autopostback="true" datasourceid="odsPromotionStatus" datatextfield="Description"
                                                datavaluefield="PromotionStatusId" onselectedindexchanged="ddlPromotionStatus_SelectedIndexChanged">
                                                <items>
                                                    <telerik:RadComboBoxItem Text="[Seleccione]" Value="" />
                                                </items>
                                            </telerik:radcombobox>
                                            <asp:ObjectDataSource ID="odsPromotionStatus" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.Rewards.BF.Domain.PromotionStatus">
                                            </asp:ObjectDataSource>
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ControlToValidate="ddlPromotionStatus"
                                                ErrorMessage="El campo estado promoción es requerido." Display="None" InitialValue="[Seleccione]"
                                                ValidationGroup="PromotionStatus">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trDenialCause" runat="server" visible="false">
                                        <td style="text-align: left;" width="180px">
                                            <span style="color: Red">*</span>&nbsp;Causa no aprobación
                                        </td>
                                        <td>
                                            <telerik:radcombobox id="ddlDenialCauses" runat="server" appenddatabounditems="True"
                                                datasourceid="odsDenialCauses" datatextfield="Description" datavaluefield="PromotionDenialCauseId">
                                                <items>
                                                    <telerik:RadComboBoxItem Text="[Seleccione]" Value="" />
                                                </items>
                                            </telerik:radcombobox>
                                            <asp:ObjectDataSource ID="odsDenialCauses" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.Rewards.BF.Domain.PromotionDenialCause">
                                            </asp:ObjectDataSource>
                                            <asp:RequiredFieldValidator ID="rfvddlDenialCauses" runat="server" ControlToValidate="ddlDenialCauses"
                                                ErrorMessage="El campo causa no aprobación es requerido." Display="None" InitialValue="[Seleccione]"
                                                ValidationGroup="PromotionStatus">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                    <tr id="trDenialCauseObservation" runat="server" visible="false">
                                        <td style="text-align: left;" width="180px">
                                            <span style="color: Red">*</span>&nbsp;Observación no aprobación
                                        </td>
                                        <td>
                                            <telerik:radtextbox id="txtDenialObservation" runat="server" width="350px" height="50px"
                                                textmode="MultiLine" maxlength="500">
                                            </telerik:radtextbox>
                                            <asp:RequiredFieldValidator ID="rfvtxtDenialObservation" runat="server" ControlToValidate="txtDenialObservation"
                                                ErrorMessage="El campo observación no aprobación es requerido." Display="None"
                                                ValidationGroup="PromotionStatus">*</asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div style="text-align: left; padding: 10px">
                    <table>
                        <tr>
                            <td>
                                <asp:Button ID="btnSaveStatus" runat="server" Text="Guardar" OnClick="btnSaveStatus_Click"
                                    ValidationGroup="PromotionStatus" />
                            </td>
                            <td>
                                <asp:Button ID="Button2" runat="server" Text="Cancelar" CausesValidation="False"
                                    OnClientClick="window.location.href='List.aspx';return false;" />
                            </td>
                        </tr>
                    </table>
                    <asp:ValidationSummary ID="ValidationSummary1" runat="server" ShowMessageBox="True"
                        ShowSummary="False" ValidationGroup="PromotionStatus" />
                </div>
            </telerik:radpageview>
        </telerik:radmultipage>
    </div>
</asp:Content>
