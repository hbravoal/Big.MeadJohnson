﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Common.ascx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.SubtypeControls.Common" %>
<table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
    <tr>
        <td style="border: solid thin gray; padding: 5px 5px 5px 5px" colspan="2">
            <table cellpadding="4" cellspacing="0" width="100%" border="0">
                <tr>
                    <td style="text-align: left;">
                        <span style="color: Red">*</span>&nbsp;Fuente
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSource" runat="server" DataSourceID="SourcesListDataSource"
                            DataTextField="Name" DataValueField="Id" AppendDataBoundItems="true">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlSource"
                            ErrorMessage="La fuente es requerida" Display="None" InitialValue="">*</asp:RequiredFieldValidator>
                        <asp:ObjectDataSource ID="SourcesListDataSource" runat="server" SelectMethod="Get"
                            TypeName="OnData.TitanFL.Systems.PQRS.BF.Domain.Source"></asp:ObjectDataSource>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; width: 180px">
                        <span style="color: Red">*</span>&nbsp;Descripción PQRS
                    </td>
                    <td>
                        <asp:TextBox ID="txtDetail" runat="server" TextMode="MultiLine" Height="96px" Width="100%"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorDetail" runat="server" ControlToValidate="txtDetail"
                            ErrorMessage="La descripción es requerida" Display="None">*</asp:RequiredFieldValidator>
                        <asp:CustomValidator ID="cvDescripcion" runat="server" ErrorMessage="La descripción no puede tener más de 4000 caracteres"
                            ControlToValidate="txtDetail" ClientValidationFunction="ClientValidate512" Display="None">*</asp:CustomValidator>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; width: 180px">
                        <span style="color: Red">*</span>&nbsp;Es de alto impacto
                    </td>
                    <td>
                        <asp:CheckBox ID="chkIsHighImpact" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        <asp:Button ID="btnAdd" runat="server" OnClick="btnAdd_Click" Text="Registrar" />
                        <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                            ShowSummary="False" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<script type="text/javascript" language="javascript">
    function ClientValidate512(source, arguments) {
        if (arguments.Value.length <= 4000)
            arguments.IsValid = true;
        else
            arguments.IsValid = false;
    }

    
</script>
