﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="CommonManagement.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.SubtypeControls.CommonManagement" %>
<%@ Register Src="~/Controls/ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc1" %>
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
    <uc1:ResultMessage ID="resultMessage" runat="server" />
    <table cellpadding="4" cellspacing="0" width="100%" border="0">
        <tr>
            <td bgcolor="LightSteelBlue">
                <strong>Contacto</strong>
            </td>
        </tr>
        <tr>
            <td>
                <asp:DetailsView ID="businessEntityDetail" runat="server" GridLines="None" CellPadding="4"
                    OnDataBound="businessEntityDetail_DataBound">
                    <FieldHeaderStyle Font-Bold="true" Width="180px" />
                </asp:DetailsView>
            </td>
        </tr>
        <tr>
            <td bgcolor="LightSteelBlue">
                <strong>Detalle de PQRS</strong>
            </td>
        </tr>
        <tr>
            <td>
                <table width="600px" cellpadding="3" cellspacing="0" border="0">
                    <tr>
                        <td style="text-align: left; font-weight: bold; width: 180px">Fecha
                        </td>
                        <td>
                            <asp:Literal ID="ltDate" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Rol
                        </td>
                        <td>
                            <asp:Literal ID="ltRoleName" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Tipo
                        </td>
                        <td>
                            <asp:Literal ID="ltType" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Subtipo
                        </td>
                        <td>
                            <asp:Literal ID="ltSubtype" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Estado
                        </td>
                        <td>
                            <asp:Literal ID="ltStatus" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Fuente
                        </td>
                        <td>
                            <asp:Literal ID="ltSource" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Punto de contacto
                        </td>
                        <td>
                            <asp:Literal ID="ltContactPoint" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Área de escalamiento
                        </td>
                        <td>
                            <asp:Literal ID="ltScalingArea" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Fecha de vencimiento
                        </td>
                        <td>
                            <asp:Literal ID="ltExpirationDate" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Alto impacto
                        </td>
                        <td>
                            <asp:Literal ID="ltHighImpact" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <asp:Repeater ID="rptSubtypeDetail" runat="server">
                        <ItemTemplate>
                            <tr>
                                <td style="text-align: left; font-weight: bold">
                                    <asp:Literal ID="ltName" runat="server" Text='<%#Eval("Key") %>'></asp:Literal>
                                </td>
                                <td>
                                    <asp:Literal ID="ltValue" runat="server" Text='<%#Eval("Value") %>'></asp:Literal>
                                </td>
                            </tr>
                        </ItemTemplate>
                    </asp:Repeater>
                    <tr>
                        <td style="text-align: left; font-weight: bold">Detalle
                        </td>
                        <td>
                            <asp:Literal ID="ltDetail" runat="server"></asp:Literal>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td bgcolor="LightSteelBlue">
                <strong>Seguimiento de PQRS</strong>
            </td>
        </tr>
        <tr>
            <td>
                <table width="600px" cellpadding="3" cellspacing="0" border="0">
                    <tr>
                        <td style="text-align: left; width: 180px">
                            <span style="color: Red">*</span>&nbsp;Nuevo Estado
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlState" runat="server" AutoPostBack="true" DataSourceID="odsStates"
                                DataTextField="Name" DataValueField="Id" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                                <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="odsStates" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.PQRS.BF.Domain.State"></asp:ObjectDataSource>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlState"
                                Display="None" ErrorMessage="El nuevo estado es requerido." InitialValue="">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr id="trScalingArea" runat="server" visible="false">
                        <td style="text-align: left">
                            <span style="color: Red">*</span>&nbsp;Area de Escalamiento
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlScalingAreas" runat="server" DataSourceID="odsScalingAreas"
                                DataTextField="Name" DataValueField="Id" AppendDataBoundItems="true">
                                <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="odsScalingAreas" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.PQRS.BF.Domain.ScalingArea"></asp:ObjectDataSource>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlScalingAreas"
                                Display="None" ErrorMessage="El área de escalamiento es requerida." InitialValue="">*</asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td>Tipo</td>
                        <td>
                            <asp:DropDownList ID="ddlType" runat="server" DataSourceID="odsType" AutoPostBack="true"
                                DataTextField="Name" DataValueField="Id" AppendDataBoundItems="true" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="odsType" runat="server" SelectMethod="GetAllTypes" TypeName="OnData.TitanFL.Systems.PQRS.BF.Domain.Type"></asp:ObjectDataSource>
                        </td>
                    </tr>
                    <tr>
                        <td>Subtipo</td>
                        <td>
                            <asp:DropDownList ID="ddlSubType" runat="server" DataTextField="Name" DataValueField="Id" AppendDataBoundItems="true">
                                <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left">
                            <span style="color: Red">*</span>&nbsp;Descripción
                        </td>
                        <td>
                            <asp:TextBox ID="txtDescription" Height="96px" Width="100%" runat="server" TextMode="MultiLine"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidatorDescription" runat="server"
                                ControlToValidate="txtDescription" Display="None" ErrorMessage="La descripción es requerida.">*</asp:RequiredFieldValidator>
                            <asp:CustomValidator ID="cvDescripcion" runat="server" ErrorMessage="La descripción no puede tener más de 4000 caracteres"
                                ControlToValidate="txtDescription" ClientValidationFunction="ClientValidate512"
                                Display="None">*</asp:CustomValidator>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnGuardarDetalle" runat="server" Text="Guardar" OnClick="btnGuardarDetalle_Click" />
                            <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
                                ShowSummary="False" />
                        </td>
                    </tr>
                </table>
                <hr />
            </td>
        </tr>
        <tr>
            <td>
                <div>
                    <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="20" GridLines="None" Width="100%" OnNeedDataSource="rgList_NeedDataSource">
                        <HeaderContextMenu EnableAutoScroll="True">
                        </HeaderContextMenu>
                        <MasterTableView TableLayout="Fixed" Width="100%">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Fecha" DataField="Date">
                                    <ItemStyle Width="160px" />
                                    <HeaderStyle Width="160px" />
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Estado" DataField="State">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Area de Escalamiento" DataField="ScalingArea"
                                    EmptyDataText="N/A">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Descripción" DataField="Description">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Usuario" DataField="UserName">
                                </telerik:GridBoundColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                Sin seguimientos registrados</NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                </div>
            </td>
        </tr>
    </table>
</telerik:RadAjaxPanel>
<script type="text/javascript" language="javascript">
    function ClientValidate512(source, arguments) {
        if (arguments.Value.length <= 4000)
            arguments.IsValid = true;
        else
            arguments.IsValid = false;
    }
</script>
