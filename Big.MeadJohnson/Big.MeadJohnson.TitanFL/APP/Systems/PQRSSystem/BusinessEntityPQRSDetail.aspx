﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/Popup.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityPQRSDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.BusinessEntityPQRSDetail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation-min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit2">
        <asp:PlaceHolder ID="controlContainer" runat="server"></asp:PlaceHolder>
        <script type="text/javascript" language="javascript">

            function ClosePQRS() {
                alert("PQRS Registrado");
                parent.ReloadPage();
            }
        </script>
    </div>
</asp:Content>
