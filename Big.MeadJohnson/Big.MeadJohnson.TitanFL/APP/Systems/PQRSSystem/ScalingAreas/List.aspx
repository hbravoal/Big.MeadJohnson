﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master"
    AutoEventWireup="true" CodeBehind="List.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.ScalingAreas.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="4" cellspacing="0" width="100%" border="0">

            <tr>
                <td style="width: 100px">Nombre del Área:
                </td>
                <td>
                    <asp:TextBox ID="txtAreaName" runat="server" MaxLength="50"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">Negocio:
                </td>
                <td>
                    <asp:DropDownList ID="ddlBusiness" runat="server" AppendDataBoundItems="true" Width="100px"
                        DataTextField="BUSINESSNAME" DataValueField="BUSINESSGUID">
                        <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">Activo:
                </td>
                <td>
                    <asp:DropDownList ID="ddlActive" runat="server" Width="100px">
                        <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                        <asp:ListItem Text="Activos" Value="1"></asp:ListItem>
                        <asp:ListItem Text="Inactivos" Value="0"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td width="100%" colspan="4">
                    <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                        PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource">
                        <MasterTableView TableLayout="Auto" Width="100%">
                            <Columns>
                                <telerik:GridBoundColumn HeaderText="Nombre Área" DataField="NAME">
                                </telerik:GridBoundColumn>
                                <telerik:GridBoundColumn HeaderText="Responsable" DataField="RESPONSIBLE">
                                </telerik:GridBoundColumn>
                                <%--<telerik:GridCheckBoxColumn HeaderText="Activo" ReadOnly="true" DataField="ISACTIVE">
                                </telerik:GridCheckBoxColumn>--%>
                                <telerik:GridTemplateColumn UniqueName="ISACTIVE" HeaderText="Activo">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# (Decimal)Eval("ISACTIVE")==0?false:true %>' Enabled="false" />
                                    </ItemTemplate>
                                </telerik:GridTemplateColumn>
                                <telerik:GridTemplateColumn UniqueName="EditColumn">
                                    <ItemTemplate>
                                        <a href='Detail.aspx?scalingAreaGuid=<%# Eval("Guid") %>' title="Ver Detalle">
                                            <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                        </a>
                                    </ItemTemplate>
                                    <ItemStyle Width="35px" HorizontalAlign="Center" />
                                    <HeaderStyle Width="35px" />
                                </telerik:GridTemplateColumn>
                            </Columns>
                            <NoRecordsTemplate>
                                Sin Áreas registradas
                            </NoRecordsTemplate>
                        </MasterTableView>
                    </telerik:RadGrid>
                    <div style="width: 100%; text-align: right">
                        <br />
                        <asp:Button ID="btnNew" runat="server" OnClientClick="window.location.href='Detail.aspx';return false;"
                            Text="Ingresar" />
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
