﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="Detail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.ScalingAreas.Detail" %>

<%@ Register Src="~/Controls/ResultMessage.ascx" TagPrefix="uc1" TagName="ResultMessage" %>


<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <div style="text-align: left">
            <table cellpadding="3" cellspacing="0" width="100%" border="0">
                <tr>
                    <td bgcolor="LightSteelBlue">
                        <strong>Información de Área</strong>
                    </td>
                </tr>
                <tr>
                    <td align="left">
                        <table cellpadding="4" cellspacing="0" width="100%" border="0">
                            <tr>
                                <td style="text-align: right;" width="100px">
                                    <span style="color: Red">*</span>&nbsp;Nombre</td>
                                <td>
                                    <asp:TextBox ID="txtName" runat="server" Width="250px" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator
                                        ID="RequiredFieldValidatorName" runat="server" ControlToValidate="txtName" ErrorMessage="Se requiere el nombre."
                                        Display="None">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="100px">
                                    <span style="color: Red">*</span>&nbsp;Responsable</td>
                                <td>
                                    <asp:TextBox ID="txtResponsible" runat="server" Width="250px" MaxLength="50"></asp:TextBox><asp:RequiredFieldValidator
                                        ID="RequiredFieldValidatorResponsible" runat="server" ControlToValidate="txtResponsible" ErrorMessage="Se requiere el responsable."
                                        Display="None">*</asp:RequiredFieldValidator></td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="100px">
                                    <span style="color: Red">*</span>&nbsp;Email de Notificación</td>
                                <td>
                                    <asp:TextBox ID="txtEmail" runat="server" Width="250px" MaxLength="50"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidatorEmail" runat="server" ControlToValidate="txtEmail" ErrorMessage="Se requiere el email de notificacón."
                                        Display="None">*</asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="validarCorreo"
                                    ControlToValidate="txtEmail"
                                    runat="server"
                                    ErrorMessage="Correo Inválido"
                                    Text="Ejemplo: correo@dominio.com"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" Display="None"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: right;" width="100px">Supervisor</td>
                                <td>
                                    <asp:DropDownList ID="ddlSupervisor" runat="server" AppendDataBoundItems="true"
                                        DataTextField="Name" DataValueField="Id" Width="255px">
                                        <asp:ListItem Value="" Text="Seleccione"></asp:ListItem>
                                    </asp:DropDownList>

                                </td>
                            </tr>
                          
                            
                            <tr>
                                <td style="text-align: right;">Activo</td>
                                <td>
                                    <asp:CheckBox ID="chkEnabled" runat="server" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="LightSteelBlue">
                        <strong>Negocios</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBoxList ID="cblBusiness" runat="server" DataTextField="BUSINESSNAME" DataValueField="BUSINESSGUID">

                        </asp:CheckBoxList>
                    </td>
                </tr>
            </table>
           
        </div>
        <table>
            <tr>
                <td>
                    <asp:Button ID="btnSave" runat="server" Text="Guardar" OnClick="btnSave_Click" CausesValidation="true" /></td>
   
                <td>
                    <asp:Button ID="btnCancelar" runat="server" Text="Cancelar" CausesValidation="False" OnClick="btnCancelar_Click" /></td>
            </tr>
            <tr>
                <td colspan="2">
                    <uc1:ResultMessage runat="server" ID="ResultMessage" />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="ValidationSummary" runat="server" ShowMessageBox="True"
            ShowSummary="False" />
    </div>

</asp:Content>
