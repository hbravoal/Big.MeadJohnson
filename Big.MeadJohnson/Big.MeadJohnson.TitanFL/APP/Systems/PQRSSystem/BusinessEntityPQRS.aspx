﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityPQRS.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.BusinessEntityPQRS" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="0" height="100%" cellspacing="0" width="100%" border="0">
            <tr>
                <td height="24px">
                    <asp:Xml ID="xmlSystemsTabs" runat="server"></asp:Xml>
                </td>
            </tr>
            <tr>
                <td style="border: solid thin gray; padding: 5px 5px 5px 5px" colspan="2">
                    <br />
                    <table cellpadding="4" cellspacing="0" width="100%" border="0">
                        <tr>
                            <td bgcolor="LightSteelBlue" colspan="2">
                                <strong>Registrar PQRS</strong>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 120px">
                                Tipo
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlType" runat="server" DataSourceID="odsTypes" DataTextField="Name"
                                    DataValueField="Id" AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                                    <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                </asp:DropDownList>
                                <asp:ObjectDataSource ID="odsTypes" runat="server" SelectMethod="GetTypesWithSubtypeInRole"
                                    TypeName="OnData.TitanFL.Systems.PQRS.BF.Domain.Type">
                                    <SelectParameters>
                                        <asp:Parameter Name="roleGuid" Type="String" />
                                    </SelectParameters>
                                </asp:ObjectDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 120px">
                                Subtipo
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlSubtypes" runat="server" DataTextField="Name" DataValueField="Id"
                                    AppendDataBoundItems="true">
                                    <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; width: 120px">
                                Punto de Contacto
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlContactPoints" runat="server" DataTextField="DESCRIPTION"
                                    DataValueField="ID" AppendDataBoundItems="true">
                                    <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:Button ID="btnAdd" runat="server" OnClientClick="javascript:openNew();return false;"
                                    Text="Registrar" />
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td bgcolor="LightSteelBlue" colspan="2">
                                <strong>PQRS Registrados</strong>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <div style="height: 100%; padding-bottom: 30px">
                                    <telerik:RadScriptBlock runat="server" ID="RadScriptBlock1">
                                        <script type="text/javascript">

                                            function GridCreated(sender, eventArgs) {
                                                var gridContainer = document.getElementById("ctl00_maincontent_rgList_GridData");
                                                if (gridContainer != null)
                                                    gridContainer.style.height = "100%";
                                            }
                                        </script>
                                    </telerik:RadScriptBlock>
                                    <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                        PageSize="10" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                                        OnItemDataBound="rgList_ItemDataBound">
                                        <HeaderContextMenu EnableAutoScroll="True">
                                        </HeaderContextMenu>
                                        <ExportSettings ExportOnlyData="true" Excel-Format="ExcelML" FileName="PQRS" IgnorePaging="true"
                                            OpenInNewWindow="true">
                                        </ExportSettings>
                                        <MasterTableView TableLayout="Fixed" Width="100%">
                                            <Columns>
                                                <telerik:GridTemplateColumn UniqueName="EditColumn">
                                                    <ItemTemplate>
                                                        <a href='javascript:openDetail("<%#Eval("GUID") %>");' title="Ver Detalle">
                                                            <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                                        </a>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="35px" />
                                                    <HeaderStyle Width="35px" />
                                                </telerik:GridTemplateColumn>
                                                <telerik:GridBoundColumn HeaderText="Fecha" DataField="PQRSDate">
                                                    <ItemStyle Width="160px" />
                                                    <HeaderStyle Width="160px" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Tipo de Solicitud" DataField="Type">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Subtipo de Solicitud" DataField="SubType">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Estado" DataField="State">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Punto de Contacto" DataField="ContactPoint">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Area de Escalamiento" DataField="ScalingArea"
                                                    EmptyDataText="N/A">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Descripción" DataField="Description" Display="false"
                                                    UniqueName="Description">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Detalle" DataField="SubtypeDetail" Display="false"
                                                    UniqueName="SubtypeDetail">
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Fecha Última Gestión" DataField="LastStateDate">
                                                    <ItemStyle Width="160px" />
                                                    <HeaderStyle Width="160px" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn HeaderText="Fecha Vencimiento" DataField="ExpirationDate"
                                                    EmptyDataText="N/A">
                                                    <ItemStyle Width="160px" />
                                                    <HeaderStyle Width="160px" />
                                                </telerik:GridBoundColumn>
                                                <telerik:GridTemplateColumn Display="false">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="chkIsExpired" runat="server" Checked='<%#Eval("IsExpired") %>' /></ItemTemplate>
                                                </telerik:GridTemplateColumn>
                                            </Columns>
                                            <NoRecordsTemplate>
                                                Sin PQRS encontrados
                                            </NoRecordsTemplate>
                                        </MasterTableView>
                                        <ClientSettings AllowDragToGroup="false">
                                            <ClientEvents OnGridCreated="GridCreated"></ClientEvents>
                                            <Selecting AllowRowSelect="false" />
                                            <Scrolling UseStaticHeaders="true" AllowScroll="true" ScrollHeight="100%" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <telerik:RadWindowManager ID="rdPopUp" runat="server" ShowContentDuringLoad="false">
        <Windows>
            <telerik:RadWindow ID="radWindow" runat="server" Width="700px" Height="550px" Behaviors="Close"
                InitialBehaviors="Close" Modal="true" ReloadOnShow="true" ShowContentDuringLoad="false"
                VisibleStatusbar="false" NavigateUrl="BusinessEntityPQRSDetail.aspx" OnClientClose="ReloadPage">
            </telerik:RadWindow>
        </Windows>
    </telerik:RadWindowManager>
    <script type="text/javascript">
        function openNew() {

            var typeId = document.getElementById('<%=ddlType.ClientID %>').options[document.getElementById('<%=ddlType.ClientID %>').selectedIndex].value;
            var subtypeId = document.getElementById('<%=ddlSubtypes.ClientID %>').options[document.getElementById('<%=ddlSubtypes.ClientID %>').selectedIndex].value;
            var contactPointId = document.getElementById('<%=ddlContactPoints.ClientID %>').options[document.getElementById('<%=ddlContactPoints.ClientID %>').selectedIndex].value;

            if (typeId == "") {
                alert("Debe seleccionar un tipo de PQRS");
                return false;
            }

            if (subtypeId == "") {
                alert("Debe seleccionar un subtipo de PQRS");
                return false;
            }

            if (contactPointId == "") {
                alert("Debe seleccionar un punto de contacto de PQRS");
                return false;
            }

            radopen("BusinessEntityPQRSDetail.aspx?typeId=" + typeId
            + "&subtypeId=" + subtypeId
            + "&contactPointId=" + contactPointId
            + "&viewName=" + viewName
            + "&entityGuid=" + entityGuid
            , "radWindow");
        }

        function openDetail(guid) {
            radopen('<%=Page.ResolveUrl("~/APP/Systems/PQRSSystem/Detail.aspx") %>?guid=' + guid
                + "&window=true"
                , "radWindow");
        }

        function ReloadPage(sender, eventArgs) {
            var oManager = GetRadWindowManager();
            oManager.CloseAll();
            window.location.href = window.location.href;
        }
    </script>
</asp:Content>
