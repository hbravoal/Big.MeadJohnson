﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Detail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.Detail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <asp:PlaceHolder ID="controlContainer" runat="server"></asp:PlaceHolder>
    </div>
    
</asp:Content>
