﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="List.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.PQRSSystem.List" %>

<%@ Register Src="~/Controls/ControlFecha.ascx" TagName="ControlFecha" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
    </telerik:RadAjaxLoadingPanel>
    <telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel"
        ClientEvents-OnRequestStart="pnlRequestStarted" EnableAJAX="false">
        <script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/event/event-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/dom/dom-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
        <script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
        <script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
        <link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
        <telerik:RadScriptBlock ID="scriptBlock" runat="server">
            <script>
                function pnlRequestStarted(ajaxPanel, eventArgs) {


                    if (eventArgs.EventTarget == "ctl00$maincontent$rgList$ctl00$ctl02$ctl00$ExportToExcelButton") {

                        eventArgs.EnableAjax = false;
                    }
                }
            </script>
        </telerik:RadScriptBlock>
        <div id="adminedit">
            <table cellpadding="4" cellspacing="0" width="100%" border="0">
                <tr>
                    <td>
                        Fecha inicial
                    </td>
                    <td>
                        <uc1:ControlFecha ID="cfInitialDate" runat="server" AllowNulls="true" />
                    </td>
                    <td>
                        Fecha final
                    </td>
                    <td>
                        <uc1:ControlFecha ID="cfEndDate" runat="server" AllowNulls="true" />
                    </td>
                </tr>
                <tr>
                    <td>
                        Rol
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlViews" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlViews_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="text-align: left">
                        Punto de contacto
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlContactPoints" runat="server" DataTextField="DESCRIPTION"
                            DataValueField="ID" AppendDataBoundItems="true">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left">
                        Tipo
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlType" runat="server" DataTextField="Name" DataValueField="Id"
                            AppendDataBoundItems="true" AutoPostBack="True" OnSelectedIndexChanged="ddlType_SelectedIndexChanged">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td style="text-align: left">
                        Subtipo
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSubtypes" runat="server" DataTextField="Name" DataValueField="Id"
                            AppendDataBoundItems="true">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Estado
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlStatus" runat="server" DataSourceID="odsStatus" DataTextField="Name"
                            DataValueField="Id" AppendDataBoundItems="true">
                            <asp:ListItem Text="[Seleccione]" Value=""></asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="odsStatus" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.PQRS.BF.Domain.State"
                            OldValuesParameterFormatString="original_{0}"></asp:ObjectDataSource>
                    </td>
                    <td>
                        Area de escalamiento
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlScalingAreas" runat="server" DataTextField="Name" DataValueField="Id">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click"
                            ValidationGroup="PQRS" />
                        <asp:ValidationSummary ID="vsPQRS" runat="server" ValidationGroup="PQRS" ShowMessageBox="true"
                            ShowSummary="false" />
                       
                    </td>
                </tr>
                <tr>
                    <td width="100%" colspan="4">
                        <div style="height: 100%">
                            <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                                PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                                OnItemDataBound="rgList_ItemDataBound" OnItemCommand="rgList_ItemCommand" >
                                <ExportSettings ExportOnlyData="true" Excel-Format="ExcelML" FileName="PQRS" IgnorePaging="true"
                                    OpenInNewWindow="true">
                                </ExportSettings>
                                <MasterTableView TableLayout="Auto" Width="100%" CommandItemDisplay="Top" UseAllDataFields="true">
                                    <CommandItemSettings ShowAddNewRecordButton="false" ShowExportToExcelButton="true"
                                        ShowRefreshButton="false" ExportToExcelText="Exportar a Excel" />
                                    <Columns>
                                        <telerik:GridTemplateColumn>
                                            <ItemTemplate>
                                                <a href='Detail.aspx?guid=<%# Eval("GUID") %>' title="Ver Detalle">
                                                    <asp:Image ID="imgEdit" runat="server" ImageUrl="~/APP/Images/icon-edit.gif" BorderWidth="0px" />
                                                </a>
                                            </ItemTemplate>
                                            <ItemStyle Width="35px" />
                                            <HeaderStyle Width="35px" />
                                        </telerik:GridTemplateColumn>
                                        <telerik:GridBoundColumn HeaderText="Fecha" DataField="PQRSDate" DataFormatString="{0:dd/MM/yyyy}">
                                            <ItemStyle Width="160px" />
                                            <HeaderStyle Width="160px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Rol" DataField="RoleName">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Tipo de Solicitud" DataField="Type">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Subtipo de Solicitud" DataField="SubType">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Estado" DataField="State">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Punto de Contacto" DataField="ContactPoint">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Area de Escalamiento" DataField="ScalingArea"
                                            EmptyDataText="N/A">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Descripción" DataField="Description" Display="false"
                                            UniqueName="DescriptionColumn">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Detalle" DataField="SubtypeDetail" Display="false"
                                            UniqueName="SubtypeDetailColumn">
                                        </telerik:GridBoundColumn>
                                        <telerik:GridBoundColumn HeaderText="Fecha Vencimiento" DataField="ExpirationDate"
                                            DataFormatString="{0:dd/MM/yyyy}" EmptyDataText="N/A">
                                            <ItemStyle Width="160px" />
                                            <HeaderStyle Width="160px" />
                                        </telerik:GridBoundColumn>
                                        <telerik:GridTemplateColumn Display="false">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkIsExpired" runat="server" Checked='<%#Eval("IsExpired") %>' /></ItemTemplate>
                                        </telerik:GridTemplateColumn>
                                    </Columns>
                                    <NoRecordsTemplate>
                                        Sin PQRS encontrados
                                    </NoRecordsTemplate>
                                </MasterTableView>
                            </telerik:RadGrid>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </telerik:RadAjaxPanel>
</asp:Content>
