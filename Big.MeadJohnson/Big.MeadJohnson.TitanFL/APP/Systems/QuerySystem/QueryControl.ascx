﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QueryControl.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Systems.QuerySystem.QueryControl" %>
<script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>
<script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>
<script type="text/javascript" src="../../Scripts/TitanFL.js"></script>
<script type="text/javascript" src="../../UIProviders/QuerySystem/QueryBuilderUIProvider.js"></script>
<link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
<div id="adminedit">
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr style="height: 24px">
            <td style="height: 24px">
                <img alt="Consultas almacenadas" id="imgQueries" src="../../Images/consultas1.jpg"
                    onclick="ShowQueries()" />
            </td>
            <td style="height: 24px">
                <img alt="Importar consultas" id="imgImportQuery" src="../../Images/importarConsulta2.jpg"
                    onclick="ShowImportQuery()" />
            </td>
            <td style="height: 24px">
                <img alt="Consulta" id="imgQuery" src="../../Images/consulta2.jpg" onclick="ShowQuery()" />
            </td>
            <td style="height: 24px">
                <img alt="Resultados" id="imgResult" src="../../Images/resultado2.jpg" onclick="ShowResult()" />
            </td>
            <td style="width: 100%; background-image: url(../../Images/consultas_raya.jpg); background-repeat: repeat-x">
            </td>
        </tr>
    </table>
    <br />
    <div id="divQuery" style="display: none">
        <table width="100%" style="height: 100%" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="height: 22px; width: 100px" valign="middle">
                    Sistema:
                </td>
                <td valign="top">
                    <select id="cboSystems" runat="server" onchange="GetEntitiesBySystem(this.value)">
                        <option value="">-- Seleccione --</option>
                    </select>
                </td>
                <td rowspan="2" style="width: 400px">
                    <table width="100%" cellpadding="2" cellspacing="0" border="0">
                        <tr>
                            <td>
                                Nombre
                            </td>
                            <td>
                                <asp:TextBox ID="txtNameQuery" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="rfvNameQuery" runat="server" ErrorMessage="Se requiere un nombre para la consulta."
                                    ControlToValidate="txtNameQuery" Display="None" ValidationGroup="vgSaveQuery">*</asp:RequiredFieldValidator>
                                <asp:Button ID="btnSaveQuery" runat="server" Text="Guardar" OnClick="btnSaveQuery_Click"
                                    Enabled="false" ValidationGroup="vgSaveQuery" />
                            </td>
                        </tr>
                        <tr>
                            <td style="border-left-width: medium; border-left-color: Black">
                                Descripción
                            </td>
                            <td>
                                <asp:TextBox ID="txtDescriptionQuery" runat="server" Width="400px" MaxLength="256"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </td>
                <td rowspan="2" style="width: 100px" valign="top" align="right">
                    &nbsp; <span id="imgLoading" style="display: none;">
                        <img alt="" src="../../Images/Wait/busy_twirl2_1.gif" />&nbsp;Cargando... </span>
                </td>
            </tr>
            <tr>
                <td style="height: 22px; width: 100px" valign="middle">
                    Entidades:
                </td>
                <td valign="top">
                    <span id="spanEntities" runat="server"></span>
                    <input type="hidden" id="hdnEntities" runat="server" />
                    <input type="hidden" id="hdnMainEntity" runat="server" />
                </td>
            </tr>
        </table>
        <table width="100%" style="height: 100%" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width: 200px">
                    Entidades relacionadas
                </td>
                <td style="width: 200px">
                    Propiedades para seleccionar
                </td>
                <td>
                    Propiedades a mostrar
                </td>
            </tr>
            <tr>
                <td>
                    <div style="height: 165px; width: 200px; overflow: scroll; border-style: outset;
                        border-bottom-width: thin">
                        <div id="divRelatedEntities" style="width: 300px" runat="server">
                        </div>
                        <input type="hidden" id="hdnRelatedEntities" runat="server" />
                        <input type="hidden" id="hdnSelectedEntities" runat="server" />
                    </div>
                </td>
                <td>
                    <div style="height: 165px; width: 200px; overflow: scroll; border-style: outset;
                        border-bottom-width: thin">
                        <div id="divPropertiesToSelect" style="width: 300px" runat="server">
                        </div>
                        <input type="hidden" id="hdnPropertiesToSelect" runat="server" />
                    </div>
                </td>
                <td>
                    <div style="height: 165px; overflow: scroll; border-style: outset; border-bottom-width: thin">
                        <div id="divSelectedProps" style="width: 500px" runat="server">
                        </div>
                        <input type="hidden" id="hdnSelectedProps" runat="server" />
                        <input type="hidden" id="hdnProperties" runat="server" />
                    </div>
                </td>
            </tr>
        </table>
        <table width="100%" style="height: 100%" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td style="width: 200px">
                    Acciones
                </td>
                <td style="width: 200px">
                    Propiedades para filtrar
                </td>
                <td>
                    Filtros
                </td>
            </tr>
            <tr>
                <td>
                    <div style="width: 203px; text-align: center; border-right: #abbdd3 thin solid; border-top: #abbdd3 thin solid;
                        border-left: #abbdd3 thin solid; border-bottom: #abbdd3 thin solid; height: 145px">
                        <input type="hidden" id="hdnEntitiesList" runat="server" />
                        <input type="hidden" id="hdnPropertiesList" runat="server" />
                        <input type="hidden" id="hdnPropertiesListDisplay" runat="server" />
                        <input type="hidden" id="hdnOrdersList" runat="server" />
                        <input type="hidden" id="hdnFiltersList" runat="server" />
                        <br />
                        <br />
                        <asp:Button ID="btnNueva" Text="Nueva consulta" Width="120px" runat="server" OnClientClick="NewQuery();"
                            OnClick="btnNueva_Click" />
                        <br />
                        <br />
                        <asp:Button ID="btnEjecutar" Text="Ejecutar consulta" Width="120px" runat="server"
                            OnClientClick="return RunQuery();" OnClick="btnEjecutar_Click" />
                        <br />
                        <asp:CheckBox ID="chkEliminarReps" Text="Eliminar registros repetidos" runat="server" />
                        <input type="hidden" id="hdnIsTargetQuery" value="<%=Request.QueryString["Targeting"] == null ? '0' : '1'%>" />
                        <br />
                        <br />
                    </div>
                </td>
                <td>
                    <div style="width: 200px; height: 145px; overflow: scroll; border-style: outset;
                        border-bottom-width: thin">
                        <div id="divSearchableProps" style="width: 300px" runat="server">
                        </div>
                        <input type="hidden" id="hdnSearchableProps" runat="server" />
                        <input type="hidden" id="hdnSelectedFilters" runat="server" />
                    </div>
                </td>
                <td>
                    <div style="height: 145px; overflow: scroll; border-style: outset; border-bottom-width: thin">
                        <div id="divFilters" style="width: 500px" runat="server">
                        </div>
                        <input type="hidden" id="hdnFilters" runat="server" />
                    </div>
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="vsQuery" runat="server" ShowMessageBox="True" ShowSummary="False"
            ValidationGroup="vgSaveQuery" />
    </div>
    <div id="divResult" style="display: none">
        <table width="100%" style="height: 100%" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td>
                    <asp:GridView ID="grdResultado" runat="server" AllowPaging="True" AllowSorting="True"
                        PageSize="20" OnPageIndexChanging="grdResultado_PageIndexChanging" OnSorting="grdResultado_Sorting">
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <b>
                        <asp:Label ID="lblTotalRows" runat="server" BorderStyle="none"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnExportarResultado" Text="Exportar datos" Width="120px" OnClick="btnExportarResultado_Click"
                        Enabled="false" runat="server" />
                    <input type="button" id="btnCerrar" value="Cerrar" style="visibility: hidden" onclick="javascript:parent.hidePopWin(false);parent.ReloadTable();" />
                </td>
            </tr>
        </table>
    </div>
    <div id="divQueries" style="display: block">
        <asp:Button ID="btnNewQuery" runat="server" Text="Nueva Consulta" OnClick="btnNewQuery_Click" />
        <br />
        <br />
        <asp:GridView ID="gvQueries" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" DataSourceID="QueriesDataSource" ForeColor="#333333" GridLines="None"
            Width="100%" DataKeyNames="Guid" OnRowCommand="gvQueries_RowCommand">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDeleteQuery" ImageUrl="../../Images/icon-delete.gif" runat="server"
                            CommandName="Delete" OnClientClick="return confirm('¿Realmente deseas eliminar este registro?');" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                    <HeaderStyle Width="5%" />
                </asp:TemplateField>
                <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Name">
                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="Descripci&#243;n" SortExpression="Description">
                    <HeaderStyle HorizontalAlign="Left" Width="50%" />
                </asp:BoundField>
                <asp:ButtonField ButtonType="Button" CommandName="Editar" Text="Editar">
                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                </asp:ButtonField>
                <asp:ButtonField ButtonType="Button" CommandName="Ejecutar" Text="Ejecutar">
                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                </asp:ButtonField>
                <asp:ButtonField ButtonType="Button" CommandName="Guardar" Text="Descargar">
                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                </asp:ButtonField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdfGuid" runat="server" Value='<%# Bind("Guid") %>' />
                    </ItemTemplate>
                    <HeaderStyle Width="5%" />
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EditRowStyle BackColor="#999999" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <asp:ObjectDataSource ID="QueriesDataSource" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.Queries.BF.Domain.QueryEntity"
            OnDeleted="QueriesDataSource_Deleted" OnDeleting="QueriesDataSource_Deleting"
            DeleteMethod="DeleteQuery">
            <DeleteParameters>
                <asp:Parameter Name="Guid" Type="String" />
            </DeleteParameters>
        </asp:ObjectDataSource>
        <asp:HiddenField ID="hdfEditQuery" runat="server" />
    </div>
    <div id="divImportQuery" style="display: none">
        <table border="0" cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td style="width: 10%; text-align: right">
                    Nombre
                </td>
                <td>
                    <asp:TextBox ID="txtNameImport" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNameImport" runat="server" ErrorMessage="Se requiere un nombre para la consulta."
                        ControlToValidate="txtNameImport" Display="None" ValidationGroup="vgSaveQueryImport">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 10%; text-align: right">
                    Descripción
                </td>
                <td style="width: 90%">
                    <asp:TextBox ID="txtDescriptionImport" runat="server" Width="50%" MaxLength="256"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 10%; text-align: right">
                    Archivo
                </td>
                <td style="width: 90%">
                    <asp:FileUpload ID="fileUpload" runat="server" Width="60%" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnImportar" runat="server" Text="Importar" OnClick="btnImportar_Click"
                        ValidationGroup="vgSaveQueryImport" />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="vsImportQuery" runat="server" ShowMessageBox="True" ShowSummary="False"
            ValidationGroup="vgSaveQueryImport" />
    </div>
    <div id="dlgManageOutOfRangeInstances" style="visibility: hidden;">
        <div id="bd">
            <table width="100%" border="0" cellpadding="10px">
                <tr>
                    <td style="width: 85%; text-align: right; vertical-align: middle">
                        Que comience por:&nbsp;&nbsp;<input type="text" id="txtFindValue" style="width: 60%"
                            value="" />
                    </td>
                    <td style="width: 30%">
                        &nbsp;<input type="button" id="btnFindValue" value="Buscar" onclick="findValue()" />
                    </td>
                </tr>
                <tr>
                    <td style="width: 80%; text-align: right">
                        <div id="divSelectOutofRange">
                        </div>
                    </td>
                    <td style="width: 30%">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
<script language="javascript" type="text/javascript">
    var systemOutOfRangeGral = "";
    var entityOutOfRangeGral = "";
    var propertyOutOfRangeGral = "";
    var oOutOfRangeGral = null;
    var oHiddenOutOfRangeGral = null;

    var handleSuccessGetSystemEntityReferentialValues = function (o) {
        DisableDisplayLoading();
        response = o.responseText;
        if (response !== undefined) {
            response = response.split("<!")[0];

            if (response != '') {
                document.getElementById("divSelectOutofRange").innerHTML = response;
            }
        }
    };

    var callbackGetSystemEntityReferentialValues =
   {
       success: handleSuccessGetSystemEntityReferentialValues,
       failure: handleFailure
   };

    function GetManageOutOfRangeInstances(systemOutOfRange, entityOutOfRange, propertyOutOfRange, oOutOfRange, oHiddenOutOfRange) {
        systemOutOfRangeGral = systemOutOfRange;
        entityOutOfRangeGral = entityOutOfRange;
        propertyOutOfRangeGral = propertyOutOfRange;
        oOutOfRangeGral = document.getElementById(oOutOfRange);
        oHiddenOutOfRangeGral = document.getElementById(oHiddenOutOfRange);

        if (document.getElementById("txtFindValue") != null)
            document.getElementById("txtFindValue").value = '';

        document.getElementById("divSelectOutofRange").innerHTML = "";

        YAHOO.OnData.TitanFL.container.dlgOutOfRange.setHeader("Buscar");
        YAHOO.OnData.TitanFL.container.dlgOutOfRange.show();
    }

    function findValue() {
        var vlrFindValue = "";

        if (document.getElementById("txtFindValue") != null && document.getElementById("txtFindValue").value != '') {
            vlrFindValue = "&FindValue=" + document.getElementById("txtFindValue").value;
            document.getElementById("divSelectOutofRange").innerHTML = "Buscando...";
            makeRequest("../../UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntityReferentialValues&System=" + systemOutOfRangeGral + "&Entity=" + entityOutOfRangeGral + "&PropertyName=" + propertyOutOfRangeGral + vlrFindValue, callbackGetSystemEntityReferentialValues);
        }
        else {
            alert("Digite un patrón de búsqueda");
        }
    }

    function SaveOutOfRange() {
        var mylist = null;

        if (document.getElementById("lstOutOfRange") != null) {
            mylist = document.getElementById("lstOutOfRange");

            if (mylist.length != 0) {
                if (mylist.selectedIndex != -1) {
                    oOutOfRangeGral.value = mylist.options[mylist.selectedIndex].text;
                    oHiddenOutOfRangeGral.value = mylist.options[mylist.selectedIndex].value;
                    YAHOO.OnData.TitanFL.container.dlgOutOfRange.hide();
                }
                else {
                    alert("Debe seleccionar un elemento");
                }
            }
            else {
                alert("No hay elementos para seleccionar");
            }
        }
        else {
            alert("No hay elementos para seleccionar");
        }
    }

    YAHOO.namespace('OnData.TitanFL.container');

    function init() {
        /*************************************************************************
        * Creación del formulario para cuando hay mas una cantidad maxima de datos
        **************************************************************************/
        var handleCancelOutOfRange = function (e) {
            this.hide();
        }

        var handleOKOutOfRange = function (e) {
            SaveOutOfRange();
        }

        YAHOO.OnData.TitanFL.container.dlgOutOfRange = new YAHOO.widget.SimpleDialog("dlgManageOutOfRangeInstances", { visible: false, width: "400px", fixedcenter: true, modal: true, draggable: true });
        YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("buttons", [
																	   { text: "Seleccionar", handler: handleOKOutOfRange, isDefault: true },
																	   { text: "Cancelar", handler: handleCancelOutOfRange }
																   ]);

        listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelOutOfRange, scope: YAHOO.OnData.TitanFL.container.dlgOutOfRange, correctScope: true });
        YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("keylisteners", listeners);

        YAHOO.OnData.TitanFL.container.dlgOutOfRange.render(document.body);
    }

    YAHOO.util.Event.addListener(window, "load", init, "", true)
</script>
