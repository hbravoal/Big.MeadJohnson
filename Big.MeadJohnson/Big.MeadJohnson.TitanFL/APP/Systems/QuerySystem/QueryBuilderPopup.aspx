﻿<%@ Page Title="" Language="C#"  AutoEventWireup="true" CodeBehind="QueryBuilderPopup.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.QuerySystem.QueryBuilderPopup" %>
<%@ Register Src="QueryControl.ascx" TagName="QueryControl" TagPrefix="uc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
   <head id="Head1" runat="server">
      <title>.:: Consulta de targeting ::.</title>
      <link href="../../TitanFL.css" rel="stylesheet" type="text/css" />
   </head>
   <body>
      <form id="Form1" method="post" runat="server">
         <uc1:QueryControl id="QueryControl1" runat="server"></uc1:QueryControl>   
      </form>
   </body>
</html>
