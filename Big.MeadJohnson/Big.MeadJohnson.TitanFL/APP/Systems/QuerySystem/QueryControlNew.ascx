﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="QueryControlNew.ascx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Systems.QuerySystem.QueryControlNew" %>
<%@ Register Assembly="OnData.Framework" Namespace="OnData.Framework.Web.Controls"
    TagPrefix="cc1" %>
<%@ Register Assembly="OnData.Framework" Namespace="OnData.Framework.Web.Controls"
    TagPrefix="cc1" %>

<script type="text/javascript" src="../../Scripts/YUI/yahoo/yahoo-min.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/connection/connection-min.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/event/event.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/dom/dom.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/dragdrop/dragdrop.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/animation/animation.js"></script>

<script type="text/javascript" src="../../Scripts/YUI/container/container.js"></script>

<script type="text/javascript" src="../../Scripts/TitanFL.js"></script>

<script type="text/javascript" src="../../UIProviders/QuerySystem/QueryBuilderUIProviderNew.js"></script>

<link rel="stylesheet" type="text/css" href="../../Scripts/YUI/container/assets/container.css" />
<div id="adminedit">
    <table cellpadding="4" cellspacing="0" width="100%" style="border-bottom: black 1px solid">
        <tr>
            <td style="width: 14%; height: 65px;">
                <div id="divTitleQueries" onclick="ShowQueries()" style="cursor: pointer; background-color: #8294a5;
                    text-align: center; color: white; font-weight: bold; height: 18px;">
                    Consultas
                </div>
            </td>
            <td style="width: 14%; height: 65px;">
                <div id="divTitleImportQuery" onclick="ShowImportQuery()" style="cursor: pointer;
                    background-color: #8baac7; text-align: center; color: white; font-weight: bold;
                    height: 18px;">
                    Importar Consulta
                </div>
            </td>
            <td style="width: 14%; height: 65px;">
                <div id="divTitleQuery" onclick="ShowQuery()" style="cursor: pointer; background-color: #8baac7;
                    text-align: center; color: white; font-weight: bold; height: 18px;">
                    Consulta
                </div>
            </td>
            <td style="width: 14%; height: 65px;">
                <div id="divTitleResult" onclick="ShowResult()" style="cursor: pointer; background-color: #8baac7;
                    text-align: center; color: white; font-weight: bold; height: 18px;">
                    Resultado
                </div>
            </td>
            <td style="width: 44%; height: 65px;">
                &nbsp;</td>
        </tr>
    </table>
    <br />
    <div id="divQueries" style="display: none">
        <asp:Button ID="btnNewQuery" runat="server" Text="Nueva Consulta" OnClick="btnNew_Click" />
        <br />
        <br />
        <asp:GridView ID="gvQueries" runat="server" AllowPaging="True" AutoGenerateColumns="False"
            CellPadding="4" DataSourceID="QueriesDataSource" ForeColor="#333333" GridLines="None"
            Width="100%" DataKeyNames="Guid" OnRowCommand="gvQueries_RowCommand">
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <Columns>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:ImageButton ID="btnDeleteQuery" ImageUrl="../../Images/icon-delete.gif" runat="server"
                            CommandName="Delete" OnClientClick="return confirm('¿Realmente deseas eliminar este registro?');" />
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" />
                    <HeaderStyle Width="5%" />
                </asp:TemplateField>
                <asp:BoundField DataField="Name" HeaderText="Nombre" SortExpression="Name">
                    <HeaderStyle HorizontalAlign="Left" Width="10%" />
                </asp:BoundField>
                <asp:BoundField DataField="Description" HeaderText="Descripci&#243;n" SortExpression="Description">
                    <HeaderStyle HorizontalAlign="Left" Width="50%" />
                </asp:BoundField>
                <asp:ButtonField ButtonType="Button" CommandName="Editar" Text="Editar">
                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                </asp:ButtonField>
                <asp:ButtonField ButtonType="Button" CommandName="Ejecutar" Text="Ejecutar">
                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                </asp:ButtonField>
                <asp:ButtonField ButtonType="Button" CommandName="Guardar" Text="Descargar">
                    <HeaderStyle Width="10%" HorizontalAlign="Center" />
                </asp:ButtonField>
                <asp:TemplateField ShowHeader="False">
                    <ItemTemplate>
                        <asp:HiddenField ID="hdfGuid" runat="server" Value='<%# Bind("Guid") %>' />
                    </ItemTemplate>
                    <HeaderStyle Width="5%" />
                </asp:TemplateField>
            </Columns>
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <EditRowStyle BackColor="#999999" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#8294a5" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        </asp:GridView>
        <asp:ObjectDataSource ID="QueriesDataSource" runat="server" SelectMethod="Get" TypeName="OnData.TitanFL.Systems.Queries.BF.Domain.QueryEntity"
            OnDeleted="QueriesDataSource_Deleted" OnDeleting="QueriesDataSource_Deleting"
            DeleteMethod="DeleteQuery">
            <DeleteParameters>
                <asp:Parameter Name="Guid" Type="String" />
            </DeleteParameters>
        </asp:ObjectDataSource>
        <asp:HiddenField ID="hdfEditQuery" runat="server" />
    </div>
    <div id="divImportQuery" style="display: none">
        <table border="0" cellpadding="3" cellspacing="0" width="100%">
            <tr>
                <td style="width: 10%; text-align: right">
                    Nombre
                </td>
                <td>
                    <asp:TextBox ID="txtNameImport" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvNameImport" runat="server" ErrorMessage="Se requiere un nombre para la consulta."
                        ControlToValidate="txtNameImport" Display="None" ValidationGroup="vgSaveQueryImport">*</asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 10%; text-align: right">
                    Descripción
                </td>
                <td style="width: 90%">
                    <asp:TextBox ID="txtDescriptionImport" runat="server" Width="50%" MaxLength="256"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 10%; text-align: right">
                    Archivo
                </td>
                <td style="width: 90%">
                    <asp:FileUpload ID="fileUpload" runat="server" Width="60%" />
                </td>
            </tr>
            <tr>
                <td>
                </td>
                <td>
                    <asp:Button ID="btnImport" runat="server" Text="Importar" OnClick="btnImport_Click"
                        ValidationGroup="vgSaveQueryImport" />
                </td>
            </tr>
        </table>
        <asp:ValidationSummary ID="vsImportQuery" runat="server" ShowMessageBox="True" ShowSummary="False"
            ValidationGroup="vgSaveQueryImport" />
    </div>
    <div id="divQuery" style="display: none">
        <table cellpadding="3" cellspacing="0" border="0" width="100%">
            <tr>
                <td style="width: 5%" align="center" valign="top">
                    <h1>
                        1.
                    </h1>
                </td>
                <td style="width: 95%">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                Nivel de profundidad para el árbol de la consulta
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="ddlLevel" onchange="DisabledLevel(this)" runat="server">
                                </select>
                                <input type="hidden" id="hdfLevel" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5%" align="center" valign="top">
                    <h1>
                        2.
                    </h1>
                </td>
                <td style="width: 95%">
                    <asp:CheckBox ID="chkEliminarReps" Text="Eliminar registros repetidos" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="width: 5%" align="center">
                    <h1>
                        3.
                    </h1>
                </td>
                <td style="width: 95%">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                Seleccione el sistema
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <select id="cboSystems" runat="server" onchange="GetEntitiesBySystem(this.value)">
                                    <option value="">-- Seleccione --</option>
                                </select>
                                <input type="hidden" id="hdfSystem" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5%" align="center">
                    <h1>
                        4.
                    </h1>
                </td>
                <td style="width: 95%">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                Seleccione la entidad
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <span id="spanEntities" runat="server"></span>
                                <input type="hidden" id="hdnMainEntity" runat="server" />
                                <input type="hidden" id="hdnEntities" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5%" align="center" valign="top">
                    <h1>
                        5.
                    </h1>
                </td>
                <td style="width: 95%">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                Entidades relacionadas
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div style="height: 165px; width: 300px; overflow: scroll; border-style: outset;
                                    border-bottom-width: thin">
                                    <div id="divRelatedEntities" style="width: 300px" runat="server">
                                    </div>
                                    <input type="hidden" id="hdnSelectedEntities" runat="server" />
                                    <input type="hidden" id="hdnRelatedEntities" runat="server" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5%" align="center" valign="top">
                    <h1>
                        6.
                    </h1>
                </td>
                <td style="width: 95%">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td style="width: 40%">
                                <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td>
                                            Propiedades para seleccionar
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="overflow: scroll; border-style: outset; border-bottom-width: thin; width: 400px;
                                                height: 400px;">
                                                <div id="divPropertiesToSelect" style="width: 500px" runat="server">
                                                </div>
                                                <input type="hidden" id="hdnPropertiesToSelect" runat="server" />
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style="width: 60%;">
                                <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                    <tr>
                                        <td valign="top" style="width: 5%;" align="right">
                                            <h1>
                                                7.</h1>
                                        </td>
                                        <td style="width: 95%;">
                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        Propiedades a mostrar
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="height: 175px; width: 450px; overflow: scroll; border-style: outset;
                                                            border-bottom-width: thin">
                                                            <div id="divSelectedProps" style="width: 550px" runat="server">
                                                            </div>
                                                            <input type="hidden" id="hdnProperties" runat="server" />
                                                            <input type="hidden" id="hdnSelectedProps" runat="server" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td valign="top" style="width: 5%;" align="right">
                                            <h1>
                                                8.</h1>
                                        </td>
                                        <td style="width: 95%;">
                                            <table cellpadding="3" cellspacing="0" border="0" width="100%">
                                                <tr>
                                                    <td>
                                                        Filtros
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <div style="height: 175px; width: 450px; overflow: scroll; border-style: outset;
                                                            border-bottom-width: thin">
                                                            <div id="divFilters" style="width: 550px" runat="server">
                                                            </div>
                                                            <input type="hidden" id="hdnSelectedFilters" runat="server" />
                                                            <input type="hidden" id="hdnFilters" runat="server" />
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5%" align="center" valign="top">
                    <h1>
                        9.
                    </h1>
                </td>
                <td style="width: 95%">
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td>
                                Acciones
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="hidden" id="hdnEntitiesList" runat="server" />
                                <input type="hidden" id="hdnPropertiesList" runat="server" />
                                <input type="hidden" id="hdnPropertiesListDisplay" runat="server" />
                                <input type="hidden" id="hdnOrdersList" runat="server" />
                                <input type="hidden" id="hdnFormatList" runat="server" />
                                <input type="hidden" id="hdnFiltersList" runat="server" />
                                <%--<input type="hidden" id="hdnSelectedEntitiesProperties" runat="server" />--%>
                                <asp:Button ID="btnExecute" Text="Ejecutar consulta" Width="120px" runat="server"
                                    OnClientClick="return RunQuery();" OnClick="btnExecute_Click" />
                                <asp:Button ID="btnNew" Text="Nueva consulta" Width="120px" runat="server" OnClientClick="return NewQuery();"
                                    OnClick="btnNew_Click" />&nbsp;&nbsp;
                                <input type="hidden" id="hdnIsTargetQuery" value="<%=Request.QueryString["Targeting"] == null ? '0' : '1'%>" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
    <div id="divResult" style="display: none">
        <table width="100%" style="height: 100%" cellpadding="2" cellspacing="0" border="0">
            <tr>
                <td>
                    <table cellpadding="3" cellspacing="0" border="0" width="100%">
                        <tr>
                            <td style="background-color: #8294a5; text-align: left; color: white; font-weight: bold;">
                                Guardar Consulta
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 120px">
                                <table width="100%" cellpadding="2" cellspacing="0" border="0">
                                    <tr>
                                        <td>
                                            Nombre</td>
                                        <td>
                                            <asp:TextBox ID="txtNameQuery" runat="server" Width="200px" MaxLength="50"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvNameQuery" runat="server" ErrorMessage="Se requiere un nombre para la consulta."
                                                ControlToValidate="txtNameQuery" Display="None" ValidationGroup="vgSaveQuery">*</asp:RequiredFieldValidator>
                                            <asp:Button ID="btnSaveQuery" runat="server" Text="Guardar" OnClick="btnSaveQuery_Click"
                                                Enabled="false" ValidationGroup="vgSaveQuery" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="border-left-width: medium; border-left-color: Black">
                                            Descripción</td>
                                        <td>
                                            <asp:TextBox ID="txtDescriptionQuery" runat="server" Width="400px" MaxLength="256"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <asp:ValidationSummary ID="vsQuery" runat="server" ShowMessageBox="True" ShowSummary="False"
                                    ValidationGroup="vgSaveQuery" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="background-color: #8294a5; text-align: left; color: white; font-weight: bold;">
                    Resultado
                </td>
            </tr>
            <tr>
                <td style="height: 29px">
                    <asp:Button ID="btnExportarResultado" Text="Exportar datos" Width="120px" OnClick="btnExportarResultado_Click"
                        Enabled="false" runat="server" />
                    <input id="btnBefore" type="button" value="Ver consulta" onclick="ShowQuery()" />
                    <input type="button" id="btnCerrar" value="Cerrar" style="visibility: hidden" onclick="javascript:parent.hidePopWin(false);parent.ReloadTable(document.getElementById('QueryControlNew1_hdnTarsystem').value,document.getElementById('QueryControlNew1_hdnTarmainEntity').value,document.getElementById('QueryControlNew1_hdnTarentitiesList').value,document.getElementById('QueryControlNew1_hdnTarpropertiesList').value,document.getElementById('QueryControlNew1_hdnTarpropertiesListDisplay').value,document.getElementById('QueryControlNew1_hdnTarfiltersList').value,document.getElementById('QueryControlNew1_hdnTarordersList').value,document.getElementById('QueryControlNew1_hdnTarformatList').value,document.getElementById('QueryControlNew1_hdnTarwithDuplicates').value);" />
                    <!--<input type="button" id="btnCerrar" value="Cerrar" style="visibility: hidden" onclick="javascript:parent.hidePopWin(false);parent.ReloadTable();" />-->
                    <input type="hidden" id="hdnResult" runat="server" />
                    <asp:HiddenField ID="hdnTarsystem" runat="server" />
                    <asp:HiddenField ID="hdnTarmainEntity" runat="server" />
                    <asp:HiddenField ID="hdnTarentitiesList" runat="server" />
                    <asp:HiddenField ID="hdnTarpropertiesList" runat="server" />
                    <asp:HiddenField ID="hdnTarpropertiesListDisplay" runat="server" />
                    <asp:HiddenField ID="hdnTarfiltersList" runat="server" />
                    <asp:HiddenField ID="hdnTarordersList" runat="server" />
                    <asp:HiddenField ID="hdnTarformatList" runat="server" />
                    <asp:HiddenField ID="hdnTarwithDuplicates" runat="server" />
                </td>
            </tr>
            <tr>
                <td>
                    <div style="height: 350px; width: 950px; overflow: scroll; border-style: outset;
                        border-bottom-width: thin">
                        <asp:GridView ID="grdResultado" runat="server" AllowPaging="True" AllowSorting="True"
                            PageSize="65000" OnPageIndexChanging="grdResultado_PageIndexChanging" OnSorting="grdResultado_Sorting"
                            CellPadding="4" ForeColor="#333333" GridLines="None">
                            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                            <EditRowStyle BackColor="#999999" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#8294A5" Font-Bold="True" ForeColor="White" />
                            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            <PagerSettings Visible="False" />
                        </asp:GridView>
                    </div>
                </td>
            </tr>
            <tr>
                <td width="666" height="26" align="right" bgcolor="#8294a5" >
                    <cc1:HtmlTableHyperLinkPager ID="HtmlTableHyperLinkPager1" runat="server" ShowCurrentPage="True"
                        CurrentPageStringFormat="Página {0} de {1} ({2} registros) &nbsp;-&nbsp;" FirstPageText="../../Images/arrow_left.jpg"
                        LastPageText="../../Images/arrow_right.jpg" NextPageText="Siguiente" PreviousPageText="Anterior"
                        IsDirty="False" Font-Bold="True" ForeColor="White" />
                </td>                
            </tr>
            <tr>
                <td>
                    <b>
                        <asp:Label ID="lblTotalRows" runat="server" BorderStyle="None" Visible="False"></asp:Label>
                    </b>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </div>
</div>
<div id="dlgManageOutOfRangeInstances" style="visibility: hidden;">
    <div id="bd">
        <table width="100%" border="0" cellpadding="10px">
            <tr>
                <td style="width: 85%; text-align: right; vertical-align: middle">
                    Que comience por:&nbsp;&nbsp;<input type="text" id="txtFindValue" style="width: 60%"
                        value="" /></td>
                <td style="width: 30%">
                    &nbsp;<input type="button" id="btnFindValue" value="Buscar" onclick="findValue()" /></td>
            </tr>
            <tr>
                <td style="width: 80%; text-align: right">
                    <div id="divSelectOutofRange">
                    </div>
                </td>
                <td style="width: 30%">
                    &nbsp;</td>
            </tr>
        </table>
    </div>
</div>

<script type="text/javascript">


    var imgLoading = "<span id='imgLoading' style='display: none;'>" +
		"<img alt='' src='../../Images/Wait/busy_twirl2_1.gif' />&nbsp;Cargando... </span>";


    function DisabledLevel(obj) {

        var hidden = document.getElementById('<%=hdfLevel.ClientID %>');
        hidden.value = obj.value;
        obj.disabled = true;
    }

    var handleSuccessGetEntitiesBySystem = function (o) {
        DisplayLoading(false);
        var div = document.getElementById('<%=spanEntities.ClientID %>');
        if (o.responseText != undefined) {
            div.innerHTML = o.responseText;
        }
    }

    var handleSuccessGetRelatedEntities = function (o) {
        DisplayLoading(false);

        var response = new String(o.responseText);
        var entitiesList = response.split(",");

        var div = document.getElementById('<%=divRelatedEntities.ClientID %>');
        div.innerHTML = "";

        Clean();

        var curSystem = "";
        var prevSystem = "";
        for (var i = 0; i < entitiesList.length; i++) {
            var entity = entitiesList[i].split("|");
            curSystem = entity[0];

            var html = "";
            if (curSystem != prevSystem)
                html = "<b>" + entity[1] + "</b>";

            var idDiv = "div_" + entity[0] + "." + entity[2] + "." + entity[4] + "." + entity[5] + "." + entity[6] + "." + entity[7];
            var idChk = "chk_" + entity[0] + "." + entity[2] + "." + entity[4] + "." + entity[5] + "." + entity[6] + "." + entity[7];
            html = html + "<div id='" + idDiv + "'>" +
			"<input type='checkbox' id='" + idChk + "' value='" + entity[2] +
			"' onclick='SetChecked(this);GetEntityProperties(\"" + entity[0] + "\", this.value,\"" + entity[4] + "\",\"" + entity[5] + "\",\"" + entity[6] + "\",\"" + entity[7] + "\", this.checked)'/>" +
			entity[3] + "</div>";
            div.innerHTML = div.innerHTML + html;
            prevSystem = curSystem;
        }
    }

    var handleSuccessGetEntityProperties = function (o) {
        DisplayLoading(false);

        var response = new String(o.responseText);

        if (response !== undefined) {
            response = response.split("<!")[0];
            var div = document.getElementById('<%=divPropertiesToSelect.ClientID %>');
            div.innerHTML = div.innerHTML + response;
        }
    }

    var callbackGetEntitiesBySystem =
{
    success: handleSuccessGetEntitiesBySystem,
    failure: handleFailure,
    argument: ['GetEntitiesBySystem']
};

    var callbackGetRelatedEntities =
{
    success: handleSuccessGetRelatedEntities,
    failure: handleFailure,
    argument: ['GetRelatedEntities']
};

    var callbackGetEntityProperties =
{
    success: handleSuccessGetEntityProperties,
    failure: handleFailure,
    argument: ['GetEntityProperties']
};

    function GetEntitiesBySystem(system) {
        if (system != "") {
            var div = document.getElementById('<%=spanEntities.ClientID %>');
            div.innerHTML = imgLoading;
            var url = "../../UIProviders/QuerySystem/QueryBuilderUIProvider.aspx"
            var method = "MethodToCall=GetEntitiesBySystemNew&SystemName=" + system;
            makeRequest(url, method, callbackGetEntitiesBySystem);

            // Cuando se cambia de sistema, se deben limpiar todos los divs y borrar todo el estado
            // actual de las variables que llevan rastro de las selecciones hechas por el usuario
            var div = document.getElementById('<%=spanEntities.ClientID %>');
            div.innerHTML = div.innerHTML + "";

            Clean();
        }
        else {
            var div = document.getElementById('<%=spanEntities.ClientID %>');
            div.innerHTML = "";
            Clean();
        }
    }

    function GetRelatedEntities(system, entityName) {
        if (system != "" && entityName != "") {
            hidden = document.getElementById('<%=hdnMainEntity.ClientID %>');
            hidden.value = entityName;

            var div = document.getElementById('<%=divRelatedEntities.ClientID %>');
            div.innerHTML = imgLoading;

            var url = "../../UIProviders/QuerySystem/QueryBuilderUIProvider.aspx";
            var method = "MethodToCall=GetRelatedEntitiesNew&System=" + system + "&EntityName=" + entityName;
            makeRequest(url, method, callbackGetRelatedEntities);
        }
        else {
            var div = document.getElementById('<%=divRelatedEntities.ClientID %>');
            div.innerHTML = "";

            var hidden = document.getElementById('<%=hdnMainEntity.ClientID %>');
            hidden.value = "";

            Clean();
        }
    }

    function GetEntityProperties(system, entityName, isMainEntity, sourceProperty, targetRelationalEntity, targetRelationalProperty, checked) {
        if (system != "" && entityName != "") {
            // Si el control que levantó el evento está seleccionado, se llama al evento que se encarga de
            // obtener las propiedades de la entidad dada en parámetros
            if (checked) {
                var div = document.getElementById('<%=divPropertiesToSelect.ClientID %>');
                div.innerHTML = div.innerHTML + imgLoading;

                var context = document.getElementById('hdfContext').value;
                var nivelList = document.getElementById('<%=ddlLevel.ClientID%>');
                var nivelValue = nivelList.options[nivelList.selectedIndex].value;

                var url = "../../UIProviders/QuerySystem/QueryBuilderUIProvider.aspx";
                var method = "MethodToCall=GetEntityProperiesNew&System=" + system + "&EntityName=" + entityName + "&Context=" + context + "&Level=" + nivelValue;
                makeRequest(url, method, callbackGetEntityProperties);

                // Lleva el rastro de las entidades seleccionadas
                var hidden = document.getElementById('<%=hdnSelectedEntities.ClientID%>');
                hidden.value = hidden.value + "div_" + system + "." + entityName + "." + isMainEntity + "." + sourceProperty + "." + targetRelationalEntity + "." + targetRelationalProperty + ",";
            }
            else {
                var hidden = document.getElementById('<%=hdnSelectedEntities.ClientID%>');

                if (hidden.value != "") {
                    hidden.value = hidden.value.replace("div_" + system + "." + entityName + "." + isMainEntity + "." + sourceProperty + "." + targetRelationalEntity + "." + targetRelationalProperty + ",", "");
                    hidden = document.getElementById('<%=hdnEntitiesList.ClientID %>');
                    hidden.value = "";
                    GetEntitiesList();
                }

                // Si el control que levantó el evento NO está seleccionado, se deben eliminar de los divs
                // aquellos nodos que están relacionados con la entidad desmarcada
                DeleteDiv("div_" + entityName + "_ToList");
                DeleteDiv("div_" + entityName + "_Show");
                DeleteDiv("div_" + entityName + "_Filter");

                DeleteHiddenValues('<%=hdnProperties.ClientID%>', entityName);
                DeleteHiddenValues('<%=hdnSelectedFilters.ClientID%>', entityName);
            }
        }
    }

    function ShowProperty(checked, propertyType, idProperty, pathProperty, entityName, pathPropertyValue) {
        // Si el control que levantó el evento está seleccionado, se adiciona un nodo al listado de las
        // propiedades a mostrar y otro al listado de ordenamientos
        if (checked) {
            if ((pathPropertyValue.lastIndexOf(".") + 1) == pathPropertyValue.length) {
                pathPropertyValue = pathPropertyValue.substring(0, pathPropertyValue.length - 1);
            }

            var html = "";
            var div;
            var idDivMain = "div_" + entityName + "_Show";
            var divMain = document.getElementById(idDivMain);

            if (divMain == null)
                html = "<div id='" + idDivMain + "'>";

            var idDiv = "div_" + idProperty + "_List";
            var idTxt = "txt_" + idProperty + "_List";
            var valTxt = pathProperty;
            var idSel = "cbo_" + idProperty + "_List";
            var idSelFormat = "cbo_" + idProperty + "_List_Format";

            html = html + "<div id='" + idDiv + "'>" +
			"<input type='text' title='" + pathProperty + "' id='" + idTxt + "' value='" + valTxt + "' valueProperty='" + pathPropertyValue + "' idProperty='" + idProperty + "' propertyType='" + propertyType + "' readonly='readonly' style='width:56%' />" +
			"<select id='" + idSel + "' onchange='SetSelectedOption(this)'><option>----</option><option>Asc</option><option>Desc</option></select>";

            if (propertyType == 7) {
                html = html + "<select id='" + idSelFormat + "' onchange='SetSelectedOption(this)'><option value='----'>----</option><option value=\"TO_CHAR({0}, 'yyyy/mm/dd')\">AAAA/MM/DD</option></select>";
            }

            html = html + "</div>";

            if (divMain == null) {
                html = html + "</div>";
                div = document.getElementById('<%=divSelectedProps.ClientID %>');
                div.innerHTML = div.innerHTML + html;
            }
            else {
                divMain.innerHTML = divMain.innerHTML + html;
            }

            //Lleva el rastro de los divs adicionados para posteriormente poderlos borrar con facilidad 
            var hidden = document.getElementById('<%=hdnProperties.ClientID%>');
            hidden.value = hidden.value + idDiv + ",";
        }
        else {
            //Si el control que levantó el evento NO está seleccionado, se elimina el nodo respectivo en el
            //árbol de las propiedades a mostrar
            DeleteDiv("div_" + idProperty + "_List");

            var idDiv = "div_" + idProperty + "_List,";
            var hidden = document.getElementById('<%=hdnProperties.ClientID%>');
            hidden.value = hidden.value.replace(idDiv, "");
        }
    }

    function ShowFilter(image, entityName, idProperty, pathProperty, propertyType, isRefProperty, propertySource,
propertySourceDisplayProp, system, pathPropertyValue) {
        if ((pathPropertyValue.lastIndexOf(".") + 1) == pathPropertyValue.length) {
            pathPropertyValue = pathPropertyValue.substring(0, pathPropertyValue.length - 1);
        }

        var html = "";
        var div;
        var idDivMain = "div_" + entityName + "_Filter";
        var divMain = document.getElementById(idDivMain);

        if (divMain == null)
            html = "<div id='" + idDivMain + "'>";

        var contDiv = parseInt(image.getAttribute("countFilter"));
        idProperty = idProperty + "_" + contDiv;
        image.setAttribute("countFilter", contDiv + 1);

        var idDiv = "div_" + idProperty + "_Value";
        var idSel = "cbo_" + idProperty + "_Value";
        var idTxtField = "txt_" + idProperty + "_Value";
        var idTxtValue = "txt_" + idProperty + "_Value2";
        var idTxtValueEnd = "txt_" + idProperty + "_Value3";
        var idChk = "chk_" + idProperty + "_Value";
        var idCboValue = "cbo_" + idProperty + "_Value2";
        var idCboCondi = "cbo_" + idProperty + "_Value3";
        var valTxt = pathProperty;

        html = html + "<div id='" + idDiv + "'>" +
		"<img alt='Quitar filtro' title='Quitar filtro' src='../../../APP/Images/icon-delete.gif' " +
		"onclick=\"DeleteFilter('" + idDiv + "')\" />&nbsp;" +
	    "<select id='" + idCboCondi + "'>" +
		    "<option value='And'>And</option>" +
		    "<option value='Or'>Or</option>" +
		"</select>" +
		"<input type='text' title='" + valTxt + "' id='" + idTxtField + "' value='" + valTxt + "' idProperty='" + idProperty + "' valueProperty='" + pathPropertyValue + "' readonly='readonly' style='width:30%'>";

        if (propertyType == "4") {
            html = html + "<select onchange='SetSelectedOption(this)' id='" + idSel + "'>" +
			"<option>----</option>" +
			"<option>=</option>" +
		"</select>" +
		"<select onchange='SetSelectedOption(this)' id='" + idChk + "'>" +
			"<option value='1'>Verdadero</option>" +
			"<option value='0'>Falso</option>" +
		"</select>";
        }
        else {
            html = html + "<select TxtValueId='" + idTxtValue + "' TxtValueEndId='" + idTxtValueEnd + "' onchange='SetSelectedOption(this);SetSelectedOptionParameter(this)' id='" + idSel + "'>" +
			"<option value='----'>----</option>" +
			"<option value='='>=</option>" +
			"<option value='<>'><></option>" +
			"<option value='>'>></option>" +
			"<option value='>='>>=</option>" +
			"<option value='<'><</option>" +
			"<option value='<='><=</option>" +
			"<option value='Contenga'>Contenga</option>" +
			"<option value='No contenga'>No contenga</option>" +
			"<option value='Entre'>Entre</option>" +
			"<option value='Comience por'>Comience por</option>" +
			"<option value='Termine en'>Termine en</option>" +
		"</select>" +
		"<input type='text' readonly='readonly' id='" + idTxtValue + "' onblur='SetControlText(this)' style='width:70px'/>";
            if (isRefProperty == "1") {
                var idHdn = "hdn_" + idProperty + "_Value";
                var idBtn = "btn_" + idProperty + "_Value";
                html = html + "<input type='hidden' id='" + idHdn + "' />" +
			 "<input type='button' id='" + idBtn + "' style='width:5%' value='...' " +
			 "onclick='GetManageOutOfRangeInstances(\"" + system + "\", \"" + propertySource + "\",\"" +
			 propertySourceDisplayProp + "\", \"" + idTxtValue + "\", \"" + idHdn + "\")' />";
            }
            html = html + "&nbsp;y&nbsp;<input type='text' readonly='readonly' id='" + idTxtValueEnd + "' onblur='SetControlText(this)' style='width:70px'/>";
            if (isRefProperty == "1") {
                var idHdnEnd = "hdn_" + idProperty + "_End_Value";
                var idBtnEnd = "btn_" + idProperty + "_End_Value";
                html = html + "<input type='hidden' id='" + idHdnEnd + "' />" +
			 "<input type='button' id='" + idBtnEnd + "' style='width:5%' value='...' " +
			 "onclick='GetManageOutOfRangeInstances(\"" + system + "\", \"" + propertySource + "\",\"" +
			 propertySourceDisplayProp + "\", \"" + idTxtValueEnd + "\", \"" + idHdnEnd + "\")' />";
            }
            if (propertyType == 7)
                html = html + "&nbsp;AAAA/MM/DD";
        }

        //	html = html + "&nbsp;<img alt='Quitar filtro' title='Quitar filtro' src='../../../APP/Images/icon-delete.gif' " +
        //	"onclick=\"DeleteFilter('" + idDiv + "')\" ";
        html = html + "</div>";

        if (divMain == null) {
            html = html + "</div>";
            div = document.getElementById('<%=divFilters.ClientID %>');
            div.innerHTML = div.innerHTML + html;
        }
        else {
            divMain.innerHTML = divMain.innerHTML + html;
        }

        //Lleva el rastro de los divs adicionados para posteriormente poderlos borrar con facilidad	
        var hidden = document.getElementById('<%=hdnSelectedFilters.ClientID%>');
        hidden.value = hidden.value + idDiv + ",";
    }

    function SetChecked(o) {
        if (o.checked)
            o.setAttribute("checked", "true");
        else
            o.removeAttribute("checked");
    }

    function ExpandedCollapsed(content, image) {
        if (image.getAttribute("isCollapsed") == '0') {
            image.src = "toc_expanded.gif";
            document.getElementById(content).style.display = "block";
            image.setAttribute("isCollapsed", "1");
        }
        else {
            image.src = "toc_collapsed.gif";
            document.getElementById(content).style.display = "none";
            image.setAttribute("isCollapsed", "0");
        }
    }

    function SetSelectedOption(o) {
        var selIdx = o.selectedIndex;
        for (i = 0; i < o.length; i++) {
            if (i != selIdx)
                o.options[i].removeAttribute("selected");
            else
                o.options[i].setAttribute("selected", "true");
        }
    }

    function SetSelectedOptionParameter(o) {
        var TxtValueId = document.getElementById(o.getAttribute("TxtValueId"));
        var TxtValueEndId = document.getElementById(o.getAttribute("TxtValueEndId"));

        if (o.value != "----") {
            if (o.value == "Entre") {
                TxtValueEndId.removeAttribute("readOnly");
                TxtValueId.removeAttribute("readOnly");
            }
            else {
                TxtValueEndId.setAttribute("readOnly", "readonly");
                TxtValueId.removeAttribute("readOnly");
            }
        }
        else {
            TxtValueEndId.setAttribute("readOnly", "readonly");
            TxtValueId.setAttribute("readOnly", "readonly");
        }
    }

    function SetControlText(o) {
        if (o.value != "")
            o.setAttribute("value", o.value);
        else
            o.setAttribute("value", "");
    }

    function DeleteDiv(divName) {
        var div = document.getElementById(divName);
        if (div != null) {
            div.parentNode.removeChild(div);
        }
    }

    function DeleteFilter(divName) {
        DeleteDiv(divName);

        var hidden = document.getElementById('<%=hdnSelectedFilters.ClientID%>');
        hidden.value = hidden.value.replace(divName + ",", "");

        GetFilterList();
    }

    function Clean() {
        //////////////////////////////////////////////////////////////////////
        // Limpia los div
        //////////////////////////////////////////////////////////////////////
        var div = document.getElementById('<%=divRelatedEntities.ClientID %>');
        div.innerHTML = "";

        div = document.getElementById('<%=divPropertiesToSelect.ClientID %>');
        div.innerHTML = "";

        div = document.getElementById('<%=divSelectedProps.ClientID %>');
        div.innerHTML = "";

        div = document.getElementById('<%=divFilters.ClientID %>');
        div.innerHTML = "";

        var hidden = document.getElementById('<%=hdnSelectedEntities.ClientID%>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnProperties.ClientID%>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnSelectedFilters.ClientID%>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnEntities.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnRelatedEntities.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnPropertiesToSelect.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnSelectedProps.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnFilters.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnEntitiesList.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnPropertiesList.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnPropertiesListDisplay.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnOrdersList.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnFormatList.ClientID %>');
        hidden.value = "";

        hidden = document.getElementById('<%=hdnFiltersList.ClientID %>');
        hidden.value = "";
    }

    function DeleteHiddenValues(hiddenName, entityName) {
        var hidden = document.getElementById(hiddenName);

        if (hidden.value != "") {
            hidden.value = hidden.value.substring(0, hidden.value.length - 1);

            var list = hidden.value.split(",");
            hidden.value = "";
            for (var i = 0; i < list.length; i++) {
                if (list[i].indexOf(entityName) == -1)
                    hidden.value += list[i] + ",";
            }
        }
    }

    function RunQuery() {
        //Antes de hacer el postback para ejecutar la consulta, se recupera el estado actual
        //de la página para restablecerlo luego de hacer la ejecución
        SaveDivHTML('<%=hdnEntities.ClientID %>', '<%=spanEntities.ClientID %>');
        SaveDivHTML('<%=hdnRelatedEntities.ClientID %>', '<%=divRelatedEntities.ClientID %>');
        SaveDivHTML('<%=hdnPropertiesToSelect.ClientID %>', '<%=divPropertiesToSelect.ClientID %>');
        SaveDivHTML('<%=hdnSelectedProps.ClientID %>', '<%=divSelectedProps.ClientID %>');
        SaveDivHTML('<%=hdnFilters.ClientID %>', '<%=divFilters.ClientID %>');

        GetEntitiesList();
        GetSelectList();
        GetFilterList();

        document.getElementById('<%=hdfSystem.ClientID %>').value = document.getElementById('<%=cboSystems.ClientID %>').value;

        //Verifica si la consulta que se quiere ejecutar fue originada desde targeting.  Si lo es, y
        //adicionalmente se están consultando muchos campos, se pide confirmación para ejecutar la
        //consulta
        var hidden = document.getElementById("hdnIsTargetQuery");
        var arrSelectList = document.getElementById('<%=hdnPropertiesList.ClientID %>').value.split(",");

        if (arrSelectList.length > 4 &&
		hidden.value == "1") {
            var message = "La consulta que quiere ejecutar proviene del sistema de campañas y " +
			"contiene demasiados atributos para consultar.  No es aconsejable tener tantos " +
			"atributos para una consulta de este tipo, dado el costo que implica su procesamiento.  " +
			"Está seguro de ejecutar la consulta?";
            if (confirm(message) == true) {
                return true;
            }
            else
                return false;
        }
        else
            return true;
    }

    function NewQuery() {
        if (confirm("¿Realmente desea realizar una nueva consulta?")) {
            var hidden = document.getElementById("hdnIsTargetQuery");

            if (hidden.value == 0) {
                var div = document.getElementById('<%=spanEntities.ClientID %>');
                div.innerHTML = "";

                var cbo = document.getElementById('<%=cboSystems.ClientID %>');
                cbo.selectedIndex = 0;
            }

            hidden = document.getElementById('<%=hdnMainEntity.ClientID %>');
            hidden.value = "";

            Clean();
            SaveDivHTML('<%=hdnEntities.ClientID %>', '<%=spanEntities.ClientID %>');

            return true;
        }
        else {
            return false;
        }
    }

    function GetEntitiesList() {
        var entitiesList = "";
        var hidden = document.getElementById('<%=hdnSelectedEntities.ClientID%>');

        if (hidden.value != "") {
            var list = hidden.value.split(",");

            for (var i = 0; i < list.length; i++) {
                if (list[i] != "") {
                    var idChk = list[i].replace("div", "chk");
                    var checkBox = document.getElementById(idChk);

                    if (checkBox.checked) {
                        entitiesList += list[i].replace("div_", "");
                        entitiesList += ",";
                    }
                }
            }

            entitiesList = entitiesList.substring(0, entitiesList.lastIndexOf(","));
            hidden = document.getElementById('<%=hdnEntitiesList.ClientID %>');
            hidden.value = entitiesList;
        }
    }

    function GetSelectList() {
        var selectList = "";
        var selectListDisplay = "";
        var orderList = "";
        var formatList = "";

        var hidden = document.getElementById('<%=hdnProperties.ClientID%>');

        if (hidden.value != "") {
            var list = hidden.value.split(",");

            for (var i = 0; i < list.length; i++) {
                if (list[i] != "") {
                    var idTxt = list[i].replace("div_", "txt_");
                    var txt = document.getElementById(idTxt);

                    selectList += txt.getAttribute('valueProperty');
                    selectList += ",";

                    var idChk = txt.getAttribute('idProperty');
                    var chk = document.getElementById(idChk);

                    selectListDisplay += chk.getAttribute("display");
                    selectListDisplay += ",";

                    var idCbo = list[i].replace("div_", "cbo_");
                    var cbo = document.getElementById(idCbo);

                    var order = cbo.options[cbo.selectedIndex].text;
                    if (order != "----") {
                        orderList += txt.getAttribute('valueProperty');
                        orderList += ".";
                        orderList += order;
                        orderList += ",";
                    }

                    var propertyType = txt.getAttribute('propertyType');

                    if (propertyType == 7) {
                        var idCboFormat = list[i].replace("div_", "cbo_") + "_Format";
                        var cboFormat = document.getElementById(idCboFormat);

                        var format = cboFormat.options[cboFormat.selectedIndex].value;
                        if (format != "----") {
                            formatList += format;
                            formatList += ";";
                        }
                        else {
                            formatList += ";";
                        }
                    }
                    else {
                        formatList += ";";
                    }
                }
            }

            selectList = selectList.substring(0, selectList.lastIndexOf(","));
            selectListDisplay = selectListDisplay.substring(0, selectListDisplay.lastIndexOf(","));
            orderList = orderList.substring(0, orderList.lastIndexOf(","));
            formatList = formatList.substring(0, formatList.lastIndexOf(";"));
        }

        hidden = document.getElementById('<%=hdnPropertiesList.ClientID %>');
        hidden.value = selectList;

        hidden = document.getElementById('<%=hdnPropertiesListDisplay.ClientID %>');
        hidden.value = selectListDisplay;

        hidden = document.getElementById('<%=hdnOrdersList.ClientID %>');
        hidden.value = orderList;

        hidden = document.getElementById('<%=hdnFormatList.ClientID %>');
        hidden.value = formatList;
    }

    function GetFilterList() {
        var filterList = "";

        var hidden = document.getElementById('<%=hdnSelectedFilters.ClientID%>');

        if (hidden.value != "") {
            var list = hidden.value.split(",");

            for (var i = 0; i < list.length; i++) {
                if (list[i] != "") {
                    var idTxtField = list[i].replace("div_", "txt_");
                    var txtField = document.getElementById(idTxtField);

                    var idTxtValue = idTxtField + "2";
                    var txtValue = document.getElementById(idTxtValue);

                    var idTxtValueEnd = idTxtField + "3";
                    var txtValueEnd = document.getElementById(idTxtValueEnd);

                    var idCbo = "cbo_" + txtField.getAttribute('idProperty') + "_Value";
                    var cbo = document.getElementById(idCbo);
                    var operator = cbo.options[cbo.selectedIndex].text;

                    var idBtn = "btn_" + txtField.getAttribute('idProperty') + "_Value";
                    var btn = document.getElementById(idBtn);

                    var idChk = "chk_" + txtField.getAttribute('idProperty') + "_Value";
                    var chk = document.getElementById(idChk);

                    var idCboCondi = "cbo_" + txtField.getAttribute('idProperty') + "_Value3";
                    var cboCondi = document.getElementById(idCboCondi);
                    var operatorCondi = cboCondi.options[cboCondi.selectedIndex].text;

                    var filterValue = "";
                    if (btn != null) {
                        var idHdn = "hdn_" + txtField.getAttribute('idProperty') + "_Value";
                        var hdn = document.getElementById(idHdn);
                        filterValue = hdn.value;
                    }

                    if (chk != null) {
                        filterValue = chk.value;
                    }

                    if (filterValue == "")
                        filterValue = txtValue.value;

                    if (operator == "Entre")
                        filterValue = txtValue.value + " y " + txtValueEnd.value;

                    if (operator != "----") {
                        filterList += txtField.getAttribute('valueProperty');
                        filterList += ".";
                        filterList += operator;
                        filterList += ".";
                        filterList += filterValue;
                        filterList += ".";
                        filterList += operatorCondi;
                        filterList += ",";
                    }
                }
            }

            filterList = filterList.substring(0, filterList.lastIndexOf(","));
        }

        hidden = document.getElementById('<%=hdnFiltersList.ClientID %>');
        hidden.value = filterList;
    }

    function SaveDivHTML(hidden, div) {
        var hiddenControl = document.getElementById(hidden);
        var divControl = document.getElementById(div);
        hiddenControl.value = divControl.innerHTML;
    }

</script>

<script language="javascript" type="text/javascript">
    var systemOutOfRangeGral = "";
    var entityOutOfRangeGral = "";
    var propertyOutOfRangeGral = "";
    var oOutOfRangeGral = null;
    var oHiddenOutOfRangeGral = null;

    var handleSuccessGetSystemEntityReferentialValues = function (o) {
        DisableDisplayLoading();
        response = o.responseText;
        if (response !== undefined) {
            response = response.split("<!")[0];

            if (response != '') {
                document.getElementById("divSelectOutofRange").innerHTML = response;
            }
        }
    };

    var callbackGetSystemEntityReferentialValues =
{
    success: handleSuccessGetSystemEntityReferentialValues,
    failure: handleFailure
};

    function GetManageOutOfRangeInstances(systemOutOfRange, entityOutOfRange, propertyOutOfRange, oOutOfRange, oHiddenOutOfRange) {
        systemOutOfRangeGral = systemOutOfRange;
        entityOutOfRangeGral = entityOutOfRange;
        propertyOutOfRangeGral = propertyOutOfRange;
        oOutOfRangeGral = document.getElementById(oOutOfRange);
        oHiddenOutOfRangeGral = document.getElementById(oHiddenOutOfRange);

        if (document.getElementById("txtFindValue") != null)
            document.getElementById("txtFindValue").value = '';

        document.getElementById("divSelectOutofRange").innerHTML = "";

        YAHOO.OnData.TitanFL.container.dlgOutOfRange.setHeader("Buscar");
        YAHOO.OnData.TitanFL.container.dlgOutOfRange.show();
    }

    function findValue() {
        var vlrFindValue = "";

        if (document.getElementById("txtFindValue") != null && document.getElementById("txtFindValue").value != '') {
            vlrFindValue = "&FindValue=" + document.getElementById("txtFindValue").value;
            document.getElementById("divSelectOutofRange").innerHTML = "Buscando...";
            makeRequest("../../UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntityReferentialValues&System=" + systemOutOfRangeGral + "&Entity=" + entityOutOfRangeGral + "&PropertyName=" + propertyOutOfRangeGral + vlrFindValue, callbackGetSystemEntityReferentialValues);
        }
        else {
            alert("Digite un patrón de búsqueda");
        }
    }

    function SaveOutOfRange() {
        var mylist = null;

        if (document.getElementById("lstOutOfRange") != null) {
            mylist = document.getElementById("lstOutOfRange");

            if (mylist.length != 0) {
                if (mylist.selectedIndex != -1) {
                    oOutOfRangeGral.setAttribute("value", mylist.options[mylist.selectedIndex].text);
                    oHiddenOutOfRangeGral.value = mylist.options[mylist.selectedIndex].value;
                    YAHOO.OnData.TitanFL.container.dlgOutOfRange.hide();
                }
                else {
                    alert("Debe seleccionar un elemento");
                }
            }
            else {
                alert("No hay elementos para seleccionar");
            }
        }
        else {
            alert("No hay elementos para seleccionar");
        }
    }

    YAHOO.namespace('OnData.TitanFL.container');

    function init() {
        /*************************************************************************
        * Creación del formulario para cuando hay mas una cantidad maxima de datos
        **************************************************************************/
        var handleCancelOutOfRange = function (e) {
            this.hide();
        }

        var handleOKOutOfRange = function (e) {
            SaveOutOfRange();
        }

        YAHOO.OnData.TitanFL.container.dlgOutOfRange = new YAHOO.widget.SimpleDialog("dlgManageOutOfRangeInstances", { visible: false, width: "400px", fixedcenter: true, modal: true, draggable: true });
        YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("buttons", [
																   { text: "Seleccionar", handler: handleOKOutOfRange, isDefault: true },
																   { text: "Cancelar", handler: handleCancelOutOfRange }
															   ]);

        listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelOutOfRange, scope: YAHOO.OnData.TitanFL.container.dlgOutOfRange, correctScope: true });
        YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("keylisteners", listeners);

        YAHOO.OnData.TitanFL.container.dlgOutOfRange.render(document.body);
    }

    YAHOO.util.Event.addListener(window, "load", init, "", true)
</script>

