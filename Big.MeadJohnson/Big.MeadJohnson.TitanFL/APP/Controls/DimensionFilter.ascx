﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DimensionFilter.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Controls.DimensionFilter" %>
<table cellpadding="3">
    <tr>
        <td>
            <asp:DropDownList ID="ddlFilterConnector" runat="server">
                <asp:ListItem Text="Y" Value="AND"></asp:ListItem>
                <asp:ListItem Text="O" Value="OR"></asp:ListItem>
            </asp:DropDownList>
        </td>
        <td>
            <asp:Literal ID="ltPropertyName" runat="server"></asp:Literal>
        </td>
        <td>
            <asp:Literal ID="ltFilterOperator" runat="server"></asp:Literal>
        </td>
        <td>
            <asp:Literal ID="ltValues" runat="server"></asp:Literal>
        </td>
        <td style="width: 16px">
            <asp:HyperLink ID="hlRemoveFilter" runat="server" ToolTip="Remover Filtro">
                <asp:Image ID="imageFilter" runat="server" ImageUrl="~/APP/Images/icon-delete.gif"
                    BorderWidth="0" />
            </asp:HyperLink>
        </td>
    </tr>
</table>
