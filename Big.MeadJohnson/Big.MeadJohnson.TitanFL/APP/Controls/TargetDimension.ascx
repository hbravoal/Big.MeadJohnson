﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TargetDimension.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Controls.TargetDimension" %>
<%@ Register Src="DimensionProperty.ascx" TagName="DimensionProperty" TagPrefix="uc1" %>
<%@ Register Src="DimensionFilter.ascx" TagName="DimensionFilter" TagPrefix="uc2" %>
<table width="100%" cellpadding="3">
    <tr>
        <td>
            <b>Variables Básicas</b>
        </td>
        <td>
            <b>Filtros</b>
        </td>
    </tr>
    <tr>
        <td style="width: 30%; vertical-align: top">
            <div style="width: 100%; max-height: 150px; overflow: auto">
                <asp:Repeater ID="rptDimensionProperties" runat="server" OnItemDataBound="rptDimensionProperties_ItemDataBound">
                    <ItemTemplate>
                        <uc1:DimensionProperty ID="dimensionProperty" runat="server" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </td>
        <td style="width: 80%; vertical-align: top">
            <div style="width: 100%; max-height: 150px; overflow: auto">
                <asp:Repeater ID="rptDimensionFilters" runat="server" OnItemDataBound="rptDimensionFilters_ItemDataBound">
                    <ItemTemplate>
                        <uc2:DimensionFilter ID="dimensionFilter" runat="server" />
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </td>
    </tr>
</table>
