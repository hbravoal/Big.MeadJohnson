﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DimensionProperty.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.Controls.DimensionProperty" %>
<table cellpadding="3">
    <tr>
        <td style="width: 10px">
            <asp:CheckBox ID="chkPropertySelected" runat="server" />
        </td>
        <td>
            <asp:Literal ID="ltPropertyName" runat="server"></asp:Literal>
        </td>
        <td style="width: 16px">
            <asp:HyperLink ID="hlAddFilter" runat="server" ToolTip="Adicionar Filtro">
                <asp:Image ID="imageFilter" runat="server" ImageUrl="~/APP/Images/filter.gif" BorderWidth="0" />
            </asp:HyperLink>
        </td>
    </tr>
</table>
