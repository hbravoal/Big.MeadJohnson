﻿<!--here for the modal windows-->
<div id="popupMask">&nbsp;</div>
<div id="popupContainer">
    <div id="popupInner">
	    <div id="popupTitleBar">
		    <div id="popupTitle"></div>
		    <div id="popupControls">
			    <img alt="Cerrar" src='<%=Page.ResolveUrl("~/App/Util/modal/close.gif")%>' onclick="hidePopWin(false);" />
		    </div>
	    </div>
	    <iframe src='<%=Page.ResolveUrl("~/App/Util/modal/loading.aspx")%>' style="width:100%;background-color:transparent;" scrolling="auto" frameborder="0" id="popupFrame" name="popupFrame" ></iframe>
    </div>
</div>
