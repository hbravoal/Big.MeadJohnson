﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="ChangePassword.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Admin.ChangePassword" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="adminedit" style="text-align: center">
        <asp:ChangePassword ID="changePassword" runat="server" CancelButtonText="Cancelar"
            CancelDestinationPageUrl="~/APP/Main.aspx" ChangePasswordButtonText="Cambiar Contraseña"
            ChangePasswordFailureText="Contraseña incorrecta o Nueva contraseña inválida. "
            ChangePasswordTitleText="Cambie su Contraseña" ConfirmNewPasswordLabelText="Confirmar nueva Contraseña:"
            ConfirmPasswordCompareErrorMessage="La confirmación de la contraseña debe coincidir con la nueva contraseña ingresada."
            ConfirmPasswordRequiredErrorMessage="Confirmar nueva Contraseña es requerido."
            ContinueButtonText="Ok" ContinueDestinationPageUrl="~/APP/Main.aspx" NewPasswordLabelText="Nueva contraseña:"
            NewPasswordRegularExpressionErrorMessage="Por favor ingrese una contraseña diferente."
            NewPasswordRequiredErrorMessage="La Nueva contraseña es requerida." PasswordLabelText="Contraseña:"
            PasswordRequiredErrorMessage="La contraseña es requerida." SuccessText="¡Su contraseña ha sido cambiada!"
            SuccessTitleText="Cambio de contraseña completo">
            <TitleTextStyle BackColor="LightSlateGray" Font-Bold="True" ForeColor="White" />
            <TextBoxStyle Width="150px" CssClass="loginTextBox" />
            <LabelStyle CssClass="loginLabel" />
        </asp:ChangePassword>
    </div>
</asp:Content>
