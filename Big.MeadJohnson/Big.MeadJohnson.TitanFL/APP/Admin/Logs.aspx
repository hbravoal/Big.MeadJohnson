﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Logs.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Admin.Logs" %>

<%@ Register Src="~/Controls/ControlFecha.ascx" TagName="ControlFecha" TagPrefix="titan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="adminedit">
        <table cellpadding="6" width="100%" cellspacing="0" border="0">
            <tr>
                <td>
                    <h3>
                        <span style="font-size: 14pt">Registro de TitanFL</span></h3>
                    Este log representa toda la actividad de los usuarios en el sistema. Para ver el
                    log de la aplicación, vea el visor de eventos del sistema.
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" border="0">
                        <tr>
                            <td>
                                Buscar por fecha: desde&nbsp;
                            </td>
                            <td>
                                <titan:ControlFecha ID="FechaDesde" runat="server">
                                </titan:ControlFecha>
                            </td>
                            <td>
                                &nbsp;hasta&nbsp;
                            </td>
                            <td>
                                <titan:ControlFecha ID="FechaHasta" runat="server">
                                </titan:ControlFecha>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    Buscar por tipo de comportamiento:
                    <asp:DropDownList ID="BehaviorType" runat="server" DataSourceID="BehaviorNameDataSource"
                        DataTextField="behaviorName" DataValueField="behaviorID">
                        <asp:ListItem Value="-1" Text="--Todos--"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:ObjectDataSource ID="BehaviorNameDataSource" runat="server" SelectMethod="GetStatsBehavior"
                        TypeName="OnData.TitanFL.Stats.Tracker"></asp:ObjectDataSource>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar..." OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td>
                    <hr />
                    <asp:ObjectDataSource ID="StatsListDataSource" runat="server" SelectMethod="GetStatsTrack"
                        TypeName="OnData.TitanFL.Stats.Tracker">
                        <SelectParameters>
                            <asp:Parameter Name="startDate" Type="String" />
                            <asp:Parameter Name="endDate" Type="String" />
                            <asp:Parameter Name="behaviorType" Type="String" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                    <asp:GridView ID="StatsList" runat="server" AllowPaging="True" CellPadding="4" DataSourceID="StatsListDataSource"
                        ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" Width="100%">
                        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                        <EditRowStyle BackColor="#999999" />
                        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                        <Columns>
                            <asp:BoundField DataField="createdOn" HeaderText="Fecha" />
                            <asp:BoundField DataField="userName" HeaderText="Usuario" />
                            <asp:BoundField DataField="systemName" HeaderText="Sistema" />
                            <asp:BoundField DataField="entityName" HeaderText="Entidad" />
                            <asp:BoundField DataField="behaviorName" HeaderText="Comportamiento" />
                            <asp:BoundField DataField="isError" HeaderText="Error" />
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
