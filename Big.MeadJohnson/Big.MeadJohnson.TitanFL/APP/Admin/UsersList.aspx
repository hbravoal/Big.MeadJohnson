﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="UsersList.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Admin.UsersList" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
   <div id="adminedit">
      <h3>
         Administración de usuarios</h3>
      <div style="padding-bottom: 5px">
         <a href="UserDetail.aspx">[+] Adicionar Usuario</a>
      </div>
      <br />
      <table cellspacing="0" cellpadding="0" class="lrbBorders">
         <tr>
            <td class="bodyTextLowTopPadding">
               <asp:Label ID="Label1" runat="server" AssociatedControlID="SearchByDropDown" Text="Buscar por: "
                  Font-Bold="true" />
               <asp:DropDownList runat="server" ID="SearchByDropDown">
                  <asp:ListItem runat="server" id="Item1" Text="Username" />
                  <asp:ListItem runat="server" id="Item2" Text="Email" />
               </asp:DropDownList>
               &nbsp;&nbsp;<asp:Label ID="Label2" runat="server" AssociatedControlID="TextBox1"
                  Text="" />
               <asp:TextBox runat="server" ID="TextBox1" />
               <asp:Button ID="Button1" runat="server" Text="Buscar por Usuario" OnClick="SearchForUsers" />
               <br />
            </td>
         </tr>
      </table>
      <br />
      <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="allUsersDataSource"
         EmptyDataText="No hay registros que cumplan con el criterio de búsqueda." Font-Italic="False" CssClass="table table-bordered">
         <Columns>
            <asp:TemplateField HeaderText="Activo" ItemStyle-Width="40" ItemStyle-HorizontalAlign="Center">
               <HeaderStyle HorizontalAlign="center" />
               <ItemStyle HorizontalAlign="center" />
               <ItemTemplate>
                  <asp:CheckBox runat="server" ID="CheckBox1" OnCheckedChanged="EnabledChanged" AutoPostBack="true"
                     Checked='<%#DataBinder.Eval(Container.DataItem, "IsApproved")%>' Value='<%#DataBinder.Eval(Container.DataItem, "UserName")%>' />
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField runat="server" HeaderText="Nombre de Usuario" ItemStyle-Width="160">
               <ItemTemplate>
                  <a href='UserDetail.aspx?username=<%#Eval("UserName")%>'>
                     <%#DataBinder.Eval(Container.DataItem, "UserName")%>
                  </a>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Email" ItemStyle-Width="180" ItemStyle-HorizontalAlign="Left">
               <ItemTemplate>
                  <asp:HyperLink ID="EmailLink" runat="server" NavigateUrl='<%# Eval("Email", "mailto:{0}") %>'
                     Text='<%# Eval("Email") %>'></asp:HyperLink>
               </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="CreationDate" HeaderText="Fecha de Creación" ReadOnly="True"
               SortExpression="CreationDate" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundField DataField="LastLoginDate" HeaderText="Ultimo Ingreso" SortExpression="LastLoginDate"
               ItemStyle-HorizontalAlign="Center" />
            <asp:TemplateField runat="server" ItemStyle-HorizontalAlign="Center">
               <ItemTemplate>
                  <asp:LinkButton runat="server" ID="linkButton2" Text="Borrar" CommandName="delete"
                     CommandArgument='<%#DataBinder.Eval(Container.DataItem, "UserName")%>' 
                     OnCommand="LinkButtonClick" OnClientClick="return confirm('¿Realmente deseas eliminar este registro?');"/>
               </ItemTemplate>
            </asp:TemplateField>
         </Columns>
         <EmptyDataRowStyle Font-Italic="True" />
      </asp:GridView>
      <div style="margin-top: 5px">
      </div>
      <br />
      <asp:ObjectDataSource ID="allUsersDataSource" runat="server" SelectMethod="GetAllUsers"
         TypeName="System.Web.Security.Membership" OnSelected="allUsersDataSource_Selected">
      </asp:ObjectDataSource>
   </div>
</asp:Content>
