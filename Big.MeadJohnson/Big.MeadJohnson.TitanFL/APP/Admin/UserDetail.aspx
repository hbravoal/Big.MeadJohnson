﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="UserDetail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Admin.UserDetail" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="adminedit">
        <table cellspacing="0" cellpadding="0" border="0">
            <tbody>
                <tr align="left" valign="top">
                    <td height="100%" class="lbBorders">
                        <table class="bodyText" cellspacing="0" width="100%" cellpadding="0" border="0">
                            <tr class="callOutStyleLowLeftPadding">
                                <td colspan="4">
                                    <h3>
                                        Editar usuario</h3>
                                </td>
                            </tr>
                            <tr id="trResultRow" runat="server" visible="false">
                                <td colspan="4">
                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table class="bodyText">
                                        <tr>
                                            <td width="2">
                                            </td>
                                            <td colspan="2">
                                                <h3>
                                                    <span style="font-size: 12pt">Detalles del usuario&nbsp;</span></h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="2">
                                            </td>
                                            <td>
                                                <asp:Label ID="Label1" runat="server" AssociatedControlID="UserID" Text="Nombre de Usuario:" />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="UserID" MaxLength="255" TabIndex="101" Columns="30" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="UserID"
                                                    Display="Dynamic" EnableClientScript="true">requerido</asp:RequiredFieldValidator>
                                                <asp:CustomValidator ID="cvUserID" runat="server" Display="Dynamic" ControlToValidate="UserID"
                                                    EnableClientScript="true" ClientValidationFunction="ValidarLongitud">Debe tener 4 o más caracteres</asp:CustomValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="2">
                                            </td>
                                            <td>
                                                <asp:Label ID="Label2" runat="server" AssociatedControlID="Email" Text="Email: " />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="Email" MaxLength="128" TabIndex="102" Columns="30" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="Email"
                                                    Display="Dynamic" EnableClientScript="true">requerido</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="PasswordRow" runat="server" visible="false">
                                            <td width="2">
                                            </td>
                                            <td>
                                                <asp:Label ID="lblPassword" runat="server" AssociatedControlID="Password" Text="Clave de acceso: " />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="Password" MaxLength="50" TabIndex="103" Columns="20"
                                                    TextMode="Password" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="Password"
                                                    Display="Dynamic" EnableClientScript="true">requerido</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="NewPasswordRow" runat="server" visible="false">
                                            <td width="2">
                                            </td>
                                            <td>
                                                <asp:Label ID="lblNewPassword" runat="server" AssociatedControlID="NewPassword" Text="Nueva clave de acceso: " />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="NewPassword" MaxLength="50" TabIndex="103" Columns="20" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="NewPassword"
                                                    Display="Dynamic" EnableClientScript="true">required</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="SecretQuestionRow" runat="server" visible="false">
                                            <td width="2">
                                            </td>
                                            <td>
                                                <asp:Label ID="Label5" runat="server" AssociatedControlID="SecretQuestion" Text="Pregunta secreta: " />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="SecretQuestion" MaxLength="128" TabIndex="104" Columns="30" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="SecretQuestion"
                                                    Display="Dynamic" EnableClientScript="true">required</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr id="SecretAnswerRow" runat="server" visible="false">
                                            <td width="2">
                                            </td>
                                            <td>
                                                <asp:Label ID="Label6" runat="server" AssociatedControlID="SecretAnswer" Text="Respuesta secreta: " />
                                            </td>
                                            <td>
                                                <asp:TextBox runat="server" ID="SecretAnswer" MaxLength="128" TabIndex="105" Columns="30" />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="SecretAnswer"
                                                    Display="Dynamic" EnableClientScript="true">requerido</asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="2">
                                            </td>
                                            <td>
                                                Usuario Activo</td>
                                            <td>
                                                <asp:CheckBox runat="server" ID="ActiveUser" Text="" TabIndex="106" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="2">
                                            </td>
                                            <td colspan="2">
                                                <asp:Button runat="server" ID="ResetPassword" Text="Reiniciar Clave" TabIndex="107"
                                                    OnClick="ResetPassword_Click" />
                                                <asp:Button runat="server" ID="unlockUser" Text="Desbloquear Cuenta" TabIndex="107"
                                                    OnClick="unlockAccount_Click" />
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td width="2">
                                            </td>
                                            <td class="userDetailsWithFontSize" height="100%" colspan="2">
                                                <br />
                                                <h3>
                                                    <asp:Label runat="server" ID="SelectRolesLabel" Text="Roles del usuario" Font-Size="12pt" />&nbsp;</h3>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td width="2">
                                            </td>
                                            <td colspan="2">
                                                <asp:Repeater runat="server" ID="CheckBoxRepeater">
                                                    <ItemTemplate>
                                                        <asp:CheckBox runat="server" ID="checkBox1" Text='<%# Container.DataItem.ToString()%>'
                                                            Checked='<%# IsUserInRole(Container.DataItem.ToString())%>' />
                                                        <br />
                                                    </ItemTemplate>
                                                </asp:Repeater>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <br />
                                                <asp:Button runat="server" ID="SaveButton" OnClick="SaveClick" Text="Guardar" Width="100" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <br />
                                                <br />
                                                <a href="UsersList.aspx">Regresar a la lista de usuarios.</a>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>

    <script type="text/javascript">
        function ValidarLongitud(source, arguments) {
            if (arguments.Value.length < 4)
                arguments.IsValid = false;
            else
                arguments.IsValid = true;
        }
    </script>

</asp:Content>