﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true" CodeBehind="InfoConfig.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.InfoConfig" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="Server">
    <div id="adminedit">
        <fieldset>
            <legend>Configuración actual del sistema</legend>
            <table border="0" width="100%">
               <tr>
                  <td style="width:30%">
                     Perfil de configuración
                     <br />
                     (configurationProfile)                 
                  </td>
                  <td style="width:70%">
                     <asp:Label ID="lblProfile" runat="server" Text=""></asp:Label>
                   </td>
               </tr>
               <tr>
                  <td>
                     Nombre de la cadena de conexión
                     <br />
                     (connectionStringName)               
                  </td>
                  <td>
                     <asp:Label ID="lblConnection" runat="server" Text=""></asp:Label>   
                  </td>
               </tr>
               <tr>
                  <td>
                     Nombre de la cadena de conexión de configuración
                     <br />
                     (configConnectionStringName)                
                  </td>
                  <td>
                     <asp:Label ID="lblConfigConnection" runat="server" Text=""></asp:Label>   
                  </td>
               </tr>  
                <tr>
                  <td>
                     Sistema de entidades                  
                  </td>
                  <td>
                     <asp:Label ID="lblAssemblyES" runat="server" Text=""></asp:Label>
                     <br />  
                     <asp:Label ID="lblClassES" runat="server" Text=""></asp:Label>  
                  </td>
               </tr>   
               <tr>
                  <td>
                     Sistema de consultas                  
                  </td>
                  <td>
                     <asp:Label ID="lblAssemblyQS" runat="server" Text=""></asp:Label>
                     <br />  
                     <asp:Label ID="lblClassQS" runat="server" Text=""></asp:Label>  
                  </td>
               </tr>  
               <tr>
                  <td>
                     Sistema de PQRS                  
                  </td>
                  <td>
                     <asp:Label ID="lblAssemblyPQRS" runat="server" Text=""></asp:Label>
                     <br />  
                     <asp:Label ID="lblClassPQRS" runat="server" Text=""></asp:Label>  
                  </td>
               </tr>
               <tr>
                  <td>
                     Sistema de Fidelización                  
                  </td>
                  <td>
                     <asp:Label ID="lblAssemblyFS" runat="server" Text=""></asp:Label>
                     <br />  
                     <asp:Label ID="lblClassFS" runat="server" Text=""></asp:Label>  
                  </td>
               </tr>
               <tr>
                  <td>
                     Sistema de Campañas                  
                  </td>
                  <td>
                     <asp:Label ID="lblAssemblyCS" runat="server" Text=""></asp:Label>
                     <br />  
                     <asp:Label ID="lblClassCS" runat="server" Text=""></asp:Label>  
                  </td>
               </tr>                                                                                                            
            </table>
        </fieldset>
    </div>
</asp:Content>
