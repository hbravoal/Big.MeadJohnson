<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:import href="../ViewsData/ViewsControls.xsl"/>
  <xsl:param name="currentPage"/>
  <xsl:param name="currentSortBy"/>
  <xsl:param name="currentSortOrder"/>
  <xsl:param name="RelatedEntityGuid"/>
  <xsl:param name="RelatedViewEntity"/>
  <xsl:param name="RelatedViewEntityProperty"/>
  <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
  <xsl:template match="/">
    <table width="100%" cellpading="0" cellspacing="0" border="0">
      <tr>
        <td colspan="3">
          <font style="font-weight:bold; font-size:large">
            Insertar en <xsl:value-of select="/BusinessEntityView/@ViewDisplayName"/>
          </font>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <hr/>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <table width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="LightSteelBlue">
            <xsl:for-each select="//Group">
              <tr bgcolor="LightSteelBlue">
                <td colspan="2">
                  <strong>
                    <xsl:value-of select="current()/@GroupDisplayName"/>
                  </strong>
                </td>
              </tr>
              <xsl:for-each select="current()/PropertiesGroup/Property">
                <xsl:sort order="ascending" data-type="number" select="@Order"/>
                <xsl:if test="current()/@PropertyName != $RelatedViewEntityProperty">
                  <tr height="10px"  valign="middle">
                    <td width="200px" align="right">
                      <strong>
                        <xsl:if test="current()/@Required = 'true'">
                          <font style="color:Red">*</font>
                        </xsl:if>&#160;<xsl:value-of select="current()/@PropertyDisplayName"/>
                      </strong>
                    </td>
                    <td  width="475px" valign="top">
                      <xsl:call-template name="TemplateFromControl">
                        <xsl:with-param name="property" select="."/>
                      </xsl:call-template>
                    </td>
                  </tr>
                </xsl:if>
              </xsl:for-each>
            </xsl:for-each>
          </table>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          &#160;
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          <input type="button" value="Guardar" style="width:100px" id="btnSave">
            <xsl:attribute name="onclick">
              SaveViewData(this.form, null)
            </xsl:attribute>
          </input>
          <input type="button" value="Cancelar" style="width:100px">
            <xsl:attribute name="onclick">
              Cancel('<xsl:value-of select="/BusinessEntityView/@ViewName"/>')
            </xsl:attribute>
          </input>
        </td>
      </tr>
    </table>
    <div></div>
  </xsl:template>
</xsl:stylesheet>

