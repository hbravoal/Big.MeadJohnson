<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <!--Controles del formulario según el tipo-->
   <xsl:template name="TemplateFromControl">
      <xsl:param name="property"/>
      <xsl:choose>
         <xsl:when test="current()/@PropertyType = '1'">
            <!--LongString-->
            <xsl:call-template name="TemplateTextArea">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="current()/@PropertyType = '4'">
            <!--Boolean-->
            <xsl:call-template name="TemplateBoolean">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="current()/@PropertyType = '7'">
            <!--Date-->
            <xsl:call-template name="TemplateDate">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="current()/@PropertyType = '11'">
            <!--Password-->
            <xsl:call-template name="TemplatePassword">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="current()/@PropertyType = '12'">
            <!--Entity-->
            <xsl:call-template name="TemplateList">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="current()/@PropertyType = '2'">
            <!--Number-->
            <xsl:call-template name="TemplateNumber">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:when test="current()/@PropertyType = '3' or current()/@PropertyType = '10'">
            <!--Decimal y Currency-->
            <xsl:call-template name="TemplateDecimalCurrency">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:when>
         <xsl:otherwise>
            <!--Input-->
            <xsl:call-template name="TemplateInput">
               <xsl:with-param name="property" select="$property"/>
            </xsl:call-template>
         </xsl:otherwise>
      </xsl:choose>
   </xsl:template>
   <!--Atributos genéricos de la propiedad-->
   <xsl:template name="TemplateGenericAttributes">
      <xsl:param name="property"/>
      <xsl:attribute name="Id">__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="$property/@EntityName"/></xsl:attribute>
      <xsl:attribute name="EntityProperty">true</xsl:attribute>
      <xsl:attribute name="Name">
         <xsl:value-of select="$property/@PropertyName"/>
      </xsl:attribute>
      <xsl:attribute name="PropertyType">
         <xsl:value-of select="$property/@PropertyType"/>
      </xsl:attribute>
      <xsl:attribute name="Required">
         <xsl:value-of select="$property/@Required"/>
      </xsl:attribute>
      <xsl:attribute name="DisplayName">
         <xsl:value-of select="$property/@PropertyDisplayName"/>
      </xsl:attribute>
      <xsl:attribute name="EntityName">
         <xsl:value-of select="$property/@EntityName"/>
      </xsl:attribute>
   </xsl:template>
   <!--Atributos de tipo texto-->
   <xsl:template name="TemplateInput">
      <xsl:param name="property"/>
      <input type="text">
         <xsl:attribute name="value">
            <xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/>
         </xsl:attribute>
         <xsl:attribute name="Maxlength">
            <xsl:value-of select="current()/@Length"/>
         </xsl:attribute>
         <xsl:call-template name="TemplateGenericAttributes">
            <xsl:with-param name="property" select="$property"/>
         </xsl:call-template>
      </input>
   </xsl:template>
   <!--Atributos de tipo number-->
   <xsl:template name="TemplateNumber">
      <xsl:param name="property"/>
      <input type="text" onkeypress="return onlyNumbersTitanFL(event)">
         <xsl:attribute name="value">
            <xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/>
         </xsl:attribute>
         <xsl:attribute name="Maxlength">
            <xsl:value-of select="current()/@Length"/>
         </xsl:attribute>
         <xsl:call-template name="TemplateGenericAttributes">
            <xsl:with-param name="property" select="$property"/>
         </xsl:call-template>
      </input>
   </xsl:template>
   <!--Atributos de tipo decimal y currency-->
   <xsl:template name="TemplateDecimalCurrency">
      <xsl:param name="property"/>
      <input type="text">
         <xsl:attribute name="value">
            <xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/>
         </xsl:attribute>
         <xsl:attribute name="Maxlength">
            <xsl:value-of select="current()/@Length"/>
         </xsl:attribute>
         <xsl:call-template name="TemplateGenericAttributes">
            <xsl:with-param name="property" select="$property"/>
         </xsl:call-template>
      </input>
   </xsl:template>
   <!--Atributos de tipo booleano-->
   <xsl:template name="TemplateBoolean">
      <xsl:param name="property"/>
      <Input type="checkbox">
         <xsl:call-template name="TemplateGenericAttributes">
            <xsl:with-param name="property" select="$property"/>
         </xsl:call-template>
         <xsl:if test="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value = 'True' or //InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value = '1'">
            <xsl:attribute name="checked">true</xsl:attribute>
         </xsl:if>
      </Input>
   </xsl:template>
   <!--Atributos de tipo password-->
   <xsl:template name="TemplatePassword">
      <xsl:param name="property"/>
      <Input type="password" readOnly="true">
         <xsl:call-template name="TemplateGenericAttributes">
            <xsl:with-param name="property" select="$property"/>
         </xsl:call-template>
         <xsl:attribute name="value">
            <xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/>
         </xsl:attribute>
      </Input>
      &#160;
      <input type="button" id="btnManagePassword" value="...">
         <xsl:attribute name="onclick">ManagePassword(__Property<xsl:value-of select="current()/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>, '<xsl:value-of select="//InstanceData/@Guid"/>', '<xsl:value-of select="current()/@PropertyName"/>')</xsl:attribute>
      </input>
   </xsl:template>
   <!--Atributos de tipo área de texto-->
   <xsl:template name="TemplateTextArea">
      <xsl:param name="property"/>
      <TextArea  style="width:90%">
         <xsl:attribute name="LengthTextArea">
            <xsl:value-of select="current()/@Length"/>
         </xsl:attribute>
         <xsl:call-template name="TemplateGenericAttributes">
            <xsl:with-param name="property" select="$property"/>
         </xsl:call-template>
         <xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/>
      </TextArea>
   </xsl:template>
   <!--Atributos de tipo fecha-->
   <xsl:template name="TemplateDate">
      <xsl:param name="property"/>
      <Select>
         <xsl:attribute name="Id">__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>Year</xsl:attribute>
         <xsl:attribute name="onchange">FillDateDays(this.id, '__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>Month', '__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>Day', '__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>')</xsl:attribute>
      </Select>
      &#160;
      <Select>
         <xsl:attribute name="Id">__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>Month</xsl:attribute>
         <xsl:attribute name="onchange">FillDateDays('__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>Year', this.id, '__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>Day', '__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>')</xsl:attribute>
      </Select>
      &#160;
      <Select>
         <xsl:attribute name="Id">__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>Day</xsl:attribute>
         <xsl:attribute name="onchange">FillControlHidden('__Property<xsl:value-of select="$property/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>')</xsl:attribute>
         <!--<option value="-1">- Día -</option>-->
      </Select>
      <Input Type="hidden">
         <xsl:attribute name="value">
            <xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/>
         </xsl:attribute>
         <xsl:call-template name="TemplateGenericAttributes">
            <xsl:with-param name="property" select="$property"/>
         </xsl:call-template>
         <xsl:attribute name="TypeDate">true</xsl:attribute>
      </Input>
   </xsl:template>
   <!--Atributos de tipo listado-->
   <xsl:template name="TemplateList">
      <xsl:param name="property"/>
      <table cellpadding="0" cellspacing="0">
         <tr>
            <td>
               <xsl:choose>
                  <xsl:when test="//EntityReferences/EntityReference[@EntityName=$property/@PropertySourceEntityName]/@InstanceCountOutOfRange = 'True'">
                     <input type="text" readOnly="true">
                        <xsl:attribute name="value"><xsl:value-of select="//EntityReference/ReferenceInstanceData[@PropertyName = $property/@PropertyName]/@Name"/></xsl:attribute>
                        <xsl:attribute name="id">__Property<xsl:value-of select="current()/@PropertyName"/>_<xsl:value-of select="$property/@EntityName"/>_Text</xsl:attribute>
                        <xsl:attribute name="TypeListOutOfRange">True</xsl:attribute>
                     </input>
                     <input type="hidden">
                        <xsl:attribute name="value"><xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/></xsl:attribute>
                        <xsl:call-template name="TemplateGenericAttributes">
                           <xsl:with-param name="property" select="$property"/>
                        </xsl:call-template>
                     </input>
                  </xsl:when>
                  <xsl:otherwise>
                     <Select style="width:200px">
                        <xsl:call-template name="TemplateGenericAttributes">
                           <xsl:with-param name="property" select="$property"/>
                        </xsl:call-template>
                        <!--Valor del campo-->
                        <xsl:variable name="propertyValue">
                           <xsl:value-of select="//InstanceData/PropertyData[@PropertyName = $property/@PropertyName]/@Value"/>
                        </xsl:variable>
                        <xsl:choose>
                           <xsl:when test="$property/@PropertySourceEntitySelectionType = '0'">
                              <xsl:attribute name="PropertySourceEntitySelectionType">0</xsl:attribute>
                              <option value="-1">-- Seleccione --</option>
                              <xsl:for-each select="//EntityReferences/EntityReference[@EntityName=$property/@PropertySourceEntityName]/ReferenceInstanceData">
                                 <option>
                                    <xsl:attribute name="value">
                                       <xsl:value-of select="current()/@Id"/>
                                    </xsl:attribute>
                                    <xsl:if test="current()/@Id = $propertyValue">
                                       <xsl:attribute name="selected">true</xsl:attribute>
                                    </xsl:if>
                                    <xsl:value-of select="current()/@Name"/>
                                 </option>
                              </xsl:for-each>
                           </xsl:when>
                           <xsl:otherwise>
                              <xsl:attribute name="size">4</xsl:attribute>
                              <xsl:attribute name="PropertySourceEntitySelectionType">1</xsl:attribute>
                              <xsl:for-each select="//EntityReferences/EntityReference[@EntityName=$property/@PropertySourceEntityName]/ReferenceInstanceData">
                                 <xsl:if test="contains($propertyValue, concat('|', current()/@Id, '|'))">
                                    <option>
                                       <xsl:attribute name="value">
                                          <xsl:value-of select="current()/@Id"/>
                                       </xsl:attribute>
                                       <xsl:value-of select="current()/@Name"/>
                                    </option>
                                 </xsl:if>
                              </xsl:for-each>
                           </xsl:otherwise>
                        </xsl:choose>
                     </Select>
                  </xsl:otherwise>
               </xsl:choose>
            </td>
            <xsl:choose>
               <xsl:when test="$property/@PropertySourceEntitySelectionType = '1'">
                  <td valign="middle" align="left">
                     &#160;
                     <input type="button" id="btnManagePassword" value="...">
                        <xsl:attribute name="onclick">
                           GetEntityReferenceData(__Property<xsl:value-of select="current()/@PropertyName"/>_<xsl:value-of select="current()/@EntityName"/>, '<xsl:value-of select="current()/@PropertySourceEntityName"/>', '<xsl:value-of select="current()/@PropertySourceEntityDisplayProperty"/>')
                        </xsl:attribute>
                     </input>
                  </td>
               </xsl:when>
               <xsl:otherwise>
                  <xsl:if test="//EntityReferences/EntityReference[@EntityName=$property/@PropertySourceEntityName]/@InstanceCountOutOfRange = 'True'">
                     <td valign="middle" align="left">
                        &#160;
                        <input type="button" id="btnManageOutOfRangeInstances" value="...">
                           <xsl:attribute name="onclick">
                              GetManageOutOfRangeInstances('<xsl:value-of select="//@System"/>', '<xsl:value-of select="current()/@PropertySourceEntityName"/>', '<xsl:value-of select="current()/@PropertySourceEntityDisplayProperty"/>', __Property<xsl:value-of select="current()/@PropertyName"/>_<xsl:value-of select="$property/@EntityName"/>_Text, __Property<xsl:value-of select="current()/@PropertyName"/>_<xsl:value-of select="$property/@EntityName"/>)
                           </xsl:attribute>
                        </input>
                     </td>
                  </xsl:if>
               </xsl:otherwise>
            </xsl:choose>
         </tr>
      </table>
   </xsl:template>
</xsl:stylesheet>

