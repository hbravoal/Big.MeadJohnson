<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:template match="/">

      <xsl:variable name="varViewName" select="BusinessEntityView/@ViewName" />
      <hr />
      <input id="btnAddGroupNew" type="button" value="Ingresar Grupo" onclick="AddGroupView()" />
      <br/>
      <br/>
      <table width ="100%" cellpadding="3" cellspacing="0" border="0" bordercolor="white">
         <tr>
            <td width="17"></td>
            <td width="17"></td>
            <td><strong>Grupo</strong></td>
         </tr>
         <xsl:for-each select="BusinessEntityView/Groups/Group">
            <tr>
               <td colspan="4">
                  <hr />
               </td>
            </tr>
            <tr bgcolor="LightSteelBlue">
               <td align="center">
                  <img src="../../Images/icon-edit.gif" style="cursor:pointer">
                     <xsl:attribute name="onclick">
                        EditViewGroup('<xsl:value-of select="$varViewName"/>', '<xsl:value-of select="@GroupName"/>')
                     </xsl:attribute>
                  </img>
               </td>
               <td align="center">
                  <img src="../../Images/icon-delete.gif" style="cursor:pointer">
                     <xsl:attribute name="onclick">
                        DeleteViewGroup('<xsl:value-of select="$varViewName"/>', '<xsl:value-of select="@GroupName"/>')
                     </xsl:attribute>
                  </img>
               </td>
               <td>
                  <xsl:value-of select="@GroupDisplayName"/>
               </td>
            </tr>
            <xsl:for-each select="current()/PropertiesGroup/Property">
               <tr>
                  <xsl:if test="position() mod 2 = 0">
                     <xsl:attribute name="bgcolor">Gainsboro</xsl:attribute>
                  </xsl:if>
                  <td></td>
                  <td></td>
                  <td>
                     <xsl:value-of select="current()/@PropertyName"/>
                  </td>
               </tr>
            </xsl:for-each>
         </xsl:for-each>
      </table>

   </xsl:template>

</xsl:stylesheet>

