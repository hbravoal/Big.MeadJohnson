<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

   <table border="0" width="100%" style="padding-left:10px;padding-top:10px">
      <tr>
         <td style="width: 30%;text-align:right">
            Nombre del Cluster
         </td>
         <td style="width: 70%">
            <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtClusterNameEdit" onkeypress="return inCharacters(event)" disabled="disabled" maxlength="128">
               <xsl:attribute name="value">
                  <xsl:value-of select="Cluster/@ClusterName"/>
               </xsl:attribute>
            </input>
         </td>
      </tr>
      <tr>
         <td style="width: 30%;text-align:right">
            <span style="color:Red">*</span>&#160;Nombre a mostrar del Cluster
         </td>
         <td style="width: 70%">
            <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtClusterDisplayNameEdit" maxlength="128">
               <xsl:attribute name="value">
                  <xsl:value-of select="Cluster/@ClusterDisplayName"/>
               </xsl:attribute>
            </input>
         </td>
      </tr>
      <tr>
         <td colspan="2">
            &#32;
         </td>
      </tr>
   </table>
   <table border="0" width="100%" cellpadding="10px">
      <tr>
         <td align="center"><strong>Entidades del Negocio</strong></td>
         <td></td>
         <td align="center"><span style="color:Red">*</span>&#160;&#160;<strong>Entidades del Cluster</strong></td>
      </tr>
      <tr>
         <td style="width: 47%; text-align:center;">
            <select style="width: 250px;" id="lstBusinessEntitiesEdit" size="10" >
            </select>
         </td>
         <td style="width: 6%;" align="center">
            <table>
               <tr>
                  <td align="center">
                     <img src="../../Images/Cluster/last.gif" style="cursor: pointer" onclick="AllBusinessEntititesToCluster('Edit')" />
                  </td>
               </tr>
               <tr>
                  <td align="center">
                     <img src="../../Images/Cluster/next.gif" style="cursor: pointer" onclick="SomeBusinessEntititesToCluster('Edit')"/>
                  </td>
               </tr>
               <tr>
                  <td align="center">
                     <img src="../../Images/Cluster/previous.gif" style="cursor: pointer" onclick="SomeClusterToBusinessEntitites('Edit')"/>
                  </td>
               </tr>
               <tr>
                  <td align="center">
                     <img src="../../Images/Cluster/first.gif" style="cursor: pointer" onclick="AllClusterToBusinessEntitites('Edit')"/>
                  </td>
               </tr>
            </table>
         </td>
         <td style="width: 47%; text-align:center;">
            <select style="width: 250px;" id="lstClustersEdit" size="10" >
               <xsl:for-each select="Cluster/BusinessEntity">
                  <option>
                     <xsl:attribute name="value">
                        <xsl:value-of select="current()/@EntityName"/>
                     </xsl:attribute>
                     <xsl:value-of select="current()/@EntityName"/>
                  </option>
               </xsl:for-each>
            </select>
         </td>
      </tr>
   </table>
   
</xsl:template>

</xsl:stylesheet> 

