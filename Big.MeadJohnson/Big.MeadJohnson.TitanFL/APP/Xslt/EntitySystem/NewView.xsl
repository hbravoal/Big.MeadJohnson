<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">
    <table cellpadding="3" cellspacing="0" border="0" bordercolor="white">
      <tr>
        <td style="text-align:right">
          <span style="color:Red">*</span>&#160;Nombre del Rol
        </td>
        <td>
          <input style="width:200px"  type="text" id="txtViewNameNew" onkeypress="return inCharacters(event)" maxlength="128"/>
        </td>
      </tr>
      <tr>
        <td style="text-align:right">
          <span style="color:Red">*</span>&#160;Nombre a mostrar del Rol
        </td>
        <td>
          <input style="width:200px" type="text" id="txtViewDisplayNameNew" maxlength="128"/>
        </td>
      </tr>
      <tr>
        <td style="text-align:right">
          <span style="color:Red">*</span>&#160;Grupo
        </td>
        <td>
          <select id="ddlRoleTypeNew" style="width:202px">
            <!--onchange="GetPropertiesCluster(this.id)">-->
            <option>Seleccione...</option>
            <xsl:for-each select="ReferentialData/RoleGroups/RoleGroup">
              <option>
                <xsl:attribute name="value">
                  <xsl:value-of select="current()/@Id"/>
                </xsl:attribute>
                <xsl:value-of select="current()/@Description"/>
              </option>
            </xsl:for-each>
          </select>
        </td>
      </tr>
      <tr>
        <td style="text-align:right">
          <span style="color:Red">*</span>&#160;Cluster
        </td>
        <td>
          <select id="ddlClusterNew" style="width:202px">
            <!--onchange="GetPropertiesCluster(this.id)">-->
            <option>Seleccione...</option>
            <xsl:for-each select="ReferentialData/Clusters/Cluster">
              <option>
                <xsl:attribute name="value">
                  <xsl:value-of select="current()/@Name"/>
                </xsl:attribute>
                <xsl:value-of select="current()/@DisplayName"/>
              </option>
            </xsl:for-each>
          </select>
        </td>
      </tr>
    </table>
    <table cellpadding="3" cellspacing="0" border="0" bordercolor="white">
      <tr>
        <td>
          <input id="btnSaveNew" type="button" value="Guardar" onclick="saveNewView('New')" />
        </td>
        <td>
          <input id="btnGenerateNew" type="button" value="Generar" onclick="Generate()" disabled="disabled"/>
        </td>
        <td>
          <input id="btnCancelNew" type="button" value="Cancelar" onclick="Load()" />
        </td>
        <td>
          <input id="btnGroupsNew" type="button" value="Grupos" onclick="GetGroupsView()" disabled="disabled"/>
        </td>
        <td>
          <input id="btnPropertiesNew" type="button" value="Propiedades" onclick="GetPropertiesCluster()" disabled="disabled"/>
        </td>
      </tr>
    </table>
    <div id="divWorkArea"></div>

  </xsl:template>

</xsl:stylesheet>

