<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
   <xsl:template match="/">
      <table width="100%" cellpading="0" cellspacing="0" border="0">
         <tr>
            <td colspan="3">
               <font style="font-weight:bold; font-size:large">
                  <xsl:value-of select="/Entity/@EntityDisplayName"/>
               </font>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <hr/>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <input type="button" value="Ingresar" style="width:100px">
                  <xsl:attribute name="onclick">
                     LoadSystemEntityInsertForm('<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',<xsl:value-of select="/Entity/SearchParameters/@PageNumber"/>,'<xsl:value-of select="/Entity/SearchParameters/@SortBy"/>')
                  </xsl:attribute>
               </input>&#160;<input type="button" value="Buscar" style="width:100px"  ></input>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <table  width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="white">
                  <tr bgcolor="LightSteelBlue">
                     <td width="17"></td>
                     <td width="17"></td>
                     <xsl:for-each select="/Entity/Properties/Property">
                        <td>
                           <a>
                              <xsl:attribute name="href">
                                 javascript:LoadSystemEntityForm('<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',<xsl:value-of select="/Entity/SearchParameters/@PageNumber"/>,'<xsl:value-of select="current()/@PropertyDisplayName"/>')
                              </xsl:attribute>
                              <strong>
                                 <xsl:value-of select="current()/@PropertyDisplayName"/>
                              </strong>
                           </a>
                        </td>
                     </xsl:for-each>
                  </tr>
                  <xsl:for-each select="/Entity/EntityData/InstanceData">
                     <!--xsl:sort select="PropertyData/@Value[PropertyData/@PropertyName='Id']" data-type="number" order="ascending"/-->
                     <xsl:variable name="property" select="current()"/>
                     <tr>
                        <xsl:if test="position() mod 2 = 0">
                           <xsl:attribute name="bgcolor">Gainsboro</xsl:attribute>
                        </xsl:if>
                        <td align="center">
                           <img src="../../Images/icon-edit.gif" style="cursor:pointer">
                              <xsl:attribute name="onclick">
                                 LoadSystemEntityUpdateForm('<xsl:value-of select="$property/@Guid"/>', '<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',<xsl:value-of select="/Entity/SearchParameters/@PageNumber"/>,'<xsl:value-of select="/Entity/SearchParameters/@SortBy"/>','<xsl:value-of select="/Entity/SearchParameters/@SortOrder"/>')
                              </xsl:attribute>
                           </img>
                        </td>
                        <td align="center">
                           <img src="../../Images/icon-delete.gif" style="cursor:pointer">
                              <xsl:attribute name="onclick">
                                 DeleteEntityData('<xsl:value-of select="$property/@Guid"/>', '<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',<xsl:value-of select="/Entity/SearchParameters/@PageNumber"/>,'<xsl:value-of select="/Entity/SearchParameters/@SortBy"/>','<xsl:value-of select="/Entity/SearchParameters/@SortOrder"/>');
                              </xsl:attribute>
                           </img>
                        </td>
                        <xsl:for-each select="/Entity/Properties/Property">
                           <td>
                              <xsl:value-of select="$property/PropertyData[@PropertyName=current()/@PropertyName]/@Value"/>
                           </td>
                        </xsl:for-each>
                     </tr>
                  </xsl:for-each>
               </table>
            </td>
         </tr>
         <tr>
            <td>
               <b>
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadSystemEntityForm('<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',1,'<xsl:value-of select="/Entity/SearchParameters/@SortBy"/>')
                     </xsl:attribute>
                     |&#60;
                  </a>
                  &#160;
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadSystemEntityForm('<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',<xsl:value-of select="/Entity/SearchParameters/@LastPage"/>,'<xsl:value-of select="/Entity/SearchParameters/@SortBy"/>')
                     </xsl:attribute>
                     &#60;
                  </a>
                  &#160;
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadSystemEntityForm('<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',<xsl:value-of select="/Entity/SearchParameters/@NextPage"/>,'<xsl:value-of select="/Entity/SearchParameters/@SortBy"/>')
                     </xsl:attribute>
                     &#62;
                  </a>
                  &#160;
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadSystemEntityForm('<xsl:value-of select="/Entity/@EntitySystem"/>','<xsl:value-of select="/Entity/@EntityName"/>',<xsl:value-of select="/Entity/SearchParameters/@TotalPages"/>,'<xsl:value-of select="/Entity/SearchParameters/@SortBy"/>')
                     </xsl:attribute>
                     &#62;|
                  </a>
                  &#160;Pagina <xsl:value-of select="/Entity/SearchParameters/@PageNumber"/> de <xsl:value-of select="/Entity/SearchParameters/@TotalPages"/>
               </b>
            </td>
            <td colspan="2" align="right">
               <b>
                  Registro(s) <xsl:value-of select="/Entity/SearchParameters/@InstanceStartNumber"/> - <xsl:value-of select="/Entity/SearchParameters/@InstanceEndNumber"/> de <xsl:value-of select="/Entity/SearchParameters/@TotalInstances"/>
               </b>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <hr/>
            </td>
         </tr>
         <tr>
            <td colspan="3" align="right">
               <a href="#">
                  D e s c a r g a r
               </a>
            </td>
         </tr>
      </table>
      <div></div>
   </xsl:template>
</xsl:stylesheet>

