<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">

   <input id="btnAddCluster" type="button" value="Ingresar" onclick="newCluster()" />
   <br/>
   <br/>
   <table width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="white">
      <tr bgcolor="LightSteelBlue">
         <td width="17"></td>
         <td width="17"></td>
         <td>
            <strong>Cluster</strong>
         </td>
      </tr>
      <xsl:for-each select="/Clusters/Cluster">
         <tr>
            <td align="center">
               <img src="../../Images/icon-edit.gif" style="cursor:pointer">
                  <xsl:attribute name="onclick">editCluster('<xsl:value-of select="current()/@Name"/>')</xsl:attribute>
               </img>
            </td>
            <td align="center">
               <img src="../../Images/icon-delete.gif" style="cursor:pointer">
                  <xsl:attribute name="onclick">deleteCluster('<xsl:value-of select="current()/@Name"/>')</xsl:attribute>
               </img>
            </td>
            <td>
               <xsl:value-of select="current()/@DisplayName"/>
            </td>
         </tr>   
      </xsl:for-each>
   </table>
    
</xsl:template>

</xsl:stylesheet> 

