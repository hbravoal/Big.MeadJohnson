<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:param name="currentSortBy"/>
   <xsl:param name="currentSortOrder"/>
   <xsl:param name="currentSearchCriteria"/>
   <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
   <xsl:template match="/">
      <table width="100%" cellpading="0" cellspacing="0" border="0">
         <tr>
            <td colspan="3">
               <font style="font-weight:bold; font-size:large">
                  <xsl:value-of select="/BusinessEntityView/@ViewDisplayName"/>
               </font>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <hr/>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <input type="button" value="Ingresar" style="width:100px">
                  <xsl:attribute name="onclick">
                     LoadViewInsertForm('<xsl:value-of select="/BusinessEntityView/@ViewName"/>')
                  </xsl:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <table  width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="white">
                  <tr bgcolor="LightSteelBlue">
                     <td width="17"></td>
                     <td width="17"></td>
                     <xsl:for-each select="//Property">
                        <xsl:if test="not( (current()/@PropertyType = '11') or (current()/@PropertyType = '12' and (current()/@PropertySourceEntitySelectionType = '1' or current()/@PropertySourceEntitySelectionType = '0')))">
                           <td>
                              <strong>
                                 <xsl:value-of select="current()/@PropertyName"/>
                              </strong>
                           </td>
                        </xsl:if>
                     </xsl:for-each>
                  </tr>
                  <xsl:for-each select="/BusinessEntityView/ViewData/InstanceData">
                     <!--xsl:sort select="PropertyData/@Value[PropertyData/@PropertyName='Id']" data-type="number" order="ascending"/-->
                     <xsl:variable name="property" select="current()"/>
                     <tr>
                        <xsl:if test="position() mod 2 = 0">
                           <xsl:attribute name="bgcolor">Gainsboro</xsl:attribute>
                        </xsl:if>
                        <td align="center">
                           <img src="../../Images/icon-edit.gif" style="cursor:pointer">
                              <xsl:attribute name="onclick">
                                 LoadViewUpdateForm('<xsl:value-of select="$property/@Guid"/>', '<xsl:value-of select="/BusinessEntityView/@ViewName"/>')
                              </xsl:attribute>
                           </img>
                        </td>
                        <td align="center">
                           <img src="../../Images/icon-delete.gif" style="cursor:pointer">
                              <xsl:attribute name="onclick">
                                 DeleteViewData('<xsl:value-of select="$property/@Guid"/>', '<xsl:value-of select="/BusinessEntityView/@ViewName"/>');
                              </xsl:attribute>
                           </img>
                        </td>
                        <xsl:for-each select="//Property">
                           <xsl:if test="not( (current()/@PropertyType = '11') or (current()/@PropertyType = '12' and (current()/@PropertySourceEntitySelectionType = '1' or current()/@PropertySourceEntitySelectionType = '0')))">
                              <td>
                                 <xsl:value-of select="$property/PropertyData[@PropertyName=current()/@PropertyName][@EntityName=current()/@EntityName]/@Value"/>
                              </td>
                           </xsl:if>
                        </xsl:for-each>
                     </tr>
                  </xsl:for-each>
               </table>
            </td>
         </tr>
         <tr>
            <td>
               <b>
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadViewForm('<xsl:value-of select="/BusinessEntityView/@ViewName"/>',1,'<xsl:value-of select="/BusinessEntityView/SearchParameters/@SortBy"/>', '<xsl:value-of select="$currentSortOrder"/>', '<xsl:value-of select="$currentSearchCriteria"/>')
                     </xsl:attribute>
                     |&#60;
                  </a>
                  &#160;
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadViewForm('<xsl:value-of select="/BusinessEntityView/@ViewName"/>',<xsl:value-of select="/BusinessEntityView/SearchParameters/@LastPage"/>,'<xsl:value-of select="/BusinessEntityView/SearchParameters/@SortBy"/>', '<xsl:value-of select="$currentSortOrder"/>', '<xsl:value-of select="$currentSearchCriteria"/>')
                     </xsl:attribute>
                     &#60;
                  </a>
                  &#160;
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadViewForm('<xsl:value-of select="/BusinessEntityView/@ViewName"/>',<xsl:value-of select="/BusinessEntityView/SearchParameters/@NextPage"/>,'<xsl:value-of select="/BusinessEntityView/SearchParameters/@SortBy"/>', '<xsl:value-of select="$currentSortOrder"/>', '<xsl:value-of select="$currentSearchCriteria"/>')
                     </xsl:attribute>
                     &#62;
                  </a>
                  &#160;
                  <a>
                     <xsl:attribute name="href">
                        javascript:LoadViewForm('<xsl:value-of select="/BusinessEntityView/@ViewName"/>',<xsl:value-of select="/BusinessEntityView/SearchParameters/@TotalPages"/>,'<xsl:value-of select="/BusinessEntityView/SearchParameters/@SortBy"/>', '<xsl:value-of select="$currentSortOrder"/>', '<xsl:value-of select="$currentSearchCriteria"/>')
                     </xsl:attribute>
                     &#62;|
                  </a>
                  &#160;Pagina <xsl:value-of select="/BusinessEntityView/SearchParameters/@PageNumber"/> de <xsl:value-of select="/BusinessEntityView/SearchParameters/@TotalPages"/>
               </b>
            </td>
            <td colspan="2" align="right">
               <b>
                  Registro(s) <xsl:value-of select="/BusinessEntityView/SearchParameters/@InstanceStartNumber"/> - <xsl:value-of select="/BusinessEntityView/SearchParameters/@InstanceEndNumber"/> de <xsl:value-of select="/BusinessEntityView/SearchParameters/@TotalInstances"/>
               </b>
            </td>
         </tr>
         <!--tr>
            <td colspan="3">
               <hr/>
            </td>
         </tr>
         <tr>
            <td colspan="3" align="right">
               <a href="#">
                  D e s c a r g a r
               </a>
            </td>
         </tr-->
      </table>
   </xsl:template>
</xsl:stylesheet>

