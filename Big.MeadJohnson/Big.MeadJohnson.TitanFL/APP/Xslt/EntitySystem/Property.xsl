<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:param name="propertyValue"/>
  <xsl:template match="/">

    <table width="100%" border="0" style="padding-left:10px;padding-top:10px">
      <!-- Titulo -->
      <tr bgcolor="LightSteelBlue" style="text-align:center;">
        <td colspan="2">
          <b>
            <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertyDisplayName"/>
          </b>
        </td>
      </tr>
      <tr>
        <td>
          &#160;
        </td>
      </tr>
      <!-- Atributos -->
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Nombre de la Propiedad
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" disabled="disabled" id="txtPropertyName">
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertyName"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          <span style="color:Red">*</span>Nombre a mostrar de la Propiedad
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" id="txtPropertyDisplayName">
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertyDisplayName"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Expresión Regular
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" id="txtPropertyRexValidator">
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType != '0' and //Property[@PropertyName=$propertyValue]/@PropertyType != '1'">
              <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertyRexValidator"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Formato
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" id="txtPropertyDisplayFormat">
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertyDisplayFormat"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td>&#160;</td>
        <td>
          <table border="0" cellpadding="0" cellspacing="0">
            <tr>
              <td>
                <table>
                  <tr>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkRequired">
                        <xsl:if test="//Property[@PropertyName=$propertyValue]/@Required = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '14'" >
                          <xsl:attribute name="disabled">disabled</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                    <td style="vertical-align: text-top;">
                      Requerido
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkUnique">
                        <xsl:if test="Entity/@KeyProperty != ''">
                          <xsl:attribute name="disabled">disabled</xsl:attribute>
                        </xsl:if>
                        <xsl:if test="Entity/@KeyProperty = //Property[@PropertyName=$propertyValue]/@PropertyName">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                    <td style="vertical-align: text-top;">
                      Único
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkPropertyState">
                        <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyState = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                    <td style="vertical-align: text-top;">
                      Estado
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkPropertyEncrypt">
                        <xsl:if test="//Property[@PropertyName=$propertyValue]/@Encrypt = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                    <td style="vertical-align: text-top;">
                      Encriptar
                    </td>
                  </tr>
                </table>
              </td>
              <td style="padding-left:20px">
                <table>
                  <tr>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkPropertyIndexed">
                        <xsl:if test="Entity/@KeyProperty = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                    <td style="vertical-align: text-top;">
                      Indexar
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkPropertyTimeStamEnable">
                        <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyTimeStampEnable = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                    <td style="vertical-align: text-top;">
                      Estampilla de cambios
                    </td>
                  </tr>
                  <tr>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkPropertySearchable">
                        <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertySearchable = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                    <td style="vertical-align: text-top;">
                      Búsqueda
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          <span style="color:Red">*</span>&#160;Tipo de Propiedad
        </td>
        <td style="width:60%;">
          <select style="width:90%; margin-bottom:2px;" id="lstPropertyType" onchange="habilityControlsEntitys(this.id)">
            <option>Seleccione...</option>
            <option value="0">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '0'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Cadena de caracteres
            </option>
            <option value="1">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '1'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Cadena larga de caracteres
            </option>
            <option value="2">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '2'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Número
            </option>
            <option value="3">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '3'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Decimal
            </option>
            <option value="4">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '4'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Verdadero/Falso
            </option>
            <!--<option value="5">
                     <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '5'">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                     </xsl:if>
                     Binary
                  </option>
                  <option value="6">
                     <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '6'">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                     </xsl:if>
                     LongBinary
                  </option>-->
            <option value="7">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '7'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Fecha
            </option>
            <!--<option value="8">
                     <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '8'">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                     </xsl:if>
                     Date
                  </option>
                  <option value="9">
                     <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '9'">
                        <xsl:attribute name="selected">selected</xsl:attribute>
                     </xsl:if>
                     Time
                  </option>-->
            <option value="10">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '10'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Moneda
            </option>
            <option value="11">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '11'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Contraseña
            </option>
            <option value="12">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '12'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Entidad
            </option>
            <option value="13">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '13'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Correo electrónico
            </option>
            <option value="14">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '14'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Identidad
            </option>
            <option value="15">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType = '15'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Ubicación Geográfica
            </option>
          </select>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Nombre de la Entidad fuente
        </td>
        <td style="width:60%;">
          <select style="width:90%; margin-bottom:2px;" id="ddlPropertySourceEntityName" onchange="fillPropertySourceEntityDisplayProperty(this.id)">
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType != '12'">
              <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <option>Seleccione...</option>
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertySourceEntityName != ''">
              <option selected="selected">
                <xsl:attribute name="value">
                  <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertySourceEntityName"/>
                </xsl:attribute>
                <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertySourceEntityName"/>
              </option>
            </xsl:if>
          </select>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Propiedad a mostrar de la Entidad
        </td>
        <td style="width:60%;">
          <select style="width:90%; margin-bottom:2px;" id="ddlPropertySourceEntityDisplayProperty">
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType != '12'">
              <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <option>Seleccione...</option>
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertySourceEntityDisplayProperty != ''">
              <option selected="selected">
                <xsl:attribute name="value">
                  <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertySourceEntityDisplayProperty"/>
                </xsl:attribute>
                <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@PropertySourceEntityDisplayProperty"/>
              </option>
            </xsl:if>
          </select>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Tipo de selección
        </td>
        <td style="width:60%;">
          <select style="width:90%; margin-bottom:2px;" id="ddlPropertySourceEntitySelectionType">
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType != '12'">
              <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <option>Seleccione...</option>
            <option value="1">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertySourceEntitySelectionType = '1'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Varios valores
            </option>
            <option value="0">
              <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertySourceEntitySelectionType = '0'">
                <xsl:attribute name="selected">selected</xsl:attribute>
              </xsl:if>
              Un sólo valor
            </option>
          </select>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Longitud
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" id="txtLength" maxlength="4" onkeypress="return onlyNumbers(event)">
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType != '0' and //Property[@PropertyName=$propertyValue]/@PropertyType != '1' and //Property[@PropertyName=$propertyValue]/@PropertyType != '11' and //Property[@PropertyName=$propertyValue]/@PropertyType != '2' and //Property[@PropertyName=$propertyValue]/@PropertyType != '3' and //Property[@PropertyName=$propertyValue]/@PropertyType != '10' and //Property[@PropertyName=$propertyValue]/@PropertyType != '13'" >
              <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@Length"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Precisión
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" id="txtPrecision" maxlength="2" onkeypress="return onlyNumbers(event)">
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType != '2' and //Property[@PropertyName=$propertyValue]/@PropertyType != '3' and //Property[@PropertyName=$propertyValue]/@PropertyType != '10'">
              <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@Precision"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Escala
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" id="txtScale" maxlength="2" onkeypress="return onlyNumbers(event)">
            <xsl:if test="//Property[@PropertyName=$propertyValue]/@PropertyType != '2' and //Property[@PropertyName=$propertyValue]/@PropertyType != '3' and //Property[@PropertyName=$propertyValue]/@PropertyType != '10'">
              <xsl:attribute name="disabled">disabled</xsl:attribute>
            </xsl:if>
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@Scale"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:40%;text-align:right">
          Descripción
        </td>
        <td style="width:60%;">
          <input style="width:90%; margin-bottom:2px;" type="text" id="txtToolTipText">
            <xsl:attribute name="value">
              <xsl:value-of select="//Property[@PropertyName=$propertyValue]/@ToolTipText"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
    </table>

  </xsl:template>
</xsl:stylesheet>