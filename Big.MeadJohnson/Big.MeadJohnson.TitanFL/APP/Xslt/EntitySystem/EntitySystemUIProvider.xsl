<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
  <xsl:template match="/">
    <xsl:variable name="entityValue" select="Entity/@EntityName"></xsl:variable>
    <xsl:variable name="varKeyProperty" select="Entity/@KeyProperty"></xsl:variable>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr>
        <td>
          <table cellpadding="0" cellspacing="0" border="0" width="100%">
            <tr>
              <td>
                <table border="0" cellpadding="1" cellspacing="0">
                  <tr>
                    <td style="text-align:right">Nombre de la Entidad&#160;</td>
                    <td>
                      <input style="width:300px" type="text" id="txtEntityName" disabled="disabled">
                        <xsl:attribute name="value">
                          <xsl:value-of select="Entity/@EntityName"/>
                        </xsl:attribute>
                      </input>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align:right">
                      <span style="color:Red">*</span>&#160;Nombre a mostrar de la Entidad&#160;
                    </td>
                    <td>
                      <input style="width:300px" type="text" maxlength="128" id="txtEntityDisplayName">
                        <xsl:attribute name="value">
                          <xsl:value-of select="Entity/@EntityDisplayName"/>
                        </xsl:attribute>
                      </input>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align:right">Tipo de la Entidad&#160;</td>
                    <td>
                      <select style="width:300px" id="ddlEntityType" disabled="disabled">
                        <option>Seleccione...</option>
                        <xsl:if test="Entity/@EntityType != ''">
                          <option selected="selected">
                            <xsl:attribute name="value">
                              <xsl:value-of select="Entity/@EntityType"/>
                            </xsl:attribute>
                            <xsl:value-of select="Entity/@EntityType"/>
                          </option>
                        </xsl:if>
                      </select>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align:right">Descripción&#160;</td>
                    <td style="width: 55%">
                      <textarea rows="3" cols="35" id="txtEntityDescription">
                        <xsl:value-of select="Entity/@Description"/>
                      </textarea>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align:right">Sólo lectura&#160;</td>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkEntityReadOnly">
                        <xsl:if test="Entity/@ReadOnly = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                  </tr>
                  <tr>
                    <td style="text-align:right">Oculto&#160;</td>
                    <td>
                      <input type="checkbox" style="margin-bottom:6px;" id="chkEntityHidden">
                        <xsl:if test="Entity/@Hidden = 'true'">
                          <xsl:attribute name="checked">checked</xsl:attribute>
                        </xsl:if>
                      </input>
                    </td>
                  </tr>
                </table>
              </td>
            </tr>
          </table>
        </td>
      </tr>
      <tr>
        <td align="center">
          <input id="btnDeleteEntity" type="button" value="Eliminar" onclick="deleteEntity()" />
          <input id="btnSaveEntity" type="button" value="Guardar" onclick="saveEntity()" />
          <input id="Button1" type="button" value="Generar" onclick="generateEntity()" />
        </td>
      </tr>
      <tr>
        <td>
          <hr/>
          <strong>Propiedades de la Entidad</strong>
        </td>
      </tr>
      <tr>
        <td>
          <br/>
          <input id="btnAddProperty" type="button" value="Ingresar" onclick="newProperty()" />
          <br/>
          <br/>
        </td>
      </tr>
      <tr>
        <td>
          <table width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="white">
            <tr bgcolor="LightSteelBlue">
              <td width="17">
                <strong>Único</strong>
              </td>
              <td width="17">
                <strong>Requerido</strong>
              </td>
              <td width="17">
                <strong>&#160;</strong>
              </td>
              <td width="17">
                <strong>&#160;</strong>
              </td>
              <td>
                <strong>Propiedad</strong>
              </td>
              <td>
                <strong>Tipo</strong>
              </td>
            </tr>
            <xsl:for-each select="/Entity/Properties/Property">
              <tr>
                <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="bgcolor">Gainsboro</xsl:attribute>
                </xsl:if>
                <td align="center">
                  <xsl:if test="$varKeyProperty = current()/@PropertyName">
                    <img src="../../Images/Key.gif"></img>
                  </xsl:if>
                </td>
                <td align="center">
                  <xsl:if test="current()/@Required = 'true'">
                    <img src="../../Images/required.gif"></img>
                  </xsl:if>
                </td>
                <td align="center">
                  <img src="../../Images/icon-edit.gif" style="cursor:pointer">
                    <xsl:attribute name="onclick">
                      editProperty('<xsl:value-of select="current()/@PropertyName"/>', '<xsl:value-of select="$entityValue"/>')
                    </xsl:attribute>
                  </img>
                </td>
                <td align="center">
                  <img src="../../Images/icon-delete.gif" style="cursor:pointer">
                    <xsl:attribute name="onclick">
                      deleteProperty('<xsl:value-of select="current()/@PropertyName"/>', '<xsl:value-of select="$entityValue"/>')
                    </xsl:attribute>
                  </img>
                </td>
                <td>
                  <xsl:value-of select="current()/@PropertyDisplayName"/>
                </td>
                <td>
                  <xsl:choose>
                    <xsl:when test="current()/@PropertyType = '0'">
                      Cadena de caracteres
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '1'">
                      Cadena larga de caracteres
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '2'">
                      Número
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '3'">
                      Decimal
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '4'">
                      Verdadero/Falso
                    </xsl:when>
                    <!--<xsl:when test="current()/@PropertyType = '5'">
                                 Binary
                              </xsl:when>
                              <xsl:when test="current()/@PropertyType = '6'">
                                 LongBinary
                              </xsl:when>-->
                    <xsl:when test="current()/@PropertyType = '7'">
                      Fecha
                    </xsl:when>
                    <!--<xsl:when test="current()/@PropertyType = '8'">
                                 Date
                              </xsl:when>
                              <xsl:when test="current()/@PropertyType = '9'">
                                 Time
                              </xsl:when>-->
                    <xsl:when test="current()/@PropertyType = '10'">
                      Moneda
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '11'">
                      Contraseña
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '12'">
                      Entidad
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '13'">
                      Correo electrónico
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '14'">
                      Identidad
                    </xsl:when>
                    <xsl:when test="current()/@PropertyType = '15'">
                      Ubicación Geográfica
                    </xsl:when>
                  </xsl:choose>
                </td>
              </tr>
            </xsl:for-each>
          </table>
        </td>
      </tr>

    </table>
  </xsl:template>

</xsl:stylesheet>