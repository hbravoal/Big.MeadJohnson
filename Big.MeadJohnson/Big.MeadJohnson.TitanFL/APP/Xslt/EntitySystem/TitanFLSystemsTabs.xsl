<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
  <xsl:param name="guid"/>
  <xsl:param name="viewName"/>
  <xsl:param name="selectedTab"/>
  <xsl:param name="appRoot"/>
  <xsl:template match="/">
    <table cellpadding="0" cellspacing="0" border="0">
      <tr>
        <xsl:for-each select="//add[@entityTab='true']">
          <xsl:choose>
            <xsl:when test="$guid=''">
              <xsl:if test="current()/@showWhenGuidIsEmpty='true'">
                <td width="130px" align="center" style="color:White; font-weight:bold" height="24px" bgcolor="#25a0da">
                  <xsl:if test="current()/@name = $selectedTab">
                    <xsl:attribute name="bgcolor">gray</xsl:attribute>
                  </xsl:if>
                  <a style="color:White; font-weight:bold">
                    <xsl:attribute name="href">
                      <xsl:value-of select="$appRoot"/><xsl:value-of select="current()/@entityTabUrl"/>?ViewName=<xsl:value-of select="$viewName"/>&#38;EntityGuid=<xsl:value-of select="$guid"/>&#38;ModuleName=<xsl:value-of select="current()/@name"/>&#38;SourceModule=<xsl:value-of select="current()/@sourceModule"/>
                    </xsl:attribute>
                    <xsl:value-of select="current()/@entityTabDescription"/>
                  </a>
                </td>
                <td width="2">&#32;</td>
              </xsl:if>
            </xsl:when>
            <xsl:otherwise>
              <xsl:if test="contains(current()/@views,$viewName) or current()/@views='*'">
                <td width="130px" align="center" style="color:White; font-weight:bold" height="24px" bgcolor="#25a0da">
                  <xsl:if test="current()/@name = $selectedTab">
                    <xsl:attribute name="bgcolor">gray</xsl:attribute>
                  </xsl:if>
                  <a style="color:White; font-weight:bold">
                    <xsl:attribute name="href">
                      <xsl:if test="contains(current()/@entityTabUrl,'?')">
                        <xsl:value-of select="$appRoot"/><xsl:value-of select="current()/@entityTabUrl"/>&#38;ViewName=<xsl:value-of select="$viewName"/>&#38;EntityGuid=<xsl:value-of select="$guid"/>&#38;ModuleName=<xsl:value-of select="current()/@name"/>&#38;SourceModule=<xsl:value-of select="current()/@sourceModule"/>
                      </xsl:if>
                      <xsl:if test="not(contains(current()/@entityTabUrl,'?'))">
                        <xsl:value-of select="$appRoot"/><xsl:value-of select="current()/@entityTabUrl"/>?ViewName=<xsl:value-of select="$viewName"/>&#38;EntityGuid=<xsl:value-of select="$guid"/>&#38;ModuleName=<xsl:value-of select="current()/@name"/>&#38;SourceModule=<xsl:value-of select="current()/@sourceModule"/>
                      </xsl:if>
                    </xsl:attribute>
                    <xsl:value-of select="current()/@entityTabDescription"/>
                  </a>
                </td>
                <td width="2">&#32;</td>
              </xsl:if>
            </xsl:otherwise>
          </xsl:choose>
        </xsl:for-each>
      </tr>
    </table>
  </xsl:template>
</xsl:stylesheet>
