<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
   <xsl:template match="/">

      <hr />
      <input id="btnSavePropertiesNew" type="button" value="Guardar propiedades" onclick="SavePropertiesViewNew(this.form)" />
      <br/>
      <br/>
      <table width ="100%" cellpadding="3" cellspacing="0" border="0" bordercolor="white">
         <xsl:for-each select="//Entity">
            <xsl:variable name="varEntityName" select="@EntityName"></xsl:variable>
            <tr bgcolor="LightSteelBlue">
               <td>
                  <strong>
                     <xsl:value-of select="@DisplayName"/>
                  </strong>
                  (<xsl:value-of select="@EntityName"/>)
               </td>
            </tr>
            <tr>
               <td>
                  <table cellpadding="3" cellspacing="0" border="0" bordercolor="white" width="100%">
                     <tr>
                        <td width="17px">
                           <strong>Seleccionar</strong>
                        </td>
                        <td width="17px">
                           <strong>Requerido</strong>
                        </td>
                        <td>
                           <strong>Propiedad</strong>
                        </td>
                        <td width="50x">
                           <strong>Grupo</strong>
                        </td>
                     </tr>
                     <xsl:for-each select="current()/Property">
                        <tr>
                           <xsl:if test="position() mod 2 = 0">
                              <xsl:attribute name="bgcolor">Gainsboro</xsl:attribute>
                           </xsl:if>
                           <td align="center">
                              <input type="checkbox" style="margin-bottom:6px;" EntityProperty="true" onclick="FillPropertyRequired(this.id, this.form)">
                                 <xsl:attribute name="id">chkSelected_<xsl:value-of select="$varEntityName"/>_<xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                 <xsl:attribute name="EntityName"><xsl:value-of select="$varEntityName"/></xsl:attribute>
                                 <xsl:attribute name="PropertyName"><xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                 <xsl:attribute name="chkRequired">chkRequired_<xsl:value-of select="$varEntityName"/>_<xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                 <xsl:attribute name="ddlGroupProperties">ddlGroupProperties_<xsl:value-of select="$varEntityName"/>_<xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                 <xsl:if test="current()/@SelectedProperty = 'true'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                 </xsl:if>
                              </input>
                           </td>
                           <td align="center">
                              <input type="checkbox" style="margin-bottom:6px;">
                                 <xsl:attribute name="id">chkRequired_<xsl:value-of select="$varEntityName"/>_<xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                 <xsl:if test="current()/@Required = 'True'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                    <xsl:attribute name="disabled">disabled</xsl:attribute>
                                 </xsl:if>
                                 <xsl:if test="current()/@RequiredProperty = 'true'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                 </xsl:if>
                              </input>
                           </td>
                           <td>
                              <strong><xsl:value-of select="current()/@DisplayName"/></strong> (<xsl:value-of select="current()/@PropertyName"/>)
                           </td>
                           <xsl:variable name="varGroupNameProperty" select="current()/@GroupNameProperty"/>
                           <td>
                              <select style="width:200px">
                                 <xsl:attribute name="id">ddlGroupProperties_<xsl:value-of select="$varEntityName"/>_<xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                 <option>Seleccione...</option>
                                 <xsl:for-each select="//Groups/Group">
                                    <option>
                                       <xsl:attribute name="value">
                                          <xsl:value-of select="current()/@GroupName"/>
                                       </xsl:attribute>
                                       <xsl:if test="current()/@GroupName = $varGroupNameProperty">
                                          <xsl:attribute name="selected">selected</xsl:attribute>
                                       </xsl:if>
                                       <xsl:value-of select="current()/@GroupDisplayName"/>
                                    </option>
                                 </xsl:for-each>
                              </select>
                           </td>
                        </tr>
                     </xsl:for-each>
                  </table>
               </td>
            </tr>
         </xsl:for-each>
      </table>
   
</xsl:template>

</xsl:stylesheet> 

