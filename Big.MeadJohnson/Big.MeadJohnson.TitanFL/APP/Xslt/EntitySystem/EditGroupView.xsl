<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

   <xsl:param name="groupValue"/>
   <xsl:template match="/">

      <table width="100%" style="padding-left:10px;padding-top:10px">
         <tr>
            <td style="width: 40%;text-align:right">
               Nombre del Grupo
            </td>
            <td style="width: 60%">
               <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtGroupNameEdit" disabled="disabled">
                  <xsl:attribute name="value">
                     <xsl:value-of select="$groupValue"/>
                  </xsl:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td style="width: 40%;text-align:right">
               <span style="color:Red">*</span>&#160;Nombre a mostrar del Grupo
            </td>
            <td style="width: 60%">
               <input style="width: 90%; margin-bottom: 2px;" type="text" maxlength="128" id="txtGroupDisplayNameEdit">
                  <xsl:attribute name="value">
                     <xsl:value-of select="//Group[@GroupName=$groupValue]/@GroupDisplayName"/>
                  </xsl:attribute>
               </input>               
            </td>
         </tr>
      </table>

   </xsl:template>

</xsl:stylesheet> 

