<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="/">

    <input id="btnAddView" type="button" value="Ingresar" onclick="NewView()" />
    <br/>
    <br/>
    <table width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="white">
      <tr bgcolor="LightSteelBlue">
        <td width="17">
          <strong></strong>
        </td>
        <td width="17">
          <strong></strong>
        </td>
        <td>
          <strong>Rol</strong>
        </td>
        <td>
          <strong>Grupo</strong>
        </td>
      </tr>
      <xsl:for-each select="/Views/View">
        <tr>
          <td align="center">
            <img src="../../Images/icon-edit.gif" style="cursor:pointer">
              <xsl:attribute name="onclick">
                EditView('<xsl:value-of select="current()/@Name"/>')
              </xsl:attribute>
            </img>
          </td>
          <td align="center">
            <img src="../../Images/icon-delete.gif" style="cursor:pointer">
              <xsl:attribute name="onclick">
                DeleteView('<xsl:value-of select="current()/@Name"/>')
              </xsl:attribute>
            </img>
          </td>
          <td>
            <xsl:value-of select="current()/@DisplayName"/>
          </td>
          <td>
            <xsl:value-of select="current()/@RoleType"/>
          </td>
        </tr>
      </xsl:for-each>
    </table>

  </xsl:template>

</xsl:stylesheet>

