<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
   <xsl:template match="/">
      <table cellpadding="0" cellspacing="0" width="100%" border="0">
         <tr>
            <td colspan="3">
               <font style="font-weight:bold; font-size:large">
                  <xsl:value-of select="/Entity/@EntityDisplayName"/>
               </font>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <hr/>
            </td>
         </tr>
         <tr>
            <td>
               <table cellpadding="3" cellspacing="0" width="100%" border="0">
                  <xsl:for-each select="//Properties/Property">
                     <xsl:sort order="ascending" data-type="number" select="@Order"/>
                     <xsl:if test="current()/@PropertyType = '12' and current()/@PropertySourceEntitySelectionType = '0'">
                        <tr>
                           <td width="150px">
                              <xsl:value-of select="current()/@PropertyDisplayName"/>:
                           </td>
                           <td>
                              <xsl:choose>
                                 <xsl:when test="//EntityReferences/EntityReference[@EntityName=current()/@PropertySourceEntityName]/@InstanceCountOutOfRange = 'True'">
                                    <input type="text" readOnly="true">
                                       <xsl:attribute name="id"><xsl:value-of select="current()/@PropertyName"/>_Text</xsl:attribute>
                                    </input>
                                    <input type="hidden" EntityPropertyCriteria="true">
                                       <xsl:attribute name="id"><xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                    </input>
                                    &#160;
                                    <input type="button" id="btnManageOutOfRangeInstances" value="...">
                                       <xsl:attribute name="onclick">GetManageOutOfRangeInstances('<xsl:value-of select="//@System"/>', '<xsl:value-of select="current()/@PropertySourceEntityName"/>', '<xsl:value-of select="current()/@PropertySourceEntityDisplayProperty"/>', <xsl:value-of select="current()/@PropertyName"/>_Text, <xsl:value-of select="current()/@PropertyName"/>)</xsl:attribute>
                                    </input>
                                 </xsl:when>
                                 <xsl:otherwise>
                                    <Select style="width:200px" EntityPropertyCriteria="true">
                                       <xsl:attribute name="Id"><xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                       <option value="-1">-- Todos --</option>
                                       <xsl:for-each select="//EntityReferences/EntityReference[@EntityName=current()/@PropertySourceEntityName]/ReferenceInstanceData">
                                          <option>
                                             <xsl:attribute name="value"><xsl:value-of select="current()/@Id"/></xsl:attribute>
                                             <xsl:value-of select="current()/@Name"/>
                                          </option>
                                       </xsl:for-each>
                                    </Select>
                                 </xsl:otherwise>
                              </xsl:choose>
                           </td>
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
                  <tr>
                     <td>Buscar por</td>
                     <td>
                        <select id="PropertiesList" style="width:200px" EntityPropertyDynamicCriteria="true">
                           <option value="-1">-- Seleccione --</option>
                           <xsl:for-each select="//Properties/Property">
                              <xsl:sort order="ascending" data-type="number" select="@Order"/>
                              <xsl:if test="not(current()/@PropertyType = '12' and current()/@PropertySourceEntitySelectionType = '0')">
                                 <option>
                                    <xsl:attribute name="value"><xsl:value-of select="current()/@PropertyName"/></xsl:attribute>
                                    <xsl:value-of select="current()/@PropertyDisplayName"/>
                                 </option>
                              </xsl:if>
                           </xsl:for-each>
                        </select>
                        <select id="OperatorList">
                           <option id="-1">----</option>
                           <option id="=">=</option>
                           <option id="&#60;&#62;">
                              &#60;&#62;
                           </option>
                           <option id="contenga">Que contenga</option>
                           <option id="no contenga">Que no contenga</option>
                           <option id="&#62;">&#62;</option>
                           <option id="&#62;=">&#62;=</option>
                           <option id="&#60;">&#60;</option>
                           <option id="&#60;=">&#60;=</option>
                        </select>
                        <input id="searchCriteriaText"  type="text"></input>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               &#160;
            </td>
         </tr>
         <tr>
            <td>
               <span id="entityDataFormContainer"></span>
            </td>
         </tr>
      </table>
   </xsl:template>
</xsl:stylesheet>
