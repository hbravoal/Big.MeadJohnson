<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html"/>
   <xsl:param name="sortBy"/>
   <xsl:param name="sortByDataType"/>
   <xsl:param name="sortByOrder"/>
   <xsl:param name="startPosition"/>
   <xsl:param name="endPosition"/>
   <xsl:template match="/">
      <EntityData>
         <xsl:for-each select="EntityData/InstanceData[position() &gt;= $startPosition and position() &lt;= $endPosition]">
            <xsl:copy-of select="."/>
         </xsl:for-each>
      </EntityData>
   </xsl:template>
</xsl:stylesheet>