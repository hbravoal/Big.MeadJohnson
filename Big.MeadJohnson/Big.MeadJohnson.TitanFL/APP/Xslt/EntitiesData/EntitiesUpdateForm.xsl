<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:import href="EntitiesControls.xsl"/>
   <xsl:param name="currentPage"/>
   <xsl:param name="currentSortBy"/>
   <xsl:param name="currentSortOrder"/>
   <xsl:param name="currentSearchCriteria"/>
   <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
   <xsl:template match="/">
      <table width="100%" cellpading="0" cellspacing="0" border="0">
         <tr>
            <td colspan="3">
               <font style="font-weight:bold; font-size:large">
                  <xsl:value-of select="/Entity/@EntityDisplayName"/>
               </font>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <hr/>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               <table width="90%" cellpadding="3" cellspacing="0" border="1" bordercolor="LightSteelBlue">
                  <tr bgcolor="LightSteelBlue">
                     <td colspan="2">
                        <strong>Editar registro</strong>
                     </td>
                  </tr>
                  <xsl:for-each select="/Entity/Properties/Property">
                     <xsl:sort order="ascending" data-type="number" select="@Order"/>
                     <xsl:if test="not(@PropertyType = '14')">
                        <tr height="10px"  valign="middle">
                           <td width="200px" align="right">
                              <strong>
                                 <xsl:if test="current()/@Required = 'true'">
                                    <font style="color:Red">*</font>
                                 </xsl:if>&#160;
                                 <span style="cursor:pointer">
                                    <xsl:attribute name="title"><xsl:value-of select="current()/@ToolTipText"/></xsl:attribute>
                                    <xsl:value-of select="current()/@PropertyDisplayName"/>
                                 </span>
                              </strong>
                           </td>
                           <td  width="475px" valign="top">
                              <xsl:call-template name="TemplateFromControl">
                                 <xsl:with-param name="property" select="."/>
                              </xsl:call-template>
                           </td>
                           <!--td width="20px">&#160;</td-->
                        </tr>
                     </xsl:if>
                  </xsl:for-each>
               </table>
            </td>
         </tr>
         <tr>
            <td colspan="3">
               &#160;
            </td>
         </tr>
         <tr>
            <td colspan="3" align="center">
               <input type="button" value="Guardar" id="btnSave" style="width:100px" >
                  <xsl:attribute name="onclick">
                     SaveEntityData(this.form, '<xsl:value-of select="/Entity/InstanceData/@Guid"/>', '<xsl:value-of select="/Entity/@System"/>', '<xsl:value-of select="/Entity/@EntityName"/>', '<xsl:value-of select="$currentPage"/>', '<xsl:value-of select="$currentSortBy"/>', '<xsl:value-of select="$currentSortOrder"/>', '<xsl:value-of select="$currentSearchCriteria"/>')
                  </xsl:attribute>
                  <xsl:if test="/Entity/@ReadOnly = 'true'">
                     <xsl:attribute name="disabled">disabled</xsl:attribute>
                  </xsl:if>
               </input>
               &#160;
               <input type="button" value="Cancelar" id="btnCancel" style="width:100px">
                  <xsl:attribute name="onclick">
                     GetSystemEntityDataSearchSection('<xsl:value-of select="/Entity/@System"/>', '<xsl:value-of select="/Entity/@EntityName"/>', '<xsl:value-of select="$currentPage"/>', '<xsl:value-of select="$currentSortBy"/>', '<xsl:value-of select="$currentSortOrder"/>', '<xsl:value-of select="$currentSearchCriteria"/>')
                  </xsl:attribute>
               </input>
            </td>
         </tr>
         <!--tr>
            <td colspan="3">
               <hr/>
            </td>
         </tr>
         <tr>
            <td colspan="3" align="right">
               <a href="#">
                  D e s c a r g a r
               </a>
            </td>
         </tr-->
      </table>
      <div></div>
   </xsl:template>
</xsl:stylesheet>