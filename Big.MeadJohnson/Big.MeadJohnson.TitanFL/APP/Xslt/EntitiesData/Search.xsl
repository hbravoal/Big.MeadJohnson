<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
  <xsl:template match="/">
    <table cellpadding="3" cellspacing="0" width="100%" border="0">
      <xsl:for-each select="//Field">
        <tr>
          <td align="right" width="150px">
            <xsl:value-of select="current()/@PropertyDisplayName"/>:
          </td>
          <td align="left">
            <xsl:choose>
              <!--Si es un listado y esta por fuera del rango-->
              <xsl:when test="current()/@Type = 'List' and current()/@InstanceCountOutOfRange = 'True'">
                <input type="text" readOnly="true" value="">
                  <xsl:attribute name="id">
                    _Property<xsl:value-of select="current()/@PropertyName"/>_Text
                  </xsl:attribute>
                  <xsl:attribute name="TypeListOutOfRange">True</xsl:attribute>
                </input>
                <input type="hidden" value="" runat="server">
                  <xsl:attribute name="id">
                    <xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:attribute name="name">
                    <xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                </input>
                <input type="button" id="btnManageOutOfRangeInstances" value="...">
                  <xsl:attribute name="onclick">
                    GetManageOutOfRangeInstances('<xsl:value-of select="//@System"/>', '<xsl:value-of select="current()/@PropertySourceEntityName"/>', '<xsl:value-of select="current()/@PropertySourceEntityDisplayProperty"/>', _Property<xsl:value-of select="current()/@PropertyName"/>_Text, <xsl:value-of select="current()/@PropertyName"/>);
                  </xsl:attribute>
                </input>
                <input type="hidden" runat="server" value="1">
                  <xsl:attribute name="id">
                    hdf<xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:attribute name="name">
                    hdf<xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                </input>
              </xsl:when>
              <!--Si es un listado pero esta dentro del rango-->
              <xsl:when test="current()/@Type = 'List' and current()/@InstanceCountOutOfRange = 'False'">
                <select runat="server">
                  <xsl:attribute name="id">
                    <xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:attribute name="name">
                    <xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:for-each select="//Options[@Property=current()/@PropertyName]/Option">
                    <option>
                      <xsl:attribute name="value">
                        <xsl:value-of select="current()/@Id"/>
                      </xsl:attribute>
                      <xsl:if test="current()/@Selected = 'true'">
                        <xsl:attribute name="selected">true</xsl:attribute>
                      </xsl:if>
                      <xsl:value-of select="current()/@Name"/>
                    </option>
                  </xsl:for-each>
                </select>
                <input type="hidden" runat="server" value="1">
                  <xsl:attribute name="id">
                    hdf<xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:attribute name="name">
                    hdf<xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                </input>
              </xsl:when>
              <!--Por defecto es un texto-->
              <xsl:otherwise>
                <input type="text" runat="server">
                  <xsl:attribute name="maxlength">
                    <xsl:value-of select="current()/@Length"/>
                  </xsl:attribute>
                  <xsl:attribute name="id">
                    <xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:attribute name="name">
                    <xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:attribute name="value">
                    <xsl:value-of select="current()/@Value"/>
                  </xsl:attribute>
                </input>
                <input type="hidden" runat="server" value="0">
                  <xsl:attribute name="id">
                    hdf<xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                  <xsl:attribute name="name">
                    hdf<xsl:value-of select="current()/@PropertyName"/>
                  </xsl:attribute>
                </input>
              </xsl:otherwise>
            </xsl:choose>
          </td>
        </tr>
      </xsl:for-each>
    </table>
  </xsl:template>
</xsl:stylesheet>
