<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
   <xsl:template match="/">
      <table cellpadding="3" cellspacing="0" width="100%" border="0">
         <tr bgcolor="LightSteelBlue">
            <td width="17"></td>
            <td width="17"></td>
            <td>
               <strong>Meta</strong>
            </td>
            <td>
               <strong>Descripción</strong></td>
            <td>
               <strong>Meta esperada</strong></td>
            <td>
               <strong>Peso de la meta</strong></td>
            <td>
               <strong>Meta real</strong></td>
            <td>
               <strong>Cumplimiento de la meta</strong></td>
         </tr>
         <xsl:for-each select="/CampaignGoals/CampaignGoal">
            <tr>
               <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="bgcolor">Gainsboro</xsl:attribute>
               </xsl:if>
               <td align="center">
                  <img src="../../Images/icon-edit.gif" style="cursor:pointer">
                     <xsl:attribute name="onclick">editGoal('<xsl:value-of select="current()/@Id"/>')</xsl:attribute>
                  </img>
               </td>
               <td align="center">
                  <img src="../../Images/icon-delete.gif" style="cursor:pointer">
                     <xsl:attribute name="onclick">deleteGoal('<xsl:value-of select="current()/@Id"/>')</xsl:attribute>
                  </img>
               </td>
               <td>
                  <xsl:value-of select="current()/@Name"/>
               </td>
               <td>
                  <xsl:value-of select="current()/@Description"/>
               </td>
               <td>
                  <xsl:value-of select="current()/@HopeGoal"/>
               </td>
               <td>
                  <xsl:value-of select="current()/@WidthGoal"/>
               </td>
               <td>
                  <xsl:value-of select="current()/@RealGoal"/>
               </td>
               <td>
                  <xsl:value-of select="current()/@FulfillmentGoal"/>
               </td>
            </tr>
         </xsl:for-each>
      </table>
   </xsl:template>
</xsl:stylesheet>

