<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <xsl:variable name="varIdTarget" select="ActivitiesResponse/@IdTarget" />
      <table cellpadding="3" cellspacing="0" border="0" bordercolor="white">
         <xsl:for-each select="/ActivitiesResponse/ActivityResponse">
            <input type="hidden">
               <xsl:attribute name="Target"><xsl:value-of select="$varIdTarget"/></xsl:attribute>
               <!--<xsl:attribute name="id">hidden_<xsl:value-of select="current()/@IdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>-->
               <xsl:attribute name="Response">Response_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
               <xsl:attribute name="ActivityResponseType"><xsl:value-of select="current()/@Id"/></xsl:attribute>
            </input>
            <tr>
               <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="bgcolor">LightSteelBlue</xsl:attribute>
               </xsl:if>
               <td>
                  <xsl:value-of select="current()/@Name"/>
               </td>
               <td>
                  <xsl:value-of select="current()/@Description"/>
               </td>
               <td>
                  <table>
                     <tr>
                        <td>
                           <input type="radio" value="True">
                              <xsl:attribute name="name">Response_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
                              <xsl:if test="current()/@Exists = 'True'">
                                 <xsl:if test="current()/@Response = 'True'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                 </xsl:if>
                              </xsl:if>
                              Si
                           </input>
                        </td>
                        <td>
                           <input type="radio" value="False">
                              <xsl:attribute name="name">Response_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
                              <xsl:if test="current()/@Exists = 'True'">
                                 <xsl:if test="current()/@Response = 'False'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                 </xsl:if>
                              </xsl:if>
                              No
                           </input>
                        </td>
                     </tr>
                  </table>
               </td>
            </tr>
         </xsl:for-each>
      </table>
   </xsl:template>

</xsl:stylesheet>

