<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <xsl:variable name="varIdTarget" select="ActivitiesResponse/@IdTarget" />
      <table width="100%" cellpadding="3" cellspacing="0" border="0" bordercolor="white">
         <xsl:for-each select="/ActivitiesResponse/ActivityResponse">
            <input type="hidden">
               <xsl:attribute name="Target"><xsl:value-of select="$varIdTarget"/></xsl:attribute>
               <!--<xsl:attribute name="id">hidden_<xsl:value-of select="current()/@IdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>-->
               <xsl:attribute name="Response">Response_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
					<xsl:attribute name="IdARState">ddl_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
					<xsl:attribute name="DescriptionARState">txt_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
               <xsl:attribute name="ActivityResponseType"><xsl:value-of select="current()/@Id"/></xsl:attribute>
					<xsl:attribute name="NameQuestion"><xsl:value-of select="current()/@Name"/></xsl:attribute>
            </input>
				<tr style="background:#5D7B9D;color:White">
					<td style="width:30%">Pregunta</td>
					<td style="width:15%">Respuesta</td>
					<td style="width:20%">Estado</td>
					<td style="width:35%">Descripción</td>
				</tr>
            <tr>
               <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="bgcolor">LightSteelBlue</xsl:attribute>
               </xsl:if>
               <td>
                  <xsl:value-of select="current()/@Name"/>
               </td>               
               <td>
                  <table cellpadding="0" cellspacing="0" width="100%">
                     <tr>
                        <td>
                           <input type="radio" value="True">
                              <xsl:attribute name="name">Response_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
                              <xsl:if test="current()/@Exists = 'True'">
                                 <xsl:if test="current()/@Response = 'True'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                 </xsl:if>
                              </xsl:if>
                              Si
                           </input>
                        </td>
                        <td>
                           <input type="radio" value="False">
                              <xsl:attribute name="name">Response_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
                              <xsl:if test="current()/@Exists = 'True'">
                                 <xsl:if test="current()/@Response = 'False'">
                                    <xsl:attribute name="checked">checked</xsl:attribute>
                                 </xsl:if>
                              </xsl:if>
                              No
                           </input>
                        </td>
                     </tr>
                  </table>
               </td>
					<td>
						<select style="width:98%">
							<xsl:attribute name="id">ddl_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
							<xsl:variable name="propertyValue">
								<xsl:value-of select="current()/@IdActivityResponseState"/>
							</xsl:variable>
							<option value="">-- Seleccione --</option>
							<xsl:for-each select="//ActivitiesResponseState/ActivityResponseState">
								<option>
									<xsl:attribute name="value">
										<xsl:value-of select="current()/@Id"/>
									</xsl:attribute>
									<xsl:if test="current()/@Id = $propertyValue">
										<xsl:attribute name="selected">true</xsl:attribute>
									</xsl:if>
									<xsl:value-of select="current()/@Name"/>
								</option>
							</xsl:for-each>
						</select>
					</td>
					<td>
						<input type="text" maxlength="255" style="width:98%">
							<xsl:attribute name="id">txt_<xsl:value-of select="$varIdTarget"/>_<xsl:value-of select="current()/@Id"/></xsl:attribute>
							<xsl:attribute name="value"><xsl:value-of select="current()/@DescriptionARS"/></xsl:attribute>
						</input>
					</td>
            </tr>
         </xsl:for-each>
      </table>
   </xsl:template>

</xsl:stylesheet>

