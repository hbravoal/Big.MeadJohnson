<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:output method="html" version="4.0" encoding="iso-8859-1" indent="yes"/>
   <xsl:template match="/">
      <table width="100%" style="padding-left:10px; padding-top:10px">
         <tr>
            <td style="width: 45%;text-align:right">
               <span style="color:Red">*</span>&#160;Meta
            </td>
            <td style="width: 55%">
               <input style="width: 90%; margin-bottom: 2px;" type="text" id="txtNameGoalEdit" maxlength="50" value="">
                  <xsl:attribute name="value">
                     <xsl:value-of select="CampaignGoals/CampaignGoal/@Name"/>
                  </xsl:attribute>
               </input>
            </td>
         </tr>
         <tr>
            <td style="width: 45%;text-align:right">
               <span style="color:Red">*</span>&#160;Descripción
            </td>
            <td style="width: 55%">
               <textarea rows="2" cols="20" style="height: 62px; width: 90%; margin-bottom: 2px;" id="txtDescriptionGoalEdit">
                  <xsl:value-of select="CampaignGoals/CampaignGoal/@Description"/>
               </textarea>
            </td>
         </tr>
         <tr>
            <td style="width: 45%;text-align:right">
               <span style="color:Red">*</span>&#160;Meta esperada
            </td>
            <td style="width: 55%">
               <input style="width: 70%; margin-bottom: 2px;text-align: right" type="text" id="txtHopedGoalEdit" maxlength="3" value="" onchange="CalculateGoalFulfillment('Edit')">
                  <xsl:attribute name="value">
                     <xsl:value-of select="CampaignGoals/CampaignGoal/@HopeGoal"/>
                  </xsl:attribute>
               </input>&#160;%
            </td>
         </tr>
         <tr>
            <td style="width: 45%;text-align:right">
               <span style="color:Red">*</span>&#160;Peso de la meta
            </td>
            <td style="width: 55%">
               <input style="width: 70%; margin-bottom: 2px;text-align: right" type="text" id="txtWidthGoalEdit" maxlength="3" value="">
                  <xsl:attribute name="value">
                     <xsl:value-of select="CampaignGoals/CampaignGoal/@WidthGoal"/>
                  </xsl:attribute>
               </input>&#160;%
            </td>
         </tr>
         <tr>
            <td style="width: 45%;text-align:right">
               Meta real
            </td>
            <td style="width: 55%">
               <input style="width: 70%; margin-bottom: 2px;text-align: right" type="text" id="txtRealGoalEdit" maxlength="3" value="" onchange="CalculateGoalFulfillment('Edit')">
                  <xsl:attribute name="value">
                     <xsl:value-of select="CampaignGoals/CampaignGoal/@RealGoal"/>
                  </xsl:attribute>
               </input>&#160;%
            </td>
         </tr>
         <tr>
            <td style="width: 45%;text-align:right">
               Cumplimiento de la meta
            </td>
            <td style="width: 55%">
               <input style="width: 70%; margin-bottom: 2px;text-align: right" type="text" id="txtFulfillmentGoalEdit" maxlength="8" value="" readonly="readonly">
                  <xsl:attribute name="value">
                     <xsl:value-of select="CampaignGoals/CampaignGoal/@FulfillmentGoal"/>
                  </xsl:attribute>
               </input>&#160;%
            </td>
         </tr>
      </table>
   </xsl:template>
</xsl:stylesheet>

