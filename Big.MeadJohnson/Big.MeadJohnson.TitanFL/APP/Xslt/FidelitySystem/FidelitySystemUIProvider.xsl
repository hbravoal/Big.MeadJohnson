<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
   <xsl:template match="/">
      <table  width="100%" cellpadding="3" cellspacing="0" border="1" bordercolor="white" >
         <tr>
            <td colspan="5">
               <strong>Productos</strong>
            </td>
         </tr>
         <tr>
            <td colspan="5">
               <input type="button" value="Ingresar" style="width:100px" onclick="newProduct()"/>
            </td>
         </tr>
         <tr bgcolor="LightSteelBlue">
            <td></td>
            <td></td>
            <td>
               <strong>Imagen</strong>
            </td>
            <td>
               <strong>Descripcion</strong>
            </td>
            <td>
               <strong>Valor</strong>
            </td>
         </tr>

         <xsl:for-each select="EntityData/InstanceData">

            <tr>
               <xsl:if test="position() mod 2 = 0">
                  <xsl:attribute name="bgcolor">Gainsboro</xsl:attribute>
               </xsl:if>
               <td width="20px">
                  <xsl:attribute name="style">text-align: center</xsl:attribute>
                  <img src="../../Images/icon-edit.gif">
                     <xsl:attribute name="onclick">
                        EditProduct('<xsl:value-of select="current()/@Guid"/>')
                     </xsl:attribute>
                     <xsl:attribute name="style">cursor: hand</xsl:attribute>
                  </img>
               </td>
               <td width="20px">
                  <xsl:attribute name="style">text-align: center</xsl:attribute>
                  <img src="../../Images/icon-delete.gif">
                     <xsl:attribute name="onclick">
                        DeleteProduct('<xsl:value-of select="current()/@Guid"/>')
                     </xsl:attribute>
                     <xsl:attribute name="style">cursor: hand</xsl:attribute>
                  </img>
               </td>
               <td>
                  <img>
                     <xsl:attribute name="src">
                        <xsl:value-of select="current()/@PropertyImage"/>
                     </xsl:attribute>
                  </img>
               </td>
               <td>
                  <xsl:value-of select="current()/@PropertyDescription" disable-output-escaping ="yes"/>
               </td>
               <td>
                  <strong>
                     <xsl:value-of select="current()/@PropertyValue"/>
                  </strong>
               </td>
            </tr>
         </xsl:for-each>
      </table>
   </xsl:template>
</xsl:stylesheet>

