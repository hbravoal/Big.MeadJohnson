<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

 <xsl:template match="/">

    <table width="100%" cellpadding="0" cellspacing="0"   border="0">
      <!-- Titulo -->
      <tr bgcolor="LightSteelBlue" style="text-align:center;">
        <td colspan="2">
          <b>
           
          </b>
        </td>
      </tr>
      <!-- Atributos -->
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Guid:
        </td>
        
          <td style="width:75%;">
            <input style="width:80%; margin-bottom:2px;" type="text" disabled="disabled" id="txtPropertyGuid">
              <xsl:attribute name="value">
                <xsl:value-of select="/EntityData/InstanceData/@Guid"/>
              </xsl:attribute>
            </input>
          </td>
        </tr>
      <tr>  
      <td style="vertical-align: text-top; width:25%;">
          Id Product:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyProductId">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyProductId"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Product Name:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyNombre">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyNombre" disable-output-escaping ="yes"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Description:
        </td>
        <td style="width:75%;">
         
           <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyDescription">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyDescription" disable-output-escaping ="yes"/>
            </xsl:attribute>
           </input>
          
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Product Value:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyValue">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyValue"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Url Product Detail Image:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyDetailImage">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyDetailImage"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Url Product Image:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyImage">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyImage"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Url Product Icon Image:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyUrlIconImage">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyUrlIconImage"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Product Enable:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyEnable">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyEnable"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Creation Date:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyCreationDate">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyCreationDate"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      <tr>
        <td style="vertical-align: text-top; width:25%;">
          Product Inventory Quantity:
        </td>
        <td style="width:75%;">
          <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyInventaryQuantity">
            <xsl:attribute name="value">
              <xsl:value-of select="/EntityData/InstanceData/@PropertyInventaryQuantity"/>
            </xsl:attribute>
          </input>
        </td>
      </tr>
      
    </table>
</xsl:template>

</xsl:stylesheet> 

