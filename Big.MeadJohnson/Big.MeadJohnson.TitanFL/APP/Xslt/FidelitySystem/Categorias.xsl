<?xml version="1.0" encoding="utf-8"?>

<xsl:stylesheet version="1.0"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <table width="100%" border="0">
    <!-- Titulo -->
    <tr bgcolor="LightSteelBlue" style="text-align:center;">
      <td colspan="2">
        <b>

        </b>
      </td>
    </tr>
    <!-- Atributos -->
    <tr>
      <!--<td style="vertical-align: text-top; width:25%;">
        Guid:
      </td>

      <td style="width:75%;">
        <input style="width:80%; margin-bottom:2px;" type="text" disabled="disabled" id="txtPropertyGuid">
          <xsl:attribute name="value">
            <xsl:value-of select="/EntityData/InstanceData/@Guid"/>
          </xsl:attribute>
        </input>
      </td>-->
    </tr>
    <tr>
      <!--<td style="vertical-align: text-top; width:25%;">
        Id Category:
      </td>
      <td style="width:75%;">
        <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyCategoryId">
          <xsl:attribute name="value">
            <xsl:value-of select="/EntityData/InstanceData/@PropertyCategoryId"/>
          </xsl:attribute>
        </input>
      </td>-->
    </tr>
    <tr>
      <td style="vertical-align: text-top; width:25%;">
        Category Name:
      </td>
      <td style="width:75%;">
        <input style="width:80%; margin-bottom:2px;" type="text" id="txtPropertyCategoryName">
          <xsl:attribute name="value">
            <xsl:value-of select="/EntityData/InstanceData/@PropertyCategoryName" disable-output-escaping ="yes"/>
          </xsl:attribute>
        </input>
      </td>
    </tr>
    <tr>
      <td style="vertical-align: text-top; width:25%;">
        Guid Category Parent:
      </td>
      <td style="width:75%;">

        <input style="width:80%; margin-bottom:2px;" type="text" disabled="disabled" id="txtPropertyCategoryParent">
          <xsl:attribute name="value">
            <xsl:value-of select="/EntityData/InstanceData/@PropertyCategoryParent" disable-output-escaping ="yes"/>
          </xsl:attribute>
        </input>

      </td>
    </tr>
    

  </table>
</xsl:template>

</xsl:stylesheet> 

