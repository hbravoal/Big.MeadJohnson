﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="List.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks2.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <telerik:RadAjaxLoadingPanel runat="server" ID="LoadingPanel1">
        </telerik:RadAjaxLoadingPanel>
        <telerik:RadAjaxManager runat="server" ID="RadAjaxManager1">
            <ClientEvents OnResponseEnd="onResponseEnd" />
            <AjaxSettings>
                <telerik:AjaxSetting AjaxControlID="radTabStrip">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radTabStrip"></telerik:AjaxUpdatedControl>
                        <telerik:AjaxUpdatedControl ControlID="radMultiPage" LoadingPanelID="LoadingPanel1">
                        </telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
                <telerik:AjaxSetting AjaxControlID="radMultiPage">
                    <UpdatedControls>
                        <telerik:AjaxUpdatedControl ControlID="radMultiPage" LoadingPanelID="LoadingPanel1">
                        </telerik:AjaxUpdatedControl>
                    </UpdatedControls>
                </telerik:AjaxSetting>
            </AjaxSettings>
        </telerik:RadAjaxManager>
        <script type="text/javascript">

            function onTabSelecting(sender, args) {
                if (args.get_tab().get_pageViewID()) {
                    args.get_tab().set_postBack(false);
                }


            }

            function onResponseEnd(sender, args) {
                init();
                errorMessage = "";
            }
             
        </script>
        <telerik:RadTabStrip ID="radTabStrip" runat="server" Width="100%" MultiPageID="radMultiPage"
            OnClientTabSelecting="onTabSelecting" OnTabClick="RadTabStrip1_TabClick" SelectedIndex="0">
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" runat="server" Width="100%" SelectedIndex="0"
            OnPageViewCreated="RadMultiPage1_PageViewCreated" CssClass="pageView">
        </telerik:RadMultiPage>
    </div>
</asp:Content>
