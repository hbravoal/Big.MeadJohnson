﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Detail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks2.Detail" %>

<%@ Register src="DetailControls/ConfigureTask.ascx" tagname="ConfigureTask" tagprefix="uc1" %>
<%@ Register src="DetailControls/TestSend.ascx" tagname="TestSend" tagprefix="uc2" %>
<%@ Register src="DetailControls/EffectivenessBrief.ascx" tagname="EffectivenessBrief" tagprefix="uc3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/animation/animation-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/container/container-min.js"></script>
    <script type="text/javascript" src="../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <telerik:RadTabStrip ID="radTabStrip" runat="server" Width="100%" MultiPageID="radMultiPage"
            SelectedIndex="0">
            <Tabs>
                <telerik:RadTab Text="Configurar tarea de envío" Value="ConfigureTask">
                </telerik:RadTab>
                <telerik:RadTab Text="Enviar correo de prueba" Value="TestSend">
                </telerik:RadTab>
                <telerik:RadTab Text="Resumen de efectividad" Value="EffectivenessBrief">
                </telerik:RadTab>
            </Tabs>
        </telerik:RadTabStrip>
        <telerik:RadMultiPage ID="radMultiPage" runat="server" Width="100%" SelectedIndex="0"
            CssClass="pageView">
            <telerik:RadPageView ID="ConfigureTask" runat="server">
                <uc1:ConfigureTask ID="ConfigureTask1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="TestSend" runat="server">
                <uc2:TestSend ID="TestSend1" runat="server" />
            </telerik:RadPageView>
            <telerik:RadPageView ID="EffectivenessBrief" runat="server">
                <uc3:EffectivenessBrief ID="EffectivenessBrief1" runat="server" />
            </telerik:RadPageView>
        </telerik:RadMultiPage>
    </div>
</asp:Content>
