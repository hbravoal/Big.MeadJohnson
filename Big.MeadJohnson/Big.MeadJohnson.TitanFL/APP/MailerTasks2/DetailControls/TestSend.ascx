﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="TestSend.ascx.cs" Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks2.DetailControls.TestSend" %>
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel"
    ClientEvents-OnResponseEnd="onResponseEnd">
    <script type="text/javascript">

        function onResponseEnd(sender, args) {
            init();
        }
             
    </script>
    <div style="padding-top: 10px">
        <table cellpadding="5" cellspacing="0" border="0" width="100%" bordercolor="lightgrey">
            <tr>
                <td>
                    <strong><span style="font-size: 14pt">Destinatarios de Correo</span></strong>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    Ingrese los destinatarios del correo de prueba, uno por linea.
                </td>
            </tr>
            <tr>
                <td>
                    <asp:TextBox ID="txtTestEmails" runat="server" TextMode="MultiLine" Width="500px"
                        Height="250px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="btnSendTest" runat="server" Text="Enviar Prueba" OnClick="btnSendTest_Click" />
                </td>
            </tr>
        </table>
    </div>
</telerik:RadAjaxPanel>
