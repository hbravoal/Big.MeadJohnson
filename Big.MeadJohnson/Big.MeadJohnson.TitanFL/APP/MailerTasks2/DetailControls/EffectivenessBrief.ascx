﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="EffectivenessBrief.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks2.DetailControls.EffectivenessBrief" %>
<telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
</telerik:RadAjaxLoadingPanel>
<telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel">
    <div style="padding-top: 10px">
        <table border="0" width="600">
            <tr>
                <td colspan="4">
                    <strong><span style="font-size: 14pt">Análisis de Envío</span></strong>
                </td>
            </tr>
            <tr>
                <td style="background-color: lightsteelblue; height: 30px;">
                    RESUMEN
                </td>
                <td style="background-color: lightsteelblue; height: 30px;">
                    CORREOS
                </td>
                <td style="background-color: lightsteelblue; height: 30px; text-align: right">
                    %
                </td>
                <td style="height: 30px; background-color: lightsteelblue">
                    DESCRIPCION
                </td>
            </tr>
            <tr>
                <td>
                    Pendientes por envío:
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblPendientes" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblPendientesPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td>
                    Correos en cola de envío
                </td>
            </tr>
            <tr>
                <td style="background-color: gainsboro; height: 15px;">
                    Enviados:
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblEnviados" runat="server"></asp:Label>
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblEnviadosPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td style="height: 15px; background-color: gainsboro">
                    Correos enviados y entregados
                </td>
            </tr>
            <tr>
                <td>
                    Errados:
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblErrados" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblErradosPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td>
                    Correos errados
                </td>
            </tr>
            <tr>
                <td style="background-color: gainsboro; height: 15px;">
                    Enviando:
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblEnviando" runat="server"></asp:Label>
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblEnviandoPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td style="background-color: gainsboro; height: 15px;">
                    Correos que estan siendo enviados
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
                <td style="text-align: right">
                    Total:
                    <asp:Label ID="lblTotalUsuarios" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    Total:
                    <asp:Label ID="lblTotalPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td>
                    Correos preparados para ser enviados
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <br />
                </td>
            </tr>
        </table>
        <br />
        <table border="0" width="600">
            <tr>
                <td colspan="4">
                    <strong><span style="font-size: 14pt">Análisis de Efectividad</span></strong>
                </td>
            </tr>
            <tr>
                <td style="background-color: lightsteelblue; height: 30px;">
                    RESUMEN
                </td>
                <td style="background-color: lightsteelblue; height: 30px;">
                    CORREOS
                </td>
                <td style="background-color: lightsteelblue; height: 30px; text-align: right">
                    %
                </td>
                <td style="height: 30px; background-color: lightsteelblue">
                    DESCRIPCION
                </td>
            </tr>
            <tr>
                <td>
                    Pendientes por leer:
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblPendientesLeer" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblPendientesLeerPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td>
                    Correos entregados y pendientes por ser leidos
                </td>
            </tr>
            <tr>
                <td style="background-color: gainsboro; height: 15px;">
                    Leídos:
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblLeidos" runat="server"></asp:Label>
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblLeidosPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td style="background-color: gainsboro; height: 15px;">
                    Efectividad: Correos entregados y leidos
                </td>
            </tr>
            <tr>
                <td style="background-color: gainsboro; height: 15px;">
                    Clic:
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblClic" runat="server"></asp:Label>
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    <asp:Label ID="lblClicPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td style="background-color: gainsboro; height: 15px;">
                    Efectividad: Correos con enlace abierto
                </td>
            </tr>
            <tr>
                <td>
                    Rebotados:
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblRebotados" runat="server"></asp:Label>
                </td>
                <td style="text-align: right">
                    <asp:Label ID="lblRebotadosPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td>
                    Correos entregados y rebotados
                </td>
            </tr>
            <tr>
                <td style="background-color: gainsboro; height: 15px;">
                    &nbsp;
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    Total:
                    <asp:Label ID="lblTotalEfec" runat="server"></asp:Label>
                </td>
                <td style="background-color: gainsboro; height: 15px; text-align: right">
                    Total:
                    <asp:Label ID="lblTotalEfecPor" runat="server"></asp:Label>&nbsp;%
                </td>
                <td style="background-color: gainsboro; height: 15px;">
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <br />
                    <asp:Button ID="btnUpdateEffectiveness" runat="server" Text="Actualizar" OnClick="btnUpdateEffectiveness_Click" />
                    <asp:Button ID="btnReturn" runat="server" Text="Volver" OnClientClick="history.back();return false;" />
                </td>
            </tr>
        </table>
    </div>
</telerik:RadAjaxPanel>
