﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ConfigureTask.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks2.DetailControls.ConfigureTask" %>
<%@ Register Src="../../MailerTasks/Controls/DateTimeControl.ascx" TagName="DateTimeControl"
    TagPrefix="uc1" %>
<script type="text/javascript">
    //-- inserting the autoformated fields for simplified use

    var savedSelection;

    function insertAtCursor(fName, fValue) {
        //-- calling the function
        //-- insertAtCursor(document.formName.fieldName, 'this value');
        var myField = document.getElementById(fName);
        var valueField = document.getElementById(fValue);
        sValue = '[' + valueField.options[valueField.selectedIndex].value + ']';
        //-- IE support
        if (document.selection) {
            myField.focus();
            sel = document.selection.createRange();
            sel.text = sValue;
        }
        //-- MOZILLA/NETSCAPE support
        else if (myField.selectionStart || myField.selectionStart == '0') {
            var startPos = myField.selectionStart;
            savedSelection = startPos;
            var endPos = myField.selectionEnd;
            myField.value = myField.value.substring(0, startPos)
	+ sValue
	+ myField.value.substring(endPos, myField.value.length);
        } else {
            myField.value += sValue;
            myField.focus();
            myField.selectionStart = savedSelection;
        }
    }

    function insertLinkSurvey(fName) {
        var myField = document.getElementById(fName);
        sValue = '[&VinculoEncuesta&]';

        if (document.selection) {
            myField.focus();
            sel = document.selection.createRange();
            sel.text = sValue;
        }

        else if (myField.selectionStart || myField.selectionStart == '0') {
            var startPos = myField.selectionStart;
            savedSelection = startPos;
            var endPos = myField.selectionEnd;
            myField.value = myField.value.substring(0, startPos)
	+ sValue
	+ myField.value.substring(endPos, myField.value.length);
        } else {
            myField.value += sValue;
            myField.focus();
            myField.selectionStart = savedSelection;
        }
    }
</script>
<style type="text/css">
    .RadUpload input.ruFakeInput
    {
        display: none;
    }
    
    .RadUpload input.ruBrowse
    {
        width: 115px;
    }
    
    .RadUpload span.ruFileWrap input.ruButtonHover
    {
        background-position: 100% -46px;
    }
    
    .RadUpload input.ruButton
    {
        background-position: 0 -46px;
    }
</style>
<telerik:RadAjaxManager ID="RadAjaxManager2" runat="server">
    <AjaxSettings>
        <telerik:AjaxSetting AjaxControlID="RadAjaxManager2">
            <UpdatedControls>
                <telerik:AjaxUpdatedControl ControlID="hfTemplateUrl" />
            </UpdatedControls>
        </telerik:AjaxSetting>
    </AjaxSettings>
</telerik:RadAjaxManager>
<script type="text/javascript">
    function fileUploaded(sender, args) {
        $find('ctl00_maincontent_ConfigureTask1_RadAjaxManager2').ajaxRequest();
        setTimeout(function () {
            sender.deleteFileInputAt(0);
        }, 10);

        setTimeout(function () {
            document.getElementById("templatePreview").src = document.getElementById("hfTemplateUrl").value;
            document.getElementById("trTemplatePreview").style["display"] = "block";
        }, 500);
    }

    function validationFailed(sender, args) {
        alert("Debe seleccionar un archivo html con peso menor o igual a 2MB");
        sender.deleteFileInputAt(0);
    }
</script>
<div style="padding-top: 10px">
    <table cellpadding="5" cellspacing="0" border="0" width="100%" bordercolor="lightgrey">
        <tr>
            <td>
                <table border="0">
                    <tr>
                        <td style="height: 205x" colspan="3">
                            <span style="font-size: 14pt"><b>Estado del Envío: </b>
                                <asp:Label ID="lblEstado" runat="server"></asp:Label></span>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="pnlAsusto" runat="server" Visible="true">
                                <table cellpadding="4px">
                                    <tr>
                                        <td>
                                            Asunto:
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtAsunto" runat="server" MaxLength="180" Width="500px"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvSubject" runat="server" ErrorMessage="El campo asunto es requerido"
                                                ControlToValidate="txtAsunto" ValidationGroup="Mailer" Display="None"></asp:RequiredFieldValidator>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="pnlMensaje" runat="server" Visible="false">
                                <table cellpadding="4px" border="0">
                                    <tr>
                                        <td valign="top" align="left" style="text-align: left">
                                            Mensaje:<br />
                                            <asp:TextBox ID="txtMensaje" runat="server" TextMode="MultiLine" Width="350px" Height="200px"
                                                Wrap="false" ToolTip="Es el mensaje que se desea enviar"></asp:TextBox><br />
                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtMensaje"
                                                Text="Máximo 4000 caracteres" ErrorMessage="M&aacte;ximo 4000 caracteres" ValidationExpression="^[\s\S]{0,4000}$"
                                                runat="server" /><br />
                                            <asp:Label ID="lblCDescripcion" runat="server" CssClass="text" Text="Máximo 4000 caracteres"></asp:Label>&nbsp;<asp:Button
                                                ID="btnEditar" OnClick="btnEditar_Click" runat="server" Width="80px" Visible="false"
                                                Text="Editar"></asp:Button>
                                        </td>
                                        <td valign="middle" align="center" style="text-align: center; width: 140px">
                                            <input style="width: 130px" id="btnAdicionarTag" onclick="insertAtCursor('<%=txtMensaje.ClientID %>', '<%=ddlPropiedadesEntidad.ClientID %>');"
                                                type="button" value="Insertar Propiedad" />
                                        </td>
                                        <td valign="top" align="left" style="text-align: left">
                                            Tags con la información del contacto:<br />
                                            <asp:ListBox ID="ddlPropiedadesEntidad" Width="350px" Height="200px" runat="server"
                                                ToolTip="Es la lista de parametros para la personalizacion del mail">
                                                <asp:ListItem Text="[GUID]" Value="GUID"></asp:ListItem>
                                                <asp:ListItem Text="[PRIMER_NOMBRE]" Value="PRIMER_NOMBRE"></asp:ListItem>
                                                <asp:ListItem Text="[SEGUNDO_NOMBRE]" Value="SEGUNDO_NOMBRE"></asp:ListItem>
                                                <asp:ListItem Text="[PRIMER_APELLIDO]" Value="PRIMER_APELLIDO"></asp:ListItem>
                                                <asp:ListItem Text="[SEGUNDO_APELLIDO]" Value="SEGUNDO_APELLIDO"></asp:ListItem>
                                                <asp:ListItem Text="[NOMBRE_COMPLETO]" Value="NOMBRE_COMPLETO"></asp:ListItem>
                                                <asp:ListItem Text="[CORREO_ELECTRONICO]" Value="CORREO_ELECTRONICO"></asp:ListItem>
                                                <asp:ListItem Text="[CIUDAD]" Value="CIUDAD"></asp:ListItem>
                                                <asp:ListItem Text="[NOMBRE_BARRIO]" Value="NOMBRE_BARRIO"></asp:ListItem>
                                                <asp:ListItem Text="[GENERO]" Value="GENERO"></asp:ListItem>
                                            </asp:ListBox>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <asp:Panel ID="pnlTemplate" runat="server" Width="100%">
                                <table cellpadding="4px" width="100%" border="0">
                                    <tr>
                                        <td style="width: 130px">
                                            Plantilla de correo
                                        </td>
                                        <td>
                                            <telerik:RadAsyncUpload runat="server" ID="AsyncUpload1" MaxFileInputsCount="1" OnClientFileUploaded="fileUploaded"
                                                OnFileUploaded="AsyncUpload1_FileUploaded" AllowedFileExtensions="html" OnClientValidationFailed="validationFailed"
                                                MaxFileSize="2097152" Width="189px">
                                                <Localization Select="Buscar Plantilla" Cancel="Cancelar" Remove="Quitar" />
                                            </telerik:RadAsyncUpload>
                                        </td>
                                    </tr>
                                </table>
                                <div id="trTemplatePreview" clientidmode="Static" runat="server" style="display: none;
                                    width: 100%">
                                    <iframe id="templatePreview" clientidmode="Static" runat="server" width="100%" height="500px"
                                        border="0"></iframe>
                                    <asp:HiddenField ID="hfTemplateUrl" ClientIDMode="Static" runat="server" />
                                </div>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3">
                            <telerik:RadAjaxLoadingPanel ID="loadingPanel" runat="server">
                            </telerik:RadAjaxLoadingPanel>
                            <telerik:RadAjaxPanel ID="ajaxPanel" runat="server" LoadingPanelID="loadingPanel"
                                ClientEvents-OnResponseEnd="onResponseEnd">
                                <script type="text/javascript">

                                    function onResponseEnd(sender, args) {
                                        init();
                                        errorMessage = "";
                                    }
             
                                </script>
                                <asp:Panel ID="pnlSinActividad" runat="server" Width="100%" Visible="true">
                                    <table width="100%" border="0">
                                        <tr>
                                            <td>
                                                <table width="100%" border="0" cellpadding="4px">
                                                    <tr>
                                                        <td>
                                                            Tipo de envío:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlTipoEnvio" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlTipoEnvio_SelectedIndexChanged">
                                                                <asp:ListItem Value="false" Text="Manual"></asp:ListItem>
                                                                <asp:ListItem Value="true" Text="Programado"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Número de partes para la cola de envio:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNumeroPartes" runat="server" Text="1" MaxLength="2"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="El campo número de partes debe ser númerico"
                                                                ControlToValidate="txtNumeroPartes" ValidationGroup="Mailer" ValidationExpression="[0-9]+"></asp:RegularExpressionValidator>
                                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtNumeroPartes"
                                                                ValidationGroup="Mailer" Display="None" ErrorMessage="El campo numero de partes es requerido"></asp:RequiredFieldValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Fecha y hora de inicio del envío:
                                                        </td>
                                                        <td>
                                                            <telerik:RadDateTimePicker ID="dtpStartDate" runat="server" Width="180px" Enabled="False"
                                                                DateInput-DateFormat="dd/MM/yyyy" TimeView-Interval="00:30:00">
                                                            </telerik:RadDateTimePicker>
                                                            <asp:RequiredFieldValidator ID="rfvStartDate" runat="server" ControlToValidate="dtpStartDate"
                                                                Enabled="false" ValidationGroup="Mailer" Display="None" ErrorMessage="El campo fecha de inicio es requerido"></asp:RequiredFieldValidator>
                                                        </td>
                                                        <td>
                                                            Fecha y hora final del envío:
                                                        </td>
                                                        <td>
                                                            <telerik:RadDateTimePicker ID="dtpEndDate" runat="server" Width="180px" Enabled="false"
                                                                DateInput-DateFormat="dd/MM/yyyy" TimeView-Interval="00:30:00">
                                                            </telerik:RadDateTimePicker>
                                                            <asp:RequiredFieldValidator ID="rfvEndDate" runat="server" ControlToValidate="dtpEndDate"
                                                                Enabled="false" ValidationGroup="Mailer" Display="None" ErrorMessage="El campo fecha de fin es requerido"></asp:RequiredFieldValidator>
                                                            <asp:CompareValidator ID="rfvCompareDates" runat="server" ControlToValidate="dtpStartDate" ControlToCompare="dtpEndDate"  Operator="LessThan"
                                                                Enabled="true" ValidationGroup="Mailer" Display="None" ErrorMessage="El campo fecha de fin debe ser mayor al campo fecha de inicio"></asp:CompareValidator>
                                                        </td>
                                                    </tr>
                                                    <tr id="trGenerate" runat="server">
                                                        <td colspan="4">
                                                            Generar Tarea de Envio?&nbsp;
                                                            <asp:Button ID="btnGenerar" runat="server" Text="Aceptar" OnClick="btnGenerar_Click"
                                                                ValidationGroup="Mailer" />
                                                            <asp:Button ID="Button1" runat="server" Text="Volver" OnClick="btnNoEnviar_Click"
                                                                Width="80px" />
                                                            <asp:ValidationSummary ID="vsMailer" runat="server" ShowMessageBox="true" ShowSummary="false"
                                                                ValidationGroup="Mailer" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </telerik:RadAjaxPanel>
                            <asp:Panel ID="pnlResumen" runat="server" Width="100%" Visible="false">
                                <br />
                                <table border="0" width="600" cellpadding="4px">
                                    <tr>
                                        <td>
                                            <asp:Button ID="btnActualizarEstados" runat="server" Text="Actualizar" OnClick="btnActualizarEstados_Click"
                                                Width="80px" />
                                            <asp:Button ID="btnSiEnviar" runat="server" Text="Enviar" OnClick="btnSiEnviar_Click"
                                                Width="80px" />
                                            <asp:Button ID="btnCancelarEnvio" runat="server" Text="Cancelar" OnClick="btnCancelarEnvio_Click"
                                                CommandName="Cancelar" OnClientClick="return confirm ('Si cancela el envio borrará la tarea y todos sus archivos, Está seguro que quiere cancelar el envio?')" />
                                            <asp:Button ID="btnPausarEnvio" runat="server" Text="Pausar" OnClick="btnModificarEstadoEnvio_Click"
                                                CommandName="Pausar" Width="80px" OnClientClick="return confirm ('Está seguro que quiere pausar el envio?')" />
                                            <asp:Button ID="btnIniciarEnvio" runat="server" Text="Iniciar" OnClick="btnModificarEstadoEnvio_Click"
                                                CommandName="Iniciar" Visible="false" Width="80px" />
                                            <asp:Button ID="btnReiniciar" runat="server" Text="Reiniciar" OnClick="btnCancelarEnvio_Click"
                                                CommandName="Cancelar" OnClientClick="return confirm ('Si reinicia el envio borrará la tarea y todos sus archivos, Está seguro que quiere reiniciar el envio?')" />
                                            <asp:Button ID="btnNoEnviar" runat="server" Text="Volver" OnClick="btnNoEnviar_Click"
                                                Width="80px" />&nbsp;
                                            <asp:HiddenField ID="hdnIdTarea" runat="server" />
                                            <asp:HiddenField ID="hdnIdPlantilla" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
