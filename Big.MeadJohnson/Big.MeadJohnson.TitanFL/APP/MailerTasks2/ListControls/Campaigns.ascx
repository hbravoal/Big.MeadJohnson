﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Campaigns.ascx.cs" Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks2.ListControls.Campaigns" %>
<table cellpadding="5" cellspacing="0" border="1" width="100%" bordercolor="lightgrey">
    <tr runat="server" id="pnlRebotados" visible="false">
        <td colspan="2">
            ¿Desea procesar los correos rebotados?&nbsp;
            <asp:Button ID="btnProcesarRebotados" runat="server" Text="Procesar" OnClick="btnProcesarRebotados_Click" />&nbsp;&nbsp;<asp:Button
                ID="btnOcultarPnlRebo" runat="server" Text="Ocultar Opcion" OnClick="btnOcultarPnlRebo_Click" />
        </td>
    </tr>
    <tr>
        <td style="width: 100px">
            Campaña:
        </td>
        <td>
            <asp:TextBox ID="txtCampaignName" runat="server"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 100px">
            Estado Tarea:
        </td>
        <td>
            <asp:DropDownList ID="ddlTaskStatus" runat="server" DataTextField="Name" DataValueField="Id">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
        </td>
    </tr>
    <tr>
        <td colspan="2">
            <div style="height: 100%">
                <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                    PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                    OnItemDataBound="rgList_ItemDataBound">
                    <MasterTableView TableLayout="Fixed" Width="100%" CommandItemDisplay="Top">
                        <CommandItemSettings ShowRefreshButton="true" RefreshText="Actualizar" ShowAddNewRecordButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Programa" DataField="PROGRAMA">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Campaña" DataField="CAMPANA">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Actividad" DataField="ACTIVIDAD">
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn HeaderText="Tarea Envío">
                                <ItemTemplate>
                                    <asp:Literal ID="ltTaskId" runat="server" Text='<%# Bind("ID_TAREA") %>'></asp:Literal>
                                </ItemTemplate>
                                <ItemStyle Width="160px" />
                                <HeaderStyle Width="160px" />
                            </telerik:GridTemplateColumn>
                            <telerik:GridBoundColumn HeaderText="Estado" DataField="ESTADO" EmptyDataText="No aplica">
                                <ItemStyle Width="160px" />
                                <HeaderStyle Width="160px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <asp:Button ID="btnDetalle" runat="server" Text="Detalle" OnClientClick='<%#string.Format("window.location.href=\"Detail.aspx?taskId={0}&type=campaign\";return false;", Eval("ID_TAREA")) %>' />
                                    <asp:Button ID="btnGenerarGrid" runat="server" Text="Generar" CommandArgument='<%# Bind("GUID_CAMPANA") %>'
                                        CommandName='<%# Bind("GUID_ACTIVIDAD") %>' OnClick="btnSiGenerar_Click" />
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                                <HeaderStyle Width="100px" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <NoRecordsTemplate>
                            Sin registros encontrados
                        </NoRecordsTemplate>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </td>
    </tr>
</table>
