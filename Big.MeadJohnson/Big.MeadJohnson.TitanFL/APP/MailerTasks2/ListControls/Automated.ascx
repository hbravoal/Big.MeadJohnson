﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="Automated.ascx.cs" Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks2.ListControls.Automated" %>
<table cellpadding="5" cellspacing="0" border="1" width="100%" bordercolor="lightgrey">
    <tr>
        <td colspan="2">
            <div style="height: 100%">
                <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                    PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource">
                    <MasterTableView TableLayout="Fixed" Width="100%" CommandItemDisplay="Top">
                        <CommandItemSettings ShowRefreshButton="true" RefreshText="Actualizar" ShowAddNewRecordButton="false" />
                        <Columns>
                            <telerik:GridBoundColumn HeaderText="Fecha" DataField="FECHA" DataFormatString="{0:dd/MM/yyyy}">
                                <ItemStyle Width="160px" />
                                <HeaderStyle Width="160px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Descripción" DataField="DESCRIPCION">
                            </telerik:GridBoundColumn>
                            <telerik:GridBoundColumn HeaderText="Estado" DataField="ESTADO">
                                <ItemStyle Width="160px" />
                                <HeaderStyle Width="160px" />
                            </telerik:GridBoundColumn>
                            <telerik:GridTemplateColumn>
                                <ItemTemplate>
                                    <asp:Button ID="btnDetalle" runat="server" Text="Detalle" OnClientClick='<%#string.Format("window.location.href=\"Detail.aspx?taskId={0}&type=automated\";return false;", Eval("ID_TAREA")) %>' />
                                </ItemTemplate>
                                <ItemStyle Width="100px" />
                                <HeaderStyle Width="100px" />
                            </telerik:GridTemplateColumn>
                        </Columns>
                        <NoRecordsTemplate>
                            Sin registros encontrados
                        </NoRecordsTemplate>
                    </MasterTableView>
                </telerik:RadGrid>
            </div>
        </td>
    </tr>
</table>
