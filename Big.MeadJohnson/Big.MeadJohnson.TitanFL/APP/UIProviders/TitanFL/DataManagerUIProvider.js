﻿var args = new Array();
var handleSuccessDeleteEntityData = function (o)
{
   DisableDisplayLoading();
   //var div = document.getElementById('entityFormContainer');
   LoadSystemEntityForm(args[0], args[1], args[2], args[3], args[4], args[5]);
   /*
   if(o.responseText !== undefined)
   {
      LoadSystemEntityForm(args[0], args[1], args[2], args[3], args[4], args[5]);
      //div.innerHTML = o.responseText;
   }     
   */
}

var handleSuccessSaveEntityData = function (o)
{
   DisableDisplayLoading();
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response != '' )
      {
         ShowError(response);
         if (document.getElementById("btnSave") != null)
            document.getElementById("btnSave").disabled = false;
         if (document.getElementById("btnCancel") != null)
            document.getElementById("btnCancel").disabled = false;
      }
      else
         GetSystemEntityDataSearchSection(args[0], args[1], args[2], args[3], args[4], args[5]);
   }
}
var handleSuccessLoadSystemEntityDataSearchSection = function(o)
{
   DisableDisplayLoading();
   var div = document.getElementById('entityFormContainer');
   if(o.responseText !== undefined)
   {
      div.innerHTML = o.responseText;
      LoadSystemEntityForm(args[0], args[1], args[2], args[3], args[4], args[5]);
   }
}

var handleSuccessLoadSystemEntityForm = function(o)
{
   DisableDisplayLoading();
   var div = document.getElementById('entityDataFormContainer');
   if(o.responseText !== undefined)
   {
      div.innerHTML = o.responseText;
   }
}

var handleSuccessLoadSystems = function(o)
{
   DisableDisplayLoading();
   if(o.responseText !== undefined)
   {
      //get response from server
      var systems = new String(o.responseText);
      var systemsList = systems.split(",");
     
      //get root node for tree:
      var root = tree.getRoot();

      for (var i=0; i < systemsList.length; i++) 
      {
         var systemData = systemsList[i].split("|");
         var tmpNode = new YAHOO.widget.TextNode(systemData[0], root, false);
         tmpNode.label = systemData[1];
         tmpNode.hasIcon = false;
      }

      tree.draw();
   }
};

var handleSuccessGetEntityReferenceData = function(o)
{
   DisableDisplayLoading();
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      CleanSelected('lstSelectedNew');
      CleanSelected('lstEntityInstanceDataNew');
      
      if ( response != '' )
      {
         fillSelect(response, "lstEntityInstanceDataNew");
      }
      
      if ( o.argument[0] != undefined && o.argument[0] != "" )
      {
         fillSelect(o.argument[0], "lstSelectedNew");
         
         var listClusters = document.getElementById('lstSelectedNew');
         var listBusinessEntities = document.getElementById('lstEntityInstanceDataNew');
         
         if ( listClusters == null || listBusinessEntities == null )
            ShowError('No existen los controles');
            
         for ( var i = 0; i < listClusters.length; i++ )
         {
            for ( var j = 0; j < listBusinessEntities.length; j++ )
            {
               if ( listClusters.options[i].value == listBusinessEntities.options[j].value )
               {
                  listClusters.options[i].text = listBusinessEntities.options[j].text;
                  listBusinessEntities.remove(j);
               }
            }
         }
      }
      
      YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.setHeader("Seleccionar valores");
      YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.show();
   }
};

var handleSuccessValidatePassword = function(o)
{
   DisableDisplayLoading();
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response == "true" )
      {
         oPassword.value = document.getElementById('pswNew').value;
         YAHOO.OnData.TitanFL.container.dlgPassword.hide();
      }
      else if ( response == "false" )
      {
         ShowError("La contraseña actual ingresada es incorrecta");
      }
   }
};

var handleSuccessGetSystemEntityReferentialValues = function(o)
{
   DisableDisplayLoading();
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      
      if ( response != '' )
      {
         document.getElementById("divSelectOutofRange").innerHTML = response;
      }
   }
};

handleSuccessLoadSystemEntityUpdateForm = function (o)
{
   DisableDisplayLoading();
   var div = document.getElementById('entityFormContainer');
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      div.innerHTML = response;
      FillControlsUpdateDate();
   }
}

handleSuccessLoadSystemEntityInsertForm = function (o)
{
   DisableDisplayLoading();
   var div = document.getElementById('entityFormContainer');
   response = o.responseText;
   if(response !== undefined)
   {
      response = response.split("<!")[0];
      div.innerHTML = response;
      FillControlsDate();
   }
}

handleSuccessLoadSystemEntities= function(o)
{
   DisableDisplayLoading();
   if(o.responseText != "")
   {      
      //get response from server
      var entities = new String(o.responseText);
      var entitiesList = entities.split(",");
     
      for (var i=0; i < entitiesList.length; i++) 
      {
         var entityData = entitiesList[i].split("|");
         
         //var tmpNode = new YAHOO.widget.HTMLNode("<strong><div style='cursor:pointer' onclick='LoadSystemEntityForm(\"" + currentNode.data + "\",\"" + entityData[0] + "\",1, \"__Default\")'>"+entityData[1]+"</div></strong>", currentNode, false, false);
         var tmpNode = new YAHOO.widget.HTMLNode("<strong><div style='cursor:pointer' title='" + entityData[2] +"' onclick='GetSystemEntityDataSearchSection(\"" + currentNode.data + "\",\"" + entityData[0] + "\", 1, \"\", \"\", \"\")'>"+entityData[1]+"</div></strong>", currentNode, false, false);
         
      }

      tree.draw();            
   }
}

var callbackDeleteEntityData = 
{
   success:handleSuccessDeleteEntityData,
   failure:handleFailure,
   argument:args
};

var callbackSaveEntityData = 
{
   success:handleSuccessSaveEntityData,
   failure:handleFailure,
   argument:args
};

var callbackLoadSystems =
{
   success:handleSuccessLoadSystems,
   failure:handleFailure,
   argument:['LoadSystems']
};

var callbackLoadSystemEntities =
{
   success:handleSuccessLoadSystemEntities,
   failure:handleFailure,
   argument:['LoadSystemEntities']
};

var callbackLoadSystemEntityForm =
{
   success:handleSuccessLoadSystemEntityForm,
   failure:handleFailure,
   argument:['LoadSystems']
};      

var callbackLoadSystemEntityDataSearchSection =
{
   success:handleSuccessLoadSystemEntityDataSearchSection,
   failure:handleFailure,
   argument:args
};      

var callbackLoadSystemEntityUpdateForm =
{
   success:handleSuccessLoadSystemEntityUpdateForm,
   failure:handleFailure,
   argument:['LoadSystemEntityUpdateForm']      
};

var callbackLoadSystemEntityInsertForm =
{
   success:handleSuccessLoadSystemEntityInsertForm,
   failure:handleFailure,
   argument:['LoadSystemEntityInsertForm']      
};

var callbackGetEntityReferenceData =
{
   success:handleSuccessGetEntityReferenceData,
   failure:handleFailure,
   argument:args
};

var callbackValidatePassword =
{
   success:handleSuccessValidatePassword,
   failure:handleFailure
};

var callbackGetSystemEntityReferentialValues =
{
   success:handleSuccessGetSystemEntityReferentialValues,
   failure:handleFailure
};

var tree, currentIconMode, currentNode;

function changeIconMode() 
{
   var newVal = parseInt(this.value);
   if (newVal != currentIconMode) 
   {
      currentIconMode = newVal;
   }
   buildTree();
}

var oPassword = null;
var guidPassword = "";
var entityPassword = "";
var systemPassword = "";
var propertyNamePassword = "";

function ManagePassword(o, guid, entity, system, propertyName)
{
   oPassword = o;   
   guidPassword = guid;
   entityPassword = entity;
   systemPassword = system;
   propertyNamePassword = propertyName;
   document.getElementById('pswCurrent').value = "";
   document.getElementById('pswNew').value = "";
   document.getElementById('pswNewConfirm').value = "";
   
   if ( guid == '' )
      document.getElementById('pswCurrent').disabled = true;
   else
      document.getElementById('pswCurrent').disabled = false;
      
   YAHOO.OnData.TitanFL.container.dlgPassword.setHeader("Contraseña");
   YAHOO.OnData.TitanFL.container.dlgPassword.show();
}

function ManageReferencialEntity(o)
{
   ShowError("Administrar referencia para " + o.getAttribute("Name"));
   o.value = "entidad"
}

function DeleteEntityData(guid, system, systemEntity, page, sortBy, sortOrder, searchCriteria)
{
   if(confirm("¿Esta segudo de eliminar el registro?"))
   {
      args[0] = system;
      args[1] = systemEntity;
      args[2] = page;
      args[3] = sortBy;
      args[4] = sortOrder;
      args[5] = searchCriteria;   
      makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=DeleteEntityData&System="+system+"&Entity="+systemEntity+"&Guid="+guid+"&CurrentPage="+page+"&CurrentSortBy="+sortBy, callbackSaveEntityData);
   }
}

function SaveEntityData(form, guid, system, systemEntity, page, sortBy, sortOrder, searchCriteria)
{

   if (guid == null)
      guid = "";

   var formIsValid = true;
   var entityData = new String("");
   var entityProperties = new String("");
   var propertyValue = new String("");
   var validatorsMessages = new String("");
   for(var i = 0; i < form.elements.length; i++)
   {
      if(form.elements[i].getAttribute("EntityProperty") == "true")
      {
         propertyValue = "";
         
         /**********************************/
         /* Tipo Booleano                  */
         if(form.elements[i].getAttribute("PropertyType") == "4")
         {
            if(form.elements[i].checked)
               propertyValue = "1";
            else
               propertyValue = "0";
         }
         /**********************************/
         /* Tipo Number                    */
         else if (form.elements[i].getAttribute("PropertyType") == "2")
         {
            propertyValue = "0";
            
            if ( form.elements[i].value != "" )
               propertyValue = form.elements[i].value;
         }
         /**********************************/
         /* Tipo Decimal y Currency        */
         else if (form.elements[i].getAttribute("PropertyType") == "3" || form.elements[i].getAttribute("PropertyType") == "10")
         {
            propertyValue = "0";
            
            if ( form.elements[i].value != "" )
            {
               if (onlyDecimals(form.elements[i].value))
               {
                  propertyValue = form.elements[i].value;
               }
               else
               {
                  validatorsMessages += "- El formato escrito para '" + form.elements[i].getAttribute("DisplayName") + "' no es correcto<br/>";
                  formIsValid = false;
               }
            }
         }
         /**********************************/
         /* Tipo Correo electronico        */
         else if (form.elements[i].getAttribute("PropertyType") == "13")
         {
            if ( form.elements[i].value != "" )
            {
               if (validateEmail(form.elements[i].value))
               {
                  if (form.elements[i].value.search("'") == -1 )
                  {
                     propertyValue = form.elements[i].value;
                  }
                  else
                  {
                     validatorsMessages += "- El correo electrónico escrito para '" + form.elements[i].getAttribute("DisplayName") + "' tiene una comilla simple (\'). Este caracter no es permitido<br/>";
                     formIsValid = false;
                  }
               }
               else
               {
                  validatorsMessages += "- El formato de correo electrónico escrito para '" + form.elements[i].getAttribute("DisplayName") + "' no es correcto<br/>";
                  formIsValid = false;
               }
            }
         }
         /**********************************/
         /* Tipo LongString                */
         else if (form.elements[i].getAttribute("PropertyType") == "1")
         {
            if ( form.elements[i].value.length > form.elements[i].getAttribute("LengthTextArea") )
            {
               validatorsMessages = validatorsMessages + "- La longitud de '" + form.elements[i].getAttribute("DisplayName") + "' tiene que ser menor o igual a " + form.elements[i].getAttribute("LengthTextArea") + "<br/>";
               formIsValid = false;
            }
            else
            {
               if (form.elements[i].value.search("'") == -1 )
               {
                  propertyValue = form.elements[i].value;
               }
               else
               {
                  validatorsMessages += "- El campo '" + form.elements[i].getAttribute("DisplayName") + "' tiene una comilla simple (\'). Este caracter no es permitido<br/>";
                  formIsValid = false;
               }
            }
         }
         /**********************************/
         /* Tipo DateTime                  */
         else if(form.elements[i].getAttribute("PropertyType") == "7")
         {
            if ( form.elements[i].value != "-1/-1/-1" )
            {
               var tempMonthValidate = form.elements[i].value.substring(5,7);
               var tempDayValidate = form.elements[i].value.substring(8,10);
               
               propertyValue = form.elements[i].value;
               
               if ( tempMonthValidate == '-1' )
               {
                  validatorsMessages = validatorsMessages + "- El mes de '" + form.elements[i].getAttribute("DisplayName") + "' tiene que ser seleccionado<br/>";
                  formIsValid = false;
               }
               else
               {
                  if ( tempDayValidate == '-1' )
                  {
                     validatorsMessages = validatorsMessages + "- El día de '" + form.elements[i].getAttribute("DisplayName") + "' tiene que ser seleccionado<br/>";
                     formIsValid = false;
                  }
               }
            }
            else
            {
               if((form.elements[i].getAttribute("Required") == "true"))
               {
                  validatorsMessages = validatorsMessages + "- El campo " + form.elements[i].getAttribute("DisplayName") + " es requerido<br/>";
                  formIsValid = false;
               }
               propertyValue = form.elements[i].value;
            }
         }            
         /**********************************/
         /* Tipo Entity y varios valores   */
         else if((form.elements[i].getAttribute("PropertyType") == "12") && (form.elements[i].getAttribute("PropertySourceEntitySelectionType") == "1"))
         {
            var elements = "";
            for ( var j = 0; j < form.elements[i].length; j++ )
            {
               elements += "|" + form.elements[i].options[j].value + "|,";
            }
            
            if ( elements.length != 0 )
               elements = elements.substring(0, elements.length-1);
               
            propertyValue = elements;
         }   
         /**********************************/
         /* Tipo Entity y un sólo valor    */
         else if((form.elements[i].getAttribute("PropertyType") == "12") && (form.elements[i].getAttribute("PropertySourceEntitySelectionType") == "0"))
         {
            if(form.elements[i].value!='-1')
               propertyValue = form.elements[i].value;
         }
         /**********************************/
         /* Cadena de caracteres           */
         /* Password                       */
         else if (form.elements[i].getAttribute("PropertyType") == "0" || form.elements[i].getAttribute("PropertyType") == "11")
         {
            if (form.elements[i].value.search("'") == -1 )
            {
               propertyValue = form.elements[i].value;
            }
            else
            {
               validatorsMessages += "- El campo '" + form.elements[i].getAttribute("DisplayName") + "' tiene una comilla simple (\'). Este caracter no es permitido<br/>";
               formIsValid = false;
            }
         }
         /**********************************/
         /* Los otros tipos                */      
         else
            propertyValue = form.elements[i].value;
       
         if((form.elements[i].getAttribute("Required") == "true") && (propertyValue == ""))
         {
            validatorsMessages = validatorsMessages + "- El campo " + form.elements[i].getAttribute("DisplayName") + " es requerido<br/>";
            formIsValid = false;
         }  
         entityProperties = entityProperties + form.elements[i].getAttribute("Name") + "|" + form.elements[i].getAttribute("PropertyType") + ",";
         entityData = entityData + form.elements[i].id + "=" + propertyValue + "&";   
      }
   }
   if(!formIsValid)
   {
      ShowError(validatorsMessages);
      return;
   }
   entityProperties = entityProperties.substring(0, entityProperties.length - 1);
   entityData =  entityData.substring(0, entityData.length - 1);
  
   args[0] = system;
   args[1] = systemEntity;
   args[2] = page;
   args[3] = sortBy;
   args[4] = sortOrder;
   args[5] = searchCriteria;
   
   document.getElementById("btnSave").disabled = true;
   document.getElementById("btnCancel").disabled = true;
   
   var postData = ("MethodToCall=SaveEntityData&System="+system+"&Entity="+systemEntity+"&Guid="+guid+"&CurrentPage="+page+"&CurrentSortBy="+sortBy+"&EntityProperties="+entityProperties+"&"+entityData);
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", postData, callbackSaveEntityData);
}

function ValidateEntityPropertyValue(val)
{
   var rex = val.getAttribute("RexValidator");
   if(ValidatorTrim(rex).length == 0)
      return true;
   var value = val.value;
   
   if (ValidatorTrim(value).length == 0)
      return true;
      
   var rx = new RegExp(rex);
   var matches = rx.exec(value);
   return (matches != null && value == matches[0]);
}

function ValidatorTrim(s) {
    var m = s.match(/^\s*(\S+(\s+\S+)*)\s*$/);
    return (m == null) ? "" : m[1];
}

function LoadSystemEntityUpdateForm(guid, system, systemEntity, currentPage, currentSortBy, currentSortOrder, searchCriteria)
{
   //document.getElementById('entityFormTitle').innerHTML = "<h3>"+system+"."+systemEntity+"</h3>";
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntityUpdateForm&Guid=" + guid + "&System="+system+"&Entity="+systemEntity+"&CurrentPage="+currentPage+"&CurrentSortBy="+currentSortBy+"&SortOrder="+currentSortOrder+"&SearchCriteria="+searchCriteria, callbackLoadSystemEntityUpdateForm);
}

function LoadSystemEntityInsertForm(system, systemEntity, currentPage, currentSortBy, currentSortOrder, searchCriteria)
{
   //document.getElementById('entityFormTitle').innerHTML = "<h3>"+system+"."+systemEntity+"</h3>";
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntityInsertForm&System="+system+"&Entity="+systemEntity+"&CurrentPage="+currentPage+"&CurrentSortBy="+currentSortBy+"&SortOrder="+currentSortOrder+"&SearchCriteria="+searchCriteria, callbackLoadSystemEntityInsertForm);
}
function LoadSystemEntityForm(system, systemEntity, page, sortBy, sortOrder, searchCriteria, form)
{
   if(form!=undefined)
   {
      for(var i = 0; i < form.elements.length; i++)
      {
         if(form.elements[i].getAttribute("EntityPropertyCriteria") == "true")
         {
            if(form.elements[i].value != -1)
            {
               if (form.elements[i].value != "")
                  searchCriteria = searchCriteria + form.elements[i].id + "|=|" + form.elements[i].value + ",";
            }
         }
         if(form.elements[i].getAttribute("EntityPropertyDynamicCriteria") == "true")
         {
            if(form.elements[i].value != -1)
            {
               var searchOperator = document.getElementById("OperatorList");
               var searchCriteriaText = document.getElementById("searchCriteriaText");
               searchCriteria = searchCriteria + form.elements[i].value + "|" + searchOperator.options[searchOperator.selectedIndex].id + "|" +  searchCriteriaText.value + ",";
            }
         }
      }
      if(searchCriteria!="")
         searchCriteria =  searchCriteria.substring(0, searchCriteria.length - 1);
   }
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntityDataForm&System="+system+"&Entity="+systemEntity+"&Page="+page+"&SortBy="+sortBy+"&SortOrder="+sortOrder+"&SearchCriteria="+searchCriteria, callbackLoadSystemEntityForm);
}
function GetSystemEntityDataSearchSection(system, systemEntity, page, sortBy, sortOrder, searchCriteria)
{
   args[0] = system;
   args[1] = systemEntity;
   args[2] = page;
   args[3] = sortBy;
   args[4] = sortOrder;
   args[5] = searchCriteria
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntityDataSearchSection&System="+system+"&Entity="+systemEntity, callbackLoadSystemEntityDataSearchSection);
}
function GetEntityReferenceData(o, system, systemEntity, nameProperty)
{
   var elements = "";
   args[1] = o.id;
   
   for ( var i = 0; i < document.getElementById(o.id).length; i++ )
   {
      elements += document.getElementById(o.id).options[i].value + "|" + document.getElementById(o.id).options[i].text + ",";
   }
   
   if ( elements.length != 0 )
      elements = elements.substring(0, elements.length-1);
   
   args[0] = elements;
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetEntityReferenceData&System="+system+"&Entity="+systemEntity+"&NameProperty="+nameProperty, callbackGetEntityReferenceData);
}

function AllEntityInstanceToSelected(type)
{
   if ( document.getElementById('lstEntityInstanceData' + type).length != 0 )
      fillSelect(CleanSelected('lstEntityInstanceData' + type), 'lstSelected' + type);
}

function AllSelectedToEntityInstance(type)
{
   if ( document.getElementById('lstSelected' + type).length != 0 )
      fillSelect(CleanSelected('lstSelected' + type), 'lstEntityInstanceData' + type);
}

function SomeSelectedToEntityInstance(type)
{
   if ( document.getElementById('lstSelected' + type).length != 0 )
      ExchangeOptions('lstSelected' + type, 'lstEntityInstanceData' + type);
}

function SomeEntityInstanceToSelected(type)
{
   if ( document.getElementById('lstEntityInstanceData' + type).length != 0 )
      ExchangeOptions('lstEntityInstanceData' + type, 'lstSelected' + type);
}

function fillSelect(stringValues, idControl)
{
   var optionsList = stringValues.split(",");
   var elSel = null;
   var item = null;
   
   if ( document.getElementById(idControl) != null )
      elSel = document.getElementById(idControl);
   
   if ( elSel != null )
   {
      for (var i=0; i < optionsList.length; i++) 
      {
         item = optionsList[i].split("|");
         var elOptNew = document.createElement('option');
         elOptNew.value = item[0];
         elOptNew.text = item[1];
         
         try {
            elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
         }
         catch(ex) {
            elSel.add(elOptNew); // IE only
         }
      }
   }
}

function ExchangeOptions(controlOld, controlNew)
{
   var selected = document.getElementById(controlOld).selectedIndex;
   var vlrOption = "";
   var vlrText = "";
   
   if ( selected != -1 )
   {
      vlrOption = document.getElementById(controlOld).options[selected].value;
      vlrText = document.getElementById(controlOld).options[selected].text;
      
      document.getElementById(controlOld).remove(selected);
      
      var elOptNew = document.createElement('option');
      elOptNew.text = vlrText;
      elOptNew.value = vlrOption;
      
      try {
         document.getElementById(controlNew).add(elOptNew, null); // standards compliant; doesn't work in IE
      }
      catch(ex) {
         document.getElementById(controlNew).add(elOptNew); // IE only
      }
   }
}

function CleanSelected(control)
{
   var elements = "";
   for ( var i = (document.getElementById(control).length - 1); i != -1; i-- )
   {
      elements += document.getElementById(control).options[0].value + "|" + document.getElementById(control).options[0].text + ",";
      document.getElementById(control).remove(0);
   }
   
   if ( elements.length != 0 )
      elements = elements.substring(0, elements.length-1);
      
   return elements;
}

function GetSelectedValues()
{
   var mylist = null;
   
   if ( document.getElementById("lstSelectedNew") != null )
      mylist = document.getElementById("lstSelectedNew");
   
   if ( document.getElementById(args[1]) != null )  
      myCtrl = document.getElementById(args[1]);
      
   CleanSelected(args[1]);
   if (  mylist.length != 0 )
   {
      var elements = "";
      for ( var i = 0; i < mylist.length; i++ )
      {
         var elOptNew = document.createElement('option');
         elOptNew.text = mylist.options[i].text;
         elOptNew.value = mylist.options[i].value;
         
         try 
         {
            myCtrl.add(elOptNew, null); // standards compliant; doesn't work in IE
         }
         catch(ex) {
            myCtrl.add(elOptNew); // IE only
         }      
      }
   }
   //document.getElementById(args[1]).value = elements;
   return 1;
}

function SavePassword()
{
   var passwordSend = "";
   
   if ( document.getElementById('pswNew').value != document.getElementById('pswNewConfirm').value )
   {
      ShowError("La nueva contraseña y su confirmación son diferentes");
   }
   else
   {
      if ( guidPassword == '' )
      {
         oPassword.value = document.getElementById('pswNew').value;
         YAHOO.OnData.TitanFL.container.dlgPassword.hide();
      }
      else
      {
         passwordSend = document.getElementById('pswCurrent').value;
         makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=ValidatePassword&Guid=" + guidPassword + "&Entity=" + entityPassword + "&System=" + systemPassword + "&PasswordSend=" + passwordSend + "&PropertyName=" + propertyNamePassword, callbackValidatePassword);
      }
   }
}

function FillControlsDate()
{
   var form = document.forms[0];
   var today = new Date();
   
   for(i = 0; i < form.elements.length; i++)
   {
      if ( form.elements[i].getAttribute("TypeDate") == 'true' )
      {
         FillDateYears(form.elements[i].id + "Year");
         FillDateMonth(form.elements[i].id + "Month");
         document.getElementById(form.elements[i].id + "Year").value = today.getFullYear();
         
         if (today.getMonth()<10)
         {
            document.getElementById(form.elements[i].id + "Month").value = "0" + String(Number(today.getMonth())+Number(1));            
         }
         else
         {
            document.getElementById(form.elements[i].id + "Month").value = "0" + today.getMonth()+1;
         }
         
         FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);
         
         if (today.getDate()<10)
         {
            document.getElementById(form.elements[i].id + "Day").value = "0" + String(today.getDate());
         }
         else
         {
            document.getElementById(form.elements[i].id + "Day").value = today.getDate();
         }
         document.getElementById(form.elements[i].id).value = today.getFullYear() + "/" + document.getElementById(form.elements[i].id + "Month").value + "/" + today.getDate();
      }
   }
}

function FillControlsUpdateDate()
{
   var form = document.forms[0];
   //var today = new Date();
   
   for(i = 0; i < form.elements.length; i++)
   {
      if ( form.elements[i].getAttribute("TypeDate") == 'true' )
      {
         var dateString = document.getElementById(form.elements[i].id).value;
         
         if (dateString != "")
         {
            if ( dateString.substring(0,10) != '01/01/1900' )
            {
               FillDateYears(form.elements[i].id + "Year");
               FillDateMonth(form.elements[i].id + "Month");
               document.getElementById(form.elements[i].id + "Year").value = dateString.substring(6,10);
               document.getElementById(form.elements[i].id + "Month").value = dateString.substring(3,5);
               FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);
               document.getElementById(form.elements[i].id + "Day").value = dateString.substring(0,2);
               document.getElementById(form.elements[i].id).value = dateString;
            }
            else
            {
               FillDateYears(form.elements[i].id + "Year");
               FillDateMonth(form.elements[i].id + "Month");
               document.getElementById(form.elements[i].id + "Year").value = '-1';
               document.getElementById(form.elements[i].id + "Month").value = '-1';
               FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);
               document.getElementById(form.elements[i].id + "Day").value = '-1';
               document.getElementById(form.elements[i].id).value = "-1/-1/-1";
            }
         }
         else
         {
            FillDateYears(form.elements[i].id + "Year");
            FillDateMonth(form.elements[i].id + "Month");
            document.getElementById(form.elements[i].id + "Year").value = '-1';
            document.getElementById(form.elements[i].id + "Month").value = '-1';
            FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);
            document.getElementById(form.elements[i].id + "Day").value = '-1';
            document.getElementById(form.elements[i].id).value = "-1/-1/-1";
         }
      }
   }
}

function loadNodeData(node, fnLoadComplete) 
{
   currentNode = node;
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntities&System="+node.data, callbackLoadSystemEntities);
   fnLoadComplete();
}

function buildTree() 
{
   tree = new YAHOO.widget.TreeView("treeContainer");
   tree.setDynamicLoad(loadNodeData, currentIconMode);
}

YAHOO.namespace('OnData.TitanFL.container');

function init() 
{
    /*************************************************************************
    * Creación del formulario para cuando hay mas una cantidad maxima de datos
    **************************************************************************/
   var handleCancelOutOfRange = function(e) {
      this.hide();
	}

	var handleOKOutOfRange = function(e) {
      SaveOutOfRange();
	}
   
   YAHOO.OnData.TitanFL.container.dlgOutOfRange = new YAHOO.widget.SimpleDialog("dlgManageOutOfRangeInstances", { visible:false, width: "400px", fixedcenter:true, modal:true, draggable:true });
	YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("buttons", [ 
																	{ text:"Seleccionar", handler:handleOKOutOfRange, isDefault:true },
																	{ text:"Cancelar", handler:handleCancelOutOfRange }
																]);
																
   listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handleCancelOutOfRange ,scope:YAHOO.OnData.TitanFL.container.dlgOutOfRange, correctScope:true} );
	YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("keylisteners", listeners);

	YAHOO.OnData.TitanFL.container.dlgOutOfRange.render(document.body);
	
	/*************************************************************************
    * FIN del formulario
    *************************************************************************/
    
    /*****************************************************************
    * Creación del formulario para editar Password
    *****************************************************************/
   var handleCancelPassword = function(e) {
      this.hide();
	}

	var handleOKPassword = function(e) {
      SavePassword();
	}
   
   YAHOO.OnData.TitanFL.container.dlgPassword = new YAHOO.widget.SimpleDialog("dlgPassword", { visible:false, width: "400px", fixedcenter:true, modal:true, draggable:true });
	YAHOO.OnData.TitanFL.container.dlgPassword.cfg.queueProperty("buttons", [ 
																	{ text:"Guardar", handler:handleOKPassword, isDefault:true },
																	{ text:"Cancelar", handler:handleCancelPassword }
																]);
																
   listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handleCancelPassword ,scope:YAHOO.OnData.TitanFL.container.dlgPassword, correctScope:true} );
	YAHOO.OnData.TitanFL.container.dlgPassword.cfg.queueProperty("keylisteners", listeners);

	YAHOO.OnData.TitanFL.container.dlgPassword.render(document.body);
	
	/*****************************************************************
    * FIN del formulario
    *****************************************************************/
    
   /*****************************************************************
    * Creación del formulario para seleccionar valores de una entidad
    *****************************************************************/
   var handleCancelEntityReferenceData = function(e) {
      this.hide();
	}

	var handleOKEntityReferenceData = function(e) {
	   if ( GetSelectedValues() == 1 )
	      this.hide();
	}
   
   YAHOO.OnData.TitanFL.container.dlgEntityReferenceData = new YAHOO.widget.SimpleDialog("dlgEntityReferenceData", { visible:false, width: "585px", fixedcenter:true, modal:true, draggable:true });
	YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.cfg.queueProperty("buttons", [ 
																	{ text:"Seleccionar", handler:handleOKEntityReferenceData, isDefault:true },
																	{ text:"Cancelar", handler:handleCancelEntityReferenceData }
																]);
																
   listeners = new YAHOO.util.KeyListener(document, { keys : 27 }, {fn:handleCancelEntityReferenceData ,scope:YAHOO.OnData.TitanFL.container.dlgEntityReferenceData, correctScope:true} );
	YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.cfg.queueProperty("keylisteners", listeners);

	YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.render(document.body);
	
	/*****************************************************************
    * FIN del formulario
    *****************************************************************/
   
   YAHOO.util.Event.on(["mode0", "mode1"], "click", changeIconMode);
   var el = document.getElementById("mode1");
   if (el && el.checked) 
   {
       currentIconMode = parseInt(el.value);
   } else 
   {
       currentIconMode = 0;
   }
   buildTree();
   makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetTitanFLSystems", callbackLoadSystems);
 }

YAHOO.util.Event.addListener(window, "load", init, "",true)

//Manejo de fecha

function FillDateYears(listYearId)
{
   // Esta informacion debe traerla del WebConfig
   var today = new Date();
   var startYear = startYearConfig;
   var endYear = endYearConfig;
   var mylistYear = document.getElementById(listYearId);
   var elements = "-1|- Año -,";
   
   if ( mylistYear.length == 0 )
   {
      for ( var i = endYear; i >= startYear; i-- )
      {
         elements += i + "|" + i + ",";
      }
   }
   
   if ( elements.length != 0 )
      elements = elements.substring(0, elements.length-1);
      
   fillSelectDate(elements, listYearId);
}

function FillDateMonth(listMonthId)
{
   var elements = "";
   
   elements = "-1|- Mes -,01|Enero,02|Febrero,03|Marzo,04|Abril,05|Mayo,06|Junio,07|Julio,08|Agosto,09|Septiembre,10|Octubre,11|Noviembre,12|Diciembre";
   fillSelectDate(elements, listMonthId);
}

function FillDateDays(listYearId, listMonthId, listDayId, dateCtrlId)
{
   var mylistYear = document.getElementById(listYearId);
   var mylistMonth = document.getElementById(listMonthId);
         
   var year = mylistYear.options[mylistYear.selectedIndex].value;
   var month = mylistMonth.options[mylistMonth.selectedIndex].value;

   if ( year == -1 )
   {
      document.getElementById(listMonthId).value = '-1';
      month = '-1';
   }
   
   CleanSelectedDateDays(listDayId);
   
   var sizeDays = 0;
   
   switch ( month )
   {
      case '01':
      case '03':
      case '05':
      case '07':
      case '08':
      case '10':
      case '12':
         sizeDays = 31;
         break;
      case '04':
      case '06':
      case '09':
      case '11':
         sizeDays = 30;
         break;
      case '02':
         if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
            sizeDays = 29;
         else
            sizeDays = 28;
         break;
      case '-1':
         sizeDays = 0;
         break;
   }
   
   var elements = "-1|- Día -,";
   
   for ( var i = 1; i <= sizeDays; i++ )
   {
      if(i<10)
         elements += "0" + i + "|0" + i + ",";
      else
         elements += i + "|" + i + ",";
   }

   if ( elements.length != 0 )
      elements = elements.substring(0, elements.length-1);
      
   fillSelectDate(elements, listDayId);
   
   if ( document.getElementById(dateCtrlId) != undefined )
   {
      document.getElementById(dateCtrlId).value = year + "/" + month + "/" + "-1";
   }
}

function onlyDecimals(varNumber) 
{
   patron =/^\d*\,?\d*$/;
   return patron.test(varNumber);
}

function FillControlHidden(dateCtrlId)
{
   var mylistYear = document.getElementById(dateCtrlId + "Year");
   var mylistMonth = document.getElementById(dateCtrlId + "Month");
   var mylistDay = document.getElementById(dateCtrlId + "Day");
   
   var year = mylistYear.options[mylistYear.selectedIndex].value;
   var month = mylistMonth.options[mylistMonth.selectedIndex].value;
   var day = mylistDay.options[mylistDay.selectedIndex].value;
   
   if ( document.getElementById(dateCtrlId) != undefined )
      document.getElementById(dateCtrlId).value = year + "/" + month + "/" + day;
}

function CleanSelectedDateDays(control)
{
   for ( var i = (document.getElementById(control).length - 1); i != -1; i-- )
   {
      document.getElementById(control).remove(0);
   }
}

function fillSelectDate(stringValues, idControl)
{
   var optionsList = stringValues.split(",");
   var elSel = null;
   var item = null;
   
   if ( document.getElementById(idControl) != null )
      elSel = document.getElementById(idControl);
   
   if ( elSel != null )
   {
      for (var i=0; i < optionsList.length; i++) 
      {
         item = optionsList[i].split("|");
         var elOptNew = document.createElement('option');
         elOptNew.value = item[0];
         elOptNew.text = item[1];
         
         try {
            elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
         }
         catch(ex) {
            elSel.add(elOptNew); // IE only
         }
      }
   }
}

var systemOutOfRangeGral = "";
var entityOutOfRangeGral = "";
var propertyOutOfRangeGral = "";
var oOutOfRangeGral = null;
var oHiddenOutOfRangeGral = null;

function GetManageOutOfRangeInstances(systemOutOfRange, entityOutOfRange, propertyOutOfRange, oOutOfRange, oHiddenOutOfRange)
{
   systemOutOfRangeGral = systemOutOfRange;
   entityOutOfRangeGral = entityOutOfRange;
   propertyOutOfRangeGral = propertyOutOfRange;
   oOutOfRangeGral = oOutOfRange;
   oHiddenOutOfRangeGral = oHiddenOutOfRange;
   
   if ( document.getElementById("txtFindValue") != null )
      document.getElementById("txtFindValue").value = '';
      
   document.getElementById("divSelectOutofRange").innerHTML = "";
   
   YAHOO.OnData.TitanFL.container.dlgOutOfRange.setHeader("Buscar");
   YAHOO.OnData.TitanFL.container.dlgOutOfRange.show();
}

function findValue()
{
   var vlrFindValue = "";
   
   if ( document.getElementById("txtFindValue") != null && document.getElementById("txtFindValue").value != '' )
   {
      vlrFindValue = "&FindValue=" + document.getElementById("txtFindValue").value;
      document.getElementById("divSelectOutofRange").innerHTML = "Buscando...";
      makeRequest("UIProviders/TitanFL/DataManagerUIProvider.aspx", "MethodToCall=GetSystemEntityReferentialValues&System=" + systemOutOfRangeGral + "&Entity=" + entityOutOfRangeGral + "&PropertyName=" + propertyOutOfRangeGral + vlrFindValue, callbackGetSystemEntityReferentialValues);
   }
   else
   {
      alert("Digite un patrón de búsqueda");
   }
}

function SaveOutOfRange()
{
   var mylist = null;
   
   if ( document.getElementById("lstOutOfRange") != null )
   {
      mylist = document.getElementById("lstOutOfRange");
      
      if ( mylist.length != 0 )
      {
         if ( mylist.selectedIndex != -1 )
         {
            oOutOfRangeGral.value = mylist.options[mylist.selectedIndex].text;
            oHiddenOutOfRangeGral.value = mylist.options[mylist.selectedIndex].value;
            YAHOO.OnData.TitanFL.container.dlgOutOfRange.hide();
         }
         else
         {
            alert("Debe seleccionar un elemento");
         }
      }
      else
      {
         alert("No hay elementos para seleccionar");
      }
   }
   else
   {
      alert("No hay elementos para seleccionar");
   }
}

function validateEmail(varEmail) 
{
   patron =/\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
   return patron.test(varEmail);
}

function ExportEntity(system, entity)
{
	document.getElementById('frameExport').src = "UIProviders/TitanFL/ExportEntity.aspx?System=" + system + "&Entity=" + entity;
}