﻿
var handleSuccessLoadEntityDataDetails = function (o) {
    DisplayLoading(false);
    var div = document.getElementById('entityDetailContainer');
    if (o.responseText !== undefined) {
        div.innerHTML = o.responseText;
    }
}

var handleSuccessGetViewsByCluster = function (o) {
    DisplayLoading(false);
    var div = document.getElementById('viewsListContainer');
    if (o.responseText !== undefined) {
        div.innerHTML = o.responseText;
    }
}

var handleSuccessGetViewDataForm = function (o) {
    DisplayLoading(false);
    var div = document.getElementById('clusterDataFormContainer');
    var response = o.responseText;
    if (response !== undefined) {
        if (response.search(/ErrorTitanFL:/) == -1)
            div.innerHTML = response;
        else {
            ShowError("La vista probablemente no ha sido generada");
            div.innerHTML = "";
        }
    }
}

var handleSuccessValidatePassword = function (o) {
    DisableDisplayLoading();
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (response == "true") {
            oPassword.value = document.getElementById('pswNew').value;
            YAHOO.OnData.TitanFL.container.dlgPassword.hide();
        }
        else if (response == "false") {
            ShowError("La contraseña actual ingresada es incorrecta");
        }
    }
};

var handleSuccessGetEntityReferenceData = function (o) {
    DisableDisplayLoading();
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        CleanSelected('lstSelectedNew');
        CleanSelected('lstEntityInstanceDataNew');

        if (response != '') {
            fillSelect(response, "lstEntityInstanceDataNew");
        }

        if (o.argument[0] != undefined && o.argument[0] != "") {
            fillSelect(o.argument[0], "lstSelectedNew");

            var listClusters = document.getElementById('lstSelectedNew');
            var listBusinessEntities = document.getElementById('lstEntityInstanceDataNew');

            if (listClusters == null || listBusinessEntities == null)
                ShowError('No existen los controles');

            for (var i = 0; i < listClusters.length; i++) {
                for (var j = 0; j < listBusinessEntities.length; j++) {
                    if (listClusters.options[i].value == listBusinessEntities.options[j].value) {
                        listClusters.options[i].text = listBusinessEntities.options[j].text;
                        listBusinessEntities.remove(j);
                    }
                }
            }
        }

        YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.setHeader("Seleccionar valores");
        YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.show();
    }
};

var handleSuccessSaveViewData = function (o) {
    DisableDisplayLoading();
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (response.search(/ErrorTitanFL:/) != -1) {
            ShowError("<strong>" + response + "</strong>");
            document.getElementById("btnSave").disabled = false;
        }
        else {
            var info = response.split('|');

            // Primera version
            //window.location.replace('BusinessEntityDataDetails.aspx?ViewName=' + info[0] + '&EntityGuid=' + info[1]);

            // Segunda version Module
            if (relatedEntityGuid == "")
                window.location.replace('BusinessEntityDataDetails.aspx?ViewName=' + info[0] + '&EntityGuid=' + info[1] + '&moduleName=EntitiesSystem');
            else
                parent.ReloadPage();
        }
    }
}

var handleSuccessDeleteViewData = function (o) {
    DisableDisplayLoading();
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (response.search(/ORA-02292:/) != -1) {
            ShowError("El dato no se puede eliminar porque está siendo referenciado por otra entidad");
            document.getElementById("btnSave").disabled = false;
        }
        else if (response != '') {
            ShowError(response);
            document.getElementById("btnSave").disabled = false;
        }

        else {
            GetViewDataForm(args1[0]);
        }
    }
}

var handleSuccessGetSystemEntityReferentialValues = function (o) {
    DisableDisplayLoading();
    response = o.responseText;
    if (response !== undefined) {
        response = response.split("<!")[0];

        if (response != '') {
            document.getElementById("divSelectOutofRange").innerHTML = response;
        }
    }
};

var callbackGetSystemEntityReferentialValues =
{
    success: handleSuccessGetSystemEntityReferentialValues,
    failure: handleFailure
};

var callbackGetViewsByCluster =
{
    success: handleSuccessGetViewsByCluster,
    failure: handleFailure,
    argument: ['GetViewsByCluster']
};

var callbackGetViewDataForm =
{
    success: handleSuccessGetViewDataForm,
    failure: handleFailure,
    argument: ['GetViewDataForm']
};

var callbackLoadEntityDataDetails =
{
    success: handleSuccessLoadEntityDataDetails,
    failure: handleFailure,
    argument: ['LoadEntityDataDetails']
};

var callbackValidatePassword =
{
    success: handleSuccessValidatePassword,
    failure: handleFailure
};

var args = new Array();

var callbackGetEntityReferenceData =
{
    success: handleSuccessGetEntityReferenceData,
    failure: handleFailure,
    argument: args
};

var callbackSaveViewData =
{
    success: handleSuccessSaveViewData,
    failure: handleFailure
};

var args1 = new Array();
var callbackDeleteViewData =
{
    success: handleSuccessDeleteViewData,
    failure: handleFailure,
    argument: args1
};

function GetViewDataForm(view) {
    if (view != "")
        makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetViewDataForm&ViewName=" + view, callbackGetViewDataForm);
    else {
        var div = document.getElementById('clusterDataFormContainer');
        div.innerHTML = '';
    }
}

function GetViewsByCluster(cluster) {
    if (cluster != "")
        makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetViewsByCluster&ClusterName=" + cluster, callbackGetViewsByCluster);
    else {
        var div = document.getElementById('viewsListContainer');
        div.innerHTML = '';
    }
}

function LoadViewUpdateForm(guid, view) // LoadSystemEntityUpdateForm: DataManagerUIProvider, Ln 442
{
    window.location.replace('BusinessEntityDataDetails.aspx?ViewName=' + view + '&EntityGuid=' + guid);
    //makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetSystemEntityUpdateForm&Guid=" + guid + "&System="+system+"&Entity="+systemEntity+"&CurrentPage="+currentPage+"&CurrentSortBy="+currentSortBy+"&CurrentSortOrder="+currentSortOrder, callbackLoadSystemEntityUpdateForm);
}

function LoadViewInsertForm(view) // LoadSystemEntityInsertForm: DataManagerUIProvider, Ln 448
{
    //document.getElementById('entityFormTitle').innerHTML = "<h3>"+system+"."+systemEntity+"</h3>";
    //makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetSystemEntityInsertForm&System="+system+"&Entity="+systemEntity+"&Page="+page+"&SortBy="+sortBy, callbackLoadSystemEntityInsertForm);
    window.location.replace('BusinessEntityDataDetails.aspx?ViewName=' + view);
}

function DeleteViewData(guid, view) // DeleteEntityData: DataManagerUIProvider, Ln 304
{
    if (confirm("¿Esta segudo de eliminar el registro?")) {
        args1[0] = view;
        makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=DeleteViewData&ViewName=" + view + "&Guid=" + guid, callbackDeleteViewData);
    }
}

function LoadViewForm(view, page, sortBy, sortOrder, searchCriteria, form) // LoadSystemEntityForm: DataManagerUIProvider, Ln 453
{
    makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetViewDataForm&ViewName=" + view + "&Page=" + page + "&SortBy=" + sortBy + "&SortOrder=" + sortOrder + "&SearchCriteria=" + searchCriteria, callbackGetViewDataForm);
}

// BEGIN

function FillControlsDate() {
    var form = document.forms[0];
    var today = new Date();

    for (i = 0; i < form.elements.length; i++) {

        if (form.elements[i].getAttribute("TypeDate") == 'true') {
            FillDateYears(form.elements[i].id + "Year");
            FillDateMonth(form.elements[i].id + "Month");
            document.getElementById(form.elements[i].id + "Year").value = today.getFullYear();

            var monthTemp = parseInt(today.getMonth()) + 1;
            if (monthTemp < 10) {
                monthTemp = "0" + monthTemp;
            }

            document.getElementById(form.elements[i].id + "Month").value = monthTemp;
            FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);

            var dayTemp = today.getDate();
            if (dayTemp < 10) {
                dayTemp = "0" + dayTemp;
            }

            document.getElementById(form.elements[i].id + "Day").value = dayTemp;
            document.getElementById(form.elements[i].id).value = today.getFullYear() + "/" + document.getElementById(form.elements[i].id + "Month").value + "/" + today.getDate();
        }
    }
}

function FillDateYears(listYearId) {
    // Esta informacion debe traerla del WebConfig
    var today = new Date();
    var startYear = startYearConfig;
    var endYear = endYearConfig;
    var mylistYear = document.getElementById(listYearId);
    var elements = "-1|- Año -,";

    if (mylistYear.length == 0) {
        for (var i = endYear; i >= startYear; i--) {
            elements += i + "|" + i + ",";
        }
    }

    if (elements.length != 0)
        elements = elements.substring(0, elements.length - 1);

    fillSelectDate(elements, listYearId);
}

function fillSelectDate(stringValues, idControl) {
    var optionsList = stringValues.split(",");
    var elSel = null;
    var item = null;

    if (document.getElementById(idControl) != null)
        elSel = document.getElementById(idControl);

    if (elSel != null) {
        for (var i = 0; i < optionsList.length; i++) {
            item = optionsList[i].split("|");
            var elOptNew = document.createElement('option');
            elOptNew.value = item[0];
            elOptNew.text = item[1];

            try {
                elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
            }
            catch (ex) {
                elSel.add(elOptNew); // IE only
            }
        }
    }
}

function FillDateMonth(listMonthId) {
    var elements = "";

    elements = "-1|- Mes -,01|Enero,02|Febrero,03|Marzo,04|Abril,05|Mayo,06|Junio,07|Julio,08|Agosto,09|Septiembre,10|Octubre,11|Noviembre,12|Diciembre";
    fillSelectDate(elements, listMonthId);
}

function FillDateDays(listYearId, listMonthId, listDayId, dateCtrlId) {
    var mylistYear = document.getElementById(listYearId);
    var mylistMonth = document.getElementById(listMonthId);

    var year = mylistYear.options[mylistYear.selectedIndex].value;
    var month = mylistMonth.options[mylistMonth.selectedIndex].value;

    if (year == -1) {
        document.getElementById(listMonthId).value = '-1';
        month = '-1';
    }

    CleanSelectedDateDays(listDayId);

    var sizeDays = 0;

    switch (month) {
        case '01':
        case '03':
        case '05':
        case '07':
        case '08':
        case '10':
        case '12':
            sizeDays = 31;
            break;
        case '04':
        case '06':
        case '09':
        case '11':
            sizeDays = 30;
            break;
        case '02':
            if ((year % 4 == 0) || (year % 100 == 0) || (year % 400 == 0))
                sizeDays = 29;
            else
                sizeDays = 28;
            break;
        case '-1':
            sizeDays = 0;
            break;
    }

    var elements = "-1|- Día -,";

    for (var i = 1; i <= sizeDays; i++) {
        if (i < 10)
            elements += "0" + i + "|0" + i + ",";
        else
            elements += i + "|" + i + ",";
    }

    if (elements.length != 0)
        elements = elements.substring(0, elements.length - 1);

    fillSelectDate(elements, listDayId);

    if (document.getElementById(dateCtrlId) != undefined) {
        document.getElementById(dateCtrlId).value = year + "/" + month + "/" + "-1";
    }
}

function FillControlHidden(dateCtrlId) {
    var mylistYear = document.getElementById(dateCtrlId + "Year");
    var mylistMonth = document.getElementById(dateCtrlId + "Month");
    var mylistDay = document.getElementById(dateCtrlId + "Day");

    var year = mylistYear.options[mylistYear.selectedIndex].value;
    var month = mylistMonth.options[mylistMonth.selectedIndex].value;
    var day = mylistDay.options[mylistDay.selectedIndex].value;

    if (document.getElementById(dateCtrlId) != undefined)
        document.getElementById(dateCtrlId).value = year + "/" + month + "/" + day;
}

function CleanSelectedDateDays(control) {
    for (var i = (document.getElementById(control).length - 1); i != -1; i--) {
        document.getElementById(control).remove(0);
    }
}

var oPassword = null;
var guidPassword = "";
var viewPassword = "";
var propertyNamePassword = "";

function ManagePassword(o, guid, propertyName) {
    oPassword = o;
    guidPassword = guid;
    viewPassword = viewName;
    propertyNamePassword = propertyName;
    document.getElementById('pswCurrent').value = "";
    document.getElementById('pswNew').value = "";
    document.getElementById('pswNewConfirm').value = "";

    if (guid == '')
        document.getElementById('pswCurrent').disabled = true;
    else
        document.getElementById('pswCurrent').disabled = false;

    YAHOO.OnData.TitanFL.container.dlgPassword.setHeader("Contraseña");
    YAHOO.OnData.TitanFL.container.dlgPassword.show();
}

function SavePassword() {
    var passwordSend = "";

    if (document.getElementById('pswNew').value != document.getElementById('pswNewConfirm').value) {
        ShowError("La nueva contraseña y su confirmación son diferentes");
    }
    else {
        if (guidPassword == '') {
            oPassword.value = document.getElementById('pswNew').value;
            YAHOO.OnData.TitanFL.container.dlgPassword.hide();
        }
        else {
            passwordSend = document.getElementById('pswCurrent').value;
            makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=ValidatePassword&Guid=" + guidPassword + "&ViewName=" + viewPassword + "&PasswordSend=" + passwordSend + "&PropertyName=" + propertyNamePassword, callbackValidatePassword);
        }
    }
}

function GetEntityReferenceData(o, systemEntity, nameProperty) {
    var elements = "";
    args[1] = o.id;

    for (var i = 0; i < document.getElementById(o.id).length; i++) {
        elements += document.getElementById(o.id).options[i].value + "|" + document.getElementById(o.id).options[i].text + ",";
    }

    if (elements.length != 0)
        elements = elements.substring(0, elements.length - 1);

    args[0] = elements;
    makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetEntityReferenceData&PropertyName=" + nameProperty + "&Entity=" + systemEntity, callbackGetEntityReferenceData);
}

function CleanSelected(control) {
    var elements = "";
    for (var i = (document.getElementById(control).length - 1); i != -1; i--) {
        elements += document.getElementById(control).options[0].value + "|" + document.getElementById(control).options[0].text + ",";
        document.getElementById(control).remove(0);
    }

    if (elements.length != 0)
        elements = elements.substring(0, elements.length - 1);

    return elements;
}

function fillSelect(stringValues, idControl) {
    var optionsList = stringValues.split(",");
    var elSel = null;
    var item = null;

    if (document.getElementById(idControl) != null)
        elSel = document.getElementById(idControl);

    if (elSel != null) {
        for (var i = 0; i < optionsList.length; i++) {
            item = optionsList[i].split("|");
            var elOptNew = document.createElement('option');
            elOptNew.value = item[0];
            elOptNew.text = item[1];

            try {
                elSel.add(elOptNew, null); // standards compliant; doesn't work in IE
            }
            catch (ex) {
                elSel.add(elOptNew); // IE only
            }
        }
    }
}

function AllEntityInstanceToSelected(type) {
    if (document.getElementById('lstEntityInstanceData' + type).length != 0)
        fillSelect(CleanSelected('lstEntityInstanceData' + type), 'lstSelected' + type);
}

function AllSelectedToEntityInstance(type) {
    if (document.getElementById('lstSelected' + type).length != 0)
        fillSelect(CleanSelected('lstSelected' + type), 'lstEntityInstanceData' + type);
}

function SomeSelectedToEntityInstance(type) {
    if (document.getElementById('lstSelected' + type).length != 0)
        ExchangeOptions('lstSelected' + type, 'lstEntityInstanceData' + type);
}

function SomeEntityInstanceToSelected(type) {
    if (document.getElementById('lstEntityInstanceData' + type).length != 0)
        ExchangeOptions('lstEntityInstanceData' + type, 'lstSelected' + type);
}

function ExchangeOptions(controlOld, controlNew) {
    var selected = document.getElementById(controlOld).selectedIndex;
    var vlrOption = "";
    var vlrText = "";

    if (selected != -1) {
        vlrOption = document.getElementById(controlOld).options[selected].value;
        vlrText = document.getElementById(controlOld).options[selected].text;

        document.getElementById(controlOld).remove(selected);

        var elOptNew = document.createElement('option');
        elOptNew.text = vlrText;
        elOptNew.value = vlrOption;

        try {
            document.getElementById(controlNew).add(elOptNew, null); // standards compliant; doesn't work in IE
        }
        catch (ex) {
            document.getElementById(controlNew).add(elOptNew); // IE only
        }
    }
}

function GetSelectedValues() {
    var mylist = null;

    if (document.getElementById("lstSelectedNew") != null)
        mylist = document.getElementById("lstSelectedNew");

    if (document.getElementById(args[1]) != null)
        myCtrl = document.getElementById(args[1]);

    CleanSelected(args[1]);
    if (mylist.length != 0) {
        var elements = "";
        for (var i = 0; i < mylist.length; i++) {
            var elOptNew = document.createElement('option');
            elOptNew.text = mylist.options[i].text;
            elOptNew.value = mylist.options[i].value;

            try {
                myCtrl.add(elOptNew, null); // standards compliant; doesn't work in IE
            }
            catch (ex) {
                myCtrl.add(elOptNew); // IE only
            }
        }
    }
    //document.getElementById(args[1]).value = elements;
    return 1;
}

function FillControlsUpdateDate() {
    var form = document.forms[0];

    for (i = 0; i < form.elements.length; i++) {
        if (form.elements[i].getAttribute("TypeDate") == 'true') {
            var dateString = document.getElementById(form.elements[i].id).value;

            if (dateString != "") {
                if (dateString.substring(0, 10) != '01/01/1900') {
                    FillDateYears(form.elements[i].id + "Year");
                    FillDateMonth(form.elements[i].id + "Month");
                    document.getElementById(form.elements[i].id + "Year").value = dateString.substring(6, 10);
                    document.getElementById(form.elements[i].id + "Month").value = dateString.substring(3, 5);
                    FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);
                    document.getElementById(form.elements[i].id + "Day").value = dateString.substring(0, 2);
                    document.getElementById(form.elements[i].id).value = dateString;
                }
                else {
                    FillDateYears(form.elements[i].id + "Year");
                    FillDateMonth(form.elements[i].id + "Month");
                    document.getElementById(form.elements[i].id + "Year").value = '-1';
                    document.getElementById(form.elements[i].id + "Month").value = '-1';
                    FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);
                    document.getElementById(form.elements[i].id + "Day").value = '-1';
                    document.getElementById(form.elements[i].id).value = "-1/-1/-1";
                }
            }
            else {
                FillDateYears(form.elements[i].id + "Year");
                FillDateMonth(form.elements[i].id + "Month");
                document.getElementById(form.elements[i].id + "Year").value = '-1';
                document.getElementById(form.elements[i].id + "Month").value = '-1';
                FillDateDays(form.elements[i].id + "Year", form.elements[i].id + "Month", form.elements[i].id + "Day", form.elements[i].id);
                document.getElementById(form.elements[i].id + "Day").value = '-1';
                document.getElementById(form.elements[i].id).value = "-1/-1/-1";
            }
        }
    }
}

function onlyDecimals(varNumber) {
    patron = /^\d*\,?\d*$/;
    return patron.test(varNumber);
}

function validateEmail(varEmail) {
    patron = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
    return patron.test(varEmail);
}

function Cancel(viewNameCancel) {
    if (relatedEntityGuid == "")
        window.location.replace('BusinessEntitiesData.aspx');
    else
        parent.ReloadPage();
}

function SaveViewData(form, guid) {
    if (guid == null)
        guid = "";

    var formIsValid = true;
    var entityData = new String("");
    var entityProperties = new String("");
    var propertyValue = new String("");
    var validatorsMessages = new String("");
    for (var i = 0; i < form.elements.length; i++) {
        if (form.elements[i].getAttribute("EntityProperty") == "true") {
            propertyValue = "";

            /**********************************/
            /* Tipo Booleano                  */
            if (form.elements[i].getAttribute("PropertyType") == "4") {
                if (form.elements[i].checked)
                    propertyValue = "1";
                else
                    propertyValue = "0";
            }
            /**********************************/
            /* Tipo Number                    */
            else if (form.elements[i].getAttribute("PropertyType") == "2") {
                propertyValue = "0";

                if (form.elements[i].value != "")
                    propertyValue = form.elements[i].value;
            }
            /**********************************/
            /* Tipo Decimal y Currency        */
            else if (form.elements[i].getAttribute("PropertyType") == "3" || form.elements[i].getAttribute("PropertyType") == "10") {
                propertyValue = "0";

                if (form.elements[i].value != "") {
                    if (onlyDecimals(form.elements[i].value)) {
                        propertyValue = form.elements[i].value;
                    }
                    else {
                        validatorsMessages += "- El formato escrito para '" + form.elements[i].getAttribute("DisplayName") + "' no es correcto<br/>";
                        formIsValid = false;
                    }
                }
            }
            /**********************************/
            /* Tipo Correo electronico        */
            else if (form.elements[i].getAttribute("PropertyType") == "13") {
                if (form.elements[i].value != "") {
                    if (validateEmail(form.elements[i].value)) {
                        if (form.elements[i].value.search("'") == -1) {
                            propertyValue = form.elements[i].value;
                        }
                        else {
                            validatorsMessages += "- El correo electrónico escrito para '" + form.elements[i].getAttribute("DisplayName") + "' tiene una comilla simple (\'). Este caracter no es permitido<br/>";
                            formIsValid = false;
                        }
                    }
                    else {
                        validatorsMessages += "- El formato de correo electrónico escrito para '" + form.elements[i].getAttribute("DisplayName") + "' no es correcto<br/>";
                        formIsValid = false;
                    }
                }
            }
            /**********************************/
            /* Tipo LongString                */
            else if (form.elements[i].getAttribute("PropertyType") == "1") {
                if (form.elements[i].value.length > form.elements[i].getAttribute("LengthTextArea")) {
                    validatorsMessages = validatorsMessages + "- La longitud de '" + form.elements[i].getAttribute("DisplayName") + "' tiene que ser menor o igual a " + form.elements[i].getAttribute("LengthTextArea") + "<br/>";
                    formIsValid = false;
                }
                else {
                    if (form.elements[i].value.search("'") == -1) {
                        propertyValue = form.elements[i].value;
                    }
                    else {
                        validatorsMessages += "- El campo '" + form.elements[i].getAttribute("DisplayName") + "' tiene una comilla simple (\'). Este caracter no es permitido<br/>";
                        formIsValid = false;
                    }
                }
            }
            /**********************************/
            /* Tipo DateTime                  */
            else if (form.elements[i].getAttribute("PropertyType") == "7") {
                if (form.elements[i].value != "-1/-1/-1") {
                    var tempMonthValidate = form.elements[i].value.substring(5, 7);
                    var tempDayValidate = form.elements[i].value.substring(8, 10);

                    propertyValue = form.elements[i].value;

                    if (tempMonthValidate == '-1') {
                        validatorsMessages = validatorsMessages + "- El mes de '" + form.elements[i].getAttribute("DisplayName") + "' tiene que ser seleccionado<br/>";
                        formIsValid = false;
                    }
                    else {
                        if (tempDayValidate == '-1') {
                            validatorsMessages = validatorsMessages + "- El día de '" + form.elements[i].getAttribute("DisplayName") + "' tiene que ser seleccionado<br/>";
                            formIsValid = false;
                        }
                    }
                }
                else {
                    if ((form.elements[i].getAttribute("Required") == "true")) {
                        validatorsMessages = validatorsMessages + "- El campo " + form.elements[i].getAttribute("DisplayName") + " es requerido<br/>";
                        formIsValid = false;
                    }
                    propertyValue = form.elements[i].value;
                }
            }
            /**********************************/
            /* Tipo Entity y varios valores   */
            else if ((form.elements[i].getAttribute("PropertyType") == "12") && (form.elements[i].getAttribute("PropertySourceEntitySelectionType") == "1")) {
                var elements = "";
                for (var j = 0; j < form.elements[i].length; j++) {
                    elements += "|" + form.elements[i].options[j].value + "|,";
                }

                if (elements.length != 0)
                    elements = elements.substring(0, elements.length - 1);

                propertyValue = elements;
            }
            /**********************************/
            /* Tipo Entity y un sólo valor    */
            else if ((form.elements[i].getAttribute("PropertyType") == "12") && (form.elements[i].getAttribute("PropertySourceEntitySelectionType") == "0")) {
                if (form.elements[i].value != '-1')
                    propertyValue = form.elements[i].value;
            }
            /**********************************/
            /* Cadena de caracteres           */
            /* Password                       */
            else if (form.elements[i].getAttribute("PropertyType") == "0" || form.elements[i].getAttribute("PropertyType") == "11") {
                if (form.elements[i].value.search("'") == -1) {
                    propertyValue = form.elements[i].value;
                }
                else {
                    validatorsMessages += "- El campo '" + form.elements[i].getAttribute("DisplayName") + "' tiene una comilla simple (\'). Este caracter no es permitido<br/>";
                    formIsValid = false;
                }
            }
            /**********************************/
            /* Los otros tipos                */
            else
                propertyValue = form.elements[i].value;

            if ((form.elements[i].getAttribute("Required") == "true") && (propertyValue == "")) {
                validatorsMessages = validatorsMessages + "- El campo " + form.elements[i].getAttribute("DisplayName") + " es requerido<br/>";
                formIsValid = false;
            }
            entityProperties = entityProperties + form.elements[i].getAttribute("Name") + "|" + form.elements[i].getAttribute("EntityName") + "|" + form.elements[i].getAttribute("PropertyType") + ",";
            entityData = entityData + form.elements[i].id + "=" + propertyValue + "&";
        }
    }

    if (!formIsValid) {
        ShowError(validatorsMessages);
        return;
    }

    entityProperties = entityProperties.substring(0, entityProperties.length - 1);
    entityData = entityData.substring(0, entityData.length - 1);

    document.getElementById("btnSave").disabled = true;

    var postData = ("MethodToCall=SaveViewData&ViewName=" + viewName + "&Guid=" + guid + "&RelatedEntityGuid=" + relatedEntityGuid + "&RelatedViewEntity=" + relatedViewEntity + "&RelatedViewEntityProperty=" + relatedViewEntityProperty + "&EntityProperties=" + entityProperties + "&" + entityData);
    makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", postData, callbackSaveViewData);
}

YAHOO.namespace('OnData.TitanFL.container');

function init() {
    DisableDisplayLoading();

    /*************************************************************************
    * Creación del formulario para cuando hay mas una cantidad maxima de datos
    **************************************************************************/
    var handleCancelOutOfRange = function (e) {
        this.hide();
    }

    var handleOKOutOfRange = function (e) {
        SaveOutOfRange();
    }

    YAHOO.OnData.TitanFL.container.dlgOutOfRange = new YAHOO.widget.SimpleDialog("dlgManageOutOfRangeInstances", { visible: false, width: "400px", fixedcenter: true, modal: true, draggable: true });
    YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("buttons", [
																	{ text: "Seleccionar", handler: handleOKOutOfRange, isDefault: true },
																	{ text: "Cancelar", handler: handleCancelOutOfRange }
																]);

    listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelOutOfRange, scope: YAHOO.OnData.TitanFL.container.dlgOutOfRange, correctScope: true });
    YAHOO.OnData.TitanFL.container.dlgOutOfRange.cfg.queueProperty("keylisteners", listeners);

    YAHOO.OnData.TitanFL.container.dlgOutOfRange.render(document.body);

    /*************************************************************************
    * FIN del formulario
    *************************************************************************/

    /*****************************************************************
    * Creación del formulario para editar Password
    *****************************************************************/
    var handleCancelPassword = function (e) {
        this.hide();
    }

    var handleOKPassword = function (e) {
        SavePassword();
    }

    YAHOO.OnData.TitanFL.container.dlgPassword = new YAHOO.widget.SimpleDialog("dlgPassword", { visible: false, width: "400px", fixedcenter: true, modal: true, draggable: true });
    YAHOO.OnData.TitanFL.container.dlgPassword.cfg.queueProperty("buttons", [
																	{ text: "Guardar", handler: handleOKPassword, isDefault: true },
																	{ text: "Cancelar", handler: handleCancelPassword }
																]);

    listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelPassword, scope: YAHOO.OnData.TitanFL.container.dlgPassword, correctScope: true });
    YAHOO.OnData.TitanFL.container.dlgPassword.cfg.queueProperty("keylisteners", listeners);

    YAHOO.OnData.TitanFL.container.dlgPassword.render(document.body);

    /*****************************************************************
    * Creación del formulario para seleccionar valores de una entidad
    *****************************************************************/
    var handleCancelEntityReferenceData = function (e) {
        this.hide();
    }

    var handleOKEntityReferenceData = function (e) {
        if (GetSelectedValues() == 1)
            this.hide();
    }

    YAHOO.OnData.TitanFL.container.dlgEntityReferenceData = new YAHOO.widget.SimpleDialog("dlgEntityReferenceData", { visible: false, width: "585px", fixedcenter: true, modal: true, draggable: true });
    YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.cfg.queueProperty("buttons", [
																	{ text: "Seleccionar", handler: handleOKEntityReferenceData, isDefault: true },
																	{ text: "Cancelar", handler: handleCancelEntityReferenceData }
																]);

    listeners = new YAHOO.util.KeyListener(document, { keys: 27 }, { fn: handleCancelEntityReferenceData, scope: YAHOO.OnData.TitanFL.container.dlgEntityReferenceData, correctScope: true });
    YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.cfg.queueProperty("keylisteners", listeners);

    YAHOO.OnData.TitanFL.container.dlgEntityReferenceData.render(document.body);

    /******************************************************************/

    try {
        if (entityGuid == '') {
            FillControlsDate();
        }
        else {
            FillControlsUpdateDate();
        }
    }
    catch (err) { }
}

YAHOO.util.Event.addListener(window, "load", init, "", true);

var systemOutOfRangeGral = "";
var entityOutOfRangeGral = "";
var propertyOutOfRangeGral = "";
var oOutOfRangeGral = null;
var oHiddenOutOfRangeGral = null;

function GetManageOutOfRangeInstances(systemOutOfRange, entityOutOfRange, propertyOutOfRange, oOutOfRange, oHiddenOutOfRange) {
    systemOutOfRangeGral = systemOutOfRange;
    entityOutOfRangeGral = entityOutOfRange;
    propertyOutOfRangeGral = propertyOutOfRange;
    oOutOfRangeGral = oOutOfRange;
    oHiddenOutOfRangeGral = oHiddenOutOfRange;

    if (document.getElementById("txtFindValue") != null)
        document.getElementById("txtFindValue").value = '';

    document.getElementById("divSelectOutofRange").innerHTML = "";

    YAHOO.OnData.TitanFL.container.dlgOutOfRange.setHeader("Buscar");
    YAHOO.OnData.TitanFL.container.dlgOutOfRange.show();
}

function SaveOutOfRange() {
    var mylist = null;

    if (document.getElementById("lstOutOfRange") != null) {
        mylist = document.getElementById("lstOutOfRange");

        if (mylist.length != 0) {
            if (mylist.selectedIndex != -1) {
                oOutOfRangeGral.value = mylist.options[mylist.selectedIndex].text;
                oHiddenOutOfRangeGral.value = mylist.options[mylist.selectedIndex].value;
                YAHOO.OnData.TitanFL.container.dlgOutOfRange.hide();
            }
            else {
                alert("Debe seleccionar un elemento");
            }
        }
        else {
            alert("No hay elementos para seleccionar");
        }
    }
    else {
        alert("No hay elementos para seleccionar");
    }
}

function findValue() {
    var vlrFindValue = "";

    if (document.getElementById("txtFindValue") != null && document.getElementById("txtFindValue").value != '') {
        vlrFindValue = "&FindValue=" + document.getElementById("txtFindValue").value;
        document.getElementById("divSelectOutofRange").innerHTML = "Buscando...";
        makeRequest("../../UIProviders/EntitySystem/BusinessEntitiesDataUIProvider.aspx", "MethodToCall=GetSystemEntityReferentialValues&System=" + systemOutOfRangeGral + "&Entity=" + entityOutOfRangeGral + "&PropertyName=" + propertyOutOfRangeGral + vlrFindValue, callbackGetSystemEntityReferentialValues);
    }
    else {
        alert("Digite un patrón de búsqueda");
    }
}