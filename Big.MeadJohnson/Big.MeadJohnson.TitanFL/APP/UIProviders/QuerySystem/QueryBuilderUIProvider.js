﻿//===============================================================================
// QueryBuilderUIProvider.js
// Author : Wveimar López M.    (wlopez@dmgcolombia.com)
//          Jorge A. Vélez G.   (jvelez@dmgcolombia.com)
// Last update: 05 enero 2007
//===============================================================================
// Copyright © Data Marketing Group.  All rights reserved.
//===============================================================================

//Muestra el tab de consulta en la página
function ShowQuery()
{
   var imgQuery = document.getElementById("imgQuery");
   imgQuery.src = "../../Images/consulta1.jpg";
   
   var imgResult = document.getElementById("imgResult");
   imgResult.src = "../../Images/resultado2.jpg";
   
   var imgQueries = document.getElementById("imgQueries");
   imgQueries.src = "../../Images/consultas2.jpg";
   
   var imgImportQuery = document.getElementById("imgImportQuery");
   imgImportQuery.src = "../../Images/importarConsulta2.jpg";
   
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");
   var divQs = document.getElementById("divQueries");
   var divIQ = document.getElementById("divImportQuery"); 

   divQ.style.display = "block";   
   divR.style.display = "none"; 
   divQs.style.display = "none"; 
   divIQ.style.display = "none";    
}

//Muestra el tab de resultado en la página
function ShowResult()
{
   var imgQuery = document.getElementById("imgQuery");
   imgQuery.src = "../../Images/consulta2.jpg";
   
   var imgResult = document.getElementById("imgResult");
   imgResult.src = "../../Images/resultado1.jpg";
   
   var imgQueries = document.getElementById("imgQueries");
   imgQueries.src = "../../Images/consultas2.jpg";
   
   var imgImportQuery = document.getElementById("imgImportQuery");
   imgImportQuery.src = "../../Images/importarConsulta2.jpg";
   
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");
   var divQs = document.getElementById("divQueries");
   var divIQ = document.getElementById("divImportQuery"); 
   
   divQ.style.display = "none";   
   divR.style.display = "block"; 
   divQs.style.display = "none";
   divIQ.style.display = "none";
}

//Muestra el botón de cerrar
function ShowCloseButton()
{
   var button = document.getElementById("btnCerrar");
   button.style.visibility = "visible";
}

//Muestra el tab de consultas en la página
function ShowQueries()
{
   var imgQuery = document.getElementById("imgQuery");
   imgQuery.src = "../../Images/consulta2.jpg";
   
   var imgResult = document.getElementById("imgResult");
   imgResult.src = "../../Images/resultado2.jpg";
   
   var imgQueries = document.getElementById("imgQueries");
   imgQueries.src = "../../Images/consultas1.jpg";
   
   var imgImportQuery = document.getElementById("imgImportQuery");
   imgImportQuery.src = "../../Images/importarConsulta2.jpg";
   
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");   
   var divQs = document.getElementById("divQueries");  
   var divIQ = document.getElementById("divImportQuery"); 

   divQ.style.display = "none";   
   divR.style.display = "none";
   divQs.style.display = "block";
   divIQ.style.display = "none";
}

//Muestra el tab de importar consultas en la página
function ShowImportQuery()
{
   var imgQuery = document.getElementById("imgQuery");
   imgQuery.src = "../../Images/consulta2.jpg";
   
   var imgResult = document.getElementById("imgResult");
   imgResult.src = "../../Images/resultado2.jpg";
   
   var imgQueries = document.getElementById("imgQueries");
   imgQueries.src = "../../Images/consultas2.jpg";
   
   var imgImportQuery = document.getElementById("imgImportQuery");
   imgImportQuery.src = "../../Images/importarConsulta1.jpg";
   
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");   
   var divQs = document.getElementById("divQueries"); 
   var divIQ = document.getElementById("divImportQuery"); 

   divQ.style.display = "none";   
   divR.style.display = "none";
   divQs.style.display = "none";
   divIQ.style.display = "block";
}

//Limpia el contenido actual de los divs y de los hidden que llevan rastro de los valores
//seleccionados en la pantalla
function Clean()
{
   var div = document.getElementById(divRelatedEntities);
   div.innerHTML = "";
   
   div = document.getElementById(divPropertiesToSelect);
   div.innerHTML = "";
   
   div = document.getElementById(divSearchableProps);
   div.innerHTML = "";      
   
   div = document.getElementById(divSelectedProps);
   div.innerHTML = "";  
   
   div = document.getElementById(divFilters);
   div.innerHTML = "";
   
   var hidden = document.getElementById(hdnSelectedEntities);
   hidden.value = "";
   
   hidden = document.getElementById(hdnProperties);
   hidden.value = "";
   
   hidden = document.getElementById(hdnSelectedFilters);
   hidden.value = "";
   
   hidden = document.getElementById(hdnEntities);
   hidden.value = "";
   
   hidden = document.getElementById(hdnRelatedEntities);
   hidden.value = "";
   
   hidden = document.getElementById(hdnPropertiesToSelect);
   hidden.value = "";
   
   hidden = document.getElementById(hdnSelectedProps);
   hidden.value = "";
   
   hidden = document.getElementById(hdnSearchableProps);
   hidden.value = "";
   
   hidden = document.getElementById(hdnFilters);
   hidden.value = "";
   
   hidden = document.getElementById(hdnEntitiesList);
   hidden.value = "";                  
   
   hidden = document.getElementById(hdnPropertiesList);
   hidden.value = "";
   
   hidden = document.getElementById(hdnPropertiesListDisplay);
   hidden.value = "";
   
   hidden = document.getElementById(hdnOrdersList);
   hidden.value = "";
   
   hidden = document.getElementById(hdnFiltersList);
   hidden.value = "";    
}

//Elimina de la pantalla el div dado en parámetros
function DeleteDiv(divName)
{
   var div = document.getElementById(divName);    
   if (div != null) 
   {
      //div.innerHTML = "";
      div.parentNode.removeChild(div);
      //div.style.display = "none";
   }
}

//Borra del hidden dado en parámetros los valores que existan dentro de él para la entidad
//también dada en parámetros
function DeleteHiddenValues(hiddenName, entityName)
{
   var hidden = document.getElementById(hiddenName);

   if (hidden.value != "")
   {
      var list = hidden.value.split(",");     
      hidden.value = "";
      for (var i = 0; i < list.length; i++)
      {
         if (list[i].indexOf("div_" + entityName) == -1)     
            hidden.value += list[i] + ",";       
      }
   }  
}

//GET_ENTITIES_BY_SYSTEM
var handleSuccessGetEntitiesBySystem = function(o)
{
   DisplayLoading(false);
   var div = document.getElementById(spanEntities);
   if(o.responseText != undefined)
   {
      div.innerHTML = o.responseText;
   }   
}

var callbackGetEntitiesBySystem =
{
   success:handleSuccessGetEntitiesBySystem,
   failure:handleFailure,
   argument:['GetEntitiesBySystem']      
}; 

function GetEntitiesBySystem(system)
{  
   if(system != "") 
   {
      var url = "../../UIProviders/QuerySystem/QueryBuilderUIProvider.aspx"
      var method = "MethodToCall=GetEntitiesBySystem&SystemName=" + system;
      makeRequest(url, method, callbackGetEntitiesBySystem);    

      //Cuando se cambia de sistema, se deben limpiar todos los divs y borrar todo el estado
      //actual de las variables que llevan rastro de las selecciones hechas por el usuario
      var div = document.getElementById(spanEntities);
      div.innerHTML = "";
      
      Clean();
   }      
}

//GET_RELATED_ENTITIES
var handleSuccessGetRelatedEntities = function(o)
{
   DisplayLoading(false);      
  
   var response = new String(o.responseText);
   var entitiesList = response.split(",");
   
   var div = document.getElementById(divRelatedEntities);
   div.innerHTML = "";
   Clean();
   
   var curSystem = "";
   var prevSystem = "";
   for (var i = 0; i < entitiesList.length; i++)
   {   
      var entity = entitiesList[i].split("|");
      curSystem = entity[0];
      
      var html = "";
      if (curSystem != prevSystem)
         html = "<b>" + entity[1] + "</b>";
      
      var idDiv = "div_" + entity[0] + "." + entity[2];
      var idChk = "chk_" + entity[0] + "." + entity[2];  
      html = html + "<div id='" + idDiv + "'>" +
         "<input type='checkbox' id='" + idChk + "' value='" + entity[2] +
         "' onclick='SetChecked(this);GetEntityProperties(\"" + entity[0] + "\", this.value, this.checked)'/>" + 
         entity[3] + "</div>";     
      div.innerHTML = div.innerHTML + html;
      prevSystem = curSystem;   
   }
}

var callbackGetRelatedEntities =
{
   success:handleSuccessGetRelatedEntities,
   failure:handleFailure,
   argument:['GetRelatedEntities']      
}; 

function GetRelatedEntities(system, entityName)
{     
   if (system != "" && entityName != "")
   {
      hidden = document.getElementById(hdnMainEntity);
      hidden.value = entityName;
   
      var url = "../../UIProviders/QuerySystem/QueryBuilderUIProvider.aspx";
      var method = "MethodToCall=GetRelatedEntities&System=" + system + "&EntityName=" + entityName;
      makeRequest(url, method, callbackGetRelatedEntities);
   }
   else
   {
      var div = document.getElementById(divRelatedEntities);
      div.innerHTML = "";   
   } 
}

//GET_ENTITY_PROPERTIES
var handleSuccessGetEntityProperties = function(o)
{   
   DisplayLoading(false); 
   
   var response = new String(o.responseText);      
   if (response.length == 0) return;   
   
   var propertiesList = response.split(",");   
   var nameList = propertiesList[0].split("|");
      
   var html1 = "<div id='div_" + nameList[0] + "_ToList'><div><b>" + nameList[1] + "</b></div>";
   var html2 = "";
   for (var i = 0; i < propertiesList.length; i++)
   {  
      var property = propertiesList[i].split("|");           
      
      //Adiciona el nodo al div ubicado en "Propiedades para seleccionar".  Sólo adiciona aquellas
      //propiedades que no están relacionadas con otra entidad
      //if (property[5] == "false" || property[5] == 0)
      //{
         var idChk = property[0] + "." + property[2] + "_ToList";      
         html1 = html1 + "<input type='checkbox' id='" + idChk + "' value ='" + property[2] + 
            "' display='" + property[3] + "' onclick='SetChecked(this);ShowProperty(\"" + property[0] + "\", this.value, this.checked,\"" + property[8] + "\")'/>" + 
            property[3] + "<br>";
      //}

      //Adiciona el nodo al div ubicado en "Propiedades para filtrar".  Sólo adiciona aquellas 
      //propiedades que son de búsqueda
      if (property[4] == "true" || property[4] == 1)
      {
         if (html2 == "") 
         {            
            html2 = "<div id='div_" + property[0] + "_ToSearch'><div><b>" + property[1] + "</b></div>";            
         }
         
         var idchk2 = property[0] + "." + property[2] + "_ToFilter";
         var html2 = html2 + "<input type='checkbox' id='" + idchk2 + "' value='" + property[2] + 
            "' onclick='SetChecked(this);ShowFilter(\"" + property[0] + "\", this.value, this.checked, \"" + 
            property[5] + "\", \"" + property[6] + "\", \"" + property[7] + "\", \"" +             
            property[8] + "\", \"" + property[9] + "\")'/>" + property[3] + "<br>";         
      }
   }   
   
   if (html2 != "") html2 = html2 + "</div>";       
   if (html1 != "") html1 = html1 + "</div>";   

   if (html1 != "")
   {
      var div = document.getElementById(divPropertiesToSelect);
      div.innerHTML = div.innerHTML + html1;
   }
   
   if (html2 != "")
   {
      div = document.getElementById(divSearchableProps);
      div.innerHTML = div.innerHTML + html2;   
   }
}

var callbackGetEntityProperties =
{
   success:handleSuccessGetEntityProperties,
   failure:handleFailure,
   argument:['GetEntityProperties']      
}; 

function GetEntityProperties(system, entityName, checked)
{   
   if (system != "" && entityName != "")
   {
      //Si el control que levantó el evento está seleccionado, se llama al evento que se encarga de
      //obtener las propiedades de la entidad dada en parámetros
      if (checked)
      {      
         var url = "../../UIProviders/QuerySystem/QueryBuilderUIProvider.aspx";
         var method = "MethodToCall=GetEntityProperies&System=" + system + "&EntityName=" + entityName;
         makeRequest(url, method, callbackGetEntityProperties);
         
         //Lleva el rastro de las entidades seleccionadas
         var hidden = document.getElementById(hdnSelectedEntities);                
         hidden.value = hidden.value + "div_" + system + "." + entityName + ",";
      }
      else
      {
         var hidden = document.getElementById(hdnSelectedEntities);
         var list = hidden.value.split(",");     
         //alert(hidden.value);
         //alert("div_" + system + "." + entityName + ",");
         for (var i = 0; i < list.length; i++)
         {
            if (list[i] == "div_" + system + "." + entityName)
            {
               // alert("entro");
               hidden.value = hidden.value.replace("div_" + system + "." + entityName + ",", "");
            }      
         }
         //alert(hidden.value);
         //Si el control que levantó el evento NO está seleccionado, se deben eliminar de los divs
         //aquellos nodos que están relacionados con la entidad desmarcada
         DeleteDiv("div_" + entityName + "_ToList");
         DeleteDiv("div_" + entityName + "_Show");
         DeleteDiv("div_" + entityName + "_ToSearch");       
         DeleteDiv("div_" + entityName + "_Filter");
         //var hidden = document.getElementById(hdnSelectedEntities);
         
         if (hidden.value != "")
            hidden.value = hidden.value.replace("div_" + system + "." + entityName + ",", "");
         
         DeleteHiddenValues(hdnProperties, entityName);         
         DeleteHiddenValues(hdnSelectedFilters, entityName);                    
      }
   }
}

//SHOW_PROPERTY
function ShowProperty(entityName, propertyName, checked, propertyType)
{        
   //Si el control que levantó el evento está seleccionado, se adiciona un nodo al árbol de las
   //propiedades a mostrar y otro nodo al árbol de ordenamientos
   if (checked)
   {
      var html = "";
      var div;
      var idDivMain = "div_" + entityName + "_Show";
      var divMain = document.getElementById(idDivMain);

      if (divMain == null)      
         html = "<div id='" + idDivMain + "'>";
        
      var idDiv = "div_" + entityName + "." + propertyName + "_List";
      var idTxt= "txt_" + entityName + "." + propertyName + "_List";
      var valTxt = entityName + "." + propertyName;
      var idSel = "cbo_" + entityName + "." + propertyName + "_List";
      var idSelFormat = "cbo_" + entityName + "." + propertyName + "_List_Format";
      
      html = html + "<div id='" + idDiv + "'>" +
         "<input type='text' id='" + idTxt + "' value='" + valTxt + "' readonly='readonly' style='width:30%'>" +
         "<select id='" + idSel + "' onchange='SetSelectedOption(this)'><option>----</option><option>Asc</option><option>Desc</option></select>";
         if(propertyType == 7)
            html = html + "<select id='" + idSelFormat + "' onchange='SetSelectedOption(this)'><option value='----'>----</option><option value='yyyy/mm/dd'>AAAA/MM/DD</option></select>";
         html = html + "</div>";       

      if (divMain == null)
      {
         html = html + "</div>";
         div = document.getElementById(divSelectedProps);
         div.innerHTML = div.innerHTML + html;         
      }
      else
      {
         divMain.innerHTML = divMain.innerHTML + html;         
      }     
      
      //Lleva el rastro de los divs adicionados para posteriormente poderlos borrar con facilidad 
      var hidden = document.getElementById(hdnProperties);
      hidden.value = hidden.value + idDiv + ",";           
   }  
   else
   {     
      //Si el control que levantó el evento NO está seleccionado, se elimina el nodo respectivo en el
      //árbol de las propiedades a mostrar
      //var div = document.getElementById("div_" + entityName + "." + propertyName + "_List");
      //if (div != null) div.removeNode(true);
      DeleteDiv("div_" + entityName + "." + propertyName + "_List");
      
      var hidden = document.getElementById(hdnProperties);
      var list = hidden.value.split(",");
      
      for (var i = 0; i < list.length; i++)
      {
         if (list[i] == "div_" + entityName + "." + propertyName + "_List")
            hidden.value = hidden.value.replace("div_" + entityName + "." + propertyName + "_List,", "");
      }
   } 
}

//SHOW_FILTER
function ShowFilter(entityName, propertyName, checked, isRefProperty, propertySource, 
   propertySourceDisplayProp, propertyType, system)
{
   //Si el control que levantó el evento está seleccionado, se adiciona un nodo al árbol de los filtros
   //y otro al árbol de valores
   if (checked)
   {
      var html = "";
      var div;
      var idDivMain = "div_" + entityName + "_Filter";
      var divMain = document.getElementById(idDivMain);
      
      if (divMain == null)
         html = "<div id='" + idDivMain + "'>";
         
      var idDiv = "div_" + entityName + "." + propertyName + "_Value";
      var idSel = "cbo_" + entityName + "." + propertyName + "_Value";
      var idTxtField = "txt_" + entityName + "." + propertyName + "_Value";
      var idTxtValue = "txt_" + entityName + "." + propertyName + "_Value2";
      var idTxtValueEnd = "txt_" + entityName + "." + propertyName + "_Value3";
      var idChk = "chk_" + entityName + "." + propertyName + "_Value";     
      var idCboValue = "cbo_" + entityName + "." + propertyName + "_Value2";     
      var valTxt = entityName + "." + propertyName;
      
      html = html + "<div id='" + idDiv + "'>" +
         "<input type='text' id='" + idTxtField + "' value='" + valTxt + "' readonly='readonly' style='width:30%'>";
         
      if (propertyType == "4")
      {
         html = html + "<select onchange='SetSelectedOption(this)' id='" + idSel + "'>" +
            "<option>----</option>" +
            "<option>=</option>" +
         "</select>" +
         "<select onchange='SetSelectedOption(this)' id='" + idChk + "'>" +
            "<option value='1'>Verdadero</option>" +
            "<option value='0'>Falso</option>" +
         "</select>";         
         //"<input type='checkbox' onclick='SetChecked(this)' id='" + idChk + "' />";      
      }
      else
      {
      //Aqui
        html = html + "<select TxtValueId='" + idTxtValue + "' TxtValueEndId='" + idTxtValueEnd + "' onchange='SetSelectedOption(this);SetSelectedOptionParameter(this)' id='" + idSel + "'>" +
            "<option value='----'>----</option>" +
            "<option value='='>=</option>" +
            "<option value='<>'><></option>" +
            "<option value='>'>></option>" +
            "<option value='>='>>=</option>" +
            "<option value='<'><</option>" +
            "<option value='<='><=</option>" +
            "<option value='Contenga'>Contenga</option>" +
            "<option value='No contenga'>No contenga</option>" +
            "<option value='Entre'>Entre</option>" +
            "<option value='Comience por'>Comience por</option>" +
            "<option value='Termine en'>Termine en</option>" +
         "</select>" +
         "<input type='text' readonly='readonly' id='" + idTxtValue + "' onblur='SetControlText(this)' style='width:70px'/>";
         if (isRefProperty == "1")
         {
             var idHdn = "hdn_" + entityName + "." + propertyName + "_Value";
             var idBtn = "btn_" + entityName + "." + propertyName + "_Value";
             html = html + "<input type='hidden' id='" + idHdn + "' />" +
             "<input type='button' id='" + idBtn + "' style='width:5%' value='...' " +
             "onclick='GetManageOutOfRangeInstances(\"" + system + "\", \"" + propertySource + "\",\"" + 
             propertySourceDisplayProp + "\", \"" + idTxtValue + "\", \"" + idHdn + "\")' />";         
         }         
         html = html + "&nbsp;y&nbsp;<input type='text' readonly='readonly' id='" + idTxtValueEnd + "' onblur='SetControlText(this)' style='width:70px'/>";
         if (isRefProperty == "1")
         {
             var idHdnEnd = "hdn_" + entityName + "." + propertyName + "_End_Value";
             var idBtnEnd = "btn_" + entityName + "." + propertyName + "_End_Value";
             html = html + "<input type='hidden' id='" + idHdnEnd + "' />" +
             "<input type='button' id='" + idBtnEnd + "' style='width:5%' value='...' " +
             "onclick='GetManageOutOfRangeInstances(\"" + system + "\", \"" + propertySource + "\",\"" + 
             propertySourceDisplayProp + "\", \"" + idTxtValueEnd + "\", \"" + idHdnEnd + "\")' />";         
         }           
         idTxtValueEnd
         if(propertyType == 7)
            html = html + "&nbsp;AAAA/MM/DD";
      }
      /*
      if (isRefProperty == "1")
      {
         var idHdn = "hdn_" + entityName + "." + propertyName + "_Value";
         var idBtn = "btn_" + entityName + "." + propertyName + "_Value";
         html = html + "<input type='hidden' id='" + idHdn + "' />" +
         "<input type='button' id='" + idBtn + "' style='width:5%' value='...' " +
         "onclick='GetManageOutOfRangeInstances(\"" + system + "\", \"" + propertySource + "\",\"" + 
         propertySourceDisplayProp + "\", \"" + idTxtValue + "\", \"" + idHdn + "\")' />";         
      }
      */
      
      html = html + "</div>";
         
      if (divMain == null)
      {
         html = html + "</div>";
         div = document.getElementById(divFilters);
         div.innerHTML = div.innerHTML + html;
      }
      else
      {
         divMain.innerHTML = divMain.innerHTML + html;
      }
      
      //Lleva el rastro de los divs adicionados para posteriormente poderlos borrar con facilidad
      var hidden = document.getElementById(hdnSelectedFilters);
      hidden.value = hidden.value + idDiv + ",";                
   }  
   else
   {      
      //Si el control que levantó el evento NO está seleccionado, se elimina el nodo respectivo en el
      //árbol de los filtros
      //var div = document.getElementById("div_" + entityName + "." + propertyName + "_Value");      
      //if (div != null) div.removeNode(true);     
      DeleteDiv("div_" + entityName + "." + propertyName + "_Value");
      
      var hidden = document.getElementById(hdnSelectedFilters);      
      var list = hidden.value.split(",");
      
      for (var i = 0; i < list.length; i++)
      {         
         if (list[i] == "div_" + entityName + "." + propertyName + "_Value")
            hidden.value = hidden.value.replace("div_" + entityName + "." + propertyName + "_Value,", "");
      }
   }
}

//*************************************************************************************************
//EJECUCIÓN, GUARDADO Y EXPORTACIÓN DE CONSULTAS
//*************************************************************************************************
function GetEntitiesList()
{  
   var entitiesList = "";
   var hidden = document.getElementById(hdnSelectedEntities);
   if (hidden.value != "")
   {
      var list = hidden.value.split(",");
      
      for (var i = 0; i < list.length; i++)
      {
         if (list[i] != "")
         {   
            var idChk = list[i].replace("div", "chk");
            var checkBox = document.getElementById(idChk);
            
            if (checkBox.checked)
            {      
               entitiesList += list[i].replace("div_", "");
               entitiesList += ",";
            }
         }
      }
   
      entitiesList = entitiesList.substring(0, entitiesList.lastIndexOf(","));   
      hidden = document.getElementById(hdnEntitiesList);
      hidden.value = entitiesList;
   }
}

function GetSelectList()
{
   var selectList = "";
   var selectListDisplay = "";
   var orderList = "";       
   
   var hidden = document.getElementById(hdnProperties);
   
   if (hidden.value != "")
   {

      var list = hidden.value.split(",");
      
      for (var i = 0; i < list.length; i++)
      {
         if (list[i] != "")
         {
            var idTxt = list[i].replace("div_", "txt_");
            var txt = document.getElementById(idTxt);
            
            selectList += txt.value;
            selectList += ",";
            
            var idChk = list[i].replace("div_", "");
            idChk = idChk.replace("List", "ToList");         
            var chk = document.getElementById(idChk);

            selectListDisplay += chk.getAttribute("display");
            selectListDisplay += ",";
            
            var idCbo = list[i].replace("div_", "cbo_");
            var cbo = document.getElementById(idCbo);
            
            var order = cbo.options[cbo.selectedIndex].text;
            if(order != "----")   
            {
               orderList += txt.value;
               orderList += ".";
               orderList += order;
               orderList += ",";
            }
         }     
      }
      //alert(selectList);
      //alert(selectListDisplay);
      selectList = selectList.substring(0, selectList.lastIndexOf(","));
      selectListDisplay = selectListDisplay.substring(0, selectListDisplay.lastIndexOf(","));
      orderList = orderList.substring(0, orderList.lastIndexOf(","));
   }
   
   hidden = document.getElementById(hdnPropertiesList);
   hidden.value = selectList;
   
   hidden = document.getElementById(hdnPropertiesListDisplay);
   hidden.value = selectListDisplay;
   
   hidden = document.getElementById(hdnOrdersList);
   hidden.value = orderList;         
}

function GetFilterList()
{
   var filterList = "";      

   var hidden = document.getElementById(hdnSelectedFilters);  

   if (hidden.value != "")
   {
      var list = hidden.value.split(",");           
        
      for (var i = 0; i < list.length; i++)
      {
         if (list[i] != "")
         {         
            var idTxtField = list[i].replace("div_", "txt_");
            var txtField = document.getElementById(idTxtField);
            
            var idTxtValue = idTxtField + "2";
            var txtValue = document.getElementById(idTxtValue);

            var idTxtValueEnd = idTxtField + "3";
            var txtValueEnd = document.getElementById(idTxtValueEnd);
                        
            var idCbo = list[i].replace("div_", "cbo_");
            var cbo = document.getElementById(idCbo);         
            var operator = cbo.options[cbo.selectedIndex].text;
            
            var idBtn = list[i].replace("div_", "btn_");
            var btn = document.getElementById(idBtn);
            
            var idChk = list[i].replace("div_", "chk_");
            var chk = document.getElementById(idChk);            
            
            var filterValue = "";            
            if (btn != null)
            {
               var idHdn = list[i].replace("div_", "hdn_");
               var hdn = document.getElementById(idHdn);               
               filterValue = hdn.value;
            }           
            
            if (chk != null)
            {         
                filterValue = chk.value;
               //if (chk.checked)
               //   filterValue = "1";
               //else
               //   filterValue = "0";
            }           
            
            if (filterValue == "")
               filterValue = txtValue.value;
            
            if (operator == "Entre")
                filterValue = txtValue.value + " y " + txtValueEnd.value;
            
            if (operator != "----")
            {                        
               filterList += txtField.value;
               filterList += ".";           
               filterList += operator;
               filterList += ".";
               filterList += filterValue;
               filterList += ",";
            }
         }
      }          
      
      filterList = filterList.substring(0, filterList.lastIndexOf(","));
   } 

   hidden = document.getElementById(hdnFiltersList);
   hidden.value = filterList;        
}

function SetChecked(o)
{
    if(o.checked)
        o.setAttribute("checked", "true");
    else
        o.removeAttribute("checked");
}

function SetSelectedOption(o)
{
    var selIdx = o.selectedIndex;
    for(i=0;i<o.length;i++)
    {
        if(i!=selIdx)
            o.options[i].removeAttribute("selected");
        else
            o.options[i].setAttribute("selected", "true");
    }
}

function SetSelectedOptionParameter(o)
{
//Aqui
    var TxtValueId = document.getElementById(o.getAttribute("TxtValueId"));
    var TxtValueEndId = document.getElementById(o.getAttribute("TxtValueEndId"));
    
    if(o.value != "----")
    {
        if(o.value == "Entre")
        {
            TxtValueEndId.removeAttribute("readOnly");
            TxtValueId.removeAttribute("readOnly");
        }
        else
        {
            TxtValueEndId.setAttribute("readOnly", "readonly");
            TxtValueId.removeAttribute("readOnly");
        }
    }
    else
    {
        TxtValueEndId.setAttribute("readOnly", "readonly");
        TxtValueId.setAttribute("readOnly", "readonly");  
    }
}

function SetControlText(o)
{
    if(o.value != "")
        o.setAttribute("value", o.value);
    else
        o.setAttribute("value", "");
}

//Guarda el contenido HTML del div dado en parámetros, en el hidden dado también en parámetros
function SaveDivHTML(hidden, div)
{
   var hiddenControl = document.getElementById(hidden);
   var divControl = document.getElementById(div);
   hiddenControl.value = divControl.innerHTML;  
}

//RUN_QUERY
function RunQuery()
{
   //Antes de hacer el postback para ejecutar la consulta, se recupera el estado actual
   //de la página para restablecerlo luego de hacer la ejecución
   SaveDivHTML(hdnEntities, spanEntities);
   SaveDivHTML(hdnRelatedEntities, divRelatedEntities);
   SaveDivHTML(hdnPropertiesToSelect, divPropertiesToSelect);
   SaveDivHTML(hdnSelectedProps, divSelectedProps);   
   SaveDivHTML(hdnSearchableProps, divSearchableProps);
   SaveDivHTML(hdnFilters, divFilters);  
   
	GetEntitiesList();
   GetSelectList(); 
   GetFilterList();
   
   //Verifica si la consulta que se quiere ejecutar fue originada desde targeting.  Si lo es, y
   //adicionalmente se están consultando muchos campos, se pide confirmación para ejecutar la
   //consulta
   var hidden = document.getElementById("hdnIsTargetQuery");      
   var arrSelectList = document.getElementById(hdnPropertiesList).value.split(",");   

   if (arrSelectList.length > 4 &&
      hidden.value == "1")
   {
      var message = "La consulta que quiere ejecutar proviene del sistema de campañas y " +
         "contiene demasiados atributos para consultar.  No es aconsejable tener tantos " +
         "atributos para una consulta de este tipo, dado el costo que implica su procesamiento.  " +
         "Está seguro de ejecutar la consulta?";      
      if (confirm(message) == true)
      {        
         return true;
      }
      else
         return false;
   }
   else
      return true;
}

//NEW_QUERY
function NewQuery()
{   
   var hidden = document.getElementById("hdnIsTargetQuery"); 
   
   if (hidden.value == 0)
   {
      var div = document.getElementById(spanEntities);
      div.innerHTML = "";
      
      var cbo = document.getElementById(cboEntities);
      cbo.selectedIndex = 0;      
   }
      
   hidden = document.getElementById(hdnMainEntity);
   hidden.value = "";
      
   Clean();
   SaveDivHTML(hdnEntities, spanEntities);
}