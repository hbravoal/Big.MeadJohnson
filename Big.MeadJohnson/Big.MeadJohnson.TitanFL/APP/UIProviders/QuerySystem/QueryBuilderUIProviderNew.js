﻿//===============================================================================
// QueryBuilderUIProviderNew.js
// Author : Jairo Y. Yate M. (jyate@dmgcolombia.com)
// Last update: 28 Marzo 2007
//===============================================================================
// Copyright © Data Marketing Group.  All rights reserved.
//===============================================================================

function ShowQueries()
{
   var varDivTitleQueries = document.getElementById("divTitleQueries");
   varDivTitleQueries.style.backgroundColor = "#8294a5";
   
   var varDivTitleImportQuery = document.getElementById("divTitleImportQuery");
   varDivTitleImportQuery.style.backgroundColor = "#8baac7";
   
   var varDivTitleQuery = document.getElementById("divTitleQuery");
   varDivTitleQuery.style.backgroundColor = "#8baac7";
   
   var varDivTitleResult = document.getElementById("divTitleResult");
   varDivTitleResult.style.backgroundColor = "#8baac7";
   
   var divQs = document.getElementById("divQueries");
   var divIQ = document.getElementById("divImportQuery"); 
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");   

   divQs.style.display = "block"; 
   divIQ.style.display = "none";
   divQ.style.display = "none";   
   divR.style.display = "none"; 
}

//Muestra el tab de importar consultas en la página
function ShowImportQuery()
{
   var varDivTitleQueries = document.getElementById("divTitleQueries");
   varDivTitleQueries.style.backgroundColor = "#8baac7";
   
   var varDivTitleImportQuery = document.getElementById("divTitleImportQuery");
   varDivTitleImportQuery.style.backgroundColor = "#8294a5";
   
   var varDivTitleQuery = document.getElementById("divTitleQuery");
   varDivTitleQuery.style.backgroundColor = "#8baac7";
   
   var varDivTitleResult = document.getElementById("divTitleResult");
   varDivTitleResult.style.backgroundColor = "#8baac7";
   
   var divQs = document.getElementById("divQueries");
   var divIQ = document.getElementById("divImportQuery"); 
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");   

   divQs.style.display = "none"; 
   divIQ.style.display = "block";
   divQ.style.display = "none";   
   divR.style.display = "none"; 
}

//Muestra el tab de consultas en la página
function ShowQuery()
{
   var varDivTitleQueries = document.getElementById("divTitleQueries");
   varDivTitleQueries.style.backgroundColor = "#8baac7";
   
   var varDivTitleImportQuery = document.getElementById("divTitleImportQuery");
   varDivTitleImportQuery.style.backgroundColor = "#8baac7";
   
   var varDivTitleQuery = document.getElementById("divTitleQuery");
   varDivTitleQuery.style.backgroundColor = "#8294a5";
   
   var varDivTitleResult = document.getElementById("divTitleResult");
   varDivTitleResult.style.backgroundColor = "#8baac7";
   
   var divQs = document.getElementById("divQueries");
   var divIQ = document.getElementById("divImportQuery"); 
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");   

   divQs.style.display = "none"; 
   divIQ.style.display = "none";
   divQ.style.display = "block";   
   divR.style.display = "none"; 
}

//Muestra el tab de resultado en la página
function ShowResult()
{
   var varDivTitleQueries = document.getElementById("divTitleQueries");
   varDivTitleQueries.style.backgroundColor = "#8baac7";
   
   var varDivTitleImportQuery = document.getElementById("divTitleImportQuery");
   varDivTitleImportQuery.style.backgroundColor = "#8baac7";
   
   var varDivTitleQuery = document.getElementById("divTitleQuery");
   varDivTitleQuery.style.backgroundColor = "#8baac7";
   
   var varDivTitleResult = document.getElementById("divTitleResult");
   varDivTitleResult.style.backgroundColor = "#8294a5";
   
   var divQs = document.getElementById("divQueries");
   var divIQ = document.getElementById("divImportQuery"); 
   var divQ = document.getElementById("divQuery");
   var divR = document.getElementById("divResult");   

   divQs.style.display = "none"; 
   divIQ.style.display = "none";
   divQ.style.display = "none";   
   divR.style.display = "block"; 
}

//Muestra el botón de cerrar
function ShowCloseButton()
{
   var button = document.getElementById("btnCerrar");
   button.style.visibility = "visible";
}