﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/Popup.Master" AutoEventWireup="true"
    CodeBehind="TargetFilter.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.TargetFilter" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <div style="padding: 5px">
        <asp:Literal ID="ltPropertyName" runat="server"></asp:Literal>&nbsp;<asp:DropDownList
            ID="ddlFilters" runat="server" Style="text-align: center" Width="110px" Height="20px"
            AutoPostBack="True" OnSelectedIndexChanged="ddlFilters_SelectedIndexChanged">
            <asp:ListItem Text="=" Value="=" />
            <asp:ListItem Text="<>" Value="<>" />
            <asp:ListItem Text=">" Value=">" />
            <asp:ListItem Text="&gt;=" Value="&gt;=" />
            <asp:ListItem Text="<" Value="<" />
            <asp:ListItem Text="&lt;=" Value="&lt;=" />
            <asp:ListItem Text="Contenga" Value="Contenga" />
            <asp:ListItem Text="No contenga" Value="No contenga" />
            <asp:ListItem Text="Entre" Value="Entre" />
            <asp:ListItem Text="Comience por" Value="Comience por" />
            <asp:ListItem Text="Termine en" Value="Termine en" />
        </asp:DropDownList>
        &nbsp;<asp:PlaceHolder ID="controlContainer" runat="server"></asp:PlaceHolder>
        &nbsp;<asp:Button ID="btnAddFilter" runat="server" Text="Adicionar Filtro" OnClick="btnAddFilter_Click" />
        <asp:ValidationSummary ID="vsFilter" runat="server" ShowMessageBox="true" ShowSummary="false" />
    </div>
</asp:Content>
