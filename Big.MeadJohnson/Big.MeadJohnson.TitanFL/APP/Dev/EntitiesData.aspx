﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="EntitiesData.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Dev.EntitiesData" %>

<%@ Register Src="~/Controls/ResultMessage.ascx" TagName="ResultMessage" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <div id="adminedit">
        <link href="../Scripts/CalendarControl.css" rel="stylesheet" type="text/css" />
        <div id="divBusquedaListado" runat="server" style="display: block">
            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <table border="0" cellpadding="0" cellspacing="0">
                            <tr>
                                <td>
                                    &nbsp;&nbsp;Entidad
                                </td>
                                <td>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:DropDownList ID="ddlReferenciales"
                                        runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlReferenciales_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                        <br />
                        <div id="divBusqueda" runat="server" style="display: none">
                        </div>
                        <br />
                        <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click"
                            CausesValidation="false" Visible="false" Width="100px" />&nbsp;
                        <asp:Button ID="btnTodos" runat="server" Text="Todos" OnClick="btnTodos_Click" CausesValidation="false"
                            Visible="false" Width="100px" />&nbsp;
                        <asp:Button ID="btnInsertar" runat="server" Text="Adicionar" OnClick="btnInsertar_Click"
                            CausesValidation="false" Visible="false" Width="100px" />&nbsp;
                        <asp:Button ID="btnExportar" runat="server" Text="Exportar" OnClick="btnExportar_Click"
                            CausesValidation="false" Visible="false" Width="100px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="overflow-y: scroll; overflow-x: scroll; width: 900px; border-width: thin;
                            height: 300px;">
                            <div id="divListado" runat="server">
                            </div>
                        </div>
                        <table border="0" cellpadding="5" cellspacing="0">
                            <tr>
                                <td>
                                    <asp:Label ID="lblTotalRegistros" runat="server" Text="Total de registros: 0" Font-Bold="true"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <uc1:ResultMessage ID="Mensaje1" runat="server" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divFormularioPanel" runat="server" style="display: none">
            <table border="0" cellpadding="5" cellspacing="0" width="100%">
                <tr>
                    <td>
                        <div id="divFormulario" runat="server">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <uc1:ResultMessage ID="Mensaje2" runat="server" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
