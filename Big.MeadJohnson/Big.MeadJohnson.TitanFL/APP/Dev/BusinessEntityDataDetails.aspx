﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="BusinessEntityDataDetails.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Dev.BusinessEntityDataDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <div id="adminedit">
        <link href="../Scripts/CalendarControl.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="../Scripts/CalendarControl.js"></script>
        <asp:PlaceHolder ID="controlContainer" runat="server"></asp:PlaceHolder>
    </div>
</asp:Content>
