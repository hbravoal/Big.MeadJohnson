﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="Detail.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks.Detail" %>

<%@ Register Src="Controls/DateTimeControl.ascx" TagName="DateTimeControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript">
        //-- inserting the autoformated fields for simplified use

        var savedSelection;

        function insertAtCursor(fName, fValue) {
            //-- calling the function
            //-- insertAtCursor(document.formName.fieldName, 'this value');
            var myField = document.getElementById(fName);
            var valueField = document.getElementById(fValue);
            sValue = '[' + valueField.options[valueField.selectedIndex].value + ']';
            //-- IE support
            if (document.selection) {
                myField.focus();
                sel = document.selection.createRange();
                sel.text = sValue;
            }
            //-- MOZILLA/NETSCAPE support
            else if (myField.selectionStart || myField.selectionStart == '0') {
                var startPos = myField.selectionStart;
                savedSelection = startPos;
                var endPos = myField.selectionEnd;
                myField.value = myField.value.substring(0, startPos)
	+ sValue
	+ myField.value.substring(endPos, myField.value.length);
            } else {
                myField.value += sValue;
                myField.focus();
                myField.selectionStart = savedSelection;
            }
        }

        function insertLinkSurvey(fName) {
            var myField = document.getElementById(fName);
            sValue = '[&VinculoEncuesta&]';

            if (document.selection) {
                myField.focus();
                sel = document.selection.createRange();
                sel.text = sValue;
            }

            else if (myField.selectionStart || myField.selectionStart == '0') {
                var startPos = myField.selectionStart;
                savedSelection = startPos;
                var endPos = myField.selectionEnd;
                myField.value = myField.value.substring(0, startPos)
	+ sValue
	+ myField.value.substring(endPos, myField.value.length);
            } else {
                myField.value += sValue;
                myField.focus();
                myField.selectionStart = savedSelection;
            }
        }
    </script>
    <div id="adminedit">
        <table cellpadding="5" cellspacing="0" border="1" width="100%" bordercolor="lightgrey">
            <tr>
                <td bgcolor="lightsteelblue">
                    <strong>Gestion de campañas de correo electrónico</strong>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td style="height: 15px" colspan="3">
                                <span style="font-size: 14pt"><b>Estado del Envío: </b>
                                    <asp:Label ID="lblEstado" runat="server"></asp:Label></span>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel ID="pnlMensaje" runat="server" Visible="false">
                                    <table border="0">
                                        <tr>
                                            <td valign="top" align="center" style="text-align: center">
                                                Mensaje:<br />
                                                <asp:TextBox ID="txtMensaje" runat="server" TextMode="MultiLine" Width="350px" Height="200px"
                                                    Wrap="false" ToolTip="Es el mensaje que se desea enviar"></asp:TextBox><br />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtMensaje"
                                                    Text="Máximo 4000 caracteres" ErrorMessage="M&aacte;ximo 4000 caracteres" ValidationExpression="^[\s\S]{0,4000}$"
                                                    runat="server" /><br />
                                                <asp:Label ID="lblCDescripcion" runat="server" CssClass="text" Text="Máximo 4000 caracteres"></asp:Label>&nbsp;<asp:Button
                                                    ID="btnEditar" OnClick="btnEditar_Click" runat="server" Width="80px" Visible="false"
                                                    Text="Editar"></asp:Button>
                                            </td>
                                            <td valign="middle" align="center" style="text-align: center; width: 140px">
                                                <input style="width: 130px" id="btnAdicionarTag" onclick="insertAtCursor('<%=txtMensaje.ClientID %>', '<%=ddlPropiedadesEntidad.ClientID %>');"
                                                    type="button" value="Insertar Propiedad" />
                                            </td>
                                            <td valign="top" align="center" style="text-align: center">
                                                Tags con la información del contacto:<br />
                                                <asp:ListBox ID="ddlPropiedadesEntidad" Width="350px" Height="200px" runat="server"
                                                    ToolTip="Es la lista de parametros para la personalizacion del mail">
                                                    <asp:ListItem Text="[GUID]" Value="GUID"></asp:ListItem>
                                                    <asp:ListItem Text="[PRIMER_NOMBRE]" Value="PRIMER_NOMBRE"></asp:ListItem>
                                                    <asp:ListItem Text="[SEGUNDO_NOMBRE]" Value="SEGUNDO_NOMBRE"></asp:ListItem>
                                                    <asp:ListItem Text="[PRIMER_APELLIDO]" Value="PRIMER_APELLIDO"></asp:ListItem>
                                                    <asp:ListItem Text="[SEGUNDO_APELLIDO]" Value="SEGUNDO_APELLIDO"></asp:ListItem>
                                                    <asp:ListItem Text="[NOMBRE_COMPLETO]" Value="NOMBRE_COMPLETO"></asp:ListItem>
                                                    <asp:ListItem Text="[CORREO_ELECTRONICO]" Value="CORREO_ELECTRONICO"></asp:ListItem>
                                                    <asp:ListItem Text="[CIUDAD]" Value="CIUDAD"></asp:ListItem>
                                                    <asp:ListItem Text="[NOMBRE_BARRIO]" Value="NOMBRE_BARRIO"></asp:ListItem>
                                                    <asp:ListItem Text="[GENERO]" Value="GENERO"></asp:ListItem>
                                                </asp:ListBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:Panel ID="pnlSinActividad" runat="server" Width="100%" Visible="false">
                                    <table>
                                        <tr>
                                            <td colspan="2">
                                                <table width="100%" border="0">
                                                    <tr>
                                                        <td>
                                                            Plantilla:
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlPlantilla" runat="server" ToolTip="Debe seleccionar la plantilla que desea enviar">
                                                                <asp:ListItem Value="NOTIFICACION" Text="Notificación"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                        <td>
                                                            Número de partes para la cola de envio:
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="txtNumeroPartes" runat="server" Text="1" MaxLength="2"></asp:TextBox>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Debe ser un valor numérico."
                                                                ControlToValidate="txtNumeroPartes" ValidationExpression="[0-9]+"></asp:RegularExpressionValidator>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            Fecha y hora de inicio del envío:
                                                        </td>
                                                        <td>
                                                            <uc1:DateTimeControl ID="dtFechaHoraIni" EnableDateValidation="true" EnableTimeValidation="true"
                                                                TimeErrorMesagge="La hora es requerida" DateErrorMesagge="La fecha es requerida"
                                                                ValidationGroup="Requeridos" runat="server" />
                                                        </td>
                                                        <td>
                                                            Fecha y hora final del envío:
                                                        </td>
                                                        <td>
                                                            <uc1:DateTimeControl ID="dtFechaHoraFin" EnableDateValidation="true" EnableTimeValidation="true"
                                                                TimeErrorMesagge="La hora es requerida" DateErrorMesagge="La fecha es requerida"
                                                                ValidationGroup="Requeridos" runat="server" />
                                                        </td>
                                                    </tr>
                                                    <tr id="trGenerate" runat="server">
                                                        <td colspan="4">
                                                            Generar Tarea de Envio?&nbsp;
                                                            <asp:Button ID="btnGenerar" runat="server" Text="Aceptar" OnClick="btnGenerar_Click" />
                                                            <asp:Button ID="Button1" runat="server" Text="Volver" OnClick="btnNoEnviar_Click"
                                                                Width="80px" />
                                                        </td>
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <asp:Panel ID="pnlResumen" runat="server" Width="100%" Visible="false">
                                    <table border="0" width="600">
                                        <tr>
                                            <td colspan="4">
                                                <strong><span style="font-size: 14pt">Análisis de Envío</span></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: lightsteelblue; height: 30px;">
                                                RESUMEN
                                            </td>
                                            <td style="background-color: lightsteelblue; height: 30px;">
                                                CORREOS
                                            </td>
                                            <td style="background-color: lightsteelblue; height: 30px; text-align: right">
                                                %
                                            </td>
                                            <td style="height: 30px; background-color: lightsteelblue">
                                                DESCRIPCION
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Pendientes por envío:
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblPendientes" runat="server"></asp:Label>
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblPendientesPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td>
                                                Correos en cola de envío
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: gainsboro; height: 15px;">
                                                Enviados:
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                <asp:Label ID="lblEnviados" runat="server"></asp:Label>
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                <asp:Label ID="lblEnviadosPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td style="height: 15px; background-color: gainsboro">
                                                Correos enviados y entregados
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Errados:
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblErrados" runat="server"></asp:Label>
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblErradosPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td>
                                                Correos errados
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: gainsboro; height: 15px;">
                                                Enviando:
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                <asp:Label ID="lblEnviando" runat="server"></asp:Label>
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                <asp:Label ID="lblEnviandoPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px;">
                                                Correos que estan siendo enviados
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td style="text-align: right">
                                                Total:
                                                <asp:Label ID="lblTotalUsuarios" runat="server"></asp:Label>
                                            </td>
                                            <td style="text-align: right">
                                                Total:
                                                <asp:Label ID="lblTotalPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td>
                                                Correos preparados para ser enviados
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <br />
                                            </td>
                                        </tr>
                                    </table>
                                    <br />
                                    <table border="0" width="600">
                                        <tr>
                                            <td colspan="4">
                                                <strong><span style="font-size: 14pt">Análisis de Efectividad</span></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: lightsteelblue; height: 30px;">
                                                RESUMEN
                                            </td>
                                            <td style="background-color: lightsteelblue; height: 30px;">
                                                CORREOS
                                            </td>
                                            <td style="background-color: lightsteelblue; height: 30px; text-align: right">
                                                %
                                            </td>
                                            <td style="height: 30px; background-color: lightsteelblue">
                                                DESCRIPCION
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Pendientes por leer:
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblPendientesLeer" runat="server"></asp:Label>
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblPendientesLeerPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td>
                                                Correos entregados y pendientes por ser leidos
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: gainsboro; height: 15px;">
                                                Leídos:
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                <asp:Label ID="lblLeidos" runat="server"></asp:Label>
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                <asp:Label ID="lblLeidosPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px;">
                                                Efectividad: Correos entregados y leidos
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Rebotados:
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblRebotados" runat="server"></asp:Label>
                                            </td>
                                            <td style="text-align: right">
                                                <asp:Label ID="lblRebotadosPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td>
                                                Correos entregados y rebotados
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="background-color: gainsboro; height: 15px;">
                                                &nbsp;
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                Total:
                                                <asp:Label ID="lblTotalEfec" runat="server"></asp:Label>
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px; text-align: right">
                                                Total:
                                                <asp:Label ID="lblTotalEfecPor" runat="server"></asp:Label>&nbsp;%
                                            </td>
                                            <td style="background-color: gainsboro; height: 15px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <br />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:Button ID="btnActualizarEstados" runat="server" Text="Actualizar" OnClick="btnActualizarEstados_Click"
                                                    Width="80px" />
                                                <asp:Button ID="btnSiEnviar" runat="server" Text="Enviar" OnClick="btnSiEnviar_Click"
                                                    Width="80px" />
                                                <asp:Button ID="btnCancelarEnvio" runat="server" Text="Cancelar" OnClick="btnCancelarEnvio_Click"
                                                    CommandName="Cancelar" OnClientClick="return confirm ('Si cancela el envio borrará la tarea y todos sus archivos, Está seguro que quiere cancelar el envio?')" />
                                                <asp:Button ID="btnPausarEnvio" runat="server" Text="Pausar" OnClick="btnModificarEstadoEnvio_Click"
                                                    CommandName="Pausar" Width="80px" OnClientClick="return confirm ('Está seguro que quiere pausar el envio?')" />
                                                <asp:Button ID="btnIniciarEnvio" runat="server" Text="Iniciar" OnClick="btnModificarEstadoEnvio_Click"
                                                    CommandName="Iniciar" Visible="false" Width="80px" />
                                                <asp:Button ID="btnReiniciar" runat="server" Text="Reiniciar" OnClick="btnCancelarEnvio_Click"
                                                    CommandName="Cancelar" OnClientClick="return confirm ('Si reinicia el envio borrará la tarea y todos sus archivos, Está seguro que quiere reiniciar el envio?')" />
                                                <asp:Button ID="btnNoEnviar" runat="server" Text="Volver" OnClick="btnNoEnviar_Click"
                                                    Width="80px" />&nbsp;
                                                <asp:HiddenField ID="hdnIdTarea" runat="server" />
                                                <asp:HiddenField ID="hdnIdPlantilla" runat="server" />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
