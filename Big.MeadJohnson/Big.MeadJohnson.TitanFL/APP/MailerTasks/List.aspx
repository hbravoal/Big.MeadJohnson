﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master" AutoEventWireup="true"
    CodeBehind="List.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks.List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <script type="text/javascript" src="../Scripts/YUI/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/event/event-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/dom/dom-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/connection/connection-min.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/dragdrop/dragdrop.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/animation/animation.js"></script>
    <script type="text/javascript" src="../Scripts/YUI/container/container.js"></script>
    <script type="text/javascript" src="../Scripts/TitanFL.js"></script>
    <link rel="stylesheet" type="text/css" href="../Scripts/YUI/container/assets/container.css" />
    <div id="adminedit">
        <table cellpadding="5" cellspacing="0" border="1" width="100%" bordercolor="lightgrey">
            <tr>
                <td bgcolor="lightsteelblue" colspan="2">
                    <strong>Gestion de campañas de correo electrónico</strong>
                </td>
            </tr>
            <tr runat="server" id="pnlRebotados">
                <td colspan="2">
                    ¿Desea procesar los correos rebotados?&nbsp;
                    <asp:Button ID="btnProcesarRebotados" runat="server" Text="Procesar" OnClick="btnProcesarRebotados_Click" />&nbsp;&nbsp;<asp:Button
                        ID="btnOcultarPnlRebo" runat="server" Text="Ocultar Opcion" OnClick="btnOcultarPnlRebo_Click" />
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Campaña:
                </td>
                <td>
                    <asp:TextBox ID="txtCampaignName" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td style="width: 100px">
                    Estado Tarea:
                </td>
                <td>
                    <asp:DropDownList ID="ddlTaskStatus" runat="server" DataTextField="Name" DataValueField="Id">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Button ID="btnBuscar" runat="server" Text="Buscar" OnClick="btnBuscar_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div style="height: 100%">
                        <telerik:RadGrid ID="rgList" runat="server" AutoGenerateColumns="false" AllowPaging="true"
                            PageSize="20" GridLines="None" Height="100%" Width="100%" OnNeedDataSource="rgList_NeedDataSource"
                            OnItemDataBound="rgList_ItemDataBound">
                            <MasterTableView TableLayout="Fixed" Width="100%" CommandItemDisplay="Top">
                                <CommandItemSettings ShowRefreshButton="true" RefreshText="Actualizar" ShowAddNewRecordButton="false" />
                                <Columns>
                                    <telerik:GridBoundColumn HeaderText="Programa" DataField="PROGRAMA">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Campaña" DataField="CAMPANA">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Actividad" DataField="ACTIVIDAD">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn HeaderText="Tarea Envío">
                                        <ItemTemplate>
                                            <asp:Literal ID="ltTaskId" runat="server" Text='<%# Bind("ID_TAREA") %>'></asp:Literal>
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                    <telerik:GridBoundColumn HeaderText="Estado" DataField="ESTADO">
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Total Correos" DataField="TOTAL_CORREOS">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Pendientes" DataField="PENDIENTES">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Enviados" DataField="ENVIADOS">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Rebotados" DataField="REBOTADOS">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Leidos" DataField="LEIDOS">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Enviando" DataField="ENVIANDO">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridBoundColumn HeaderText="Errados" DataField="ERRADOS">
                                        <ItemStyle HorizontalAlign="Right" />
                                    </telerik:GridBoundColumn>
                                    <telerik:GridTemplateColumn>
                                        <ItemTemplate>
                                            <asp:Button ID="btnDetalle" runat="server" Text="Detalle" OnClientClick='<%#string.Format("window.location.href=\"Detail.aspx?taskId={0}\";return false;", Eval("ID_TAREA")) %>' />
                                            <asp:Button ID="btnGenerarGrid" runat="server" Text="Generar" CommandArgument='<%# Bind("GUID_CAMPANA") %>'
                                                CommandName='<%# Bind("GUID_ACTIVIDAD") %>' OnClick="btnSiGenerar_Click" />
                                        </ItemTemplate>
                                    </telerik:GridTemplateColumn>
                                </Columns>
                                <NoRecordsTemplate>
                                    Sin registros encontrados
                                </NoRecordsTemplate>
                            </MasterTableView>
                        </telerik:RadGrid>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>
