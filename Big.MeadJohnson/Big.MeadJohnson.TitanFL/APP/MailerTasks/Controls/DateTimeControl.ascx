﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DateTimeControl.ascx.cs"
    Inherits="OnData.TitanFL.Web.UI.APP.MailerTasks.Controls.DateTimeControl" %>
<%@ Register Assembly="eWorld.UI" Namespace="eWorld.UI" TagPrefix="ew" %>
<script type="text/javascript">
    function ValidarControlHora(source, arguments) {
        var control = document.getElementById('<%=controlHoras.ClientID %>_textBox');

        if (control.value == '') {
            arguments.IsValid = false;
        }
        else {
            arguments.IsValid = true;
        }
    }
</script>
<div>
    <table cellpadding="0">
        <tr>
            <td>
                <ew:CalendarPopup ID="controlCalendario" runat="server" ClearDateText="Limpiar" GoToTodayText="Hoy:"
                    MonthYearApplyText="Aceptar" MonthYearCancelText="Cancelar" NullableLabelText="Seleccione una fecha"
                    PopupLocation="Bottom" Nullable="true" Width="74px">
                    <todaydaystyle backcolor="Aqua" font-bold="True" />
                </ew:CalendarPopup>
                <ew:RequiredFieldValidator ID="rfvFecha" ControlToValidate="controlCalendario" ErrorMessage="La fecha es requerida"
                    runat="server" Display="none" Enabled="false">
                </ew:RequiredFieldValidator>
            </td>
            <td>
                &nbsp;-&nbsp;
            </td>
            <td>
                <ew:TimePicker ID="controlHoras" runat="server" ClearTimeText="Limpiar" NullableLabelText="Seleccione una hora"
                    PopupLocation="bottom" Width="74px" Nullable="true" NumberOfColumns="1" LowerBoundTime="05/16/2008 00:00:00"
                    UpperBoundTime="01/01/0001 23:30:00">
                </ew:TimePicker>
                <asp:CustomValidator ID="rfvHora" runat="server" Enabled="false" ControlToValidate="txtValidador"
                    Display="none" ClientValidationFunction="ValidarControlHora" ValidateEmptyText="True"></asp:CustomValidator>
            </td>
        </tr>
    </table>
</div>
<div style="display: none">
    <asp:TextBox ID="txtValidador" runat="server"></asp:TextBox></div>
<asp:HiddenField ID="habilitarSemanaUnica" runat="server" Value="False" />
