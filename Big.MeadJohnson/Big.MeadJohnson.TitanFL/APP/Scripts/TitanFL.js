﻿//===============================================================================
// TitanFL.js
// Author : Wveimar López M. (wlopez@dmgcolombia.com)
// Last update: 12 octubre 2006
//===============================================================================
// Copyright © Data Marketing Group.  All rights reserved.
//===============================================================================

//Funciones JavaScript generales para todo el sistema

//Función para el manejo de las excepciones ocurridas dentro del sistema
var handleFailure = function (o) {
    DisableDisplayLoading();
    if (o.responseText !== undefined) {
        errorMessage = "Opss... HTTP status: " + o.status + " " + o.statusText + " " + o.tId;
        ShowError(errorMessage);
    }
};

//Función que permite habilitar o deshabilitar la imagen que indica que se está
//realizando un procesamiento en la página
function DisplayLoading(show) {
    if (show)
        document.getElementById('imgLoading').style.display = "block";
    else
        document.getElementById('imgLoading').style.display = "none";
}

//Función que oculta la imagen que indica que se está realizando un procesamiento
//en la página
function DisableDisplayLoading() {
    document.getElementById('imgLoading').style.display = "none";
}

//Función encargada de hacer las peticiones al servidor
function makeRequest(sUrl, postData, callback) {
    document.getElementById('imgLoading').style.display = "block";
    var request = YAHOO.util.Connect.asyncRequest('POST', sUrl, callback, postData);
}

//Validacion
function ValidatePropertyData(controlProperty) {
    //Valida requerido
    if (controlProperty.getAttribute("Required") == "true")
        if (controlProperty.value == "")
            controlProperty.style.color = "red";
        else
            controlProperty.style.color = "";
    //controlProperty.style.background-color="";

}

/****************************************************************************************
* Sólo numeros enteros
****************************************************************************************/
function onlyNumbersTitanFL(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    if (tecla == 8)
        return true;

    if (tecla == 0)
        return true;

    if (tecla == 45)
        return true;

    patron = /[0-9]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

/****************************************************************************************
* Caracteres para password
****************************************************************************************/
function inCharactersPasswordTitanFL(e) {
    tecla = (document.all) ? e.keyCode : e.which;

    if (tecla == 8)
        return true;

    if (tecla == 0)
        return true;

    patron = /[A-Za-z0-9]/;
    te = String.fromCharCode(tecla);
    return patron.test(te);
}

//*************************************************************************
//Funciones utilizadas para mostrar los mensajes de error de forma amigable
//*************************************************************************
function ShowError(message) {
    if (message.indexOf("<table") == -1) {
        message = "<table cellpadding='5' width='100%' border='0'>" +
                "<tr style='text-align:left;'><td>" + message + "</td></tr>" +
                "</table>";
    }

    YAHOO.example.simpledialog.dlg.setBody(message);
    YAHOO.example.simpledialog.dlg.show();
}

YAHOO.namespace("example.simpledialog");

var errorMessage = "";
function init() {
    var handleOK = function () {
        this.hide();
    }
    YAHOO.example.simpledialog.dlg = new YAHOO.widget.SimpleDialog("dlg",
      { visible: false, width: "400px", effect: [
         { effect: YAHOO.widget.ContainerEffect.SLIDE, duration: 0.25 },
         { effect: YAHOO.widget.ContainerEffect.FADE, duration: 0.25}],
          fixedcenter: true, modal: true, draggable: true
      });

    YAHOO.example.simpledialog.dlg.setHeader("Multisponsor - BackEnd Alianzas");
    YAHOO.example.simpledialog.dlg.cfg.queueProperty("icon", YAHOO.widget.SimpleDialog.ICON_INFO);
    YAHOO.example.simpledialog.dlg.cfg.queueProperty("buttons",
      [{ text: "Aceptar", handler: handleOK, isDefault: true}]);

    var listeners = new YAHOO.util.KeyListener(document,
      { keys: 27 }, { fn: handleOK, scope: YAHOO.example.simpledialog.dlg, correctScope: true });
    YAHOO.example.simpledialog.dlg.cfg.queueProperty("keylisteners", listeners);
    YAHOO.example.simpledialog.dlg.render(document.body);

    if (errorMessage != "")
        ShowError(errorMessage);
}

YAHOO.util.Event.addListener(window, "load", init); 