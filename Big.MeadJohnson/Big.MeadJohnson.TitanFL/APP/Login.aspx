﻿<%@ Page Title="" Language="C#" MasterPageFile="~/APP/MasterPage.Master"
    AutoEventWireup="true"
    CodeBehind="Login.aspx.cs" Inherits="OnData.TitanFL.Web.UI.APP.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="maincontent" runat="server">
    <style type="text/css">
        .form-signin {
            max-width: 450px;
            padding: 19px 29px 29px;
            margin: 15px auto 20px;
            background-color: #fff;
            border: 1px solid #e5e5e5;
            -webkit-border-radius: 5px;
            -moz-border-radius: 5px;
            border-radius: 5px;
            -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
            box-shadow: 0 1px 2px rgba(0,0,0,.05);
        }

            .form-signin .form-signin-heading,
            .form-signin .checkbox {
                margin-bottom: 10px;
            }

            .form-signin input[type="text"],
            .form-signin input[type="password"] {
                font-size: 16px;
                height: auto;
                margin-bottom: 15px;
                margin-left: 15px;
                padding: 7px 9px;
            }

            .form-signin .label {
                margin-top: 10px;
                margin-right: 10px;
            }
    </style>
    <div id="adminedit">
        <div class="form-signin">
            <!-- add H2 here and hide it with css since you can not put
h2 inside a legend tag -->
            <h3>Autenticación de Usuario</h3>
            <asp:Login ID="loginControl" CheckBoxStyle-CssClass="none"
                RememberMeText="Recordarme la próxima vez" DisplayRememberMe="false"
                TitleText="" UserNameLabelText="Nombre de usuario:" PasswordLabelText="Clave
de acceso:"
                runat="server" DestinationPageUrl="~/APP/Main.aspx" LoginButtonText="Ingresar"
                PasswordRecoveryUrl="~/APP/RecoveryPassword.aspx" PasswordRecoveryText="Recordar
Contraseña"
                OnLoggedIn="Login1_LoggedIn" FailureText="No se pudo ingresar.
Por favor intente de nuevo."
                PasswordRequiredErrorMessage="La clave de acceso es requerida."
                UserNameRequiredErrorMessage="El nombre de usuario es requerido.">
                <CheckBoxStyle CssClass="none" />
                <LoginButtonStyle CssClass="submit" />
                <TextBoxStyle CssClass="loginTextBox" />
                <LabelStyle CssClass="loginLabel" />
            </asp:Login>
        </div>
    </div>
</asp:Content>
