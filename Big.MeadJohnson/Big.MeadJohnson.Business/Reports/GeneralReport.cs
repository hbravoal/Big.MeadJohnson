﻿using Big.MeadJohnson.Business.Business;
using Big.MeadJohnson.Core.DAL;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Big.MeadJohnson.Business.Reports
{
    public class GeneralReport
    {

        public static List<Models.GeneralReport> GetGeneralReport(int? departamentId, int? cityId, int? userId, int? professionalTypeId)
        {
            var generalReport = new List<Models.GeneralReport>();

            try
            {
                var report = new List<MJ_GENERAL_REPORT_PROFESSIONAL_v2_Result>();

                using (dbBigMeadJohnson db = new dbBigMeadJohnson())
                {
                    report = db.MJ_GENERAL_REPORT_PROFESSIONAL_v2(departamentId, cityId, userId, professionalTypeId).ToList();
                }

                if (report != null)
                {
                    foreach (var row in report)
                    {
                        try
                        {
                            var professional = ProfessionalBLL.GetProfessionalById((int)row.ID);

                            var objRow = new Models.GeneralReport();

                            objRow.Departamento_Representante = row.Departamento_Representante;
                            objRow.Ciudad_Representante = row.Ciudad_Representante;
                            objRow.Nombre_Representante = row.Nombre_Representante;
                            objRow.Tipo_Profesional = row.Tipo_Profesional != null ? row.Tipo_Profesional : string.Empty;
                            objRow.Primer_Nombre = row.Primer_Nombre;
                            objRow.Segundo_Nombre = row.Segundo_Nombre;
                            objRow.Primer_Apellido = row.Primer_Apellido;
                            objRow.Segundo_Apellido = row.Segundo_Apellido;
                            objRow.Cedula = row.Cedula;
                            objRow.Genero = row.Genero;
                            objRow.Fecha_Nacimiento = Convert.ToString(row.Fecha_Nacimiento);
                            objRow.Departamento = row.Departamento != null ? row.Departamento : string.Empty;
                            objRow.Ciudad = row.Ciudad != null ? row.Ciudad : string.Empty;
                            objRow.Direccion = row.Direccion;
                            objRow.Celular = row.Celular;
                            objRow.Correo = row.Correo;
                            objRow.Tiene_hijos = row.Tiene_hijos != null ? row.Tiene_hijos == 1 ? "SI" : "NO" : string.Empty;
                            objRow.Tiene_nietos = row.Tiene_nietos != null ? row.Tiene_nietos == 1 ? "SI" : "NO" : string.Empty;
                            objRow.Codigo_Brick = row.Codigo_Brick;
                            objRow.Es_Especialista = row.Es_Especialista == null ? string.Empty : (bool)row.Es_Especialista ? "SI" : "NO";
                            objRow.Especialidad = row.Especialidad;
                            objRow.Estudia_Actualmente = row.Estudia_Actualmente == null ? string.Empty : (bool)row.Estudia_Actualmente ? "SI" : "NO";
                            objRow.Fecha_Inicio_Estudios = Convert.ToString(row.Fecha_Inicio_Estudios);
                            objRow.Fecha_finalizacion_estudios = Convert.ToString(row.Fecha_finalizacion_estudios);
                            objRow.Nivel_Profesional = row.Nivel_profesional;
                            objRow.Fecha_inicio_Profesion = Convert.ToString(row.Fecha_inicio_Profesion);
                            objRow.Entidad_en_la_que_trabaja = row.Entidad_en_la_que_trabaja;
                            objRow.Profesional_para_el_que_trabja = row.Profesional_para_el_que_trabja;
                            objRow.Fecha_inicio_labor_con_profesional = Convert.ToString(row.Fecha_inicio_labor_con_profesional);
                            objRow.Fecha_Registro = Convert.ToString(row.Fecha_Registro);
                            objRow.Ultima_Actualizacion = Convert.ToString(row.Ultima_Actualizacion);
                            objRow.Creado_Por = row.Creado_Por;
                            objRow.Tiene_Secretaria = row.Tiene_Secretaria == null ? string.Empty : row.Tiene_Secretaria == 1 ? "SI" : "NO";
                            objRow.Nombre_secretaria = row.Nombre_secretaria;
                            objRow.Cedula_secretaria = row.Cedula_secretaria;
                            objRow.Correo_secretaria = row.Correo_secretaria;
                            objRow.Direcccion_secretaria = row.Direcccion_secretaria;
                            objRow.Fecha_nacimiento_secretaria = Convert.ToString(row.Fecha_nacimiento_secretaria);
                            objRow.Secretaria_tiene_hijos = row.Secretaria_tiene_hijos == null ? string.Empty : (bool)row.Secretaria_tiene_hijos ? "SI" : "NO";
                            objRow.Celular_secretaria = row.Celular_secretaria;
                            objRow.Aceptacion_Terminos = row.Aceptacion_Terminos == null ? string.Empty : (bool)row.Aceptacion_Terminos ? "SI" : "NO";
                            objRow.Fecha_aceptacion_terminos = Convert.ToString(row.Fecha_aceptacion_Terminos);
                            objRow.Completitud = Convert.ToString(row.Completitud) + " %";
                            generalReport.Add(objRow);
                        }
                        catch (Exception EX)
                        {


                        }
                    }

                    if (generalReport == null || generalReport.Count <= 0)
                        return null;

                    return generalReport;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                return null;
            }
        }
    }
}
