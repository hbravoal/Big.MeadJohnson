﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.ViewModel
{

    public class EditProfessionalViewModel
    {
        [Display(Name = "Id")]
        public int? ProfessionalId { get; set; }

        public string FirstName { get; set; }

        //public string SecondName { get; set; }

        public string LastName { get; set; }

        //public string SecondLastName { get; set; }

        [Display(Name = "Aceptacion de uso de información")]
        public bool CheckTerms { get; set; }

        public string FullName
        {
            get
            {
                return string.Format("{0} {1}", FirstName, LastName);

            }
        }
    }
}
