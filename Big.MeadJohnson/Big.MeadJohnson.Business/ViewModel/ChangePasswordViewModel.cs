﻿using System.ComponentModel.DataAnnotations;

namespace Big.MeadJohnson.Business.ViewModel
{
    public class ChangePasswordViewModel
    {
        
        [DataType(DataType.Password)]
        [Display(Name = "Contraseña Actual")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string OldPassword { get; set; }


        [DataType(DataType.Password)]
        [Display(Name = "Nueva Contraseña")]       
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(15, MinimumLength = 8, ErrorMessage = "La {0} debe ser alfanumérica, con mínimo 1 letra y 1 número y que contenga entre 8 y 15 caracteres.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$",
            ErrorMessage = "La {0} debe sea alfanumérica, con mínimo 1 letra y 1 número y que contenga entre 8 y 15 caracteres.")]

        public string NewPassword { get; set; }


        [Compare("NewPassword", ErrorMessage = "Las contraseñas no coinciden.")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirmar contraseña")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string ConfirmPassword { get; set; }
    }
}
