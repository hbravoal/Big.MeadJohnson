﻿using System.Collections.Generic;

namespace Big.MeadJohnson.Business.ViewModel
{
    public class ProfessionalViewModel
    {
        public Models.Professional Professional { get; set; }
        public Models.LaborInformation ProfessionalLaborInformation { get; set; }
        public Models.ProfessionalContact ProfessionalContact { get; set; }
        public Models.GeneralInformation GeneralInformation { get; set; }
        public Models.SecretaryInformation SecretaryInformation { get; set; }
        public Models.HabeasData HabeasData { get; set; }
        public List<Models.GonnaDaddy> GonnaDaddy{ get; set; }
        public int? RoleId { get; set; }
        public Models.HabeasData SecretaryHabeasData{ get; set; }
    }
}
