﻿namespace Big.MeadJohnson.Business.ViewModel
{
    public class GeneralReportViewModel
    {
        public int? DepartamentId { get; set; }

        public int? CityId { get; set; }

        public int? MsrId { get; set; }

        public int? ProfessionalTypeId { get; set; }
    }
}
