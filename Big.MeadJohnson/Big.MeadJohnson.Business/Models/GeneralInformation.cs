﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Big.MeadJohnson.Business.Models
{
    public class GeneralInformation
    {


        [Display(Name = "Guid")]
        public Guid Guid { get; set; }

        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Profesional")]
        public int ProfessionalId { get; set; }

        [Display(Name = "Hijos")]
        public Parent Son { get; set; }

        public List<Parent> ListSon { get; set; }

        [Display(Name = "Nietos")]
        public Parent Grandchild { get; set; }

        public List<Parent> ListGrandchild { get; set; }

        [Display(Name = "* Tiene hijos ?")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public bool IsFather { get; set; }

        [Display(Name = "* Tiene nietos ?")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public bool IsGrandFather { get; set; }

        public List<GonnaDaddy> ListGonnaDaddy { get; set; }
    }
}
