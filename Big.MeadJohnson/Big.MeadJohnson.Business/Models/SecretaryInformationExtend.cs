﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    [NotMapped]
    public class SecretaryInformationExtend
    {
        public string  SecretaryHabbeasData{ get; set; }
        public string PropertyMessage { get; set; }
        
    }
}
