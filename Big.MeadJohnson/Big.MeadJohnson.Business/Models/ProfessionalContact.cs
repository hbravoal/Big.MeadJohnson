﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Big.MeadJohnson.Business.Models
{
    public class ProfessionalContact
    {
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }

        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Profesional")]
        public int ProfessionalId { get; set; }

        //[DataType(DataType.EmailAddress)]

        [EmailAddress(ErrorMessage = "Debe ingresar un E-mail valido.")]
        [RegularExpression(@"^[A-Za-z0-9](([_\.\-]?[a-zA-Z0-9]+)*)@([A-Za-z0-9]+)(([\.\-‌​]?[a-zA-Z0-9]+)*)\.([A-Za-z]{2,})$")]

        [StringLength(50, MinimumLength = 10, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "* Correo electrónico")]
        public string ProfessionalEmail { get; set; }

        [Display(Name = "Celular")]
        [DataType(DataType.PhoneNumber)]
        [MinLength(10,ErrorMessage = "El número de celular debe ser numérico de diez dígitos")]
        [MaxLength(10, ErrorMessage = "El número de celular debe ser numérico de diez dígitos")]      
        public string ProfessionalCellPhone { get; set; }

        [Display(Name = "* Dirección del consultorio o institución")]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string ProfessionalAddress { get; set; }

        [Display(Name = "* Ciudad")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public int? ProfessionalCityId { get; set; }

        [Display(Name = "* Departamento")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public int? ProfessionalDepartamentId { get; set; }

        [Display(Name = "Brick")]
        public string ProfessionalBrickId { get; set; }
        
    }
}
