﻿

namespace Big.MeadJohnson.Business.Models
{
    using System;
    using System.ComponentModel.DataAnnotations;
    public class Professional
    {
        [Display(Name = "Guid")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public Guid Guid { get; set; }


        [Display(Name = "Id")]
        public int? Id { get; set; }


        [Display(Name = "* Representante")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public int? MsrId { get; set; }

        [StringLength(50, MinimumLength = 10, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Debe ingresar un E-mail valido.")]
        [Display(Name = "MSR Mail")]
        public string MsrMail { get; set; }


        [Display(Name = "* Tipo de profesión")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public int? ProfessionalTypeId { get; set; }

        [Display(Name = "* Primer nombre")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string FirstName { get; set; }


        [Display(Name = "Segundo nombre")]
        [StringLength(50, MinimumLength = 0, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string SecondName { get; set; }


        [Display(Name = "* Primer apellido")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string LastName { get; set; }


        [Display(Name = "Segundo apellido")]
        [StringLength(50, MinimumLength = 0, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string SecondLastName { get; set; }


        [Display(Name = "* Cédula")]
        //[StringLength(11, MinimumLength = 8, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [RegularExpression("^[0-9]*$", ErrorMessage = "El campo {0} solo admite valores numéricos.")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public string DocumentNumber { get; set; }


        [Display(Name = "* Género")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public int? GenderId { get; set; }

      //  [Required(ErrorMessage = "El campo {0} es requerido.")]     
        [DataType(DataType.DateTime, ErrorMessage ="La fecha ingresada no es valida.")]
        public DateTime? BirthDate { get; set; }


        [Display(Name = "* Día")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]        
        [Range(1, 31, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? BirthDay { get; set; }


        [Display(Name = "* Mes")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Range(1, 12, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? BirthMonth { get; set; }

        [Display(Name = "Año")]
        [Range(1900, 9999, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? BirthYear { get; set; }


        [Display(Name = "Fecha Registro")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? RegisterDate { get; set; }


        [Display(Name = "Creado por")]
        public int? CreateBy { get; set; }

        [Display(Name = "última actualización")]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? LastUpdate { get; set; }


        public string Token { get; set; }

        public DateTime? ExpirationDateToken { get; set; }

        //relaciones           

        [Display(Name = "* Ciudad")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public int? CityId { get; set; }

        [Display(Name = "* Departamento")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public int? DepartamentId { get; set; }

        [Display(Name ="* Representantes Secundarios")]
        public string RepresentantesSecundarios { get; set; }


        public DateTime? GetBirthDate
        {
            get
            {
                try
                {
                    var date = string.Format("{0}/{1}/{2}",
                        BirthYear != null ? BirthYear : 1900,
                        BirthMonth,
                        BirthDay);

                    return DateTime.Parse(date);

                }
                catch (Exception ex)
                {
                    return null;                    
                }

            }
        }


    }
}
