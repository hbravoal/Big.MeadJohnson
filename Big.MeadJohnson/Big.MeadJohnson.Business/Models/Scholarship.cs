﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Big.MeadJohnson.Business.Models
{
    public class Scholarship
    {
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }


        [Display(Name = "Id")]
        public int Id { get; set; }


        [StringLength(150, MinimumLength = 4, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "Descripcion")]
        public string Name { get; set; }
    }
}
