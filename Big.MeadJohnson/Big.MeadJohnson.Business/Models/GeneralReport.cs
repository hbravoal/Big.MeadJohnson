﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class GeneralReport
    {
        public string Departamento_Representante { get; set; }

        public string Ciudad_Representante { get; set; }

        public string Nombre_Representante { get; set; }

        public string Tipo_Profesional { get; set; }

        public string Primer_Nombre { get; set; }

        public string Segundo_Nombre { get; set; }

        public string Primer_Apellido { get; set; }

        public string Segundo_Apellido { get; set; }

        public string Cedula { get; set; }

        public string Genero { get; set; }

        public string Fecha_Nacimiento { get; set; }

        public string Departamento { get; set; }

        public string Ciudad { get; set; }

        public string Direccion { get; set; }

        public string Celular { get; set; }

        public string Correo { get; set; }

        public string Fecha_Registro { get; set; }

        public string Ultima_Actualizacion { get; set; }


        public string Creado_Por { get; set; }


        public string Codigo_Brick { get; set; }

        public string Fecha_inicio_Profesion { get; set; }

        public string Es_Especialista { get; set; }

        public string Especialidad { get; set; }

        public string Estudia_Actualmente { get; set; }

        public string Nivel_Profesional { get; set; }

        public string Fecha_Inicio_Estudios { get; set; }

        public string Fecha_finalizacion_estudios { get; set; }

        public string Entidad_en_la_que_trabaja { get; set; }

        public string Profesional_para_el_que_trabja { get; set; }

        public string Fecha_inicio_labor_con_profesional { get; set; }

        public string Tiene_Secretaria { get; set; }

        public string Nombre_secretaria { get; set; }

        public string Cedula_secretaria { get; set; }

        public string Correo_secretaria { get; set; }        

        public string Direcccion_secretaria { get; set; }

        public string Fecha_nacimiento_secretaria { get; set; }

        public string Secretaria_tiene_hijos { get; set; }

        public string Celular_secretaria { get; set; }

        public string Aceptacion_Terminos { get; set; }

        public string Fecha_aceptacion_terminos { get; set; }

        public string Completitud { get; set; }

        public string Tiene_hijos { get; set; }

        public string Tiene_nietos { get; set; }

    }
}
