﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class LaborInformation
    {
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }

        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Profesional")]
        public int ProfessionalId { get; set; }

        // fecha profesion        
        public DateTime? StartDateProfession { get; set; }

        [Display(Name = "* Día")]
        [Range(1, 31, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? StartProfessionDay { get; set; }

        [Display(Name = "* Mes")]
        [Range(1, 12, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? StartProfessionMonth { get; set; }

        [Display(Name = "* Año")]
        [Range(1900, 9999, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? StartProfessionYear { get; set; }

        [Display(Name = "* ¿Es especialista?")]       
        public bool IsSpecialist { get; set; }

        [Display(Name = "* Especialidad")]
        public int? SpecialityId { get; set; }

        [Display(Name = "* Otra especialidad")]
        public string OtherSpeciality { get; set; }

       
        [Display(Name = "* ¿Está estudiando actualmente?")]
        public bool IsStudent { get; set; }

        [Display(Name = "* Nivel Profesional")]
        public int? ProfessionalLevelId { get; set; }

        //fecha estudio       
        public DateTime? StartDateStudy { get; set; }

        [Display(Name = "* Día")]
        [Range(1, 31, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? StartStudyDay { get; set; }

        [Display(Name = "* Mes")]
        [Range(1, 12, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? StartStudyMonth { get; set; }

        [Display(Name = "* Año")]
        [Range(1900, 9999, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? StartStudyYear { get; set; }
    
        public DateTime? EndDateStudy { get; set; }

        [Display(Name = "* Día")]
        [Range(1, 31, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? EndStudyDay { get; set; }

        [Display(Name = "* Mes")]
        [Range(1, 12, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? EndStudyMonth { get; set; }

        [Display(Name = "* Año")]
        [Range(1900, 9999, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? EndStudyYear { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "* Entidad en la que trabaja")]
        public string Company { get; set; }

      
        [Display(Name = "* Nombre del profesional para el que trabaja")]
        public string BossName { get; set; }

        //fecha inicio actual labor
       
        public DateTime? CurrentDateProfession { get; set; }

        [Display(Name = "* Día")]
        [Range(1, 31, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? CurrentProfessionDay { get; set; }

        [Display(Name = "* Mes")]
        [Range(1, 12, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? CurrentProfessionMonth { get; set; }

        [Display(Name = "* Año")]
        [Range(1900, 9999, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? CurrentProfessionYear { get; set; }

        public string AssociatedProfessionals { get; set; }
    }
}
