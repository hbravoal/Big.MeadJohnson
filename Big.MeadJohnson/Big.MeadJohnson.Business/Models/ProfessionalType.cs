﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class ProfessionalType
    {
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }


        [Display(Name = "Id")]
        public int Id { get; set; }    

        [Display(Name = "Descripcion")]
        public string Name { get; set; }


        [Display(Name = "Activo ?")]
        public bool IsActive { get; set; }
        
    }
}
