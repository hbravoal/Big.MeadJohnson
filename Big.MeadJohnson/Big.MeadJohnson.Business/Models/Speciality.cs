﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Big.MeadJohnson.Business.Models
{
    public class Speciality
    {
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }

        [Display(Name = "Id")]
        public int Id { get; set; }

        [Display(Name = "Especialidad")]
        public string Name { get; set; }

        [Display(Name = "Activo")]
        public bool? IsActive { get; set; }

        [Display(Name = "Tipo Profesional")]
        public int? ProfessionalTypeId { get; set; }
    }
}
