﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class HabeasData
    {
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }


        [Display(Name = "Id")]
        public int Id { get; set; }

        public int ProfessionalId { get; set; }

        public bool? CheckTerms { get; set; }

        public DateTime StartTermsDate { get; set; }

        public DateTime LastUpdateTermsDate { get; set; }

        public string UrlImage { get; set; }
    }
}
