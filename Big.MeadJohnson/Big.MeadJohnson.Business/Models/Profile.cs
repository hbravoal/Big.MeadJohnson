﻿

namespace Big.MeadJohnson.Business.Models
{
    using System;
    using System.Collections.Generic;

    public class Profile
    {
        public Guid Guid { get; set; }

        public int Id { get; set; }

        public string Description { get; set; }

        public bool? IsActive { get; set; }

        public string RoleId { get; set; }

        //relaciones
        public ICollection<User> Users { get; set; }
    }
}
