﻿
namespace Big.MeadJohnson.Business.Models
{
    using System;
    using System.Collections.Generic;

    public class Departament
    {
        public Guid Guid { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool? IsActive { get; set; }

        public ICollection<Professional> Professionals { get; set; }
    }
}
