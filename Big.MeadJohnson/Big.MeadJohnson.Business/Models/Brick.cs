﻿using System;

namespace Big.MeadJohnson.Business.Models
{
    public class Brick
    {
        public Guid Guid { get; set; }
        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public int CityId { get; set; }
        public int? UserId { get; set; }
    }
}
