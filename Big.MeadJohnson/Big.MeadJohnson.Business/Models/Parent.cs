﻿using System.ComponentModel.DataAnnotations;

namespace Big.MeadJohnson.Business.Models
{
    public class Parent
    {
        public int? Id { get; set; }

        [Display(Name = "Nombres")]
        public string Name { get; set; }

        [Required(ErrorMessage ="El campo {0} es requerido.")]
        [Display(Name = "* Edad ")]
        public int Age { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "* Mes/Año")]
        public int IsYearAge { get; set; }

        [Display(Name = "Escolaridad")]
        public int? SchoolYear { get; set; }

        [Display(Name = "Escolaridad")]
        public string SchoolName { get; set; }
    }
}
