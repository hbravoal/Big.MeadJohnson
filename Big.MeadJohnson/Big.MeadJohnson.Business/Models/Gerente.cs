﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class Gerente
    {
        [Key]
        public string ID { get; set; }
        [Display(Name ="Nombre Completo")]
        [Required(ErrorMessage ="Debe ingresar un nombre válido")]
        public string NombreCompleto { get; set; }
        [Display(Name ="Cédula")]
        [Required(ErrorMessage = "Debe ingresar una cédula válida")]
        public string Cedula { get; set; }
        [Display(Name ="Correo Electrónico")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Debe ingresar un Correo Electrónico valido.")]
        public string CorreoElectronico { get; set; }        
        [Display(Name ="Ciudad")]
        public string CiudadId { get; set; }
        [Display(Name = "Ciudad")]
        public ICollection<City> Ciudades { get; set; }
        [Display(Name = "Activo")]
        public bool Activo { get; set; }

        public Gerente()
        {

            Ciudades = new List<City>();
        }

    }
}
