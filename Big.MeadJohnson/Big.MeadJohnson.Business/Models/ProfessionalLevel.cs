﻿using System;

namespace Big.MeadJohnson.Business.Models
{
    public class ProfessionalLevel
    {
        public Guid Guid { get; set; }
        public int Id { get; set; }
        public string Level { get; set; }
        public bool IsActive { get; set; }
    }
}
