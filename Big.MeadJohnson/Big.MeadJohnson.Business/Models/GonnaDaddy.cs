﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class GonnaDaddy
    {

        public int Id { get; set; }
        public int Professional_id { get; set; }
        [Display(Name = "¿Usted será padre/madre?")]
        public bool GonnaDaddyQuestion { get; set; }

        [Display(Name = "Día estimado Nacimiento")]
        public int? DayStimated { get; set; }
        [Display(Name = "* Mes estimado Nacimiento")]
        public int MonthStimated { get; set; }

    }
}