﻿using System.Reflection;

namespace Big.MeadJohnson.Business.Models
{
    public class Response<T>
    {

        public bool Result { get; set; }    

        public string StatusCode { set; get; }

        public string Message { get; set; }

        public string Title { get; set; }

        public T Model { get; set; }

    }
}
