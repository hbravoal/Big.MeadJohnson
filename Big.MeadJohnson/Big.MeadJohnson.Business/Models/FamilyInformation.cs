﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class FamilyInformation
    {
       
        public Guid Guid { get; set; }
      
        public int Id { get; set; }
    
        public int ProfessionalId { get; set; }

        public string Name { get; set; }

        public int Age { get; set; }

        public int? SchoolYear { get; set; }

        public bool IsFather { get; set; }

        public bool IsGrandFather { get; set; }

        public int IsYearAge{ get; set; }
    }
}
