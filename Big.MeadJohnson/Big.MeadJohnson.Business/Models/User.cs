﻿

namespace Big.MeadJohnson.Business.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public class User
    {
        public Guid Guid { get; set; }

        public int Id { get; set; }

        [StringLength(150, MinimumLength = 4, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "Nombres")]
        public string Name { get; set; }

        [StringLength(30, MinimumLength = 5, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
       // [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,15}$", ErrorMessage = "El campo {0} solo debe contener valores alfanuméricos.")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "Nombre de Usuario")]
        public string UserName { get; set; }


        [Display(Name = "Contraseña")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [StringLength(15, MinimumLength = 8, ErrorMessage = "La {0} debe ser alfanumérica, con mínimo 1 letra y 1 número y que contenga entre 8 y 15 caracteres.")]
        [RegularExpression(@"^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[^\da-zA-Z]).{8,15}$",
            ErrorMessage = "La {0} debe sea alfanumérica, con mínimo 1 letra y 1 número y que contenga entre 8 y 15 caracteres.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "Perfil")]
        public int ProfileId { get; set; }

        [Display(Name = "Ciudad")]
        public int? CityId { get; set; }

        [Display(Name = "Departamento")]
        public int? DepartamentId { get; set; }

        [Display(Name = "Activo")]
        public bool IsActive { get; set; }

        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Debe ingresar un E-mail valido.")]
        [StringLength(50, MinimumLength = 10, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "Correo electrónico")]
        public string Email { get; set; }

        [Display(Name = "Fecha de Registro")]
        public DateTime RegisterDate { get; set; }

        public bool? IsFirstUse { get; set; }

        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "Gerente")]
        public int? ManagerId { get; set; }

        //relaciones
        [Display(Name = "Perfil")]
        public string Profilename { get; set; }

        [Display(Name = "Ciudad")]
        public City City { get; set; }


        [Display(Name = "Perfil")]
        public Profile Profile { get; set; }


        public ICollection<Professional> Professionals { get; set; }
    }
}
