﻿using System;

namespace Big.MeadJohnson.Business.Models
{
    public class Manager
    {
        public int Id { get; set; }

        public Guid Guid { get; set; }

        public string Names { get; set; }
    }
}
