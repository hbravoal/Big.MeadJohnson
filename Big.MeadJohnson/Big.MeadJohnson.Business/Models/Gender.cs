﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Models
{
    public class Gender
    {
        [Display(Name = "Guid")]
        public Guid Guid { get; set; }


        [Display(Name = "Id")]
        public int Id { get; set; }


        [StringLength(150, MinimumLength = 4, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        [Display(Name = "Descripcion")]
        public string Name { get; set; }

        public ICollection<Professional> Professionals { get; set; }
    }
}
