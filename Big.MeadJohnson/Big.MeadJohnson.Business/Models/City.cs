﻿
namespace Big.MeadJohnson.Business.Models
{
    using System;
    using System.Collections.Generic;

    public class City
    {
        public Guid Guid { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool? IsActive { get; set; }

        public int DepartamentId { get; set; }

        //relaciones
        public ICollection<User> Users { get; set; }

        public ICollection<Professional> Professionals { get; set; }        
        public ICollection<Gerente> Gerentes { get; set; }
    }
}
