﻿namespace Big.MeadJohnson.Business.Enums
{
    public enum EnumProfession
    {
        Medico = 1,
        Enfermera = 2,
        PersonAccount = 3,
        Secretaria = 4,
    }
}
