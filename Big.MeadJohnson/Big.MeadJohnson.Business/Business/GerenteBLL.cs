﻿using Big.MeadJohnson.Business.Models;
using Big.MeadJohnson.Core.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Business
{
    public class GerenteBLL
    {

        public static List<Gerente> GetAllGerentes()
        {
            var gerentes = GerenteDAL.GetAllGerentes();
            var result = new List<Gerente>();
            
            foreach (var gerente in gerentes)
            {
                //Cargar ciudades asociadas al gerente
                var aux = new Gerente();
                aux.Cedula = gerente.CEDULA.ToString();
                aux.CorreoElectronico = gerente.EMAIL;
                aux.NombreCompleto = gerente.NOMBRE_COMPLETO;
                aux.ID = gerente.ID.ToString();
                aux.Activo = gerente.ACTIVE.Value;


                //aux.Ciudades = gerente.MJ_GERENTES_CITIES.Select(p => new City
                //{
                //    Id = Convert.ToInt32(p.MJ_CITIES.ID),
                //    Name = p.MJ_CITIES.NAME
                //}).ToList();
                result.Add(aux);               
            }

            return result;
        }

        public static void CrearGerente(Gerente gerente)
        {
            var newId = Guid.NewGuid();
            if (!string.IsNullOrEmpty(gerente.CiudadId))
            {
                var cn = CityBLL.GetCityById(Convert.ToInt32(gerente.CiudadId));
                gerente.Ciudades = new List<City>();
                gerente.Ciudades.Add
                    (cn);
            }
            MJ_GERENTES mJ_ = new MJ_GERENTES
            {
                ID = newId,
                CEDULA = Convert.ToInt32(gerente.Cedula),
                EMAIL = gerente.CorreoElectronico,
                NOMBRE_COMPLETO = gerente.NombreCompleto,
                ACTIVE = gerente.Activo,
                MJ_GERENTES_CITIES = gerente.Ciudades.Select(p => new MJ_GERENTES_CITIES
                {
                    ID = Guid.Parse(Guid.NewGuid().ToString().ToUpper()),
                    CREATED_DATE=DateTime.Now,
                    CITY_ID= p.Guid.ToString().ToUpper(),
                    GERENTE_ID =newId
                }).ToList()
            };
            var nuevo = GerenteDAL.CreateGerente(mJ_);

        }

        public static Gerente ConsultarGerenteXCorreo(string correoElectronico)
        {
            var aux = GerenteDAL.GetGerenteByEmail(correoElectronico);
            if (aux != null)
            {
                var ret = new Gerente
                {
                    ID = aux.ID.ToString().ToUpper(),
                    Cedula = aux.CEDULA.ToString(),
                    CorreoElectronico = aux.EMAIL,
                    NombreCompleto = aux.NOMBRE_COMPLETO
                };
                return ret;
            }
            return null;
            
        }

        public static Gerente ConsultarGerenteXCedula(string cedula)
        {
            var aux = GerenteDAL.GetGerenteByCedula(Convert.ToInt32(cedula));
            if (aux != null)
            {
                var ret = new Gerente
                {
                    ID = aux.ID.ToString().ToUpper(),
                    Cedula = aux.CEDULA.ToString(),
                    CorreoElectronico = aux.EMAIL,
                    NombreCompleto = aux.NOMBRE_COMPLETO,
                    Activo = aux.ACTIVE.Value
                };
                return ret;
            }
            return null;
        }

        public static Gerente ConsultarGerente(string Id)
        {
            var gerente = GerenteDAL.GetGerenteById(Id.ToUpper());

            var ret = new Gerente();
            ret.ID = gerente.ID.ToString();
            ret.Cedula = gerente.CEDULA.ToString();
            ret.CorreoElectronico = gerente.EMAIL;
            ret.NombreCompleto = gerente.NOMBRE_COMPLETO;
            ret.Activo = gerente.ACTIVE.Value;

            var ciudades = GerenteDAL.GetCitiesOfGerente(Id.ToUpper());
            
            if (ciudades.Count > 0)
            {
                foreach (var c in ciudades)
                {
                    var aux = new City();
                    aux.Id = Convert.ToInt32(c.MJ_CITIES.ID);
                    aux.Name = c.MJ_CITIES.NAME;
                    
                    ret.Ciudades.Add(aux);
                }
            }
            
            return ret;
        }

        public static Gerente EditarGerente(Gerente EditGerente)
        {
            var gerente = GerenteDAL.GetGerenteById(EditGerente.ID);

            gerente.ACTIVE = EditGerente.Activo;
            gerente.EMAIL = EditGerente.CorreoElectronico;
            gerente.CEDULA = Convert.ToInt32(EditGerente.Cedula);
            gerente.NOMBRE_COMPLETO = EditGerente.NombreCompleto;

            gerente = GerenteDAL.EditarGerente(gerente);

            var ret = new Gerente();
            ret.ID = gerente.ID.ToString();
            ret.Cedula = gerente.CEDULA.ToString();
            ret.CorreoElectronico = gerente.EMAIL;
            ret.NombreCompleto = gerente.NOMBRE_COMPLETO;
            ret.Activo = gerente.ACTIVE.Value;

            //var ciudades = GerenteDAL.GetCitiesOfGerente(EditGerente.ID);

            //if (ciudades.Count > 0)
            //{
            //    foreach (var c in ciudades)
            //    {
            //        var aux = new City();
            //        aux.Id = Convert.ToInt32(c.MJ_CITIES.ID);
            //        aux.Name = c.MJ_CITIES.NAME;

            //        ret.Ciudades.Add(aux);
            //    }
            //}

            return ret;
        }

        public static void ActualizarGerente(Gerente gerente)
        {

        }

        public static void EliminarGerente(int Id)
        {

        }

        public static string AsociarCiudad(MJ_GERENTES_CITIES gerenCities)
        {
            gerenCities.ID = Guid.NewGuid();
            return GerenteDAL.AsociarCiudad(gerenCities);
        }
    }
}
