﻿

namespace Big.MeadJohnson.Business.Business
{
    using Big.MeadJohnson.Business.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;

    public class ProfileBLL
    {
        public static List<Profile> GetAllProfiles()
        {
            var departaments =  Core.DAL.ProfileDAL.GetAllProfiles();
            var result = new List<Profile>();
            foreach (var departament in departaments)
            {
                result.Add(new Profile
                {
                    Guid = Guid.Parse(departament.GUID),
                    Id = (int)departament.ID,
                    Description = departament.DESCRIPTION,
                    IsActive = departament.IS_ACTIVE,
                });
            }

            return result.OrderBy(x => x.Description).ToList();
        }
    }
}
