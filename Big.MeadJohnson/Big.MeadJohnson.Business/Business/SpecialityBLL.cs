﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Business
{
    public class SpecialityBLL
    {
        public static List<Models.Speciality> GetAllSpecialities()
        {
            try
            {
                var list = Core.DAL.Speciality.GetAllSpecialities();
                var result = new List<Models.Speciality>();

                foreach (var p in list)
                {
                    var speciality = new Models.Speciality
                    {
                        Guid = Guid.Parse(p.GUID),
                        Id = (int)p.ID,
                        Name = p.NAME,
                        IsActive = (bool)p.IS_ACTIVE,
                    };

                    result.Add(speciality);
                }

                return result.OrderBy(p => p.Name).ToList();

            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static List<Models.Speciality> GetSpecialitiesByProfesionalTypeId(int profesionalTypeId)
        {
            try
            {
                var list = Core.DAL.Speciality.GetAllSpecialities().Where(p => p.PROFESSIONAL_TYPE_ID == profesionalTypeId).ToList();
                var result = new List<Models.Speciality>();

                foreach (var p in list)
                {
                    var speciality = new Models.Speciality
                    {
                        Guid = Guid.Parse(p.GUID),
                        Id = (int)p.ID,
                        Name = p.NAME,
                        IsActive = (bool)p.IS_ACTIVE,
                    };

                    result.Add(speciality);
                }

                return result.OrderBy(p => p.Name).ToList();

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Models.Speciality GetSpecialitiesByName(string name, int professionalType)
        {
            try
            {
                var objSpeciality = Core.DAL.Speciality.GetASpecialityByName(name, professionalType);
                if (objSpeciality == null) return null;


                return new Models.Speciality
                {
                    Guid = Guid.Parse(objSpeciality.GUID),
                    Id = (int)objSpeciality.ID,
                    Name = objSpeciality.NAME,
                    IsActive = objSpeciality.IS_ACTIVE,
                };

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static int? SaveSpeciality(Models.Speciality speciality)
        {
            try
            {
                var objSpeciality = new Core.DAL.MJ_SPECIALITY
                {
                    GUID = speciality.Guid.ToString(),
                    ID = speciality.Id,
                    IS_ACTIVE = speciality.IsActive,
                    NAME = speciality.Name,
                    PROFESSIONAL_TYPE_ID = speciality.ProfessionalTypeId
                };

                return Core.DAL.Speciality.SaveSpeciality(objSpeciality);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
