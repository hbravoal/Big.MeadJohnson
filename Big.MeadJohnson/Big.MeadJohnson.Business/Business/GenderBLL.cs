﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Business
{
    public class GenderBLL
    {
        public static ICollection<Models.Gender> GetAllGender()
        {
            try
            {
                var list = new List<Models.Gender>();

                var Genders = Core.DAL.GenderDAL.GetAllGender();
                foreach (var item in Genders)
                {
                    var gender = new Models.Gender
                    {
                        Guid = Guid.Parse(item.GUID),
                        Id = (int)item.ID,
                        Name = item.NAME
                    };

                    list.Add(gender);
                }

                return list;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
