﻿
namespace Big.MeadJohnson.Business.Business
{
    using Big.MeadJohnson.Business.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    public class DepartamentBLL
    {
        public static List<Departament> GetAllDepartaments()
        {
            var departaments = Core.DAL.DepartamentDAL.GetAllDepartaments();
            var result = new List<Departament>();
            foreach (var departament in departaments)
            {
                result.Add(new Departament
                {
                    Guid = Guid.Parse(departament.GUID),
                    Id = (int)departament.ID,
                    Name = departament.NAME,                  
                    IsActive = departament.IS_ACTIVE, 
                });
            }

            return result.OrderBy(x=> x.Name).ToList();
        }

        public static Departament GetDepartamentById(int id)
        {
            try
            {
                var departament = Core.DAL.DepartamentDAL.GetDepartamentById(id);

                return new Departament
                {
                    Guid = Guid.Parse(departament.GUID),
                    Id = (int)departament.ID,
                    Name = departament.NAME,
                    IsActive = departament.IS_ACTIVE,
                };
            }
            catch (Exception ex)
            {
                return null;
                throw;
            }
        }
    }
}
