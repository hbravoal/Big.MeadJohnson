﻿

namespace Big.MeadJohnson.Business.Business
{
    using Big.MeadJohnson.Business.Models;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    public class UserBLL
    {
        #region Consultas
        public static async Task<List<User>> GetAllUser()
        {
            var users = await Core.DAL.UserDAL.GetAllUser();
            var result = new List<User>();
            foreach (var user in users)
            {
                result.Add(new User
                {
                    Guid = Guid.Parse(user.GUID),
                    Id = (int)user.ID,
                    Name = user.NAME,
                    UserName = user.USER_NAME,
                    IsActive = user.IS_ACTIVE,
                    IsFirstUse = user.IS_FIRST_USE,
                    Email = user.EMAIL,
                    Password = user.PASSWORD,
                    ProfileId = (int)user.PROFILE_ID,
                    RegisterDate = user.REGISTER_DATE,
                    CityId = (int?)user.CITY_ID,
                    ManagerId = (int?)user.MANAGER_ID
                });
            }

            return result.OrderBy(u=> u.Name).ToList();
        }
        
        public static List<User> GetAUserByCityId(int cityId)
        {
            var users = Core.DAL.UserDAL.GetAUserByCityId(cityId);
            var result = new List<User>();
            foreach (var user in users)
            {
                result.Add(new User
                {
                    Guid = Guid.Parse(user.GUID),
                    Id = (int)user.ID,
                    Name = user.NAME,
                    UserName = user.USER_NAME,
                    IsActive = user.IS_ACTIVE,
                    IsFirstUse = user.IS_FIRST_USE,
                    Email = user.EMAIL,
                    Password = user.PASSWORD,
                    ProfileId = (int)user.PROFILE_ID,
                    RegisterDate = user.REGISTER_DATE,
                    CityId = (int?)user.CITY_ID,
                    ManagerId = (int?)user.MANAGER_ID
                });
            }

            return result.OrderBy(u => u.Name).ToList();
        }

        public static User GetUserById(int id)
        {
            try
            {
                var user = Core.DAL.UserDAL.GetUserById(id);

                return new User
                {
                    Guid = Guid.Parse(user.GUID),
                    Id = (int)user.ID,
                    Name = user.NAME,
                    UserName = user.USER_NAME,
                    IsActive = user.IS_ACTIVE,
                    IsFirstUse = user.IS_FIRST_USE,
                    Email = user.EMAIL,
                    Password = user.PASSWORD,
                    ProfileId = (int)user.PROFILE_ID,
                    RegisterDate = user.REGISTER_DATE,
                    CityId =(int?) user.CITY_ID,
                    ManagerId = (int?)user.MANAGER_ID
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static User GetUserByGuid(string id)
        {
            try
            {
                var user = Core.DAL.UserDAL.GetUserByGuId(id);

                return new User
                {
                    Guid = Guid.Parse(user.GUID),
                    Id = (int)user.ID,
                    Name = user.NAME,
                    UserName = user.USER_NAME,
                    IsActive = user.IS_ACTIVE,
                    IsFirstUse = user.IS_FIRST_USE,
                    Email = user.EMAIL,
                    Password = user.PASSWORD,
                    ProfileId = (int)user.PROFILE_ID,
                    RegisterDate = user.REGISTER_DATE,
                    CityId = (int?)user.CITY_ID,
                    ManagerId = (int?)user.MANAGER_ID
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static User GetUserByUserName(string userName)
        {
            try
            {
                var user = Core.DAL.UserDAL.GetUserByUserName(userName);
                if (user == null) return null;

                return new User
                {
                    Guid = Guid.Parse(user.GUID),
                    Id = (int)user.ID,
                    Name = user.NAME,
                    UserName = user.USER_NAME,
                    IsActive = user.IS_ACTIVE,
                    IsFirstUse = user.IS_FIRST_USE,
                    Email = user.EMAIL,
                    Password = user.PASSWORD,
                    ProfileId = (int)user.PROFILE_ID,
                    RegisterDate = user.REGISTER_DATE,
                    CityId = (int?)user.CITY_ID,
                    ManagerId = (int?)user.MANAGER_ID
                };
            }
            catch (Exception ex)
            {
                return null;                
            }
        }

        public static User GetUserByEmail(string email)
        {
            try
            {
                var user = Core.DAL.UserDAL.GetUserByEmail(email);
                if (user == null) return null;

                return new User
                {
                    Guid = Guid.Parse(user.GUID),
                    Id = (int)user.ID,
                    Name = user.NAME,
                    UserName = user.USER_NAME,
                    IsActive = user.IS_ACTIVE,
                    IsFirstUse = user.IS_FIRST_USE,
                    Email = user.EMAIL,
                    Password = user.PASSWORD,
                    ProfileId = (int)user.PROFILE_ID,
                    RegisterDate = user.REGISTER_DATE,
                    CityId = (int?)user.CITY_ID,
                    ManagerId = (int?)user.MANAGER_ID
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        #endregion


        #region Acciones
        public static bool SaveNewUser(User user)
        {
            try
            {
                var objUser = new Core.DAL.MJ_USERS
                {
                    GUID = user.Guid.ToString(),
                    NAME = user.Name,
                    USER_NAME = user.UserName,
                    IS_ACTIVE = user.IsActive,
                    IS_FIRST_USE = user.IsFirstUse,
                    EMAIL = user.Email,
                    PASSWORD = user.Password,
                    PROFILE_ID = user.ProfileId,
                    REGISTER_DATE = user.RegisterDate,
                    CITY_ID = (user.CityId == 0 || user.CityId == null) ? null : user.CityId,
                    MANAGER_ID =  (user.ManagerId == 0 || user.ManagerId == null) ? null: user.ManagerId,
                };

                return Core.DAL.UserDAL.SaveNewUser(objUser);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateUser(User user)
        {
            try
            {
                var objUser = new Core.DAL.MJ_USERS
                {
                    ID = user.Id,
                    GUID = user.Guid.ToString(),
                    NAME = user.Name,
                    USER_NAME = user.UserName,
                    IS_ACTIVE = user.IsActive,
                    IS_FIRST_USE = user.IsFirstUse,
                    EMAIL = user.Email,
                    PASSWORD = user.Password,
                    PROFILE_ID = user.ProfileId,
                    REGISTER_DATE = user.RegisterDate,
                    CITY_ID = (user.CityId == 0 || user.CityId == null) ? null : user.CityId,
                    MANAGER_ID = (user.ManagerId == 0 || user.ManagerId == null) ? null : user.ManagerId,
                };

                return Core.DAL.UserDAL.UpdateUser(objUser);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion

    }
}
