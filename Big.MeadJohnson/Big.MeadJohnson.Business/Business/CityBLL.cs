﻿using Big.MeadJohnson.Business.Models;
using Big.MeadJohnson.Core.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Business
{
    public class CityBLL
    {
        public static List<City> GetAllCities()
        {
            var cities = Core.DAL.CityDAL.GetAllCities();
            var result = new List<City>();
            foreach (var city in cities)
            {
                result.Add(new City
                {
                    Guid = Guid.Parse(city.GUID),
                    Id = (int)city.ID,
                    Name = city.NAME,
                    IsActive = city.IS_ACTIVE,
                    DepartamentId = (int)city.DEPARTMENT_ID
                });
            }

            return result;
        }

        public static List<City> GetCitiesByDepartamentId(int departamentId)
        {
            var cities = Core.DAL.CityDAL.GetCitiesByDepartamentId(departamentId);
            var result = new List<City>();
            foreach (var city in cities)
            {
                result.Add(new City
                {
                    Guid = Guid.Parse(city.GUID),
                    Id = (int)city.ID,
                    Name = city.NAME,
                    IsActive = city.IS_ACTIVE,
                    DepartamentId = (int)city.DEPARTMENT_ID
                });
            }

            return result;
        }
            
        public static City GetCityById(int id)
        {
            try
            {
                var city = Core.DAL.CityDAL.GetCityById(id);

                return new City
                {
                    Guid = Guid.Parse(city.GUID),
                    Id = (int)city.ID,
                    Name = city.NAME,
                    IsActive = city.IS_ACTIVE,
                    DepartamentId = (int)city.DEPARTMENT_ID
                };
            }
            catch (Exception ex)
            {
                return null;               
            }
        }

        //internal static MJ_CITIES GetCitiesByIds(IEnumerable<string> ciudades)
        //{
        //    var cities = CityDAL.GetCitiesByIds(ciudades);
        //    return cities;
        //}
    }
}
