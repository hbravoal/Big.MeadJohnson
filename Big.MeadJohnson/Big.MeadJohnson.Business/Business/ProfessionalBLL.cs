﻿

using Big.MeadJohnson.Business.Enums;
using Big.MeadJohnson.Business.Models;
using Big.MeadJohnson.Business.ViewModel;
using Big.MeadJohnson.Core.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Business
{
    public class ProfessionalBLL
    {
        public static List<Models.Professional> GetAllProfessionalByCityId(int id)
        {
            List<Models.Professional> obj = new List<Professional>();
            try
            {
                var objProfessional = Core.DAL.ProfessionalDAL.GetAllProfessionalByCityId(id);
                foreach (var item in objProfessional)
                {
                    obj.Add(GetProfessionalById((int)item.PROFESSIONAL_ID));
                }
                return obj;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<ICollection<Models.Professional>> GetAllProfessionals()
        {
            var list = await Core.DAL.ProfessionalDAL.GetAllProfessionals();
            var result = new List<Models.Professional>();

            foreach (var p in list)
            {
                var profesional = new Models.Professional
                {
                    Guid = p.GUID,
                    Id = (int)p.ID,
                    BirthDate = (DateTime?)p.BIRTH_DATE,
                    DocumentNumber = p.DOCUMENT_NUMBER,
                    CreateBy = (int?)p.CREATED_BY,
                    FirstName = p.FIRST_NAME,
                    SecondName = p.SECOND_NAME,
                    LastName = p.LAS_NAME,
                    SecondLastName = p.SECOND_LAST_NAME,
                    LastUpdate = p.LAST_UPDATE,
                    GenderId = (int?)p.GENDER_ID,
                    MsrMail = p.MSR_MAIL,
                    ProfessionalTypeId = (int?)p.PROFESSIONAL_TYPE_ID,
                    RegisterDate = p.REGISTER_DATE,
                    MsrId = (int?)p.MSR_ID,
                    Token = p.TOKEN,
                    ExpirationDateToken = p.EXPIRATION_DATE_TOKEN
                };

                result.Add(profesional);
            }

            return result.OrderBy(r => r.DocumentNumber).ToList();
        }

        public static Models.Professional GetProfessionalById(int id)
        {
            try
            {
                var objProfessional = Core.DAL.ProfessionalDAL.GetProfessionalById(id);

                return new Models.Professional
                {
                    Guid =objProfessional.GUID,
                    Id = (int)objProfessional.ID,
                    BirthDate = (DateTime?)objProfessional.BIRTH_DATE,
                    DocumentNumber = objProfessional.DOCUMENT_NUMBER,
                    CreateBy = (int?)objProfessional.CREATED_BY,
                    FirstName = objProfessional.FIRST_NAME,
                    SecondName = objProfessional.SECOND_NAME,
                    LastName = objProfessional.LAS_NAME,
                    SecondLastName = objProfessional.SECOND_LAST_NAME,
                    LastUpdate = objProfessional.LAST_UPDATE,
                    GenderId = (int?)objProfessional.GENDER_ID,
                    MsrMail = objProfessional.MSR_MAIL,
                    ProfessionalTypeId = (int?)objProfessional.PROFESSIONAL_TYPE_ID,
                    RegisterDate = objProfessional.REGISTER_DATE,
                    MsrId = (int?)objProfessional.MSR_ID,
                    ExpirationDateToken = objProfessional.EXPIRATION_DATE_TOKEN,
                    Token = objProfessional.TOKEN,
                    
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static Models.Professional GetProfessionalCustomById(int id)
        {
            try
            {
                var objProfessional = Core.DAL.ProfessionalDAL.GetProfessionalById(id);

                return new Models.Professional
                {
                    Id = (int)objProfessional.ID,
                    BirthDate = (DateTime?)objProfessional.BIRTH_DATE,
                    DocumentNumber = objProfessional.DOCUMENT_NUMBER,
                    CreateBy = (int?)objProfessional.CREATED_BY,
                    FirstName = objProfessional.FIRST_NAME,
                    SecondName = objProfessional.SECOND_NAME,
                    LastName = objProfessional.LAS_NAME,
                    SecondLastName = objProfessional.SECOND_LAST_NAME,
                    LastUpdate = objProfessional.LAST_UPDATE,
                    GenderId = (int?)objProfessional.GENDER_ID,
                    MsrMail = objProfessional.MSR_MAIL,
                    ProfessionalTypeId = (int?)objProfessional.PROFESSIONAL_TYPE_ID,
                    RegisterDate = objProfessional.REGISTER_DATE,
                    MsrId = (int?)objProfessional.MSR_ID,
                    ExpirationDateToken = objProfessional.EXPIRATION_DATE_TOKEN,
                    Token = objProfessional.TOKEN,

                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static Models.Professional GetProfessionalByGuid(int id)
        {
            try
            {
                var objProfessional = Core.DAL.ProfessionalDAL.GetProfessionalById(id);

                return new Models.Professional
                {
                    Guid = objProfessional.GUID,
                    Id = (int)objProfessional.ID,
                    BirthDate = (DateTime?)objProfessional.BIRTH_DATE,
                    DocumentNumber = objProfessional.DOCUMENT_NUMBER,
                    CreateBy = (int?)objProfessional.CREATED_BY,
                    FirstName = objProfessional.FIRST_NAME,
                    SecondName = objProfessional.SECOND_NAME,
                    LastName = objProfessional.LAS_NAME,
                    SecondLastName = objProfessional.SECOND_LAST_NAME,
                    LastUpdate = objProfessional.LAST_UPDATE,
                    GenderId = (int?)objProfessional.GENDER_ID,
                    MsrMail = objProfessional.MSR_MAIL,
                    ProfessionalTypeId = (int?)objProfessional.PROFESSIONAL_TYPE_ID,
                    RegisterDate = objProfessional.REGISTER_DATE,
                    MsrId = (int?)objProfessional.MSR_ID,
                    ExpirationDateToken = objProfessional.EXPIRATION_DATE_TOKEN,
                    Token = objProfessional.TOKEN,
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Models.Professional GetProfessionalByToken(string Token)
        {
            try
            {
                var objProfessional = Core.DAL.ProfessionalDAL.GetProfessionalByToken(Token);

                return new Models.Professional
                {
                    Guid = objProfessional.GUID,
                    Id = (int)objProfessional.ID,
                    BirthDate = (DateTime?)objProfessional.BIRTH_DATE,
                    DocumentNumber = objProfessional.DOCUMENT_NUMBER,
                    CreateBy = (int?)objProfessional.CREATED_BY,
                    FirstName = objProfessional.FIRST_NAME,
                    SecondName = objProfessional.SECOND_NAME,
                    LastName = objProfessional.LAS_NAME,
                    SecondLastName = objProfessional.SECOND_LAST_NAME,
                    LastUpdate = objProfessional.LAST_UPDATE,
                    GenderId = (int?)objProfessional.GENDER_ID,
                    MsrMail = objProfessional.MSR_MAIL,
                    ProfessionalTypeId = (int?)objProfessional.PROFESSIONAL_TYPE_ID,
                    RegisterDate = objProfessional.REGISTER_DATE,
                    MsrId = (int?)objProfessional.MSR_ID,
                    ExpirationDateToken = objProfessional.EXPIRATION_DATE_TOKEN,
                    Token = objProfessional.TOKEN,
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static Models.Professional GetProfessionalByDocumentNumber(string documentNumber)
        {
            try
            {
                var objProfessional = Core.DAL.ProfessionalDAL.GetProfessionalByDocumentNumber(documentNumber);

                if (objProfessional == null) return null;


                return new Models.Professional
                {
                    Guid = objProfessional.GUID,
                    Id = (int)objProfessional.ID,
                    BirthDate = (DateTime?)objProfessional.BIRTH_DATE,
                    DocumentNumber = objProfessional.DOCUMENT_NUMBER,
                    CreateBy = (int?)objProfessional.CREATED_BY,
                    FirstName = objProfessional.FIRST_NAME,
                    SecondName = objProfessional.SECOND_NAME,
                    LastName = objProfessional.LAS_NAME,
                    SecondLastName = objProfessional.SECOND_LAST_NAME,
                    LastUpdate = objProfessional.LAST_UPDATE,
                    GenderId = (int?)objProfessional.GENDER_ID,
                    MsrMail = objProfessional.MSR_MAIL,
                    ProfessionalTypeId = (int?)objProfessional.PROFESSIONAL_TYPE_ID,
                    RegisterDate = objProfessional.REGISTER_DATE,
                    MsrId = (int?)objProfessional.MSR_ID,
                    Token = objProfessional.TOKEN,
                    ExpirationDateToken = objProfessional.EXPIRATION_DATE_TOKEN
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static Models.ProfessionalContact GetProfessionalContactById(int professionalId)
        {
            try
            {
                var professionalContact = Core.DAL.ProfessionalDAL.GetProfessionalContactById(professionalId);

                return new Models.ProfessionalContact
                {
                    Guid = Guid.Parse(professionalContact.GUID),
                    Id = (int)professionalContact.ID,
                    ProfessionalAddress = professionalContact.ADDRESS,
                    ProfessionalCellPhone = professionalContact.CELL_PHONE,
                    ProfessionalCityId = (int?)professionalContact.CYTY_ID,
                    ProfessionalEmail = professionalContact.E_MAIL,
                    ProfessionalId = professionalId,
                    ProfessionalBrickId = professionalContact.BRICK
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static Models.ProfessionalContact GetProfessionalContactByEmail(string email)
        {
            try
            {
                var professionalContact = Core.DAL.ProfessionalDAL.GetProfessionalContactByEmail(email);

                return new Models.ProfessionalContact
                {
                    Guid = Guid.Parse(professionalContact.GUID),
                    Id = (int)professionalContact.ID,
                    ProfessionalAddress = professionalContact.ADDRESS,
                    ProfessionalCellPhone = professionalContact.CELL_PHONE,
                    ProfessionalCityId = (int?)professionalContact.CYTY_ID,
                    ProfessionalEmail = professionalContact.E_MAIL,
                    ProfessionalId = (int)professionalContact.PROFESSIONAL_ID
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static List<Models.GonnaDaddy> GetGonnaDaddyByProfessionalId(int id)
        {
            try
            {
                var professionalGonna = Core.DAL.ProfessionalDAL.GetGonnaDaddyByProfessionalId(id);
                List<Models.GonnaDaddy> listGonna = new List<GonnaDaddy>();
                if (professionalGonna.Any())
                {
                    foreach (var item in professionalGonna)
                    {
                        var generalInfo = new Models.GonnaDaddy
                        {
                            Id= item.id,
                            DayStimated= item.DayStimated,
                            GonnaDaddyQuestion= (Boolean)item.GonnaDaddyQuestion,MonthStimated= (int)item.MonthStimated,
                            Professional_id= (int)item.Professional_id,

                         
                        };

                        listGonna.Add(generalInfo);
                    }

                    return listGonna;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static ICollection<Models.ProfessionalType> GetAllProfessionalTypes()
        {
            var list = Core.DAL.ProfessionalDAL.GetAllProfessionalType();
            var result = new List<Models.ProfessionalType>();

            foreach (var p in list)
            {
                var profesional = new Models.ProfessionalType
                {
                    Guid = Guid.Parse(p.GUID),
                    Id = (int)p.ID,
                    Name = p.NAME,
                    IsActive = (bool)p.IS_ACTIVE,
                };

                result.Add(profesional);
            }

            return result.OrderBy(r => r.Name).ToList();
        }

        public static ICollection<Models.ProfessionalLevel> GetAllProfessionalLevels()
        {
            var list = Core.DAL.ProfessionalDAL.GetAllProfessionalLevels();
            var result = new List<Models.ProfessionalLevel>();

            foreach (var p in list)
            {
                var profesional = new Models.ProfessionalLevel
                {
                    Guid = Guid.Parse(p.GUID),
                    Id = (int)p.ID,
                    Level = p.LEVEL,
                    IsActive = (bool)p.ISACTIVE,
                };

                result.Add(profesional);
            }

            return result.OrderBy(r => r.Level).ToList();
        }

        public static int? SaveNewProfessional(Models.Professional professional)
        {
            try
            {
                var objProfessional = new Core.DAL.MJ_PROFESSIONAL
                {
                    GUID = professional.Guid,
                    ID = (int)professional.Id,
                    BIRTH_DATE = professional.BirthDate,
                    DOCUMENT_NUMBER = professional.DocumentNumber,
                    CREATED_BY = (int?)professional.CreateBy,
                    FIRST_NAME = professional.FirstName,
                    SECOND_NAME = professional.SecondName,
                    LAS_NAME = professional.LastName,
                    SECOND_LAST_NAME = professional.SecondLastName,
                    LAST_UPDATE = professional.LastUpdate,
                    GENDER_ID = (int?)professional.GenderId,
                    MSR_MAIL = professional.MsrMail,
                    PROFESSIONAL_TYPE_ID = (int?)professional.ProfessionalTypeId,
                    REGISTER_DATE = professional.RegisterDate,
                    MSR_ID = (Decimal)professional.MsrId,
                    TOKEN = professional.Token,
                    EXPIRATION_DATE_TOKEN = professional.ExpirationDateToken
                };

                return Core.DAL.ProfessionalDAL.SaveNewProfessional(objProfessional);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static int? SaveProfessionalContact(Models.ProfessionalContact professionalContact)
        {
            try
            {
                var objProfessionalContact = new Core.DAL.MJ_PROFESSIONAL_CONTACT
                {
                    GUID = professionalContact.Guid.ToString(),
                    ID = (int)professionalContact.Id,
                    ADDRESS = professionalContact.ProfessionalAddress,
                    CELL_PHONE = professionalContact.ProfessionalCellPhone,
                    CYTY_ID = professionalContact.ProfessionalCityId,
                    E_MAIL = professionalContact.ProfessionalEmail,
                    PROFESSIONAL_ID = professionalContact.ProfessionalId,
                    BRICK = professionalContact.ProfessionalBrickId,
                };

                return Core.DAL.ProfessionalDAL.SaveProfessionalContact(objProfessionalContact);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool UpdateProfessionalContact(Models.ProfessionalContact professionalContact)
        {
            try
            {
                var objProfessionalContact = new Core.DAL.MJ_PROFESSIONAL_CONTACT
                {
                    GUID = professionalContact.Guid.ToString(),
                    ID = (int)professionalContact.Id,
                    ADDRESS = professionalContact.ProfessionalAddress,
                    CELL_PHONE = professionalContact.ProfessionalCellPhone,
                    CYTY_ID = professionalContact.ProfessionalCityId,
                    E_MAIL = professionalContact.ProfessionalEmail,
                    PROFESSIONAL_ID = professionalContact.ProfessionalId,
                    BRICK = professionalContact.ProfessionalBrickId,

                };

                return Core.DAL.ProfessionalDAL.UpdateProfessionalContact(objProfessionalContact);
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static int? SaveLabolarInformation(Models.LaborInformation LaborInformation)
        {
            try
            {
                var laboralInfo = new Core.DAL.MJ_PROFESSIONAL_LABOR_INFORMATION
                {
                    BOSS_NAME = LaborInformation.BossName,
                    COMPANY_NAME = LaborInformation.Company,
                    GUID = LaborInformation.Guid.ToString(),
                    ID = LaborInformation.Id,
                    IS_SPECIALIST = LaborInformation.IsSpecialist,
                    IS_STUDENT = LaborInformation.IsStudent,
                    LABOR_START_DATE = LaborInformation.CurrentDateProfession,
                    PROFESSIONAL_ID = LaborInformation.ProfessionalId,
                    PROFESSIONAL_LEVEL = LaborInformation.ProfessionalLevelId,
                    SPECIALTY_ID = LaborInformation.SpecialityId,
                    START_DATE_PROFESSION = LaborInformation.StartDateProfession,
                    STUDY_END_DATE = LaborInformation.EndDateStudy,
                    STUDY_START_DATE = LaborInformation.StartDateStudy,
                    ASSOCIATED_PROFESSIONALS=LaborInformation.AssociatedProfessionals
                };

                return Core.DAL.ProfessionalDAL.SaveLabolarInformation(laboralInfo);
            }
            catch (Exception)
            {
                return null;

            }
        }

        public static bool UpdateLabolarInformation(Models.LaborInformation LaborInformation)
        {
            try
            {
                var laboralInfo = new Core.DAL.MJ_PROFESSIONAL_LABOR_INFORMATION
                {
                    BOSS_NAME = LaborInformation.BossName,
                    COMPANY_NAME = LaborInformation.Company,
                    GUID = LaborInformation.Guid.ToString(),
                    ID = LaborInformation.Id,
                    IS_SPECIALIST = LaborInformation.IsSpecialist,
                    IS_STUDENT = LaborInformation.IsStudent,
                    LABOR_START_DATE = LaborInformation.CurrentDateProfession,
                    PROFESSIONAL_ID = LaborInformation.ProfessionalId,
                    PROFESSIONAL_LEVEL = LaborInformation.ProfessionalLevelId,
                    SPECIALTY_ID = LaborInformation.SpecialityId,
                    START_DATE_PROFESSION = LaborInformation.StartDateProfession,
                    STUDY_END_DATE = LaborInformation.EndDateStudy,
                    STUDY_START_DATE = LaborInformation.StartDateStudy,
                    ASSOCIATED_PROFESSIONALS= LaborInformation.AssociatedProfessionals,
                };

                return Core.DAL.ProfessionalDAL.UpdateLabolarInformation(laboralInfo);
            }
            catch (Exception)
            {
                return false;

            }
        }

        public static int? SaveSecretaryInformation(Models.SecretaryInformation SecretaryInformation)
        {
            try
            {
                var objSecretary = new Core.DAL.MJ_PROFESSIONAL_SECRETARY_INFORMATION
                {
                    GUID = SecretaryInformation.SecretaryGuid.ToString(),
                    ID = SecretaryInformation.SecretaryId,
                    ADDRESS = SecretaryInformation.SecretaryAddress,
                    BIRTH_DATE = SecretaryInformation.SecretaryBirthDate,
                    CELL_PHONE = SecretaryInformation.SecretaryCellPhone,
                    COMPANY_NAME = SecretaryInformation.SecretaryCompany,
                    DOCUMENT_NUMBER = SecretaryInformation.SecretaryDocumentNumber,
                    EMAIL = SecretaryInformation.SecretaryEmail,
                    HAVE_SON = SecretaryInformation.SecretaryHaveSon,
                    LAST_NAMES = SecretaryInformation.SecretaryLastName,
                    NAME = SecretaryInformation.SecretaryFirstName,
                    PROFESSIONAL_ID = SecretaryInformation.ProfessionalId,

                };

                return Core.DAL.ProfessionalDAL.SaveSecretaryInformation(objSecretary);
            }
            catch (Exception)
            {
                return null;
            }
        }


              public static int? SaveGonnaDaddy(Models.GonnaDaddy obj)
             {
            try
            {
                var objGonna = new Core.DAL.MJ_GONNA_DADDY
                {
                    DayStimated= obj.DayStimated,
                    GonnaDaddyQuestion=obj.GonnaDaddyQuestion,
                    MonthStimated=obj.MonthStimated,
                    Professional_id=obj.Professional_id,
                };

                return Core.DAL.ProfessionalDAL.SaveGonnaDaddy(objGonna);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static int? SaveFamilyInformation(Models.FamilyInformation familyInformation)
        {
            try
            {
                var objSecretary = new Core.DAL.MJ_PROFESSIONAL_FAMILY_INFORMATION
                {
                    GUID = familyInformation.Guid.ToString(),
                    ID = familyInformation.Id,
                    IS_SON = familyInformation.IsFather,
                    IS_GRANDCHILD = familyInformation.IsGrandFather,
                    AGE = familyInformation.Age,
                    NAME = familyInformation.Name,
                    PROFESSIONAL_ID = familyInformation.ProfessionalId,
                    SCHOOL_YEAR = familyInformation.SchoolYear,
                    IsYearAge= familyInformation.IsYearAge,
                };

                return Core.DAL.ProfessionalDAL.SaveFamilyInformation(objSecretary);
            }
            catch (Exception)
            {
                return null;
            }
        }


        public static Models.LaborInformation GetLaboralInformationByProfessionalId(int professionalId)
        {
            var ObjlaboralInfo = Core.DAL.ProfessionalDAL.GetLaboralInformationByProfessionalId(professionalId);
            if (ObjlaboralInfo != null)
            {
                var laboralInfo = new Models.LaborInformation
                {
                    Guid = Guid.Parse(ObjlaboralInfo.GUID),
                    Id = (int)ObjlaboralInfo.ID,
                    ProfessionalId = (int)ObjlaboralInfo.PROFESSIONAL_ID,
                    StartDateProfession = ObjlaboralInfo.START_DATE_PROFESSION,
                    IsSpecialist = (bool)ObjlaboralInfo.IS_SPECIALIST,
                    SpecialityId = (int?)ObjlaboralInfo.SPECIALTY_ID,
                    IsStudent = (bool)ObjlaboralInfo.IS_STUDENT,
                    ProfessionalLevelId = (int?)ObjlaboralInfo.PROFESSIONAL_LEVEL,
                    StartDateStudy = ObjlaboralInfo.STUDY_START_DATE,
                    EndDateStudy = ObjlaboralInfo.STUDY_END_DATE,
                    Company = ObjlaboralInfo.COMPANY_NAME,
                    BossName = ObjlaboralInfo.BOSS_NAME,
                    CurrentDateProfession = ObjlaboralInfo.LABOR_START_DATE,
                    AssociatedProfessionals= ObjlaboralInfo.ASSOCIATED_PROFESSIONALS,
                };

                return laboralInfo;
            }

            return null;

        }

        public static Models.SecretaryInformation GetSecretaryByProfessionalId(int professionalId)
        {
            var objSecreatryInfo = Core.DAL.ProfessionalDAL.GetSecretaryByProfessionalId(professionalId);
            if (objSecreatryInfo != null)
            {
                var SecreatryInfo = new Models.SecretaryInformation
                {
                    SecretaryGuid = Guid.Parse(objSecreatryInfo.GUID),
                    SecretaryId = (int)objSecreatryInfo.ID,
                    ProfessionalId = (int)objSecreatryInfo.PROFESSIONAL_ID,
                    SecretaryAddress = objSecreatryInfo.ADDRESS,
                    SecretaryBirthDate = objSecreatryInfo.BIRTH_DATE,
                    SecretaryCellPhone = objSecreatryInfo.CELL_PHONE,
                    SecretaryCompany = objSecreatryInfo.COMPANY_NAME,
                    SecretaryDocumentNumber = objSecreatryInfo.DOCUMENT_NUMBER,
                    SecretaryEmail = objSecreatryInfo.EMAIL,
                    SecretaryFirstName = objSecreatryInfo.NAME,
                    SecretaryHaveSon = (bool)objSecreatryInfo.HAVE_SON,
                    SecretaryLastName = objSecreatryInfo.LAST_NAMES,
                    SecretaryHaveSecretary = true,
                };

                return SecreatryInfo;
            }

            return null;

        }
        public static Models.SecretaryInformation GetSecretaryByDocument(string document)
        {
            if (ProfessionalDAL.GetProfessionalTypebyDocument(document)!=null)
            {
                return new SecretaryInformation
                {
                    PropertyMessage = "Ya existe un profesional con ése documento",
                 
                };                
            }

            var objSecreatryInfo = Core.DAL.ProfessionalDAL.GetSecretaryDocument(document);
            if (objSecreatryInfo != null)
            {
                var SecreatryInfo = new Models.SecretaryInformation
                {
                    SecretaryGuid = Guid.Parse(objSecreatryInfo.GUID),
                    SecretaryId = (int)objSecreatryInfo.ID,
                    ProfessionalId = (int)objSecreatryInfo.PROFESSIONAL_ID,
                    SecretaryAddress = objSecreatryInfo.ADDRESS,
                    SecretaryBirthDate = objSecreatryInfo.BIRTH_DATE,
                    
                    SecretaryCellPhone = objSecreatryInfo.CELL_PHONE,
                    SecretaryCompany = objSecreatryInfo.COMPANY_NAME,
                    SecretaryDocumentNumber = objSecreatryInfo.DOCUMENT_NUMBER,
                    SecretaryEmail = objSecreatryInfo.EMAIL,
                    SecretaryFirstName = objSecreatryInfo.NAME,
                    SecretaryHaveSon = (bool)objSecreatryInfo.HAVE_SON,
                    SecretaryLastName = objSecreatryInfo.LAST_NAMES,
                    SecretaryHaveSecretary = true,
                };

                return SecreatryInfo;
            }

            return null;

        }
        public static List<Models.SecretaryInformation> GetAllSecretaryByDocument(string document)
        {

            List<SecretaryInformation> returned = new List<SecretaryInformation>();
            var objSecreatryInfoGet = Core.DAL.ProfessionalDAL.GetAllSecretaryDocument(document);
            
            foreach (var objSecreatryInfo in objSecreatryInfoGet)
            {
                returned.Add(new Models.SecretaryInformation {

                    SecretaryGuid = Guid.Parse(objSecreatryInfo.GUID),
                    SecretaryId = (int)objSecreatryInfo.ID,
                    ProfessionalId = (int)objSecreatryInfo.PROFESSIONAL_ID,
                    SecretaryAddress = objSecreatryInfo.ADDRESS,
                    SecretaryBirthDate = objSecreatryInfo.BIRTH_DATE,

                    SecretaryCellPhone = objSecreatryInfo.CELL_PHONE,
                    SecretaryCompany = objSecreatryInfo.COMPANY_NAME,
                    SecretaryDocumentNumber = objSecreatryInfo.DOCUMENT_NUMBER,
                    SecretaryEmail = objSecreatryInfo.EMAIL,
                    SecretaryFirstName = objSecreatryInfo.NAME,
                    SecretaryHaveSon = (bool)objSecreatryInfo.HAVE_SON,
                    SecretaryLastName = objSecreatryInfo.LAST_NAMES,
                    SecretaryHaveSecretary = true,
                });
                    


            }
            return returned;





        }

        public static List<Models.FamilyInformation> GetFamilyInformationByProfessionalId(int professionalId)
        {
            var list = Core.DAL.ProfessionalDAL.GetFamilyInformationByProfessionalId(professionalId);
            var listFamily = new List<Models.FamilyInformation>();

            if (list.Any())
            {
                foreach (var item in list)
                {
                    var generalInfo = new Models.FamilyInformation
                    {
                        Guid = Guid.Parse(item.GUID),
                        Id = (int)item.ID,
                        Name = item.NAME,
                        Age = (int)item.AGE,
                        SchoolYear = (int?)item.SCHOOL_YEAR,
                        IsFather = (bool)item.IS_SON,
                        IsGrandFather = (bool)item.IS_GRANDCHILD,
                        IsYearAge= (int)item.IsYearAge
                    };

                    listFamily.Add(generalInfo);
                }

                return listFamily;
            }

            return null;
        }

        public static bool UpdateProfessional(Models.Professional professional)
        {
            try
            {
                var objProfessional = new Core.DAL.MJ_PROFESSIONAL
                {
                    GUID = professional.Guid,
                    ID = (int)professional.Id,
                    BIRTH_DATE = professional.BirthDate,
                    DOCUMENT_NUMBER = professional.DocumentNumber,
                    CREATED_BY = (int?)professional.CreateBy,
                    FIRST_NAME = professional.FirstName,
                    SECOND_NAME = professional.SecondName,
                    LAS_NAME = professional.LastName,
                    SECOND_LAST_NAME = professional.SecondLastName,
                    LAST_UPDATE = professional.LastUpdate,
                    GENDER_ID = (int?)professional.GenderId,
                    MSR_MAIL = professional.MsrMail,
                    PROFESSIONAL_TYPE_ID = (int?)professional.ProfessionalTypeId,
                    REGISTER_DATE = professional.RegisterDate,
                    MSR_ID = (Decimal)professional.MsrId,
                    TOKEN = professional.Token,
                    EXPIRATION_DATE_TOKEN = professional.ExpirationDateToken
                };

                return Core.DAL.ProfessionalDAL.UpdateProfessional(objProfessional);
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void DeleteGonnaDaddyByProfessionalId(int professionalId)
        {
            Core.DAL.ProfessionalDAL.DeleteGonnaDaddyById(professionalId);
        }
        public static void DeleteGeneralInformationById(int professionalId)
        {
            Core.DAL.ProfessionalDAL.DeleteGeneralInformationById(professionalId);
        }

        public static bool UpdateSecretaryInformation(Models.SecretaryInformation SecretaryInformation)
        {
            try
            {
                var objSecretary = new Core.DAL.MJ_PROFESSIONAL_SECRETARY_INFORMATION
                {
                    GUID = SecretaryInformation.SecretaryGuid.ToString(),
                    ID = SecretaryInformation.SecretaryId,
                    ADDRESS = SecretaryInformation.SecretaryAddress,
                    BIRTH_DATE = SecretaryInformation.SecretaryBirthDate,
                    CELL_PHONE = SecretaryInformation.SecretaryCellPhone,
                    COMPANY_NAME = SecretaryInformation.SecretaryCompany,
                    DOCUMENT_NUMBER = SecretaryInformation.SecretaryDocumentNumber,
                    EMAIL = SecretaryInformation.SecretaryEmail,
                    HAVE_SON = SecretaryInformation.SecretaryHaveSon,
                    LAST_NAMES = SecretaryInformation.SecretaryLastName,
                    NAME = SecretaryInformation.SecretaryFirstName,
                    PROFESSIONAL_ID = SecretaryInformation.ProfessionalId,

                };

                return Core.DAL.ProfessionalDAL.UpdateSecretaryInformation(objSecretary);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static void DeleteSecretaryInformationById(int professionalId)
        {
            Core.DAL.ProfessionalDAL.DeleteSecretaryInformationById(professionalId);
        }

        

       public static bool SaveSecretaryHabeasData(Models.HabeasData habeasData)
        {
            var objHabeasData = new Core.DAL.MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA
            {
                CHECK_TERMS = habeasData.CheckTerms,
                GUID = habeasData.Guid.ToString(),
                ID = habeasData.Id,
                LAST_UPDATE_TTERMS = habeasData.LastUpdateTermsDate,
                SECRETARY_ID = habeasData.ProfessionalId,
                START_DATE_TERMS = habeasData.StartTermsDate,
                URL_IMAGE = habeasData.UrlImage

            };

            return Core.DAL.ProfessionalDAL.SaveSecretaryHabeasData(objHabeasData);
        }

        public static bool SaveProfessionalHabeasData(Models.HabeasData habeasData)
        {
            var objHabeasData = new Core.DAL.MJ_PROFESSIONAL_HABEAS_DATA
            {
                CHECK_TERMS = habeasData.CheckTerms,
                GUID = habeasData.Guid.ToString(),
                ID = habeasData.Id,
                LAST_UPDATE_TTERMS = habeasData.LastUpdateTermsDate,
                PROFESSIONAL_ID = habeasData.ProfessionalId,
                START_DATE_TERMS = habeasData.StartTermsDate,
                URL_IMAGE = habeasData.UrlImage

            };

            return Core.DAL.ProfessionalDAL.SaveProfessionalHabeasData(objHabeasData);
        }

        public static Models.HabeasData GetHabeasDataByProfessionalId(int professionalId)
        {
            var objHabeasData = Core.DAL.ProfessionalDAL.GetProfessionalHabeasDataByProfessionalId(professionalId);

            if (objHabeasData != null)
            {
                return new Models.HabeasData
                {
                    CheckTerms = objHabeasData.CHECK_TERMS,
                    Guid = Guid.Parse(objHabeasData.GUID),
                    Id = (int)objHabeasData.ID,
                    ProfessionalId = (int)objHabeasData.PROFESSIONAL_ID,
                    LastUpdateTermsDate = (DateTime)objHabeasData.LAST_UPDATE_TTERMS,
                    StartTermsDate = (DateTime)objHabeasData.START_DATE_TERMS,
                    UrlImage = objHabeasData.URL_IMAGE

                };
            }

            return null;
        }

        public static Models.HabeasData GetHabeasDataBySecretaryId(int professionalId)
        {
            var objHabeasData = Core.DAL.ProfessionalDAL.GetProfessionalHabeasDataBySecretaryId(professionalId);

            if (objHabeasData != null)
            {
                return new Models.HabeasData
                {
                    CheckTerms = objHabeasData.CHECK_TERMS,
                    Guid = Guid.Parse(objHabeasData.GUID),
                    Id = (int)objHabeasData.ID,
                    ProfessionalId = (int)objHabeasData.SECRETARY_ID,
                    LastUpdateTermsDate = (DateTime)objHabeasData.LAST_UPDATE_TTERMS,
                    StartTermsDate = (DateTime)objHabeasData.START_DATE_TERMS,
                    UrlImage = objHabeasData.URL_IMAGE

                };
            }

            return null;
        }


        public static List<ListProfessionalViewModel> GetListProfessionals(int? mrsId)
        {
            var professionals = new List<ListProfessionalViewModel>();

            try
            {
                var list = new List<MJ_GET_PROFESSIONAL_LIST_Result>();

                using (dbBigMeadJohnson db = new dbBigMeadJohnson())
                {
                    list = db.MJ_GET_PROFESSIONAL_LIST(mrsId).ToList();
                }

                if (list != null)
                {
                    foreach (var row in list)
                    {

                        var objRow = new ListProfessionalViewModel();

                        objRow.Id = (int)row.ID;
                        objRow.FirstName = row.FIRST_NAME;
                        objRow.SecondName = row.SECOND_NAME;
                        objRow.LastName = row.LAS_NAME;
                        objRow.LastUpdate = string.IsNullOrEmpty(row.LAST_UPDATE) ? string.Empty : row.LAST_UPDATE;
                        objRow.PercentCompleteness = (int)row.Completitud;
                        objRow.ProfessionalTypeName = row.NAME;

                        professionals.Add(objRow);
                    }

                    return professionals;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                return null;
            }
        }



        public static int getPercentCompleted(Professional professional)
        {
            var count = 0;
            var totalPropperties = 0;

            if (ValidaNumber(Convert.ToString(professional.ProfessionalTypeId)))
                count += 1;

            if (ValidateString(Convert.ToString(professional.FirstName)))
                count += 1;

            if (ValidateString(Convert.ToString(professional.LastName)))
                count += 1;

            if (ValidaNumber(Convert.ToString(professional.DocumentNumber)))
                count += 1;

            if (ValidaNumber(Convert.ToString(professional.GenderId)))
                count += 1;

            if (ValidateString(Convert.ToString(professional.BirthDate)))
                count += 1;

            if (ValidaNumber(Convert.ToString(professional.MsrId)))
                count += 1;

            if (ValidateString(Convert.ToString(professional.MsrMail)))
                count += 1;

            var professionalContact = ProfessionalBLL.GetProfessionalContactById((int)professional.Id);
            if (professionalContact != null)
            {
                if (ValidateString(Convert.ToString(professionalContact.ProfessionalEmail)))
                    count += 1;

                if (ValidateString(Convert.ToString(professionalContact.ProfessionalAddress)))
                    count += 1;

                if (ValidaNumber(Convert.ToString(professionalContact.ProfessionalCityId)))
                    count += 1;
            }

            var laboralInformation = ProfessionalBLL.GetLaboralInformationByProfessionalId((int)professional.Id);
            if (laboralInformation != null)
            {
                if (professional.ProfessionalTypeId == (int)EnumProfession.Secretaria)
                {

                    if (ValidateString(Convert.ToString(laboralInformation.BossName)))
                        count += 1;

                    if (ValidateString(Convert.ToString(laboralInformation.CurrentDateProfession)))
                        count += 1;

                }
                else
                {
                    if (ValidateString(Convert.ToString(laboralInformation.StartDateProfession)))
                        count += 1;

                    if (laboralInformation.IsSpecialist != null)
                    {
                        count += 1;

                        if (ValidaNumber(Convert.ToString(laboralInformation.SpecialityId)))
                            count += 1;

                        if (ValidateString(Convert.ToString(laboralInformation.Company)))
                            count += 1;
                    }

                    if (laboralInformation.IsStudent != null)
                    {
                        count += 1;

                        if (ValidateString(Convert.ToString(laboralInformation.StartDateStudy)))
                            count += 1;

                        if (ValidateString(Convert.ToString(laboralInformation.EndDateStudy)))
                            count += 1;

                    }
                }
            }

            var generalInformation = ProfessionalBLL.GetFamilyInformationByProfessionalId((int)professional.Id);

            if (generalInformation != null)
            {
                var listSon = generalInformation.Where(f => f.IsFather == true).ToList();
                var listGrandChild = generalInformation.Where(f => f.IsGrandFather == true).ToList();

                if (listGrandChild != null && listSon.Count > 0)
                    count += 1;

                if (listGrandChild != null && listSon.Count > 0)
                    count += 1;
            }

            if (professional.ProfessionalTypeId != (int)EnumProfession.Secretaria)
            {

                var secretaryInformation = ProfessionalBLL.GetSecretaryByProfessionalId((int)professional.Id);
                if (secretaryInformation != null)
                {
                    count += 1;

                    if (ValidateString(Convert.ToString(secretaryInformation.SecretaryFirstName)))
                        count += 1;

                    if (ValidateString(Convert.ToString(secretaryInformation.SecretaryLastName)))
                        count += 1;

                    if (ValidateString(Convert.ToString(secretaryInformation.SecretaryEmail)))
                        count += 1;

                    if (ValidateString(Convert.ToString(secretaryInformation.SecretaryCellPhone)))
                        count += 1;

                    if (secretaryInformation.SecretaryHaveSon != null)
                        count += 1;
                }
            }

            var habeasData = ProfessionalBLL.GetHabeasDataByProfessionalId((int)professional.Id);
            if (habeasData != null)
            {
                if (habeasData.CheckTerms != null)
                    count += 1;

                if (ValidateString(Convert.ToString(habeasData.UrlImage)))
                    count += 1;
            }

            //calcular el total de campos por tipo de profesion 
            if (professional.ProfessionalTypeId != (int)EnumProfession.Secretaria)
            {
                totalPropperties = 21;

                if (laboralInformation != null)
                {
                    if (laboralInformation.IsSpecialist)
                    {
                        totalPropperties += 4;
                    }

                    if (laboralInformation.IsStudent)
                    {
                        totalPropperties += 3;
                    }
                }
            }
            else
            {
                totalPropperties = 17;
            }

            return (count * 100) / totalPropperties;

        }

        private static bool ValidaNumber(string number)
        {
            int n = 0;
            if (string.IsNullOrEmpty(number)) return false;

            if (number.Equals(0)) return false;

            return int.TryParse(number, out n);

        }

        private static bool ValidateString(string str)
        {
            return !string.IsNullOrEmpty(str);
        }

        public static List<MJ_USERS> GetAllRepresentantsByCityId(int cityId)
        {
            List<MJ_USERS> obj = new List<MJ_USERS>();
            try
            {
                var objProfessional = Core.DAL.ProfessionalDAL.GetAllRepresentantsByCityId(cityId);
                
                return objProfessional;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static void AsociateRepresentatsToProfessional(string profesionalId, List<User> asociados)
        {
            List<MJ_PROFESSIONAL_USER> asociadosBll = new List<MJ_PROFESSIONAL_USER>();
            try
            {
                foreach(var item in asociados)
                {
                    var aux = new MJ_PROFESSIONAL_USER();
                    aux.GUID = Guid.NewGuid();
                    aux.PROFESSIONAL_ID =Guid.Parse(profesionalId);
                    aux.USER_ID = item.Guid.ToString();
                    asociadosBll.Add(aux);
                }

                ProfessionalDAL.AsociateRepresentatsToProfessional(asociadosBll);

            }catch(Exception ex)
            {

            }
        }

        public static object GetAllRepresentants()
        {
            throw new NotImplementedException();
        }
               
    }
}
