﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Big.MeadJohnson.Business.Business
{
    public class BrickBLL
    {
        public static ICollection<Models.Brick> GetAllBricks()
        {
            var list = Core.DAL.BrickDAL.GetAllBricks();
            var result = new List<Models.Brick>();

            foreach (var p in list)
            {
                var brick = new Models.Brick
                {
                    Guid = Guid.Parse(p.GUID),
                    Id = (int)p.ID,
                    Code = p.BRICK_CODE,
                    Description = p.BRICK_DES,
                    CityId = (int)p.CYTY_ID,
                    UserId = (int?)p.USER_ID,
                };

                result.Add(brick);
            }

            return result.OrderBy(r => r.Description).ToList();
        }


        public static Models.Brick GetBricksByCode(string code)
        {
            var objBrick = Core.DAL.BrickDAL.GetBricksByCode(code);

            if (objBrick == null)
                return null;

            return  new Models.Brick
            {
                Guid = Guid.Parse(objBrick.GUID),
                Id = (int)objBrick.ID,
                Code = objBrick.BRICK_CODE,
                Description = objBrick.BRICK_DES,
                CityId = (int)objBrick.CYTY_ID,
                UserId = (int?)objBrick.USER_ID,
            };
        }

        public static bool SaveBrick(Models.Brick brick)
        {
            var objBrick = new Core.DAL.MJ_BRICK
            {
              GUID = brick.Guid.ToString(),
              BRICK_CODE = brick.Code,
              CYTY_ID = brick.CityId,
              BRICK_DES = brick.Description,
              USER_ID = brick.UserId,
              ID = brick.Id,
            };

            return Core.DAL.BrickDAL.SaveBrick(objBrick);
        }

    }
}
