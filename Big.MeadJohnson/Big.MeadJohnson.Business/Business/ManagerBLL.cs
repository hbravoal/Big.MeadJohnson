﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Business.Business
{
    public class ManagerBLL
    {
        public static List<Models.Manager> GetAllManagers()
        {
            var managers =  Core.DAL.ManagerDAL.GetAllManagers();
            var result = new List<Models.Manager>();
            foreach (var manager in managers)
            {
                result.Add(new Models.Manager
                {
                    Guid = Guid.Parse(manager.GUID),
                    Id = (int)manager.ID,
                    Names = manager.NAMES,
                   
                });
            }

            return result.OrderBy(u => u.Names).ToList();
        }

    }
}
