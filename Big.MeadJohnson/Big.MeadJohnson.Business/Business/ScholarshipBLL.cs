﻿using System;
using System.Collections.Generic;

namespace Big.MeadJohnson.Business.Business
{
    public class ScholarshipBLL
    {
        public static ICollection<Models.Scholarship> GetAllScholarship()
        {
            try
            {
                var list = new List<Models.Scholarship>();

                var ScholarshipList = Core.DAL.Scholarship.GetAllScholarship();
                foreach (var item in ScholarshipList)
                {
                    var scholarship= new Models.Scholarship
                    {
                        Guid = Guid.Parse(item.GUID),
                        Id = (int)item.ID,
                        Name = item.DESCRIPTION
                    };

                    list.Add(scholarship);
                }

                return list;

            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static ICollection<Models.Scholarship> GetAllScholarshipsdsdsd()
        {
            try
            {
                var list = new List<Models.Scholarship>();

                var ScholarshipList = Core.DAL.Scholarship.GetAllScholarship();
                foreach (var item in ScholarshipList)
                {
                    var scholarship = new Models.Scholarship
                    {
                        Guid = Guid.Parse(item.GUID),
                        Id = (int)item.ID,
                        Name = item.DESCRIPTION
                    };

                    list.Add(scholarship);
                }

                return list;

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
