﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Big.MeadJohnson.Business.Business;
using Big.MeadJohnson.Business.Models;
using Big.MeadJohnson.Core.DAL;
using PagedList;

namespace Big.MeadJohnson.CRM.UI.Controllers.BusinessController
{
    public class GerentesController : Controller
    {
        //private dbBigMeadJohnson db = new dbBigMeadJohnson();

        // GET: Gerentes
        public ActionResult Index(List<Gerente> view, int? page)
        {
            page = (page ?? 1);
            ViewBag.Creado = TempData["CreadoDetalle"];
            return View(GerenteBLL.GetAllGerentes().ToPagedList((int)page, 15));
        }

        // GET: Gerentes/Details/5
        public ActionResult Details(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var gerente = GerenteBLL.ConsultarGerente(id);
            if (gerente == null)
            {
                return HttpNotFound();
            }
            return View(gerente);
        }

        // GET: Gerentes/Create
        public ActionResult Create()
        {
            ViewBag.DepartamentId = new SelectList(DepartamentDAL.GetAllDepartaments(), "ID", "NAME");
            ViewBag.CiudadId = new SelectList(CityDAL.GetAllCities(), "ID", "NAME");
            return View();
        }

        // POST: Gerentes/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "NombreCompleto,Cedula,CorreoElectronico,CiudadId")] Gerente gerente)
        {
            Gerente existeXCedula = GerenteBLL.ConsultarGerenteXCedula(gerente.Cedula);
            if (existeXCedula != null)
            {
                ModelState.AddModelError("Cedula", "La cédula pertenece a " + existeXCedula.NombreCompleto);
            }

            Gerente existeXCorreo = GerenteBLL.ConsultarGerenteXCorreo(gerente.CorreoElectronico);
            if (existeXCorreo != null)
            {
                ModelState.AddModelError("CorreoElectronico", "El Correo existe para " + existeXCorreo.NombreCompleto);
            }

            if (ModelState.IsValid)
            {
                gerente.Activo = true;
                gerente.ID = Guid.NewGuid().ToString();
                GerenteBLL.CrearGerente(gerente);
                TempData["CreadoDetalle"] = "El gerente ha sido creado con éxito";
                return RedirectToAction("Index");
            }

            ViewBag.DepartamentId = new SelectList(DepartamentDAL.GetAllDepartaments(), "ID", "NAME");
            ViewBag.CiudadId = new SelectList(CityDAL.GetAllCities(), "ID", "NAME");
            return View(gerente);
        }

        // GET: Gerentes/Edit/5
        public ActionResult Edit(string id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var gerente = GerenteBLL.ConsultarGerente(id);
            if (gerente == null)
            {
                return HttpNotFound();
            }
            ViewBag.DepartamentId = new SelectList(DepartamentDAL.GetAllDepartaments(), "ID", "NAME");
            return View(gerente);
        }

        // POST: Gerentes/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Gerente gerente)
        {
            Gerente existeXCedula = GerenteBLL.ConsultarGerenteXCedula(gerente.Cedula);
            if (gerente.ID.ToUpper() != existeXCedula.ID.ToUpper())
            {
                if (existeXCedula != null)
                {
                    if (gerente.ID != existeXCedula.ID)
                        ModelState.AddModelError("Cedula", "La cédula pertenece a " + existeXCedula.NombreCompleto);
                }
            }
            Gerente existeXCorreo = GerenteBLL.ConsultarGerenteXCorreo(gerente.CorreoElectronico);
            if (gerente.ID.ToUpper() != existeXCorreo.ID.ToUpper())
            {
                if (existeXCorreo != null)
                {
                    if (gerente.ID != existeXCorreo.ID)
                        ModelState.AddModelError("CorreoElectronico", "El Correo existe para " + existeXCorreo.NombreCompleto);
                }
            }                

            if (ModelState.IsValid)
            {
                var editado = GerenteBLL.EditarGerente(gerente);

                return RedirectToAction("Index");
            }
            ViewBag.DepartamentId = new SelectList(DepartamentDAL.GetAllDepartaments(), "ID", "NAME");
            return View(gerente);
        }

        // GET: Gerentes/Delete/5
        public ActionResult Delete(string id)
        {
            return View();
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //Gerente gerente = db.Gerentes.Find(id);
            //if (gerente == null)
            //{
            //    return HttpNotFound();
            //}
            //return View(gerente);
        }

        // POST: Gerentes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(string id)
        {
            return RedirectToAction("Index");
            //Gerente gerente = db.Gerentes.Find(id);
            //db.Gerentes.Remove(gerente);
            //db.SaveChanges();
            //return RedirectToAction("Index");
        }

        [HttpPost]
        public ActionResult GetCitiesByDepartament(string departamentId)
        {
            int statId;
            List<SelectListItem> districtNames = new List<SelectListItem>();
            if (!string.IsNullOrEmpty(departamentId))
            {
                statId = Convert.ToInt32(departamentId);
                List<City> districts = CityBLL.GetCitiesByDepartamentId(statId).ToList();
                districts.ForEach(x =>
                {
                    districtNames.Add(new SelectListItem { Text = x.Name, Value = x.Id.ToString() });
                });
            }
            return Json(districtNames, JsonRequestBehavior.AllowGet);
        }

        public ActionResult AddCities(string id)
        {
            ViewBag.GERENTE_ID = new SelectList(GerenteDAL.GetAllGerentes().Where(x => x.ID.ToString() == id), "ID", "NOMBRE_COMPLETO", id);
            ViewBag.IDGerente = id;
            ViewBag.DepartamentId = new SelectList(DepartamentDAL.GetAllDepartaments(), "ID", "NAME");
            ViewBag.CITY_ID = new SelectList(CityDAL.GetAllCities(), "GUID", "NAME");
            return View();
        }

        [HttpPost]
        public ActionResult AddCities(MJ_GERENTES_CITIES gerenCities)
        {
            try
            {
                var rta = GerenteBLL.AsociarCiudad(gerenCities);
                if (rta == "OK")
                {
                    TempData["CreadoDetalle"] = "Se ha agregado una ciudad correctamente al gerente";
                    return RedirectToAction("Index");
                }

                else
                {
                    ViewBag.GERENTE_ID = new SelectList(GerenteDAL.GetAllGerentes().Where(x => x.ID == gerenCities.GERENTE_ID), "ID", "NOMBRE_COMPLETO", gerenCities.GERENTE_ID);
                    ViewBag.DepartamentId = new SelectList(DepartamentDAL.GetAllDepartaments(), "ID", "NAME");
                    ViewBag.CITY_ID = new SelectList(CityDAL.GetAllCities(), "ID", "NAME");
                    return View();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                //db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
