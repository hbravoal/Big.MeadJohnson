﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Mvc;
using Big.MeadJohnson.Business.Models;
using Big.MeadJohnson.Core.DAL;
using Big.MeadJohnson.Business.Business;
using Microsoft.AspNet.Identity;
using Big.MeadJohnson.Business.ViewModel;
using System.Linq;
using Big.MeadJohnson.Business.Enums;
using System.Transactions;
using System.Configuration;
using PagedList;
using Big.MeadJohnson.CRM.UI.Models;
using Big.MeadJohnson.Domain.Controller;
using Big.MeadJohnson.CRM.UI.Helper;

namespace Big.MeadJohnson.CRM.UI.Controllers.BusinessController
{
    // [Authorize(Roles = "Administrador,Representantes,Súper_Administrador")]
    public class ProfessionalsController : Controller
    {
        private DataContextLocal db = new DataContextLocal();

        [Authorize(Roles = "Súper_Administrador,Administrador,Representantes")]
        public async Task<ActionResult> Index(ListProfessionalViewModel view,int? page = null )
        {
            Session["professionalId"] = null;
            page = (page ?? 1);
            var profesionalViewModel = new List<ListProfessionalViewModel>();
            

            //var professionals = await ProfessionalBLL.GetAllProfessionals();
            var professionalType = ProfessionalBLL.GetAllProfessionalTypes();

            var listProfessionals = new List<Professional>();
            var roles = Helper.UserHelper.GetRolesByUser(User.Identity.GetUserName());

            if (roles.Where(rol => rol == "Súper_Administrador").FirstOrDefault() == "Súper_Administrador" ||
                roles.Where(rol => rol == "Administrador").FirstOrDefault() == "Administrador")
                profesionalViewModel = ProfessionalBLL.GetListProfessionals(null);
            else if (roles.Where(rol => rol == "Representantes").FirstOrDefault() == "Representantes")
            {
                var user = UserBLL.GetUserByUserName(User.Identity.GetUserName());
                profesionalViewModel = ProfessionalBLL.GetListProfessionals(user.Id).ToList();
            }
            if (!string.IsNullOrEmpty(view.FirstName))
            {
             var   profesionalViewMode = profesionalViewModel.Where(m => m.FullName.ToUpper().Contains(view.FirstName.ToUpper()));
                return View(profesionalViewMode.OrderBy(p => p.FullName).ToPagedList((int)page, 15));
            }


           else { 
                return View(profesionalViewModel.OrderBy(p => p.FullName).ToPagedList((int)page, 15));
            }
        }

        #region Creacion Profesionales

        [Authorize(Roles = "Súper_Administrador")]
        #region Profesionales
        public ActionResult Create()
        {
            var model = new ProfessionalViewModel
            {
                Professional = new Professional { Guid = Guid.NewGuid() },
                ProfessionalContact = new ProfessionalContact(),
                ProfessionalLaborInformation = new LaborInformation(),
            };
            return View(model);
        }

        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult CreateProfessional()
        {
            var professionalType = ProfessionalBLL.GetAllProfessionalTypes();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();
            var genders = GenderBLL.GetAllGender();
            var users = new List<User>();

            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name");
            ViewBag.CityId = new SelectList(cities, "Id", "Name");
            ViewBag.ProfessionalTypeId = new SelectList(professionalType, "Id", "Name");
            ViewBag.GenderId = new SelectList(genders, "Id", "Name");
            ViewBag.MsrId = new SelectList(users, "Id", "Name");

            var model = new Professional { Guid = Guid.NewGuid(), };
            return PartialView("~/Views/Professionals/_CreateProfessional.cshtml", model);
        }

        [Authorize(Roles = "Súper_Administrador")]
        [HttpPost]
        public ActionResult CreateProfessional(Professional model)
        {
            var response = new Response<ProfessionalContact>();
            if (Session["professionalId"] != null)
            {
                var professionalId = Convert.ToInt32(Session["professionalId"]);
                var professional = ProfessionalBLL.GetProfessionalById((int)professionalId);

                if (professional != null)
                {
                    var fail = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "El profesional ya ha sido registrado.",
                        Title = "Administracion Profesionales"
                    };

                    return Json(fail);
                }
            }


            var professionalType = ProfessionalBLL.GetAllProfessionalTypes();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();
            var genders = GenderBLL.GetAllGender();
            var users = new List<User>();

            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", model.DepartamentId);
            ViewBag.CityId = new SelectList(cities, "Id", "Name", model.CityId);
            ViewBag.ProfessionalTypeId = new SelectList(professionalType, "Id", "Name", model.ProfessionalTypeId);
            ViewBag.GenderId = new SelectList(genders, "Id", "Name", model.GenderId);
            ViewBag.MsrId = new SelectList(users, "Id", "Name", model.MsrId);

            model.RegisterDate = DateTime.Now;
            // model.LastUpdate = DateTime.Now;
            model.Id = 0;

            if (model.MsrId != null && model.MsrId != 0)
                model.MsrMail = UserBLL.GetUserById((int)model.MsrId).Email;
            else
                ModelState.AddModelError(string.Empty, "El campo representante es requerido.");

            model.CreateBy = UserBLL.GetUserByUserName(User.Identity.GetUserName()).Id;

            try
            {
                model.BirthDate = (DateTime)ValidateDate(model.BirthDay,
                            model.BirthMonth, model.BirthYear);
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(string.Empty, "La fecha ingresada no es valida.");
            }

            if (model.DocumentNumber != null)
            {
                var objProfessional = ProfessionalBLL.GetProfessionalByDocumentNumber(Convert.ToString(model.DocumentNumber));

                if (objProfessional != null)
                    ModelState.AddModelError(string.Empty, "El numero de cédula ya se encuentra registrado.");
            }
            List<User> dataRepreSecundarios = new List<User>();

            if (model.RepresentantesSecundarios != null)
            {
                string[] ass = model.RepresentantesSecundarios.Split('|');
                
                if (ass != null && ass.FirstOrDefault() != "")
                {
                    //ViewBag.AssociatedProfessionals = convesion;

                    foreach (var item in ass)
                    {
                        if (item != "")
                        {
                            var data2 = UserBLL.GetUserByGuid(item);
                            dataRepreSecundarios.Add(data2);
                        }
                    }
                    ViewBag.RepresentantesSecundarios = new SelectList(dataRepreSecundarios, "Guid", "Name");
                }
            }

            if (ModelState.IsValid)
            {
                model.Token = Guid.NewGuid().ToString();
                var expirationDate = ConfigurationManager.AppSettings["Token.Expiration.Date"];
                model.ExpirationDateToken = Convert.ToDateTime(model.RegisterDate).AddDays(Convert.ToDouble(expirationDate));
                var professionalId = ProfessionalBLL.SaveNewProfessional(model);
                if (professionalId != null)
                {
                    Session["professionalId"] = professionalId;
                    var professionalContact = new ProfessionalContact()
                    {
                        Id = 0,
                        Guid = Guid.NewGuid(),
                        ProfessionalId = (int)professionalId
                    };

                    professionalContact.Id = (int)ProfessionalBLL.SaveProfessionalContact(professionalContact);
                    var nuevoProfesional = ProfessionalBLL.GetProfessionalById(professionalId.Value);
                    ProfessionalBLL.AsociateRepresentatsToProfessional(nuevoProfesional.Guid.ToString().ToUpper(), dataRepreSecundarios);

                    response = new Response<ProfessionalContact>()
                    {
                        Result = true,
                        Model = professionalContact,
                        Message = "El registro ha sido creado con éxito.",
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<ProfessionalContact>()
                {
                    Result = false,
                    Model = null,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }
        #endregion

        #region Informacion de contacto
        [Authorize(Roles = "Súper_Administrador")]
        [HttpGet]
        public ActionResult ProfessionalContact()
        {
            var professionalId = Session["professionalId"];
            var professionalContact = new ProfessionalContact();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();

            ViewBag.ProfessionalDepartamentId = new SelectList(departaments, "Id", "Name");
            ViewBag.ProfessionalCityId = new SelectList(cities, "Id", "Name");
            var bricks = new List<Brick>();
            ViewBag.ProfessionalBrickId = new SelectList(bricks, "Code", "Code");
         


            if (professionalId != null)
                professionalContact = ProfessionalBLL.GetProfessionalContactById((int)professionalId);

            return PartialView("~/Views/Professionals/_ProfessionalContact.cshtml", professionalContact);
        }

        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult SaveProfessionalContact(ProfessionalContact model)
        {
            var response = new Response<bool>();
            if (Session["professionalId"] != null)
            {
                var professionalId = Session["professionalId"];
                model.ProfessionalId = (int)professionalId;
                var professionalContact = new ProfessionalContact();
                var departaments = DepartamentBLL.GetAllDepartaments();
                var cities = new List<City>();
                ViewBag.ProfessionalDepartamentId = new SelectList(departaments, "Id", "Name", model.ProfessionalDepartamentId);
                ViewBag.ProfessionalCityId = new SelectList(cities, "Id", "Name", model.ProfessionalCityId);
                var bricks = new List<Brick>();
                if (model.ProfessionalCityId != null && model.ProfessionalCityId != 0)
                    bricks = BrickBLL.GetAllBricks().Where(b => b.CityId == model.ProfessionalCityId).ToList();

                ViewBag.ProfessionalBrickId = new SelectList(bricks, "Code", "Code", model.ProfessionalBrickId);

                if (!string.IsNullOrEmpty(model.ProfessionalEmail))
                {
                    var validationEmail = ProfessionalBLL.GetProfessionalContactByEmail(model.ProfessionalEmail);
                    if (validationEmail != null)
                        ModelState.AddModelError(string.Empty, "El correo ingrasado ya se encuantra registrado.");
                }

                if (professionalId != null)
                {
                    professionalContact = ProfessionalBLL.GetProfessionalContactById((int)professionalId);
                    model.Guid = professionalContact.Guid;
                    model.Id = professionalContact.Id;
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "Debe registrar un profesional.",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }

            if (ModelState.IsValid)
            {
                var result = ProfessionalBLL.UpdateProfessionalContact(model);
                if (result)
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = result,
                        Message = "El registro ha sido creado con éxito.",
                        Title = "Administracion Profesionales"
                    };

                    UpdateLastUpdateByProfessional(model.ProfessionalId);
                }
                else
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error al guardar la información de contacto. Intente nuevamnete.",
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }
        #endregion

        #region Informacion laboral
        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult LaborInformation()
        {
            int? professionalId = Convert.ToInt32(Session["professionalId"]);
            var specialities = new List<Business.Models.Speciality>();
            var professional = new Professional();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();

            ViewBag.ProfessionalDepartamentId = new SelectList(departaments, "Id", "Name");
            ViewBag.ProfessionalCityId = new SelectList(cities, "Id", "Name");
            if (professionalId != null && professionalId != 0)
            {
                professional = ProfessionalBLL.GetProfessionalById((int)professionalId);
                specialities = SpecialityBLL.GetSpecialitiesByProfesionalTypeId((int)professional.ProfessionalTypeId);
                specialities.Add(new Business.Models.Speciality { Id = 100, Name = "Otra" });
            }

            var professionalLevels = ProfessionalBLL.GetAllProfessionalLevels();
            ViewBag.ProfessionalLevelId = new SelectList(professionalLevels, "Id", "Level");
            ViewBag.SpecialityId = new SelectList(specialities, "Id", "Name");
            ViewBag.ProfesionalTypeId = professional.ProfessionalTypeId != null ? professional.ProfessionalTypeId : 0;
            var model = new LaborInformation();

            return PartialView("~/Views/Professionals/_LaborInformation.cshtml", model);
        }

        public ActionResult SaveLaborInformation(LaborInformation model)
        {
            var response = new Response<bool>();
            int? professionalId = null;

            if (Session["professionalId"] != null)
            {
                professionalId = Convert.ToInt32(Session["professionalId"]);
                model.ProfessionalId = (int)professionalId;
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "Debe registrar un profesional.",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }

            var professional = ProfessionalBLL.GetProfessionalById((int)professionalId);
            DateTime? startProfessionDate = null;
            DateTime? startdateSpeciality = null;
            DateTime? currentProfessionYear = null;
            
            DateTime? endProfessionDate = null;
            if (professional.ProfessionalTypeId != (int)EnumProfession.Secretaria)
            {
                try
                {
                    
                    startProfessionDate = ValidateDateAllRequerid(model.StartProfessionDay,
                     model.StartProfessionMonth, model.StartProfessionYear);

                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "La fecha ingresada no es valida.");
                }

                if (startProfessionDate == null)
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó a ejercer su profesión.");
                //Si es especialista
                if (model.IsSpecialist)
                {                   

                    try
                    {
                        startdateSpeciality = ValidateDateAllRequerid(model.StartStudyDay, model.StartStudyMonth, model.StartStudyYear);
                        if (startdateSpeciality == null)
                            ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual inició su especialidad.");

                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual inició su especialidad.");
                    }

                    try
                    {
                        endProfessionDate = ValidateDateAllRequerid(model.EndStudyDay, model.EndStudyMonth, model.EndStudyYear);
                        if (endProfessionDate == null)
                            ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual terminó su especialidad.");

                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual terminó su especialidad.");
                    }
                                        
                }
                
            }
            else
            {
                try
                {

                    currentProfessionYear  = ValidateDateAllRequerid(model.CurrentProfessionDay,
                     model.CurrentProfessionMonth, model.CurrentProfessionYear);
                    if (currentProfessionYear == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida.");
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "La fecha ingresada no es valida.");
                }

                if (( model.AssociatedProfessionals == null) && (model.BossName == "" || model.BossName == null))
                    ModelState.AddModelError(string.Empty, "Debe Asignar  un professional por algún medio (Lista y/o Campo de texto).");

                //if (string.IsNullOrEmpty(model.BossName))
                //    ModelState.AddModelError(string.Empty, "Debe ingresar el nombre del profesional para el trabaja.");
            }

            model.StartDateProfession = startProfessionDate;

            if (model.IsSpecialist && (model.SpecialityId == 0 || model.SpecialityId == null))
                ModelState.AddModelError(string.Empty, "Debe seleccionar una especialidad.");

            if (model.SpecialityId == 100 && string.IsNullOrEmpty(model.OtherSpeciality))
                ModelState.AddModelError(string.Empty, "Debe seleccionar una especialidad de la lista o ingresar la especialidad en el campo otra.");


            if (!model.IsSpecialist && (model.SpecialityId == 0 || model.SpecialityId == null))
                model.SpecialityId = null;

            if (model.IsStudent && (model.ProfessionalLevelId == 0 || model.ProfessionalLevelId == null))
                ModelState.AddModelError(string.Empty, "Debe seleccionar el nivel profesional.");

            DateTime? startStudyDate = null;
            DateTime? endStudyDate = null;
            if (model.IsStudent)
            {
                try
                {
                    startStudyDate = ValidateDate(model.StartStudyDay,
                                model.StartStudyMonth, model.StartStudyYear);

                    if (startStudyDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó los estudios.");
                    else
                    {
                        if (DateTime.Compare((DateTime)startStudyDate, DateTime.Now) > 0)
                            ModelState.AddModelError(string.Empty, "La fecha en la que comenzó los estudios debe ser menor a la fecha actual.");
                    }
                }
                catch (Exception)
                {

                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó los estudios.");
                }

                try
                {
                    endStudyDate = ValidateDate(model.EndStudyDay,
                               model.EndStudyMonth, model.EndStudyYear);

                    if (endStudyDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual finalizará los estudios.");
                    else
                    {
                        if (DateTime.Compare(DateTime.Now, (DateTime)endStudyDate) > 0)
                            ModelState.AddModelError(string.Empty, "La fecha estimada de finalización de estudios debe ser mayor a la fecha actual.");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual finalizará los estudios.");
                }
            }

            model.StartDateStudy = startStudyDate;
            model.EndDateStudy = endStudyDate;

            if (ModelState.IsValid)
            {
                int? result = null;
                if (model.SpecialityId == 100 && !string.IsNullOrEmpty(model.OtherSpeciality))
                {
                    var saveSpeciality = SpecialityBLL.SaveSpeciality(new Business.Models.Speciality
                    {
                        Guid = Guid.NewGuid(),
                        Id = 0,
                        IsActive = true,
                        Name = model.OtherSpeciality.First().ToString().ToUpper() + model.OtherSpeciality.Substring(1).ToLower(),
                        ProfessionalTypeId = professional.ProfessionalTypeId,
                    });

                    if (saveSpeciality != null)
                    {
                        model.SpecialityId = saveSpeciality;
                        result = ProfessionalBLL.SaveLabolarInformation(model);
                    }
                    else
                        response = new Response<bool>()
                        {
                            Result = false,
                            Model = false,
                            Message = "Ha ocurrido un error al guardar la nueva especialidad. Intente nuevamnete.",
                            Title = "Administracion Profesionales"
                        };
                }
                else
                    result = ProfessionalBLL.SaveLabolarInformation(model);

                if (result != null)
                {

                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = false,
                        Message = "El registro ha sido creado con éxito.",
                        Title = "Administracion Profesionales"
                    };

                    UpdateLastUpdateByProfessional(model.ProfessionalId);
                }
                else
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error al guardar la información laboral. Intente nuevamnete.",
                        Title = "Administracion Profesionales"
                    };
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        #endregion

        #region Informacion General
        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult GeneralInformation()
        {
            var professionalId = Session["professionalId"];
            var GeneralInformation = new GeneralInformation();

            var scholarships = ScholarshipBLL.GetAllScholarship();
            ViewBag.SchoolYearSon = new SelectList(scholarships, "Id", "Name");
            ViewBag.SchoolYearGrandChild = new SelectList(scholarships, "Id", "Name");
            var months = DataHelper.MotnGenerate();
            ViewBag.Months = new SelectList(months, "Value", "Text");

            var days = DataHelper.NumbersGenerate(31);
            ViewBag.Days = new SelectList(days, "Value", "Text");
            var type = DataHelper.TypeAgeGenerate();
            ViewBag.TypeAge = new SelectList(type, "Value", "Text");
            return PartialView("~/Views/Professionals/_GeneralInformation.cshtml", GeneralInformation);
        }

        public ActionResult SaveGeneralInformation(GeneralInformation model)
        {
            int? professionalId = null;
            var response = new Response<bool>();

            if (Session["professionalId"] != null)
            {
                professionalId = Convert.ToInt32(Session["professionalId"]);
                model.ProfessionalId = (int)professionalId;

                if (model.IsFather && model.ListSon == null)
                    ModelState.AddModelError(string.Empty, "Si ha indicado tener hijos debe ingresar mínimo uno en la lista.");

                if (model.IsGrandFather && model.ListGrandchild == null)
                    ModelState.AddModelError(string.Empty, "Si ha indicado tener nietos debe ingresar mínimo uno en la lista.");
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "Debe registrar un profesional.",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        var objSon = new FamilyInformation
                        {
                            Id = 0,
                            ProfessionalId = (int)professionalId,
                            IsFather = false,
                            IsGrandFather = false,
                        };

                        if (model.IsFather)
                        {
                            if (model.ListSon != null && model.ListSon.Count > 0)
                            {
                                foreach (var son in model.ListSon)
                                {
                                    objSon.Guid = Guid.NewGuid();
                                    objSon.Name = son.Name;
                                    objSon.Age = son.Age;
                                    objSon.SchoolYear = son.SchoolYear;
                                    objSon.IsFather = true;
                                    objSon.IsYearAge = son.IsYearAge;
                                    objSon.IsGrandFather = false;


                                    ProfessionalBLL.SaveFamilyInformation(objSon);
                                };
                            }
                        }

                        if (model.IsGrandFather)
                        {
                            if (model.ListGrandchild != null && model.ListGrandchild.Count > 0)
                            {
                                foreach (var son in model.ListGrandchild)
                                {
                                    objSon.Guid = Guid.NewGuid();
                                    objSon.Name = son.Name;
                                    objSon.Age = son.Age;
                                    objSon.IsYearAge = son.IsYearAge;
                                    objSon.SchoolYear = son.SchoolYear;
                                    objSon.IsGrandFather = true;
                                    objSon.IsFather = false;

                                    ProfessionalBLL.SaveFamilyInformation(objSon);
                                };
                            }
                        }
                        var objGonnaDaddy = new GonnaDaddy();

                        //model.ListGonnaDaddy.FirstOrDefault().GonnaDaddyQuestion = false;
                        if (model.ListGonnaDaddy != null)
                        {
                            if (model.ListGonnaDaddy.Any())

                            {
                                foreach (var son in model.ListGonnaDaddy)
                                {

                                    objGonnaDaddy.MonthStimated = son.MonthStimated;
                                    objGonnaDaddy.DayStimated = son.DayStimated;
                                    objGonnaDaddy.GonnaDaddyQuestion = true;
                                    objGonnaDaddy.Professional_id = model.ProfessionalId;

                                    ProfessionalBLL.SaveGonnaDaddy(objGonnaDaddy);
                                };
                            }
                        }

                        if (model.ListGrandchild == null && model.ListSon == null)
                        {
                            objSon.Guid = Guid.NewGuid();
                            ProfessionalBLL.SaveFamilyInformation(objSon);
                        }


                        scope.Complete();
                        response = new Response<bool>()
                        {
                            Result = true,
                            Model = true,
                            Message = "El registro ha sido creado con éxito.",
                            Title = "Administracion Profesionales"
                        };

                        UpdateLastUpdateByProfessional(model.ProfessionalId);
                    }
                }
                catch (Exception ex)
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error. Intente nuevamente o comuniquese con el administrador." + ex.Message,
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        #endregion

        #region Informacion Secretaria
        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult SecretaryInformation()
        {
            var professionalId = Session["professionalId"];
            var SecretaryInformation = new SecretaryInformation();
            return PartialView("~/Views/Professionals/_SecretaryInformation.cshtml", SecretaryInformation);
        }

        [HttpPost]
        public JsonResult ValidateSecretaryExits(string cedula)
        {
           
            var secretaryInformation = ProfessionalBLL.GetSecretaryByDocument(cedula);
            if (secretaryInformation == null)
            {
                secretaryInformation = new SecretaryInformation
                {
                    PropertyMessage = "No se encontraron registros",

                };
            }

            if (secretaryInformation.SecretaryBirthDate != null)
            {
                var dataResult = ProfessionalBLL.GetHabeasDataBySecretaryId(secretaryInformation.SecretaryId);
                if(dataResult!=null)
                {
                    secretaryInformation.SecretaryHabbeasData = dataResult.UrlImage;
                }
                secretaryInformation.SecretaryBirthMonth = secretaryInformation.SecretaryBirthDate.Value.Month;
                secretaryInformation.SecretaryBirthDay = secretaryInformation.SecretaryBirthDate.Value.Day;
                secretaryInformation.SecretaryBirthYear=secretaryInformation.SecretaryBirthDate.Value.Year;
                secretaryInformation.SecretaryAddress = secretaryInformation.SecretaryAddress;
                secretaryInformation.SecretaryHaveSon = secretaryInformation.SecretaryHaveSon;                
                secretaryInformation.SecretaryCellPhone = secretaryInformation.SecretaryCellPhone;
                secretaryInformation.SecretaryCompany = secretaryInformation.SecretaryCompany;
                secretaryInformation.SecretaryFirstName = secretaryInformation.SecretaryFirstName;
                secretaryInformation.SecretaryLastName = secretaryInformation.SecretaryLastName;

            }

            //UpdateSecretaryInformation


            return Json(secretaryInformation);
            
        }
        public ActionResult SaveSecretaryInformation(SecretaryInformation model)
        {
            var response = new Response<bool>();
            if (Session["professionalId"] != null)
            {
                var professionalId = Session["professionalId"];
                model.ProfessionalId = (int)professionalId;

                if (model.SecretaryHaveSecretary)
                {
                    if (string.IsNullOrEmpty(model.SecretaryFirstName))
                        ModelState.AddModelError(string.Empty, "El campo Nombre es requerido.");

                    if (string.IsNullOrEmpty(model.SecretaryLastName))
                        ModelState.AddModelError(string.Empty, "El campo Apellido es requerido.");

                    if (string.IsNullOrEmpty(model.SecretaryEmail))
                        ModelState.AddModelError(string.Empty, "El campo Correo es requerido.");

                    if (string.IsNullOrEmpty(model.SecretaryCellPhone))
                        ModelState.AddModelError(string.Empty, "El campo Celular es requerido.");

                    try
                    {
                        var birthDate = ValidateDate(model.SecretaryBirthDay,
                                   model.SecretaryBirthMonth, model.SecretaryBirthYear);

                        if (birthDate == null)
                            ModelState.AddModelError(string.Empty, "Debe ingresar una fecha de nacimiento valida.");
                        else
                            model.SecretaryBirthDate = birthDate;
                    }
                    catch (Exception)
                    {
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha de nacimiento valida.");
                    }
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "Debe registrar un profesional.",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }

            if (ModelState.IsValid)
            {
                int? result = null;
                if (model.SecretaryHaveSecretary)
                {
                    var cant= ProfessionalDAL.GetSecretaryDocument(model.SecretaryDocumentNumber);
                    
                    model.SecretaryGuid = Guid.NewGuid();
                    var secretaries=ProfessionalBLL.GetAllSecretaryByDocument(model.SecretaryDocumentNumber);
                    result = ProfessionalBLL.SaveSecretaryInformation(model);
                    foreach (var item in secretaries)
                    {
                        item.SecretaryAddress = model.SecretaryAddress;
                        item.SecretaryBirthDate = model.SecretaryBirthDate;
                        item.SecretaryCellPhone = model.SecretaryCellPhone;
                        item.SecretaryCompany = model.SecretaryCompany;
                        item.SecretaryEmail = model.SecretaryEmail;
                        item.SecretaryFirstName = model.SecretaryFirstName;
                        item.SecretaryLastName = model.SecretaryLastName;
                        item.SecretaryHaveSon = model.SecretaryHaveSon;
                        ProfessionalBLL.UpdateSecretaryInformation(item);
                    }
                 
                }
                else
                    result = 0;

                if (result != null)
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = true,
                        Message = "El registro ha sido creado con éxito.",
                        Title = "Administracion Profesionales"
                    };

                    UpdateLastUpdateByProfessional(model.ProfessionalId);
                }
                else
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error al guardar la información de la secretaria. Intente nuevamnete.",
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);

        }

        #endregion

        #region Firma Digital
        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult DigitalSignature()
        {
            return PartialView("~/Views/Professionals/_DigitalSignature.cshtml");
        }
        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult DigitalSignatureSecretary()
        {
            return PartialView("~/Views/Professionals/_DigitalSignatureSecretary.cshtml");
        }
        #endregion


        #endregion

        #region Edicion Profesionales

        [AllowAnonymous]
        [HttpGet]
        public ActionResult EditAnonymousProfessional(string token)
        {
            if (string.IsNullOrEmpty(token))
                return RedirectToAction("Index");

            var professional = ProfessionalBLL.GetProfessionalByToken(token);

            if (professional == null)
                RedirectToAction("Index");

            var model = new EditProfessionalViewModel
            {
                ProfessionalId = professional.Id,
                FirstName = professional.FirstName,
                LastName = professional.LastName,
            };

            var validationDateToken = DateTime.Compare(DateTime.Now, (DateTime)professional.ExpirationDateToken);

            if (validationDateToken > 0)
            {
                ModelState.AddModelError(string.Empty, "El token de autenticación ha caducado.");
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult EditAnonymousProfessional(EditProfessionalViewModel model)
        {
            if (model.CheckTerms)
            {
                var professional = ProfessionalBLL.GetProfessionalById((int)model.ProfessionalId);
                if (professional != null)
                {
                    var validationDateToken = DateTime.Compare(DateTime.Now, (DateTime)professional.ExpirationDateToken);

                    if (validationDateToken > 0)
                    {
                        ModelState.AddModelError(string.Empty, "El token de autenticación ha caducado.");
                        return View(model);
                    }


                    var habeasData = ProfessionalBLL.GetHabeasDataByProfessionalId((int)model.ProfessionalId);

                    if (habeasData == null)
                    {
                        habeasData = new HabeasData
                        {
                            Guid = Guid.NewGuid(),
                            CheckTerms = true,
                            StartTermsDate = DateTime.Now,
                            ProfessionalId = (int)professional.Id,
                            LastUpdateTermsDate = DateTime.Now
                        };
                    }
                    else
                    {
                        habeasData.LastUpdateTermsDate = DateTime.Now;
                        habeasData.CheckTerms = true;
                    }

                    if (ProfessionalBLL.SaveProfessionalHabeasData(habeasData))
                        return RedirectToAction("Edit/" + model.ProfessionalId);

                }

            }

            ModelState.AddModelError(string.Empty, "Debe Aceptar el uso de informacion personal.");
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Edit(int? id)
        {
            HabeasData secretaryHabeasData = new HabeasData();
            if (id == null)
                RedirectToAction("Index");

            var professional = ProfessionalBLL.GetProfessionalById((int)id);

            if (professional == null)
                RedirectToAction("Index");

            var professionalContact = ProfessionalBLL.GetProfessionalContactById((int)professional.Id);
            var laboralInformation = ProfessionalBLL.GetLaboralInformationByProfessionalId((int)professional.Id);
            var generalInformation = new GeneralInformation { ProfessionalId = (int)professional.Id };
            var secretaryInformation = new SecretaryInformation { ProfessionalId = (int)professional.Id};
            var gonnaDaddy = ProfessionalBLL.GetGonnaDaddyByProfessionalId((int)professional.Id);
            var habeasData = ProfessionalBLL.GetHabeasDataByProfessionalId((int)professional.Id);
            var secretary = ProfessionalBLL.GetSecretaryByProfessionalId((int)professional.Id);
            if(secretary!=null  )
            {
                secretaryHabeasData = ProfessionalBLL.GetHabeasDataBySecretaryId(secretary.SecretaryId);
            }
            if (laboralInformation==null){
            
                    laboralInformation = new Business.Models.LaborInformation
                    {
                        ProfessionalId=(int)professional.Id,
                    };
            }
            
            
            
            int? roleId = null;

            if (User.Identity.IsAuthenticated)
            {
                var user = UserBLL.GetUserByUserName(User.Identity.GetUserName());
                roleId = user.ProfileId;
            }
            var model = new ProfessionalViewModel
            {
                Professional = professional,
                ProfessionalContact = professionalContact,
                ProfessionalLaborInformation = laboralInformation,
                GeneralInformation = generalInformation,
                SecretaryInformation = secretaryInformation,
                HabeasData = habeasData != null ? habeasData : new HabeasData(),
                GonnaDaddy = gonnaDaddy,
                RoleId = roleId,
                SecretaryHabeasData= secretaryHabeasData,
            };
            Session["professionalId"] = (int)professional.Id;
            return View(model);
        }

        //EditProfessional

        public ActionResult EditProfessional(Professional professional)
        {

            var professionalType = ProfessionalBLL.GetAllProfessionalTypes();
            ViewBag.ProfessionalTypeId = new SelectList(professionalType, "Id", "Name", professional);
            var genders = GenderBLL.GetAllGender();
            ViewBag.GenderId = new SelectList(genders, "Id", "Name", professional.GenderId != null);


            var user = UserBLL.GetUserById((int)professional.MsrId);
            var cities = CityBLL.GetAllCities();
            City city = null;
            if (user.CityId != null)
                city = cities.Where(c => c.Id == user.CityId).FirstOrDefault();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var users = new List<Business.Models.User>();

            if (city != null)
            {

                professional.DepartamentId = departaments.Where(d => d.Id == city.DepartamentId).FirstOrDefault().Id;
                professional.CityId = city.Id;
                users = UserBLL.GetAUserByCityId((int)professional.CityId).ToList();
                ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", professional.DepartamentId);
                ViewBag.CityId = new SelectList(cities.Where(c => c.DepartamentId == professional.DepartamentId).ToList().OrderBy(c => c.Name), "Id", "Name", professional.CityId);
                ViewBag.MsrId = new SelectList(users, "Id", "Name", (int)professional.MsrId);
            }
            else
            {
                ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name");
                ViewBag.CityId = new SelectList(cities.Where(c => c.DepartamentId == professional.DepartamentId).OrderByDescending(d => d.Name), "Id", "Name");
                ViewBag.MsrId = new SelectList(users, "Id", "Name");
            }


            if (professional.BirthDate != null)
            {
                var date = Convert.ToDateTime(professional.BirthDate);
                professional.BirthDay = Convert.ToInt32(date.ToString("dd"));
                professional.BirthMonth = Convert.ToInt32(date.ToString("MM"));
                professional.BirthYear = Convert.ToInt32(date.ToString("yyyy"));
            }

            //ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", professional.DepartamentId);
            //ViewBag.CityId = new SelectList(cities.Where(c => c.DepartamentId == professional.DepartamentId).ToList(), "Id", "Name", professional.CityId);
            //ViewBag.MsrId = new SelectList(users, "Id", "Name", (int)professional.MsrId);

            return PartialView("~/Views/Professionals/_EditProfessional.cshtml", professional);

        }

        public ActionResult EditProfessionalContact(ProfessionalContact professionalContact)
        {
            var bricks = BrickBLL.GetAllBricks().OrderBy(c=>c.Code);
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = CityBLL.GetAllCities();

            var brick = bricks.Where(b => b.Code == professionalContact.ProfessionalBrickId).FirstOrDefault();

            var city = cities.Where(c => c.Id == professionalContact.ProfessionalCityId).FirstOrDefault();
            if (city != null)
            {
                professionalContact.ProfessionalDepartamentId = departaments.Where(d => d.Id == city.DepartamentId).FirstOrDefault().Id;
                professionalContact.ProfessionalCityId = city.Id;
                ViewBag.ProfessionalBrickId = new SelectList(bricks.Where(b => b.CityId == city.Id), "Code", "Code", professionalContact.ProfessionalBrickId);
            }
            else
            {
                ViewBag.ProfessionalBrickId = new SelectList(new List<Brick>(), "Code", "Code", professionalContact.ProfessionalBrickId);
            }


            ViewBag.ProfessionalDepartamentId = new SelectList(departaments, "Id", "Name", professionalContact.ProfessionalDepartamentId);
            ViewBag.ProfessionalCityId = new SelectList(cities.Where(c => c.DepartamentId == professionalContact.ProfessionalDepartamentId).ToList().OrderBy(d => d.Name), "Id", "Name", professionalContact.ProfessionalCityId);

            return PartialView("~/Views/Professionals/_EditProfessionalContact.cshtml", professionalContact);
        }

        public ActionResult EditLaborInformation(LaborInformation laborInformation)
        {
            var professional = ProfessionalBLL.GetProfessionalById((int)laborInformation.ProfessionalId);
            var specialities = new List<Business.Models.Speciality>();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();
            specialities = SpecialityBLL.GetSpecialitiesByProfesionalTypeId((int)professional.ProfessionalTypeId);
            specialities.Add(new Business.Models.Speciality { Id = 100, Name = "Otra" });

            var professionalLevels = ProfessionalBLL.GetAllProfessionalLevels();
            ViewBag.ProfessionalLevelId = new SelectList(professionalLevels, "Id", "Level", laborInformation.ProfessionalLevelId);
            ViewBag.SpecialityId = new SelectList(specialities, "Id", "Name", laborInformation.SpecialityId);
            ViewBag.ProfesionalTypeId = professional.ProfessionalTypeId != null ? professional.ProfessionalTypeId : 0;
            ViewBag.ProfessionalDepartamentId = new SelectList(departaments, "Id", "Name");
            ViewBag.ProfessionalCityId = new SelectList(cities, "Id", "Name");
            if (laborInformation.StartDateProfession != null)
            {
                var date = Convert.ToDateTime(laborInformation.StartDateProfession);
                laborInformation.StartProfessionDay = Convert.ToInt32(date.ToString("dd"));
                laborInformation.StartProfessionMonth = Convert.ToInt32(date.ToString("MM"));
                laborInformation.StartProfessionYear = Convert.ToInt32(date.ToString("yyyy"));
            }

            if (laborInformation.StartDateStudy != null)
            {
                var date = Convert.ToDateTime(laborInformation.StartDateStudy);
                laborInformation.StartStudyDay = Convert.ToInt32(date.ToString("dd"));
                laborInformation.StartStudyMonth = Convert.ToInt32(date.ToString("MM"));
                laborInformation.StartStudyYear = Convert.ToInt32(date.ToString("yyyy"));
            }

            if (laborInformation.EndDateStudy != null)
            {
                var date = Convert.ToDateTime(laborInformation.EndDateStudy);
                laborInformation.EndStudyDay = Convert.ToInt32(date.ToString("dd"));
                laborInformation.EndStudyMonth = Convert.ToInt32(date.ToString("MM"));
                laborInformation.EndStudyYear = Convert.ToInt32(date.ToString("yyyy"));
            }
            if (laborInformation.AssociatedProfessionals != null)
            {
                
                string[] ass= laborInformation.AssociatedProfessionals.Split('|');
                List<Professional> data = new List<Professional>();
                if (ass != null && ass.FirstOrDefault() != ""){
                    //ViewBag.AssociatedProfessionals = convesion;
                    
                    foreach (var item in ass)
                    {
                        if (item != "")
                        {
                            var data2 = ProfessionalBLL.GetProfessionalCustomById(Convert.ToInt32(item));
                            data.Add(new Professional
                            {
                                FirstName = data2.FirstName +" "+data2.SecondName + " "+data2.LastName +" " +data2.SecondLastName,
                                Id= data2.Id,
                            }
                                
                                
                                );

                        }
                        
                    }
                    ViewBag.AssociatedProfessionals = new SelectList(data, "Id", "FirstName");
                }
              
            }
            return PartialView("~/Views/Professionals/_EditLaborInformation.cshtml", laborInformation);
        }


        public ActionResult EditGeneralInformation(GeneralInformation generalInformation)
        {
            var professionalId = generalInformation.ProfessionalId;
            var listFamily = ProfessionalBLL.GetFamilyInformationByProfessionalId(professionalId);
            var scholarships = ScholarshipBLL.GetAllScholarship().OrderBy(c => c.Name);
            ViewBag.SchoolYearSon = new SelectList(scholarships, "Id", "Name");
            ViewBag.SchoolYearGrandChild = new SelectList(scholarships, "Id", "Name");
            var months = DataHelper.MotnGenerate();
            ViewBag.Months = new SelectList(months, "Value", "Text");

            var days = DataHelper.NumbersGenerate(31);
            ViewBag.Days = new SelectList(days, "Value", "Text");
            var type = DataHelper.TypeAgeGenerate();
            ViewBag.TypeAge = new SelectList(type, "Value", "Text");
            
            var listgonnaDaddy = ProfessionalBLL.GetGonnaDaddyByProfessionalId((int)professionalId);
            if (listgonnaDaddy != null)
            {

                if (listgonnaDaddy != null)
                {
                    if (listgonnaDaddy.Any())
                    {
                        foreach (var gonnaDaddy in listgonnaDaddy)
                        {
                            generalInformation.ListGonnaDaddy.Add(
                          new GonnaDaddy
                          {
                              GonnaDaddyQuestion = gonnaDaddy.GonnaDaddyQuestion,
                              DayStimated = gonnaDaddy.DayStimated,
                              Id = gonnaDaddy.Id,
                              MonthStimated = gonnaDaddy.MonthStimated
                          });
                        }
                    }
                }
              

            }
            if (listFamily != null)
            {

                if (listFamily.Any())
                {
                    var listSon = listFamily.Where(f => f.IsFather == true).ToList();
                    var listGrandChild = listFamily.Where(f => f.IsGrandFather == true).ToList();

                    if (listSon.Any())
                    {
                        generalInformation.IsFather = true;
                        foreach (var son in listSon)
                        {
                            generalInformation.ListSon.Add(new Parent
                            {
                                Id = son.Id,
                                Name = son.Name,
                                Age = son.Age,
                                IsYearAge=son.IsYearAge,
                                SchoolYear = son.SchoolYear,
                                SchoolName = son.SchoolYear != null ? scholarships.FirstOrDefault(s => s.Id == son.SchoolYear).Name : string.Empty
                            });
                        }
                    }

                    if (listGrandChild.Any())
                    {
                        generalInformation.IsGrandFather = true;
                        foreach (var grandChild in listGrandChild)
                        {
                            generalInformation.ListGrandchild.Add(new Parent
                            {
                                Id = grandChild.Id,
                                Name = grandChild.Name,
                                Age = grandChild.Age,
                                IsYearAge = grandChild.IsYearAge,
                                SchoolName = grandChild.SchoolYear != null ? scholarships.FirstOrDefault(s => s.Id == grandChild.SchoolYear).Name : string.Empty,
                            });
                        }
                    }
                }
            }

            return PartialView("~/Views/Professionals/_EditGeneralInformation.cshtml", generalInformation);
        }

        public ActionResult EditSecretaryInformation(SecretaryInformation secretaryInformation)
        {
            var professionalId = secretaryInformation.ProfessionalId;
            secretaryInformation = ProfessionalBLL.GetSecretaryByProfessionalId(professionalId);

            if (secretaryInformation != null)
            {
                if (secretaryInformation.SecretaryBirthDate != null)
                {
                    var date = Convert.ToDateTime(secretaryInformation.SecretaryBirthDate);
                    secretaryInformation.SecretaryBirthDay = Convert.ToInt32(date.ToString("dd"));
                    secretaryInformation.SecretaryBirthMonth = Convert.ToInt32(date.ToString("MM"));
                    secretaryInformation.SecretaryBirthYear = Convert.ToInt32(date.ToString("yyyy"));
                }
            }
            else
            {
                secretaryInformation = new SecretaryInformation()
                {
                    ProfessionalId = professionalId,
                    SecretaryHaveSecretary = false,
                    SecretaryHaveSon = false,
                };
            }

            return PartialView("~/Views/Professionals/_EditSecretaryInformation.cshtml", secretaryInformation);
        }

        public ActionResult EditDigitalSignature(HabeasData HabeasData)
        {
            if (!string.IsNullOrEmpty(HabeasData.UrlImage))
                HabeasData.UrlImage = string.Format("{0}{1}", System.Configuration.ConfigurationManager.AppSettings["urlSite"], HabeasData.UrlImage);
            return PartialView("~/Views/Professionals/_EditDigitalSignature.cshtml", HabeasData);
        }
        public ActionResult EditSecretaryDigitalSignature(HabeasData HabeasData)
        {
            if (!string.IsNullOrEmpty(HabeasData.UrlImage))
                HabeasData.UrlImage = string.Format("{0}{1}", System.Configuration.ConfigurationManager.AppSettings["urlSite"], HabeasData.UrlImage);
            return PartialView("~/Views/Professionals/_EditSecretaryDigitalSignature.cshtml", HabeasData);
        }

        #endregion

        #region Actualizacion Profesionales

        //informacion basica
        [HttpPost]
        public ActionResult UpdateProfessional(Professional model)
        {
            var response = new Response<bool>();

            var professionalType = ProfessionalBLL.GetAllProfessionalTypes();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();
            var genders = GenderBLL.GetAllGender();
            var users = new List<User>();

            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", model.DepartamentId);
            ViewBag.CityId = new SelectList(cities, "Id", "Name", model.CityId);
            ViewBag.ProfessionalTypeId = new SelectList(professionalType, "Id", "Name", model.ProfessionalTypeId);
            ViewBag.GenderId = new SelectList(genders, "Id", "Name", model.GenderId);
            ViewBag.MsrId = new SelectList(users, "Id", "Name", model.MsrId);

            model.LastUpdate = DateTime.Now;

            if (model.MsrId != null && model.MsrId != 0)
                model.MsrMail = UserBLL.GetUserById((int)model.MsrId).Email;
            else
                ModelState.AddModelError(string.Empty, "El campo representante es requerido.");

            if (model.GetBirthDate == null)
                ModelState.AddModelError(string.Empty, "La fecha ingresada no es válida.");
            else
                model.BirthDate = (DateTime)model.GetBirthDate;

            if (model.DocumentNumber != null)
            {
                var objProfessional = ProfessionalBLL.GetProfessionalByDocumentNumber(Convert.ToString(model.DocumentNumber));

                if (objProfessional != null && (model.Id != objProfessional.Id))
                    ModelState.AddModelError(string.Empty, "El numero de cédula ya se encuentra registrado.");

                objProfessional = ProfessionalBLL.GetProfessionalById((int)model.Id);
                model.Token = objProfessional.Token;
                model.ExpirationDateToken = objProfessional.ExpirationDateToken;
                model.RegisterDate = objProfessional.RegisterDate;
            }

            if (ModelState.IsValid)
            {
                if (ProfessionalBLL.UpdateProfessional(model))
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = true,
                        Message = "El registro ha sido actualizado con éxito.",
                        Title = "Administracion Profesionales"
                    };
                }
                else
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = true,
                        Message = "Ha ocurrido un error al actualizar el registro. Intente nuevamente.",
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        // Informacion de contacto
        public ActionResult UpdateProfessionalContact(ProfessionalContact model)
        {
            var response = new Response<bool>();

            var professionalContact = new ProfessionalContact();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();
            ViewBag.ProfessionalDepartamentId = new SelectList(departaments, "Id", "Name", model.ProfessionalDepartamentId);
            ViewBag.ProfessionalCityId = new SelectList(cities, "Id", "Name", model.ProfessionalCityId);
            var bricks = new List<Brick>();
            if (model.ProfessionalCityId != null && model.ProfessionalCityId != 0)
                bricks = BrickBLL.GetAllBricks().Where(b => b.CityId == model.ProfessionalCityId).ToList();

            ViewBag.ProfessionalBrickId = new SelectList(bricks, "Code", "Code", model.ProfessionalBrickId);

            if (!string.IsNullOrEmpty(model.ProfessionalEmail))
            {
                var validationEmail = ProfessionalBLL.GetProfessionalContactByEmail(model.ProfessionalEmail);
                if (validationEmail != null && (validationEmail.ProfessionalId != model.ProfessionalId))
                    ModelState.AddModelError(string.Empty, "El correo ingrasado ya se encuantra registrado.");
            }
            professionalContact = ProfessionalBLL.GetProfessionalContactById((int)model.ProfessionalId);
            model.Guid = professionalContact.Guid;
            model.Id = professionalContact.Id;


            if (ModelState.IsValid)
            {
                if (ProfessionalBLL.UpdateProfessionalContact(model))
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = true,
                        Message = "El registro ha sido actualizado con éxito.",
                        Title = "Administracion Profesionales"
                    };

                    UpdateLastUpdateByProfessional(model.ProfessionalId);
                }
                else
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error al actualizar la información de contacto. Intente nuevamente.",
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        // Informacion Laboral
        public ActionResult UpdateLaborInformation(LaborInformation model)
        {
            var response = new Response<bool>();

            var professional = ProfessionalBLL.GetProfessionalById((int)model.ProfessionalId);
            DateTime? startProfessionDate = null;

            if (professional.ProfessionalTypeId != (int)EnumProfession.Secretaria)
            {
                try
                {
                    startProfessionDate = ValidateDate(model.StartProfessionDay,
                               model.StartProfessionMonth, model.StartProfessionYear);

                    if (startProfessionDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó a ejercer su profesión.");
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó a ejercer su profesión.");
                }
            }
            else
            {
                if (string.IsNullOrEmpty(model.BossName))
                    ModelState.AddModelError(string.Empty, "Debe ingresar el nombre del profesional para el trabaja.");
            }

            model.StartDateProfession = startProfessionDate;

            if (model.IsSpecialist && (model.SpecialityId == 0 || model.SpecialityId == null))
                ModelState.AddModelError(string.Empty, "Debe seleccionar una especialidad.");


            if (model.SpecialityId == 100 && string.IsNullOrEmpty(model.OtherSpeciality))
                ModelState.AddModelError(string.Empty, "Debe seleccionar una especialidad de la lista o ingresar la especialidad en el campo otra.");

            if (model.AssociatedProfessionals == "" || model.AssociatedProfessionals == null)
                ModelState.AddModelError(string.Empty, "Debe Asociar un professional.");

            if (!model.IsSpecialist && (model.SpecialityId == 0 || model.SpecialityId == null))
                model.SpecialityId = null;

            if (model.IsStudent && (model.ProfessionalLevelId == 0 || model.ProfessionalLevelId == null))
                ModelState.AddModelError(string.Empty, "Debe seleccionar el nivel profesional.");

            DateTime? startStudyDate = null;
            DateTime? endStudyDate = null;
            if (model.IsStudent)
            {
                try
                {
                    startStudyDate = ValidateDate(model.StartStudyDay,
                               model.StartStudyMonth, model.StartStudyYear);
                    if (startStudyDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó los estudios.");
                    else
                    {
                        if (DateTime.Compare((DateTime)startStudyDate, DateTime.Now) > 0)
                            ModelState.AddModelError(string.Empty, "La fecha en la que comenzó los estudios debe ser menor a la fecha actual.");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó los estudios.");
                }

                try
                {
                    endStudyDate = ValidateDate(model.EndStudyDay,
                                model.EndStudyMonth, model.EndStudyYear);

                    if (endStudyDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual finalizará los estudios.");
                    else
                    {
                        if (DateTime.Compare(DateTime.Now, (DateTime)endStudyDate) > 0)
                            ModelState.AddModelError(string.Empty, "La fecha estimada de finalización de estudios debe ser mayor a la fecha actual.");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual finalizará los estudios.");
                }
            }

            model.StartDateStudy = startStudyDate;
            model.EndDateStudy = endStudyDate;

            if (ModelState.IsValid)
            {
                bool result = false;
                if (model.SpecialityId == 100 && !string.IsNullOrEmpty(model.OtherSpeciality))
                {
                    var saveSpeciality = SpecialityBLL.SaveSpeciality(new Business.Models.Speciality
                    {
                        Guid = Guid.NewGuid(),
                        Id = 0,
                        IsActive = true,
                        Name = model.OtherSpeciality.First().ToString().ToUpper() + model.OtherSpeciality.Substring(1).ToLower(),
                        ProfessionalTypeId = professional.ProfessionalTypeId,
                    });

                    if (saveSpeciality != null)
                    {
                        model.SpecialityId = saveSpeciality;
                        result = ProfessionalBLL.UpdateLabolarInformation(model);
                    }
                    else
                        response = new Response<bool>()
                        {
                            Result = false,
                            Model = false,
                            Message = "Ha ocurrido un error al guardar la nueva especialidad. Intente nuevamnete.",
                            Title = "Administracion Profesionales"
                        };
                }
                else
                    result = ProfessionalBLL.UpdateLabolarInformation(model);

                if (result)
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = false,
                        Message = "El registro ha sido actualizado con éxito.",
                        Title = "Administracion Profesionales"
                    };

                    UpdateLastUpdateByProfessional(model.ProfessionalId);
                }
                else
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error al actualizar la información laboral. Intente nuevamente.",
                        Title = "Administracion Profesionales"
                    };
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        // Informacion General
        public ActionResult UpdateGeneralInformation(GeneralInformation model)
        {
            int? professionalId = model.ProfessionalId;
            var response = new Response<bool>();

            if (model.IsFather && model.ListSon == null)
                ModelState.AddModelError(string.Empty, "Si ha indicado tener hijos debe ingresar mínimo uno en la lista.");

            if (model.IsGrandFather && model.ListGrandchild == null)
                ModelState.AddModelError(string.Empty, "Si ha indicado tener nietos debe ingresar mínimo uno en la lista.");


            if (ModelState.IsValid)
            {
                try
                {
                    using (TransactionScope scope = new TransactionScope())
                    {
                        var listFamily = ProfessionalBLL.GetFamilyInformationByProfessionalId(model.ProfessionalId);

                        if (listFamily != null && listFamily.Any())
                        {
                            foreach (var item in listFamily)
                            {
                                ProfessionalBLL.DeleteGeneralInformationById(item.Id);
                            }
                        }
                        var listGonna = ProfessionalBLL.GetGonnaDaddyByProfessionalId(model.ProfessionalId);
                        if (listGonna != null && listGonna.Any())
                        {
                            foreach (var item in listGonna)

                            {
                                ProfessionalBLL.DeleteGonnaDaddyByProfessionalId(item.Professional_id);
                            }
                        }
                        

                        var objSon = new FamilyInformation
                        {
                            Id = 0,
                            ProfessionalId = (int)professionalId,
                        };

                        if (model.IsFather)
                        {
                            if (model.ListSon.Any())
                            {
                                foreach (var son in model.ListSon)
                                {
                                    objSon.Guid = Guid.NewGuid();
                                    objSon.Name = son.Name;
                                    objSon.Age = son.Age;
                                    objSon.SchoolYear = son.SchoolYear;
                                    objSon.IsFather = true;
                                    objSon.IsGrandFather = false;
                                    objSon.IsYearAge = son.IsYearAge;
                                    ProfessionalBLL.SaveFamilyInformation(objSon);
                                };
                            }
                        }


                        if (model.IsGrandFather)
                        {
                            if (model.ListGrandchild.Any())
                            {
                                foreach (var son in model.ListGrandchild)
                                {
                                    objSon.Guid = Guid.NewGuid();
                                    objSon.Name = son.Name;
                                    objSon.Age = son.Age;
                                    objSon.SchoolYear = son.SchoolYear;
                                    objSon.IsFather = false;
                                    objSon.IsGrandFather = true;
                                    objSon.IsYearAge = son.IsYearAge;
                                    ProfessionalBLL.SaveFamilyInformation(objSon);
                                };
                            }
                        }

                        var objGonnaDaddy = new GonnaDaddy();
                        
                        //model.ListGonnaDaddy.FirstOrDefault().GonnaDaddyQuestion = false;
                        if (model.ListGonnaDaddy !=null )
                        {
                            if (model.ListGonnaDaddy.Any())

                            {
                                foreach (var son in model.ListGonnaDaddy)
                                {

                                    objGonnaDaddy.MonthStimated = son.MonthStimated;
                                    objGonnaDaddy.DayStimated = son.DayStimated;
                                    objGonnaDaddy.GonnaDaddyQuestion = true;
                                    objGonnaDaddy.Professional_id = model.ProfessionalId;

                                    ProfessionalBLL.SaveGonnaDaddy(objGonnaDaddy);
                                };
                            } 
                        }
                      

                        if (model.ListGrandchild == null && model.ListSon == null)
                            ProfessionalBLL.SaveFamilyInformation(objSon);

                        scope.Complete();
                        response = new Response<bool>()
                        {
                            Result = true,
                            Model = true,
                            Message = "El registro ha sido actualizado con éxito.",
                            Title = "Administracion Profesionales"
                        };

                        UpdateLastUpdateByProfessional(model.ProfessionalId);
                    }
                }
                catch (Exception ex)
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error. Intente nuevamente.",
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        // Informacion Secretaria
        public ActionResult UpdateSecretaryInformation(SecretaryInformation model)
        {
            var response = new Response<bool>();


            if (model.SecretaryHaveSecretary)
            {
                if (string.IsNullOrEmpty(model.SecretaryFirstName))
                    ModelState.AddModelError(string.Empty, "El campo Nombre es requerido.");

                if (string.IsNullOrEmpty(model.SecretaryLastName))
                    ModelState.AddModelError(string.Empty, "El campo Apellido es requerido.");

                if (string.IsNullOrEmpty(model.SecretaryEmail))
                    ModelState.AddModelError(string.Empty, "El campo Correo es requerido.");

                if (string.IsNullOrEmpty(model.SecretaryCellPhone))
                    ModelState.AddModelError(string.Empty, "El campo Celular es requerido.");

                try
                {
                    var birthDate = ValidateDate(model.SecretaryBirthDay,
                           model.SecretaryBirthMonth, model.SecretaryBirthYear);

                    if (birthDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha de nacimiento valida.");
                    else
                        model.SecretaryBirthDate = birthDate;
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha de nacimiento valida.");
                }
            }
            else
            {
                ProfessionalBLL.DeleteSecretaryInformationById(model.ProfessionalId);
                response = new Response<bool>()
                {
                    Result = true,
                    Model = true,
                    Message = "El registro ha sido actualizado con éxito.",
                    Title = "Administracion Profesionales"
                };
                return Json(response);
            }

            if (ModelState.IsValid)
            {

                if (model.SecretaryHaveSecretary)
                {
                    bool band;
                    band=ProfessionalBLL.UpdateSecretaryInformation(model);
                    var cant = ProfessionalDAL.GetSecretaryDocument(model.SecretaryDocumentNumber);
                    //model.SecretaryHaveSon="1" ? model.SecretaryHaveSon=true: model.SecretaryHaveSon = false;
                    
                    var secretaries = ProfessionalBLL.GetAllSecretaryByDocument(model.SecretaryDocumentNumber);
                    
                    foreach (var item in secretaries)
                    {
                        item.SecretaryAddress = model.SecretaryAddress;
                        item.SecretaryBirthDate = model.SecretaryBirthDate;
                        item.SecretaryCellPhone = model.SecretaryCellPhone;
                        item.SecretaryCompany = model.SecretaryCompany;
                        item.SecretaryEmail = model.SecretaryEmail;
                        item.SecretaryFirstName = model.SecretaryFirstName;
                        item.SecretaryLastName = model.SecretaryLastName;
                        item.SecretaryHaveSon = model.SecretaryHaveSon;

                        band=ProfessionalBLL.UpdateSecretaryInformation(item);
                    }

                    if (band)
                    {
                        response = new Response<bool>()
                        {
                            Result = true,
                            Model = true,
                            Message = "El registro ha sido actualizado con éxito.",
                            Title = "Administracion Profesionales"
                        };

                        UpdateLastUpdateByProfessional(model.ProfessionalId);
                    }
                    else
                    {
                        response = new Response<bool>()
                        {
                            Result = false,
                            Model = false,
                            Message = "Ha ocurrido un error al guardar la información de la secretaria. Intente nuevamnete.",
                            Title = "Administracion Profesionales"
                        };
                    }
                }
                else
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = true,
                        Message = "El registro ha sido actualizado con éxito.",
                        Title = "Administracion Profesionales"
                    };
                }
            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = getModelError(ModelState.Values),
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        #endregion

        #region Guardar firma

        public ActionResult SaveImage(string imageB64)
        {
            var response = new Response<bool>();
            int? professionalId = null;

            if (Session["professionalId"] != null)
            {
                professionalId = Convert.ToInt32(Session["professionalId"]);

            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "Debe registrar un profesional.",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }


            if (string.IsNullOrEmpty(imageB64))
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "No es posible procesar la firma. Intente nuevamente",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }

            try
            {
                var professional = ProfessionalBLL.GetProfessionalById((int)professionalId);
                var habeasData = ProfessionalBLL.GetHabeasDataByProfessionalId((int)professionalId);
                var image = new Helper.ImageHelper();
                var objImage = image.Base64ToImage(imageB64);
                var newGuid= Guid.NewGuid();
                var urlImage = "Content/CanvasImages/" + newGuid + ".jpg";

                

                objImage.Save(Server.MapPath("~/Content/CanvasImages/" + newGuid + ".jpg"));

                

                if (habeasData == null)
                {
                  
                    habeasData = new HabeasData
                    {

                        Guid = Guid.NewGuid(),
                        CheckTerms = true,
                        StartTermsDate = DateTime.Now,
                        ProfessionalId = (int)professional.Id,
                        UrlImage = urlImage,
                        LastUpdateTermsDate = DateTime.Now
                    };
                }
                else
                {
                    if (System.IO.File.Exists(Server.MapPath(habeasData.UrlImage)))
                        System.IO.File.Delete(Server.MapPath(habeasData.UrlImage));
                    habeasData.LastUpdateTermsDate = DateTime.Now;
                    habeasData.CheckTerms = true;
                    habeasData.UrlImage = urlImage;
                    
                }

                if (ProfessionalBLL.SaveProfessionalHabeasData(habeasData))
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = false,
                        Message = "La firma ha sido guardada con éxito.",
                        Title = "Administracion Profesionales",
                        StatusCode = System.Configuration.ConfigurationManager.AppSettings["urlSite"]+urlImage
                    };

                    UpdateLastUpdateByProfessional(habeasData.ProfessionalId);
                }
                else
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error al guardar la firma. Intente Nuevamente",
                        Title = "Administracion Profesionales",
                      
                    };
                }

            }
            catch (Exception ex)
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "No es posible procesar la firma. Intente nuevamente",
                    Title = "Administracion Profesionales"
                };
            }
            
            return Json(response);
        }
        public ActionResult SaveImageSecretary(string imageB64)
        {
            var response = new Response<bool>();
            int? professionalId = null;

            if (Session["professionalId"] != null)
            {
                professionalId = Convert.ToInt32(Session["professionalId"]);

            }
            else
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "Debe registrar un profesional.",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }


            if (string.IsNullOrEmpty(imageB64))
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "No es posible procesar la firma. Intente nuevamente",
                    Title = "Administracion Profesionales"
                };

                return Json(response);
            }

            try
            {
                var secretary = ProfessionalBLL.GetSecretaryByProfessionalId((int)professionalId);
                if (secretary == null)
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Debe registrar una Secretaria",
                        Title = "Administracion Profesionales"
                    };

                    return Json(response);
                }

                var habeasData = ProfessionalBLL.GetHabeasDataByProfessionalId(secretary.SecretaryId);
                var image = new Helper.ImageHelper();
                var objImage = image.Base64ToImage(imageB64);
                var newGuid = Guid.NewGuid();
                var urlImage = "Content/CanvasImages/Secretary/" + newGuid + ".jpg";


                objImage.Save(Server.MapPath("~/Content/CanvasImages/Secretary/" + newGuid + ".jpg"));



                if (habeasData == null)
                {

                    habeasData = new HabeasData
                    {

                        Guid = Guid.NewGuid(),
                        CheckTerms = true,
                        StartTermsDate = DateTime.Now,
                        ProfessionalId = (int)secretary.SecretaryId,
                        UrlImage = urlImage,
                        LastUpdateTermsDate = DateTime.Now
                    };
                }
                else
                {
                    if (System.IO.File.Exists(Server.MapPath(habeasData.UrlImage)))
                        System.IO.File.Delete(Server.MapPath(habeasData.UrlImage));
                    habeasData.LastUpdateTermsDate = DateTime.Now;
                    habeasData.CheckTerms = true;
                    habeasData.UrlImage = urlImage;

                }

                if (ProfessionalBLL.SaveSecretaryHabeasData(habeasData))
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = false,
                        Message = "La firma ha sido guardada con éxito.",
                        Title = "Administracion Profesionales",
                        StatusCode = System.Configuration.ConfigurationManager.AppSettings["urlSite"] + urlImage
                    };

                    //UpdateLastUpdateByProfessional(habeasData.ProfessionalId);
                }
                else
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = "Ha ocurrido un error al guardar la firma. Intente Nuevamente",
                        Title = "Administracion Profesionales",

                    };
                }

            }
            catch (Exception ex)
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "No es posible procesar la firma. Intente nuevamente",
                    Title = "Administracion Profesionales"
                };
            }

            return Json(response);
        }

        #endregion


        private DateTime? ValidateDate(int? day, int? month, int? year)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(day))) return null;
                if (string.IsNullOrEmpty(Convert.ToString(month))) return null;

                var date = string.Format("{0}/{1}/{2}",
                    year != null ? year : 1900,
                    month,
                    day);
                if (date.Length > 10)
                    return null;

                if (date.Contains('e'))
                    return null;

                return DateTime.Parse(date);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        private DateTime? ValidateDateAllRequerid(int? day, int? month, int? year)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(day))) return null;
                if (string.IsNullOrEmpty(Convert.ToString(month))) return null;
                if (string.IsNullOrEmpty(Convert.ToString(year))) return null;

                var date = string.Format("{0}/{1}/{2}",
                    year,
                    month,
                    day);
                if (date.Length > 10)
                    return null;

                if (date.Contains('e'))
                    return null;

                return DateTime.Parse(date);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #region Json
        public JsonResult GetUserByCityId(int cityId)
        {
            var users = UserBLL.GetAUserByCityId(cityId).Where(u => u.ProfileId == (int)EnumProfile.representantes).ToList();
            return Json(users);
        }

        public JsonResult GetBricksByCityId(int cityId)
        {
            var bricks = BrickBLL.GetAllBricks().Where(b => b.CityId == cityId).ToList().OrderBy(x=>x.Code);
            return Json(bricks);
        }

        public JsonResult GetCitiesByDepartamentIdAsync(int departmentId)
        {
            var cities = CityBLL.GetCitiesByDepartamentId(departmentId).OrderBy(c=>c.Name);
            return Json(cities);
        }
        public JsonResult GetAllProfessionalsByCityId(int cityId)
        {
            
            var cities = ProfessionalBLL.GetAllProfessionalByCityId(cityId);
            return Json(cities);
        }
        public JsonResult GetProfessionalById(int id)
        {

            var profess = ProfessionalBLL.GetProfessionalById(id);
            return Json(profess);
        }
        public JsonResult GetSpecialities()
        {
            var specialities = new List<Business.Models.Speciality>();
            int? professionalId = Convert.ToInt32(Session["professionalId"]);
            if (professionalId != null && professionalId != 0)
            {
                var professional = ProfessionalBLL.GetProfessionalById((int)professionalId);
                specialities = SpecialityBLL.GetSpecialitiesByProfesionalTypeId((int)professional.ProfessionalTypeId);
                specialities.Add(new Business.Models.Speciality { Id = 100, Name = "Otra" });
            }
            return Json(specialities);
        }

        private string getModelError(ICollection<ModelState> values)
        {
            return "• " + string.Join("</br> • ", values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
        }

        private void UpdateLastUpdateByProfessional(int professionalId)
        {
            var professional = ProfessionalBLL.GetProfessionalById((int)professionalId);
            if (professional != null)
            {
                professional.LastUpdate = DateTime.Now;
                ProfessionalBLL.UpdateProfessional(professional);
            }
        }

        public JsonResult GetAllRepresentantsByCity(int cityId, string repr)
        {
            var idRepr = Convert.ToDecimal(repr);
            var cities = ProfessionalBLL.GetAllRepresentantsByCityId(cityId);
            var results = cities.Where(i => i.ID!= idRepr).Select(x=> new
            {
                GUID = x.GUID,
                NAME = x.NAME
            }).ToList();
            return Json(results);
        }

        


        #endregion
    }
   
}
