﻿using Big.MeadJohnson.Business.Business;
using Big.MeadJohnson.Business.Enums;
using Big.MeadJohnson.Business.Models;
using Big.MeadJohnson.Business.ViewModel;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Big.MeadJohnson.CRM.UI.Controllers.BusinessController
{
    [Authorize(Roles = "Súper_Administrador")]
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {

            GeneralReportViewModel model = new GeneralReportViewModel();

            var professionalType = ProfessionalBLL.GetAllProfessionalTypes();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<City>();
            var users = new List<User>();

            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", model.DepartamentId);
            ViewBag.CityId = new SelectList(cities, "Id", "Name", model.CityId);
            ViewBag.ProfessionalTypeId = new SelectList(professionalType, "Id", "Name", model.ProfessionalTypeId);
            ViewBag.MsrId = new SelectList(users, "Id", "Name");

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Index(GeneralReportViewModel model)
        {
            if (model.DepartamentId == 0)
                model.DepartamentId = null;

            if (model.CityId == 0)
                model.CityId = null;

            if (model.MsrId == 0)
                model.MsrId = null;

            if (model.ProfessionalTypeId == 0)
                model.ProfessionalTypeId = null;

            var report = Business.Reports.GeneralReport.GetGeneralReport(model.DepartamentId, model.CityId, model.MsrId, model.ProfessionalTypeId);

            if (report == null)
                ModelState.AddModelError(string.Empty, "No se encontraron registros que coincidan con los criterios de búsqueda.");
            else
            {
                ModelState.AddModelError(string.Empty, "El reporte se esta generando.");
                ExportToExcel(report);
            }


            var professionalType = ProfessionalBLL.GetAllProfessionalTypes();
            var departaments = DepartamentBLL.GetAllDepartaments();

            var cities = new List<City>();
            var users = new List<User>();

            if (model.CityId != null && model.CityId != 0)
            {
                cities = CityBLL.GetCitiesByDepartamentId((int)model.DepartamentId);
                users = UserBLL.GetAUserByCityId((int)model.CityId).Where(u => u.ProfileId == (int)EnumProfile.representantes).ToList();
            }

            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", model.DepartamentId);
            ViewBag.CityId = new SelectList(cities, "Id", "Name", model.CityId);
            ViewBag.ProfessionalTypeId = new SelectList(professionalType, "Id", "Name", model.ProfessionalTypeId);
            ViewBag.MsrId = new SelectList(users, "Id", "Name", model.MsrId);

            return View(model);
        }


        public void ExportToExcel<T>(List<T> list)
        {

            var grid = new GridView();
            grid.DataSource = list;
            grid.DataBind();

            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=MyExcelFile.xls");
            Response.ContentType = "application/ms-excel";

            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }
    }
}