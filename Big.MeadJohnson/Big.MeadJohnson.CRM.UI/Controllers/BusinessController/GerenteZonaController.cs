﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Big.MeadJohnson.CRM.UI.Models;
using Big.MeadJohnson.Core.DAL;

namespace Big.MeadJohnson.CRM.UI.Controllers.BusinessController
{
    public class GerenteZonaController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: GerenteZona
        public ActionResult Index()
        {


            return View();
        }

        // GET: GerenteZona/Details/5
        public ActionResult Details(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MJ_PROFESSIONAL mJ_PROFESSIONAL = db.MJ_PROFESSIONAL.Find(id);
            if (mJ_PROFESSIONAL == null)
            {
                return HttpNotFound();
            }
            return View(mJ_PROFESSIONAL);
        }

        // GET: GerenteZona/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GerenteZona/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,GUID,MSR_ID,MSR_MAIL,PROFESSIONAL_TYPE_ID,FIRST_NAME,SECOND_NAME,LAS_NAME,SECOND_LAST_NAME,DOCUMENT_NUMBER,GENDER_ID,BIRTH_DATE,REGISTER_DATE,CREATED_BY,LAST_UPDATE,TOKEN,EXPIRATION_DATE_TOKEN")] MJ_PROFESSIONAL mJ_PROFESSIONAL)
        {
            if (ModelState.IsValid)
            {
                db.MJ_PROFESSIONAL.Add(mJ_PROFESSIONAL);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mJ_PROFESSIONAL);
        }

        // GET: GerenteZona/Edit/5
        public ActionResult Edit(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MJ_PROFESSIONAL mJ_PROFESSIONAL = db.MJ_PROFESSIONAL.Find(id);
            if (mJ_PROFESSIONAL == null)
            {
                return HttpNotFound();
            }
            return View(mJ_PROFESSIONAL);
        }

        // POST: GerenteZona/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,GUID,MSR_ID,MSR_MAIL,PROFESSIONAL_TYPE_ID,FIRST_NAME,SECOND_NAME,LAS_NAME,SECOND_LAST_NAME,DOCUMENT_NUMBER,GENDER_ID,BIRTH_DATE,REGISTER_DATE,CREATED_BY,LAST_UPDATE,TOKEN,EXPIRATION_DATE_TOKEN")] MJ_PROFESSIONAL mJ_PROFESSIONAL)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mJ_PROFESSIONAL).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mJ_PROFESSIONAL);
        }

        // GET: GerenteZona/Delete/5
        public ActionResult Delete(decimal id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MJ_PROFESSIONAL mJ_PROFESSIONAL = db.MJ_PROFESSIONAL.Find(id);
            if (mJ_PROFESSIONAL == null)
            {
                return HttpNotFound();
            }
            return View(mJ_PROFESSIONAL);
        }

        // POST: GerenteZona/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(decimal id)
        {
            MJ_PROFESSIONAL mJ_PROFESSIONAL = db.MJ_PROFESSIONAL.Find(id);
            db.MJ_PROFESSIONAL.Remove(mJ_PROFESSIONAL);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
