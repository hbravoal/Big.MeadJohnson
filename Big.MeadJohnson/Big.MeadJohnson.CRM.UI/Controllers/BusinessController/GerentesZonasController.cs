﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Big.MeadJohnson.Core.DAL;

namespace Big.MeadJohnson.CRM.UI.Controllers.BusinessController
{
    public class GerentesZonasController : Controller
    {
        private dbBigMeadJohnson db = new dbBigMeadJohnson();

        // GET: GerentesZonas
        public ActionResult Index()
        {
            return View(db.MJ_GERENTES.ToList());
        }

        // GET: GerentesZonas/Details/5
        public ActionResult Details(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MJ_GERENTES mJ_GERENTES = db.MJ_GERENTES.Find(id);
            if (mJ_GERENTES == null)
            {
                return HttpNotFound();
            }
            return View(mJ_GERENTES);
        }

        // GET: GerentesZonas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GerentesZonas/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,NOMBRE_COMPLETO,CEDULA,EMAIL")] MJ_GERENTES mJ_GERENTES)
        {
            if (ModelState.IsValid)
            {
                mJ_GERENTES.ID = Guid.NewGuid();
                db.MJ_GERENTES.Add(mJ_GERENTES);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(mJ_GERENTES);
        }

        // GET: GerentesZonas/Edit/5
        public ActionResult Edit(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MJ_GERENTES mJ_GERENTES = db.MJ_GERENTES.Find(id);
            if (mJ_GERENTES == null)
            {
                return HttpNotFound();
            }
            return View(mJ_GERENTES);
        }

        // POST: GerentesZonas/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,NOMBRE_COMPLETO,CEDULA,EMAIL")] MJ_GERENTES mJ_GERENTES)
        {
            if (ModelState.IsValid)
            {
                db.Entry(mJ_GERENTES).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(mJ_GERENTES);
        }

        // GET: GerentesZonas/Delete/5
        public ActionResult Delete(Guid? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MJ_GERENTES mJ_GERENTES = db.MJ_GERENTES.Find(id);
            if (mJ_GERENTES == null)
            {
                return HttpNotFound();
            }
            return View(mJ_GERENTES);
        }

        // POST: GerentesZonas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Guid id)
        {
            MJ_GERENTES mJ_GERENTES = db.MJ_GERENTES.Find(id);
            db.MJ_GERENTES.Remove(mJ_GERENTES);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
