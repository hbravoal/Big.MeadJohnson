﻿

namespace Big.MeadJohnson.CRM.UI.Controllers.BusinessController
{
    using System.Linq;
    using System.Threading.Tasks;
    using System.Net;
    using System.Web.Mvc;
    using Big.MeadJohnson.Core.DAL;
    using Big.MeadJohnson.Business.Business;
    using System.Collections.Generic;
    using Big.MeadJohnson.Business.Enums;
    using System;
    using Big.MeadJohnson.Business.ViewModel;
    using Microsoft.AspNet.Identity;
    using Big.MeadJohnson.Business.Models;

    
    public class UsersController : Controller
    {
        private dbBigMeadJohnson db = new dbBigMeadJohnson();

        [Authorize(Roles = "Súper_Administrador")]
        public async Task<ActionResult> Index()
        {
            var profiles = ProfileBLL.GetAllProfiles();
            var users = await UserBLL.GetAllUser();

            foreach (var user in users)
            {
                user.Profilename = profiles.Where(p => p.Id == user.ProfileId).FirstOrDefault().Description;
            }

            return View(users);
        }

        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult Details(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = UserBLL.GetUserById((int)id);
            return View(user);
        }

        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult Create()
        {
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = new List<Business.Models.City>();
            var profiles = ProfileBLL.GetAllProfiles();
            var managers = ManagerBLL.GetAllManagers();

            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name");
            ViewBag.CityId = new SelectList(cities, "Id", "Name");
            ViewBag.ProfileId = new SelectList(profiles, "Id", "Description");
            ViewBag.ManagerId = new SelectList(managers, "Id", "Names");
            User user = new User { Guid = Guid.NewGuid() };

            return View(user);
        }

        [Authorize(Roles = "Súper_Administrador")]
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Create(Business.Models.User user)
        {
            var response = new Response<bool>();
            var profiles = ProfileBLL.GetAllProfiles();

            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = CityBLL.GetCitiesByDepartamentId(departaments.FirstOrDefault().Id);
            var managers = ManagerBLL.GetAllManagers();

            ViewBag.CityId = new SelectList(cities, "Id", "Name", user.CityId);
            ViewBag.ProfileId = new SelectList(profiles, "Id", "Description", user.ProfileId);
            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", user.DepartamentId);
            ViewBag.ManagerId = new SelectList(managers, "Id", "Names", user.ManagerId);

            if (user.ProfileId == (int)EnumProfile.representantes && (user.CityId == null || user.CityId == 0))
            {
                ModelState.AddModelError(string.Empty, "Debe seleccionar un departamento y una ciudad");
            }

            var objUser = UserBLL.GetUserByUserName(user.UserName);
            if (objUser != null)
            {
                ModelState.AddModelError("UserName", "El nombre de usuario ya se encuentra registrado.");
            }
            else
            {

                objUser = UserBLL.GetUserByEmail(user.Email);
                if (objUser != null)
                    ModelState.AddModelError("Email", "El correo ya se encuentra registrado.");

                //if (user.ManagerId == 0 || user.ManagerId == null)
                //    ModelState.AddModelError("ManagerId", "Debe seleccionar un gerente");

                if (user.ProfileId == 0 || user.ProfileId == null)
                    ModelState.AddModelError("ProfileId", "Debe seleccionar un perfil");
            }

            user.Guid = Guid.NewGuid();
            user.RegisterDate = DateTime.Now;
            user.IsFirstUse = true;

            if (ModelState.IsValid)
            {
                var result = UserBLL.SaveNewUser(user);
                if (result)
                {

                    Helper.UserHelper.CreateUserASP(
                        user.Email,
                        user.UserName,
                        profiles.Where(p => p.Id == user.ProfileId).FirstOrDefault().Description,
                        user.Password);

                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = result,
                        Message = "El usuario ha sido creado con éxito.",
                        Title = "Usuarios"
                    };

                    return Json(response);
                }
            }

            response = new Response<bool>()
            {
                Result = false,
                Model = false,
                Message = getModelError(ModelState.Values),
                Title = "Usuarios"
            };



            return Json(response);
            //return View(user);
        }

        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);

            var user = UserBLL.GetUserById((int)id);
            if (user == null)
            {
                return HttpNotFound();
            }

            var departaments = DepartamentBLL.GetAllDepartaments();
            var city = new Business.Models.City();
            var departament = new Business.Models.Departament();
            var managers = ManagerBLL.GetAllManagers();

            if (user.CityId != null)
            {
                city = CityBLL.GetCityById((int)user.CityId);
                departament = departaments.Where(d => d.Id == city.DepartamentId).FirstOrDefault();
            }

            var cities = CityBLL.GetCitiesByDepartamentId(departament != null ? departament.Id : departaments.FirstOrDefault().Id);

            var profiles = ProfileBLL.GetAllProfiles();
            ViewBag.ProfileId = new SelectList(profiles, "Id", "Description", user.ProfileId);
            ViewBag.CityId = new SelectList(cities, "Id", "Name", user.CityId != null ? user.CityId : null);
            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", city?.DepartamentId);
            ViewBag.ManagerId = new SelectList(managers, "Id", "Names", user.ManagerId);
            return View(user);
        }

        [Authorize(Roles = "Súper_Administrador")]
        [HttpPost]
        // [ValidateAntiForgeryToken]
        public ActionResult Edit(User user)
        {
            var response = new Response<bool>();
            var profiles = ProfileBLL.GetAllProfiles();
            var departaments = DepartamentBLL.GetAllDepartaments();
            var cities = CityBLL.GetCitiesByDepartamentId(departaments.FirstOrDefault().Id);
            var managers = ManagerBLL.GetAllManagers();
            ViewBag.CityId = new SelectList(cities, "Id", "Name", user.CityId);
            ViewBag.ProfileId = new SelectList(profiles, "Id", "Description", user.ProfileId);
            ViewBag.DepartamentId = new SelectList(departaments, "Id", "Name", user.DepartamentId);
            ViewBag.ManagerId = new SelectList(managers, "Id", "Names", user.ManagerId);

            if (user.ProfileId == (int)EnumProfile.representantes && (user.CityId == null || user.CityId == 0))
            {
                ModelState.AddModelError(string.Empty, "Debe seleccionar un departamento y una ciudad");
                return View(user);
            }

            var oldUser = UserBLL.GetUserById(user.Id);
            if (oldUser != null)
            {
                if (oldUser.ProfileId != user.ProfileId)
                    Helper.UserHelper.UpdateUserRole(
                        user.UserName,
                        profiles.Where(p => p.Id == user.ProfileId).FirstOrDefault().Description,
                        profiles.Where(p => p.Id == oldUser.ProfileId).FirstOrDefault().Description);

                if (oldUser.Password != user.Password)
                    Helper.UserHelper.ChangePassword(user.UserName, oldUser.Password, user.Password);


                if (user.Email != oldUser.Email)
                    Helper.UserHelper.UpdateUserMail(user.Email, user.UserName);

                user.RegisterDate = oldUser.RegisterDate;
                user.IsFirstUse = oldUser.IsFirstUse;
            }

            if (user.IsActive == true && oldUser.IsActive == false)
            {
                Helper.UserHelper.RemoveUserRole(
                   user.UserName,
                   profiles.Where(p => p.Id == oldUser.ProfileId).FirstOrDefault().Description);

                Helper.UserHelper.SaveUserRole(
                    user.UserName,
                    profiles.Where(p => p.Id == user.ProfileId).FirstOrDefault().Description);
            }

            if (!user.IsActive)
            {
                Helper.UserHelper.RemoveUserRole(
                  user.UserName,
                  profiles.Where(p => p.Id == user.ProfileId).FirstOrDefault().Description);
            }

            if (ModelState.IsValid)
            {
                var result = UserBLL.UpdateUser(user);

                if (result)
                {
                    response = new Response<bool>()
                    {
                        Result = true,
                        Model = result,
                        Message = "El usuario ha sido Actualizado con éxito.",
                        Title = "Usuarios"
                    };

                    return Json(response);
                }

            }

            response = new Response<bool>()
            {
                Result = false,
                Model = false,
                Message = getModelError(ModelState.Values),
                Title = "Usuarios"
            };

            return Json(response);
        }

        [Authorize(Roles = "Súper_Administrador,Administrador,Representantes")]
        public ActionResult ChangePassword()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Súper_Administrador,Administrador,Representantes")]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (model.OldPassword.Equals(model.NewPassword))
                    ModelState.AddModelError(string.Empty, "La nueva contraseña debe ser diferente a la contraseña actual.");

                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var userName = User.Identity.GetUserName();
                var user = UserBLL.GetUserByUserName(userName);

                if (user != null)
                {
                    var changedPass = Helper.UserHelper.ChangePassword(userName, model.OldPassword, model.NewPassword);
                    if (changedPass)
                    {
                        user.IsFirstUse = false;
                        user.Password = model.NewPassword;
                        if (UserBLL.UpdateUser(user))
                            return RedirectToAction("Index", "Professionals");

                        ModelState.AddModelError(string.Empty, "No fue posible actualizar la contraseña.");
                    }
                }

                ModelState.AddModelError(string.Empty, "La contraseña actual ingresada no es valida.");
            }

            return View(model);
        }


        //EnableDisableUser
        [Authorize(Roles = "Súper_Administrador")]
        public ActionResult EnableDisableUser(int? id)
        {
            var response = new Response<bool>();
            var user = UserBLL.GetUserById((int)id);
            if (user == null)
            {
                response = new Response<bool>()
                {
                    Result = false,
                    Model = false,
                    Message = "No es posible encontrar el usuario.",
                    Title = "Usuarios"
                };

                return Json(response);
            }
            else
            {
                var message = string.Empty;
                if (user.IsActive)
                {
                    message = "El usuario ha sido Inactivado.";
                    user.IsActive = false;
                }
                else
                {
                    message = "El usuario ha sido Habilitado.";
                    user.IsActive = true;
                }
                    

                if (UserBLL.UpdateUser(user))
                {
                    response = new Response<bool>()
                    {
                        Result = false,
                        Model = false,
                        Message = message,
                        Title = "Usuarios"
                    };
                }
            }

            return Json(response);
        }

        public JsonResult GetCitiesByDepartamentIdAsync(int departmentId)
        {
            var cities = CityBLL.GetCitiesByDepartamentId(departmentId);
            return Json(cities);
        }

        private string getModelError(ICollection<ModelState> values)
        {
            return "• " + string.Join("</br> • ", values
                    .SelectMany(x => x.Errors)
                    .Select(x => x.ErrorMessage));
        }
    }
}
