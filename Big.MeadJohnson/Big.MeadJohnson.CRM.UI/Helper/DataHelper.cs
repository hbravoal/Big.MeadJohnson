﻿using Big.MeadJohnson.CRM.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.MeadJohnson.CRM.UI.Helper
{
    public class DataHelper
    {
        public static List<DataGeneralty> MotnGenerate()
        {
            List<DataGeneralty> moth = new List<DataGeneralty>();
            moth.Add(new DataGeneralty{Value = 1,Text = "Enero"});
            moth.Add(new DataGeneralty { Value = 2, Text = "Febrero" });
            moth.Add(new DataGeneralty { Value = 3, Text = "Marzo" });
            moth.Add(new DataGeneralty { Value = 4, Text = "Abril" });
            moth.Add(new DataGeneralty { Value = 5, Text = "Mayo" });
            moth.Add(new DataGeneralty { Value = 6, Text = "Junio" });
            moth.Add(new DataGeneralty { Value = 7, Text = "Julio" });
            moth.Add(new DataGeneralty { Value = 8, Text = "Agosto" });
            moth.Add(new DataGeneralty { Value = 9, Text = "Septiembre" });
            moth.Add(new DataGeneralty { Value = 10, Text = "Octubre" });
            moth.Add(new DataGeneralty { Value = 11, Text = "Noviembre" });
            moth.Add(new DataGeneralty { Value = 12, Text = "Diciembre" });

            return moth; 
        }
        public static List<DataGeneralty> NumbersGenerate(int length)
        {
            List<DataGeneralty> day = new List<DataGeneralty>();
            for (int i = 1; i <= length; i++)
            {

                day.Add(new DataGeneralty { Value = i, Text = i.ToString() });
            }

            return day;
        }

        public static List<DataGeneralty> TypeAgeGenerate()
        {
            List<DataGeneralty> type = new List<DataGeneralty>();
            type.Add(new DataGeneralty { Value = 1, Text = "Meses" });
            type.Add(new DataGeneralty { Value = 2, Text = "Años" });

            return type;
        }

    }
}