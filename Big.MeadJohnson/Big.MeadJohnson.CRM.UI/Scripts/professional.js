﻿/// <reference path="jquery.listswap.js" />
/// <reference path="jquery.listswap.js" />
/// <reference path="jquery.listswap.js" />
$(document).ready(function () {
    

    $('#tab7').click(function () {
        $('#canvas').attr('id', 'canvas_ocult');
        $('#limpiar').attr('id', 'limpiar_ocult');
    });
    $('#tab6').click(function () {
        $('#canvas').attr('id', 'canvas_ocult');
        $('#limpiar').attr('id', 'limpiar_ocult');
        $('#canvas_ocult').attr('id', 'canvas');
        $('#limpiar_ocult').attr('id', 'limpiar');
        
    });


    

    $('#validateCedula').click(function () {
        var documentCatch = $('#cedula').val();
        $('#SecretaryFirstName').val("");
        $('#SecretaryLastName').val("");
        $('#SecretaryEmail').val("");
        $('#SecretaryBirthDay').val("");
        $('#SecretaryBirthMonth').val("");
        $('#SecretaryBirthYear').val("");
        $('#SecretaryAddress').val("");
        $('#SecretaryHaveSon').val("");
        $('#SecretaryCellPhone').val("");
        $('#secretaryInformation').val("");
        $('#SecretaryCompany').val("");
        
       
        try {
            
            $.ajax({
                type: 'POST',
                url: $("#hfUrlBase").val() + 'Professionals/ValidateSecretaryExits',
                data: { 'cedula': $('#SecretaryDocumentNumber').val() },
                dataType: 'json',
                
                success: function (specialities) {
                    if (specialities.PropertyMessage != null) {
                        $('#modalTitle').html('Administracion profesionales');
                        $('#modalMessage').html(specialities.PropertyMessage);
                        $('#genericModal').modal('show');
                        $("#validateCedula").removeClass('btn-default').addClass('btn-danger');
                    
                        
                        
                    }
                    else {
                        $('#SecretaryFirstName').val(specialities.SecretaryFirstName);
                        $('#SecretaryLastName').val(specialities.SecretaryLastName);
                        $('#SecretaryEmail').val(specialities.SecretaryEmail);
                        $('#SecretaryBirthDay').val(specialities.SecretaryBirthDay);
                        $('#SecretaryBirthMonth').val(specialities.SecretaryBirthMonth);
                        $('#SecretaryBirthYear').val(specialities.SecretaryBirthYear);
                        $('#SecretaryAddress').val(specialities.SecretaryAddress);
                        $('#SecretaryHaveSon').val(specialities.SecretaryHaveSon);
                        $('#SecretaryCellPhone').val(specialities.SecretaryCellPhone);
                        $('#SecretaryCompany').val(specialities.SecretaryCompany);
                        if (specialities.SecretaryHabbeasData != null) {
                            

                            $("#myImgSecretary").show();
                            $("#myImgSecretary").attr("src", ($("#hfUrlBase").val()+specialities.SecretaryHabbeasData));
                        }
                        if (specialities.SecretaryHaveSon) {
                            $('#SecretaryHaveSon').prop("checked", true);
                        }
                        else {
                            $('#NoSecretaryHaveSon').prop("checked", true);
                        }
                        $("#validateCedula").removeClass('btn-default').addClass('btn-success');
                    }
                    
                    
                  
                },
            });
        }
        catch (err) {
            $("#SpecialityId").empty();
            $("#SpecialityId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
        }
    });

    var roleId = $('#roleId').val();

    if (roleId == 3) {
        $(".divUserInfo").hide();
    } else {
        $(".divUserInfo").show();
    }
 
            


    if ($('#GonnaDaddyQuestion').is(':checked')) {
        $('#divGonnaDaddy').show();
        $("#tableGonnaDaddy").show();
    } else {
        $('#divGonnaDaddy').hide();
        $("#tableGonnaDaddy").hide();
    }


   $('#DayStimated').val('');
      $('#DayStimated').val('');
    function activaTab(tab) {
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };

    var selectedVal = $("#ProfileId option:selected").text();
    if (selectedVal == 'Representantes') {
        $('#departament').show();
        $('#city').show();
    } else {
        $('#departament').hide();
        $('#city').hide();
    }


    if ($("#ProfileId option:selected").text() == 3) {
        $('#departament').show();
        $('#city').show();
    }

    $("#pCurrentDateLabor").show();
    $('#divCurrentDatelabor').show();



    //divIsStudent


    if ($('#IsStudent').is(':checked')) {
        $('#divIsStudent').show();
    } else {
        $('#divIsStudent').hide();
    }


    $('#IsStudent').change(function () {
        if ($(this).is(":checked")) {
            $('#divIsStudent').show();
        } else {
            $('#divIsStudent').hide();
        }
    });
    //Information Contact GonnaDaddy
    $('#GonnaDaddyQuestion').change(function () {
        if ($(this).is(":checked")) {
            $("#divGonnaDaddy").show();
            $("#tableGonnaDaddy").show();
            
        } else {
            $("#divGonnaDaddy").hide();
            $("#tableGonnaDaddy").hide();
        }
    });

    $("#tableGonnaDaddy").on("click", ".deleteGonnaDaddy", function (e) {
        e.preventDefault();
        $(this).parents("tr").remove();
    });

    $('#btnSaveGonnaDaddy').click(function () {

        var month = $('#MonthStimated').val();
        var day = $('#DayStimated').val();
        var monthName = $("#MonthStimated option:selected").text();
        

        if (month !== '') {
            if (month <= 0) {
                $('#modalTitle').html('Administracion profesionales');
                $('#modalMessage').html('El mes debe ser mayor a 0.');
                $('#genericModal').modal('show');
                return;
            }
            if (month > 12) {
                $('#modalTitle').html('Administracion profesionales');
                $('#modalMessage').html('Ingrese un mes inválido.');
                $('#genericModal').modal('show');
                return;
            }
            if (day > 31) {
                $('#modalTitle').html('Administracion profesionales');
                $('#modalMessage').html('Ingrese un día inválido.');
                $('#genericModal').modal('show');
                return;
            }
            if (day <= 0 && day!='') {
                $('#modalTitle').html('Administracion profesionales');
                $('#modalMessage').html('El día debe ser mayor as 0.');
                $('#genericModal').modal('show');
                return;
            }   
            $('#tableGonnaDaddy > tbody:last-child').append('<tr><td class="DayStimated">' + day + '</td>' +
                '<td class="MonthStimatedName">' + monthName + '</td>' +
                        '<td class="MonthStimated hide ">' + month + '</td>' +
                        
                        '<td><button class="btn btn-sm btn-warning deleteGonnaDaddy" type="button">Eliminar</button></td></tr >');
            $('#MonthStimated').val('');
            $('#DayStimated').val('');
                    
                }
        
        else {
            $('#modalTitle').html('Administracion profesionales');
            $('#modalMessage').html('El mes es  requerido.');
            $('#genericModal').modal('show');
        }
    });

    //FIN Gonna Daddy
    //divIsSpecialist

    if ($('#IsSpecialist').is(':checked')) {
        $('#divIsSpecialist').show();
    } else {
        $('#divIsSpecialist').hide();
    }

    $('#IsSpecialist').change(function () {
        if ($(this).is(":checked")) {
            $('#divIsSpecialist').show();
        } else {
            $('#divIsSpecialist').hide();
        }
    });

    if ($('#SecretaryHaveSecretary').is(':checked')) {
        $('#divInfoSecretary').show();
    } else {
        $('#divInfoSecretary').hide();
    }

    $('#SecretaryHaveSecretary').change(function () {
        if ($(this).is(":checked")) {
            $('#divInfoSecretary').show();
        } else {
            $('#divInfoSecretary').hide();
        }
    });


    if ($('#IsFather').val() == 'true') {
        $('#IsFather').prop('checked', true);
    } else {
        $('#IsFather').prop('checked', false);
    }
    
    if ($('#IsGrandFather').val() == 'true') {
        $('#IsGrandFather').prop('checked', true);
    } else {
        $('#IsGrandFather').prop('checked', false);
    }

    if ($('#IsFather').is(':checked')) {
        $("#divIsFather").show();
        $("#tableSon").show();
    } else {
        $("#divIsFather").hide();
        $("#tableSon").hide();
    }

    if ($('#IsGrandFather').is(':checked')) {
        $("#divIsGrandFather").show();
        $("#tableGrandchild").show();
    } else {
        $("#divIsGrandFather").hide();
        $("#tableGrandchild").hide();
    }

    $('#IsFather').change(function () {
        if ($(this).is(":checked")) {
            $("#divIsFather").show();
            $("#tableSon").show();
        } else {
            $("#divIsFather").hide();
            $("#tableSon").hide();
        }
    });

    $('#IsGrandFather').change(function () {
        if ($(this).is(":checked")) {
            $("#divIsGrandFather").show();
            $("#tableGrandchild").show();
        } else {
            $("#divIsGrandFather").hide();
            $("#tableGrandchild").hide();
        }
    });

    var profesionType = $('#hfProfessionType').val();
    if (profesionType == 4) {
        $("#divStartProfession").hide();
        $('#pStarProfession').hide();
        $("#divBoss").show();
        $("#pCurrentDateLabor").show();
        $('#divCurrentDatelabor').show();
        //$('#divSecretaryProfessionalsAssign').show();

    }
    else {
        $("#divStartProfession").show();
        $('#pStarProfession').show
        $("#divBoss").hide();
        $("#pCurrentDateLabor").hide();
        $('#divCurrentDatelabor').hide();
        //$('#divSecretaryProfessionalsAssign').hide();
        
    }

    var selectedVal = $("#SpecialityId option:selected").text();
    if (selectedVal === 'Otra') {
        $('#divSpeciality').show();
    } else {
        $('#divSpeciality').hide();
    }

    $('#SpecialityId').change(function () {
        var selectedVal = $("#SpecialityId option:selected").text();
        if (selectedVal === 'Otra') {
            $('#divSpeciality').show();
        } else {
            $('#divSpeciality').hide();
        }
    });


    $('#btnSaveSon').click(function () {

        var name = $('#Son_Name').val();
        var Age = $('#Son_Age').val();
        var SchoolYear = $('#SchoolYearSon').val();
        var IsYearAge = $('#Son_IsYearAge').val();
        var SchoolYearName = $("#SchoolYearSon option:selected").text();
        var Son_IsYearAgeName = $("#Son_IsYearAge option:selected").text();
        

        if (SchoolYearName == '[Seleccione]')
            SchoolYearName = '';
        if (Son_IsYearAgeName == '[Seleccione]') {
            $('#modalTitle').html('Administracion profesionales');
            $('#modalMessage').html('El campo Mes/Año es requerido.');
            $('#genericModal').modal('show');
            return;
        }
        if (Age !== '') {

            if (SchoolYear != '') {
                if (parseInt(SchoolYear) < 0 || isNaN(SchoolYear)) {
                    $('#modalTitle').html('Administracion profesionales');
                    $('#modalMessage').html('La escolaridad debe ser mayor a 0.');
                    $('#genericModal').modal('show');
                    return;
                }
                $('#tableSon > tbody:last-child').append('<tr><td class="name">' + name + '</td>' +
                    '<td class="age">' + Age + '</td>' +
                    '<td class="Son_IsYearAge hide">' + IsYearAge + '</td>' +
                        '<td class="Son_IsYearAgeName">' + Son_IsYearAgeName + '</td>' +
                    '<td class="SchoolYearName">' + SchoolYearName + '</td>' +
                    '<td class="SchoolYear hide">' + SchoolYear + '</td>' +
                    '<td><button class="btn btn-sm btn-warning deleteSon" type="button">Eliminar</button></td></tr >');
                $('#Son_Name').val('');
                $('#Son_Age').val('');
                $('#Son_IsYearAge').val('');
                $('#Son_SchoolYear').val('');

            } else {

                if (Age <= 0) {
                    $('#modalTitle').html('Administracion profesionales');
                    $('#modalMessage').html('la edad debe ser mayor a 0.');
                    $('#genericModal').modal('show');
                } else {
                    $('#tableSon > tbody:last-child').append('<tr><td class="name">' + name + '</td>' +
                        '<td class="age">' + Age + '</td>' +
                        '<td class="Son_IsYearAge hide">' + IsYearAge + '</td>' +
                        '<td class="Son_IsYearAgeName">' + Son_IsYearAgeName + '</td>' +
                        '<td class="SchoolYearName">' + SchoolYearName + '</td>' +
                        '<td class="SchoolYear hide">' + SchoolYear + '</td>' +
                        '<td><button class="btn btn-sm btn-warning deleteSon" type="button">Eliminar</button></td></tr >');
                    $('#Son_Name').val('');
                    $('#Son_Age').val('');
                    $('#Son_IsYearAge').val('');
                    $('#Son_SchoolYear').val('');
                }
            }
        }
        else {
            $('#modalTitle').html('Administracion profesionales');
            $('#modalMessage').html('La edad es requerida.');
            $('#genericModal').modal('show');
        }
    });

    $('#btnSaveGrandchild').click(function () {

        var name = $('#Grandchild_Name').val();
        var Age = $('#Grandchild_Age').val();
        var Grand_IsYearAgeName = $("#Grandchild_IsYearAge option:selected").text();
        var IsYearAge = $('#Grandchild_IsYearAge').val();
        
        var SchoolYear = $('#SchoolYearGrandChild').val();
        var SchoolYearName = $("#SchoolYearGrandChild option:selected").text();

        if (SchoolYearName == '[Seleccione]')
            SchoolYearName = '';
        if (Grand_IsYearAgeName  == '[Seleccione]') {
            $('#modalTitle').html('Administracion profesionales');
            $('#modalMessage').html('El campo Mes/Año es requerido.');
            $('#genericModal').modal('show');
            return;
        }
        if (Age !== '') {


            if (SchoolYear != '') {
                if (parseInt(SchoolYear) < 0 || isNaN(SchoolYear)) {
                    $('#modalTitle').html('Administracion profesionales');
                    $('#modalMessage').html('La escolaridad debe ser mayor a 0.');
                    $('#genericModal').modal('show');
                    return;
                }

                $('#tableGrandchild > tbody:last-child').append('<tr><td class="name">' + name + '</td>' +
                    '<td class="age">' + Age + '</td>' +
                    '<td class="Son_IsYearAgeName">' + Grand_IsYearAgeName + '</td>' +
                    '<td class="Son_IsYearAge hide">' + IsYearAge + '</td>' +
                    '<td class="SchoolYearName">' + SchoolYearName + '</td>' +
                    '<td class="SchoolYear hide">' + SchoolYear + '</td>' +
                    '<td><button class="btn btn-sm btn-warning deleteGrandchild" type="button">Eliminar</button></td></tr>');
                $('#Grandchild_Name').val('');
                $('#Grandchild_Age').val('');
                $('#Grandchild_SchoolYear').val('');
            } else {

                if (Age !== '') {
                    if (Age <= 0) {
                        $('#modalTitle').html('Administracion profesionales');
                        $('#modalMessage').html('la edad debe ser mayor a 0.');
                        $('#genericModal').modal('show');
                    } else {
                        $('#tableGrandchild > tbody:last-child').append('<tr><td class="name">' + name + '</td>' +
                            '<td class="age">' + Age + '</td>' +
                              '<td class="Son_IsYearAgeName">' + Grand_IsYearAgeName + '</td>' +
                                '<td class="Son_IsYearAge hide">' + IsYearAge + '</td>' +                            
                            '<td class="SchoolYearName">' + SchoolYearName + '</td>' +
                            '<td class="SchoolYear hide">' + SchoolYear + '</td>' +
                            '<td><button class="btn btn-sm btn-warning deleteGrandchild" type="button">Eliminar</button></td></tr>');
                        $('#Grandchild_Name').val('');
                        $('#Grandchild_Age').val('');
                        $('#Grandchild_SchoolYear').val('');
                    }
                } else {
                    $('#modalTitle').html('Administracion profesionales');
                    $('#modalMessage').html('La edad es requerida.');
                    $('#genericModal').modal('show');
                }
            }
        } else {

            $('#modalTitle').html('Administracion profesionales');
            $('#modalMessage').html('La edad es requerida.');
            $('#genericModal').modal('show');
        }
    });

    $('#IsSpecialist').click(function () {
        if ($('#IsSpecialist').is(':checked')) {
            $('#IsSpecialist').prop('checked', true);
            try {
                $("#SpecialityId").empty();
                $.ajax({
                    type: 'POST',
                    url: $("#hfUrlBase").val() + 'Professionals/GetSpecialities',
                    dataType: 'json',
                    success: function (specialities) {
                        $("#SpecialityId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                        $.each(specialities, function (i, speciality) {
                            $("#SpecialityId").append('<option value="'
                                + speciality.Id + '">'
                                + speciality.Name + '</option>');
                        });
                    },
                });
            }
            catch (err) {
                $("#SpecialityId").empty();
                $("#SpecialityId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
            }
        }
        else {
            $('#IsSpecialist').prop('checked', false);
            $("#SpecialityId").empty();
            $("#SpecialityId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
        }
    });

    $('#btnSaveGeneralInformation').click(function () {

        var ListSon = [];
        var ListGrandchild = [];
        var ListGonnaDaddy = [];
        if ($('#GonnaDaddyQuestion').is(':checked')) {
            $('#tableGonnaDaddy > tbody tr').each(function () {
                var month = $(this).find(".MonthStimated").html();
                var Age = $(this).find(".DayStimated").html();

                var gonnaDaddy = {
                    MonthStimated: month,
                    DayStimated: Age,
                    GonnaDaddyQuestion: true,

                };


                ListGonnaDaddy.push(gonnaDaddy);

            });
            if (ListGonnaDaddy.length == 0) {
                $('#modalTitle').html('Administracion profesionales');
                $('#modalMessage').html('Si indica que será padre/madre debe diligenciar los datos correspondientes');
                $('#genericModal').modal('show');
                return;

            }
        }
        if ($('#IsFather').is(':checked')) {
            $('#tableSon > tbody tr').each(function () {
                var name = $(this).find(".name").html();
                var Age = $(this).find(".age").html();
                var IsYearAge = $(this).find(".Son_IsYearAge").html();
                var SchoolYear = $(this).find(".SchoolYear").html();

                var son = {
                    Name: name,
                    Age: Age,
                    SchoolYear: SchoolYear,
                    IsYearAge: IsYearAge,
                };

                ListSon.push(son);
            });
        }

        if ($('#IsGrandFather').is(':checked')) {
            $('#tableGrandchild > tbody tr').each(function () {
                var name = $(this).find(".name").html();
                var Age = $(this).find(".age").html();
                var SchoolYear = $(this).find(".SchoolYear").html();
                var IsYearAge = $(this).find(".Son_IsYearAge").html();
                var Grandchild = {
                    Name: name,
                    Age: Age,
                    IsYearAge: IsYearAge,
                    SchoolYear: SchoolYear,
                };

                ListGrandchild.push(Grandchild);
            });
        }

        var model = {

            Guid: $('#Guid').val(),
            Id: 0,
            ProfessionalId: $('#ProfessionalId').val(),
            IsFather: $('#IsFather').is(':checked'),
            IsGrandFather: $('#IsGrandFather').is(':checked'),
            ListSon: ListSon,
            ListGrandchild: ListGrandchild,
            ListGonnaDaddy: ListGonnaDaddy,
        }

        var idButton = 'btnSaveGeneralInformation';
        var nextTab = 5
        AjaxFunction('SaveGeneralInformation', model, nextTab, idButton);
    });

    $('#btnSaveSecretary').click(function () {

        var model = {

            SecretaryGuid: $('#SecretaryGuid').val(),
            SecretaryId: $('#SecretaryId').val(),
            ProfessionalId: $('#ProfessionalId').val(),
            SecretaryHaveSecretary: $('#SecretaryHaveSecretary').is(':checked'),
            SecretaryFirstName: $('#SecretaryFirstName').val(),
            SecretaryLastName: $('#SecretaryLastName').val(),
            SecretaryDocumentNumber: $('#SecretaryDocumentNumber').val(),
            SecretaryEmail: $('#SecretaryEmail').val(),
            SecretaryCellPhone: $('#SecretaryCellPhone').val(),
            SecretaryAddress: $('#SecretaryAddress').val(),
            SecretaryCompany: $('#SecretaryCompany').val(),
            SecretaryHaveSon: $('#SecretaryHaveSon').is(':checked'),
            SecretaryBirthDay: $('#SecretaryBirthDay').val(),
            SecretaryBirthMonth: $('#SecretaryBirthMonth').val(),
            SecretaryBirthYear: $('#SecretaryBirthYear').val()
        }

        var idButton = 'btnSaveSecretary';
        var nextTab = 6
        AjaxFunction('SaveSecretaryInformation', model, nextTab, idButton);

    });

    $("#LaborDepartamentId").change(function () {
        $("#LaborCityId").empty();
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/GetCitiesByDepartamentIdAsync',
            dataType: 'json',
            data: { departmentId: $("#LaborDepartamentId").val() },
            success: function (cities) {
                $("#LaborCityId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(cities, function (i, city) {
                    $("#LaborCityId").append('<option value="'
                        + city.Id + '">'
                        + city.Name + '</option>');
                });
            },
        });
        return false;
    });
    $("#waitingGIF").hide();
  
    $("#LaborCityId").change(function () {
     //Coloca GIF limpia lo generado por ListWap y lo vuelve a generar al traer los datos.
        $("#waitingGIF").show();
        
        $(".listbox_controls").remove();
        $("#listbox_source_wrapper").remove();
        $("#listbox_destination_wrapper").remove();
        $("#LaborCityId").attr("disabled", "disabled");
        $.ajax({

            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/GetAllProfessionalsByCityId',
            dataType: 'json',
            data: { cityId: $("#LaborCityId").val() },
            success: function (cities) {
                $.each(cities, function (i, item) {
                    $("#source").append('<option value="' + item.Id+'">'
                        + item.FirstName + " " + item.LastName + "</option>");
                });
                $('#source, #destination').listswap({
                    truncate: true,
                    height: 135,
                    is_scroll: true,
                    label_add: 'Agergar', //Set add label (Can bet set to empty)
                    label_remove: 'Quitar',
                });
                $("#LaborCityId").removeAttr("disabled");
                $("#waitingGIF").hide();
               
            },
        });
        return false;
    });

    $('#btnSaveLaboralInformation').click(function () {
        var dataValue="";
        $('#listbox_destination_wrapper >ul li').each(function () {
           
            dataValue =dataValue + $(this).attr("data-value")+"|";
            
         
            //var Age = $(this).find(".DayStimated").html();

            //var gonnaDaddy = {
            //    MonthStimated: month,
            //    DayStimated: Age,
            //    GonnaDaddyQuestion: true,

        });
       
      
        
        var model = {

            Guid: $('#Guid').val(),
            Id: 0,
            ProfessionalId: $('#ProfessionalId').val(),
            StartProfessionDay: $('#StartProfessionDay').val(),
            StartProfessionMonth: $('#StartProfessionMonth').val(),
            StartProfessionYear: $('#StartProfessionYear').val(),
            IsSpecialist: $('#IsSpecialist').is(':checked'),
            SpecialityId: $('#SpecialityId').val(),
            OtherSpeciality: $('#OtherSpeciality').val(),
            IsStudent: $('#IsStudent').is(':checked'),
            ProfessionalLevelId: $('#ProfessionalLevelId').val(),
            StartStudyDay: $('#StartStudyDay').val(),
            StartStudyMonth: $('#StartStudyMonth').val(),
            StartStudyYear: $('#StartStudyYear').val(),
            EndStudyDay: $('#EndStudyDay').val(),
            EndStudyMonth: $('#EndStudyMonth').val(),
            EndStudyYear: $('#EndStudyYear').val(),
            Company: $('#Company').val(),
            BossName: $('#BossName').val(),
            CurrentProfessionDay: $('#CurrentProfessionDay').val(),
            CurrentProfessionMonth: $('#CurrentProfessionMonth').val(),
            CurrentProfessionYear: $('#CurrentProfessionYear').val(),
            AssociatedProfessionals:dataValue,

        }

        var idButton = 'btnSaveLaboralInformation';
        var nextTab = 4
        AjaxFunction('SaveLaborInformation', model, nextTab, idButton);
    });

    $('#btnSaveProfessional').click(function () {
        var dataValue = "";
        $('#listbox_destinationRepr_wrapper >ul li').each(function () {

            dataValue = dataValue + $(this).attr("data-value") + "|";
        });
        var model = {

            Guid: $('#Guid').val(),
            Id: 0,
            ProfessionalTypeId: $('#ProfessionalTypeId').val(),
            DocumentNumber: $('#DocumentNumber').val(),
            FirstName: $('#FirstName').val(),
            SecondName: $('#SecondName').val(),
            LastName: $('#LastName').val(),
            SecondLastName: $('#SecondLastName').val(),
            GenderId: $('#GenderId').val(),
            BirthDay: $('#BirthDay').val(),
            BirthMonth: $('#BirthMonth').val(),
            BirthYear: $('#BirthYear').val(),
            DepartamentId: $('#DepartamentId').val(),
            CityId: $('#CityId').val(),
            MsrId: $('#MsrId').val(),
            RepresentantesSecundarios: dataValue
        }

        var idButton = 'btnSaveProfessional';
        var nextTab = 2;
        var selectedVal = $("#ProfessionalTypeId option:selected").text();
        var profesionType = $('#hfProfessionType').val();
       
        if (selectedVal == 'Secretaria') {
            $('#tab5').hide();
            
        }

        AjaxFunction('CreateProfessional', model, nextTab, idButton);


    });

    $('#btnSaveContact').click(function () {

        var model = {
            Guid: $('#Guid').val(),
            Id: $('#Id').val(),
            ProfessionalId: $('#ProfessionalId').val(),
            ProfessionalEmail: $('#ProfessionalEmail').val(),
            ProfessionalCellPhone: $('#ProfessionalCellPhone').val(),
            ProfessionalAddress: $('#ProfessionalAddress').val(),
            SpecialityId: $('#SpecialityId').val(),
            ProfessionalDepartamentId: $('#ProfessionalDepartamentId').val(),
            ProfessionalCityId: $('#ProfessionalCityId').val(),
            ProfessionalTypeId: $('#ProfessionalTypeId').val(),
            ProfessionalBrickId: $('#ProfessionalBrickId').val(),

        }

        var idButton = 'btnSaveContact';
        var nextTab = 3
        AjaxFunction('SaveProfessionalContact', model, nextTab, idButton);
    });

    $("#ProfessionalDepartamentId").change(function () {
        $("#ProfessionalCityId").empty();
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/GetCitiesByDepartamentIdAsync',
            dataType: 'json',
            data: { departmentId: $("#ProfessionalDepartamentId").val() },
            success: function (cities) {
                $("#ProfessionalCityId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(cities, function (i, city) {
                    $("#ProfessionalCityId").append('<option value="'
                        + city.Id + '">'
                        + city.Name + '</option>');
                });
            },
        });
        return false;
    });

    $("#ProfessionalCityId").change(function () {
        $("#ProfessionalBrickId").empty();
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/GetBricksByCityId',
            dataType: 'json',
            data: { cityId: $("#ProfessionalCityId").val() },
            success: function (bricks) {
                $("#ProfessionalBrickId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(bricks, function (i, brick) {
                    $("#ProfessionalBrickId").append('<option value="'
                        + brick.Code + '">'
                        + brick.Code + '</option>');
                });
            },
        });
        return false;
    });

    function AjaxFunction(controller, model, nextTab, idButton) {
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/' + controller,
            ContentType: 'application/json; charset=utf-8',
            data: { model: model },
            error: function (data) {
                alert('Ha ocurrido un error al. Intente nuevamente o comuniquese con el administrador.');
            },
        })
            .done(function (data) {

                if (data.Result === false) {
                    $('#' + idButton).prop('disabled', false);
                    $('#modalTitle').html(data.Title);
                    $('#modalMessage').html(data.Message);
                    $('#genericModal').modal('show');
                }
                else {
                    $('#' + idButton).prop('disabled', true);
                    activaTab('tab' + nextTab);
                    $("#tab1").prop('disabled', true);
                    $('#modalTitle').html(data.Title);
                    $('#modalMessage').html(data.Message);
                    $('#genericModal').modal('show');
                }
            });
    }

    function isCheckedById(id) {
        alert(id);
        var checked = $("input[@id=" + id + "]:checked").length;
        alert(checked);

        if (checked === 0) {
            return false;
        } else {
            return true;
        }
    }


    $('#ProfileId').change(function () {
        var selectedVal = $("#ProfileId option:selected").text();

        if (selectedVal == 'Representantes') {
            $('#departament').show();
            $('#city').show();
        } else {
            $('#departament').hide();
            $('#city').hide();
        }
        return;
    });

    $("#ProfessionalTypeId").change(function () {

        var profesionType = $('#ProfessionalTypeId').val();

        if (profesionType == 4) {
            $("#divStartProfession").hide();
            $('#pStarProfession').hide();
            $("#divBoss").show();
            $("#pCurrentDateLabor").show();
            $('#divCurrentDatelabor').show();
            $('#divSecretaryProfessionalsAssign').show();
            $('#divIsSecretary').hide();
            
        }
        else {
            $('#divIsSecretary').show();
            $("#divStartProfession").show();
            $('#pStarProfession').show
            $("#divBoss").hide();
            $("#pCurrentDateLabor").hide();
            $('#divCurrentDatelabor').hide();
            $('#divSecretaryProfessionalsAssign').hide();
        }

    });

    $("#DepartamentId").change(function () {
        $("#CityId").empty();
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/GetCitiesByDepartamentIdAsync',
            dataType: 'json',
            data: { departmentId: $("#DepartamentId").val() },
            success: function (cities) {
                $("#CityId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(cities, function (i, city) {
                    $("#CityId").append('<option value="'
                        + city.Id + '">'
                        + city.Name + '</option>');
                });
            },
        });
        return false;
    });

    $("#CityId").change(function () {
        $("#MsrId").empty();
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/GetUserByCityId',
            dataType: 'json',
            data: { cityId: $("#CityId").val() },
            success: function (users) {
                $("#MsrId").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(users, function (i, user) {
                    $("#MsrId").append('<option value="'
                        + user.Id + '">'
                        + user.Name + '</option>');
                });
            },
        });
        return false;
    });

    $(function () {
        $('.table-striped').on('keydown', '.SchoolYear', function (e) { -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true)) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault() });
    })

    $(function () {
        $('.table-striped').on('keydown', '.age', function (e) { -1 !== $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) || (/65|67|86|88/.test(e.keyCode) && (e.ctrlKey === true || e.metaKey === true)) && (!0 === e.ctrlKey || !0 === e.metaKey) || 35 <= e.keyCode && 40 >= e.keyCode || (e.shiftKey || 48 > e.keyCode || 57 < e.keyCode) && (96 > e.keyCode || 105 < e.keyCode) && e.preventDefault() });
    })

});




//Edicion Profesionales

$(document).ready(function () {

    $('#btnUpdateGeneralInformation').click(function () {

        var ListSon = [];
        var ListGrandchild = [];
        var ListGonnaDaddy = [];

        if ($('#GonnaDaddyQuestion').is(':checked')) {
            $('#tableGonnaDaddy > tbody tr').each(function () {
                var month = $(this).find(".MonthStimated").html();
                var Age = $(this).find(".DayStimated").html();
                
                var gonnaDaddy = {
                    MonthStimated: month,                    
                    DayStimated: Age,
                    GonnaDaddyQuestion:true,
                 
                };
          

                ListGonnaDaddy.push(gonnaDaddy);
              
            });
            if (ListGonnaDaddy.length == 0) {
                $('#modalTitle').html('Administracion profesionales');
                $('#modalMessage').html('Si indica que será padre/madre debe diligenciar los datos correspondientes');
                $('#genericModal').modal('show');
                return;

            }
        }
     
        if ($('#IsFather').is(':checked')) {
            $('#tableSon > tbody tr').each(function () {
                var name = $(this).find(".name").html();
                var Age = $(this).find(".age").html();
                var IsYearAge = $(this).find(".Son_IsYearAge").html();
                var SchoolYear = $(this).find(".SchoolYear").html();

                var son = {
                    Name: name,
                    Age: Age,
                    SchoolYear: SchoolYear,
                    IsYearAge: IsYearAge,
                };

                ListSon.push(son);
            });
        }

        if ($('#IsGrandFather').is(':checked')) {
            $('#tableGrandchild > tbody tr').each(function () {
                var name = $(this).find(".name").html();
                var Age = $(this).find(".age").html();
                var IsYearAge = $(this).find(".Son_IsYearAge").html();
                
                var SchoolYear = $(this).find(".SchoolYear").html();

                var Grandchild = {
                    Name: name,
                    Age: Age,
                    SchoolYear: SchoolYear,
                    IsYearAge: IsYearAge,
                };

                ListGrandchild.push(Grandchild);
            });
        }

        var model = {

            Guid: $('#Guid').val(),
            Id: 0,
            ProfessionalId: $('#ProfessionalId').val(),
            IsFather: $('#IsFather').is(':checked'),
            IsGrandFather: $('#IsGrandFather').is(':checked'),
            ListSon: ListSon,
            ListGrandchild: ListGrandchild,
            ListGonnaDaddy: ListGonnaDaddy,
        }
        
        //if (ListSon.length == 0 && ListGrandchild.length == 0 && ListGonnaDaddy.length == 0) {
        //    $('#modalTitle').html('Administracion profesionales');
        //    $('#modalMessage').html('Para ésta acción necesita diligenciar los campos');
        //    $('#genericModal').modal('show');
        //    return;
            
        //}
        
        
        
        var nextTab = 5;
        AjaxFunctionUpdate('UpdateGeneralInformation', model, nextTab);
    });

    $('#btnUpdateSecretary').click(function () {

        var model = {

            SecretaryGuid: $('#SecretaryGuid').val(),
            SecretaryId: $('#SecretaryId').val(),
            ProfessionalId: $('#ProfessionalId').val(),
            SecretaryHaveSecretary: $('#SecretaryHaveSecretary').is(':checked'),
            SecretaryFirstName: $('#SecretaryFirstName').val(),
            SecretaryLastName: $('#SecretaryLastName').val(),
            SecretaryDocumentNumber: $('#SecretaryDocumentNumber').val(),
            SecretaryEmail: $('#SecretaryEmail').val(),
            SecretaryCellPhone: $('#SecretaryCellPhone').val(),
            SecretaryAddress: $('#SecretaryAddress').val(),
            SecretaryCompany: $('#SecretaryCompany').val(),
            SecretaryHaveSon: $('#SecretaryHaveSon').is(':checked'),
            SecretaryBirthDay: $('#SecretaryBirthDay').val(),
            SecretaryBirthMonth: $('#SecretaryBirthMonth').val(),
            SecretaryBirthYear: $('#SecretaryBirthYear').val()
        }


        var nextTab = 6
        AjaxFunctionUpdate('UpdateSecretaryInformation', model, nextTab);

    });

    $('#btnUpdateLaborInformation').click(function () {
        var dataValue="";
        $('#listbox_destination_wrapper >ul li').each(function () {

            dataValue = dataValue + $(this).attr("data-value") + "|";
        });
        var model = {

            Guid: $('#Guid').val(),
            Id: 0,
            ProfessionalId: $('#ProfessionalId').val(),
            StartProfessionDay: $('#StartProfessionDay').val(),
            StartProfessionMonth: $('#StartProfessionMonth').val(),
            StartProfessionYear: $('#StartProfessionYear').val(),
            IsSpecialist: $('#IsSpecialist').is(':checked'),
            SpecialityId: $('#SpecialityId').val(),
            OtherSpeciality: $('#OtherSpeciality').val(),
            IsStudent: $('#IsStudent').is(':checked'),
            ProfessionalLevelId: $('#ProfessionalLevelId').val(),
            StartStudyDay: $('#StartStudyDay').val(),
            StartStudyMonth: $('#StartStudyMonth').val(),
            StartStudyYear: $('#StartStudyYear').val(),
            EndStudyDay: $('#EndStudyDay').val(),
            EndStudyMonth: $('#EndStudyMonth').val(),
            EndStudyYear: $('#EndStudyYear').val(),
            Company: $('#Company').val(),
            BossName: $('#BossName').val(),
            CurrentProfessionDay: $('#CurrentProfessionDay').val(),
            CurrentProfessionMonth: $('#CurrentProfessionMonth').val(),
            CurrentProfessionYear: $('#CurrentProfessionYear').val(),
            AssociatedProfessionals: dataValue,
        }

        var nextTab = 4
        AjaxFunctionUpdate('UpdateLaborInformation', model, nextTab);
    });

    $('#btnUpdateProfessional').click(function () {

        var model = {

            Guid: $('#Guid').val(),
            Id: $('#Id').val(),
            ProfessionalTypeId: $('#ProfessionalTypeId').val(),
            DocumentNumber: $('#DocumentNumber').val(),
            FirstName: $('#FirstName').val(),
            SecondName: $('#SecondName').val(),
            LastName: $('#LastName').val(),
            SecondLastName: $('#SecondLastName').val(),
            GenderId: $('#GenderId').val(),
            BirthDay: $('#BirthDay').val(),
            BirthMonth: $('#BirthMonth').val(),
            BirthYear: $('#BirthYear').val(),
            DepartamentId: $('#DepartamentId').val(),
            CityId: $('#CityId').val(),
            MsrId: $('#MsrId').val(),
            CreateBy: $('#CreateBy').val(),
            RegisterDate: $('#RegisterDate').val()
        }

        var nextTab = 2
        AjaxFunctionUpdate('UpdateProfessional', model, nextTab);

    });

    $('#btnUpdateProfessionalContact').click(function () {

        var model = {
            Guid: $('#Guid').val(),
            Id: $('#Id').val(),
            ProfessionalId: $('#ProfessionalId').val(),
            ProfessionalEmail: $('#ProfessionalEmail').val(),
            ProfessionalCellPhone: $('#ProfessionalCellPhone').val(),
            ProfessionalAddress: $('#ProfessionalAddress').val(),
            SpecialityId: $('#SpecialityId').val(),
            ProfessionalDepartamentId: $('#ProfessionalDepartamentId').val(),
            ProfessionalCityId: $('#ProfessionalCityId').val(),
            ProfessionalTypeId: $('#ProfessionalTypeId').val(),
            ProfessionalBrickId: $('#ProfessionalBrickId').val(),
        }

        var nextTab = 3
        AjaxFunctionUpdate('UpdateProfessionalContact', model, nextTab);
    });

    function AjaxFunctionUpdate(controller, model, nextTab) {
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/' + controller,
            ContentType: 'application/json; charset=utf-8',
            data: { model: model },
            error: function (data) {
                alert('Ha ocurrido un error al. Intente nuevamente o comuniquese con el administrador.');
            },
        })
            .done(function (data) {

                if (data.Result === false) {
                    $('#modalTitle').html(data.Title);
                    $('#modalMessage').html(data.Message);
                    $('#genericModal').modal('show');
                }
                else {
                    //activaTab('tab' + nextTab);                  
                    $('#modalTitle').html(data.Title);
                    $('#modalMessage').html(data.Message);
                    $('#genericModal').modal('show');
                }
            });
    }

    $("#tableSon").on("click", ".deleteSon", function (e) {
        e.preventDefault();
        $(this).parents("tr").remove();
    });

    $("#tableGrandchild").on("click", ".deleteGrandchild", function (e) {
        e.preventDefault();
        $(this).parents("tr").remove();
    });

    //_CreateProfessional
    //GetAllRepresentantsByCityId
    
    $("#waitingGIFRepr").hide();
    //$('#sourceRepr, #destinationRepr').listswap({
    //    truncate: false,
    //    height: 135,
    //    is_scroll: true,
    //    label_add: 'Agergar', //Set add label (Can bet set to empty)
    //    label_remove: 'Quitar',
    //});
    $("#MsrId").change(function () {
        $(".listbox_controls").remove();
        $(".listbox_option").remove();
        $("#listbox_sourceRepr_wrapper").remove();
        $("#listbox_destinationRepr_wrapper").remove();
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/GetAllRepresentantsByCity',
            dataType: 'json',
            data: { cityId: $("#CityId").val(), repr: $('#MsrId').val() },
            success: function (cities) {
                $("#sourceRepr").html('');
                $.each(cities, function (i, item) {
                    $("#sourceRepr").append('<option value="' + item.GUID + '">'
                        + item.NAME + "</option>");
                });
                $('#sourceRepr, #destinationRepr').listswap({
                    truncate: false,
                    height: 135,
                    is_scroll: true,
                    label_add: 'Agergar', //Set add label (Can bet set to empty)
                    label_remove: 'Quitar',
                });
                $("#CityId").removeAttr("disabled");
                $("#waitingGIF").hide();

            },
        });
    });
    
});



