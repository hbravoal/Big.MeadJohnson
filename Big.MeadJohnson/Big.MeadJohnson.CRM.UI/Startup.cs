﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Big.MeadJohnson.CRM.UI.Startup))]
namespace Big.MeadJohnson.CRM.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
