﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Big.MeadJohnson.CRM.UI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            // Se añade éste Redirect por cuestión: Al momento de Consultar Account/LogOff sin estar log sacaba
            //error (Vista no encontrada. Garantizamos que cierre sessión y/o lleva al Log In siempre.
            routes.MapRoute(
                name: "Account/Login",
                url: "Account/LogOff",
                defaults: new { controller = "Account", action = "LoginUrl" }
            );
            
            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Login", id = UrlParameter.Optional }
            );
            

        }
    }
}
