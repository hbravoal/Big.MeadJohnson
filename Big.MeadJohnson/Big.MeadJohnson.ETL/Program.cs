﻿using Big.MeadJohnson.Core.Common.Diagnostics;
using System;
using System.Configuration;
using System.IO;

namespace Big.MeadJohnson.ETL
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Inicio Proceso Carga de usuarios.");

            var message = string.Empty;
            var fileName = ConfigurationManager.AppSettings["User.File.Name"];
            var filePath = ConfigurationManager.AppSettings["User.File.Path"];
            var ProcessedFilePath = ConfigurationManager.AppSettings["User.File.Path.Processed"];

            if (!File.Exists(string.Format("{0}{1}", filePath, fileName)))
            {
                message = "No se encontro el archivo en la ruta especificada";
                Console.WriteLine(message);
                return;
            }

            var errors = ProcessUserFile.ProccessFileUser();

            if (errors != null && errors.Count > 0)
            {
                message = "El archivo se ha procesado con algunos errores. Verifique el archivo de logs.";
                Console.WriteLine(message);

                foreach (var error in errors)
                {
                    ExceptionLogging.LogInfo(error);
                    Console.WriteLine(error);
                }

                if (File.Exists(string.Format("{0}{1}", filePath, fileName)))
                {
                    File.Copy(string.Format("{0}{1}", filePath, fileName),
                        string.Format("{0}{1}{2}", ProcessedFilePath, "Precarga_Usuarios_" + DateTime.Now.ToString("yyyyMMdd"), ".xls"));
                }

            }
            else
            {
                message = "El archivo se ha procesado con exito.";
                Console.WriteLine(message);

            }

            Console.WriteLine("Fin porceso");
           // Console.ReadKey();
        }
    }
}
