﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.ETL
{
    public class Row
    {
        public string Names { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }

        public string Profile { get; set; }

        public string Departament { get; set; }

        public string City { get; set; }
    }
}
