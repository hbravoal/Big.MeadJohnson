﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.Configuration;
using System.Web.Security;

namespace Big.MeadJohnson.ETL
{
    public class ProcessUserFile
    {
        public static List<string> ProccessFileUser()
        {
            var listErrorMessage = new List<string>();
            var message = string.Empty;

            //Create COM Objects. Create a COM object for everything that is referenced
            Excel.Application xlApp = new Excel.Application();

            var fileName = ConfigurationManager.AppSettings["User.File.Name"];
            var filePath = ConfigurationManager.AppSettings["User.File.Path"];
            var ProcessedFilePath = ConfigurationManager.AppSettings["User.File.Path.Processed"];

            if (!File.Exists(string.Format("{0}{1}", filePath, fileName)))
            {
                message = "No se encontro el archivo " + fileName + " en la ruta especificada";
                listErrorMessage.Add(message);
                Console.WriteLine(message);
                return listErrorMessage;
            }

            string ext = Path.GetExtension(string.Format("{0}{1}", filePath, fileName));
            if (string.IsNullOrEmpty(ext) || !ext.Equals(".xlsx"))
            {
                message = "El formato del archivo " + fileName + " no es valido.";
                listErrorMessage.Add(message);
                Console.WriteLine(message);
                return listErrorMessage;
            }

            if (listErrorMessage.Count > 0)
                return listErrorMessage;

            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(string.Format("{0}{1}", filePath, fileName));
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;

            var totalColumns = 6;

            if (colCount > totalColumns || colCount < totalColumns)
            {
                message = "El archivo " + fileName + " no tiene el numero de columnas validos.";
                listErrorMessage.Add(message);
                return listErrorMessage;
            }

            var listCities = Business.Business.CityBLL.GetAllCities();

            var count = 0;
            var lineProccesed = 1;
            for (int i = 0; i <= rowCount; i++)
            {
                var row = new Row();

                if (count > 0 && i > 1)
                {
                    try
                    {
                        row = new Row()
                        {
                            Names = Convert.ToString(xlRange.Cells[i, 1].Value2),
                            UserName = Convert.ToString(xlRange.Cells[i, 2].Value2),
                            Email =Convert.ToString(xlRange.Cells[i, 3].Value2),
                            Profile = Convert.ToString(xlRange.Cells[i, 4].Value2),
                            Departament = string.IsNullOrEmpty(Convert.ToString(xlRange.Cells[i, 5].Value2)) ?
                                           string.Empty : Convert.ToString(xlRange.Cells[i, 5].Value2),
                            City = string.IsNullOrEmpty(Convert.ToString(xlRange.Cells[i, 6].Value2)) ?
                                           string.Empty : Convert.ToString(xlRange.Cells[i, 6].Value2),
                        };


                        if (string.IsNullOrEmpty(row.UserName))
                            throw new InvalidOperationException("Error: El nombre de usuario esta vacio. \t Linea: " + lineProccesed);


                        if (string.IsNullOrEmpty(row.Email))
                            throw new InvalidOperationException("Error: El correo del usuario esta vacio. \t Linea: " + lineProccesed);


                        var objUser = Business.Business.UserBLL.GetUserByUserName(row.UserName);
                        if (objUser == null)
                        {
                            objUser = Business.Business.UserBLL.GetUserByEmail(row.UserName);
                            if (objUser == null)
                            {
                                int profile = 0;
                                string profileName = string.Empty;

                                if (row.Profile.Equals("Súper Administrador"))
                                {
                                    profile = (int)Business.Enums.EnumProfile.sa;
                                    profileName = "Súper_Administrador";
                                }


                                if (row.Profile.Equals("Administrador"))
                                {
                                    profile = (int)Business.Enums.EnumProfile.admin;
                                    profileName = "Administrador";
                                }

                                if (row.Profile.Equals("Representante"))
                                {
                                    profile = (int)Business.Enums.EnumProfile.representantes;
                                    profileName = "Representantes";
                                }


                                Business.Models.City city = null;

                                if (!string.IsNullOrEmpty(row.City))
                                    city = listCities.Where(d => d.Name.ToLower() == row.City.ToLower()).FirstOrDefault();

                                if (city == null)
                                    listErrorMessage.Add("Error: No se encontró un ciudad. \t Linea: " + lineProccesed);

                                var user = new Business.Models.User();

                                user.Guid = Guid.NewGuid();
                                user.Name = row.Names;
                                user.UserName = row.UserName;
                                user.Password = Membership.GeneratePassword(12, 3);
                                user.IsFirstUse = true;
                                user.Email = row.Email.ToLower();
                                user.IsActive = true;
                                user.ProfileId = profile;
                                user.RegisterDate = DateTime.Now;
                                user.ManagerId = null;
                                if (city != null)
                                    user.CityId = city.Id;

                                if (Business.Business.UserBLL.SaveNewUser(user))
                                {
                                    CRM.UI.Helper.UserHelper.CreateUserASP(
                                        row.Email,
                                        row.UserName,
                                        profileName,
                                        user.Password);
                                }
                                else
                                {
                                    listErrorMessage.Add(string.Format("Ocurrio un error al guardar el usuario {0}  \t Linea: {1}", row.UserName, lineProccesed));
                                }

                                user = null;
                            }
                            else
                            {
                                listErrorMessage.Add(string.Format("{0} ya se encuentra registrado \t Linea: {1}", row.Email, lineProccesed));
                            }
                        }
                        else
                            listErrorMessage.Add(string.Format("{0} ya se encuentra registrado \t Linea {1}", row.UserName, lineProccesed));

                        row = null;
                    }
                    catch (Exception ex)
                    {
                        message = "Ha ocurrido un error al procesar la linea: " + lineProccesed;
                        listErrorMessage.Add(ex.Message+ "\n " + message);
                        continue;
                    }

                    lineProccesed++;
                }

                count++;
                Console.WriteLine(count.ToString());

            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return listErrorMessage;
        }

    }
}
