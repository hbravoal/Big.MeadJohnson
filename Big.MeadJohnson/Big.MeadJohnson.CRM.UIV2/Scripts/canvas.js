﻿$(document).ready(function () {

    var limpiar = document.getElementById("limpiar");
    var canvas = document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    var cw = canvas.width = 700,
        cx = cw / 2;
    var ch = canvas.height = 200,
        cy = ch / 2;

    var dibujar = false;
    var factorDeAlisamiento = 5;
    var Trazados = [];
    var puntos = [];
    ctx.lineJoin = "round";

    limpiar.addEventListener('click', function (evt) {
        dibujar = false;
        ctx.clearRect(0, 0, cw, ch);
        Trazados.length = 0;
        puntos.length = 0;
    }, false);


    canvas.addEventListener('mousedown', function (evt) {
        dibujar = true;
        //ctx.clearRect(0, 0, cw, ch);
        puntos.length = 0;
        ctx.beginPath();

    }, false);

    canvas.addEventListener('mouseup', function (evt) {
        redibujarTrazados();
    }, false);

    canvas.addEventListener("mouseout", function (evt) {
        redibujarTrazados();
    }, false);

    canvas.addEventListener("mousemove", function (evt) {
        if (dibujar) {
            var m = oMousePos(canvas, evt);
            puntos.push(m);
            ctx.lineTo(m.x, m.y);
            ctx.stroke();
        }
    }, false);




    //ANOTHER


    function reducirArray(n, elArray) {
        var nuevoArray = [];
        nuevoArray[0] = elArray[0];
        for (var i = 0; i < elArray.length; i++) {
            if (i % n == 0) {
                nuevoArray[nuevoArray.length] = elArray[i];
            }
        }
        nuevoArray[nuevoArray.length - 1] = elArray[elArray.length - 1];
        Trazados.push(nuevoArray);
    }
  

    function calcularPuntoDeControl(ry, a, b) {
        var pc = {}
        pc.x = (ry[a].x + ry[b].x) / 2;
        pc.y = (ry[a].y + ry[b].y) / 2;
        return pc;
    }
   

    function alisarTrazado(ry) {
        if (ry.length > 1) {
            var ultimoPunto = ry.length - 1;
            ctx.beginPath();
            ctx.moveTo(ry[0].x, ry[0].y);
            for (i = 1; i < ry.length - 2; i++) {
                var pc = calcularPuntoDeControl(ry, i, i + 1);
                ctx.quadraticCurveTo(ry[i].x, ry[i].y, pc.x, pc.y);
            }
            ctx.quadraticCurveTo(ry[ultimoPunto - 1].x, ry[ultimoPunto - 1].y, ry[ultimoPunto].x, ry[ultimoPunto].y);
            ctx.stroke();
        }
    }
    f


    function redibujarTrazados() {
        dibujar = false;
        ctx.clearRect(0, 0, cw, ch);
        reducirArray(factorDeAlisamiento, puntos);
        for (var i = 0; i < Trazados.length; i++)
            alisarTrazado(Trazados[i]);
    }


    function oMousePos(canvas, evt) {
        var ClientRect = canvas.getBoundingClientRect();
        return { //objeto
            x: Math.round(evt.clientX - ClientRect.left),
            y: Math.round(evt.clientY - ClientRect.top)
        }
    }


    ///// guardar canvas

    $("#btnSaveCanvas").click(function () {

        var image = document.getElementById("canvas").toDataURL("image/png");
        //var blank = document.getElementById("canvas").toDataURL("image/png");

        //if (image == blank) {
        //    alert('It is blank');
        //    return;
        //}

        image = image.replace('data:image/png;base64,', '');
        $("#imageData").val(image);

        var imageB64 = $("#imageData").val();
        var idButton = 'btnSaveCanvas';
        var controller = 'SaveImage';
        var nextTab = 8
        AjaxFunction(controller, imageB64,nextTab, idButton);

    });

    //function convertImgToCanvas() {
    //    var myImgElement = document.getElementById("myImg");
    //  //  var myCanvasElement = document.createElement("canvas");
    //    // don't forget to add it to the DOM!!
    //    //document.body.appendChild(canvas);
    //    //canvas.width = 700;
    //    //canvas.height = 200;
    //    var context = canvas.getContext('2d');
    //    context.drawImage(myImgElement, 0, 0, 1600, 900, 0, 0, 796, 448);
    //    // remove the image for the snippet
    //    myImgElement.style.display = 'none';
    //}

    //convertImgToCanvas();


    function AjaxFunction(controller, imageB64, idButton) {
        $.ajax({
            type: 'POST',
            url: $("#hfUrlBase").val() + 'Professionals/' + controller,
            ContentType: 'application/json; charset=utf-8',
            data: { imageB64: imageB64 },
            error: function (data) {
                alert('Ha ocurrido un error al. Intente nuevamente o comuniquese con el administrador.');
            },
        })
            .done(function (data) {

                if (data.Result === false) {
                    $('#' + idButton).prop('disabled', false);
                    $('#modalTitle').html(data.Title);                    
                    $('#genericModal').modal('show');
                    
                    
                   
                }
                else {
                    console.log(data.StatusCode);
                    $('#' + idButton).prop('disabled', true);
                    $('#modalTitle').html(data.Title);
                    //$('#modalMessage').html(data.Message);
                    
                    $("#myImg2").show();
                    if ($("#myImg").val()!=null) {
                        $("#myImg").attr("src", (data.StatusCode));
                    }
                    else {
                        $("#myImg2").attr("src", (data.StatusCode));
                    }
                    //$('#modalMessage').html(data.Message + "<img src=" + (data.StatusCode) + ">");
                    $('#modalMessage').html(data.Message);
                    $('#genericModal').modal('show');
                  
                    

                }
            });
    }


canvas.addEventListener("touchstart", function (e) {
    mousePos = getTouchPos(canvas, e);
    var touch = e.touches[0];
    var mouseEvent = new MouseEvent("mousedown", {
        clientX: touch.clientX,
        clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
}, false);
canvas.addEventListener("touchend", function (e) {
    var mouseEvent = new MouseEvent("mouseup", {});
    canvas.dispatchEvent(mouseEvent);
}, false);
canvas.addEventListener("touchmove", function (e) {
    var touch = e.touches[0];
    var mouseEvent = new MouseEvent("mousemove", {
        clientX: touch.clientX,
        clientY: touch.clientY
    });
    canvas.dispatchEvent(mouseEvent);
}, false);

// Get the position of a touch relative to the canvas
function getTouchPos(canvasDom, touchEvent) {
    var rect = canvasDom.getBoundingClientRect();
    return {
        x: touchEvent.touches[0].clientX - rect.left,
        y: touchEvent.touches[0].clientY - rect.top
    };
}
	
});