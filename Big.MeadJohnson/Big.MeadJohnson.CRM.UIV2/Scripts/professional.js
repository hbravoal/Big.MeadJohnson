﻿
$(document).ready(function () {
    /*  PROFESSIONAL/CREATE  */
    $("#DEPARTMENT_ID").change(function () {

        $("#CITY_ID").empty();
        $.ajax({
            type: 'POST',
            url: '../Generic/GetCities',
            dataType: 'json',
            data: { DEPARTMENT_ID: $("#DEPARTMENT_ID").val() },
            success: function (cities) {

                $("#CITY_ID").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(cities, function (i, CITY_ID) {
                    $("#CITY_ID").append('<option value="'
                        + CITY_ID.CITY_ID + '">'
                        + CITY_ID.NAME + '</option>');
                });
            },
        });
        return false;
    });
    $("#CITY_ID").change(function () {
        $("#MSR_ID").empty();
        $.ajax({
            type: 'POST',
            url: Url2,
            dataType: 'json',
            data: { cityId: $("#CITY_ID").val() },
            success: function (users) {
                $("#MSR_ID").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(users, function (i, user) {
                    $("#MSR_ID").append('<option value="'
                        + user.USER_ID + '">'
                        + user.NAME + '</option>');
                });
            },
        });
        return false;
    });

    /*  PROFESSIONAL/CREATE   END */
    
    /* PROFESSIONAL CONTACT /CREATE   */
    $("#DEPARMENT_NO").change(function () {

        $("#CITY_NO").empty();
        $.ajax({
            type: 'POST',
            url: '../Generic/GetCities',
            dataType: 'json',
            data: { DEPARTMENT_ID: $("#DEPARMENT_NO").val() },
            success: function (cities) {

                $("#CITY_NO").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(cities, function (i, CITY_ID) {
                    $("#CITY_NO").append('<option value="'
                        + CITY_ID.CITY_ID + '">'
                        + CITY_ID.NAME + '</option>');
                });
            },
        });
        return false;
    });
    $("#CITY_NO").change(function () {
        $("#BRICK").empty();
        $.ajax({
            type: 'POST',
            url: UrlBrickByCity,
            dataType: 'json',
            data: { cityId: $("#CITY_NO").val() },
            success: function (users) {
                $("#BRICK").append('<option value="' + '0' + '">' + '[Seleccione]' + '</option>');
                $.each(users, function (i, users) {
                    $("#BRICK").append('<option value="'
                        + users.Text + '">'
                        + users.Value + '</option>');
                });
            },
        });
        return false;
    });

    /* PROFESSIONAL CONTACT /CREATE    END */

    
    /* PROFESSIONAL LABOR INFORMATION */

    if ($('#IS_SPECIALIST').is(':checked')) {
        $('#divIsSpecialist').show();
    } else {
        $('#divIsSpecialist').hide();
    }
    $('#IS_SPECIALIST').change(function () {
        if ($(this).is(":checked")) {
            $('#divIsSpecialist').show();
        } else {
            $('#divIsSpecialist').hide();
        }
    });

    if ($('#IS_STUDENT').is(':checked')) {
        $('#divIsStudent').show();
    } else {
        $('#divIsStudent').hide();
    }


    $('#IS_STUDENT').change(function () {
        if ($(this).is(":checked")) {
            $('#divIsStudent').show();
        } else {
            $('#divIsStudent').hide();
        }
    });

    var selectedVal = $("#SPECIALTY_ID option:selected").text();
    if (selectedVal === 'Otra') {
        $('#divSpeciality').show();
    } else {
        $('#divSpeciality').hide();
    }

    $('#SPECIALTY_ID').change(function () {
        var selectedVal = $("#SPECIALTY_ID option:selected").text();
        if (selectedVal === 'Otra') {
            $('#divSpeciality').show();
        } else {
            $('#divSpeciality').hide();
        }
    });

    /*  END LABOR*/

    /* FAMILY INFORMATION */
    if ($('#GonnaDaddyQuestion').is(':checked')) {
        $('#divGonnaDaddy').show();
        $("#tableGonnaDaddy").show();
    } else {
        $('#divGonnaDaddy').hide();
        $("#tableGonnaDaddy").hide();
    }
    $('#GonnaDaddyQuestion').change(function () {
        if ($(this).is(":checked")) {
            $("#divGonnaDaddy").show();
            $("#tableGonnaDaddy").show();

        } else {
            $("#divGonnaDaddy").hide();
            $("#tableGonnaDaddy").hide();
        }
    });

  
    if ($('#IS_FATHER').is(':checked')) {
        $("#divIsFather").show();
        $("#tableSon").show();
    } else {
        $("#divIsFather").hide();
        $("#tableSon").hide();
    }
    $('#IS_FATHER').change(function () {
        if ($(this).is(":checked")) {
            $("#divIsFather").show();
            $("#tableSon").show();

        } else {
            $("#divIsFather").hide();
            $("#tableSon").hide();
        }
    });

    if ($('#IS_GRANDFATHER').is(':checked')) {
        $("#divIsGrandFather").show();
        $("#tableGrandchild").show();
    } else {
        $("#divIsGrandFather").hide();
        $("#tableGrandchild").hide();
    }
    $('#IS_GRANDFATHER').change(function () {
        if ($(this).is(":checked")) {
            $("#divIsGrandFather").show();
            $("#tableGrandchild").show();

        } else {
            $("#divIsGrandFather").hide();
            $("#tableGrandchild").hide();
        }
    });
    /* FUNCIONALITY IN FAMILI */
    $('#btnSaveSon').click(function () {

        var name = $('#NAME').val();
        var Age = $('#Son_Age').val();
        var SchoolYear = $('#SchoolYearSon').val();
        var IsYearAge = $('#Son_IsYearAge').val();
        var SchoolYearName = $("#SchoolYearSon option:selected").text();
        var Son_IsYearAgeName = $("#Son_IsYearAge option:selected").text();


        if (SchoolYearName == '[Seleccione]')
            SchoolYearName = '';
        if (Son_IsYearAgeName == '[Seleccione]') {
            $('#modalTitle').html('Administracion profesionales');
            $('#modalMessage').html('El campo Mes/Año es requerido.');
            $('#genericModal').modal('show');
            return;
        }
        if (Age !== '') {

            if (SchoolYear != '') {
                if (parseInt(SchoolYear) < 0 || isNaN(SchoolYear)) {
                    $('#modalTitle').html('Administracion profesionales');
                    $('#modalMessage').html('La escolaridad debe ser mayor a 0.');
                    $('#genericModal').modal('show');
                    return;
                }
                $('#tableSon > tbody:last-child').append('<tr><td class="name">' + name + '</td>' +
                    '<td class="age">' + Age + '</td>' +
                    '<td class="Son_IsYearAge hide">' + IsYearAge + '</td>' +
                        '<td class="Son_IsYearAgeName">' + Son_IsYearAgeName + '</td>' +
                    '<td class="SchoolYearName">' + SchoolYearName + '</td>' +
                    '<td class="SchoolYear hide">' + SchoolYear + '</td>' +
                    '<td><button class="btn btn-sm btn-warning deleteSon" type="button">Eliminar</button></td></tr >');
                $('#Son_Name').val('');
                $('#Son_Age').val('');
                $('#Son_IsYearAge').val('');
                $('#Son_SchoolYear').val('');

            } else {

                if (Age <= 0) {
                    $('#modalTitle').html('Administracion profesionales');
                    $('#modalMessage').html('la edad debe ser mayor a 0.');
                    $('#genericModal').modal('show');
                } else {
                    $('#tableSon > tbody:last-child').append('<tr><td class="name">' + name + '</td>' +
                        '<td class="age">' + Age + '</td>' +
                        '<td class="Son_IsYearAge hide">' + IsYearAge + '</td>' +
                        '<td class="Son_IsYearAgeName">' + Son_IsYearAgeName + '</td>' +
                        '<td class="SchoolYearName">' + SchoolYearName + '</td>' +
                        '<td class="SchoolYear hide">' + SchoolYear + '</td>' +
                        '<td><button class="btn btn-sm btn-warning deleteSon" type="button">Eliminar</button></td></tr >');
                    $('#Son_Name').val('');
                    $('#Son_Age').val('');
                    $('#Son_IsYearAge').val('');
                    $('#Son_SchoolYear').val('');
                }
            }
        }
        else {
            $('#modalTitle').html('Administracion profesionales');
            $('#modalMessage').html('La edad es requerida.');
            $('#genericModal').modal('show');
        }
    });

    
});

