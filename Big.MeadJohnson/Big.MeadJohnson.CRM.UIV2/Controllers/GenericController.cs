﻿using Big.MeadJohnson.CRM.UIV2.Models;
using Big.MeadJohnson.Domain.Controller;
using Big.MeadJohnson.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.MeadJohnson.CRM.UIV2.Controllers
{
    public class GenericController : Controller
    {
        // GET: Generic
        private DataContextLocal db = new DataContextLocal();

        public JsonResult GetCities(int? DEPARTMENT_ID)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var cities =  db.MJ_CITIES.Where(u => u.DEPARTMENT_ID == DEPARTMENT_ID).ToList().OrderBy(c=>c.NAME);
            
            return Json(cities,JsonRequestBehavior.AllowGet);
           
                db.Dispose();
           
        }


        public JsonResult GetAUserByCityId(int? cityId)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var users=db.MJ_USERS.Where(u => u.CITY_ID == cityId).ToList().OrderBy(c=>c.NAME);
                return Json(users, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;

            }
            finally
            {
                // cerrar la coneccion

                db.Dispose();
            }

        }

        public JsonResult GetBricksByCityId(int? cityId)
        {
            try
            {
                db.Configuration.ProxyCreationEnabled = false;
                var users = BrickController.GetBricksByCityId((int)cityId);

                return Json(users, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;

            }
            finally
            {
                // cerrar la coneccion
                
                db.Dispose();
            }

        }


        
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

    }
}