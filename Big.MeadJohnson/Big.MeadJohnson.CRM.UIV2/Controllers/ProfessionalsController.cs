﻿using Big.MeadJohnson.CRM.UIV2.Models;
using Big.MeadJohnson.Domain.Controller;
using Big.MeadJohnson.Domain.Data;
using Big.MeadJohnson.Domain.SpDataModels;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using PagedList;
using System.Configuration;
using Big.MeadJohnson.Domain.Models;
using Big.MeadJohnson.CRM.UIV2.Helper;
using Big.MeadJohnson.Domain.Enums;

namespace Big.MeadJohnson.CRM.UIV2.Controllers
{
    public class ProfessionalsController : Controller
    {
        [Authorize(Roles = "Súper_Administrador,Administrador,Representantes")]
        public async Task<ActionResult> Index(ListProfessional view, int? page = null)
        {
            Session["professionalId"] = null;
            page = (page ?? 1);
            var profesionalViewModel = new List<ListProfessional>();


            //var professionals = await ProfessionalBLL.GetAllProfessionals();
            var professionalType = ProfessionalDataController.GetAllProfessionalTypes();
            DataContextLocal db = new DataContextLocal();

            var listProfessionals = new List<MJ_PROFESSIONAL>();
            
            if(User.IsInRole("Súper_Administrador") || User.IsInRole("Administrador"))
            {
                 profesionalViewModel = ProfessionalDataController.GetListProfessionals(null);
            }


            else if (User.IsInRole("Representantes"))
            {
                
                var user= ProfessionalDataController.GetUserByUserName(User.Identity.GetUserName());
                profesionalViewModel  = ProfessionalDataController.GetListProfessionals(Convert.ToInt32(user.USER_ID));

            }
            if (!string.IsNullOrEmpty(view.FIRST_NAME))
            {
                var profesionalViewMode = profesionalViewModel.Where(m => m.FIRST_NAME.ToUpper().Contains(view.FIRST_NAME.ToUpper()));
                return View(profesionalViewMode.OrderBy(p => p.FIRST_NAME).ToPagedList((int)page, 15));
            }
            else
            {
                return View(profesionalViewModel.OrderBy(p => p.FIRST_NAME).ToPagedList((int)page, 15));
            }

        }
        public ActionResult Edit(int? id)
        {
            int ProfessionalID = Convert.ToInt32(id);
            if (id == null)
                RedirectToAction("Index");

            var professional = ProfessionalDataController.GetProfessionalById(ProfessionalID);

            if (professional == null)
                RedirectToAction("Index");

            var professionalContact = ProfessionalDataController.GetProfessionalContactById((int)professional.PROFESSIONAL_ID);
            var laboralInformation = ProfessionalDataController.GetLaboralInformationByProfessionalId((int)professional.PROFESSIONAL_ID);
            var generalInformation = new MJ_PROFESSIONAL_FAMILY_INFORMATION { PROFESSIONAL_ID = (int)professional.PROFESSIONAL_ID };
            //var secretaryInformation = new SecretaryInformation { ProfessionalId = (int)professional.PROFESSIONAL_ID };
            //var gonnaDaddy = ProfessionalDataController.GetGonnaDaddyByProfessionalId((int)professional.PROFESSIONAL_ID);
            var habeasData = ProfessionalDataController.GetHabeasDataByProfessionalId((int)professional.PROFESSIONAL_ID);

            int? roleId = null;

            if (User.Identity.IsAuthenticated)
            {
                var user = ProfessionalDataController.GetUserByUserName(User.Identity.GetUserName());
                roleId =Convert.ToInt32(user.PROFILE_ID);
            }
           

            Session["professionalId"] = (int)professional.PROFESSIONAL_ID;
            return View(professional);
        }
        public ActionResult EditProfessionalContact(MJ_PROFESSIONAL_CONTACT professionalContact)
        {
            var bricks = BrickController.GetAllBricks().OrderBy(c => c.BRICK_CODE);
            var departaments = DepartmentController.GetAllDepartaments();
            var cities = CityController.GetAllCities();

            var brick = bricks.Where(b => b.BRICK_CODE == professionalContact.BRICK).FirstOrDefault();

            var city = cities.Where(c => c.CITY_ID == professionalContact.CITY_ID).FirstOrDefault();
            if (city != null)
            {
                professionalContact.CITY.DEPARTMENT_ID = departaments.Where(d => d.DEPARTMENT_ID == city.DEPARTMENT_ID).FirstOrDefault().DEPARTMENT_ID;
                professionalContact.CITY_ID = city.CITY_ID;
                ViewBag.ProfessionalBrickId = new SelectList(bricks.Where(b => b.CITY_ID == city.CITY_ID), "Code", "Code", professionalContact.CITY.MJ_BRICKS.FirstOrDefault().ID);
            }
            else
            {
                ViewBag.ProfessionalBrickId = new SelectList(new List<MJ_BRICK>(), "Code", "Code", professionalContact.CITY.MJ_BRICKS);
            }


            ViewBag.ProfessionalDepartamentId = new SelectList(departaments, "Id", "Name", professionalContact.CITY.DEPARTMENT_ID);
            ViewBag.ProfessionalCityId = new SelectList(cities.Where(c => c.DEPARTMENT_ID == professionalContact.CITY.DEPARTMENT_ID).ToList().OrderBy(d => d.NAME), "Id", "Name", professionalContact.CITY_ID);

            return PartialView("~/Views/Professionals/_EditProfessionalContact.cshtml", professionalContact);
        }
        public ActionResult Create()
        {
           
            return View();
        }
        #region PARTIAL CLASS /BASE CREATE
        public ActionResult CreateProfessional()
        {
            var professionalType = ProfessionalDataController.GetAllProfessionalTypes();
            var departaments = DepartmentController.GetAllDepartaments();
            var cities = new List<MJ_CITIES>();
            var genders = GenderController.GetAllGender();
            var users = new List<MJ_USERS>();

            ViewBag.DepartamentId = new SelectList(departaments, "DEPARTMENT_ID", "Name");
            ViewBag.CityId = new SelectList(cities, "CITY_ID", "Name");
            ViewBag.ProfessionalTypeId = new SelectList(professionalType, "PROFESSIONAL_TYPE_ID", "Name");
            ViewBag.GenderId = new SelectList(genders, "GENDER_ID", "Name");
            ViewBag.MsrId = new SelectList(users, "USER_ID", "Name");

            var model = new MJ_PROFESSIONAL { GUID = Guid.NewGuid(), };
            return PartialView("~/Views/Professionals/_CreateProfessional.cshtml", model);
        }

        public ActionResult ProfessionalContact()
        {
            var professionalId = Session["professionalId"];
            var professionalContact = new MJ_PROFESSIONAL_CONTACT();
            var departaments = DepartmentController.GetAllDepartaments();
            var cities = new List<Models.City>();

            ViewBag.DepartamentId = new SelectList(departaments, "DEPARTMENT_ID", "Name");
            ViewBag.CityId = new SelectList(cities, "CITY_ID", "Name");
            var bricks = new List<MJ_BRICK>();
            ViewBag.BRICK = new SelectList(bricks, "BRICK_CODE", "BRICK_CODE");



            //if (professionalId != null)
            //    professionalContact = ProfessionalBLL.GetProfessionalContactById((int)professionalId);

            return PartialView("~/Views/Professionals/_ProfessionalContact.cshtml", professionalContact);
        }

        public ActionResult LaborInformation()
        {
            int? professionalId = Convert.ToInt32(Session["professionalId"]);
            var specialities = new List<MJ_SPECIALITY>();
            var professional = new MJ_PROFESSIONAL();

            if (professionalId != null && professionalId != 0)
            {
                professional = ProfessionalDataController.GetProfessionalById((int)professionalId);
                specialities = SpecialityDataController.GetSpecialitiesByProfesionalTypeId((int)professionalId);
                specialities.Add(new MJ_SPECIALITY { SPECIALTY_ID = 100, NAME = "Otra" });
            }

            var professionalLevels = ProfessionalDataController.GetAllProfessionalLevels();
            ViewBag.ProfessionalLevelId = new SelectList(professionalLevels, "PROFESSIONAL_LEVEL_ID", "Level");
            ViewBag.SpecialityId = new SelectList(specialities, "Id", "Name");
            ViewBag.ProfesionalTypeId = professional.PROFESSIONAL_TYPE_ID != null ? professional.PROFESSIONAL_TYPE_ID : 0;
            var model = new MJ_PROFESSIONAL_LABOR_INFORMATION();

            return PartialView("~/Views/Professionals/_LaborInformation.cshtml", model);
        }

        public ActionResult GeneralInformation()
        {
            var professionalId = Session["professionalId"];
            var GeneralInformation = new MJ_PROFESSIONAL_FAMILY_INFORMATION();

            var scholarships = ScholarshipDataController.GetAllScholarship();
            ViewBag.SchoolYearSon = new SelectList(scholarships, "Id", "Name");
            ViewBag.SchoolYearGrandChild = new SelectList(scholarships, "Id", "Name");
            var months = DataHelper.MotnGenerate();
            ViewBag.Months = new SelectList(months, "Value", "Text");

            var days = DataHelper.NumbersGenerate(31);
            ViewBag.Days = new SelectList(days, "Value", "Text");
            var type = DataHelper.TypeAgeGenerate();
            ViewBag.TypeAge = new SelectList(type, "Value", "Text");
            return PartialView("~/Views/Professionals/_GeneralInformation.cshtml", GeneralInformation);
        }
        public ActionResult SecretaryInformation()
        {
            var professionalId = Session["professionalId"];
            var SecretaryInformation = new MJ_PROFESSIONAL();
            return PartialView("~/Views/Professionals/_SecretaryInformation.cshtml", SecretaryInformation);
        }
        public ActionResult DigitalSignature()
        {
            return PartialView("~/Views/Professionals/_DigitalSignature.cshtml");
        }
        public ActionResult DigitalSignatureSecretary()
        {
            return PartialView("~/Views/Professionals/_DigitalSignatureSecretary.cshtml");
        } 
        #endregion
        #region SAVE
        [HttpPost]
        public ActionResult CreateProfessional(MJ_PROFESSIONAL model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    using (DataContextLocal db = new DataContextLocal())
                    {

                        if (model.MSR_ID != null && model.MSR_ID != 0)
                            model.MSR_MAIL = UserController.GetUserById((int)model.MSR_ID).EMAIL;
                        else
                            ModelState.AddModelError(string.Empty, "El campo representante es requerido.");
                        model.CREATED_BY = Convert.ToInt32(UserController.GetUserByUserName(User.Identity.GetUserName()).USER_ID);
                        try
                        {
                            model.BIRTH_DATE = (DateTime)ValidateDate(model.BirthDay,
                                        model.BirthMonth, model.BirthYear);
                        }
                        catch (Exception ex)
                        {
                            ModelState.AddModelError(string.Empty, "La fecha ingresada no es valida.");
                        }
                        model.TOKEN = Guid.NewGuid().ToString();
                        var expirationDate = ConfigurationManager.AppSettings["Token.Expiration.Date"];
                        model.REGISTER_DATE = DateTime.Now;
                        model.EXPIRATION_DATE_TOKEN = Convert.ToDateTime(model.REGISTER_DATE).AddDays(Convert.ToDouble(expirationDate));

                        var professionalIdSaved = ProfessionalDataController.SaveNewProfessional(model);
                        Session["professionalId"] = professionalIdSaved;




                        return Json(new Response { Message = "Professional Guardado Corractamente", Position = "#btnSaveProfessional" });

                    };
                }
                else
                {
                    string error = "• " + string.Join("</br> • ", ModelState.Values
                                        .SelectMany(x => x.Errors)
                                        .Select(x => x.ErrorMessage));

                    return Json(new Response { Message = error, Position = "#None" });
                }

            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpPost]
        public ActionResult SaveProfessionalContact(MJ_PROFESSIONAL_CONTACT model)
        {

            var response = new Response();

            if (Session["professionalId"] != null)
            {
                var professionalId = Session["professionalId"];
                model.PROFESSIONAL_ID = (int)professionalId;
                var professionalContact = new MJ_PROFESSIONAL_CONTACT();
                var departaments = DepartmentController.GetAllDepartaments();
                var cities = new List<City>();
                ViewBag.DepartamentId = new SelectList(departaments, "DEPARTMENT_ID", "Name");
                ViewBag.CityId = new SelectList(cities, "CITY_ID", "Name");
                var bricks = new List<MJ_BRICK>();

                if (model.CITY_NO != null && model.CITY_NO != 0)
                    bricks = BrickController.GetAllBricks().Where(b => b.CITY_ID == model.CITY_NO).ToList();

                ViewBag.BRICK = new SelectList(bricks, "BRICK_CODE", "BRICK_CODE", model.BRICK);

                if (professionalId != null)
                {
                    //professionalContact = ProfessionalBLL.GetProfessionalContactById((int)professionalId);
                    //model.Guid = professionalContact.Guid;
                    //model.Id = professionalContact.Id;
                }
            }
            else
            {
                return Json(new Response { Message = "Debe registrar un profesional.", Position = "#None" });

            }

            if (ModelState.IsValid)
            {
                model.CITY_ID = model.CITY_NO;
                var result = ProfessionalDataController.SaveProfessionalContact(model);
                if (result != null)
                {

                    if (ProfessionalDataController.UpdateOutProfessionalByID(model.PROFESSIONAL_ID))
                    {
                        return Json(new Response { Message = "El registro ha sido creado con éxito.", Position = "#btnSaveProfessional" });
                    }
                    else
                    {
                        return Json(new Response { Message = "Ha ocurrido un error al guardar la información de contacto.Intente nuevamnete.", Position = "#None" });


                    }
                }
                else
                {
                    return Json(new Response { Message = "Ha ocurrido un error al guardar la información de contacto.Intente nuevamnete.", Position = "#None" });

                }
            }
            else
            {

                string error = "• " + string.Join("</br> • ", ModelState.Values
                                     .SelectMany(x => x.Errors)
                                     .Select(x => x.ErrorMessage));


                return Json(new Response { Message = error, Position = "#None" });
            }


        }
        [HttpPost]
        public ActionResult SaveLaborInformation(MJ_PROFESSIONAL_LABOR_INFORMATION model)
        {
            var response = new Response();
            int? professionalId = null;

            if (Session["professionalId"] != null)
            {
                professionalId = Convert.ToInt32(Session["professionalId"]);
                model.PROFESSIONAL_ID = (int)professionalId;
            }
            else
            {
                return Json(new Response { Message = "Debe registrar un profesional.", Position = "#None" });
            }

            var professional = ProfessionalDataController.GetProfessionalById((int)professionalId);
            DateTime? startProfessionDate = null;

            if (professional.PROFESSIONAL_TYPE_ID != (int)EnumProfession.Secretaria)
            {

                try
                {
                    startProfessionDate = ValidateDate(model.START_DAY_PROFESSION,
                     model.START_MONTH_PROFESSION, model.START_YEAR_PROFESSION);
                }
                catch (Exception ex)
                {
                    ModelState.AddModelError(string.Empty, "La fecha ingresada no es valida.");
                    
                }

                if (startProfessionDate == null)
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó a ejercer su profesión.");
            }
            else
            {
                if (string.IsNullOrEmpty(model.BOSS_NAME))
                    ModelState.AddModelError(string.Empty, "Debe ingresar el nombre del profesional para el trabaja.");
            }

            model.START_DATE_PROFESSION = startProfessionDate;

            if (model.IS_SPECIALIST && (model.SPECIALTY_ID == 0 || model.SPECIALTY_ID == null))
                ModelState.AddModelError(string.Empty, "Debe seleccionar una especialidad.");

            if (model.SPECIALTY_ID == 100 && string.IsNullOrEmpty(model.OTHER_SPECIALIST))
                ModelState.AddModelError(string.Empty, "Debe seleccionar una especialidad de la lista o ingresar la especialidad en el campo otra.");

            if (!model.IS_SPECIALIST && (model.SPECIALTY_ID == 0 || model.SPECIALTY_ID == null))
                model.SPECIALTY_ID = null;

            if (model.IS_STUDENT && (model.PROFESSIONAL_LEVEL_ID == 0 || model.PROFESSIONAL_LEVEL_ID == null))
                ModelState.AddModelError(string.Empty, "Debe seleccionar el nivel profesional.");

            DateTime? startStudyDate = null;
            DateTime? endStudyDate = null;
            if (model.IS_STUDENT)
            {
                try
                {
                    startStudyDate = ValidateDate(model.START_DAY_STUDY,
                                model.START_MONTH_STUDY, model.START_YEAR_STUDY);

                    if (startStudyDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó los estudios.");
                    else
                    {
                        if (DateTime.Compare((DateTime)startStudyDate, DateTime.Now) > 0)
                            ModelState.AddModelError(string.Empty, "La fecha en la que comenzó los estudios debe ser menor a la fecha actual.");
                    }
                }
                catch (Exception)
                {

                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual comenzó los estudios.");
                }

                try
                {
                    endStudyDate = ValidateDate(model.END_DAY_STUDY,
                               model.END_MONTH_STUDY, model.END_YEAR_STUDY);

                    if (endStudyDate == null)
                        ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual finalizará los estudios.");
                    else
                    {
                        if (DateTime.Compare(DateTime.Now, (DateTime)endStudyDate) > 0)
                            ModelState.AddModelError(string.Empty, "La fecha estimada de finalización de estudios debe ser mayor a la fecha actual.");
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError(string.Empty, "Debe ingresar una fecha valida en la cual finalizará los estudios.");
                }
            }

            model.STUDY_START_DATE = startStudyDate;
            model.STUDY_END_DATE = endStudyDate;

            if (ModelState.IsValid)
            {
                int? result = null;
                if (model.SPECIALTY_ID == 100 && !string.IsNullOrEmpty(model.OTHER_SPECIALIST))
                {
                    var saveSpeciality = SpecialityDataController.SaveSpeciality(new MJ_SPECIALITY
                    {
                        GUID = Guid.NewGuid(),
                        SPECIALTY_ID = 0,
                        IS_ACTIVE = true,
                        NAME = model.OTHER_SPECIALIST.First().ToString().ToUpper() + model.OTHER_SPECIALIST.Substring(1).ToLower(),
                        PROFESSIONAL_TYPE_ID = professional.PROFESSIONAL_TYPE_ID,
                    });

                    if (saveSpeciality != null)
                    {
                        model.SPECIALTY_ID = saveSpeciality;
                        result = ProfessionalDataController.SaveLabolarInformation(model);
                    }
                    else
                        return Json(new Response { Message = "Ha ocurrido un error al guardar la información de contacto.Intente nuevamnete.", Position = "#None" });
                }
                else
                    result = ProfessionalDataController.SaveLabolarInformation(model);

                if (result != null)
                {

                    ProfessionalDataController.UpdateOutProfessionalByID(model.PROFESSIONAL_ID);
                    return Json(new Response { Message = "El registro ha sido creado con éxito.", Position = "#btnSaveLaboralInformation" });
                    
                }
                else
                    
                return Json(new Response { Message = "Ha ocurrido un error al guardar la información de contacto.Intente nuevamnete.", Position = "#None" });
            }
            else
            {
               
                return Json(new Response { Message = Helper.DataHelper.getModelError(ModelState.Values), Position = "#None" });
            }

          
        }
        [HttpPost]
        public ActionResult SaveGeneralInformation(MJ_PROFESSIONAL_FAMILY_INFORMATION model)
        {
            
            if (ModelState.IsValid)
            {

                return Json("");
               
            }

          
            else
            {

                return Json(new Response { Message = Helper.DataHelper.getModelError(ModelState.Values), Position = "#None" });
            }


        }
        [HttpPost]
        public ActionResult SaveSecretaryInformation(MJ_PROFESSIONAL model)
        {

            if (ModelState.IsValid)
            {

                return Json("");

            }


            else
            {

                return Json(new Response { Message = Helper.DataHelper.getModelError(ModelState.Values), Position = "#None" });
            }


        }



        #endregion
        #region Guardar firma

        
        [HttpPost]
        public ActionResult SaveDigitalSignature(string imageB64)
        {
            return Json("Hol");
            //var response = new Response<bool>();
            //int? professionalId = null;

            //if (Session["professionalId"] != null)
            //{
            //    professionalId = Convert.ToInt32(Session["professionalId"]);

            //}
            //else
            //{
            //    response = new Response<bool>()
            //    {
            //        Result = false,
            //        Model = false,
            //        Message = "Debe registrar un profesional.",
            //        Title = "Administracion Profesionales"
            //    };

            //    return Json(response);
            //}


            //if (string.IsNullOrEmpty(imageB64))
            //{
            //    response = new Response<bool>()
            //    {
            //        Result = false,
            //        Model = false,
            //        Message = "No es posible procesar la firma. Intente nuevamente",
            //        Title = "Administracion Profesionales"
            //    };

            //    return Json(response);
            //}

            //try
            //{
            //    var secretary = ProfessionalBLL.GetSecretaryByProfessionalId((int)professionalId);
            //    var habeasData = ProfessionalBLL.GetHabeasDataByProfessionalId(secretary.SecretaryId);
            //    var image = new Helper.ImageHelper();
            //    var objImage = image.Base64ToImage(imageB64);
            //    var newGuid = Guid.NewGuid();
            //    var urlImage = "Content/CanvasImages/" + newGuid + ".jpg";



            //    objImage.Save(Server.MapPath("~/Content/CanvasImages/" + newGuid + ".jpg"));



            //    if (habeasData == null)
            //    {

            //        habeasData = new HabeasData
            //        {

            //            Guid = Guid.NewGuid(),
            //            CheckTerms = true,
            //            StartTermsDate = DateTime.Now,
            //            ProfessionalId = (int)secretary.SecretaryId,
            //            UrlImage = urlImage,
            //            LastUpdateTermsDate = DateTime.Now
            //        };
            //    }
            //    else
            //    {
            //        if (System.IO.File.Exists(Server.MapPath(habeasData.UrlImage)))
            //            System.IO.File.Delete(Server.MapPath(habeasData.UrlImage));
            //        habeasData.LastUpdateTermsDate = DateTime.Now;
            //        habeasData.CheckTerms = true;
            //        habeasData.UrlImage = urlImage;

            //    }

            //    if (ProfessionalBLL.SaveProfessionalHabeasData(habeasData))
            //    {
            //        response = new Response<bool>()
            //        {
            //            Result = true,
            //            Model = false,
            //            Message = "La firma ha sido guardada con éxito.",
            //            Title = "Administracion Profesionales",
            //            StatusCode = System.Configuration.ConfigurationManager.AppSettings["urlSite"] + urlImage
            //        };

            //        UpdateLastUpdateByProfessional(habeasData.ProfessionalId);
            //    }
            //    else
            //    {
            //        response = new Response<bool>()
            //        {
            //            Result = false,
            //            Model = false,
            //            Message = "Ha ocurrido un error al guardar la firma. Intente Nuevamente",
            //            Title = "Administracion Profesionales",

            //        };
            //    }

            //}
            //catch (Exception ex)
            //{
            //    response = new Response<bool>()
            //    {
            //        Result = false,
            //        Model = false,
            //        Message = "No es posible procesar la firma. Intente nuevamente",
            //        Title = "Administracion Profesionales"
            //    };
            //}

            //return Json(response);
        }

        #endregion

        private DateTime? ValidateDate(int? day, int? month, int? year)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(day))) return null;
                if (string.IsNullOrEmpty(Convert.ToString(month))) return null;

                var date = string.Format("{0}/{1}/{2}",
                    year != null ? year : 1900,
                    month,
                    day);
                if (date.Length > 10)
                    return null;

                if (date.Contains('e'))
                    return null;

                return DateTime.Parse(date);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
