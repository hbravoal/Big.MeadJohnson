﻿

namespace Big.MeadJohnson.CRM.UIV2.Helper
{
    using Big.MeadJohnson.CRM.UIV2.Models;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.AspNet.Identity.Owin;
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class UserHelper : IDisposable
    {
        private static Models.ApplicationDbContext userContext = new Models.ApplicationDbContext();

        public static IList<string> GetRolesByUser( string userName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<Models.ApplicationUser>(userContext));
            var userASP = userManager.FindByName(userName);
            var roles = userManager.GetRoles(userASP.Id);
            return roles;
        }

        public static bool DeleteUser(string userName, string roleName)
        {
            var userManager = new UserManager<Models.ApplicationUser>(new UserStore<Models.ApplicationUser>(userContext));
            var userASP = userManager.FindByName(userName);
            if (userASP == null)
            {
                return false;
            }

            var response = userManager.RemoveFromRole(userASP.Id, roleName);
            if (response.Succeeded)
            {
                var deleteAspuser = userManager.Delete(userASP);
                return deleteAspuser.Succeeded;
            }

            return false;
        }

        public static bool UpdateUserRole(string userName, string roleName, string oldRoleName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.FindByName(userName);
            if (userASP != null)
            {
                var response = userManager.RemoveFromRole(userASP.Id, oldRoleName);
                if (response.Succeeded)
                {
                    CheckRole(roleName);
                    response = userManager.AddToRole(userASP.Id, roleName.Replace(' ', '_'));
                    return response.Succeeded;
                }
            }

            return false;
        }

        public static bool SaveUserRole(string userName, string roleName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.FindByName(userName);
            if (userASP != null)
            {
                CheckRole(roleName);
                var response = userManager.AddToRole(userASP.Id, roleName.Replace(' ', '_'));
                return response.Succeeded;
            }

            return false;
        }

        public static bool RemoveUserRole(string userName, string roleName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.FindByName(userName);
            if (userASP != null)
            {
                var response = userManager.RemoveFromRole(userASP.Id, roleName.Replace(' ', '_'));
                return response.Succeeded;
            }

            return false;
        }

        public static void CheckRole(string roleName)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(userContext));
            if (!roleManager.RoleExists(roleName))
            {
                roleManager.Create(new IdentityRole(roleName));
            }
        }

        public static bool CheckUser(string email, string userName, string role, string password)
        {
            try
            {
                var userManager = new UserManager<Models.ApplicationUser>(new UserStore<ApplicationUser>(userContext));
                var userASP = userManager.FindByName(userName);

                if (userASP == null)
                {
                    CreateUserASP(email, userName, role, password);
                    return true;
                }

                userManager.AddToRole(userASP.Id, role);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void CreateUserASP(string email, string userName, string roleName, string password)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = new ApplicationUser
            {
                Email = email,
                UserName = userName,
            };

            var result = userManager.Create(userASP, password);
            if (result.Succeeded)
            {
                CheckRole(roleName.Replace(' ', '_'));
                userManager.AddToRole(userASP.Id, roleName.Replace(' ', '_'));
            }
        }

        public static bool UpdateUserMail(string email, string userName)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.FindByName(userName);

            if (userASP != null)
            {
                userASP.Email = email;
                var result = userManager.Update(userASP);
                return result.Succeeded;
            }

            return false;
        }

        public static bool ChangePassword(string userName, string oldPassword, string newPassword)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(userContext));
            var userASP = userManager.FindByName(userName);

            if(userASP != null)
            {
                var userId = userASP.Id;
                var result = userManager.ChangePassword(userId, oldPassword, newPassword);
                return result.Succeeded;
            }

            return false;

        }

        public void Dispose()
        {
            userContext.Dispose();
        }
    }
}