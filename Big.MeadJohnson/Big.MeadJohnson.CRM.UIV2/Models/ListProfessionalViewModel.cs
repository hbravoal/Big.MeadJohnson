﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Big.MeadJohnson.CRM.UIV2.Models
{
    public class ListProfessionalViewModel { 
    public int Id { get; set; }

    public string FirstName { get; set; }

    public string SecondName { get; set; }

    public string LastName { get; set; }

    public string SecondLastName { get; set; }

    public int? ProfessionalTypeId { get; set; }

    [Display(Name = "Tipo de profesional")]
    public string ProfessionalTypeName { get; set; }

    [Display(Name = "Última actualización")]
    [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
    public string LastUpdate { get; set; }


    [Display(Name = "% de completitud")]
    public int PercentCompleteness { get; set; }

    [Display(Name = "Nombre completo")]
    public string FullName
    {
        get
        {
            return string.Format("{0} {1} {2} {3}", FirstName,
                string.IsNullOrEmpty(SecondName) ? "" : SecondName,
                LastName,
                string.IsNullOrEmpty(SecondLastName) ? "" : SecondLastName);
        }
    }
}
}