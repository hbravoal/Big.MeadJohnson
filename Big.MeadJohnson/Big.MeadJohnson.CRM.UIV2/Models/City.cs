﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Big.MeadJohnson.CRM.UIV2.Models
{
    public class City
    {
        public Guid Guid { get; set; }

        public int Id { get; set; }

        public string Name { get; set; }

        public bool? IsActive { get; set; }

        public int DepartamentId { get; set; }
    }
}