﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Big.MeadJohnson.CRM.UIV2.Models
{
    public class Response
    {
        public string Status { get; set; }
        public string Message { get; set; }
        public string Position { get; set; }

        public string ErrorMessage { get; set; }
    }
}