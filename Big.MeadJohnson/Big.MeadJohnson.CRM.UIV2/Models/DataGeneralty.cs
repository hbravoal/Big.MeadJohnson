﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Big.MeadJohnson.CRM.UIV2.Models
{
    public class DataGeneralty
    {
        public int Value { get; set; }

        public string Text { get; set; }
    }
}