﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Big.MeadJohnson.CRM.UIV2.Startup))]
namespace Big.MeadJohnson.CRM.UIV2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
