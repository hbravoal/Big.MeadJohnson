﻿using Big.MeadJohnson.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Domain.Basic
{
    public class UserBasic
    {
        private static DataContext db = new DataContext();
        public static MJ_USERS GetUserByUserName(string userName)
        {
            try
            {
                return db.MJ_USERS.Where(u => u.USER_NAME == userName).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
