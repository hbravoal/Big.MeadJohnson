//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Big.MeadJohnson.Domain.Data
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class MJ_USERS
    {
        public string GUID { get; set; }
        [Key]
        public int USER_ID { get; set; }
        public string NAME { get; set; }
        public string USER_NAME { get; set; }
        public string PASSWORD { get; set; }
        
        
        public bool IS_ACTIVE { get; set; }
        public string EMAIL { get; set; }
        public System.DateTime REGISTER_DATE { get; set; }
        public Nullable<bool> IS_FIRST_USE { get; set; }
        

        [JsonIgnore]
        public int? CITY_ID { get; set; }
        public virtual MJ_CITIES CITY { get; set; }

        [JsonIgnore]
        public int PROFILE_ID { get; set; }
        public virtual MJ_PROFILE PROFILE { get; set; }

        [JsonIgnore]
        public int? MANAGER_ID { get; set; }
        public virtual MJ_MANAGER MANAGER{ get; set; }


     

        [JsonIgnore]        
        public virtual  ICollection<MJ_BRICK> MJ_BRICKS { get; set; }
    }
}
