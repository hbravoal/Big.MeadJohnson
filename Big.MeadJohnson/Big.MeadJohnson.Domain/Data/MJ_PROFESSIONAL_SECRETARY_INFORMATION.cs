//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Big.MeadJohnson.Domain.Data
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class MJ_PROFESSIONAL_SECRETARY_INFORMATION
    {
        [Display(Name = "Guid")]
        public Guid SecretaryGuid { get; set; }

        [Key]
        [Display(Name = "Id")]
        public int SecretaryId { get; set; }
        

        [Display(Name = "* Tiene secretaria ?")]
        [Required(ErrorMessage = "El campo {0} es requerido.")]
        public bool SecretaryHaveSecretary { get; set; }

        [Display(Name = "* Nombres")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string SecretaryFirstName { get; set; }

        [Display(Name = "* Apellidos")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string SecretaryLastName { get; set; }

        [Display(Name = "C�dula")]
        [StringLength(11, MinimumLength = 8, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string SecretaryDocumentNumber { get; set; }

        [StringLength(50, MinimumLength = 10, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        [DataType(DataType.EmailAddress)]
        [EmailAddress(ErrorMessage = "Debe ingresar un E-mail valido.")]
        [Display(Name = "* Correo")]
        public string SecretaryEmail { get; set; }

        [Display(Name = "* Celular")]
        [DataType(DataType.PhoneNumber)]
        [MinLength(10, ErrorMessage = "El numero celular no es valido.")]
        [MaxLength(10, ErrorMessage = "El numero celular no es valido")]
        public string SecretaryCellPhone { get; set; }

        [Display(Name = "Direcci�n")]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string SecretaryAddress { get; set; }

        [Display(Name = "Entidad en la que trabaja")]
        [StringLength(100, MinimumLength = 8, ErrorMessage = "El campo {0} debe contener entre {2} y {1} caracteres")]
        public string SecretaryCompany { get; set; }

        [Display(Name = "Tiene hijos ?")]
        public bool SecretaryHaveSon { get; set; }


        [Display(Name = "D�a")]
        [Range(1, 31, ErrorMessage = "Debe ingresar un valor entre {1} y {2}")]
        public int? SecretaryBirthDay { get; set; }

        [Display(Name = "Mes")]
        [Range(1, 12, ErrorMessage = "Debe ingresar un valor entre {1} y {2}")]
        public int? SecretaryBirthMonth { get; set; }

        [Display(Name = "A�o")]
        [Range(1900, 9999, ErrorMessage = "El campo {0} debe ser un valor entre {1} y {2}")]
        public int? SecretaryBirthYear { get; set; }

        public DateTime? SecretaryBirthDate { get; set; }



        
    }
}
