//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Big.MeadJohnson.Domain.Data
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    public partial class MJ_GONNA_DADDY
    {
        [Key]
        public int ID { get; set; }
        
        

        [Display(Name = "�Usted ser� padre/madre?")]
        public bool GonnaDaddyQuestion { get; set; }

        [Display(Name = "D�a estimado Nacimiento")]
        public int? DayStimated { get; set; }
        [Display(Name = "* Mes estimado Nacimiento")]
        public int MonthStimated { get; set; }

        
        
        [JsonIgnore]
        public int FAMILY_INFORMATION_ID { get; set; }
        public virtual MJ_PROFESSIONAL_FAMILY_INFORMATION FAMILY_INFORMATION { get; set; }
        


    }
}
