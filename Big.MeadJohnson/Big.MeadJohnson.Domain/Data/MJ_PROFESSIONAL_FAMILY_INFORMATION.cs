//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Big.MeadJohnson.Domain.Data
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class MJ_PROFESSIONAL_FAMILY_INFORMATION
    {
        public Guid GUID { get; set; }

        [Key]
        public int ID { get; set; }


        [Display(Name = "Nombres")]
        public string NAME { get; set; }
        [Display(Name = " * Edad")]
        public int AGE { get; set; }
        [Display(Name = " * Mes/A�o")]
        public int IS_YEAR_AGE { get; set; }
        [Display(Name = " Escolaridad")]
        public int? SCHOOL_YEAR{ get; set; }
        [Display(Name = "* Tiene hijos?")]
        public bool IS_FATHER { get; set; }
        [Display(Name = "* Tiene Nietos?")]
        public bool IS_GRANDFATHER { get; set; }


        public bool? IS_SON { get; set; }
        

        public bool? IS_GRANDCHILD { get; set; }

      

        [JsonIgnore]
        public int PROFESSIONAL_ID { get; set; }
        public virtual MJ_PROFESSIONAL PROFESSIONAL { get; set; }

        [JsonIgnore]
        public virtual ICollection<MJ_GONNA_DADDY> MJ_GONNA_DADDIES { get; set; }
    }
}
