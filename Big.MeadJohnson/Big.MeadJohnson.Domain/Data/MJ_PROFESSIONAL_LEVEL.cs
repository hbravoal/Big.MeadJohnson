//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Big.MeadJohnson.Domain.Data
{
    using Newtonsoft.Json;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    public partial class MJ_PROFESSIONAL_LEVEL
    {
        
        public string GUID { get; set; }

        [Key]
        public int PROFESSIONAL_LEVEL_ID { get; set; }
        public string LEVEL { get; set; }
        public Nullable<bool> ISACTIVE { get; set; }

        [JsonIgnore]
        public virtual ICollection<MJ_PROFESSIONAL_LABOR_INFORMATION> MJ_PROFESSIONAL_LABOR_INFORMATIONS { get; set; }

        
    }
}
