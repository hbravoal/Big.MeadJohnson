﻿using Big.MeadJohnson.Domain.Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Domain
{
    public class DataContext:DbContext
    {
        public DataContext() : base("DefaultConnection")
        {
        }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            //#region CHANGE DECIMAL PROPERTIES
            //modelBuilder.Entity<MJ_BRICK>()
            //              .Property(p => p.ID)
            //              .HasPrecision(18, 0); // or whatever your schema specifies

            //modelBuilder.Entity<MJ_GENDER>()
            //            .Property(p => p.GENDER_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_GONNA_DADDY>()
            //            .Property(p => p.ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_GONNA_DADDY>()
            //            .Property(p => p.ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_MANAGER>()
            //            .Property(p => p.MANAGER_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_PROFESSIONAL>()
            //            .Property(p => p.PROFESSIONAL_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies

            //modelBuilder.Entity<MJ_PROFESSIONAL_CONTACT>()
            //            .Property(p => p.PROFESSIONAL_CONTACT_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_PROFESSIONAL_FAMILY_INFORMATION>()
            //            .Property(p => p.ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies

            //modelBuilder.Entity<MJ_PROFESSIONAL_HABEAS_DATA>()
            //            .Property(p => p.ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies

            //modelBuilder.Entity<MJ_PROFESSIONAL_LABOR_INFORMATION>()
            //            .Property(p => p.ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_PROFESSIONAL_LEVEL>()
            //            .Property(p => p.PROFESSIONAL_LEVEL_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_PROFESSIONAL_TYPE>()
            //            .Property(p => p.PROFESSIONAL_TYPE_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_PROFILE>()
            //            .Property(p => p.PROFILE_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies

            //modelBuilder.Entity<MJ_DEPARTMENTS>()
            //            .Property(p => p.DEPARTMENT_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies

            //modelBuilder.Entity<MJ_REGION>()
            //                .Property(p => p.REGION_ID)
            //                .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_DEPARTMENTS>()
            //                .Property(p => p.DEPARTMENT_ID)
            //                .HasPrecision(18, 0); // or whatever your schema specifies
            //modelBuilder.Entity<MJ_CITIES>()

            //                .Property(p => p.CITY_ID)
            //                .HasPrecision(18, 0); // or whatever your schema specifies 
            //modelBuilder.Entity<MJ_USERS>()
            //            .Property(p => p.USER_ID)
            //            .HasPrecision(18, 0); // or whatever your schema specifies
            //#endregion

            #region CHANGE: PRIMARY_COLUMNS
            modelBuilder.Entity<MJ_BRICK>()
                                .Property(p => p.ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_CITIES>()
                    .Property(p => p.CITY_ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_DEPARTMENTS>()
                    .Property(p => p.DEPARTMENT_ID).HasColumnName("ID");
            modelBuilder.Entity<MJ_GENDER>()
                    .Property(p => p.GENDER_ID).HasColumnName("ID");

            
                modelBuilder.Entity<MJ_MANAGER>()
                    .Property(p => p.MANAGER_ID).HasColumnName("ID");
            modelBuilder.Entity<MJ_PROFESSIONAL>()
                    .Property(p => p.PROFESSIONAL_ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_PROFESSIONAL_CONTACT>()
                    .Property(p => p.PROFESSIONAL_CONTACT_ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_PROFESSIONAL_LEVEL>()
                    .Property(p => p.PROFESSIONAL_LEVEL_ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_PROFESSIONAL_TYPE>()
                    .Property(p => p.PROFESSIONAL_TYPE_ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_PROFILE>()
                    .Property(p => p.PROFILE_ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_REGION>()
                    .Property(p => p.REGION_ID).HasColumnName("ID");
            
            modelBuilder.Entity<MJ_SPECIALITY>()
                    .Property(p => p.SPECIALTY_ID).HasColumnName("ID");

            modelBuilder.Entity<MJ_USERS>()
        .Property(p => p.USER_ID).HasColumnName("ID");





            #endregion


        //    modelBuilder.Entity<MJ_PROFESSIONAL>()
        //.HasOptional(p => p.Secretary)
        //.WithOptionalDependent()
        //.Map(d => d.MapKey("SECRETARY_ID"));


        }
        public virtual DbSet<MJ_CITIES> MJ_CITIES { get; set; }
        public virtual DbSet<MJ_DEPARTMENTS> MJ_DEPARTMENTS { get; set; }
        public virtual DbSet<MJ_PROFILE> MJ_PROFILE { get; set; }
        public virtual DbSet<MJ_USERS> MJ_USERS { get; set; }
        public virtual DbSet<MJ_GENDER> MJ_GENDER { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL> MJ_PROFESSIONAL { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_CONTACT> MJ_PROFESSIONAL_CONTACT { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_FAMILY_INFORMATION> MJ_PROFESSIONAL_FAMILY_INFORMATION { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_LABOR_INFORMATION> MJ_PROFESSIONAL_LABOR_INFORMATION { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_LEVEL> MJ_PROFESSIONAL_LEVEL { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_SECRETARY_INFORMATION> MJ_PROFESSIONAL_SECRETARY_INFORMATION { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_TYPE> MJ_PROFESSIONAL_TYPE { get; set; }
        public virtual DbSet<MJ_SPECIALITY> MJ_SPECIALITY { get; set; }
        public virtual DbSet<MJ_BRICK> MJ_BRICK { get; set; }
        public virtual DbSet<MJ_MANAGER> MJ_MANEGER { get; set; }
        public virtual DbSet<MJ_REGION> MJ_REGION { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_HABEAS_DATA> MJ_PROFESSIONAL_HABEAS_DATA { get; set; }
        public virtual DbSet<MJ_SCHOLARSHIP> MJ_SCHOLARSHIP { get; set; }
        public virtual DbSet<MJ_GONNA_DADDY> MJ_GONNA_DADDY { get; set; }
      

    }
}
