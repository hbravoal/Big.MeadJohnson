﻿using Big.MeadJohnson.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Domain.Controller
{
    public class ProfileController
    {
        private static DataContext db = new DataContext();
        public static ICollection<MJ_PROFILE> GetAllProfiles()
        {
            try
            {
                return db.MJ_PROFILE.ToList();

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
