﻿using Big.MeadJohnson.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Big.MeadJohnson.Domain.Controller
{
    public class SpecialityDataController : IDisposable
    {

        static  DataContext db = new DataContext();
        public static List<MJ_SPECIALITY> GetAllSpecialities()
        {
           
                try
                {
                    return db.MJ_SPECIALITY.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
         
        }
        public static List<MJ_SPECIALITY> GetSpecialitiesByProfesionalTypeId(int professional_id)
        {

            try
            {
                return db.MJ_SPECIALITY.Where(c=>c.PROFESSIONAL_TYPE_ID==professional_id).ToList();
            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static int? SaveSpeciality(MJ_SPECIALITY speciality)
        {
            
                try
                {
                    db.MJ_SPECIALITY.Add(speciality);
                    db.SaveChanges();
                    return (int?)speciality.SPECIALTY_ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
            
        }
        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~City() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
