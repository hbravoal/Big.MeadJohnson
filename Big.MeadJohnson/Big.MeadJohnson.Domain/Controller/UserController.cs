﻿using Big.MeadJohnson.Domain.Basic;
using Big.MeadJohnson.Domain.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Domain.Controller
{
    public class UserController
    {
        public static MJ_USERS GetUserByUserName(string userName)
        {
            try
            {

                var user = UserBasic.GetUserByUserName(userName);
                if (user == null) return null;

                return new MJ_USERS
                {
                    
                    GUID = user.GUID,
                    USER_ID = user.USER_ID,
                    NAME = user.NAME,
                    USER_NAME = user.USER_NAME,
                    IS_ACTIVE = user.IS_ACTIVE,
                    IS_FIRST_USE = user.IS_FIRST_USE,
                    EMAIL = user.EMAIL,
                    PASSWORD = user.PASSWORD,
                    PROFILE_ID = (int)user.PROFILE_ID,
                    REGISTER_DATE = user.REGISTER_DATE,
                    CITY_ID = user.CITY_ID                    
                };
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static MJ_USERS GetUserById(int id)
        {
            try
            {
                using(DataContext db = new DataContext())
                {
                    return db.MJ_USERS.Where(u => u.USER_ID == id).FirstOrDefault();
                }
                

            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
