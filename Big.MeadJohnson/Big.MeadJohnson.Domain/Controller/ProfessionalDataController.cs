﻿using Big.MeadJohnson.Domain.Data;
using Big.MeadJohnson.Domain.Models;
using Big.MeadJohnson.Domain.SpDataModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Domain.Controller
{
    public class ProfessionalDataController
    {
        private static DataContext db = new DataContext();
        #region SAVE
        public static int? SaveNewProfessional(MJ_PROFESSIONAL professional)
        {

            try
            {
                professional.GUID = Guid.NewGuid();
                db.MJ_PROFESSIONAL.Add(professional);
                db.SaveChanges();
                return (int?)professional.PROFESSIONAL_ID;
            }
            catch (Exception ex)
            {
                return null;
            }

        }

       
        public static int? SaveProfessionalContact(MJ_PROFESSIONAL_CONTACT professionalContact)
        {

            try
            {
                professionalContact.GUID = Guid.NewGuid();
                db.MJ_PROFESSIONAL_CONTACT.Add(professionalContact);
                db.SaveChanges();
                return (int?)professionalContact.PROFESSIONAL_CONTACT_ID;
            }
            catch (Exception ex)
            {
                return null;
            }

        }
        public static int? SaveLabolarInformation(MJ_PROFESSIONAL_LABOR_INFORMATION LaborInformation)
        {
           
                try
                {
                    db.MJ_PROFESSIONAL_LABOR_INFORMATION.Add(LaborInformation);
                    db.SaveChanges();
                    return (int?)LaborInformation.ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
          
        }
        #endregion

        #region UPDATE
        public static bool UpdateProfessionalContact(MJ_PROFESSIONAL_CONTACT professionalContact)
        {

            try
            {
                db.Entry(professionalContact);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        public static bool UpdateOutProfessionalByID(int professionalId)
        {

              var professional = GetProfessionalById(professionalId);
                try
                {
                    professional.LAST_UPDATE = DateTime.Now;
                    db.Entry(professional);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            
        }
        #endregion

        public static ICollection<MJ_PROFESSIONAL_TYPE> GetAllProfessionalTypes()
        {
            using (DataContext db = new DataContext())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_TYPE.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static ICollection<MJ_PROFESSIONAL_LEVEL> GetAllProfessionalLevels()
        {
          
                try
                {
                    return db.MJ_PROFESSIONAL_LEVEL.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
        }

        public static List<ListProfessional> GetListProfessionals(int? mrsId)
        {
            var professionals = new List<MJ_PROFESSIONAL>();

            try
            {
                var list = new List<ListProfessional>();

                using (DataContext db = new DataContext())
                {

                    list = db.Database.SqlQuery<ListProfessional>("MJ_GET_PROFESSIONAL_LIST @MRS_ID", new SqlParameter("@MRS_ID", mrsId)).ToList();
                }
                return list;
               
            }
            catch (Exception ex)
            {
                var message = ex.Message;
                return null;
            }
        }
        public static MJ_USERS GetUserByUserName(string userName)
        {
            try
            {
                return db.MJ_USERS.Where(u => u.USER_NAME == userName).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static MJ_PROFESSIONAL GetProfessionalById(int professionalId)
        {
            
                try
                {
                    return db.MJ_PROFESSIONAL.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            
        }

        public static MJ_PROFESSIONAL_CONTACT GetProfessionalContactById(int professionalId)
        {
            
                try
                {
                    return db.MJ_PROFESSIONAL_CONTACT.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
           
        }
        public static MJ_PROFESSIONAL_LABOR_INFORMATION GetLaboralInformationByProfessionalId(int professionalId)
        {
            
            
                try
                {
                    return db.MJ_PROFESSIONAL_LABOR_INFORMATION.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            
        }
        public static List<MJ_GONNA_DADDY> GetGonnaDaddyByProfessionalId(int professionalId)
        {
           
                try
                {
                    return db.MJ_GONNA_DADDY.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            
        }
        public static MJ_PROFESSIONAL_HABEAS_DATA GetHabeasDataByProfessionalId(int professionalId)
        {
           
                try
                {
                    return db.MJ_PROFESSIONAL_HABEAS_DATA.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
          
        }
     
    }
}
