﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Domain.SpDataModels
{
    public class ListProfessional
    {

        public decimal ID { get; set; }
        public string FIRST_NAME {get;set;}
        public string SECOND_NAME { get; set; }
        public string LAST_NAME { get; set; }
        public string SECOND_LAST_NAME { get; set; }
        [Display(Name = "Tipo de profesional")]
        public string NAME { get; set; }
        [Display(Name = "Última actualización")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
        public string LAST_UPDATE { get; set; }
        [Display(Name = "% de completitud")]
        public Nullable<int> Completitud { get; set; }
        [Display(Name = "Nombre completo")]
        public string FullName
        {
            get
            {
                return string.Format("{0} {1} {2} {3}", FIRST_NAME,
                    string.IsNullOrEmpty(SECOND_NAME) ? "" : SECOND_NAME,
                    LAST_NAME,
                    string.IsNullOrEmpty(SECOND_LAST_NAME) ? "" : SECOND_LAST_NAME);
            }
        }


    }
}
