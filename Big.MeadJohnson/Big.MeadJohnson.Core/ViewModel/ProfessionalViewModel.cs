﻿

namespace Big.MeadJohnson.Core.ViewModel
{
    using System;

    public class ProfessionalViewModel
    {
        //profesional
        public Guid Guid { get; set; }
        public int Id { get; set; }
        public int? MsrId { get; set; }
        public string MsrMail { get; set; }
        public int? ProfessionalTypeId { get; set; }
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public string LastName { get; set; }
        public string SecondLastName { get; set; }
        public string DocumentNumber { get; set; }
        public int? GenderId { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? RegisterDate { get; set; }
        public int? CreateBy { get; set; }
        public DateTime? LastUpdate { get; set; }
        //fin profesional       

        //Contact profesional

        public Guid? ContactGuid { get; set; }

        public int? ContactId { get; set; }

        public string ProfessionalEmail { get; set; }

        public string ProfessionalCellPhone { get; set; }

        public string ProfessionalAddress { get; set; }

        public int? ProfessionalCityId { get; set; }

        public string ProfessionalCity { get; set; }

        public int ProfessionalDepartamentId { get; set; }

        public string ProfessionalDepartament{ get; set; }

        public string ProfessionalBrick { get; set; }



    }
}
