﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Core.DAL
{
    public class Speciality : IDisposable
    {

        //private static dbBigMeadJohnson db = new dbBigMeadJohnson();

        public static ICollection<MJ_SPECIALITY> GetAllSpecialities()
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_SPECIALITY.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static MJ_SPECIALITY GetASpecialityByName(string name, int professionalType)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_SPECIALITY.FirstOrDefault(s => s.NAME == name && s.PROFESSIONAL_TYPE_ID == professionalType);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static int? SaveSpeciality(MJ_SPECIALITY speciality)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    db.MJ_SPECIALITY.Add(speciality);
                    db.SaveChanges();
                    return (int?)speciality.ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Speciality() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
