﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Big.MeadJohnson.Core.DAL
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class dbBigMeadJohnson : DbContext
    {
        public dbBigMeadJohnson()
            : base("name=dbBigMeadJohnson")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<MJ_BRICK> MJ_BRICK { get; set; }
        public virtual DbSet<MJ_CITIES> MJ_CITIES { get; set; }
        public virtual DbSet<MJ_DEPARTMENTS> MJ_DEPARTMENTS { get; set; }
        public virtual DbSet<MJ_GENDER> MJ_GENDER { get; set; }
        public virtual DbSet<MJ_GERENTES> MJ_GERENTES { get; set; }
        public virtual DbSet<MJ_GERENTES_CITIES> MJ_GERENTES_CITIES { get; set; }
        public virtual DbSet<MJ_GONNA_DADDY> MJ_GONNA_DADDY { get; set; }
        public virtual DbSet<MJ_MANEGER> MJ_MANEGER { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL> MJ_PROFESSIONAL { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_CONTACT> MJ_PROFESSIONAL_CONTACT { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_FAMILY_INFORMATION> MJ_PROFESSIONAL_FAMILY_INFORMATION { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_HABEAS_DATA> MJ_PROFESSIONAL_HABEAS_DATA { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_LABOR_INFORMATION> MJ_PROFESSIONAL_LABOR_INFORMATION { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_LEVEL> MJ_PROFESSIONAL_LEVEL { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA> MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_SECRETARY_INFORMATION> MJ_PROFESSIONAL_SECRETARY_INFORMATION { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_TYPE> MJ_PROFESSIONAL_TYPE { get; set; }
        public virtual DbSet<MJ_PROFILE> MJ_PROFILE { get; set; }
        public virtual DbSet<MJ_REGION> MJ_REGION { get; set; }
        public virtual DbSet<MJ_SCHOLARSHIP> MJ_SCHOLARSHIP { get; set; }
        public virtual DbSet<MJ_SPECIAL_DATES> MJ_SPECIAL_DATES { get; set; }
        public virtual DbSet<MJ_SPECIALITY> MJ_SPECIALITY { get; set; }
        public virtual DbSet<MJ_USERS> MJ_USERS { get; set; }
        public virtual DbSet<MJ_PROFESSIONAL_USER> MJ_PROFESSIONAL_USER { get; set; }
    
        public virtual ObjectResult<MJ_GENERAL_REPORT_PROFESSIONAL_Result> MJ_GENERAL_REPORT_PROFESSIONAL(Nullable<int> dEPARTAMENT_ID, Nullable<int> cITY_ID, Nullable<int> uSER_ID, Nullable<int> pROFESSIONAL_TYPE_ID)
        {
            var dEPARTAMENT_IDParameter = dEPARTAMENT_ID.HasValue ?
                new ObjectParameter("DEPARTAMENT_ID", dEPARTAMENT_ID) :
                new ObjectParameter("DEPARTAMENT_ID", typeof(int));
    
            var cITY_IDParameter = cITY_ID.HasValue ?
                new ObjectParameter("CITY_ID", cITY_ID) :
                new ObjectParameter("CITY_ID", typeof(int));
    
            var uSER_IDParameter = uSER_ID.HasValue ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(int));
    
            var pROFESSIONAL_TYPE_IDParameter = pROFESSIONAL_TYPE_ID.HasValue ?
                new ObjectParameter("PROFESSIONAL_TYPE_ID", pROFESSIONAL_TYPE_ID) :
                new ObjectParameter("PROFESSIONAL_TYPE_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MJ_GENERAL_REPORT_PROFESSIONAL_Result>("MJ_GENERAL_REPORT_PROFESSIONAL", dEPARTAMENT_IDParameter, cITY_IDParameter, uSER_IDParameter, pROFESSIONAL_TYPE_IDParameter);
        }
    
        public virtual ObjectResult<MJ_GENERAL_REPORT_PROFESSIONAL_v1_Result> MJ_GENERAL_REPORT_PROFESSIONAL_v1(Nullable<int> dEPARTAMENT_ID, Nullable<int> cITY_ID, Nullable<int> uSER_ID, Nullable<int> pROFESSIONAL_TYPE_ID)
        {
            var dEPARTAMENT_IDParameter = dEPARTAMENT_ID.HasValue ?
                new ObjectParameter("DEPARTAMENT_ID", dEPARTAMENT_ID) :
                new ObjectParameter("DEPARTAMENT_ID", typeof(int));
    
            var cITY_IDParameter = cITY_ID.HasValue ?
                new ObjectParameter("CITY_ID", cITY_ID) :
                new ObjectParameter("CITY_ID", typeof(int));
    
            var uSER_IDParameter = uSER_ID.HasValue ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(int));
    
            var pROFESSIONAL_TYPE_IDParameter = pROFESSIONAL_TYPE_ID.HasValue ?
                new ObjectParameter("PROFESSIONAL_TYPE_ID", pROFESSIONAL_TYPE_ID) :
                new ObjectParameter("PROFESSIONAL_TYPE_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MJ_GENERAL_REPORT_PROFESSIONAL_v1_Result>("MJ_GENERAL_REPORT_PROFESSIONAL_v1", dEPARTAMENT_IDParameter, cITY_IDParameter, uSER_IDParameter, pROFESSIONAL_TYPE_IDParameter);
        }
    
        public virtual ObjectResult<MJ_GENERAL_REPORT_PROFESSIONAL_v2_Result> MJ_GENERAL_REPORT_PROFESSIONAL_v2(Nullable<int> dEPARTAMENT_ID, Nullable<int> cITY_ID, Nullable<int> uSER_ID, Nullable<int> pROFESSIONAL_TYPE_ID)
        {
            var dEPARTAMENT_IDParameter = dEPARTAMENT_ID.HasValue ?
                new ObjectParameter("DEPARTAMENT_ID", dEPARTAMENT_ID) :
                new ObjectParameter("DEPARTAMENT_ID", typeof(int));
    
            var cITY_IDParameter = cITY_ID.HasValue ?
                new ObjectParameter("CITY_ID", cITY_ID) :
                new ObjectParameter("CITY_ID", typeof(int));
    
            var uSER_IDParameter = uSER_ID.HasValue ?
                new ObjectParameter("USER_ID", uSER_ID) :
                new ObjectParameter("USER_ID", typeof(int));
    
            var pROFESSIONAL_TYPE_IDParameter = pROFESSIONAL_TYPE_ID.HasValue ?
                new ObjectParameter("PROFESSIONAL_TYPE_ID", pROFESSIONAL_TYPE_ID) :
                new ObjectParameter("PROFESSIONAL_TYPE_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MJ_GENERAL_REPORT_PROFESSIONAL_v2_Result>("MJ_GENERAL_REPORT_PROFESSIONAL_v2", dEPARTAMENT_IDParameter, cITY_IDParameter, uSER_IDParameter, pROFESSIONAL_TYPE_IDParameter);
        }
    
        public virtual ObjectResult<MJ_GET_PROFESSIONAL_LIST_Result> MJ_GET_PROFESSIONAL_LIST(Nullable<int> mRS_ID)
        {
            var mRS_IDParameter = mRS_ID.HasValue ?
                new ObjectParameter("MRS_ID", mRS_ID) :
                new ObjectParameter("MRS_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MJ_GET_PROFESSIONAL_LIST_Result>("MJ_GET_PROFESSIONAL_LIST", mRS_IDParameter);
        }
    
        public virtual ObjectResult<MJ_PROFESSIONAL_LIST_Result> MJ_PROFESSIONAL_LIST(Nullable<int> mRS_ID)
        {
            var mRS_IDParameter = mRS_ID.HasValue ?
                new ObjectParameter("MRS_ID", mRS_ID) :
                new ObjectParameter("MRS_ID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<MJ_PROFESSIONAL_LIST_Result>("MJ_PROFESSIONAL_LIST", mRS_IDParameter);
        }

        object placeHolderVariable;
    }
}
