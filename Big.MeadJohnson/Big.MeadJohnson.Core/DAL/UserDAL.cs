﻿

namespace Big.MeadJohnson.Core.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;

    public class UserDAL : IDisposable
    {
        private static dbBigMeadJohnson db = new dbBigMeadJohnson();

        public static async Task<ICollection<MJ_USERS>> GetAllUser()
        {
            try
            {
                return await db.MJ_USERS.ToListAsync();

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        public static MJ_USERS GetUserByGuId(string id)
        {
            try
            {
                return db.MJ_USERS.Where(u => u.GUID == id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public static MJ_USERS GetUserById(int id)
        {
            try
            {
                return db.MJ_USERS.Where(u => u.ID == id).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static MJ_USERS GetUserByUserName(string userName)
        {
            try
            {
                return db.MJ_USERS.Where(u => u.USER_NAME == userName).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }
        
        public static MJ_USERS GetUserByEmail(string email)
        {
            try
            {
                return db.MJ_USERS.Where(u => u.EMAIL == email).FirstOrDefault();

            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool SaveNewUser(MJ_USERS user)
        {
            try
            {
                db.MJ_USERS.Add(user);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static bool UpdateUser(MJ_USERS user)
        {
            try
            {
                var objUser = GetUserById((int)user.ID);
                db.Entry(objUser).CurrentValues.SetValues(user);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        

        public static ICollection<MJ_USERS> GetAUserByCityId(int cityId)
        {
            try
            {
                return db.MJ_USERS.Where(u => u.CITY_ID == cityId).ToList();

            }
            catch (Exception ex)
            {
                return null;

            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~User() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
