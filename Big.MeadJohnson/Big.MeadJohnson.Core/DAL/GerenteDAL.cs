﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Core.DAL
{
    public class GerenteDAL
    {
        private static dbBigMeadJohnson db = new dbBigMeadJohnson();
        public static ICollection<MJ_GERENTES> GetAllGerentes()
        {
            try
            {
                return db.MJ_GERENTES.ToList();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static List<MJ_GERENTES> GetGerentesByCities(string cityId)
        {
            try
            {
                var gc = db.MJ_GERENTES_CITIES.Where(x => x.CITY_ID == cityId).Select(x=>x.GERENTE_ID).ToList();
                var gerentesPorCiudad = db.MJ_GERENTES.Where(x => gc.Any(i => i == x.ID)).ToList();
                return gerentesPorCiudad;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static MJ_GERENTES GetGerenteById(string gerenteId)
        {
            try
            {
                return db.MJ_GERENTES.Where(u => u.ID.ToString() == gerenteId).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static object CreateGerente(MJ_GERENTES gerente)
        {
            try
            {
                db.MJ_GERENTES.Add(gerente);
                db.SaveChanges();
                return "OK";

            }catch(Exception ex)
            {
                throw ex;
            }
        }

        public static bool SaveNewUser(MJ_USERS user)
        {
            try
            {
                db.MJ_USERS.Add(user);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public static List<MJ_GERENTES_CITIES> GetCitiesOfGerente(string id)
        {
            var idsCities = db.MJ_GERENTES_CITIES.Where(x => x.GERENTE_ID.ToString() == id).Select(x => x.CITY_ID.ToUpper()).ToList();
            var cities = db.MJ_CITIES.Include(x => x.MJ_GERENTES_CITIES);
            var ret = db.MJ_GERENTES_CITIES.Include(x=>x.MJ_CITIES).Where(x => x.GERENTE_ID.ToString() == (id)).ToList();
            return ret;
        }

        public static MJ_GERENTES GetGerenteByEmail(string correoElectronico)
        {
            return db.MJ_GERENTES.Where(x => x.EMAIL == correoElectronico).FirstOrDefault();
        }

        public static MJ_GERENTES GetGerenteByCedula(int cedula)
        {
            return db.MJ_GERENTES.Where(x => x.CEDULA == cedula).FirstOrDefault();
        }

        //public static bool UpdateUser(MJ_USERS user)
        //{
        //    try
        //    {
        //        var objUser = GetGerenteById((int)user.ID);
        //        db.Entry(objUser).CurrentValues.SetValues(user);
        //        db.SaveChanges();
        //        return true;
        //    }
        //    catch (Exception ex)
        //    {
        //        return false;
        //    }
        //}


        //public static ICollection<MJ_USERS> GetAUserByCityId(int cityId)
        //{
        //    try
        //    {
        //        return db.MJ_USERS.Where(u => u.CITY_ID == cityId).ToList();

        //    }
        //    catch (Exception ex)
        //    {
        //        return null;

        //    }
        //}

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    db.Dispose();
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        public static MJ_GERENTES EditarGerente(MJ_GERENTES gerente)
        {
            db.Entry(gerente).State = EntityState.Modified;
            db.SaveChanges();
            return gerente;
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~User() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public static string AsociarCiudad(MJ_GERENTES_CITIES gerenCities)
        {
            db.MJ_GERENTES_CITIES.Add(gerenCities);
            db.SaveChanges();
            return "OK";
        }
        #endregion
    }
}
