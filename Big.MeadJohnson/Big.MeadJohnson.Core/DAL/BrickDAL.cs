﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Core.DAL
{
    public class BrickDAL : IDisposable
    {
        private static dbBigMeadJohnson db = new dbBigMeadJohnson();
        public static ICollection<MJ_BRICK> GetAllBricks()
        {
            try
            {
                return db.MJ_BRICK.ToList();
            }
            catch (Exception ex)
            {
                return null;
            }
        }


        public static MJ_BRICK GetBricksByCode(string code)
        {
            try
            {
                return db.MJ_BRICK.Where(b => b.BRICK_CODE == code).FirstOrDefault();
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static bool SaveBrick(MJ_BRICK brick)
        {
            try
            {
                db.MJ_BRICK.Add(brick);
                db.SaveChanges();
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~BrickDAL() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}
