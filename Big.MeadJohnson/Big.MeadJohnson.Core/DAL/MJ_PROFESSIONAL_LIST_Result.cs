//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Big.MeadJohnson.Core.DAL
{
    using System;
    
    public partial class MJ_PROFESSIONAL_LIST_Result
    {
        public decimal ID { get; set; }
        public string FIRST_NAME { get; set; }
        public string SECOND_NAME { get; set; }
        public string LAS_NAME { get; set; }
        public string SECOND_LAST_NAME { get; set; }
        public string NAME { get; set; }
        public string LAST_UPDATE { get; set; }
        public Nullable<int> Completitud { get; set; }
    }
}
