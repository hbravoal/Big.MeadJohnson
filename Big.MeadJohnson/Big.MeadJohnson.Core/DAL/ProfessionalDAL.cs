﻿

namespace Big.MeadJohnson.Core.DAL
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Threading.Tasks;
    public class ProfessionalDAL : IDisposable
    {
        private static dbBigMeadJohnson db = new dbBigMeadJohnson();

        public static async Task<ICollection<MJ_PROFESSIONAL>> GetAllProfessionals()
        {

            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return await db.MJ_PROFESSIONAL.ToListAsync();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static MJ_PROFESSIONAL_CONTACT GetProfessionalContactById(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_CONTACT.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static List<MJ_PROFESSIONAL_CONTACT> GetAllProfessionalByCityId(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_CONTACT.Where(pc => pc.CYTY_ID == professionalId).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static MJ_PROFESSIONAL GetProfessionalById(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL.Where(pc => pc.ID == (decimal)professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static MJ_PROFESSIONAL GetProfessionalByIdDiferentSecretary(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL.Where(pc => pc.PROFESSIONAL_TYPE_ID!=4 && pc.ID == (decimal)professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        public static MJ_PROFESSIONAL GetProfessionalByToken(string token)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL.Where(pc => pc.TOKEN == token).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        public static MJ_PROFESSIONAL GetProfessionalByDocumentNumber(string documentNumber)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL.Where(pc => pc.DOCUMENT_NUMBER == documentNumber).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        public static MJ_PROFESSIONAL_CONTACT GetProfessionalContactByEmail(string email)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_CONTACT.Where(pc => pc.E_MAIL == email).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        public static ICollection<MJ_PROFESSIONAL_TYPE> GetAllProfessionalType()
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_TYPE.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static ICollection<MJ_PROFESSIONAL_LEVEL> GetAllProfessionalLevels()
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_LEVEL.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static int? SaveNewProfessional(MJ_PROFESSIONAL professional)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    professional.GUID = Guid.NewGuid();
                    db.MJ_PROFESSIONAL.Add(professional);
                    db.SaveChanges();
                    return (int?)professional.ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static int? SaveProfessionalContact(MJ_PROFESSIONAL_CONTACT professionalContact)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    db.MJ_PROFESSIONAL_CONTACT.Add(professionalContact);
                    db.SaveChanges();
                    return (int?)professionalContact.ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static bool UpdateProfessionalContact(MJ_PROFESSIONAL_CONTACT professionalContact)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    var objProfessional = db.MJ_PROFESSIONAL_CONTACT.
                        Where(c => c.PROFESSIONAL_ID == professionalContact.PROFESSIONAL_ID).FirstOrDefault();
                    db.Entry(objProfessional).CurrentValues.SetValues(professionalContact);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        //UpdateLabolarInformation

        public static bool UpdateLabolarInformation(MJ_PROFESSIONAL_LABOR_INFORMATION laboralInformation)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    var objLaboralInfo = db.MJ_PROFESSIONAL_LABOR_INFORMATION.
                        Where(c => c.PROFESSIONAL_ID == laboralInformation.PROFESSIONAL_ID).FirstOrDefault();
                    if (objLaboralInfo != null)
                    {
                        laboralInformation.GUID = objLaboralInfo.GUID;
                        laboralInformation.ID = objLaboralInfo.ID;
                        db.Entry(objLaboralInfo).CurrentValues.SetValues(laboralInformation);
                    }
                    else
                    {
                        laboralInformation.GUID = Guid.NewGuid().ToString();
                        db.MJ_PROFESSIONAL_LABOR_INFORMATION.Add(laboralInformation);
                    }

                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static int? SaveLabolarInformation(MJ_PROFESSIONAL_LABOR_INFORMATION LaborInformation)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    db.MJ_PROFESSIONAL_LABOR_INFORMATION.Add(LaborInformation);
                    db.SaveChanges();
                    return (int?)LaborInformation.ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static int? SaveSecretaryInformation(MJ_PROFESSIONAL_SECRETARY_INFORMATION secretaryInformation)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.Add(secretaryInformation);
                    db.SaveChanges();
                    return (int?)secretaryInformation.ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        

         public static int? SaveGonnaDaddy(MJ_GONNA_DADDY gonnaDaddy)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    db.MJ_GONNA_DADDY.Add(gonnaDaddy);
                    db.SaveChanges();
                    return (int?)gonnaDaddy.id;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static int? SaveFamilyInformation(MJ_PROFESSIONAL_FAMILY_INFORMATION familyInformation)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    db.MJ_PROFESSIONAL_FAMILY_INFORMATION.Add(familyInformation);
                    db.SaveChanges();
                    return (int?)familyInformation.ID;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        public static List<MJ_GONNA_DADDY> GetGonnaDaddyByProfessionalId(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_GONNA_DADDY.Where(pc => pc.Professional_id == professionalId).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static MJ_PROFESSIONAL_LABOR_INFORMATION GetLaboralInformationByProfessionalId(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_LABOR_INFORMATION.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static MJ_PROFESSIONAL_SECRETARY_INFORMATION GetSecretaryByProfessionalId(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static MJ_PROFESSIONAL_SECRETARY_INFORMATION GetSecretaryDocument(string document)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.Where(pc => pc.DOCUMENT_NUMBER == document).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static List<MJ_PROFESSIONAL_SECRETARY_INFORMATION> GetAllSecretaryDocument(string document)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    var temp= db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.Where(pc => pc.DOCUMENT_NUMBER == document);
                    return temp.ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static MJ_PROFESSIONAL GetProfessionalTypebyDocument(string document)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL.Where(pc => pc.DOCUMENT_NUMBER == document && pc.PROFESSIONAL_TYPE_ID==4 ).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static List<MJ_PROFESSIONAL_FAMILY_INFORMATION> GetFamilyInformationByProfessionalId(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_FAMILY_INFORMATION.Where(pc => pc.PROFESSIONAL_ID == professionalId).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static bool SaveProfessionalHabeasData(MJ_PROFESSIONAL_HABEAS_DATA habeasData)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {

                try
                {
                    var objhabeasData = db.MJ_PROFESSIONAL_HABEAS_DATA.
                        Where(pc => pc.PROFESSIONAL_ID == habeasData.PROFESSIONAL_ID).FirstOrDefault();

                    if (objhabeasData == null)
                    {
                        habeasData.GUID = Guid.NewGuid().ToString();
                        db.MJ_PROFESSIONAL_HABEAS_DATA.Add(habeasData);
                    }
                    else
                    {
                        habeasData.GUID = objhabeasData.GUID;
                        habeasData.ID = objhabeasData.ID;
                        db.Entry(objhabeasData).CurrentValues.SetValues(habeasData);
                    }

                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static bool SaveSecretaryHabeasData(MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA habeasData)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {

                try
                {
                    var objhabeasData = db.MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA.
                        Where(pc => pc.SECRETARY_ID == habeasData.SECRETARY_ID).FirstOrDefault();

                    if (objhabeasData == null)
                    {
                        habeasData.GUID = Guid.NewGuid().ToString();
                        db.MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA.Add(habeasData);
                    }
                    else
                    {
                        habeasData.GUID = objhabeasData.GUID;
                        habeasData.ID = objhabeasData.ID;
                        db.Entry(objhabeasData).CurrentValues.SetValues(habeasData);
                    }

                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static MJ_PROFESSIONAL_HABEAS_DATA GetProfessionalHabeasDataByProfessionalId(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_HABEAS_DATA.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }
        public static MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA GetProfessionalHabeasDataBySecretaryId(int secretaryId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_PROFESSIONAL_SECRETARY_HABEAS_DATA.Where(pc => pc.SECRETARY_ID == secretaryId).FirstOrDefault();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }


        /// <summary>
        /// Metodos Actualizacion
        /// </summary>      

        public static bool UpdateProfessional(MJ_PROFESSIONAL professional)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    var objProfessional = db.MJ_PROFESSIONAL.
                        Where(p => p.ID == professional.ID).FirstOrDefault();
                    db.Entry(objProfessional).CurrentValues.SetValues(professional);
                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        public static void DeleteGonnaDaddyById(int id)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {

                    var gonnaDaddy = db.MJ_GONNA_DADDY.Where(pc => pc.Professional_id == id).FirstOrDefault();

                    db.MJ_GONNA_DADDY.Remove(gonnaDaddy);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
        }
        
        public static void DeleteGeneralInformationById(int id)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    var gonna = db.MJ_PROFESSIONAL_FAMILY_INFORMATION.Where(pc => pc.ID == id).FirstOrDefault();
                    db.MJ_PROFESSIONAL_FAMILY_INFORMATION.Remove(gonna);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {

                }
            }
        }


        public static bool UpdateSecretaryInformation(MJ_PROFESSIONAL_SECRETARY_INFORMATION secretaryInformation)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {


                try
                {
                    var secretary = db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.
                        Where(s => s.PROFESSIONAL_ID == secretaryInformation.PROFESSIONAL_ID).FirstOrDefault();

                    if (secretary == null)
                    {
                        secretaryInformation.GUID = Guid.NewGuid().ToString();
                        db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.Add(secretaryInformation);
                    }
                    else
                    {
                        secretaryInformation.GUID = secretary.GUID;
                        secretaryInformation.ID = secretary.ID;
                        db.Entry(secretary).CurrentValues.SetValues(secretaryInformation);
                    }

                    db.SaveChanges();
                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }

        public static void DeleteSecretaryInformationById(int professionalId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {

                try
                {
                    var secretary = db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.Where(pc => pc.PROFESSIONAL_ID == professionalId).FirstOrDefault();
                    db.MJ_PROFESSIONAL_SECRETARY_INFORMATION.Remove(secretary);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { }
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ProfessionalDAL() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        public static List<MJ_USERS> GetAllRepresentantsByCityId(int cityId)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    return db.MJ_USERS.Where(pc => pc.CITY_ID == cityId).ToList();
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        public static void AsociateRepresentatsToProfessional(List<MJ_PROFESSIONAL_USER> asociados)
        {
            using (dbBigMeadJohnson db = new dbBigMeadJohnson())
            {
                try
                {
                    db.MJ_PROFESSIONAL_USER.AddRange(asociados);
                    db.SaveChanges();
                }
                catch (Exception ex)
                {
                    
                }
            }
        }

        #endregion
    }
}
