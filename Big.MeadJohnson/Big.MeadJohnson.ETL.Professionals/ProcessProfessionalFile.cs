﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using System.Transactions;
using Big.MeadJohnson.Business.Business;

namespace Big.MeadJohnson.ETL.Professionals
{

    public class ProcessProfessionalFile
    {

        public static List<Business.Models.City> ListCities
        {
            get
            {
                return CityBLL.GetAllCities();
            }
        }

        public static List<string> ProcessProfessionalsFile()
        {

            var listErrorMessage = new List<string>();
            var message = string.Empty;

            //Create COM Objects. Create a COM object for everything that is referenced
            Excel.Application xlApp = new Excel.Application();

            var fileName = ConfigurationManager.AppSettings["Professional.File.Name"];
            var filePath = ConfigurationManager.AppSettings["Professional.File.Path"];
            var ProcessedFilePath = ConfigurationManager.AppSettings["Professional.File.Path.Processed"];

            if (!File.Exists(string.Format("{0}{1}", filePath, fileName)))
            {
                message = "No se encontro el archivo " + fileName + " en la ruta especificada";
                listErrorMessage.Add(message);
                Console.WriteLine(message);
                return listErrorMessage;
            }

            string ext = Path.GetExtension(string.Format("{0}{1}", filePath, fileName));
            if (string.IsNullOrEmpty(ext) || !ext.Equals(".xlsx"))
            {
                message = "El formato del archivo " + fileName + " no es valido.";
                listErrorMessage.Add(message);
                Console.WriteLine(message);
                return listErrorMessage;
            }

            if (listErrorMessage.Count > 0)
                return listErrorMessage;

            Excel.Workbook xlWorkbook = xlApp.Workbooks.Open(string.Format("{0}{1}", filePath, fileName));
            Excel._Worksheet xlWorksheet = xlWorkbook.Sheets[1];
            Excel.Range xlRange = xlWorksheet.UsedRange;

            int rowCount = xlRange.Rows.Count;
            int colCount = xlRange.Columns.Count;

            var totalColumns = 18;

            if (colCount > totalColumns || colCount < totalColumns)
            {
                message = "El archivo " + fileName + " no tiene el numero de columnas validos.";
                listErrorMessage.Add(message);
                return listErrorMessage;
            }


            var count = 0;
            var lineProccesed = 2;
            for (int i = 0; i <= rowCount; i++)
            {
                Row row = null;

                if (count > 0 && i > 1)
                {
                    try
                    {
                        row = new Row()
                        {

                            Msr = Convert.ToString(xlRange.Cells[i, 1].Value2),
                            MsrMail = Convert.ToString(xlRange.Cells[i, 2].Value2),
                            TipoProfesional = Convert.ToString(xlRange.Cells[i, 3].Value2),
                            Cedula = Convert.ToString(xlRange.Cells[i, 4].Value2),
                            PrimerNombre = Convert.ToString(xlRange.Cells[i, 5].Value2),
                            SegundoNombre = Convert.ToString(xlRange.Cells[i, 6].Value2),
                            PrimerApellido = Convert.ToString(xlRange.Cells[i, 7].Value2),
                            SegundoApellido = Convert.ToString(xlRange.Cells[i, 8].Value2),
                            Dia = Convert.ToString(xlRange.Cells[i, 9].Value2),
                            Mes = Convert.ToString(xlRange.Cells[i, 10].Value2),
                            Ano = Convert.ToString(xlRange.Cells[i, 11].Value2),
                            Especialidad = Convert.ToString(xlRange.Cells[i, 12].Value2),
                            Entidad = Convert.ToString(xlRange.Cells[i, 13].Value2),
                            Celular = Convert.ToString(xlRange.Cells[i, 14].Value2),
                            Email = Convert.ToString(xlRange.Cells[i, 15].Value2),
                            Ciudad = Convert.ToString(xlRange.Cells[i, 16].Value2),
                            Brick = Convert.ToString(xlRange.Cells[i, 17].Value2),
                            Direccion = Convert.ToString(xlRange.Cells[i, 18].Value2),
                        };

                        string proccessMessage = string.Empty;
                        if (row != null)
                            proccessMessage = ProccessRow(row, lineProccesed);

                        if (!string.IsNullOrEmpty(proccessMessage)) listErrorMessage.Add(proccessMessage);

                        row = null;
                    }
                    catch (Exception ex)
                    {
                        message = "Ha ocurrido un error al procesar la linea: " + lineProccesed;
                        listErrorMessage.Add(ex.Message + "\n " + message);
                        continue;
                    }

                    lineProccesed++;
                }

                count++;
                Console.WriteLine(count.ToString());

            }

            GC.Collect();
            GC.WaitForPendingFinalizers();
            Marshal.ReleaseComObject(xlRange);
            Marshal.ReleaseComObject(xlWorksheet);
            xlWorkbook.Close();
            Marshal.ReleaseComObject(xlWorkbook);
            xlApp.Quit();
            Marshal.ReleaseComObject(xlApp);

            return listErrorMessage;
        }

        private static string ProccessRow(Row row, int lineProccesed)
        {

            System.Threading.Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.CreateSpecificCulture("es-CO");
            var message = string.Empty;

            if (row == null)
                return "No se pudo procesar la linea: " + lineProccesed;

            if (string.IsNullOrEmpty(row.MsrMail))
                return "El campo mrs mail esta vacio. \t Linea:" + lineProccesed;

            if (!IsValidEmail(row.MsrMail)) return "El campo mrs mail no tiene el formato correcto. \t Linea:" + lineProccesed;

            var user = UserBLL.GetUserByEmail(row.MsrMail.ToLower());
            if (user == null) return "No se encontró un representante asociado al correo: " + row.MsrMail + " \t Linea:" + lineProccesed;


            if (!string.IsNullOrEmpty(row.Email))
            {
                var professionalContact = ProfessionalBLL.GetProfessionalContactByEmail(row.Email.ToLower());
                if (professionalContact != null)
                    return "El correo: " + row.Email + " ya se encuentra asociado a un profesional \t Linea:" + lineProccesed;
            }

            try
            {
                using (TransactionScope scope = new TransactionScope())
                {
                    var professional = new Business.Models.Professional();
                    professional.Id = 0;
                    professional.Guid = Guid.NewGuid();
                    professional.MsrMail = row.MsrMail.ToLower();
                    professional.MsrId = user.Id;
                    professional.DocumentNumber = row.Cedula;
                    professional.FirstName = row.PrimerNombre;
                    professional.SecondName = row.SegundoNombre;
                    professional.LastName = row.PrimerApellido;
                    professional.SecondLastName = row.SegundoApellido;


                    if (!string.IsNullOrEmpty(row.TipoProfesional))
                    {
                        var professionalType = ProfessionalBLL.GetAllProfessionalTypes().
                            Where(pt => pt.Name.ToLower() == row.TipoProfesional.ToLower()).FirstOrDefault();
                        if (professionalType != null)
                            professional.ProfessionalTypeId = professionalType.Id;
                    }

                    DateTime? date = null;

                    if (!string.IsNullOrEmpty(row.Dia) && !string.IsNullOrEmpty(row.Mes))
                        date = ValidateDate(row.Dia, row.Mes, row.Ano);

                    if (date != null)
                        professional.BirthDate = (DateTime)date;

                    professional.CreateBy = user.Id;
                    professional.RegisterDate = DateTime.Now;
                    professional.Token = Guid.NewGuid().ToString();
                    professional.ExpirationDateToken = DateTime.Now.
                        AddDays(Convert.ToDouble(ConfigurationManager.AppSettings["Token.Expiration.Date"]));

                    var professionalId = ProfessionalBLL.SaveNewProfessional(professional);

                    if (professionalId == null)
                        return "Ha ocurrido un error al intentar guardar el profesional en la base de datos \t Linea: " + lineProccesed;


                    var professionalContact = new Business.Models.ProfessionalContact();

                    professionalContact.Guid = Guid.NewGuid();
                    professionalContact.Id = 0;
                    professionalContact.ProfessionalId = (int)professionalId;

                    if (!string.IsNullOrEmpty(row.Email))
                    {
                        if (IsValidEmail(row.Email))
                            professionalContact.ProfessionalEmail = row.Email;
                    }

                    professionalContact.ProfessionalAddress = row.Direccion;
                    professionalContact.ProfessionalCellPhone = row.Celular;

                    Business.Models.City city = null;

                    if (!string.IsNullOrEmpty(row.Ciudad))
                        city = ListCities.Where(c => c.Name.ToLower() == row.Ciudad.ToLower()).FirstOrDefault();

                    if (!string.IsNullOrEmpty(row.Brick))
                    {
                        var brick = BrickBLL.GetBricksByCode(row.Brick);
                        if (brick == null)
                        {
                            if (city != null)
                            {
                                BrickBLL.SaveBrick(new Business.Models.Brick
                                {
                                    Guid = Guid.NewGuid(),
                                    CityId = city.Id,
                                    Code = row.Brick,
                                    UserId = null,
                                    Description = null
                                });
                            }
                        }

                        professionalContact.ProfessionalBrickId = row.Brick;
                    }

                    if (city != null)
                        professionalContact.ProfessionalCityId = city.Id;

                    ProfessionalBLL.SaveProfessionalContact(professionalContact);

                    var laboralInformation = new Business.Models.LaborInformation();
                    laboralInformation.Id = lineProccesed;


                    if (!string.IsNullOrEmpty(row.Especialidad))
                    {

                        var speciality = SpecialityBLL.GetSpecialitiesByName(row.Especialidad.ToLower(), (int)professional.ProfessionalTypeId);

                        if (speciality == null)
                        {

                            var specialityId = SpecialityBLL.SaveSpeciality(new Business.Models.Speciality
                            {
                                Guid = Guid.NewGuid(),
                                IsActive = true,
                                Name = row.Especialidad,
                                ProfessionalTypeId = professional.ProfessionalTypeId,
                            });

                            laboralInformation.IsSpecialist = true;
                            laboralInformation.SpecialityId = specialityId;
                        }
                        else
                        {
                            laboralInformation.IsSpecialist = true;
                            laboralInformation.SpecialityId = speciality.Id;
                        }

                        laboralInformation.Guid = Guid.NewGuid();
                        laboralInformation.Company = row.Entidad;
                        laboralInformation.ProfessionalId = (int)professionalId;
                        ProfessionalBLL.SaveLabolarInformation(laboralInformation);
                    }

                    professional = null;
                    professionalContact = null;
                    laboralInformation = null;
                    scope.Complete();
                }
            }
            catch (Exception ex)
            {
                message = "Ha ocurrido un error al intentar guardar el profesional en la base de datos \t Linea: " + lineProccesed;
            }

            return message;

        }


        private static DateTime? ValidateDate(string day, string month, string year)
        {
            try
            {

                if (string.IsNullOrEmpty(Convert.ToString(day))) return null;
                if (string.IsNullOrEmpty(Convert.ToString(month))) return null;
                if (string.IsNullOrEmpty(Convert.ToString(year))) year = "1900";

                var date = string.Format("{0}/{1}/{2}",
                    year,
                    month,
                    day);

                if (date.Length > 10)
                    return null;

                if (date.Length < 10)
                    return null;

                if (date.Contains('e'))
                    return null;

                return DateTime.Parse(date);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
