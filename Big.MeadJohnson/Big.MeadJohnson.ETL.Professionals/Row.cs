﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.ETL.Professionals
{
    public class Row
    {
        public string Msr { get; set; }

        public string MsrMail { get; set; }

        public string TipoProfesional { get; set; }

        public string Cedula { get; set; }

        public string PrimerNombre { get; set; }

        public string SegundoNombre { get; set; }

        public string PrimerApellido { get; set; }

        public string SegundoApellido { get; set; }

        public string Dia { get; set; }

        public string Mes { get; set; }

        public string Ano { get; set; }

        public string Especialidad { get; set; }

        public string Entidad { get; set; }

        public string Celular { get; set; }

        public string Email { get; set; }

        public string Ciudad { get; set; }

        public string Brick { get; set; }

        public string Direccion { get; set; }
    }
}
