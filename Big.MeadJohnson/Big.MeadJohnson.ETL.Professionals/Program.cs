﻿using Big.MeadJohnson.Core.Common.Diagnostics;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.ETL.Professionals
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Inicio Proceso Carga de Profesionales.");

            var message = string.Empty;
            var fileName = ConfigurationManager.AppSettings["Professional.File.Name"];
            var filePath = ConfigurationManager.AppSettings["Professional.File.Path"];
            var ProcessedFilePath = ConfigurationManager.AppSettings["Professional.File.Path.Processed"];

            if (!File.Exists(string.Format("{0}{1}", filePath, fileName)))
            {
                message = "No se encontro el archivo en la ruta especificada";
                Console.WriteLine(message);
                ExceptionLogging.LogInfo(message);
                return;
            }

            var errors = ProcessProfessionalFile.ProcessProfessionalsFile();

            if (errors != null && errors.Count > 0)
            {
                message = "El archivo se ha procesado con algunos errores. Verifique el archivo de logs.";
                Console.WriteLine(message);

                foreach (var error in errors)
                {
                    ExceptionLogging.LogInfo(error);
                    Console.WriteLine(error);
                }

                if (File.Exists(string.Format("{0}{1}", filePath, fileName)))
                {
                    File.Copy(string.Format("{0}{1}", filePath, fileName),
                        string.Format("{0}{1}{2}", ProcessedFilePath, "Precarga_Profesionales_" + DateTime.Now.ToString("yyyyMMdd"), ".xls"));
                }

            }
            else
            {
                message = "El archivo se ha procesado con exito.";
                Console.WriteLine(message);

            }

            Console.WriteLine("Fin porceso");
            Console.ReadKey();
        }
    }
}
