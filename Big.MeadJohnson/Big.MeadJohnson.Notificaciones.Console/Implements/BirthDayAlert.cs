﻿namespace Big.MeadJohnson.Notificaciones.Console.Implements
{
    using System.Collections.Generic;
    using Big.MeadJohnson.Notificaciones.Console.Domain;
    using Big.MeadJohnson.Notificaciones.Console.Interfaces;
    public class BirthDayAlert : ITemplate
    {
        public List<string> Item { get => throw new System.NotImplementedException(); set => throw new System.NotImplementedException(); }

        public string TableConstruct(List<Homenajeado> homenajeados)
        {
            string tabla = "<table width='530' cellspacing='0' cellpadding='0' border='0' align='center'>";
            int count = 1;
            foreach (var home in homenajeados)
            {
                count += 1;
                if (count % 2 == 0)
                {
                    tabla += string.Format(@"
            <tr>
                <td>
                    <table width='530' cellspacing='0' cellpadding='0' border='0' align=''>
                        <tr>
                            <td width='262' style='font-family: Arial, Helvetiva; color: #2c3f8c; text-align: center; background-color: #ffffff; -moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px; padding: 5px; box-sizing: border-box;'>
                                <strong style='font-size: 16px;'>
                                        {3}
                                        <br>
                                        {0}
                                </strong>                   
                                <span style='font-size: 12px;'>  
                                        {2}
                                        <br> 
                                        {1} 
                                </span>
                            </td>
                            <td width='6' height='1'>
                            </td>", home.NOMBRE, home.EMAIL, home.CIUDAD, home.FECHA);
                    continue;
                }
                tabla += string.Format(@"
                            <td width='262' style='font-family: Arial, Helvetiva; color: #2c3f8c; text-align: center; background-color: #ffffff; -moz-border-radius:5px; -webkit-border-radius:5px; border-radius:5px; padding: 5px; box-sizing: border-box;'>
                                <strong style='font-size: 16px;'>
                                    {3}
                                    <br>
                                    {0}
                                    <br>
                                </strong>                   
                                <span style='font-size: 12px;'>  
                                    {2}
                                    <br>
                                    {1} 
                                </span>
                            </td>
                        </tr>
                </table>
            </td>
        </tr>", home.NOMBRE, home.EMAIL, home.CIUDAD, home.FECHA);
                tabla += "<tr><td width = '530' height = '6' ></td></tr> ";
            }



            tabla += "</table>";
            return tabla;
        }
    }
}
