﻿namespace Big.MeadJohnson.Notificaciones.Console.Implements
{
    using System.Collections.Generic;
    using System.Linq;
    using Big.MeadJohnson.Notificaciones.Console.Domain;
    using Console.Interfaces;
    public class DefaultSpecialDate : ITemplate
    {
        public List<string> Item { get; set; }
        public string TableConstruct(List<Homenajeado> homenajeados)
        {
            Item = new List<string>();
            
            string lastItem = "";
            int flag = 0;
            if (homenajeados.Count % 2 != 0)
            {
                //Se quita el último si es par.
                var lastHomenajeado = new Homenajeado();

                lastHomenajeado = homenajeados.LastOrDefault();

                lastItem = @"<tr>
                                        <td>
										    <table width='530' cellspacing='0' cellpadding='0' border='0' align=''>
                                                <tr>";
                lastItem += HomenajeadoToHtml(lastHomenajeado);

                lastItem += @"</tr>
                                             </table>
                                        </td>
                                       </tr>
									  <tr>
                                        <td width='530' height='6' style='font-size: 1px;'>
                                             &nbsp;
									    </td>
                                       </tr>";
                homenajeados.Remove(lastHomenajeado);
            }
            //int numberHomenaje = homenajeados.Count;
            foreach (var item in homenajeados)
            {
                Item.Add(
                HomenajeadoToHtml(item));
            }
            
            
            int numberHomenaje = Item.Count;
            string table = "";
            
            //La primera mitad de los homenajeados
                    for (flag =0 ; flag < (numberHomenaje); flag= flag+1)
                    {
                            table += @"<tr>
                                        <td>
										    <table width='530' cellspacing='0' cellpadding='0' border='0' align=''>
                                                <tr>";
                                                    table += Item[flag];
                                                    table += "<td width='6' height='83' style='font-size: 1px;'>&nbsp;</td>";
                                                    table += Item[flag+1];
                                                    flag++;
                                        table+=@"</tr>
                                             </table>
                                        </td>
                                       </tr>
									  <tr>
                                        <td width='530' height='6' style='font-size: 1px;'>
                                             &nbsp;
									    </td>
                                       </tr>";
                    }
            table += lastItem;
            return table;
        }

        private string HomenajeadoToHtml(Homenajeado item)
        {
            return
                string.Format(@"
            <td width='262' height='83' style='background-color:#fce3c5; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #29408A; text-align: center; border-radius: 10px;'>
                        <strong style='font-size: 16px;'> {0}<br></strong>
                        {3}/{1}  <br>
                        {2}
            </td>", item.NOMBRE, item.CIUDAD, item.EMAIL,item.ESPECIALIDAD);
                
        }

    }
}
