﻿

using System.Collections.Generic;
using Big.MeadJohnson.Notificaciones.Console.Domain;
using Big.MeadJohnson.Notificaciones.Console.Interfaces;

namespace Big.MeadJohnson.Notificaciones.Console.Implements
{
    public class MedicDay : ITemplate
    {
        public string TableConstruct(List<Homenajeado> homenajeados)
        {
            string colorhexadecimalRecuadro = "dbe6ae";
            int counter = 0;
            string tabla = "<table width='586' cellspacing='0' cellpadding='0' border='0' align='left'><tr>";
            tabla += "<td width = '31' height='261' style = 'font-size: 1px;'>&nbsp;</td><td>";

            string tabla1 = "<table width = '262' cellspacing = '0' cellpadding = '0' border = '0' align = 'left'><tr><td width = '262' height = '2' style = 'font-size: 1px;'>&nbsp;</td></tr>";
            string tabla2 = "<table width = '262' cellspacing = '0' cellpadding = '0' border = '0' align = 'left'><tr><td width = '262' height = '2' style = 'font-size: 1px;'>&nbsp;</td></tr>";
            foreach (var home in homenajeados)
            {
                if (counter % 2 == 0)
                {
                    tabla1 += string.Format("<tr><td width='262' height='83' style='background-color: " + colorhexadecimalRecuadro + "; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #29408A; text-align: center; box-sizing: border-box; border-radius: 8px;'><strong style='font-size: 16px;'> {0} </strong> <br>{3} / {2} <br>{1}</td>  </tr> <tr><td width = '262' height='10' style = 'font-size: 1px;'>&nbsp;</td></tr>", home.NOMBRE, home.EMAIL, home.CIUDAD, home.ESPECIALIDAD);
                }
                else
                {
                    tabla2 += string.Format("<tr><td width='262' height='83' style='background-color: " + colorhexadecimalRecuadro + "; font-family: Arial, Helvetica, sans-serif; font-size: 12px; color: #29408A; text-align: center; box-sizing: border-box; border-radius: 8px;'><strong style='font-size: 16px;'> {0} </strong> <br>{3} / {2} <br>{1}</td>  </tr> <tr><td width = '262' height='10' style = 'font-size: 1px;'>&nbsp;</td></tr>", home.NOMBRE, home.EMAIL, home.CIUDAD, home.ESPECIALIDAD);
                }


                counter++;
            }

            tabla1 += "</table>";
            tabla2 += "</table>";

            tabla += "<td>" + tabla1 + "</td>";

            tabla += "<td width = '31' height='261' style = 'font-size: 1px;'>&nbsp;</td>";

            tabla += "<td>" + tabla2 + "</td>";

            tabla += @"</td><td width = '25' height = '261' style = 'font-size: 1px;'>&nbsp;</td></tr></table>";

            return tabla;
        }
    }
}
