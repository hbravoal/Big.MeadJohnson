﻿namespace Big.MeadJohnson.Notificaciones.Console.Interfaces
{
    using Console.Domain;
    using System.Collections.Generic;
    public interface ITemplate
    {
        string TableConstruct(List<Homenajeado> homenajeados);
    }
}

