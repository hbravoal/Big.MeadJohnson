﻿namespace Big.MeadJohnson.Notificaciones.Core
{
    using Console.Core;
    using Console.Domain;
    using Console.Implements;
    using Console.Interfaces;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;

    public class Procesos
    {
        internal static void ObtenerListadoNotificaciones()
        {
            try
            {
                using (dbBigMeadJohnsonEntities db = new dbBigMeadJohnsonEntities())
                {
                    var receptores = db.PrMjCargarDestinatariosAlertas().ToList();
                    var fechasEspeciales = db.PrMjCargarFechasEspeciales().ToList();

                    foreach (var fechaEspecial in fechasEspeciales.Where(x => x.ACTIVE == true).ToList())
                    {


                        if (fechaEspecial.ALERT_PREVIOUS_DAYS.Value != 0)
                        {
                            var fechaHomenaje = new DateTime(DateTime.Now.Year, fechaEspecial.CELEBRATION_MONTH.Value, fechaEspecial.CELEBRATION_DAY.Value);

                            if (DateTime.Now.ToShortDateString() != fechaHomenaje.AddDays(-(fechaEspecial.ALERT_PREVIOUS_DAYS.Value)).ToShortDateString())
                            {
                                continue;
                            }
                        }

                        List<Homenajeado> homenajeados = new List<Homenajeado>();

                        foreach (var receptor in receptores)
                        {
                            var inDestinatarioID = new SqlParameter
                            {
                                ParameterName = "inDestinatarioID",
                                Value = receptor.ID.ToString().ToUpper()
                            };

                            var inTipoDestinatario = new SqlParameter
                            {
                                ParameterName = "inTipoDestinatario",
                                Value = Convert.ToDecimal(receptor.TIPO.ToString())
                            };

                            List<Homenajeado> _homenajeados = new List<Homenajeado>();

                            _homenajeados = db.Database.SqlQuery<Homenajeado>("exec " + fechaEspecial.SP_USED + " @inDestinatarioID, @inTipoDestinatario ", inDestinatarioID, inTipoDestinatario).ToList<Homenajeado>();

                            if (_homenajeados.Count < 1)
                                continue;


                            Hashtable parameters = new Hashtable();
                            string tabla;
                            string mailMessageId;
                            var tipoDecimal = Convert.ToDecimal(receptor.TIPO);
                            var tipoEncargado = tipoDecimal == 1 ? "Representante" : tipoDecimal == 2 ? "Gerente" : "Representante Secundario";
                            parameters.Add("[NOMBRE_REPRESENTANTE]", receptor.NAME);
                            parameters.Add("[RELACION]", tipoEncargado);
                            parameters.Add("[TIPOALERTA]", _homenajeados[0].TIPOALERTA);
                            ITemplate itemplate = new BirthDayAlert();


                              
                                    mailMessageId = "AlertaCumpleanios";
                                    tabla = itemplate.TableConstruct(_homenajeados);                                    
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);
                            parameters.Remove("[TABLA]");

                                    mailMessageId = "AlertaCumpleanios";
                                    tabla = itemplate.TableConstruct(_homenajeados);
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);


                             
                            parameters.Remove("[TABLA]");
                                    mailMessageId = "AlertaAniversarioEjercicio";
                            itemplate = new DefaultSpecialDate();
                                    tabla = itemplate.TableConstruct(_homenajeados);
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);
                            parameters.Remove("[TABLA]");
                            itemplate = new DefaultSpecialDate();
                            mailMessageId = "AlertaDiaMedico";
                            tabla = itemplate.TableConstruct(_homenajeados);
                            parameters.Add("[TABLA]", tabla);
                            MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);

                            itemplate = new DefaultSpecialDate();
                            parameters.Remove("[TABLA]");
                                    mailMessageId = "alertaDiaSecretaria";
                                    tabla = itemplate.TableConstruct(_homenajeados);
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);

                            parameters.Remove("[TABLA]");
                                    mailMessageId = "alertaDiaPediatra";
                                    tabla = itemplate.TableConstruct(_homenajeados);
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);
                            parameters.Remove("[TABLA]");
                                    mailMessageId = "alertaDiaPadre";
                                    tabla = itemplate.TableConstruct(_homenajeados);
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);
                            parameters.Remove("[TABLA]");
                                    mailMessageId = "alertaDiaMadre";
                                    tabla = itemplate.TableConstruct(_homenajeados);
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);
                               parameters.Remove("[TABLA]");

                            //PENDIENTE
                            //case "CUMPLEAÑOS SECRETARIA":
                            //    mailMessageId = "AlertaHomenajeados";
                            //    tabla = construirTabla(_homenajeados);
                            //    break;

                                    mailMessageId = "alertaDiaEnfermera";
                                    tabla = itemplate.TableConstruct(_homenajeados);
                                    parameters.Add("[TABLA]", tabla);
                                    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);

                            //case "DÍA DEL MÉDICO":
                            //    mailMessageId = "AlertaHomenajeados";
                            //    tabla = construirTabla(_homenajeados);
                            //    break;
                            //case "NUEVO PAPÁ":
                            //    mailMessageId = "AlertaHomenajeados";
                            //    tabla = construirTabla(_homenajeados);
                            //    break;
                            //case "NUEVA MAMÁ":
                            //    mailMessageId = "AlertaHomenajeados";
                            //    tabla = construirTabla(_homenajeados);
                            //    break;
                            //case "GRADUACIÓN ESPECIALIDAD":
                            //    mailMessageId = "AlertaHomenajeados";
                            //    tabla = construirTabla(_homenajeados);
                            //    break;

                            //mailMessageId = "AlertaHomenajeados";
                            //tabla = construirTabla(_homenajeados);





                            //switch (_homenajeados.FirstOrDefault().TIPOALERTA.ToString())
                            //{
                            //    case "CUMPLEAÑOS":
                            //        mailMessageId = "AlertaCumpleanios";                                    
                            //         tabla = construirTablaCumplenios(_homenajeados);
                            //        break;
                            //    case "REFUERZO CUMPLEAÑOS":
                            //        mailMessageId = "AlertaCumpleanios";
                            //        tabla = construirTablaCumplenios(_homenajeados);
                            //        break;
                            //    case "ANIVERSARIO DE EJERCICIO":
                            //        mailMessageId = "AlertaAniversarioEjercicio";
                            //        tabla = p.TableConstruct(_homenajeados);
                            //        break;
                            //    case "DÍA DE LA SECRETARIA":
                            //        mailMessageId = "alertaDiaSecretaria";
                            //        tabla = p.TableConstruct(_homenajeados);
                            //        break;
                            //    case "DÍA DEL PEDIATRA":
                            //        mailMessageId = "alertaDiaPediatra";
                            //        tabla = construirTabla(_homenajeados);
                            //        break;
                            //    case "DÍA DEL PADRE":
                            //        mailMessageId = "alertaDiaPadre";
                            //        tabla = construirTabla(_homenajeados);
                            //        break;
                            //    case "DÍA DE LA MADRE":
                            //        mailMessageId = "alertaDiaMadre";
                            //        tabla = construirTabla(_homenajeados);
                            //        break;
                            //        //PENDIENTE
                            //    //case "CUMPLEAÑOS SECRETARIA":
                            //    //    mailMessageId = "AlertaHomenajeados";
                            //    //    tabla = construirTabla(_homenajeados);
                            //    //    break;
                            //    case "DÍA DE LA ENFERMERA":
                            //        mailMessageId = "alertaDiaEnfermera";
                            //        tabla = construirTabla(_homenajeados);
                            //        break;
                            //    //case "DÍA DEL MÉDICO":
                            //    //    mailMessageId = "AlertaHomenajeados";
                            //    //    tabla = construirTabla(_homenajeados);
                            //    //    break;
                            //    //case "NUEVO PAPÁ":
                            //    //    mailMessageId = "AlertaHomenajeados";
                            //    //    tabla = construirTabla(_homenajeados);
                            //    //    break;
                            //    //case "NUEVA MAMÁ":
                            //    //    mailMessageId = "AlertaHomenajeados";
                            //    //    tabla = construirTabla(_homenajeados);
                            //    //    break;
                            //    //case "GRADUACIÓN ESPECIALIDAD":
                            //    //    mailMessageId = "AlertaHomenajeados";
                            //    //    tabla = construirTabla(_homenajeados);
                            //    //    break;
                            //    default:
                            //        mailMessageId = "AlertaHomenajeados";
                            //        tabla = construirTabla(_homenajeados);
                            //        break;

                            //}
                            //parameters.Add("[TABLA]", tabla);
                            //try
                            //{
                            //    MailingHelper.SendMail(parameters, receptor.EMAIL, mailMessageId);

                            //}
                            //catch (Exception ex)
                            //{
                            //    //Insertar en Tabla NOTIFICATION_LOGS
                            //}

                            #region Fechas Switch
                            //switch (fechaEspecial.NAME)
                            //{
                            //    case "CUMPLEAÑOS":
                            //        //db.Database.ExecuteSqlCommand()
                            //        var listaHomenajeados = db.PrMjBirthday(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = listaHomenajeados.Select(x => new Homenajeado
                            //        {
                            //            NOMBRE = x.NOMBRE,
                            //            EMAIL = x.E_MAIL,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString()
                            //        }).ToList();

                            //        break;
                            //    case "ANIVERSARIO DE EJERCICIO":
                            //        var listaAniv = db.PrMjAnniversaryExercise(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = listaAniv.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            EMAIL = x.EMAIL,
                            //            FECHA = x.FECHAINICIOPROFESION.Value.ToShortDateString(),
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "DÍA DE LA SECRETARIA":
                            //        var secres = db.PrMjSecretartDay(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = secres.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            EMAIL = x.EMAIL,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString(),
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "DÍA DEL PEDIATRA":
                            //        var pediatras = db.PrMkPediatrianDay(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = pediatras.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            EMAIL = x.EMAIL,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString(),
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "CUMPLEAÑOS 2":
                            //        //Procedimient refuerzo
                            //        break;
                            //    case "DÍA DEL PADRE":
                            //        var padres = db.PrMjFatherDay(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = padres.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString(),
                            //            EMAIL = x.EMAIL,
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "DÍA DE LA MADRE":
                            //        var madres = db.PrMjMotherDay(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = madres.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString(),
                            //            EMAIL = x.EMAIL,
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "CUMPLEAÑOS SECRETARIA":
                            //        var cumpleSecres = db.PrMjSecretaryBirthday(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = cumpleSecres.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = "Ciudad",
                            //            EMAIL = x.EMAIL,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString(),
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "DÍA DE LA ENFERMERA":
                            //        var enfermeras = db.PrMjNurseDay(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = enfermeras.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = "Ciudad",
                            //            EMAIL = x.E_MAIL,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString(),
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "DÍA DEL MÉDICO":
                            //        var medicos = db.PrMjDoctorDay(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = medicos.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = "Ciudad",
                            //            EMAIL = x.E_MAIL,
                            //            FECHA = x.BIRTH_DATE.Value.ToShortDateString(),
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "NUEVO PAPÁ":
                            //        var papas = db.PrMjNewDad(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = papas.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            EMAIL = x.E_MAIL,
                            //            FECHA = x.FECHAESTIMADA,
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "NUEVA MAMÁ":
                            //        var mamas = db.PrMjNewMom(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = mamas.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            EMAIL = x.E_MAIL,
                            //            FECHA = x.FECHAESTIMADA,
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //    case "GRADUACIÓN ESPECIALIDAD":
                            //        var espcializados = db.PrMjSpecialGraduation(receptor.ID.ToString().ToUpper()).ToList();
                            //        homenajeados = espcializados.Select(x => new Homenajeado
                            //        {
                            //            CIUDAD = x.CIUDAD,
                            //            EMAIL = x.E_MAIL,
                            //            FECHA = x.FECHAFINESPECIALIDAD.Value.ToShortDateString(),
                            //            NOMBRE = x.NOMBRE
                            //        }).ToList();
                            //        break;
                            //}
                            #endregion Fechas Switch
                            //Enviar correo a cada receptor
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static string construirTabla(List<Homenajeado> homenajeados)
        {
            string tabla = "<table style='border-collapse:collapse; border-spacing:0'>";
            tabla += ("<tr><th style='border-style:solid;border-width:1px;border-color:#333333;'>" + "NOMBRE" + "</th>");
            tabla += ("<th style='border-style:solid;border-width:1px;border-color:#333333;'>" + "EMAIL" + "</th>");
            tabla += ("<th style='border-style:solid;border-width:1px;border-color:#333333;'>" + "CIUDAD" + "</th>");
            tabla += ("<th style='border-style:solid;border-width:1px;border-color:#333333;'>" + "FECHA" + "</th></tr>");
            foreach (var home in homenajeados)
            {
                tabla += string.Format(@"<tr>
                    <td style='border-style:solid;border-width:1px;border-color:inherit;'>{0}</td>
                    <td style='border-style:solid;border-width:1px;border-color:inherit;'>{1}</td>  
                    <td style='border-style:solid;border-width:1px;border-color:inherit;'>{2}</td> 
                    <td style='border-style:solid;border-width:1px;border-color:inherit;'>{3}</td> 
                     </tr>", home.NOMBRE, home.EMAIL, home.CIUDAD, home.FECHA);
            }
            tabla += "</table>";

            return tabla;
        }
        //private static string construirTablaCumplenios(List<Homenajeado> homenajeados)
        //{
            
        //}
    }
}
