﻿using Big.MeadJohnson.Notificaciones.Console.Common.Diagnostics;
using DMG.Core.Mailer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Notificaciones.Console.Core
{
    public class MailingHelper
    {
        static MailHelper _mailHelper = new MailHelper(ConfigurationManager.AppSettings["MailConfiguration"], false);
        
        public static void SendMailX(Hashtable parameters, string mailTo, string messageId, string identification)
        {
            bool isValid = true;
            string guid = Guid.NewGuid().ToString();
            parameters.Add("[SendEmailGuid]", guid);
            parameters.Add("[IdentificationNumber]", identification);
            string msg = string.Empty;
            try
            {
                _mailHelper.Send(parameters, messageId, mailTo, true);
                ExceptionLogging.LogInfo("Correo " + messageId + " enviado a " + mailTo);
            }
            catch (System.Exception ex)
            {
                ExceptionLogging.LogException(ex);
                ExceptionLogging.LogWarn("No se pudo enviar el correo " + messageId + " a " + mailTo);
                isValid = false;
                msg = ex.Message.ToString();
            }

            //try
            //{
            //    Domain.Generated.SendMail objSendMail = new Domain.Generated.SendMail();

            //    objSendMail.Guid = guid;
            //    objSendMail.MailId = messageId;
            //    objSendMail.SentIdentificationClientNumber = identification;
            //    objSendMail.DateSend = DateTime.Now;
            //    if (isValid)
            //    {
            //        objSendMail.Send = true;
            //        objSendMail.MessageException = null;
            //    }
            //    else
            //    {
            //        objSendMail.Send = false;
            //        objSendMail.MessageException = msg;
            //    }
            //    objSendMail.Retry = null;

            //    objSendMail.Save();
            //}
            //catch (Exception ex)
            //{

            //    ExceptionLogging.LogException(ex);
            //    ExceptionLogging.LogWarn("No se pudo guardar la informacion del correo " + messageId + " a " + mailTo + " , del usuario con identificacion=" + identification);
            //}




        }

        public static void SendMail(Hashtable parameters, string mailTo, string messageId)
        {
            try
            {
                _mailHelper.Send(parameters, messageId, mailTo, true);
                ExceptionLogging.LogInfo("Correo " + messageId + " enviado a " + mailTo);
            }
            catch (System.Exception ex)
            {
                ExceptionLogging.LogException(ex);
                ExceptionLogging.LogWarn("No se pudo enviar el correo " + messageId + " a " + mailTo);
                throw ex;
            }
        }

        public static void SendMail(string mailTo, string message, string subject)
        {
            try { _mailHelper.Send(mailTo, message, subject, false); }
            catch (System.Exception ex)
            { ExceptionLogging.LogWarn("No se pudo enviar el correo " + subject + " a " + mailTo); }
        }

        public static void SendMail(string mailTo, string message, string subject, bool bodyIsHtml)
        {
            try { _mailHelper.Send(mailTo, message, subject, bodyIsHtml); }
            catch (System.Exception ex)
            { ExceptionLogging.LogWarn("No se pudo enviar el correo " + subject + " a " + mailTo); }
        }

        public static List<string> ParseEmailString(string emails)
        {
            char[] fieldSeparator = { ';' };
            List<string> emailList = new List<string>();

            emailList.AddRange(emails.Split(fieldSeparator, StringSplitOptions.RemoveEmptyEntries));

            return emailList;
        }
    }
}
