﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Notificaciones.Console.Domain
{
    public class Homenajeado
    {
        public string NOMBRE { get; set; }
        public string EMAIL { get; set; }
        public string CIUDAD { get; set; }
        public string FECHA { get; set; }
        public string TIPOALERTA { get; set; }
        public string ESPECIALIDAD { get; set; }


    }
}
