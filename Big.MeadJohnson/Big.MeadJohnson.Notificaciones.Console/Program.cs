﻿using Big.MeadJohnson.Notificaciones.Console.Common.Diagnostics;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.MeadJohnson.Notificaciones.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            //CultureInfo.CreateSpecificCulture("es-CO");
            try
            {
                Batch.Start();
            }
            catch (Exception ex)
            {
                ExceptionLogging.LogException(ex);
                throw ex;
            }
        }
    }
}
